﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System;
using System.Collections;

[CustomEditor(typeof(PatternEditorNode))]
public class PatternEditor : Editor {


    public int		countS = 0;
    public int		countM = 0;
    public int		countL = 0;
	public int		countB = 0;

	public	JSONObject	json = new JSONObject();

	private string output = "";
	private string input = "";
	private TextAsset txtAsset;
	private Vector2	scroll;

	public void Awake()
	{
		PatternEditorNode script = (PatternEditorNode)target;
		script.CheckCount(this);
	}

	public override void OnInspectorGUI()
	{
		PatternEditorNode script = (PatternEditorNode)target;

		DrawDefaultInspector();

		EditorGUILayout.LabelField("[[[[[ Making Object ]]]]]");

        countS = EditorGUILayout.IntField("Small Count : ", countS);
        countM = EditorGUILayout.IntField("Middle Count : ", countM);
        countL = EditorGUILayout.IntField("Large Count : ", countL);
        countB = EditorGUILayout.IntField("Boss Count : ", countB);

        if(GUILayout.Button("MAKE OBJECTS"))
        {
			script.RemoveAllObjects();
			script.MakeObjects("S", countS);
			script.MakeObjects("M", countM);
			script.MakeObjects("L", countL);
			script.MakeObjects("B", countB);
        }
		

		EditorGUILayout.LabelField("");
		EditorGUILayout.LabelField("[[[[[ Export JSON ]]]]]");
		
		output = (json.IsNull)? "" : json.ToString();
		output = EditorGUILayout.TextArea (output, GUILayout.Height (100));


		if (GUILayout.Button ("MAKE JSON")) {
			script.MakeJSON (this);
		}

		EditorGUILayout.LabelField("");
		EditorGUILayout.LabelField("[[[[[ Import from JSON ]]]]]");

		input = EditorGUILayout.TextArea (input, GUILayout.Height (100));

		if (GUILayout.Button ("FROM JSON")) {

			//inputPattern = JsonUtility.FromJson<PEDataPattern> (input);

			//inputPattern = JsonUtility.FromJson<PEDataPattern> (input);

			json = new JSONObject(input);

			if (json.IsNull) {
				input = "JSON parse ERROR!";
				OnInspectorGUI ();
				return;
			}

			script.FromJSON(json);
		}

	}
}

#endif