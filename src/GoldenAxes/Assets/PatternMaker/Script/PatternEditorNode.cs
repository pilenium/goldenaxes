﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PatternEditorNode : MonoBehaviour {
	
	public PatternObject GetObject(string size)
	{
		PatternObject[]	objs = FindObjectsOfType<PatternObject> ();

		//PatternObject[]	objs = transform.GetComponentsInParent<PatternObject> ();
		foreach (PatternObject obj in objs) {
			if (obj.strSize == size) {
				return obj;
			}
		}
		return null;
	}

	public void CheckCount(PatternEditor editor)
	{
		editor.countS = 0;
		editor.countM = 0;
		editor.countL = 0;
		editor.countB = 0;
		
		PatternObject[]	objs = GetComponentsInChildren<PatternObject> ();

		JSONObject json = new JSONObject();
			
		foreach (PatternObject obj in objs)
		{
			switch(obj.strSize)
			{
				case "S":
					editor.countS++;
				break;

				case "M":
					editor.countM++;
				break;

				case "L":
					editor.countL++;
				break;

				case "B":
					editor.countB++;
				break;
			}
		}


	}

	public void FromJSON(JSONObject json)
	{
		if (json == null) {
			return;
		}

		RemoveAllObjects();

		for (int i = 0; i < json["objects"].Count ; ++i)
		{
			MakeObject(json["objects"][i]["size"].str, json["objects"][i]["x"].f - json["width"].f / 2, json["objects"][i]["z"].f - json["height"].f / 2);
		}
	}

	public void RemoveAllObjects()
	{
		PatternObject[]	objs = GetComponentsInChildren<PatternObject> (false);

		foreach (PatternObject obj in objs) {
			DestroyImmediate (obj.gameObject);
		}
	}

	public PatternObject MakeObject(string size, float pos_x, float pos_z)
	{
		PatternObject obj = GetObject(size);
		PatternObject nObj = null;
		if (obj != null) {
			GameObject go = Instantiate (obj.gameObject) as GameObject;
			nObj = go.GetComponent<PatternObject>() as PatternObject;
			go.transform.parent = this.transform;

			Vector3 pos = new Vector3();
			pos.x = pos_x;
			pos.z = pos_z;
			pos.y = obj.fSize / 2;
			go.transform.localPosition = pos;
		}
		return nObj;
	}

	public void MakeObjects(string size, int count)
	{
		PatternObject[]	objs = GetComponentsInChildren<PatternObject> (false);
		//int i = objs.Length;

		float pos_x;
		float pos_z;

		float start_x = -3.5f;
		float start_z = 13;
		float width = 4;
		float height = 4;
		float ver_count = 6;

		for(int i = objs.Length ; i < count + objs.Length ; ++i) 
		{
			Debug.Log(Mathf.FloorToInt(i/ver_count));
			pos_x = start_x + width * Mathf.FloorToInt(i/ver_count);
			pos_z = start_z - height * (i%ver_count);
			MakeObject(size, pos_x, pos_z);
		}
	}

	public Rect GetRect()
	{
		float rectL = 11;
		float rectR = -11;
		float rectT = -20;
		float rectB = 20;

		PatternObject[]	objs = GetComponentsInChildren<PatternObject> ();

		if(objs.Length == 0)
			return Rect.zero;
			
		foreach (PatternObject obj in objs)
		{
			if (obj.transform.localPosition.x - obj.fSize / 2 < rectL) {
				rectL = obj.transform.localPosition.x - obj.fSize / 2;
			}

			if (obj.transform.localPosition.x + obj.fSize / 2 > rectR) {
				rectR = obj.transform.localPosition.x + obj.fSize / 2;
			}

			if (obj.transform.localPosition.z - obj.fSize / 2 < rectB) {
				rectB = obj.transform.localPosition.z - obj.fSize / 2;
			}

			if (obj.transform.localPosition.z + obj.fSize / 2 > rectT) {
				rectT = obj.transform.localPosition.z + obj.fSize / 2;
			}
		}

		Rect rect = new Rect();
		rect.x = rectL;
		rect.y = rectB;

		rect.width = rectR - rectL;
		rect.height = rectT - rectB;

		return rect;

	}

	public void MakeJSON(PatternEditor editor)
	{
		PatternObject[]	objs = GetComponentsInChildren<PatternObject> ();

		JSONObject json = new JSONObject();

		Rect rect = GetRect();

		int epsilon = 100;
		float width = Mathf.Round(rect.width * epsilon) / epsilon;
		float height = Mathf.Round(rect.height * epsilon) / epsilon;

		json.AddField("width", width);
		json.AddField("height", height);

		JSONObject objArray = new JSONObject(JSONObject.Type.ARRAY);
		foreach (PatternObject obj in objs)
		{
			JSONObject objJson = JSONObject.Create();
			objJson.AddField("size", obj.strSize);
			float x = Mathf.Round((obj.transform.localPosition.x - rect.x) * epsilon) / epsilon;
			float z = Mathf.Round((obj.transform.localPosition.z - rect.y) * epsilon) / epsilon;
			objJson.AddField("x", x);
			objJson.AddField("z", z);

			objArray.Add(objJson);
		}
		json.AddField("objects", objArray);

		editor.json = json;
		// 다시 그리자
		FromJSON(json);
		editor.Repaint ();

	}
}

#endif