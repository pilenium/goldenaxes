﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameUI : MonoBehaviour {

	public UIRoot				uiRoot;
	public GameObject			effect;
	public UITop				top;
	public UIAxe				axe;
	public UIManagement			management;
	public UIBottom				bottom;
	public GameObject			popup;

	public	UILabel				labelDebug;

	public	int					ui_width				{ get; private set; }
	public	int					ui_height				{ get; private set; }

	public	float				pixelSizeAdjustment		{ get; private set; }

	private UIPopupManager		m_popupManager;

	// Use this for initialization
	void Awake () {
		RemoveDebug ();
		
		uiRoot = GameObject.Find("UI Root").GetComponent<UIRoot>() as UIRoot;

		PGTool.NGUIManager.SetDepths(top.gameObject, Common.UI_TOP_DEPTH);
		PGTool.NGUIManager.SetDepths(axe.gameObject, Common.UI_AXE_DEPTH);
		PGTool.NGUIManager.SetDepths(management.gameObject, Common.UI_MANAGEMENT_DEPTH);
		PGTool.NGUIManager.SetDepths(bottom.gameObject, Common.UI_BOTTOM_DEPTH);
	}

	public void InitResolution()
	{
		if(uiRoot == null)
		{
			// error : cannot find root ui
		}

		/*
		if(root.fitWidth)
		{
			float actual_ui_height = (float) Screen.height * root.pixelSizeAdjustment;
			
			ui_width = root.manualWidth;
			ui_height = (int) actual_ui_height;
		}
		else
		{
			float actual_ui_width = (float) Screen.width * root.pixelSizeAdjustment;
			
			ui_width = (int) actual_ui_width;
			ui_height = root.manualHeight;
		} */
		
		pixelSizeAdjustment = uiRoot.pixelSizeAdjustment;
		float actual_ui_width = (float) Screen.width * pixelSizeAdjustment;
		float actual_ui_height = (float) Screen.height * pixelSizeAdjustment;

		if(actual_ui_height / actual_ui_width > Common.BASE_RATIO)
		{
			actual_ui_height = Common.BASE_RATIO * actual_ui_width;
		}

		ui_width = (int) actual_ui_width;
		ui_height = (int) actual_ui_height;

		effect.transform.localPosition = new Vector3(-ui_width/2, -ui_height/2, 0);

		top.SetScreenSize(ui_width, ui_height, false, false);

		axe.SetScreenSize(ui_width, ui_height, false, false);
		bottom.SetScreenSize(ui_width, ui_height, false, false);
		management.SetScreenSize(ui_width, ui_height, false, false);
	}

	public void Init()
	{
		//TODO 여기서 팝업매니저 크리에이트
		m_popupManager = new UIPopupManager();

		bottom.SetStrings();
	}

	public void CheckProperties()
	{
		management.CheckProperties();
		top.CheckDamages();
	}

	public void CheckPrice()
	{
		top.CheckCoin();
		management.CheckPrice();
	}


	public void SetDebug(string ss)
	{
		labelDebug.gameObject.SetActive (true);
		labelDebug.text += ss;
	}

	public void RemoveDebug()
	{
		labelDebug.text = "";
		labelDebug.gameObject.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
	
	}

	public GameObject AddUI(string prefab)
	{
		GameObject ret = PrefabManager.LoadPrefab(prefab);

		return ret;
	}

	// 팝업 관련
	/*
	public UIPopup AddMsgPopup(string title, string msg)
	{
		UIPopup nPopup = m_popupManager.Create<UIPopupMsg>(url);

		return nPopup;
	}
	*/
	public UIPopup AddPopup<T>(string url) where T : UIPopup
	{
		UIPopup nPopup = m_popupManager.Create<T>(url);

		return nPopup;
	}
	public bool ClosePopup(UIPopup popup)
	{
		return m_popupManager.ClosePopup(popup);
	}
}
