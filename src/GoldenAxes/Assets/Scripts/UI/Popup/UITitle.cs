﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITitle : MonoBehaviour {

	private Animator[]		m_animators;
	public UISprite			logo;
	public UILabel			copyright;

	// Use this for initialization
	void Awake () {
		m_animators = GetComponents<Animator> ();
	}


	public void OnFade()
	{
		ModeTitle mode = GamePlay.instance.mode as ModeTitle;
		if (mode == null) {
			// ERRROR
			return;
		}
		mode.ShowUI ();
	}
	public void OnEnd()
	{
		// animation stop
		foreach(Animator ani in m_animators)
		{
			ani.enabled = false;
		}

		ModeTitle mode = GamePlay.instance.mode as ModeTitle;
		if (mode == null) {
			// ERRROR
			return;
		}
		mode.BeginGame ();
	}
}
