﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIStageIntro : MonoBehaviour {
	private Animator[]	m_animators;
	public UILabel	labelStage;
	public UILabel	labelChapter;

	// delegate
	public delegate void OnEndAnimation ();
	public OnEndAnimation onEndAnimation = null;

	// Use this for initialization
	void Awake () {
		onEndAnimation = null;

		m_animators = GetComponents<Animator> ();

		labelStage.text = "STAGE " + Data.user.stage.Get().ToString();
		labelChapter.text = Data.strings.GetString(GamePlay.instance.dataChapter.name);
	}

	void OnDestroy()
	{
		m_animators = null;
		onEndAnimation = null;
	}
	

	private void EndAnimation()
	{
		if(onEndAnimation != null)
		{
			onEndAnimation();
		}
	}
}
