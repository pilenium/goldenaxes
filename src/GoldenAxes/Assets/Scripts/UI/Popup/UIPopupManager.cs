﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPopupManager {

	public List<UIPopup>		m_popups;


	public UIPopupManager()
	{
		m_popups = new List<UIPopup>();
	}

	~UIPopupManager()
	{
		foreach(UIPopup popup in m_popups)
		{
	//		popup.Close();
	//		GameObject.DestroyImmediate (popup.gameObject);
		}
		m_popups.Clear();
		
	}

	public UIPopup Create<T>(string url) where T : UIPopup
	{
//		GameObject go = GameObject.Instantiate (Resources.Load(url)) as GameObject;
		GameObject go = NGUITools.AddChild(GamePlay.instance.ui.popup, Resources.Load(url) as GameObject);

		UIPopup popup = go.AddComponent<T>() as UIPopup;
		popup.SetPanelDepth(GetNextPanelDepth());
		popup.Open();
		m_popups.Add(popup);

		return popup;
	}

	public bool ClosePopup(UIPopup popup)
	{
		bool result = m_popups.Remove(popup);

		if(result)
		{
			ReOrderPopupPanels();
		}

		GameObject.Destroy(popup.gameObject);

		return result;

	}


	private int GetNextPanelDepth()
	{
		if(m_popups.Count == 0)	return Common.UI_PANEL_POPUP_BASE_DEPTH;

		return m_popups[m_popups.Count - 1].GetLastestPanelCount() + 1;
	}

	private void ReOrderPopupPanels()
	{
		int depth = Common.UI_PANEL_POPUP_BASE_DEPTH;
		foreach(UIPopup popup in m_popups)
		{
			popup.SetPanelDepth(depth);
			depth += popup.GetLastestPanelCount();
		}
	}
}
