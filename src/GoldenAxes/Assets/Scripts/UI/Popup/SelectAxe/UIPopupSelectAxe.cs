﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPopupSelectAxe : UIPopup {
	protected UIPanel			m_itemScrollPanel;
	protected UIScrollView		m_itemScrollView;

    public override void Awake()
	{
		base.Awake();

		GameObject itemScroll = m_popup.transform.Find("ItemScroll").gameObject;
		m_itemScrollPanel = itemScroll.GetComponent<UIPanel>() as UIPanel;
		m_itemScrollView = itemScroll.GetComponent<UIScrollView>() as UIScrollView;
	}

	public override void SetPanelDepth(int depth)
	{
		base.SetPanelDepth(depth);
		m_itemScrollPanel.depth = depth + 1;

	}
	public override int GetLastestPanelCount()
	{
		return m_itemScrollPanel.depth;
	}
	public override int GetPanelCount()
	{
		return 2;
	}
	public override void Init()
	{
		Data.strings.SetTextFromKey(m_labelTitle, "UI_SELECT_AXE");
		InitButtons();
	}


	protected virtual void InitButtons()
	{
		// 이건 팝업이니깐 풀에서 안쓰고 걍 지워버리자
		Vector3 pos = Vector3.zero;
		GameObject buttonGO;
		UIButtonSelectAxe button;
		int count = 0;
		foreach(KeyValuePair<int, ItemAxe> kv in Data.inventory.GetAxes())
		{
			pos.x = 154f * count;

			buttonGO = PrefabManager.LoadNGUIPrefab(m_itemScrollView.gameObject, "Prefaps/UI/Popup/ButtonSelectAxe");
			buttonGO.transform.parent = m_itemScrollView.gameObject.transform;
			buttonGO.transform.localScale = Vector3.one;
			buttonGO.transform.localPosition = pos;

			button = buttonGO.AddComponent<UIButtonSelectAxe>() as UIButtonSelectAxe;
			button.Init(kv.Value, this);

			UIDragScrollView dragScrollView = buttonGO.GetComponent<UIDragScrollView>() as UIDragScrollView;
			dragScrollView.scrollView = m_itemScrollView;


			count++;
		}
	}

	public void SetPosY(float y)
	{
		Vector3 pos = Vector3.zero;
		pos.y = y + 210f;
		m_popup.transform.localPosition = pos;

	}
}
