﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonSelectAxe : MonoBehaviour {

	protected UILabel		m_labelName;
	protected UILabel		m_labelInfo;

	protected UIButton		m_buttonDetail;
	protected UILabel		m_labelDetail;

	protected UIButton		m_buttonEquip;
	protected UILabel		m_labelEquip;

	protected UIPopup		m_parentPopup;
	protected ItemAxe		m_itemAxe;

	public virtual void Awake()
	{
		m_labelName = this.transform.Find("LabelName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelInfo = this.transform.Find("LabelInfo").gameObject.GetComponent<UILabel>() as UILabel;

		m_buttonDetail = this.transform.Find("ButtonDetail").gameObject.GetComponent<UIButton>() as UIButton;
		m_labelDetail = m_buttonDetail.transform.Find("Label").gameObject.GetComponent<UILabel>() as UILabel;

		m_buttonEquip = this.transform.Find("ButtonEquip").gameObject.GetComponent<UIButton>() as UIButton;
		m_labelEquip = m_buttonEquip.transform.Find("Label").gameObject.GetComponent<UILabel>() as UILabel;


		UIEventListener.Get (m_buttonDetail.gameObject).onClick += this.OnClickDetail;
		UIEventListener.Get (m_buttonEquip.gameObject).onClick += this.OnClickEquip;
		
	}

	public void OnDestroy()
	{
		UIEventListener.Get (m_buttonDetail.gameObject).onClick -= this.OnClickDetail;
		UIEventListener.Get (m_buttonEquip.gameObject).onClick -= this.OnClickEquip;
	}

	public void Init(ItemAxe itemAxe, UIPopup parentPopup)
	{
		m_itemAxe = itemAxe;
		m_parentPopup = parentPopup;

		// icon
		GameObject go = PrefabManager.LoadNGUIPrefab(gameObject, "Prefaps/UI/IconAxe");
		
		go.transform.localPosition = new Vector3(0, 39, 0);

		UIIconAxe icon = go.GetComponent<UIIconAxe>() as UIIconAxe;
		PGTool.NGUIManager.SetDepths(icon.gameObject, 10);
		icon.SetAxe(itemAxe);
		icon.SetActiveColiider(false);

		// name
		Data.strings.SetTextFromKey(m_labelName, itemAxe.data.throwSlot.name);

		DataAxeSlot dataSlot = m_itemAxe.data.throwSlot;
		string strInfo = Data.strings.GetString(dataSlot.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(dataSlot.GetParam1(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(dataSlot.GetParam2(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(dataSlot.GetParam3(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", dataSlot.activeTime.ToString());
		strInfo = strInfo.Replace("DT", dataSlot.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
		Data.strings.SetText(m_labelInfo, strInfo);

		Data.strings.SetTextFromKey(m_labelDetail, "UI_DETAIL");
		Data.strings.SetTextFromKey(m_labelEquip, "UI_EQUIP");
	}

	protected virtual void OnClickDetail(GameObject go)
	{
		// TODO 도끼 디테일 페이지
	}
	protected virtual void OnClickEquip(GameObject go)
	{
		Data.user.slotAxe.Set(m_itemAxe.id);
		GamePlay.instance.SetAxe(m_itemAxe.id);
		GamePlay.instance.ui.management.EquipAxe(m_itemAxe);
		m_parentPopup.Close();
	}
}
