﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonSelectGod : MonoBehaviour {

	protected int			m_index;
	protected UILabel		m_labelName;
	protected UILabel		m_labelInfo;

	protected UIButton		m_buttonDetail;
	protected UILabel		m_labelDetail;

	protected UIButton		m_buttonEquip;
	protected UILabel		m_labelEquip;

	protected UIPopup		m_parentPopup;
	protected ItemGod		m_itemGod;

	public virtual void Awake()
	{
		m_labelName = this.transform.Find("LabelName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelInfo = this.transform.Find("LabelInfo").gameObject.GetComponent<UILabel>() as UILabel;

		m_buttonDetail = this.transform.Find("ButtonDetail").gameObject.GetComponent<UIButton>() as UIButton;
		m_labelDetail = m_buttonDetail.transform.Find("Label").gameObject.GetComponent<UILabel>() as UILabel;

		m_buttonEquip = this.transform.Find("ButtonEquip").gameObject.GetComponent<UIButton>() as UIButton;
		m_labelEquip = m_buttonEquip.transform.Find("Label").gameObject.GetComponent<UILabel>() as UILabel;


		UIEventListener.Get (m_buttonDetail.gameObject).onClick += this.OnClickDetail;
		UIEventListener.Get (m_buttonEquip.gameObject).onClick += this.OnClickEquip;
		
	}

	public void OnDestroy()
	{
		UIEventListener.Get (m_buttonDetail.gameObject).onClick -= this.OnClickDetail;
		UIEventListener.Get (m_buttonEquip.gameObject).onClick -= this.OnClickEquip;
	}

	public void Init(int index, ItemGod itemGod, UIPopup parentPopup)
	{
		m_index = index;
		m_itemGod = itemGod;
		m_parentPopup = parentPopup;

		// icon
		GameObject go = PrefabManager.LoadNGUIPrefab(gameObject, "Prefaps/UI/IconGod");
		
		go.transform.localPosition = new Vector3(0, 39, 0);

		UIIconGod icon = go.GetComponent<UIIconGod>() as UIIconGod;
		PGTool.NGUIManager.SetDepths(icon.gameObject, 10);
		icon.SetGod(itemGod);
		icon.SetActiveColiider(false);

		// name
		Data.strings.SetTextFromKey(m_labelName, itemGod.data.name);
		// skill
		string strInfo = Data.strings.GetString(itemGod.data.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(itemGod.param1, "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(itemGod.param2, "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(itemGod.param3, "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", itemGod.data.activeTime.ToString());
		strInfo = strInfo.Replace("DT", itemGod.data.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
		Data.strings.SetText(m_labelInfo, strInfo);

		Data.strings.SetTextFromKey(m_labelDetail, "UI_DETAIL");
		Data.strings.SetTextFromKey(m_labelEquip, "UI_EQUIP");
	}

	protected virtual void OnClickDetail(GameObject go)
	{
		// TODO 도끼 디테일 페이지
	}
	protected virtual void OnClickEquip(GameObject go)
	{
		Data.user.slotGod.Set(m_index, m_itemGod.id);
		GamePlay.instance.SetGod(m_index, m_itemGod.id);
		GamePlay.instance.ui.management.EquipGod(m_index, m_itemGod);
		m_parentPopup.Close();
	}
}
