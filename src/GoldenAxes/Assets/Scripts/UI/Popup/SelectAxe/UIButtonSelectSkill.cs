﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonSelectSkill : UIButtonSelectAxe {
	
	private int m_index;

	public void Init(int index, ItemAxe itemAxe, UIPopup parentPopup)
	{
		m_index = index;
		m_itemAxe = itemAxe;
		m_parentPopup = parentPopup;

		// icon
		GameObject go = PrefabManager.LoadNGUIPrefab(gameObject, "Prefaps/UI/IconAxeSkill");
		go.transform.localPosition = new Vector3(0, 39, 0);

		UIIconAxeSkill icon = go.GetComponent<UIIconAxeSkill>() as UIIconAxeSkill;
		PGTool.NGUIManager.SetDepths(icon.gameObject, 10);
		icon.SetAxe(itemAxe);
		icon.SetActiveColiider(false);

		// name
		Data.strings.SetTextFromKey(m_labelName, itemAxe.data.skillSlot.name);
		
		DataAxeSlot dataSlot = m_itemAxe.data.skillSlot;
		string strInfo = Data.strings.GetString(dataSlot.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(dataSlot.GetParam1(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(dataSlot.GetParam2(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(dataSlot.GetParam3(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", dataSlot.activeTime.ToString());
		strInfo = strInfo.Replace("DT", dataSlot.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
		Data.strings.SetText(m_labelInfo, strInfo);

		Data.strings.SetTextFromKey(m_labelDetail, "UI_DETAIL");
		Data.strings.SetTextFromKey(m_labelEquip, "UI_EQUIP");
	}

	protected override void OnClickDetail(GameObject go)
	{
		// TODO 도끼 디테일 페이지
	}
	protected override void OnClickEquip(GameObject go)
	{
		Data.user.slotSkillAxe.Set(m_index, m_itemAxe.id);
		GamePlay.instance.SetAxeSkill(m_index, m_itemAxe.id);
		GamePlay.instance.ui.management.EquipSkill(m_index, m_itemAxe);
		m_parentPopup.Close();
	}
}
