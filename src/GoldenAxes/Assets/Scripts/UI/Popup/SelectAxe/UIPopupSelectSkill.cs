﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPopupSelectSkill : UIPopupSelectAxe {

	private int m_index;

	public void Init(int index)
	{
		m_index = index;
		InitButtons();
		Data.strings.SetTextFromKey(m_labelTitle, "UI_SELECT_SKILL");
	}
	protected override void InitButtons()
	{
		// 이건 팝업이니깐 풀에서 안쓰고 걍 지워버리자
		Vector3 pos = Vector3.zero;
		GameObject buttonGO;
		UIButtonSelectSkill button;
		int count = 0;
		foreach(KeyValuePair<int, ItemAxe> kv in Data.inventory.GetAxes())
		{
			pos.x = 154f * count;

			buttonGO = PrefabManager.LoadNGUIPrefab(m_itemScrollView.gameObject, "Prefaps/UI/Popup/ButtonSelectAxe");
			buttonGO.transform.parent = m_itemScrollView.gameObject.transform;
			buttonGO.transform.localScale = Vector3.one;
			buttonGO.transform.localPosition = pos;

			button = buttonGO.AddComponent<UIButtonSelectSkill>() as UIButtonSelectSkill;
			button.Init(m_index, kv.Value, this);

			UIDragScrollView dragScrollView = buttonGO.GetComponent<UIDragScrollView>() as UIDragScrollView;
			dragScrollView.scrollView = m_itemScrollView;

			count++;
		}
	}
}
