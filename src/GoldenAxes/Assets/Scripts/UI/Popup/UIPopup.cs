﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPopup : MonoBehaviour {

	public static UIPopup Create(string title, string subs)
	{
		/*
		GameObject go = UIRoot.Instantiate (Resources.Load("Prefaps/UI/Popup")) as GameObject;

		GameObject.Find("UIRoot").GetComponent().AddPrefab(go);
		*/

		GameObject go = NGUITools.AddChild(GameObject.Find ("UI Root"), Resources.Load ("Prefaps/UI/Popup") as GameObject);
		//go.transform.position = new Vector3(-1000,-1000,0);
		UIPopup ret = go.AddComponent<UIPopup> () as UIPopup;
		//ret.SetString (title, subs);


		return ret;
	}


	protected	UIPanel			m_panel = null;
	protected	UISprite		m_black = null;
	protected	GameObject		m_popup = null;
	protected	UISprite		m_popup_bg = null;
	protected	UISprite		m_popup_inner_bg = null;
	protected	UIButton		m_button_close = null;
	public		UILabel			m_labelTitle = null;
	public		UILabel			m_labelDesc = null;


    public virtual void Awake()
	{
		m_panel = GetComponent<UIPanel>() as UIPanel;

		if(m_panel == null)
		{
			m_panel = gameObject.AddComponent<UIPanel>() as UIPanel;
		}
		
		Transform find;

		m_black	= ((find = this.transform.Find("Black")) == null)? null : find.gameObject.GetComponent<UISprite>() as UISprite;
		m_popup	= ((find = this.transform.Find("Popup")) == null)? null : find.gameObject;
		if(m_popup != null)
		{
			m_popup_bg			= ((find = m_popup.transform.Find("BG")) == null)? null : find.gameObject.GetComponent<UISprite>() as UISprite;
			m_popup_inner_bg	= ((find = m_popup.transform.Find("InnerBG")) == null)? null : find.gameObject.GetComponent<UISprite>() as UISprite;
			m_button_close		= ((find = m_popup.transform.Find("ButtonClose")) == null)? null : find.gameObject.GetComponent<UIButton>() as UIButton;
			m_labelTitle		= ((find = m_popup.transform.Find("LabelTitle")) == null)? null : find.gameObject.GetComponent<UILabel>() as UILabel;
			m_labelDesc			= ((find = m_popup.transform.Find("LabelDesc")) == null)? null : find.gameObject.GetComponent<UILabel>() as UILabel;
		}

		if(m_black != null)
		{
			m_black.width = GamePlay.instance.ui.ui_width;
			m_black.height = GamePlay.instance.ui.ui_height;
			UIEventListener.Get (m_black.gameObject).onClick += this.OnClickClose;
		}

		if(m_button_close != null)
		{
			UIEventListener.Get (m_button_close.gameObject).onClick += this.OnClickClose;
		}

    }

    public virtual void OnDestroy()
    {
		if(m_black != null)
		{
			UIEventListener.Get (m_black.gameObject).onClick -= this.OnClickClose;
		}

		if(m_button_close != null)
		{
	        UIEventListener.Get (m_button_close.gameObject).onClick -= this.OnClickClose;
		}
	}

	public virtual void SetPanelDepth(int depth)
	{
		m_panel.depth = depth;
	}
	public virtual int GetLastestPanelCount()
	{
		return m_panel.depth;
	}
	public virtual int GetPanelCount()
	{
		return 1;
	}

	public virtual void Open(float time = 0.1f, float init_size = 0.9f)
	{
		m_popup.transform.localScale = Vector3.one * init_size;
		TweenScale.Begin(m_popup.gameObject, time, Vector3.one).method = UITweener.Method.EaseOut;
	}

	public virtual void Close()
	{
		GamePlay.instance.ui.ClosePopup(this);
	}

	public virtual void OnClickClose(GameObject go)
	{
		Close();
	}

	public virtual void Init()
	{

	}

/*
	public void SetString(string title, string subs)
	{
		labelTitle = GameObject.Find ("LabelTitle").GetComponent<UILabel>() as UILabel;
		labelSubs = GameObject.Find ("LabelSubs").GetComponent<UILabel>() as UILabel;

		labelTitle.text = title;
		labelSubs.text = subs;
	}
 */
/*
	public void OnClose()
	{
		GameObject.DestroyImmediate (this.gameObject);
	}
	 */
}
