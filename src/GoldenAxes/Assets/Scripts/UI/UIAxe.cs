﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAxe : MonoBehaviour {

	private int					m_left_gap		= 70;
	public UISlotAxe			slotThrow;

	public UISlotAxeSkill[]		slotAxeSkill;

	private	float				m_left_show_x;
	private	float				m_left_hide_x;
	private	float				m_right_show_x;
	private	float				m_right_hide_x;

	public void Init()
	{

	}

	public void InitAxeSlot(AxeSlot slot)
	{
		slotThrow.InitSlot(slot);
	}

	public void InitSkillSlot(int index, AxeSlot slot)
	{
		slotAxeSkill[index].InitSlot(slot);
	}

	public void SetScreenSize(int ui_width, int height, bool show = true, bool animation = true)
	{
		this.transform.localPosition = new Vector3(0.0f, height / 2, 0.0f);

		m_left_show_x = -ui_width/2 + m_left_gap;
		m_left_hide_x = m_left_show_x - 150f;
		m_right_show_x = ui_width/2 - m_left_gap;
		m_right_hide_x = m_right_show_x + 150f;
		
		if(show)
			Show(animation);
		else
			Hide(animation);
	}

	public void Show(bool animation = true)
	{
		if(animation)
		{
			TweenPosition.Begin(slotThrow.gameObject, Common.UI_INTRO_ANIMATION_TIME,
				new Vector3(m_left_show_x, slotThrow.transform.localPosition.y, slotThrow.transform.localPosition.z)).method = UITweener.Method.EaseInOut;

			for(int i = 0 ; i < slotAxeSkill.Length ; ++i)
			{
				TweenPosition.Begin(slotAxeSkill[i].gameObject, Common.UI_INTRO_ANIMATION_TIME,
					new Vector3(m_right_show_x, slotAxeSkill[i].transform.localPosition.y, slotAxeSkill[i].transform.localPosition.z)).method = UITweener.Method.EaseInOut;
			}
		}
		else
		{
			slotThrow.transform.localPosition = new Vector3(m_left_show_x, slotThrow.transform.localPosition.y, slotThrow.transform.localPosition.z);
			for(int i = 0 ; i < slotAxeSkill.Length ; ++i)
			{
				slotAxeSkill[i].transform.localPosition = new Vector3(m_right_show_x, slotAxeSkill[i].transform.localPosition.y, slotAxeSkill[i].transform.localPosition.z);
			}
		}
	}

	public void Hide(bool animation = true)
	{
		if(animation)
		{
			TweenPosition.Begin(slotThrow.gameObject, Common.UI_INTRO_ANIMATION_TIME,
				new Vector3(m_left_hide_x, slotThrow.transform.localPosition.y, slotThrow.transform.localPosition.z)).method = UITweener.Method.EaseInOut;

			for(int i = 0 ; i < slotAxeSkill.Length ; ++i)
			{
				TweenPosition.Begin(slotAxeSkill[i].gameObject, Common.UI_INTRO_ANIMATION_TIME,
					new Vector3(m_right_hide_x, slotAxeSkill[i].transform.localPosition.y, slotAxeSkill[i].transform.localPosition.z)).method = UITweener.Method.EaseInOut;
			}
		}
		else
		{
			slotThrow.transform.localPosition = new Vector3(m_left_hide_x, slotThrow.transform.localPosition.y, slotThrow.transform.localPosition.z);
			for(int i = 0 ; i < slotAxeSkill.Length ; ++i)
			{
				slotAxeSkill[i].transform.localPosition = new Vector3(m_right_hide_x, slotAxeSkill[i].transform.localPosition.y, slotAxeSkill[i].transform.localPosition.z);
			}
		}
	}


}
