﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UISlotAxe : MonoBehaviour {
	
	protected	AxeSlot		m_slot;

	public		UISprite	bg;
	public		UISprite	icon = null;

	public virtual void Awake()
	{
	}

	public virtual void OnDestory()
	{
	}

	public virtual void InitSlot(AxeSlot slot)
	{
		gameObject.SetActive(false);
		m_slot = slot;

		if (m_slot == null) {
			// SYS-ERROR wrong slot
		}

		m_slot.onSetAxe = this.OnSetAxe;
		m_slot.onResetAxe = this.OnResetAxe;

		m_slot.onReady = this.OnReady;

		m_slot.onCool = this.OnCool;
		m_slot.onCooling = this.OnCooling;

		m_slot.onActivate = this.OnActivate;
		m_slot.onActivating = this.OnActivating;
	}

	public virtual void OnSetAxe (ItemAxe itemAxe)
	{
		gameObject.SetActive(true);

		bg.spriteName = "axe_slot_bg" + itemAxe.data.grade.ToString();
		icon.spriteName = itemAxe.data.throwSlot.icon;
	}

	public virtual void OnResetAxe ()
	{
		m_slot = null;
	}

	public virtual void OnReady ()
	{
	}

	public virtual void OnActivate (float time)
	{
	}
	
	public virtual void OnActivating (float time)
	{
	}

	public virtual void OnCool (float time)
	{
	}

	public virtual void OnCooling (float time)
	{
	}
}
