﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISlotAxeSkill : UISlotAxe {

	public		UISprite	bgSmall = null;
	public		UISprite	iconSmall = null;
	public		GameObject	iconTouch = null;

	public		UISprite	bgProgress = null;

	public		UISprite	coolProgress = null;
	public		UISprite	activeProgress = null;


	public		UILabel		timeLabel = null;

	public override void Awake()
	{
		if (activeProgress) {
			activeProgress.alpha = 0.0f;
			TweenAlpha tween = TweenAlpha.Begin (activeProgress.gameObject, 0.5f, 1.0f);
			tween.method = UITweener.Method.EaseInOut;
			tween.style = UITweener.Style.PingPong;
		}

	}

	public override void OnDestory()
	{
	}

	public override void InitSlot(AxeSlot slot)
	{
		gameObject.SetActive(false);
		base.InitSlot (slot);

		m_slot.onWaitControl = this.OnWaitControl;

		// init ui status
		switch (m_slot.status) {
		case AxeSlot.STATUS.READY:
			if (timeLabel) {
				timeLabel.color = new Color32 (255, 255, 0, 255);
				timeLabel.text = "";
			}
			if (icon) {
				icon.color = new Color (1.0f, 1.0f, 1.0f, 1.0f);
			}
			break;

		case AxeSlot.STATUS.COOL:
			if (timeLabel) {
				timeLabel.color = new Color32 (255, 255, 255, 255);
			}
			if (icon) {
				icon.color = new Color (0.3f, 0.3f, 0.3f, 1.0f);
			}
			break;

		// 있을까 싶긴 하지만서도 (TO-DO 나중에 이전 로직에서 있는지 확인해보자)
		case AxeSlot.STATUS.ACTIVATE:
			if (timeLabel) {
				timeLabel.color = new Color32 (255, 255, 0, 255);
				timeLabel.text = "";
			}
			if (icon) {
				icon.color = new Color (1.0f, 1.0f, 1.0f, 1.0f);
			}
			break;
		}

		if (iconTouch) {
			iconTouch.SetActive (false);
		}
	}

	public override void OnSetAxe (ItemAxe itemAxe)
	{
		gameObject.SetActive(true);
		icon.spriteName = m_slot.dataSlot.icon;
		iconSmall.spriteName = itemAxe.data.throwSlot.iconSmall;

		bg.spriteName = "axe_slot_bg" + itemAxe.data.grade.ToString();
		bgSmall.spriteName = "axe_slot_small_bg" + itemAxe.data.grade.ToString();
		bgProgress.spriteName = "axe_slot_abillity_progress_bg" + itemAxe.data.grade.ToString();
		coolProgress.spriteName = "axe_slot_abillity_progress" + itemAxe.data.grade.ToString();
		activeProgress.spriteName = "axe_slot_abillity_progress_active" + itemAxe.data.grade.ToString();
		
	}
	public override void OnResetAxe ()
	{
		base.OnResetAxe();
	}

	public override void OnReady ()
	{
		if (coolProgress) {
			coolProgress.fillAmount = 1.0f;
		}
		if (activeProgress) {
			activeProgress.fillAmount = 1.0f;
		}

		if (timeLabel) {
			timeLabel.color = new Color32 (255, 255, 0, 255);
			timeLabel.text = "";
		}

		if (icon) {
			TweenColor.Begin (icon.gameObject, 0.2f, new Color (1.0f, 1.0f, 1.0f, 1.0f)).method = UITweener.Method.EaseIn;
		}

		if (iconTouch) {
			iconTouch.SetActive (false);
		}
	}

	public virtual void OnWaitControl()
	{
		if (iconTouch) {
			iconTouch.SetActive (true);
		}
	}

	public override void OnActivate (float time)
	{
		OnActivating (time);

		if (iconTouch) {
			iconTouch.SetActive (false);
		}
	}

	public override void OnActivating (float time)
	{
		if (coolProgress) {
			coolProgress.fillAmount = time / m_slot.activeTime;
		}
		if (activeProgress) {
			activeProgress.fillAmount = time / m_slot.activeTime;
		}

		if (timeLabel) {
			timeLabel.text = Mathf.CeilToInt (time).ToString ();
		}
	}


	public override void OnCool (float time)
	{
		if (timeLabel) {
			timeLabel.color = new Color32 (255, 255, 255, 255);
		}

		OnCooling (time);

		if (icon) {
			TweenColor.Begin (icon.gameObject, 0.2f, new Color (0.3f, 0.3f, 0.3f, 1.0f)).method = UITweener.Method.EaseOut;
		}
	}

	public override void OnCooling (float time)
	{
		if (coolProgress)
			coolProgress.fillAmount = 1.0f - time / m_slot.coolTime;

		if (activeProgress) {
			activeProgress.fillAmount = 0.0f;
		}

		if (timeLabel) {
			timeLabel.text = Mathf.CeilToInt (time).ToString ();
		}
	}

	public void OnClick()
	{
		m_slot.Active();
	}
}
