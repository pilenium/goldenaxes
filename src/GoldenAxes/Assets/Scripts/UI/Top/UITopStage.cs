﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITopStage : MonoBehaviour {

	public UILabel 		labelStage;
	public UISprite		iconChapter;

	public GameObject	stageIcons;
	public UISprite[]	iconStages;

	void Awake()
	{
		stageIcons.SetActive(false);
	}

	public void OnClick()
	{
		stageIcons.SetActive(!stageIcons.activeSelf);
	}

	public void SetStage(int stage)
	{
		labelStage.text = stage.ToString();

		float startx = -48f;
		int current = (stage-1) % DataChapters.STAGE_IN_CHAPTER;
		float current_width;

		for(int i = 0 ; i < iconStages.Length ; ++i)
		{
			Vector3 pos = iconStages[i].transform.localPosition;
			if(i < current)
			{
				current_width = 10f;
				iconStages[i].spriteName = "top_stage_icon_clear";
			}
			else if(i == current)
			{
				current_width = 14f;
				iconStages[i].spriteName = "top_stage_icon_progress";
			}
			else 
			{
				current_width = 10f;
				iconStages[i].spriteName = "top_stage_icon_empty";
			}

			pos.x = startx + (current_width - 10)/2;
			iconStages[i].transform.localPosition = pos;
			iconStages[i].width = Mathf.FloorToInt(current_width);
			iconStages[i].height = Mathf.FloorToInt(current_width);
			startx += current_width;
		}
	}

}
