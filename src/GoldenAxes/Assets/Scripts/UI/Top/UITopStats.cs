﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITopStats : MonoBehaviour {

	public	UILabel				labelWDMDamage;
	public	UILabel				labelGodDamage;

	public void OnClick()
	{

	}

	public void CheckDamages()
	{
		labelWDMDamage.text = PGTool.StringManager.To3GANumber(GamePlay.instance.properties.throwDMG);
		//labelGodDamage
		GodSlot slot;
		double dmg = 0;
		for(int i = 0 ; i < Common.MAX_GOD_SLOT ; ++i)
		{
			slot = GamePlay.instance.GetGodSlot(i);
			if(slot != null) dmg += slot.dmg;
		}
		labelGodDamage.text = PGTool.StringManager.To3GANumber(dmg);
	}

}
