﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITopCoin : MonoBehaviour {

	public UILabel		labelCoin;

	// Use this for initialization
	void Awake() {
		
	}

	public void CheckCoin()
	{
		labelCoin.text = PGTool.StringManager.ToGANumber(Data.user.coin.Get(), "0.00", "0");
	}
}
