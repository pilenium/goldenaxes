﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITopProgress : MonoBehaviour {
	public	UISprite			progressBallLeft;
	public	UISprite			progressBallRight;
	public	UISprite			progressBallBoss;

	public	UISlider			progress;

	private	float				ball_boss_x;
	private	float				ball_boss_width;

	void Awake()
	{
		ball_boss_x = progressBallLeft.transform.position.x;
		ball_boss_width = progressBallRight.transform.position.x - progressBallLeft.transform.position.x;
	}
	
	public void SetProgressNormal()
	{
		progressBallBoss.gameObject.SetActive(false);

		TweenColor.Begin(progressBallLeft.gameObject, 0.3f, new Color(1.0f, 1.0f, 1.0f, 1.0f));
		TweenColor.Begin(progressBallRight.gameObject, 0.3f, new Color(1.0f, 1.0f, 1.0f, 1.0f));
		TweenColor.Begin(progress.foregroundWidget.gameObject, 0.3f, new Color(1.0f, 1.0f, 1.0f, 1.0f));
	}
	public void SetProgressBoss()
	{
		progressBallBoss.gameObject.SetActive(true);

		TweenColor.Begin(progressBallLeft.gameObject, 0.3f, new Color(1.0f, 0.0f, 0.0f, 1.0f));
		TweenColor.Begin(progressBallRight.gameObject, 0.3f, new Color(1.0f, 0.0f, 0.0f, 1.0f));
		TweenColor.Begin(progress.foregroundWidget.gameObject, 0.3f, new Color(1.0f, 0.0f, 0.0f, 1.0f));
		
	}
	public void SetProgress(float value)
	{
		progress.value = value;

		if(value == 0.0f)
		{
			progressBallLeft.enabled = false;
			progressBallRight.enabled = false;
		}
		else if(value >= 1.0f)
		{
			progressBallLeft.enabled = true;
			progressBallRight.enabled = true;
		}
		else
		{
			progressBallLeft.enabled = true;
			progressBallRight.enabled = false;
		}

		if(progressBallBoss.enabled)
		{
			Vector3 pos = progressBallBoss.transform.position;
			pos.x = ball_boss_x + ball_boss_width * value;
			progressBallBoss.transform.position = pos;
		}
	}
}
