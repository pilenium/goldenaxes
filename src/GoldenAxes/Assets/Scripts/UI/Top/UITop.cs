﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITop : MonoBehaviour {

	public	UITopStage			stageUI;
	public	UITopCoin			coinUI;
	public	UITopProgress		progressUI;


	public float				m_menu_height		{ private set; get; }
	private float				m_ui_show_y;
	private float				m_ui_hide_y;

	void Awake()
	{
		m_menu_height = 90f;
		//SetProgressNormal();
		SetProgress(0.0f);
	}
	public void SetScreenSize(int ui_width, int height, bool show = true, bool animation = true)
	{
		m_ui_show_y = height / 2;
		m_ui_hide_y = m_ui_show_y + 100f;
		
		if(show)
			Show(animation);
		else
			Hide(animation);
	}

	public void Show(bool animation = true)
	{
		if(animation)
		{
			TweenPosition.Begin(gameObject, Common.UI_INTRO_ANIMATION_TIME,  new Vector3(0f, m_ui_show_y, 0f)).method = UITweener.Method.EaseInOut;
		}
		else
		{
			gameObject.transform.localPosition = new Vector3(0f, m_ui_show_y, 0f);
		}
	}

	public void Hide(bool animation = true)
	{
		if(animation)
		{
			TweenPosition.Begin(gameObject, Common.UI_INTRO_ANIMATION_TIME,  new Vector3(0f, m_ui_hide_y, 0f)).method = UITweener.Method.EaseInOut;
		}
		else
		{
			gameObject.transform.localPosition = new Vector3(0f, m_ui_hide_y, 0f);
		}
	}

	public void CheckCoin()
	{
		coinUI.CheckCoin();
	}

	public void CheckDamages()
	{
	}

	public void SetStage(int stage)
	{
		stageUI.SetStage(stage);
	}

	public void SetProgressNormal()
	{
		progressUI.SetProgressNormal();
	}
	public void SetProgressBoss()
	{
		progressUI.SetProgressBoss();
	}
	public void SetProgress(float value)
	{
		progressUI.SetProgress(value);
	}
}
