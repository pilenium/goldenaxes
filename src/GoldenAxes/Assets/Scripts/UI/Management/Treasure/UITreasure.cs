﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITreasure : UIManagePage {

	private Vector3		m_bt_pos	= new Vector3(40.0f, -333.0f, 0.0f);
	private Vector3		m_bt_gap	= new Vector3(370.0f, -150.0f, 0.0f);

	private List<UIButtonTreasure>		m_buttonTreasures = null;

	public UIButtonPrestige				buttonPrestige;
	public UIButtonDiscover				buttonbuttonDiscover;

	public UILabel			labelTreasures;
	public UILabel			labelTreasureCount;
	public UILabel			labelMyTreasures;
	private UIPanel			m_panel = null;
	private UIScrollView	m_scrollView = null;
	private UIBuyCount		m_buyCount = null;

    public void Awake()
	{
//		UIEventListener.Get(buttonAccount.gameObject).onClick += this.OnClickAccount;
//		UIEventListener.Get(buttonMSG.gameObject).onClick += this.OnClickMSG;
//		UIEventListener.Get(buttonSkillTree.gameObject).onClick += this.OnClickSkillTree;
    }

    public void OnDestroy()
    {
//        UIEventListener.Get (buttonAccount.gameObject).onClick -= this.OnClickAccount;
//        UIEventListener.Get (buttonMSG.gameObject).onClick -= this.OnClickMSG;
//        UIEventListener.Get (buttonSkillTree.gameObject).onClick -= this.OnClickSkillTree;
	}

	public override void Init()
	{
		Vector3 pos = m_bt_pos;

		if(m_panel == null)
		{
			m_panel = GetComponent<UIPanel>() as UIPanel;
			float height = GamePlay.instance.ui.ui_height / 2 - 70;
			float y = -height/2;
			m_panel.baseClipRegion = new Vector4(224, y, 748, height);
		}

		if(m_scrollView == null)
		{
			m_scrollView = GetComponent<UIScrollView>() as UIScrollView;
		}

		if(m_buyCount == null)
		{
			m_buyCount = GetComponentInChildren<UIBuyCount>() as UIBuyCount;
		}

		if(m_buttonTreasures == null)
		{
			m_buttonTreasures = new List<UIButtonTreasure>();

			GameObject obj;
			UIDragScrollView dragScrollView;
			UIButtonTreasure button;

			int count = 0;
			Vector3 newPos = m_bt_pos;
			
			foreach(KeyValuePair<int, ItemTreasure> kv in Data.inventory.GetTreasures()) 
			{
				obj = PrefabManager.LoadPrefab("Prefaps/UI/ButtonTreasure");
				obj.transform.parent = this.transform;
				obj.transform.localScale = Vector3.one;

				newPos.x = m_bt_pos.x + m_bt_gap.x * (count%2);
				newPos.y = m_bt_pos.y + m_bt_gap.y * Mathf.FloorToInt(count/2);
				newPos.z = m_bt_pos.z + m_bt_gap.z * Mathf.FloorToInt(count/2);
				obj.transform.localPosition = newPos;

				button = obj.AddComponent<UIButtonTreasure>();
				button.Init(kv.Value);

				dragScrollView = obj.GetComponent<UIDragScrollView>() as UIDragScrollView;
				dragScrollView.scrollView = m_scrollView;
				m_buttonTreasures.Add(button);

				count++;
			}
		}

		m_buyCount.CheckCount();
		m_buyCount.Close(false);

		SetStrings();
		RefreshAll();
	}
	public override void Clear()
	{
	}

	public override void RefreshAll()
	{
		CheckProperties();
		CheckPrice();
	}

	public void SetStrings()
	{
        Data.strings.SetTextFromKey(labelTreasures, "UI_TREASURES_DISCOVERED");
        Data.strings.SetTextFromKey(labelMyTreasures, "UI_MY_TREASURES");

		buttonPrestige.SetStrings();
		buttonbuttonDiscover.SetStrings();
	}
	
	public override void CheckProperties()
	{
		labelTreasureCount.text = Data.inventory.GetTreasures().Count + "/" + Data.treasure.GetTotalCount();

		foreach(UIButtonTreasure button in m_buttonTreasures)
		{
			button.CheckProperties();
		}
	}

	public override void CheckPrice()
	{
		foreach(UIButtonTreasure button in m_buttonTreasures)
		{
			button.CheckPrice();
		}
	}


}
