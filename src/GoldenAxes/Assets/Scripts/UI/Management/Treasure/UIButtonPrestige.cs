﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonPrestige : MonoBehaviour {

	private UILabel		m_labelName;
	private UILabel		m_labelInfo;

	private UIButton	m_button;
	private UILabel		m_labelButton;
	private UILabel		m_labelPrice;

	public void Awake()
	{
		m_labelName = this.transform.Find("LabelName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelInfo = this.transform.Find("LabelInfo").gameObject.GetComponent<UILabel>() as UILabel;

		m_button = this.transform.Find("Button").gameObject.GetComponent<UIButton>() as UIButton;
		m_labelButton = m_button.transform.Find("Label").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelPrice = m_button.transform.Find("LabelPrice").gameObject.GetComponent<UILabel>() as UILabel;

		UIEventListener.Get (m_button.gameObject).onClick += this.OnClickButton;
	}
	public void OnDestroy()
	{
		UIEventListener.Get (m_button.gameObject).onClick -= this.OnClickButton;
	}

	public void SetStrings()
    {
        Data.strings.SetTextFromKey(m_labelName, "UI_PRESTIGE");
        Data.strings.SetTextFromKey(m_labelInfo, "UI_PRESTIGE_INFO");
        Data.strings.SetTextFromKey(m_labelButton, "UI_PRESTIGE_BTN");
    }

	protected virtual void OnClickButton(GameObject go)
	{
	}
}
