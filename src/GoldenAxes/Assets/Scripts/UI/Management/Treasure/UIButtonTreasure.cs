﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonTreasure : MonoBehaviour {

    private UISprite	m_icon;

	private UILabel		m_labelName;
	private UILabel		m_labelInfo;
	private UILabel		m_labelGrade;

	private UIButton	m_buttonUpgrade;
	private UISprite	m_buttonBG;

	private	UILabel		m_labelUpgrade;
	private	UILabel		m_labelPrice;
	private	UILabel		m_labelUpgradeCount;

	private ItemTreasure	m_itemTreasure;
	public	ItemTreasure	itemTreasure			{ get { return m_itemTreasure; } }


    protected int			m_buyCount = 0;

	public void Awake()
	{
		m_icon = this.transform.Find("Icon").gameObject.GetComponent<UISprite>() as UISprite;
		m_labelName = this.transform.Find("LabelName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelInfo = this.transform.Find("LabelInfo").gameObject.GetComponent<UILabel>() as UILabel;

		m_buttonUpgrade = this.transform.Find("Button").gameObject.GetComponent<UIButton>() as UIButton;
		UIEventListener.Get (m_buttonUpgrade.gameObject).onClick += this.OnClickUpgrade;

		m_buttonBG = m_buttonUpgrade.transform.Find("Background").gameObject.GetComponent<UISprite>() as UISprite;

		m_labelUpgrade = m_buttonUpgrade.transform.Find("Label").gameObject.GetComponent<UILabel>() as UILabel;
		Data.strings.SetTextFromKey(m_labelUpgrade, "UI_TREASURE_UPGRADE");

		m_labelPrice = m_buttonUpgrade.transform.Find("LabelPrice").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelUpgradeCount = m_buttonUpgrade.transform.Find("LabelUpgradeCount").gameObject.GetComponent<UILabel>() as UILabel;
		
	}

	public void OnDestroy()
	{
		UIEventListener.Get (m_buttonUpgrade.gameObject).onClick -= this.OnClickUpgrade;
	}

	public void Init(ItemTreasure itemTreasure)
	{
		m_itemTreasure = itemTreasure;

		m_icon.spriteName = itemTreasure.data.icon;

		// name
        Data.strings.SetTextFromKey(m_labelName, itemTreasure.data.name);

		string strInfo = Data.strings.GetString(itemTreasure.data.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(itemTreasure.param1, "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(itemTreasure.param2, "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(itemTreasure.param3, "0.0", "0.000"));
        strInfo = strInfo.Replace("\n", " ");
        Data.strings.SetText(m_labelInfo, strInfo);
	}

	public void CheckProperties()
	{
	}

	public void CheckPrice()
	{
        double price;
        double dmg;
	
		m_buyCount = Data.user.buyCount.Get();
		
		// 구매 제한 있을경우
		if(m_buyCount > 0)
		{
			price = GamePlay.instance.properties.GetTreasureUpgradePrice(m_itemTreasure.level, m_buyCount);

			// 돈 맞음
			if(price <= Data.server.relic)
			{
				m_buttonBG.spriteName = "button_bg_c";
				m_labelPrice.text = PGTool.StringManager.ToGANumber(price, "0.00", "0");
				m_labelUpgradeCount.text = "+" + m_buyCount.ToString();
				return;
			}
		}


		// 구매 제한 수정해서 사자
		m_buyCount = GamePlay.instance.properties.GetTreasureUpgradeCount(m_itemTreasure.level, Data.server.relic);
		if(m_buyCount > 0)
		{
			price = GamePlay.instance.properties.GetTreasureUpgradePrice(m_itemTreasure.level, m_buyCount);

			m_buttonBG.spriteName = "button_bg_c";
			m_labelPrice.text = PGTool.StringManager.ToGANumber(price, "0.00", "0");
			m_labelUpgradeCount.text = "+" + m_buyCount.ToString();
			return;
		}
		
		// 못삼
		m_buyCount = 1;

		price = GamePlay.instance.properties.GetTreasureUpgradePrice(m_itemTreasure.level, m_buyCount);

		m_buttonBG.spriteName = "button_bg_c";
		m_labelPrice.text = PGTool.StringManager.ToGANumber(price, "0.00", "0");
		m_labelUpgradeCount.text = "+" + m_buyCount.ToString();

		return;
	}

	protected virtual void OnClickUpgrade(GameObject go)
	{
		// TODO 도끼 디테일 페이지
//		GamePlay.instance.SetShip(dataShip.id);
        //GamePlay.instance.ui.CheckProperties();
	}
}
