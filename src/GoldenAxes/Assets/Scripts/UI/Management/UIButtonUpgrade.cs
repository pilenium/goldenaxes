﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonUpgrade : UIButton {

	protected UISprite	bg;
	protected UILabel	label1;
	protected UILabel	label1Value;
	protected UILabel	label2;
	protected UILabel	label2Value;

	protected UILabel	labelPrice;
	

	private	string	str_bg_enable;
	private string	str_bg_disable;

	public void Awake()
	{
		bg = this.transform.Find("Background").gameObject.GetComponent<UISprite>() as UISprite;
		label1 = this.transform.Find("label1").gameObject.GetComponent<UILabel>() as UILabel;
		label1Value = this.transform.Find("label1Value").gameObject.GetComponent<UILabel>() as UILabel;
		label2 = this.transform.Find("label2").gameObject.GetComponent<UILabel>() as UILabel;
		label2Value = this.transform.Find("label2Value").gameObject.GetComponent<UILabel>() as UILabel;
		labelPrice = this.transform.Find("LabelPrice").gameObject.GetComponent<UILabel>() as UILabel;
	}

	public void SetEnable(string sprite = "button_bg_r")
	{
		bg.spriteName = sprite;
		this.isEnabled = true;
	}

	public void SetDisable()
	{
		bg.spriteName = "button_bg_gray";
		this.isEnabled = false;
	}

	public void SetPrice(double price)
	{
		labelPrice.text = PGTool.StringManager.ToGANumber(price, "0.00", "0");
	}

	public void SetStrings(string str1, string value1, string str2, string value2)
	{
		label1.gameObject.SetActive(true);
		label1.alignment = NGUIText.Alignment.Left;
		Data.strings.SetText(label1, str1);

		label1Value.gameObject.SetActive(true);
		Data.strings.SetText(label1Value, value1);

		label2.gameObject.SetActive(true);
		label2.alignment = NGUIText.Alignment.Left;
		Data.strings.SetText(label2, str2);

		label2Value.gameObject.SetActive(true);
		Data.strings.SetText(label2Value, value2);
	}

	public void SetStrings(string str1, string str2, string value2)
	{
		label1.gameObject.SetActive(true);
		label1.alignment = NGUIText.Alignment.Center;
		Data.strings.SetText(label1, str1);
		
		label1Value.gameObject.SetActive(false);

		label2.gameObject.SetActive(true);
		label2.alignment = NGUIText.Alignment.Left;
		Data.strings.SetText(label2, str2);

		label2Value.gameObject.SetActive(true);
		Data.strings.SetText(label2Value, value2);
	}

	public void SetStrings(string str1, string str2)
	{
		label1.gameObject.SetActive(true);
		label1.alignment = NGUIText.Alignment.Center;
		Data.strings.SetText(label1, str1);

		label1Value.gameObject.SetActive(false);

		label2.gameObject.SetActive(true);
		label2.alignment = NGUIText.Alignment.Center;
		Data.strings.SetText(label2, str2);

		label2Value.gameObject.SetActive(false);
	}

}
