﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGodSlot : UIManagePage {
	private Vector3		m_bt_pos	= new Vector3(225.0f, -143.0f, 0.0f);
	private Vector3		m_bt_gap	= new Vector3(0.0f, -140.0f, 0.0f);
	private List<UIButtonGodSlot>	m_buttons = null;


	public UIButton			buttonStats;
	public UIButton			buttonMission;
	public UIButton			buttonAchievement;

	public UILabel			labelDamage;
	public UILabel			throwDamage;
	public UILabel			labelStats;
	public UILabel			labelMission;
	public UILabel			labelAchievement;
	private UIPanel			m_panel = null;
	private UIScrollView	m_scrollView = null;
	private UIBuyCount		m_buyCount = null;

    public void Awake()
	{
		UIEventListener.Get(buttonStats.gameObject).onClick += this.OnClickStats;
		UIEventListener.Get(buttonMission.gameObject).onClick += this.OnClickMission;
		UIEventListener.Get(buttonAchievement.gameObject).onClick += this.OnClickAchievement;
    }

    public void OnDestroy()
    {
        UIEventListener.Get(buttonStats.gameObject).onClick -= this.OnClickStats;
        UIEventListener.Get(buttonMission.gameObject).onClick -= this.OnClickMission;
        UIEventListener.Get(buttonAchievement.gameObject).onClick -= this.OnClickAchievement;
	}


	public override void Init()
	{
		if(m_panel == null)
		{
			m_panel = GetComponent<UIPanel>() as UIPanel;
			float height = GamePlay.instance.ui.ui_height / 2 - 70;
			float y = -height/2;
			m_panel.baseClipRegion = new Vector4(224, y, 748, height);
		}

		if(m_scrollView == null)
		{
			m_scrollView = GetComponent<UIScrollView>() as UIScrollView;
		}

		if(m_buyCount == null)
		{
			m_buyCount = GetComponentInChildren<UIBuyCount>() as UIBuyCount;
		}

		if(m_buttons == null)
		{
			m_buttons = new List<UIButtonGodSlot>();


			GameObject obj;
			UIButtonGodSlot button;
			UIDragScrollView dragScrollView;
			
			for(int i = 0 ; i < GamePlay.instance.stage.dataChapter.size.god_count ; ++i)
			{
				obj = PrefabManager.LoadPrefab("Prefaps/UI/ButtonGodSlot");
				obj.transform.parent = this.transform;
				obj.transform.localScale = Vector3.one;
				obj.transform.localPosition = m_bt_pos;

				button = obj.AddComponent<UIButtonGodSlot>();
				button.Init(i);

				dragScrollView = obj.GetComponent<UIDragScrollView>() as UIDragScrollView;
				dragScrollView.scrollView = m_scrollView;
				m_buttons.Add(button);

				m_bt_pos += m_bt_gap;
				//ButtonWoodman

			}
		}

		m_buyCount.CheckCount();
		m_buyCount.Close(false);

		throwDamage.text = PGTool.StringManager.ToGANumber(GamePlay.instance.properties.throwDMG);

		SetStrings();
		RefreshAll();
	}
	

	public override void RefreshAll()
	{
		CheckDamages();

		foreach(UIButtonGodSlot button in m_buttons)
		{
			button.RefreshAll();
		}
	}
	public void SetStrings()
	{
        Data.strings.SetTextFromKey(labelDamage, "UI_GOD_DMG");
        Data.strings.SetTextFromKey(labelStats, "UI_STATS");
        Data.strings.SetTextFromKey(labelMission, "UI_MISSION");
        Data.strings.SetTextFromKey(labelAchievement, "UI_ACHIEVEMENT");
	}

	
	public void CheckDamages()
	{
		GodSlot slot;
		double dmg = 0;
		for(int i = 0 ; i < Common.MAX_GOD_SLOT ; ++i)
		{
			slot = GamePlay.instance.GetGodSlot(i);
			if(slot != null) dmg += slot.dmg;
		}
		throwDamage.text = PGTool.StringManager.ToGANumber(dmg);
	}

	public override void CheckProperties()
	{
		CheckDamages();

		foreach(UIButtonGodSlot button in m_buttons)
		{
			button.CheckProperties();
		}
	}

	public override void CheckPrice()
	{
		foreach(UIButtonGodSlot button in m_buttons)
		{
			button.CheckPrice();
		}
	}

	public void EquipGod(int index, ItemGod itemGod)
	{
		foreach(UIButtonGodSlot button in m_buttons)
		{
			if(button.index == index)
			{
				button.EquipGod(itemGod);
			}
		}
	}
	public void OnClickStats(GameObject go)
	{

	}
    public void OnClickMission(GameObject go)
	{
		
	}
    public void OnClickAchievement(GameObject go)
	{
		
	}
}
