﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonGodSlot : MonoBehaviour {

    protected UILabel			m_labelName;
    protected UILabel			m_labelLevel;
    protected UILabel			m_labelLevelValue;
    protected UILabel			m_labelDamage;
    protected UILabel			m_labelDamageValue;

    protected UILabel			m_labelAxeName;
    protected UILabel			m_labelAxeInfo;

    protected UIButtonUpgrade	m_buttonUpgrade;
    protected UIIconGod			m_iconGod;
	
	protected int				m_index;
    public    int               index { get { return m_index; } }
	protected int				m_level;
    protected int               m_buyCount = 0;
	protected GodSlot			m_slot;

    public virtual void Awake()
    {
		InitComponents();
        InitIcon();

		UIEventListener.Get (m_buttonUpgrade.gameObject).onClick += this.OnClickUpgrade;
		UIEventListener.Get (m_iconGod.gameObject).onClick += this.OnClickGod;
    }

    public void OnDestroy()
    {
        UIEventListener.Get (m_buttonUpgrade.gameObject).onClick -= this.OnClickUpgrade;
        m_buttonUpgrade = null;

        UIEventListener.Get (m_iconGod.gameObject).onClick -= this.OnClickGod;
        m_iconGod = null;
    }

	protected virtual void InitComponents()
	{
		m_labelName = this.transform.Find("LabelName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelLevel = this.transform.Find("LabelLevel").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelLevelValue = this.transform.Find("LabelLevelValue").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelDamage = this.transform.Find("LabelDamage").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelDamageValue = this.transform.Find("LabelDamageValue").gameObject.GetComponent<UILabel>() as UILabel;

		m_labelAxeName = this.transform.Find("LabelAxeName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelAxeInfo = this.transform.Find("LabelAxeInfo").gameObject.GetComponent<UILabel>() as UILabel;

        GameObject go = PrefabManager.LoadNGUIPrefab(gameObject, "Prefaps/UI/ButtonUpgrade");
        go.transform.localPosition = new Vector3(265, -2, 0);
		m_buttonUpgrade = go.GetComponent<UIButtonUpgrade>() as UIButtonUpgrade;
	}

    protected virtual void InitIcon()
    {
        GameObject obj = PrefabManager.LoadPrefab("Prefaps/UI/IconGod");

        m_iconGod = obj.GetComponent<UIIconGod>() as UIIconGod;
        PGTool.NGUIManager.SetDepths(m_iconGod.gameObject, 303);
        m_iconGod.transform.parent = this.transform;
        m_iconGod.transform.localScale = Vector3.one;
        m_iconGod.transform.localPosition = new Vector3(-284, 0, 0);
    }

	public void Init(int index)
	{
		m_index = index;
		m_level = Data.user.slotGodLevel.Get(m_index);
		m_slot = GamePlay.instance.GetGodSlot(m_index);
	}

    public virtual void RefreshAll()
    {
        SetStrings();

		CheckProperties();
		CheckPrice();
    }
    public virtual void SetGod(ItemGod itemGod)
    {
        // 슬롯 레벨이 0이면 못장착
        if(m_level == 0)
        {
            // 장착 해주지 말자
            RemoveAxe();
            return;
        }

        m_iconGod.SetGod(itemGod);

		// god name
        Data.strings.SetTextFromKey(m_labelName, itemGod.data.name);
        // axe name
        Data.strings.SetTextFromKey(m_labelAxeName, itemGod.data.skill_name);
        
        // skill Info
		string strInfo = Data.strings.GetString(itemGod.data.skill_info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(itemGod.param1, "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(itemGod.param2, "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(itemGod.param3, "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", itemGod.data.activeTime.ToString());
		strInfo = strInfo.Replace("DT", itemGod.data.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
        Data.strings.SetText(m_labelAxeInfo, strInfo);
    }
    public virtual void RemoveAxe()
    {
        m_iconGod.RemoveAxe();

        Data.strings.SetTextFromKey(m_labelName, "UI_GOD_SLOT");

        if(m_level == 0)
        {
            Data.strings.SetTextFromKey(m_labelAxeName, "UI_LOCKED");
            Data.strings.SetTextFromKey(m_labelAxeInfo, "UI_TOUCH_UNLCKBTN");
        }
        else
        {
            Data.strings.SetTextFromKey(m_labelAxeName, "UI_EQUIP_GOD");
            Data.strings.SetTextFromKey(m_labelAxeInfo, "UI_TOUCH_GODBTN");
        }
    }

    public virtual void SetStrings()
    {
        Data.strings.SetTextFromKey(m_labelLevel, "UI_LEVEL");
        Data.strings.SetTextFromKey(m_labelDamage, "UI_DAMAGE");
    }



    public virtual void CheckProperties()
    {
        if(m_slot.hasGod)
        {
            SetGod(m_slot.itemGod);
        }
        else
        {
            RemoveAxe();
        }

        m_labelLevelValue.text = m_level.ToString();
        m_labelLevelValue.AssumeNaturalSize();
        m_labelLevelValue.transform.localPosition = new Vector3(157 - m_labelLevelValue.width/2, 48, 0);
        m_labelLevel.transform.localPosition = new Vector3(157 - m_labelLevel.width/2 - m_labelLevelValue.width - 10, 48, 0);

        if(m_level == 0)
        {
            m_labelDamageValue.text = "0";
        }
        else
        {
            m_labelDamageValue.text = PGTool.StringManager.ToGANumber(m_slot.dmg);
        }
        m_labelDamageValue.AssumeNaturalSize();
        m_labelDamageValue.transform.localPosition = new Vector3(157 - m_labelDamageValue.width/2, -7, 0);
        m_labelDamage.transform.localPosition = new Vector3(157 - m_labelDamage.width/2 - m_labelDamageValue.width - 10, -7, 0);
    }

	public virtual void CheckPrice()
	{
        double price;
        double dmg;

        // 첫 고용일 경우
        if(m_level == 0)
        {
            m_buyCount = 1;

            price = GamePlay.instance.properties.GetGodUpgradePrice(m_index, m_level, m_buyCount);
            m_buttonUpgrade.SetPrice(price);
            dmg = GamePlay.instance.properties.GetGodDamage(m_index, m_level + m_buyCount);
            m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_UNLOCK"), Data.strings.GetString("UI_DAMAGE"), "+" + PGTool.StringManager.ToGANumber(dmg, "0.0", "0.0"));
            if(Data.user.coin.Get() >= price)
            {
                m_buttonUpgrade.SetEnable("button_bg_y");
                return;
            }
            else
            {
                m_buttonUpgrade.SetDisable();
                return;
            }
        }
        // 레벨업일 경우
        else 
        {
            m_buyCount = Data.user.buyCount.Get();
            
            // 구매 제한 있을경우
            if(m_buyCount > 0)
            {
                price = GamePlay.instance.properties.GetGodUpgradePrice(m_index, m_level, m_buyCount);

                // 돈 맞음
                if(price <= Data.user.coin.Get())
                {   
                    dmg = GamePlay.instance.properties.GetGodDamage(m_index, m_level + m_buyCount) - m_slot.dmg;

                    m_buttonUpgrade.SetEnable("button_bg_b");
                    m_buttonUpgrade.SetPrice(price);
                    m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_LEVEL_UP"), "+" + m_buyCount.ToString(),
                        Data.strings.GetString("UI_DAMAGE"), "+" + PGTool.StringManager.ToGANumber(dmg, "0.0", "0.0"));
                    return;
                }
            }


            // 구매 제한 수정해서 사자
            m_buyCount = GamePlay.instance.properties.GetGodUpgradeCount(m_index, m_level, Data.user.coin.Get());
            if(m_buyCount > 0)
            {
                price = GamePlay.instance.properties.GetGodUpgradePrice(m_index, m_level, m_buyCount);
                dmg = GamePlay.instance.properties.GetGodDamage(m_index, m_level + m_buyCount) - m_slot.dmg;
            
                    m_buttonUpgrade.SetEnable("button_bg_b");
                m_buttonUpgrade.SetPrice(price);
                m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_LEVEL_UP"), "+" + m_buyCount.ToString(),
                    Data.strings.GetString("UI_DAMAGE"), "+" + PGTool.StringManager.ToGANumber(dmg, "0.0", "0.0"));
                return;
            }
            
            // 못삼
            m_buyCount = 1;

            price = GamePlay.instance.properties.GetGodUpgradePrice(m_index, m_level, m_buyCount);
            dmg = GamePlay.instance.properties.GetGodDamage(m_index, m_level + m_buyCount) - m_slot.dmg;
            m_buttonUpgrade.SetDisable();
            m_buttonUpgrade.SetPrice(price);
            m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_LEVEL_UP"), "+" + m_buyCount.ToString(),
                Data.strings.GetString("UI_DAMAGE"), "+" + PGTool.StringManager.ToGANumber(dmg, "0.0", "0.0"));

            return;
        }
	}

    public virtual void EquipGod(ItemGod itemGod)
    {
        SetGod(itemGod);
        // TODO 이펙트
    }

	public virtual void OnClickUpgrade(GameObject go)
    {
        // 계산 갯수가 없다
        if(m_buyCount <= 0)     return;

        double price = GamePlay.instance.properties.GetGodUpgradePrice(m_index, m_level, m_buyCount);
        
        // 
        if(Data.user.coin.Subtract(price) == false) return;

        
        Data.user.slotGodLevel.Add(m_index, m_buyCount);
        m_level = Data.user.slotGodLevel.Get(m_index);
        
        // 갓 재 계산
        m_slot.RefreshProperties();
        GamePlay.instance.ui.CheckProperties();
        GamePlay.instance.ui.CheckPrice();
	}

    public virtual void OnClickGod(GameObject go)
    {
        if(m_level > 0)
        {
            UIPopupSelectGod popup = GamePlay.instance.ui.AddPopup<UIPopupSelectGod>("Prefaps/UI/Popup/PopupSelectAxe") as UIPopupSelectGod;
        
            popup.Init(m_index);
        
            UIPanel parentPanel = this.transform.parent.GetComponent<UIPanel>() as UIPanel;
            popup.SetPosY(transform.localPosition.y - parentPanel.clipOffset.y);
        }
    }
}
