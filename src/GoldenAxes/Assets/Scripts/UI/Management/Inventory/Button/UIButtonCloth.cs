﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonCloth : UIButtonItem {

    private UIIconCloth	m_iconCloth;

	private UILabel		m_labelName;
	private UILabel		m_labelInfo;
	private UILabel		m_labelGrade;

	private UISprite	m_bg;
	private GameObject	m_objEquipped;
	private UIButton	m_btnEquip;

	private	UILabel		m_labelEquipped;
	private	UILabel		m_labelEquip;

	public DataCloth	dataCloth				{ get; private set; }

	public override void Awake()
	{
		base.Awake();

		m_labelName = this.transform.Find("LabelName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelInfo = this.transform.Find("LabelInfo").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelGrade = this.transform.Find("LabelGrade").gameObject.GetComponent<UILabel>() as UILabel;

		m_bg = this.transform.Find("BG").gameObject.GetComponent<UISprite>() as UISprite;

		m_objEquipped = this.transform.Find("Equipped").gameObject;
		m_labelEquipped = m_objEquipped.transform.Find("LabelEquipped").gameObject.GetComponent<UILabel>() as UILabel;
		Data.strings.SetTextFromKey(m_labelEquipped, "UI_EQUIPPED");

		m_btnEquip = this.transform.Find("ButtonEquip").gameObject.GetComponent<UIButton>() as UIButton;
		UIEventListener.Get (m_btnEquip.gameObject).onClick += this.OnClickEquip;

		m_labelEquip = m_btnEquip.transform.Find("Label").gameObject.GetComponent<UILabel>() as UILabel;
		Data.strings.SetTextFromKey(m_labelEquip, "UI_EQUIP_CLOTH");

		
		InitIcon();
	}

	public void OnDestroy()
	{
		UIEventListener.Get (m_btnEquip.gameObject).onClick -= this.OnClickEquip;
	}


    protected virtual void InitIcon()
    {
        GameObject obj = PrefabManager.LoadPrefab("Prefaps/UI/IconCloth");
        m_iconCloth = obj.GetComponent<UIIconCloth>() as UIIconCloth;
        PGTool.NGUIManager.SetDepths(m_iconCloth.gameObject, 303);
        m_iconCloth.transform.parent = this.transform;
        m_iconCloth.transform.localScale = Vector3.one;
        m_iconCloth.transform.localPosition = new Vector3(-105, 0, 0);
		m_iconCloth.SetActiveColiider(false);
    }

	public void Init(ItemCloth itemCloth)
	{
		dataCloth = itemCloth.data;
		
		// item
		m_iconCloth.SetCloth(itemCloth);

		// name
        Data.strings.SetTextFromKey(m_labelName, itemCloth.data.name);

		string strInfo = Data.strings.GetString(itemCloth.data.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(itemCloth.param1, "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(itemCloth.param2, "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(itemCloth.param3, "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", itemCloth.data.activeTime.ToString());
		strInfo = strInfo.Replace("DT", itemCloth.data.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
        Data.strings.SetText(m_labelInfo, strInfo);
	}

	public void Equip()
	{
		m_objEquipped.SetActive(true);
		m_bg.spriteName = "wdm_bt_bg3";

		m_btnEquip.gameObject.SetActive(false);

	}

	public void UnEquip()
	{
		m_objEquipped.SetActive(false);
		m_bg.spriteName = "wdm_bt_bg2";

		m_btnEquip.gameObject.SetActive(true);
		
	}
	protected virtual void OnClickEquip(GameObject go)
	{
		// TODO 도끼 디테일 페이지
		GamePlay.instance.SetCloth(dataCloth.id);
        GamePlay.instance.ui.CheckProperties();
	}
}
