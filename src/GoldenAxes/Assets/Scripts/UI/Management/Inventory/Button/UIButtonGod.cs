﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonGod : UIButtonItem {


    private UIIconGod	m_iconGod;

	private UILabel		m_labelName;
	private UILabel		m_labelInfo;
	private UILabel		m_labelGrade;


	private UISprite	m_bgSkill;
	private UISprite	m_iconSkill;
	private UILabel		m_labelSkillName;
	private UILabel		m_labelSkillInfo;



	public DataAxe dataAxe				{ get; private set; }

	public override void Awake()
	{
		base.Awake();

		m_labelName = this.transform.Find("LabelName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelInfo = this.transform.Find("LabelInfo").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelGrade = this.transform.Find("LabelGrade").gameObject.GetComponent<UILabel>() as UILabel;
		
		m_bgSkill = this.transform.Find("BGSkill").gameObject.GetComponent<UISprite>() as UISprite;
		m_iconSkill = this.transform.Find("IconSkill").gameObject.GetComponent<UISprite>() as UISprite;
		m_labelSkillName = this.transform.Find("LabelSkillName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelSkillInfo = this.transform.Find("LabelSkillInfo").gameObject.GetComponent<UILabel>() as UILabel;

		InitIcon();
	}

    protected virtual void InitIcon()
    {
        GameObject obj = PrefabManager.LoadPrefab("Prefaps/UI/IconGod");
        m_iconGod = obj.GetComponent<UIIconGod>() as UIIconGod;
        PGTool.NGUIManager.SetDepths(m_iconGod.gameObject, 303);
        m_iconGod.transform.parent = this.transform;
        m_iconGod.transform.localScale = Vector3.one;
        m_iconGod.transform.localPosition = new Vector3(-105, 0, 0);
    }

	public void Init(ItemGod itemGod)
	{
		// god
		m_iconGod.SetGod(itemGod);

		m_bgSkill.spriteName = "axe_slot_small_bg" + itemGod.data.grade.ToString();

		// name
        Data.strings.SetTextFromKey(m_labelName, itemGod.data.name);

		string strInfo = Data.strings.GetString(itemGod.data.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(itemGod.param1, "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(itemGod.param2, "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(itemGod.param3, "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", itemGod.data.activeTime.ToString());
		strInfo = strInfo.Replace("DT", itemGod.data.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
        Data.strings.SetText(m_labelInfo, strInfo);

		// skill
		m_iconSkill.spriteName = itemGod.data.skill_iconSmall;
        Data.strings.SetTextFromKey(m_labelSkillName, itemGod.data.skill_name);

		strInfo = Data.strings.GetString(itemGod.data.skill_info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(itemGod.param1, "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(itemGod.param2, "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(itemGod.param3, "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", itemGod.data.activeTime.ToString());
		strInfo = strInfo.Replace("DT", itemGod.data.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
        Data.strings.SetText(m_labelSkillInfo, strInfo);
	}
}
