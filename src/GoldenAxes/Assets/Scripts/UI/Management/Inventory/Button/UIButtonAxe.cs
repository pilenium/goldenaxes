﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonAxe : UIButtonItem {


    private UIIconAxe	m_iconAxe;

	private UILabel		m_labelName;
	private UILabel		m_labelInfo;
	private UILabel		m_labelGrade;

	private UISprite	m_bgSkill;
	private UISprite	m_iconSkill;
	private UILabel		m_labelSkillName;
	private UILabel		m_labelSkillInfo;



	public DataAxe dataAxe				{ get; private set; }

	public override void Awake()
	{
		base.Awake();

		m_labelName = this.transform.Find("LabelName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelInfo = this.transform.Find("LabelInfo").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelGrade = this.transform.Find("LabelGrade").gameObject.GetComponent<UILabel>() as UILabel;
		
		m_bgSkill = this.transform.Find("BGSkill").gameObject.GetComponent<UISprite>() as UISprite;
		m_iconSkill = this.transform.Find("IconSkill").gameObject.GetComponent<UISprite>() as UISprite;
		m_labelSkillName = this.transform.Find("LabelSkillName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelSkillInfo = this.transform.Find("LabelSkillInfo").gameObject.GetComponent<UILabel>() as UILabel;

		InitIcon();
	}

    protected virtual void InitIcon()
    {
        GameObject obj = PrefabManager.LoadPrefab("Prefaps/UI/IconAxe");
        m_iconAxe = obj.GetComponent<UIIconAxe>() as UIIconAxe;
        PGTool.NGUIManager.SetDepths(m_iconAxe.gameObject, 303);
        m_iconAxe.transform.parent = this.transform;
        m_iconAxe.transform.localScale = Vector3.one;
        m_iconAxe.transform.localPosition = new Vector3(-105, 0, 0);
    }

	public void Init(ItemAxe itemAxe)
	{
		// axe
		m_iconAxe.SetAxe(itemAxe);

		m_bgSkill.spriteName = "axe_slot_small_bg" + itemAxe.data.grade.ToString();

		DataAxeSlot dataSlot = itemAxe.data.throwSlot;
        Data.strings.SetTextFromKey(m_labelName, dataSlot.name);

		string strInfo = Data.strings.GetString(dataSlot.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(dataSlot.GetParam1(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(dataSlot.GetParam2(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(dataSlot.GetParam3(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", dataSlot.activeTime.ToString());
		strInfo = strInfo.Replace("DT", dataSlot.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
        Data.strings.SetText(m_labelInfo, strInfo);

		// skill
		dataSlot = itemAxe.data.skillSlot;
		m_iconSkill.spriteName = dataSlot.iconSmall;

        Data.strings.SetTextFromKey(m_labelSkillName, dataSlot.name);

		strInfo = Data.strings.GetString(dataSlot.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(dataSlot.GetParam1(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(dataSlot.GetParam2(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(dataSlot.GetParam3(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", dataSlot.activeTime.ToString());
		strInfo = strInfo.Replace("DT", dataSlot.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
        Data.strings.SetText(m_labelSkillInfo, strInfo);
	}
}
