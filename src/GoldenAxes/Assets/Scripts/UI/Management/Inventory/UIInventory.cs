﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventory : UIManagePage {

	public	UISprite			bg;
	public List<UIButton>		tapButtons;
	private List<UIItemList>	pages;

	private UIItemList			m_currentPage = null;

	// Use this for initialization
	void Awake () {
		
		bg.height = GamePlay.instance.ui.ui_height/2 - 70 - 80;
		bg.transform.localPosition = new Vector3(225f, -70 - bg.height/2, 0f);
		
		PGTool.NGUIManager.SetDepths(gameObject, Common.UI_MANAGEMENT_DEPTH);

		// init taps buttons
		for(int i = 0 ; i < tapButtons.Count ; ++i)
		{
			UIEventListener.Get (tapButtons[i].gameObject).onClick += this.OnClickTap;

			tapButtons[i].isEnabled = (i == 0)? false : true;
		}

		// init pages
		pages = new List<UIItemList>();
		for(int i = 0 ; i < tapButtons.Count ; ++i)
		{
			pages.Add(null);
		}
		m_currentPage = null;

		SetStrings();
		SetPage(0);
	}

	void OnDestroy()
	{
		for(int i = 0 ; i < tapButtons.Count ; ++i)
		{
			UIEventListener.Get (tapButtons[i].gameObject).onClick -= this.OnClickTap;
		}
	}

	private void OnClickTap(GameObject go)
	{
		for(int i = 0 ; i < tapButtons.Count ; ++i)
		{
			if(tapButtons[i].gameObject == go)
			{
				tapButtons[i].isEnabled = false;
				SetPage(i);
			}
			else
			{
				tapButtons[i].isEnabled = true;
			}
		}
	}
    public void RefreshAll()
    {
        SetStrings();
		CheckProperties();
		CheckPrice();
    }

	public void SetStrings() 
	{
		UIInventoryTap tap;
		for(int i = 0 ; i < tapButtons.Count ; ++i)
		{
			tap = tapButtons[i].GetComponent<UIInventoryTap>();
			Data.strings.SetTextFromKey(tap.labelName, GetMenuName(i));
		}
	}
	public override void CheckProperties()
	{
		if(m_currentPage != null)
			m_currentPage.CheckProperties();
	}

	public override void CheckPrice()
	{
	}

	private string GetMenuName(int index)
	{
		switch(index)
		{
			case 0:		return "UI_INVEN_AXE";
			case 1:		return "UI_INVEN_GOD";
			case 2:		return "UI_INVEN_HAT";
			case 3:		return "UI_INVEN_CLOTH";
			case 4:		return "UI_INVEN_SHIP";
			default:	return "...";
		}
	}

	private void SetPage(int index)
	{
		if(m_currentPage != null)
		{
			m_currentPage.Clear();
			m_currentPage.gameObject.SetActive(false);
		}


		if(pages[index] == null)
		{
			pages[index] = CreatePage(index);
			if(pages[index] == null)
			{
				return;
			}

			pages[index].transform.parent = this.transform;
			pages[index].transform.localPosition = new Vector3(10.0f, -183.0f, 0.0f);
			pages[index].transform.localScale = Vector3.one;
		}

		m_currentPage = pages[index];
		m_currentPage.gameObject.SetActive(true);

		m_currentPage.Init();
		m_currentPage.enabled = true;
	}
	private UIItemList CreatePage(int index)
	{
		GameObject obj;
		switch(index)
		{
			case 0:
				obj = PrefabManager.LoadPrefab("Prefaps/UI/UIAxeList");
				return obj.GetComponent<UIAxeList>();

			case 1:
				obj = PrefabManager.LoadPrefab("Prefaps/UI/UIGodList");
				return obj.GetComponent<UIGodList>();

			case 2:
				obj = PrefabManager.LoadPrefab("Prefaps/UI/UIHatList");
				return obj.GetComponent<UIHatList>();

			case 3:
				obj = PrefabManager.LoadPrefab("Prefaps/UI/UIClothList");
				return obj.GetComponent<UIClothList>();

			case 4:
				obj = PrefabManager.LoadPrefab("Prefaps/UI/UIShipList");
				return obj.GetComponent<UIShipList>();
		}
		
		return null;
	}


}
