﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIClothList : UIAxeList {

	public override void Init()
	{
		RemoveAllButtons();

		Vector3 pos = Vector3.zero;

		int count = 0;
		UIButtonCloth button;

		foreach(KeyValuePair<int, ItemCloth> kv in Data.inventory.GetCloths()) 
		{
			pos.x = 360.0f * (count%2) + 35.0f;
			pos.y = -148.0f * Mathf.FloorToInt(count/2) - 28.0f;

			button = AddButton<UIButtonCloth>("Prefaps/UI/ButtonItem", m_scrollView.gameObject, pos) as UIButtonCloth;
			button.Init(kv.Value);

			count++;
		}

		SetStrings();
		CheckProperties();
	}

	public override void SetStrings()
	{
		Data.strings.SetTextFromKey(labelTitle, "UI_CLOTH_INVEN");
		Data.strings.SetTextFromKey(labelBook, "UI_CLOTH_BOOK");
		Data.strings.SetTextFromKey(labelMedal, "UI_MEDAL");
		Data.strings.SetTextFromKey(labelPotion, "UI_POTION");
		Data.strings.SetTextFromKey(labelGet, "UI_GET_NEW_CLOTH");
	}

	public override void CheckProperties()
	{
		labelCount.text = Data.inventory.GetCloths().Count + "/" + Data.cloth.GetTotalCount();

		foreach(UIButtonItem button in m_buttons)
		{
			UIButtonCloth buttonCloth = button as UIButtonCloth;
			if(buttonCloth.dataCloth.id == Data.user.slotCloth.Get())
			{
				buttonCloth.Equip();
			}
			else
			{
				buttonCloth.UnEquip();
			}
		}

	}

	/*
	public override void SetTap(UIInventoryTap tap)
	{
		base.SetTap(tap);
		m_tap.labelName.text = Data.strings.GetString("UI_AXE");
//		m_tap.labelCount.text = Data.inventory.GetAxes().Count + "/" + Data.axe.GetTotalCount(); 
	}
	*/
}
