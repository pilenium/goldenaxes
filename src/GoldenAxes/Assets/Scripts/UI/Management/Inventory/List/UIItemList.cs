﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PGTool;
public class UIItemList : MonoBehaviour {
	
	protected	List<UIButtonItem>	m_buttons = null;
	protected	UIScrollView		m_scrollView;

	protected	UIPanel				m_panel;

	void Awake()
	{
		m_scrollView = gameObject.GetComponent<UIScrollView>() as UIScrollView;
		m_panel = GetComponent<UIPanel>() as UIPanel;

		float height = GamePlay.instance.ui.ui_height/2 - 70 - 80;
		float y = -70 - height/2 - transform.localPosition.y;
		m_panel.baseClipRegion = new Vector4(215, y, 730, height);

		m_buttons = new List<UIButtonItem>();
	}

	public virtual void Init()
	{

	}

	public virtual void Clear()
	{

	}
	
	protected UIButtonItem AddButton<T>(string src, GameObject parent, Vector3 position) where T : UIButtonItem
	{
		UIButtonItem nRet = ResourceManager.instance.Get<T>(src, src) as UIButtonItem;
		nRet.transform.parent = parent.transform;
		nRet.transform.localScale = Vector3.one;
		nRet.transform.localPosition = position;

		UIDragScrollView dragScrollView = nRet.gameObject.GetComponent<UIDragScrollView>() as UIDragScrollView;
		dragScrollView.scrollView = m_scrollView;

		m_buttons.Add(nRet);

		return nRet;
	}

	protected void RemoveAllButtons()
	{
		foreach(UIButtonItem button in m_buttons)
		{
			ResourceManager.instance.Return(button);
		}
		m_buttons.Clear();
	}

	public virtual void CheckProperties()
	{
	}

}
