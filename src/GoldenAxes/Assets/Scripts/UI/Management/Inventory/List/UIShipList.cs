﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIShipList : UIAxeList {

	public override void Init()
	{
		RemoveAllButtons();

		Vector3 pos = Vector3.zero;

		int count = 0;
		UIButtonShip button;

		foreach(KeyValuePair<int, ItemShip> kv in Data.inventory.GetShips()) 
		{
			pos.x = 360.0f * (count%2) + 35.0f;
			pos.y = -148.0f * Mathf.FloorToInt(count/2) - 28.0f;

			button = AddButton<UIButtonShip>("Prefaps/UI/ButtonItem", m_scrollView.gameObject, pos) as UIButtonShip;
			button.Init(kv.Value);

			count++;
		}

		SetStrings();
		CheckProperties();
	}

	public override void SetStrings()
	{
		Data.strings.SetTextFromKey(labelTitle, "UI_SHIP_INVEN");
		Data.strings.SetTextFromKey(labelBook, "UI_SHIP_BOOK");
		Data.strings.SetTextFromKey(labelMedal, "UI_MEDAL");
		Data.strings.SetTextFromKey(labelPotion, "UI_POTION");
		Data.strings.SetTextFromKey(labelGet, "UI_GET_NEW_SHIP");
	}

	public override void CheckProperties()
	{
		labelCount.text = Data.inventory.GetShips().Count + "/" + Data.ship.GetTotalCount();

		foreach(UIButtonItem button in m_buttons)
		{
			UIButtonShip buttonShip = button as UIButtonShip;
			if(buttonShip.dataShip.id == Data.user.slotShip.Get())
			{
				buttonShip.Equip();
			}
			else
			{
				buttonShip.UnEquip();
			}
		}

	}

	/*
	public override void SetTap(UIInventoryTap tap)
	{
		base.SetTap(tap);
		m_tap.labelName.text = Data.strings.GetString("UI_AXE");
//		m_tap.labelCount.text = Data.inventory.GetAxes().Count + "/" + Data.axe.GetTotalCount(); 
	}
	*/
}
