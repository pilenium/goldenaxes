﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAxeList : UIItemList {

	public UILabel	labelTitle;
	public UILabel	labelCount;

	public UIButton	buttonBook;
	public UILabel	labelBook;
	public UIButton	buttonMedal;
	public UILabel	labelMedal;
	public UIButton	buttonPotion;
	public UILabel	labelPotion;
	public UIButton	buttonGet;
	public UILabel	labelGet;

	public override void Init()
	{
		RemoveAllButtons();

		Vector3 pos = Vector3.zero;

		int count = 0;
		UIButtonAxe button;


		foreach(KeyValuePair<int, ItemAxe> kv in Data.inventory.GetAxes()) 
		{
			pos.x = 360.0f * (count%2) + 35.0f;
			pos.y = -148.0f * Mathf.FloorToInt(count/2) - 28.0f;

			button = AddButton<UIButtonAxe>("Prefaps/UI/ButtonAxe", m_scrollView.gameObject, pos) as UIButtonAxe;
			button.Init(kv.Value);
			count++;
		}

		SetStrings();
		CheckProperties();
	}

	public virtual void SetStrings()
	{
		Data.strings.SetTextFromKey(labelTitle, "UI_AXE_INVEN");
		Data.strings.SetTextFromKey(labelBook, "UI_AXE_BOOK");
		Data.strings.SetTextFromKey(labelMedal, "UI_MEDAL");
		Data.strings.SetTextFromKey(labelPotion, "UI_POTION");
		Data.strings.SetTextFromKey(labelGet, "UI_GET_NEW_AXE");
	}

	public override void CheckProperties()
	{
		labelCount.text = Data.inventory.GetAxes().Count + "/" + Data.axe.GetTotalCount();
	}

	/*
	public override void SetTap(UIInventoryTap tap)
	{
		base.SetTap(tap);
		m_tap.labelName.text = Data.strings.GetString("UI_AXE");
//		m_tap.labelCount.text = Data.inventory.GetAxes().Count + "/" + Data.axe.GetTotalCount(); 
	}
	*/
}
