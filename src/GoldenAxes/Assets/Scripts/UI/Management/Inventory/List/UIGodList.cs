﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGodList : UIAxeList {

	public override void Init()
	{
		RemoveAllButtons();

		Vector3 pos = Vector3.zero;

		int count = 0;
		UIButtonGod button;

		foreach(KeyValuePair<int, ItemGod> kv in Data.inventory.GetGods()) 
		{
			pos.x = 360.0f * (count%2) + 35.0f;
			pos.y = -148.0f * Mathf.FloorToInt(count/2) - 28.0f;

			button = AddButton<UIButtonGod>("Prefaps/UI/ButtonAxe", m_scrollView.gameObject, pos) as UIButtonGod;
			button.Init(kv.Value);


			count++;
		}

		SetStrings();
		CheckProperties();
	}

	public override void SetStrings()
	{
		Data.strings.SetTextFromKey(labelTitle, "UI_GOD_INVEN");
		Data.strings.SetTextFromKey(labelBook, "UI_GOD_BOOK");
		Data.strings.SetTextFromKey(labelMedal, "UI_MEDAL");
		Data.strings.SetTextFromKey(labelPotion, "UI_POTION");
		Data.strings.SetTextFromKey(labelGet, "UI_GET_NEW_GOD");
	}

	public override void CheckProperties()
	{
		labelCount.text = Data.inventory.GetGods().Count + "/" + Data.god.GetTotalCount();
	}

	/*
	public override void SetTap(UIInventoryTap tap)
	{
		base.SetTap(tap);
		m_tap.labelName.text = Data.strings.GetString("UI_AXE");
//		m_tap.labelCount.text = Data.inventory.GetAxes().Count + "/" + Data.axe.GetTotalCount(); 
	}
	*/
}
