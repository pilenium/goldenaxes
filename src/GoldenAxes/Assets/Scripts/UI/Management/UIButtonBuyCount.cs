﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonBuyCount : MonoBehaviour {

	public int			count;
	public UISprite		bg;
	public UILabel		label;
	public UISprite		select;

	public UIBuyCount	buyCount;

	public bool			IsSelected { get; private set; }


	public void Open(bool animation, float ani_time, UITweener.Method method)
	{
		if(animation)
		{
			TweenAlpha.Begin(bg.gameObject, ani_time, 0.5f).method = method;
			TweenAlpha.Begin(label.gameObject, ani_time, 1.0f).method = method;
			TweenAlpha.Begin(select.gameObject, ani_time, 1.0f).method = method;
		}
		else
		{
			bg.alpha = 0.5f;
			label.alpha = 1.0f;
			select.alpha = 1.0f;
		}
	}

	public void Close()
	{
		bg.alpha = 0.0f;
		label.alpha = 0.0f;
		select.alpha = 0.0f;
	}

	public void Selected(bool selected)
	{
		IsSelected = selected;
		select.enabled = selected;
	}

	public void OnClick()
	{
		if(!IsSelected)
		{
			buyCount.SetCount(count);
		}
		
	}
}
