﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonWoodmanSkill : UIButtonWoodman {

    private int             m_index;
    private SlotProperty    m_dataProperties;
    protected override void InitIcon()
    {
        GameObject obj = PrefabManager.LoadPrefab("Prefaps/UI/IconAxeSkill");
        m_iconAxe = obj.GetComponent<UIIconAxeSkill>() as UIIconAxe;
        PGTool.NGUIManager.SetDepths(m_iconAxe.gameObject, 303);
        m_iconAxe.transform.parent = this.transform;
        m_iconAxe.transform.localScale = Vector3.one;
        m_iconAxe.transform.localPosition = new Vector3(-284, 0, 0);
    }

    public virtual void Init(int index)
    {
        m_level = Data.user.slotSkillLevel.Get(index);
        m_slot = GamePlay.instance.GetAxeSkill(index);

        m_index = index;
        m_dataProperties = Data.properties.woodman.GetSlotProperty(m_index);
    }

    public override void SetInfo(ItemAxe itemAxe)
    {
		DataAxeSlot dataSlot = itemAxe.data.skillSlot;

        Data.strings.SetTextFromKey(m_labelName, dataSlot.name);

		string strInfo = Data.strings.GetString(dataSlot.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(dataSlot.GetParam1(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(dataSlot.GetParam2(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(dataSlot.GetParam3(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", dataSlot.activeTime.ToString());
		strInfo = strInfo.Replace("DT", dataSlot.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
        Data.strings.SetText(m_labelAxeInfo, strInfo);
    }

    public override void SetStrings()
    {
        // 이름
        Data.strings.SetTextFromKey(m_labelLevel, "UI_LEVEL");
        m_labelDamage.text = "";//Data.strings.GetString("UI_DAMAGE");
        m_labelDamageValue.text = "";
    }

    public override void CheckProperties()
    {
        if(m_slot.hasAxe)
        {
            SetAxe(m_slot.axe);
        }
        else
        {
            RemoveAxe();
        }

        m_labelLevelValue.text = m_level.ToString();
        m_labelLevelValue.AssumeNaturalSize();
        m_labelLevelValue.transform.localPosition = new Vector3(157 - m_labelLevelValue.width/2, 48, 0);
        m_labelLevel.transform.localPosition = new Vector3(157 - m_labelLevel.width/2 - m_labelLevelValue.width - 10, 48, 0);

        /*
        if(m_level == 0)
        {
            //m_labelDamageValue.text = "0";
        }
        else
        {
            //m_labelDamageValue.text = "TO-DO";
        }
        m_labelDamageValue.AssumeNaturalSize();
        m_labelDamageValue.transform.localPosition = new Vector3(157 - m_labelDamageValue.width/2, -77, 0);
        m_labelDamage.transform.localPosition = new Vector3(157 - m_labelDamage.width/2 - m_labelDamageValue.width - 10, -78, 0);
        */
    }

    public override void CheckPrice()
    {
        m_buyCount = Data.user.buyCount.Get();
        double price;
        double dmg;

        // 언락은 그냥 1개만 가자
        if(m_level == 0)
        {
            m_buyCount = 1;
            price = GamePlay.instance.properties.GetWoodmanSkillUpgradePrice(m_index, m_level, m_buyCount);

            // 돈 맞음
            if(price <= Data.user.coin.Get())
            {
                SetUpgradeEnable(price, 0);
            }
            else
            {
                SetUpgradeDisable(price, 0);
            }
            return;
        }
        // 업그레이드는 여러개도 가능하게
        else
        {
            // 구매 제한 있을경우
            if(m_buyCount > 0)
            {
                price = GamePlay.instance.properties.GetWoodmanSkillUpgradePrice(m_index, m_level, m_buyCount);

                // 돈 맞음
                if(price <= Data.user.coin.Get())
                {
                    SetUpgradeEnable(price, 0);
                    return;
                }
            }

            // 구매 제한 수정해서 사자
            m_buyCount = GamePlay.instance.properties.GetWoodmanSkillUpgradeCount(m_index, m_level, Data.user.coin.Get());
            if(m_buyCount > 0)
            {
                price = GamePlay.instance.properties.GetWoodmanSkillUpgradePrice(m_index, m_level, m_buyCount);
                SetUpgradeEnable(price, 0);
                return;
            }

            // 못삼
            m_buyCount = 1;
            price = GamePlay.instance.properties.GetWoodmanSkillUpgradePrice(m_index, m_level, m_buyCount);
            SetUpgradeDisable(price, 0);
        }
    }

    public override void SetUpgradeEnable(double price, double dmg)
    {
        m_buttonUpgrade.SetEnable();
        m_buttonUpgrade.SetPrice(price);
        /*
        m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_LEVEL_UP"), "+" + m_buyCount.ToString(),
        Data.strings.GetString("UI_DAMAGE"), "+" + PGTool.StringManager.ToGANumber(dmg, "0.0", "0.0"));
        */
        if(m_level == 0)
        {
            m_buttonUpgrade.SetEnable("button_bg_y");
            m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_UNLCK_SKILL1"), Data.strings.GetString("UI_UNLCK_SKILL2"));
        }
        else
        {
            m_buttonUpgrade.SetEnable("button_bg_r");
            m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_LEVEL_UP"), "+" + m_buyCount.ToString());
        }
    }
    public override void SetUpgradeDisable(double price, double dmg)
    {
        m_buttonUpgrade.SetDisable();
        m_buttonUpgrade.SetPrice(price);
        if(m_level == 0)
        {
            m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_UNLCK_SKILL1"), Data.strings.GetString("UI_UNLCK_SKILL2"));
        }
        else
        {
            m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_LEVEL_UP"), "+" + m_buyCount.ToString());
        }
    }

    public override void OnClickUpgrade(GameObject go)
    {
        // 계산 갯수가 없다
        if(m_buyCount <= 0)     return;

        double cost = GamePlay.instance.properties.GetWoodmanSkillUpgradePrice(m_index, m_level, m_buyCount);
         
        if(Data.user.coin.Subtract(cost) == false) return;
        
        Data.user.slotSkillLevel.Add(m_index, m_buyCount);
        m_level = Data.user.slotSkillLevel.Get(m_index);

        // 나무꾼 재 계산
        //GamePlay.instance.properties.CalculateWoodman();
        GamePlay.instance.ui.CheckProperties();
        GamePlay.instance.ui.CheckPrice();
    }

    public override void OnClickAxe(GameObject go)
    {
        if(m_slot.IsStatus(AxeSlot.STATUS.ACTIVATE))
        {
            return;
        }
        

        if(m_level > 0)
        {
            UIPopupSelectSkill popup = GamePlay.instance.ui.AddPopup<UIPopupSelectSkill>("Prefaps/UI/Popup/PopupSelectAxe") as UIPopupSelectSkill;
    
            popup.Init(m_index);
        
            UIPanel parentPanel = this.transform.parent.GetComponent<UIPanel>() as UIPanel;
            popup.SetPosY(transform.localPosition.y - parentPanel.clipOffset.y);
        }
    }
    
}
