﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIWoodman : UIManagePage {

	private Vector3		m_bt_pos	= new Vector3(225.0f, -172.0f, 0.0f);
	private Vector3		m_skill_pos	= new Vector3(225.0f, -342.0f, 0.0f);
	private Vector3		m_bt_gap	= new Vector3(0.0f, -140.0f, 0.0f);

	private UIButtonWoodman				m_buttonWoodman = null;
	private List<UIButtonWoodmanSkill>	m_buttonSkills = null;

	public UIButton			buttonAccount;
	public UIButton			buttonMSG;
	public UIButton			buttonSkillTree;

	public UILabel			labelDamage;
	public UILabel			throwDamage;
	public UILabel			labelAccount;
	public UILabel			labelMessage;
	public UILabel			labelSkillTree;
	public UILabel			labelWoodman;
	public UILabel			labelSkills;
	private UIPanel			m_panel = null;
	private UIScrollView	m_scrollView = null;
	private UIBuyCount		m_buyCount = null;

    public void Awake()
	{
		UIEventListener.Get(buttonAccount.gameObject).onClick += this.OnClickAccount;
		UIEventListener.Get(buttonMSG.gameObject).onClick += this.OnClickMSG;
		UIEventListener.Get(buttonSkillTree.gameObject).onClick += this.OnClickSkillTree;
    }

    public void OnDestroy()
    {
        UIEventListener.Get (buttonAccount.gameObject).onClick -= this.OnClickAccount;
        UIEventListener.Get (buttonMSG.gameObject).onClick -= this.OnClickMSG;
        UIEventListener.Get (buttonSkillTree.gameObject).onClick -= this.OnClickSkillTree;
	}

	public override void Init()
	{
		Vector3 pos = m_bt_pos;

		if(m_panel == null)
		{
			m_panel = GetComponent<UIPanel>() as UIPanel;
			float height = GamePlay.instance.ui.ui_height / 2 - 70;
			float y = -height/2;
			m_panel.baseClipRegion = new Vector4(224, y, 748, height);
		}

		if(m_scrollView == null)
		{
			m_scrollView = GetComponent<UIScrollView>() as UIScrollView;
		}

		if(m_buyCount == null)
		{
			m_buyCount = GetComponentInChildren<UIBuyCount>() as UIBuyCount;
		}

		// 나무꾼 버튼
		if(m_buttonWoodman == null)
		{
			GameObject obj;
			UIDragScrollView dragScrollView;

			obj = PrefabManager.LoadPrefab("Prefaps/UI/ButtonWoodman");
			obj.transform.parent = this.transform;
			obj.transform.localScale = Vector3.one;
			obj.transform.localPosition = pos;

			m_buttonWoodman = obj.AddComponent<UIButtonWoodman>();
			m_buttonWoodman.Init();

			dragScrollView = obj.GetComponent<UIDragScrollView>() as UIDragScrollView;
			dragScrollView.scrollView = m_scrollView;
		}

		pos = m_skill_pos;

		if(m_buttonSkills == null)
		{
			m_buttonSkills = new List<UIButtonWoodmanSkill>();

			GameObject obj;
			UIDragScrollView dragScrollView;
			UIButtonWoodmanSkill button;
			
			for(int i = 0 ; i < Common.MAX_SKILL_SLOT ; ++i)
			{
				obj = PrefabManager.LoadPrefab("Prefaps/UI/ButtonWoodman");
				obj.transform.parent = this.transform;
				obj.transform.localScale = Vector3.one;
				obj.transform.localPosition = pos;
				button = obj.AddComponent<UIButtonWoodmanSkill>();
				button.Init(i);

				dragScrollView = obj.GetComponent<UIDragScrollView>() as UIDragScrollView;
				dragScrollView.scrollView = m_scrollView;
				m_buttonSkills.Add(button);

				pos += m_bt_gap;
				//ButtonWoodman

			}
		}

		m_buyCount.CheckCount();
		m_buyCount.Close(false);

		SetStrings();
		RefreshAll();
	}
	public override void Clear()
	{
	}

	public override void RefreshAll()
	{
		CheckDamage();

		m_buttonWoodman.RefreshAll();

		foreach(UIButtonWoodmanSkill button in m_buttonSkills)
		{
			button.RefreshAll();
		}
	}

	public void SetStrings()
	{
        Data.strings.SetTextFromKey(labelDamage, "UI_WDM_DMG");
        Data.strings.SetTextFromKey(labelAccount, "UI_ACCOUNT");
        Data.strings.SetTextFromKey(labelMessage, "UI_MSG");
        Data.strings.SetTextFromKey(labelSkillTree, "UI_SKILLTREE");
        Data.strings.SetTextFromKey(labelWoodman, "WOODMAN");
        Data.strings.SetTextFromKey(labelSkills, "AXE_SKILL");
	}
	public void CheckDamage()
	{
		throwDamage.text = PGTool.StringManager.ToGANumber(GamePlay.instance.properties.throwDMG);
	}
	public override void CheckProperties()
	{
		CheckDamage();

		m_buttonWoodman.CheckProperties();
		
		foreach(UIButtonWoodmanSkill button in m_buttonSkills)
		{
			button.CheckProperties();
		}
	}

	public override void CheckPrice()
	{
		m_buttonWoodman.CheckPrice();
		
		foreach(UIButtonWoodmanSkill button in m_buttonSkills)
		{
			button.CheckPrice();
		}
	}

	public void EquipAxe(ItemAxe itemAxe)
	{
		m_buttonWoodman.EquipAxe(itemAxe);
	}

	public void EquipSkill(int index, ItemAxe itemAxe)
	{
		m_buttonSkills[index].EquipAxe(itemAxe);
	}

    public void OnClickAccount(GameObject go)
	{

	}
    public void OnClickMSG(GameObject go)
	{
		
	}
    public void OnClickSkillTree(GameObject go)
	{
		
	}

}
