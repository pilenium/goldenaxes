﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIButtonWoodman : MonoBehaviour {
    protected   AxeSlot             m_slot;

    protected   UILabel		    	m_labelName;
    protected   UILabel		    	m_labelLevel;
    protected   UILabel		    	m_labelLevelValue;
    protected   UILabel	    		m_labelDamage;
    protected   UILabel		    	m_labelDamageValue;

    protected   UILabel		    	m_labelAxeName;
    protected   UILabel	        	m_labelAxeInfo;

    protected   UIButtonUpgrade     m_buttonUpgrade;
    protected   UIIconAxe           m_iconAxe;

    protected   int                 m_level;
    protected   int                 m_buyCount = 0;


    public virtual void Awake()
    {
        InitComponents();
        InitIcon();

		UIEventListener.Get (m_buttonUpgrade.gameObject).onClick += this.OnClickUpgrade;
        m_iconAxe.SetActiveColiider(true);
		UIEventListener.Get (m_iconAxe.gameObject).onClick += this.OnClickAxe;
    }

    public void OnDestroy()
    {
        UIEventListener.Get (m_buttonUpgrade.gameObject).onClick -= this.OnClickUpgrade;
        m_buttonUpgrade = null;

		UIEventListener.Get (m_iconAxe.gameObject).onClick -= this.OnClickAxe;
        m_iconAxe = null;
    }
	protected virtual void InitComponents()
	{
		m_labelName = this.transform.Find("LabelName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelLevel = this.transform.Find("LabelLevel").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelLevelValue = this.transform.Find("LabelLevelValue").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelDamage = this.transform.Find("LabelDamage").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelDamageValue = this.transform.Find("LabelDamageValue").gameObject.GetComponent<UILabel>() as UILabel;

		m_labelAxeName = this.transform.Find("LabelAxeName").gameObject.GetComponent<UILabel>() as UILabel;
		m_labelAxeInfo = this.transform.Find("LabelAxeInfo").gameObject.GetComponent<UILabel>() as UILabel;

        GameObject go = PrefabManager.LoadNGUIPrefab(gameObject, "Prefaps/UI/ButtonUpgrade");
        go.transform.localPosition = new Vector3(265, -2, 0);
		m_buttonUpgrade = go.GetComponent<UIButtonUpgrade>() as UIButtonUpgrade;
	}

    protected virtual void InitIcon()
    {
        GameObject obj = PrefabManager.LoadPrefab("Prefaps/UI/IconAxe");
        m_iconAxe = obj.GetComponent<UIIconAxe>() as UIIconAxe;
        PGTool.NGUIManager.SetDepths(m_iconAxe.gameObject, 303);
        m_iconAxe.transform.parent = this.transform;
        m_iconAxe.transform.localScale = Vector3.one;
        m_iconAxe.transform.localPosition = new Vector3(-284, 0, 0);
    }

    public virtual void Init()
    {
        m_level = Data.user.slotAxeLevel.Get();
        m_slot = GamePlay.instance.GetAxeSlot();
    }

    public virtual void RefreshAll()
    {
        SetStrings();
		CheckProperties();
		CheckPrice();
    }
    public virtual void SetAxe(ItemAxe itemAxe)
    {
        // 슬롯 레벨이 0이면 못장착
        if(m_level == 0)
        {
            // 장착 해주지 말자
            RemoveAxe();
            return;
        }

        m_iconAxe.SetAxe(itemAxe);

        Vector3 pos;

        // axe name
        pos = m_labelAxeName.transform.localPosition;
        Data.strings.SetTextFromKey(m_labelAxeName, itemAxe.data.throwSlot.name);

        SetInfo(itemAxe);
    }

    public virtual void SetInfo(ItemAxe itemAxe)
    {
		DataAxeSlot dataSlot = itemAxe.data.throwSlot;
		string strInfo = Data.strings.GetString(dataSlot.info);
		strInfo = strInfo.Replace("P1", PGTool.StringManager.ToGANumber(dataSlot.GetParam1(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P2", PGTool.StringManager.ToGANumber(dataSlot.GetParam2(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("P3", PGTool.StringManager.ToGANumber(dataSlot.GetParam3(itemAxe.level), "0.0", "0.000"));
		strInfo = strInfo.Replace("AT", dataSlot.activeTime.ToString());
		strInfo = strInfo.Replace("DT", dataSlot.coolTime.ToString());
        strInfo = strInfo.Replace("\n", " ");
        Data.strings.SetText(m_labelAxeInfo, strInfo);
    }
    
    public virtual void RemoveAxe()
    {
        m_iconAxe.RemoveAxe();

        Data.strings.SetTextFromKey(m_labelName, "AXE_SKILL");

        if(m_level == 0)
        {
            m_labelAxeName.text = Data.strings.GetString("");
            m_labelAxeInfo.text = Data.strings.GetString("");

            Data.strings.SetTextFromKey(m_labelAxeName, "UI_LOCKED");
            Data.strings.SetTextFromKey(m_labelAxeInfo, "UI_TOUCH_UNLCKBTN");
        }
        else
        {
            Data.strings.SetTextFromKey(m_labelAxeName, "UI_EQUIP_AXE");
            Data.strings.SetTextFromKey(m_labelAxeInfo, "UI_TOUCH_AXEBTN");
        }
    }
    public virtual void SetStrings()
    {
        Data.strings.SetTextFromKey(m_labelName, "WOODMAN");
        Data.strings.SetTextFromKey(m_labelLevel, "UI_LEVEL");
        Data.strings.SetTextFromKey(m_labelDamage, "UI_DAMAGE");
    }

    public virtual void CheckProperties()
    {
        if(m_slot.hasAxe)
        {
            SetAxe(m_slot.axe);
        }
        else
        {
            RemoveAxe();
        }

        m_labelLevelValue.text = m_level.ToString();
        m_labelLevelValue.AssumeNaturalSize();
        m_labelLevelValue.transform.localPosition = new Vector3(157 - m_labelLevelValue.width/2, 48, 0);
        m_labelLevel.transform.localPosition = new Vector3(157 - m_labelLevel.width/2 - m_labelLevelValue.width - 10, 48, 0);

        if(m_level == 0)
        {
            m_labelDamageValue.text = "0";
        }
        else
        {
            m_labelDamageValue.text = PGTool.StringManager.ToGANumber(GamePlay.instance.properties.throwDMG);
        }
        m_labelDamageValue.AssumeNaturalSize();
        m_labelDamageValue.transform.localPosition = new Vector3(157 - m_labelDamageValue.width/2, -7, 0);
        m_labelDamage.transform.localPosition = new Vector3(157 - m_labelDamage.width/2 - m_labelDamageValue.width - 10, -7, 0);
    }


    public virtual void CheckPrice()
    {
        m_buyCount = Data.user.buyCount.Get();
        double price;
        double dmg;


        // 구매 제한 있을경우
        if(m_buyCount > 0)
        {
            price = GamePlay.instance.properties.GetWoodmanUpgradePrice(m_level, m_buyCount);

            // 돈 맞음
            if(price <= Data.user.coin.Get())
            {   
                dmg = GamePlay.instance.properties.GetWoodmanDamage(m_level + m_buyCount) - GamePlay.instance.properties.throwDMG;

                SetUpgradeEnable(price, dmg);
                return;
            }
        }


        // 구매 제한 수정해서 사자
        m_buyCount = GamePlay.instance.properties.GetWoodmanUpgradeCount(m_level, Data.user.coin.Get());
        if(m_buyCount > 0)
        {
            price = GamePlay.instance.properties.GetWoodmanUpgradePrice(m_level, m_buyCount);
            dmg = GamePlay.instance.properties.GetWoodmanDamage(m_level + m_buyCount) - GamePlay.instance.properties.throwDMG;

            SetUpgradeEnable(price, dmg);
            return;
        }
        
        // 못삼
        m_buyCount = 1;
        price = GamePlay.instance.properties.GetWoodmanUpgradePrice(m_level, m_buyCount);
        dmg = GamePlay.instance.properties.GetWoodmanDamage(m_level + m_buyCount) - GamePlay.instance.properties.throwDMG;

        SetUpgradeDisable(price, dmg);
    }
    public virtual void SetUpgradeEnable(double price, double dmg)
    {
        m_buttonUpgrade.SetEnable();
        m_buttonUpgrade.SetPrice(price);
        m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_LEVEL_UP"), "+" + m_buyCount.ToString(),
            Data.strings.GetString("UI_DAMAGE"), "+" + PGTool.StringManager.ToGANumber(dmg, "0.0", "0.0"));
    }
    public virtual void SetUpgradeDisable(double price, double dmg)
    {
        m_buttonUpgrade.SetDisable();
        m_buttonUpgrade.SetPrice(price);
        m_buttonUpgrade.SetStrings(Data.strings.GetString("UI_LEVEL_UP"), "+" + m_buyCount.ToString(),
            Data.strings.GetString("UI_DAMAGE"), "+" + PGTool.StringManager.ToGANumber(dmg, "0.0", "0.0"));
    }

    public virtual void EquipAxe(ItemAxe itemAxe)
    {
        SetAxe(itemAxe);
        // TODO 이펙트
    }
    

    public virtual void OnClickUpgrade(GameObject go)
    {
        // 계산 갯수가 없다
        if(m_buyCount <= 0)     return;

        double cost = GamePlay.instance.properties.GetWoodmanUpgradePrice(m_level, m_buyCount);
        
        // 
        if(Data.user.coin.Subtract(cost) == false) return;

        Data.user.slotAxeLevel.Add(m_buyCount);
        m_level = Data.user.slotAxeLevel.Get();
        // 나무꾼 재 계산
        GamePlay.instance.properties.CalculateWoodman();

        GamePlay.instance.ui.CheckProperties();
        GamePlay.instance.ui.CheckPrice();
    }

    public virtual void OnClickAxe(GameObject go)
    {
        if(m_level > 0)
        {
            UIPopupSelectAxe popup = GamePlay.instance.ui.AddPopup<UIPopupSelectAxe>("Prefaps/UI/Popup/PopupSelectAxe") as UIPopupSelectAxe;
    
            popup.Init();
        
            UIPanel parentPanel = this.transform.parent.GetComponent<UIPanel>() as UIPanel;
            popup.SetPosY(transform.localPosition.y - parentPanel.clipOffset.y);
        }
    }
}