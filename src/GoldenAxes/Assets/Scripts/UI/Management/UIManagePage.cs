﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManagePage : MonoBehaviour {

	public virtual void Init()
	{
	}
	public virtual void Clear()
	{
	}

	public virtual void RefreshAll()
	{
	}
	public virtual void CheckProperties()
	{
	}
	public virtual void CheckPrice()
	{
	}
}
