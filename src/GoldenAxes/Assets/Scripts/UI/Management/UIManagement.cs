﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManagement : MonoBehaviour {

	private Vector3 pos_open;
	private Vector3 pos_close;
	private Vector3 pos_show;
	private Vector3 pos_hide;
	private float	m_ui_hide_y;

	private float					open_time	= 0.2f;
	private UITweener.Method		open_method	= UITweener.Method.EaseOut;


	private int						page_count = 5;
	private List<UIManagePage>		pages;

	private UIManagePage			m_currentPage = null;

	void Awake()
	{
		m_currentPage = null;

		pages = new List<UIManagePage>();
		for(int i = 0 ; i < page_count ; ++i)
		{
			pages.Add(null);
		}
	}

	public void SetScreenSize(int ui_width, int height, bool show = true, bool animation = true)
	{
		pos_open = new Vector3(0.0f, 0, 0.0f);
		pos_close = new Vector3(0.0f, -height/2, 0.0f);
		pos_hide = new Vector3(0.0f, -height/2 - 100f, 0.0f);

		if(show)
			Show(animation);
		else
			Hide(animation);
	}

	public void Show(bool animation = true)
	{
		if(animation)
		{
			TweenPosition.Begin(gameObject, Common.UI_INTRO_ANIMATION_TIME, pos_close).method = UITweener.Method.EaseInOut;
		}
		else
		{
			gameObject.transform.localPosition = pos_close;
		}
	}

	public void Hide(bool animation = true)
	{
		if(animation)
		{
			TweenPosition.Begin(gameObject, Common.UI_INTRO_ANIMATION_TIME, pos_hide).method = UITweener.Method.EaseInOut;
		}
		else
		{
			gameObject.transform.localPosition = pos_hide;
		}
	}

	private UIManagePage CreatePage(int index)
	{
		GameObject obj;
		switch(index)
		{
			case 0:
				obj = PrefabManager.LoadPrefab("Prefaps/UI/UIWoodman");
				return obj.GetComponent<UIWoodman>();

			case 1:
				obj = PrefabManager.LoadPrefab("Prefaps/UI/UIGodSlot");
				return obj.GetComponent<UIGodSlot>();

			case 2:
				obj = PrefabManager.LoadPrefab("Prefaps/UI/UIInventory");
				return obj.GetComponent<UIInventory>();

			case 3:
				obj = PrefabManager.LoadPrefab("Prefaps/UI/UITreasure");
				return obj.GetComponent<UITreasure>();
		}
		
		return null;
	}

	public void Set(int index)
	{
		TweenPosition.Begin(gameObject, open_time, pos_open).method = open_method;

		if(m_currentPage != null)
		{
			m_currentPage.Clear();
			m_currentPage.gameObject.SetActive(false);
		}


		if(pages[index] == null)
		{
			pages[index] = CreatePage(index);
			if(pages[index] == null)
			{
				return;
			}

			pages[index].transform.parent = this.transform;
			pages[index].transform.localPosition = new Vector3(-225.0f, 0f, 0.0f);
			pages[index].transform.localScale = Vector3.one;
		}

		m_currentPage = pages[index];
		m_currentPage.gameObject.SetActive(true);

		m_currentPage.Init();
//		m_currentPage.enabled = true;
	}

	public void RefreshAll()
	{
		if(m_currentPage != null)	m_currentPage.RefreshAll();
	}

	public void CheckProperties()
	{
		if(m_currentPage != null)	m_currentPage.CheckProperties();
	}
	public void CheckPrice()
	{
		if(m_currentPage != null)	m_currentPage.CheckPrice();
	}

	public void EquipAxe(ItemAxe itemAxe)
	{
		UIWoodman woodmanPage = m_currentPage as UIWoodman;

		if(woodmanPage != null)
		{
			woodmanPage.EquipAxe(itemAxe);
		}
	}

	public void EquipSkill(int index, ItemAxe itemAxe)
	{
		UIWoodman woodmanPage = m_currentPage as UIWoodman;

		if(woodmanPage != null)
		{
			woodmanPage.EquipSkill(index, itemAxe);
		}
	}

	public void EquipGod(int index, ItemGod itemGod)
	{
		UIGodSlot woodmanPage = m_currentPage as UIGodSlot;

		if(woodmanPage != null)
		{
			woodmanPage.EquipGod(index, itemGod);
		}
	}

	public void Close()
	{
		TweenPosition.Begin(gameObject, open_time, pos_close).method = open_method;
	}

	public void OnClick()
	{
		GamePlay.instance.ui.bottom.Close();
	}

}
