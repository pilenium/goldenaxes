﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBuyCount : MonoBehaviour {


	private static int		m_bg_close_width	= 184;
	private static Vector3	m_bg_close_pos		= new Vector3(0.0f, 0.0f, 0.0f);

	private static int		m_bg_open_width		= 519;
	private static Vector3	m_bg_open_pos		= new Vector3(-168.0f, 0.0f, 0.0f);

	
	private static Vector3	m_select_open_pos	= new Vector3(-264.0f, 43.0f, 0.0f);
	private static Vector3	m_select_close_pos	= new Vector3(-200.0f, 43.0f, 0.0f);

	private static float	m_ani_time = 0.1f;
	private	static UITweener.Method	m_ani_method = UITweener.Method.EaseOut;




	public UISprite				bg;
	public UILabel				labelCount;

	public UIButtonBuyCount[]	buttons;
	public UISprite				line;


	public GameObject	selectObject;

	public bool			isOpened				{ get; private set; }

	// Use this for initialization
	void Awake () {
		Close(false);

		CheckCount();
		
		Data.strings.SetTextFromKey(buttons[0].label, "UI_MAX");
		
	}

	public void CheckCount()
	{
		if(Data.user.buyCount.Get() == 0)
		{
			Data.strings.SetTextFromKey(labelCount, "UI_BUY_MAX");
		}
		else
		{
			string strCount = Data.strings.GetString("UI_BUY_COUNT");
			strCount = strCount.Replace("N", Data.user.buyCount.Get().ToString());
			Data.strings.SetText(labelCount, strCount);
		}
		foreach(UIButtonBuyCount button in buttons)
		{
			if(button.count == Data.user.buyCount.Get())
			{
				button.Selected(true);
			}
			else
			{
				button.Selected(false);
			}
		}
	}

	public void SetCount(int count)
	{
		Data.user.buyCount.Set(count);
		CheckCount();
		Close();
		GamePlay.instance.ui.CheckPrice();
	}

	void OnClick()
	{
		if(isOpened)
		{
			Close();
		}	
		else
		{
			Open();
		}
	}

	public void Open(bool animation = true)
	{
		if(animation)
		{
			TweenWidth.Begin(bg, m_ani_time, m_bg_open_width).method = m_ani_method;
			TweenPosition.Begin(bg.gameObject, m_ani_time, m_bg_open_pos).method = m_ani_method;
			TweenPosition.Begin(selectObject, m_ani_time, m_select_open_pos).method = m_ani_method;

			TweenAlpha.Begin(line.gameObject, m_ani_time, 1.0f).method = m_ani_method;
		}
		else
		{
			bg.width = m_bg_open_width;
			bg.transform.localPosition = m_bg_open_pos;
			selectObject.transform.localPosition = m_select_open_pos;

			line.alpha = 1.0f;
		}

		foreach(UIButtonBuyCount button in buttons)
		{
			button.Open(animation, m_ani_time, m_ani_method);
		}

		isOpened = true;
		selectObject.SetActive(true);
	}

	public void Close(bool animation = true)
	{
		if(animation)
		{
			TweenWidth.Begin(bg, m_ani_time/2, m_bg_close_width).method = m_ani_method;
			TweenPosition.Begin(bg.gameObject, m_ani_time/2, m_bg_close_pos).method = m_ani_method;
			TweenPosition.Begin(selectObject, m_ani_time/2, m_select_close_pos).method = m_ani_method;
		}
		else
		{
			bg.width = m_bg_close_width;
			bg.transform.localPosition = m_bg_close_pos;
		}

		// select안에는 무조건 애니 없다
		selectObject.transform.localPosition = m_select_close_pos;
		foreach(UIButtonBuyCount button in buttons)
		{
			button.Close();
		}
		line.alpha = 0.0f;
		selectObject.SetActive(false);

		isOpened = false;	
	}
}
