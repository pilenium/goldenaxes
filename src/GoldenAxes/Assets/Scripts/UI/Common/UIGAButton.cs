﻿
using UnityEngine;

/// <summary>
/// Simple example script of how a button can be scaled visibly when the mouse hovers over it or it gets pressed.
/// </summary>

//[AddComponentMenu("NGUI/Interaction/Button Scale")]
public class UIGAButton : MonoBehaviour
{
	public Transform tweenTarget;
	public Vector3 pressed = new Vector3(1.05f, 1.05f, 1.05f);
	public float duration = 0.2f;

	Vector3 mScale;
	bool mStarted = false;
	void Start ()
	{
		UIButton btn = this.GetComponent<UIButton> ();
		if(btn != null)
		btn.isEnabled = true;



		if (!mStarted)
		{
			mStarted = true;
			if (tweenTarget == null)	 tweenTarget = transform;
			mScale = tweenTarget.localScale;
		}
	}

	void OnEnable ()
	{
		if (mStarted) OnPress(false);
	}

	void OnDisable ()
	{
		if (mStarted && tweenTarget != null)
		{
			TweenScale tc = tweenTarget.GetComponent<TweenScale>();

			if (tc != null)
			{
				tc.value = mScale;
				tc.enabled = false;
			}
		}
	}

	void OnPress (bool isPressed)
	{
		if (enabled)
		{
			if (!mStarted) Start();
			if(isPressed)
			{
				TweenScale.Begin(tweenTarget.gameObject, 0.0f, Vector3.Scale(mScale, pressed) ).method = UITweener.Method.EaseOut;
			}
			else
			{
				TweenScale.Begin(tweenTarget.gameObject, duration, mScale ).method = UITweener.Method.EaseOut;
			}
		}
	}
}
