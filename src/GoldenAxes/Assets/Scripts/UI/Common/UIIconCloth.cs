﻿using UnityEngine;
using System.Collections;

public class UIIconCloth : MonoBehaviour {

	public UISprite			bg;
	public UISprite			icon;
	public GameObject		levelObject;
	public UIProgressBar	levelProgress;
	public UILabel			labelLevelProgress;
	public UILabel			labelLevel;

	public virtual void Awake()
	{
		RemoveCloth();
	}
	

	/// <summary>
	/// 모자 장착
	/// </summary>
	/// <param name="ItemCloth">복장 아이템</param>
    public virtual void SetCloth(ItemCloth itemCloth)
    {
		bg.spriteName = "axe_slot_bg" + itemCloth.data.grade.ToString();
		levelObject.SetActive(true);
		icon.enabled = true;

		icon.spriteName = itemCloth.data.icon;

		labelLevel.text = itemCloth.level.ToString();
		labelLevelProgress.text = itemCloth.exp.ToString();
    }

	public virtual void RemoveCloth()
	{
		bg.spriteName = "axe_slot_bg_none";
		levelObject.SetActive(false);
		icon.enabled = false;
	}

	public virtual void SetActiveColiider(bool active)
	{
		BoxCollider collider = gameObject.GetComponent<BoxCollider>() as BoxCollider;
		collider.enabled = active;
	}

}
