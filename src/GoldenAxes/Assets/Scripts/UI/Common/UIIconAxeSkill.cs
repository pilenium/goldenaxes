﻿using UnityEngine;
using System.Collections;

public class UIIconAxeSkill : UIIconAxe  {

	public UISprite			bgProgress;
	public UISprite			coolProgress;
	public UISprite			iconSmall;
	public UISprite			bgSmall;

    public override void SetAxe(ItemAxe itemAxe)
    {
		base.SetAxe(itemAxe);

		icon.spriteName = itemAxe.data.skillSlot.icon;
		
		bgSmall.spriteName = "axe_slot_small_bg" + itemAxe.data.grade.ToString();

		bgProgress.enabled = true;
		bgProgress.spriteName = "axe_slot_abillity_progress_bg" + itemAxe.data.grade.ToString();
		coolProgress.enabled = true;
		coolProgress.spriteName = "axe_slot_abillity_progress" + itemAxe.data.grade.ToString();
		iconSmall.enabled = true;
		bgSmall.enabled = true;

		iconSmall.spriteName = itemAxe.data.throwSlot.iconSmall;
    }

	public override void RemoveAxe()
	{
		base.RemoveAxe();

		bgProgress.enabled = false;
		coolProgress.enabled = false;
		iconSmall.enabled = false;
		bgSmall.enabled = false;
	}

}
