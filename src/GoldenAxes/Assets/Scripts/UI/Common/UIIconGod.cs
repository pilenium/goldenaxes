﻿using UnityEngine;
using System.Collections;

public class UIIconGod : UIIconAxe {
	public UISprite			iconSmall;
	public UISprite			bgSmall;

	/// <summary>
	/// 도끼 장착 UIIconGod일 경우 이걸로 초기화 하자 (슬롯 타입이 없음)
	/// </summary>
	/// <param name="ItemGod">신령 아이템</param>
	/// <param name="AxeSlot.TYPE">슬롯 타입 (스킬 구분)</param>
	public void SetGod(ItemGod itemGod)
    {
		bg.spriteName = "axe_slot_bg" + itemGod.data.grade.ToString();
		bgSmall.spriteName = "axe_slot_small_bg" + itemGod.data.grade.ToString();

		levelObject.SetActive(true);
		icon.enabled = true;
		iconSmall.enabled = true;
		bgSmall.enabled = true;

		icon.spriteName = itemGod.data.icon;
		iconSmall.spriteName = itemGod.data.skill_iconSmall;

		labelLevel.text = itemGod.level.ToString();
		labelLevelProgress.text = itemGod.exp.ToString();
    }
	public override void RemoveAxe()
	{
		bg.spriteName = "god_slot_bg_none";
		levelObject.SetActive(false);
		icon.enabled = false;
		iconSmall.enabled = false;
		bgSmall.enabled = false;
	}
}
