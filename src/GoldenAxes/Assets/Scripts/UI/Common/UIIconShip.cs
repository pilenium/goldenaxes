﻿using UnityEngine;
using System.Collections;

public class UIIconShip : MonoBehaviour {

	public UISprite			bg;
	public UISprite			icon;
	public GameObject		levelObject;
	public UIProgressBar	levelProgress;
	public UILabel			labelLevelProgress;
	public UILabel			labelLevel;

	public virtual void Awake()
	{
		RemoveShip();
	}
	

	/// <summary>
	/// 배 장착
	/// </summary>
	/// <param name="ItemChip">배 아이템</param>
    public virtual void SetShip(ItemShip itemShip)
    {
		bg.spriteName = "axe_slot_bg" + itemShip.data.grade.ToString();
		levelObject.SetActive(true);
		icon.enabled = true;

		icon.spriteName = itemShip.data.icon;

		labelLevel.text = itemShip.level.ToString();
		labelLevelProgress.text = itemShip.exp.ToString();
    }

	public virtual void RemoveShip()
	{
		bg.spriteName = "axe_slot_bg_none";
		levelObject.SetActive(false);
		icon.enabled = false;
	}

	public virtual void SetActiveColiider(bool active)
	{
		BoxCollider collider = gameObject.GetComponent<BoxCollider>() as BoxCollider;
		collider.enabled = active;
	}

}
