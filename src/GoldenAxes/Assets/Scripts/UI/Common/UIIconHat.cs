﻿using UnityEngine;
using System.Collections;

public class UIIconHat : MonoBehaviour {

	public UISprite			bg;
	public UISprite			icon;
	public GameObject		levelObject;
	public UIProgressBar	levelProgress;
	public UILabel			labelLevelProgress;
	public UILabel			labelLevel;

	public virtual void Awake()
	{
		RemoveHat();
	}
	

	/// <summary>
	/// 모자 장착
	/// </summary>
	/// <param name="ItemHat">도끼 아이템</param>
    public virtual void SetHat(ItemHat itemHat)
    {
		bg.spriteName = "axe_slot_bg" + itemHat.data.grade.ToString();
		levelObject.SetActive(true);
		icon.enabled = true;

		icon.spriteName = itemHat.data.icon;

		labelLevel.text = itemHat.level.ToString();
		labelLevelProgress.text = itemHat.exp.ToString();
    }

	public virtual void RemoveHat()
	{
		bg.spriteName = "axe_slot_bg_none";
		levelObject.SetActive(false);
		icon.enabled = false;
	}

	public virtual void SetActiveColiider(bool active)
	{
		BoxCollider collider = gameObject.GetComponent<BoxCollider>() as BoxCollider;
		collider.enabled = active;
	}

}
