﻿using UnityEngine;
using System.Collections;

public class UIIconAxe : MonoBehaviour {

	public UISprite			bg;
	public UISprite			icon;
	public GameObject		levelObject;
	public UIProgressBar	levelProgress;
	public UILabel			labelLevelProgress;
	public UILabel			labelLevel;

	public virtual void Awake()
	{
		RemoveAxe();
	}
	

	/// <summary>
	/// 도끼 장착
	/// </summary>
	/// <param name="ItemAxe">도끼 아이템</param>
    public virtual void SetAxe(ItemAxe itemAxe)
    {
		bg.spriteName = "axe_slot_bg" + itemAxe.data.grade.ToString();
		levelObject.SetActive(true);
		icon.enabled = true;

		icon.spriteName = itemAxe.data.throwSlot.icon;

		labelLevel.text = itemAxe.level.ToString();
		labelLevelProgress.text = itemAxe.exp.ToString();
    }

	public virtual void RemoveAxe()
	{
		bg.spriteName = "axe_slot_bg_none";
		levelObject.SetActive(false);
		icon.enabled = false;
	}

	public virtual void SetActiveColiider(bool active)
	{
		BoxCollider collider = gameObject.GetComponent<BoxCollider>() as BoxCollider;
		collider.enabled = active;
	}

}
