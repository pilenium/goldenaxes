﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBottom : MonoBehaviour {

	public UIBottomBG		dummyL;
	public UIBottomBG		dummyR;
	public UIBottomMenu[]	menu;


	private int		m_currentSelected = -1;
	public float	m_menu_width		{ private set; get; }
	public float	m_menu_height		{ private set; get; }

	private float	m_ui_show_y;
	private float	m_ui_hide_y;

	void Awake()
	{
		m_menu_width = 750f;
		m_menu_height = 50f;
	}

    private static UIRoot NewMethod()
    {
        return GameObject.Find("UI Root").GetComponent<UIRoot>() as UIRoot;
    }

	public void SetScreenSize(int ui_width, int height, bool show = true, bool animation = true) 
	{
		m_ui_show_y = -height / 2;
		m_ui_hide_y = m_ui_show_y - 100f;

		if(show)
			Show(animation);
		else
			Hide(animation);
	}

    public void SetStrings()
    {
		for(int i = 0 ; i < menu.Length ; ++i)
		{
			Data.strings.SetTextFromKey(menu[i].label, GetMenuName(i));
		}

		
    }
	
	private string GetMenuName(int index)
	{
		switch(index)
		{
			case 0:
			return "UI_BATTLE";

			case 1:
			return "UI_GOD";

			case 2:
			return "UI_ITEM";

			case 3:
			return "UI_TREASURE";

			case 4:
			return "UI_SHOP";
		}

		return "...";
	}

	public void Show(bool animation = true)
	{
		if(animation)
		{
			TweenPosition.Begin(gameObject, Common.UI_INTRO_ANIMATION_TIME,  new Vector3(0f, m_ui_show_y, 0f)).method = UITweener.Method.EaseInOut;
		}
		else
		{
			gameObject.transform.localPosition = new Vector3(0f, m_ui_show_y, 0f);
		}
	}

	public void Hide(bool animation = true)
	{
		if(animation)
		{
			TweenPosition.Begin(gameObject, Common.UI_INTRO_ANIMATION_TIME,  new Vector3(0f, m_ui_hide_y, 0f)).method = UITweener.Method.EaseInOut;
		}
		else
		{
			gameObject.transform.localPosition = new Vector3(0f, m_ui_hide_y, 0f);
		}
	}

    public void ClickMenu(int index)
	{
		if(m_currentSelected == index)
		{
			Close();
		}
		else
		{
			Set(index);
		}
	}

	public void Close()
	{
		Vector3 pos = Vector3.zero;
		// 전부 닫아
		m_currentSelected = -1;
		pos.x = - m_menu_width / 2;

		dummyL.SetDisabled();
		dummyR.SetDisabled();

		for(int i = 0 ; i < menu.Length ; ++i)
		{

			menu[i].SetDisabled();

			pos.x = (i == 0) ? pos.x + menu[i].bg_width/2 : pos.x + menu[i-1].bg_width/2 + menu[i].bg_width/2;
			TweenPosition.Begin(menu[i].gameObject, UIBottomMenu.ani_time, pos).method = UIBottomMenu.ani_method;

		}

		GamePlay.instance.ui.management.Close();
	}

	private  void Set(int index)
	{
		Vector3 pos = Vector3.zero;
	
		// 선택됨
		dummyL.SetEnabled();
		dummyR.SetEnabled();
		
		m_currentSelected = index;
		pos.x = - m_menu_width / 2;

		// 전부 닫아
		for(int i = 0 ; i < menu.Length ; ++i)
		{
			if(i == m_currentSelected)
			{
				menu[i].SetSelected();
			}
			else
			{
				menu[i].SetEnabled();
			}

			pos.x = (i == 0) ? pos.x + menu[i].bg_width/2 : pos.x + menu[i-1].bg_width/2 + menu[i].bg_width/2;
			TweenPosition.Begin(menu[i].gameObject, UIBottomMenu.ani_time, pos).method = UIBottomMenu.ani_method;
		
		}

		GamePlay.instance.ui.management.Set(m_currentSelected);
	}
}