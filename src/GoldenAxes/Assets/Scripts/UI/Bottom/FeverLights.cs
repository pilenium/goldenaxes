﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FeverLights : MonoBehaviour {

	public	List<UISprite>		lights;

	private	float				m_time;
	private	bool				m_enable;

	private int					m_seq;
	private int					m_max_seq;

	delegate void UpdateFunction();
	UpdateFunction				m_updateFunc = null;

	void Awake() {
	}

	void OnDestroy()
	{
	}

	public void Init()
	{
		m_enable = true;
		StartCoroutine ("ChangeLight");
	}

	public void Clear()
	{
		m_enable = false;
		StopCoroutine ("ChangeLight");
	}

	private IEnumerator ChangeLight()
	{
		while (m_enable) {
			if (m_seq > m_max_seq) {
				SetIdle ();
			}
			
			if (m_updateFunc != null)
				m_updateFunc ();

			m_seq++;


			yield return new WaitForSeconds (m_time);
		}
		yield return null;
	}


	public void SetIdle()
	{
		m_seq = 0;
		m_max_seq = int.MaxValue;
		m_time = 0.5f;
		m_updateFunc = UpdateIdle;

		StopCoroutine ("ChangeLight");
		StartCoroutine ("ChangeLight");

	}
	public void SetFlicker(int times)
	{
		m_seq = 0;
		m_max_seq = times * 2;
		m_time = 0.1f;
		m_updateFunc = UpdateFlicker;

		StopCoroutine ("ChangeLight");
		StartCoroutine ("ChangeLight");
	}



	private void UpdateFlicker()
	{
		bool odd = (m_seq % 2 == 0);

		for (int i = 0; i < lights.Count; ++i) {
			lights [i].enabled = odd;
		}
	}

	private void UpdateIdle()
	{
		bool odd = (m_seq % 2 == 0);

		for (int i = 0; i < lights.Count; ++i) {
			if (i % 2 == 0) {
				lights [i].enabled = odd;
			} else {
				lights [i].enabled = !odd;
			}
		}
	}

	private void UpdateRotate()
	{
		int seq = m_seq % 4;

		for (int i = 0; i < lights.Count; ++i) {
			if (i%4 == seq || i%4 == seq+1) {
				lights [i].enabled = true;
			} else {
				lights [i].enabled = false;
			}
		}
	}

}
