﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBottomBG : MonoBehaviour {

	public	static float	ani_time = 0.2f;
	public	static UITweener.Method	ani_method = UITweener.Method.EaseInOut;

	protected static int		m_bg_height_safe = 4;
	protected static int		m_bg_height_d = 50;
	protected static int		m_bg_height_e = 70;
	protected static Color		m_bg_color_d = new Color(0.51f, 0.53f, 0.56f);
	protected static Color		m_bg_color_s = new Color(1.0f, 1.0f, 1.0f);

	public UISprite		bg;

	public int		bg_height			{ get; private set; }
	public Vector3	bg_position			{ get; private set; }


	// Use this for initialization
	void Awake () {
		SetDisabled(false);
	}

	public virtual void SetDisabled(bool animate = true)
	{
		SetBGHeight(m_bg_height_d, animate);

		Vector3 centerPos = Vector3.zero;
		centerPos.y = bg_height/2;

		SetBGPosition(centerPos, animate);
		SetBGColor(true, animate);
	}

	public virtual void SetEnabled(bool animate = true)
	{
		SetBGHeight(m_bg_height_e, animate);

		Vector3 centerPos = Vector3.zero;
		centerPos.y = bg_height/2;

		SetBGPosition(centerPos, animate);
		SetBGColor(true, animate);
	}

	protected void SetBGHeight(int height, bool animate)
	{
		if(animate)
		{
			if(bg_height == height)	return;
			TweenHeight.Begin(bg, ani_time, height+m_bg_height_safe).method = ani_method;
		}
		else
		{
			bg.height = height+m_bg_height_safe;
		}
		
		bg_height = height;
	}

	protected void SetBGPosition(Vector3 pos, bool animate)
	{
		Vector3 safe_pos = pos;
		safe_pos.y -= m_bg_height_safe;
		if(animate)
		{
			if(bg_position == pos)	return;

			TweenPosition.Begin(bg.gameObject, ani_time, safe_pos).method = ani_method;
		}
		else
		{
			bg.transform.localPosition = safe_pos;
		}

		bg_position = pos;
	}

	
	protected  void SetBGColor(bool disable, bool animate)
	{
		if(disable)
		{
			if(animate)
				TweenColor.Begin(bg.gameObject, ani_time, m_bg_color_d).method = ani_method;
			else
				bg.color = m_bg_color_d;
		}
		else
		{
			if(animate)
				TweenColor.Begin(bg.gameObject, ani_time, m_bg_color_s).method = ani_method;
			else
				bg.color = m_bg_color_s;
		}

	}
}
