﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBottomMenu : UIBottomBG {

	protected static int		m_bg_width_d = 150;
	protected static int		m_bg_width_e = 130;
	protected static int		m_bg_width_s = 230;

	protected static float	m_icon_scale_d = 0.8f;
	protected static float	m_icon_scale_s = 1.0f;
	protected static float	m_icon_pos_y_s = 15.0f;

	protected static Vector3	m_arrow_L_pos_hide = new Vector3(-70.0f, 35.0f, 0.0f);
	protected static Vector3	m_arrow_L_pos_show = new Vector3(-100.0f, 35.0f, 0.0f);
	protected static Vector3	m_arrow_R_pos_hide = new Vector3(70.0f, 35.0f, 0.0f);
	protected static Vector3	m_arrow_R_pos_show = new Vector3(100.0f, 35.0f, 0.0f);

	public int			index;

	public UISprite		icon;
	public UILabel		label;
	public UISprite		arrowL = null;
	public UISprite		arrowR = null;

	private BoxCollider	m_collider;

	public int		bg_width			{ get; private set; }


	// Use this for initialization
	void Awake () {
		m_collider = this.GetComponent<BoxCollider>();

		SetDisabled(false);
	}

	public void OnClick()
	{
		GamePlay.instance.ui.bottom.ClickMenu(index);
	}

	public override void SetDisabled(bool animate = true)
	{
		SetBGWidth(m_bg_width_d, animate);
		SetBGHeight(m_bg_height_d, animate);
		SetBGColor(true, animate);

		Vector3 centerPos = Vector3.zero;
		centerPos.y = bg_height/2;

		SetBGPosition(centerPos, animate);

		SetIconScale(m_icon_scale_d, animate);
		SetIconPosition(centerPos, animate);

		SetInfoHide(animate);

		m_collider.size = new Vector3(bg_width, bg_height, 0.0f);
		m_collider.center = centerPos;
	}

	public override void SetEnabled(bool animate = true)
	{
		SetBGWidth(m_bg_width_e, animate);
		SetBGHeight(m_bg_height_e, animate);
		SetBGColor(true, animate);

		Vector3 centerPos = Vector3.zero;
		centerPos.y = bg_height/2;

		SetBGPosition(centerPos, animate);

		SetIconScale(m_icon_scale_d, animate);
		SetIconPosition(centerPos, animate);

		SetInfoHide(animate);

		m_collider.size = new Vector3(bg_width, bg_height, 0.0f);
		m_collider.center = centerPos;
	}

	public virtual void SetSelected(bool animate = true)
	{
		SetBGWidth(m_bg_width_s, animate);
		SetBGHeight(m_bg_height_e, animate);
		SetBGColor(false, animate);

		Vector3 centerPos = Vector3.zero;
		centerPos.y = bg_height/2;

		SetBGPosition(centerPos, animate);

		SetIconScale(m_icon_scale_s, animate);

		centerPos.y += m_icon_pos_y_s;
		SetIconPosition(centerPos, animate);

		SetInfoShow(animate);

		m_collider.size = new Vector3(bg_width, bg_height, 0.0f);
		m_collider.center = new Vector3(0, bg_height/2, 0.0f);
	}


	protected void SetBGWidth(int width, bool animate)
	{
		if(animate)
		{
			if(bg_width == width)	return;

			TweenWidth.Begin(bg, ani_time, width).method = ani_method;
		}
		else
		{
			bg.width = width;
		}

		bg_width = width;
	}



	protected void SetIconScale(float scale, bool animate)
	{
		if(animate)
		{
			TweenScale.Begin(icon.gameObject, ani_time, Vector3.one * scale).method = ani_method;
		}
		else
		{
			icon.transform.localScale = Vector3.one * scale;
		}
	}

	protected void SetIconPosition(Vector3 pos, bool animate)
	{
		if(animate)
		{
			TweenPosition.Begin(icon.gameObject, ani_time, pos).method = ani_method;
		}
		else
		{
			icon.transform.localPosition = pos;
		}

	}

	protected void SetInfoHide(bool animate)
	{
		if(animate)
		{
			if(arrowL != null) 
			{
				TweenPosition.Begin(arrowL.gameObject, ani_time/2, m_arrow_L_pos_hide).method = ani_method;
				TweenAlpha.Begin(arrowL.gameObject, ani_time/2, 0.0f);
			}
			if(arrowR != null)
			{
				TweenPosition.Begin(arrowR.gameObject, ani_time/2, m_arrow_R_pos_hide).method = ani_method;
				TweenAlpha.Begin(arrowR.gameObject, ani_time/2, 0.0f);
			}
			if(label != null)
			{
				TweenAlpha.Begin(label.gameObject, ani_time/2, 0.0f);
			}
		}
		else
		{
			if(arrowL != null)
			{
				arrowL.transform.position = m_arrow_L_pos_hide;
				arrowL.alpha = 0.0f;
			}
			if(arrowR != null)
			{
				arrowR.transform.position = m_arrow_R_pos_hide;
				arrowR.alpha = 0.0f;
			}
			if(label != null)
			{
				label.alpha = 0.0f;
			}
		}

	}
	protected void SetInfoShow(bool animate)
	{
		if(animate)
		{
			if(arrowL != null)
			{
				TweenPosition.Begin(arrowL.gameObject, ani_time, m_arrow_L_pos_show).method = ani_method;
				TweenAlpha.Begin(arrowL.gameObject, ani_time, 1.0f);
			}
			if(arrowR != null)
			{
				TweenPosition.Begin(arrowR.gameObject, ani_time, m_arrow_R_pos_show).method = ani_method;
				TweenAlpha.Begin(arrowR.gameObject, ani_time, 1.0f);
			}
			if(label != null)
			{
				TweenAlpha.Begin(label.gameObject, ani_time, 1.0f);
			}
		}
		else
		{
			if(arrowL != null)
			{
				arrowL.transform.position = m_arrow_L_pos_show;
				arrowL.alpha = 1.0f;
			}
			if(arrowR != null)
			{
				arrowR.transform.position = m_arrow_R_pos_show;
				arrowR.alpha = 1.0f;
			}
			if(label != null)
			{
				label.alpha = 1.0f;
			}
		}
	}
	
}
