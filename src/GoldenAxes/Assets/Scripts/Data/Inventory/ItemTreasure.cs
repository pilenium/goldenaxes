﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTreasure : Item {

	private DataTreasure m_data;
	public DataTreasure data { get { return m_data; } }

	public float param1 { get { return m_data.GetParam1(level); } }
	public float param2 { get { return m_data.GetParam2(level); } }
	public float param3 { get { return m_data.GetParam3(level); } }

	public ItemTreasure(int id, int level) : base(id, level, 0)
	{
		m_data = Data.treasure.GetData(m_id);
		if(m_data == null)
		{
			// ERROR
		}
	}

	~ItemTreasure()
	{

	}

	public override void Clear()
	{
		m_data = null;
	}
}
