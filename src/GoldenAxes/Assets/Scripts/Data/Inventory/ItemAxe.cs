﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAxe : Item {

	private DataAxe m_data;
	public DataAxe data { get { return m_data; } }

	public ItemAxe(int id, int level, int exp) : base(id, level, exp)
	{
		m_data = Data.axe.GetData(m_id);
		if(m_data == null)
		{
			// ERROR
		}
	}

	~ItemAxe()
	{

	}

	public override void Clear()
	{
		m_data = null;
	}
}
