﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemHat : Item {

	private DataHat m_data;
	public DataHat data { get { return m_data; } }

	public float param1 { get { return m_data.GetParam1(level); } }
	public float param2 { get { return m_data.GetParam2(level); } }
	public float param3 { get { return m_data.GetParam3(level); } }

	public ItemHat(int id, int level, int exp) : base(id, level, exp)
	{
		m_data = Data.hat.GetData(m_id);
		if(m_data == null)
		{
			// ERROR
		}
	}

	~ItemHat()
	{

	}

	public override void Clear()
	{
		m_data = null;
	}
}
