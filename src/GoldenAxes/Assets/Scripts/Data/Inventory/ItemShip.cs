﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemShip : Item {

	private DataShip m_data;
	public DataShip data { get { return m_data; } }

	public float param1 { get { return m_data.GetParam1(level); } }
	public float param2 { get { return m_data.GetParam2(level); } }
	public float param3 { get { return m_data.GetParam3(level); } }

	public ItemShip(int id, int level, int exp) : base(id, level, exp)
	{
		m_data = Data.ship.GetData(m_id);
		if(m_data == null)
		{
			// ERROR
		}
	}

	~ItemShip()
	{

	}

	public override void Clear()
	{
		m_data = null;
	}
}
