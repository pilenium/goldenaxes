﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCloth : Item {

	private DataCloth m_data;
	public DataCloth data { get { return m_data; } }

	public float param1 { get { return m_data.GetParam1(level); } }
	public float param2 { get { return m_data.GetParam2(level); } }
	public float param3 { get { return m_data.GetParam3(level); } }

	public ItemCloth(int id, int level, int exp) : base(id, level, exp)
	{
		m_data = Data.cloth.GetData(m_id);
		if(m_data == null)
		{
			// ERROR
		}
	}

	~ItemCloth()
	{

	}

	public override void Clear()
	{
		m_data = null;
	}
}
