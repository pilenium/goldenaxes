﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Item {

	protected	int m_id;
	protected	int m_level;
	protected	int m_exp;

	public int id { get { return m_id; } }
	public int level { get { return m_level; } }
	public int exp { get { return m_exp; } }

	public Item(int id, int level, int exp)
	{
		m_id = id;
		m_level = level;
		m_exp = exp;
	}

	~Item()
	{

	}

	public virtual void Clear()
	{
	}
}
