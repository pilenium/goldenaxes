﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class DataShip {
	public	int			id				{ private set; get; }
	public	int			grade			{ private set; get; }
	public	string		icon			{ private set; get; }
	public	string		name			{ private set; get; }
	public	string		info			{ private set; get; }
	private	string		m_prefab;
	public	string		prefab			{ get { return Data.ship.common_prefab + m_prefab; } }
	public	Vector3		ride_position	{ private set; get; }

	public	string		buff			{ private set; get; }
	public	float		coolTime		{ private set; get; }
	public	float		activeTime		{ private set; get; }
	private	float		param1, param2, param3;
	private	float		upgrade1, upgrade2, upgrade3;

	public DataShip()
	{
	}

	~DataShip()
	{
	}

	public bool Parse(XmlElement xml)
	{
		id = (xml.HasAttribute("id"))? int.Parse(xml.GetAttribute("id")) : 0;
		grade = (xml.HasAttribute("grade"))? int.Parse(xml.GetAttribute("grade")) : 0;
		icon = (xml.HasAttribute("icon"))? xml.GetAttribute("icon") : "";
		name = (xml.HasAttribute("name"))? xml.GetAttribute("name") : "";
		info = (xml.HasAttribute("info"))? xml.GetAttribute("info") : "";
		m_prefab = (xml.HasAttribute("prefab"))? xml.GetAttribute("prefab") : "";

		Vector3 value;
		value.x = (xml.HasAttribute("ride_pos_x"))? float.Parse(xml.GetAttribute("ride_pos_x")) : 0f;
		value.y = (xml.HasAttribute("ride_pos_y"))? float.Parse(xml.GetAttribute("ride_pos_y")) : 0f;
		value.z = (xml.HasAttribute("ride_pos_z"))? float.Parse(xml.GetAttribute("ride_pos_z")) : 0f;
		ride_position = value;

		buff = (xml.HasAttribute("buff"))? xml.GetAttribute("buff") : "";

		coolTime = (xml.HasAttribute("coolTime"))? float.Parse(xml.GetAttribute("coolTime")) : 0f;
		activeTime = (xml.HasAttribute("activeTime"))? float.Parse(xml.GetAttribute("activeTime")) :0f;

		param1 = (xml.HasAttribute("param1"))? float.Parse(xml.GetAttribute("param1")) :0f;
		param2 = (xml.HasAttribute("param2"))? float.Parse(xml.GetAttribute("param2")) :0f;
		param3 = (xml.HasAttribute("param3"))? float.Parse(xml.GetAttribute("param3")) :0f;

		upgrade1 = (xml.HasAttribute("upgrade1"))? float.Parse(xml.GetAttribute("upgrade1")) :0f;
		upgrade2 = (xml.HasAttribute("upgrade2"))? float.Parse(xml.GetAttribute("upgrade2")) :0f;
		upgrade3 = (xml.HasAttribute("upgrade3"))? float.Parse(xml.GetAttribute("upgrade3")) :0f;

		return true;
	}

	public float GetParam1(int level)
	{
		return param1 + upgrade1 * level;
	}
	public float GetParam2(int level)
	{
		return param2 + upgrade2 * level;
	}
	public float GetParam3(int level)
	{
		return param3 + upgrade3 * level;
	}

}


public class DataShips {
	private Dictionary<int, DataShip>		m_ships;
	public	string		common_prefab			{ private set; get; }


	public DataShips()
	{
		m_ships = new Dictionary<int, DataShip>();
	}

	~DataShips()
	{
		m_ships.Clear();
		m_ships = null;
	}

	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml);
	}

	private bool ParseData(XmlNode xml)
	{
		common_prefab = xml["SHIPS"].GetAttribute("common_prefab");
		int id;
		DataShip data;
		foreach (XmlElement xmlAxeModel in xml["SHIPS"].ChildNodes)
		{
			id = int.Parse(xmlAxeModel.GetAttribute("id"));

			data = new DataShip();
			data.Parse(xmlAxeModel);
			m_ships[id] = data;
		}


		return true;
	}

	public DataShip GetData(int ship_id)
	{
		if(m_ships.ContainsKey(ship_id))
		{
			return m_ships[ship_id];
		}
		else
		{
			return null;
		}
	}

	public int GetTotalCount()
	{
		return m_ships.Count;
	}
}