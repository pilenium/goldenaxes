﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class DataCloth {
	public	int			id				{ private set; get; }
	public	int			grade			{ private set; get; }
	public	string		icon			{ private set; get; }
	public	string		name			{ private set; get; }
	public	string		info			{ private set; get; }
	private	string		m_mesh;
	public	string		mesh			{ get { return Data.cloth.common_mesh + m_mesh; } }
	private	string		m_mat;
	public	string		mat				{ get { return Data.cloth.common_mat + m_mat; } }
	public	string		buff			{ private set; get; }
	public	float		coolTime		{ private set; get; }
	public	float		activeTime		{ private set; get; }
	private	float		param1, param2, param3;
	private	float		upgrade1, upgrade2, upgrade3;

	public DataCloth()
	{
	}

	~DataCloth()
	{
	}

	public bool Parse(XmlElement xml)
	{
		id = (xml.HasAttribute("id"))? int.Parse(xml.GetAttribute("id")) : 0;
		grade = (xml.HasAttribute("grade"))? int.Parse(xml.GetAttribute("grade")) : 0;
		icon = (xml.HasAttribute("icon"))? xml.GetAttribute("icon") : "";
		name = (xml.HasAttribute("name"))? xml.GetAttribute("name") : "";
		info = (xml.HasAttribute("info"))? xml.GetAttribute("info") : "";

		m_mesh = (xml.HasAttribute("mesh"))? xml.GetAttribute("mesh") : "";
		m_mat = (xml.HasAttribute("mat"))? xml.GetAttribute("mat") : "";
		buff = (xml.HasAttribute("buff"))? xml.GetAttribute("buff") : "";

		coolTime = (xml.HasAttribute("coolTime"))? float.Parse(xml.GetAttribute("coolTime")) : 0f;
		activeTime = (xml.HasAttribute("activeTime"))? float.Parse(xml.GetAttribute("activeTime")) :0f;

		param1 = (xml.HasAttribute("param1"))? float.Parse(xml.GetAttribute("param1")) :0f;
		param2 = (xml.HasAttribute("param2"))? float.Parse(xml.GetAttribute("param2")) :0f;
		param3 = (xml.HasAttribute("param3"))? float.Parse(xml.GetAttribute("param3")) :0f;

		upgrade1 = (xml.HasAttribute("upgrade1"))? float.Parse(xml.GetAttribute("upgrade1")) :0f;
		upgrade2 = (xml.HasAttribute("upgrade2"))? float.Parse(xml.GetAttribute("upgrade2")) :0f;
		upgrade3 = (xml.HasAttribute("upgrade3"))? float.Parse(xml.GetAttribute("upgrade3")) :0f;

		return true;
	}

	public float GetParam1(int level)
	{
		return param1 + upgrade1 * level;
	}
	public float GetParam2(int level)
	{
		return param2 + upgrade2 * level;
	}
	public float GetParam3(int level)
	{
		return param3 + upgrade3 * level;
	}
}


public class DataCloths {
	private Dictionary<int, DataCloth>		m_cloths;
	public	string		common_mesh			{ private set; get; }
	public	string		common_mat			{ private set; get; }

	public DataCloths()
	{
		m_cloths = new Dictionary<int, DataCloth>();
	}

	~DataCloths()
	{
		m_cloths.Clear();
		m_cloths = null;
	}

	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml);
	}

	private bool ParseData(XmlNode xml)
	{
		common_mesh = xml["CLOTHS"].GetAttribute("common_mesh");
		common_mat = xml["CLOTHS"].GetAttribute("common_mat");
		int id;
		DataCloth data;
		foreach (XmlElement xmlAxeModel in xml["CLOTHS"].ChildNodes)
		{
			id = int.Parse(xmlAxeModel.GetAttribute("id"));

			data = new DataCloth();
			data.Parse(xmlAxeModel);
			m_cloths[id] = data;
		}


		return true;
	}

	public DataCloth GetData(int cloth_id)
	{
		if(m_cloths.ContainsKey(cloth_id))
		{
			return m_cloths[cloth_id];
		}
		else
		{
			return null;
		}
	}


	public int GetTotalCount()
	{
		return m_cloths.Count;
	}
}