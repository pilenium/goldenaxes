﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class HPMultiplier {
	public int stage;
	public float value;
}

public class HPBoss {

	private List<HPMultiplier>		multipliers;

	public	float	multiplier2min		{ get; private set; }
	public	float	multiplier2minPow	{ get; private set; }
	public	float	multiplier2sqroot	{ get; private set; }


	public void Clear()
	{
		multipliers.Clear ();
		multipliers = null;
	}


	public void Init(XmlNode xml)
	{

		// Parse Damage Multipliers
		HPMultiplier mul;
		multipliers = new List<HPMultiplier> ();
		foreach (XmlElement multiplier in xml["Multipliers"].ChildNodes) {
			mul = new HPMultiplier ();
			mul.stage = int.Parse (multiplier.GetAttribute ("stage"));
			mul.value = float.Parse (multiplier.GetAttribute ("value"));
			multipliers.Add (mul);
		}

		multiplier2min		= float.Parse (xml ["Multiplier2min"].InnerText);
		multiplier2minPow	= float.Parse (xml ["Multiplier2minPow"].InnerText);
		multiplier2sqroot	= float.Parse (xml ["Multiplier2sqroot"].InnerText);
	}

	public float GetHP(int stage)
	{
		float mul = 1.0f;
		foreach (HPMultiplier multiplier in multipliers) {
			if(multiplier.stage == stage%10)
			{
				mul = multiplier.value;
			}
		}

		return mul * Mathf.Min(multiplier2min, Mathf.Pow(multiplier2sqroot, Mathf.Floor(stage/multiplier2minPow)));
	}
}


public class DataMonsterProperties {
	public	float	hpBase				{ get; private set; }
	public	float	hpMultiplier		{ get; private set; }
	public	int		hpHurdleStage		{ get; private set; }
	public	float	hpHurdleMultiplier	{ get; private set; }

	public	HPBoss	hpMidBoss			{ get; private set; }
	public	HPBoss	hpBoss				{ get; private set; }

	public	float	goldBase			{ get; private set; }
	public	float	goldMidBoss			{ get; private set; }
	public	float	goldBoss			{ get; private set; }


	public void Clear()
	{
		hpMidBoss.Clear ();
		hpMidBoss = null;

		hpBoss.Clear ();
		hpBoss = null;
	}

	public DataMonsterProperties()
	{
		hpMidBoss = new HPBoss ();
		hpBoss = new HPBoss ();
	}


	public void Init(XmlNode xml)
	{
		hpBase 			 	= float.Parse (xml ["HPBase"].InnerText);
		hpMultiplier		= float.Parse (xml ["HPMultiplier"].InnerText);
		hpHurdleStage		= int.Parse (xml ["HPHurdleStage"].InnerText);
		hpHurdleMultiplier	= float.Parse (xml ["HPHurdleMultiplier"].InnerText);

		hpMidBoss.Init (xml ["HPMidBoss"]);
		hpBoss.Init (xml ["HPBoss"]);


		goldBase 		 	= float.Parse (xml ["GoldBase"].InnerText);
		goldMidBoss		 	= float.Parse (xml ["GoldMidBoss"].InnerText);
		goldBoss 		 	= float.Parse (xml ["GoldBoss"].InnerText);
	}
}

public class DmgMultiplier {
	public int level;
	public float multiplier;
	public float value;
}

public class SlotProperty {
	public float cost;
	public float multiplier;
	public int level;
}

public class DataWoodmanProperties {
	public	float	coin				{ get; private set; }
	public	float	coinMultiplier		{ get; private set; }
	public	float	dmg					{ get; private set; }

	private List<DmgMultiplier>		dmgMultipliers;

	private List<SlotProperty>		slotProperties;

	public void Clear()
	{
		dmgMultipliers.Clear ();
		dmgMultipliers = null;

		slotProperties.Clear ();
		slotProperties = null;
	}

	public void Init(XmlNode xml)
	{
		// Parse Woodman Dmg
		coin = float.Parse (xml ["Coin"].InnerText);
		coinMultiplier = float.Parse (xml ["CoinMultiplier"].InnerText);

		dmg = float.Parse (xml ["DMG"].InnerText);


		// Parse Damage Multipliers
		DmgMultiplier mul;
		dmgMultipliers = new List<DmgMultiplier> ();
		float value = 1;
		foreach (XmlElement multiplier in xml["DMGMultipliers"].ChildNodes) {
			mul = new DmgMultiplier ();
			mul.level = int.Parse (multiplier.GetAttribute ("level"));
			mul.multiplier = float.Parse (multiplier.GetAttribute ("value"));
			value *= mul.multiplier;
			mul.value = value;
			dmgMultipliers.Add (mul);
		}


		// Parse Slot Datas
		SlotProperty slot;
		slotProperties = new List<SlotProperty> ();
		foreach (XmlElement slotProperty in xml["Slots"].ChildNodes) {
			slot = new SlotProperty ();
			slot.cost = float.Parse (slotProperty.GetAttribute ("Cost"));
			slot.multiplier = float.Parse (slotProperty.GetAttribute ("Multiplier"));
			slot.level = int.Parse (slotProperty.GetAttribute ("Level"));
			slotProperties.Add (slot);
		}
	}

	public float GetDmgMultiplier(int level)
	{
		float ret = dmgMultipliers[0].value;
		foreach (DmgMultiplier multiplier in dmgMultipliers) {
			if (multiplier.level > level) {
				return ret;
			}
			ret = multiplier.value;
		}

		return ret;
	}
	public SlotProperty GetSlotProperty(int index)
	{
		if(index >= slotProperties.Count)	return null;
		return slotProperties[index];
	}
}

public class DataGodProperties {
	public	float	coinMultiplier		{ get; private set; }

	private List<DmgMultiplier>		dmgMultipliers;

	private List<float>				slotCosts;

	public void Clear()
	{
		dmgMultipliers.Clear ();
		dmgMultipliers = null;

		slotCosts.Clear();
		slotCosts = null;
	}

	public void Init(XmlNode xml)
	{
		// Parse Woodman Dmg
		coinMultiplier = float.Parse (xml ["CoinMultiplier"].InnerText);

		// Parse Damage Multipliers
		DmgMultiplier mul;
		dmgMultipliers = new List<DmgMultiplier> ();
		float value = 1;
		foreach (XmlElement multiplier in xml["DMGMultipliers"].ChildNodes) {
			mul = new DmgMultiplier ();
			mul.level = int.Parse (multiplier.GetAttribute ("level"));
			mul.multiplier = float.Parse (multiplier.GetAttribute ("value"));
			value *= mul.multiplier;
			mul.value = value;
			dmgMultipliers.Add (mul);
		}


		// Parse Slot Datas
		float cost;
		slotCosts = new List<float> ();
		foreach (XmlElement slotProperty in xml["Unlock"].ChildNodes) {
			cost = float.Parse (slotProperty.GetAttribute ("Cost"));
			slotCosts.Add (cost);
		}
	}

	public float GetDmgMultiplier(int level)
	{
		float ret = dmgMultipliers[0].value;
		foreach (DmgMultiplier multiplier in dmgMultipliers) {
			if (multiplier.level > level) {
				return ret;
			}
			ret = multiplier.value;
		}

		return ret;
	}
	public float GetSlotCost(int index)
	{
		if(index < slotCosts.Count)
		{
			return slotCosts[index];
		}
		return 0;
	}
}

public class DataProperties  {

	// woodman
	public DataMonsterProperties		monster		{ get; private set; }
	public DataWoodmanProperties		woodman		{ get; private set; }

	public DataGodProperties			god			{ get; private set; }

	public DataProperties()
	{
		monster = new DataMonsterProperties ();
		woodman = new DataWoodmanProperties ();
		god = new DataGodProperties ();
	}

	public void Clear()
	{
		monster.Clear ();
		monster = null;

		woodman.Clear ();
		woodman = null;

		god.Clear ();
		god = null;
	}


	public bool LoadData()
	{
		if (LoadMonsterData () == false)
			return false;

		if (LoadWoodmanData () == false)
			return false;

		if (LoadGodData () == false)
			return false;
		
		return true;
	}

	private bool LoadMonsterData()
	{
		TextAsset xmlData = (TextAsset)Resources.Load("Data/MonsterProperties");

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", "MonsterProperties.xml");
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		monster.Init (xml["MonsterProperties"]);

		return true;

		/*
		XmlDocument xml = new XmlDocument();

		try
		{
			xml.Load(Application.dataPath + "/Resources/Data/MonsterProperties.xml");

			monster.Init (xml["MonsterProperties"]);
			return true;
		}
		catch (System.Exception ex)
		{
			UIPopup.Create ("LOAD_ERROR", "MonsterProperties.xml");
		}

		return false;

		*/
	}

	private bool LoadWoodmanData()
	{
		TextAsset xmlData = (TextAsset)Resources.Load("Data/WoodmanProperties");

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", "WoodmanProperties.xml");
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		woodman.Init (xml["WoodmanProperties"]);

		return true;
		/*
		XmlDocument xml = new XmlDocument();

		try
		{
			string FilePath = Path.Combine(Application.dataPath + "/Resources/Data/Chapters/", ChapterName + ".xml");

			var myXml = File.ReadAllText(Application.dataPath + "whatever/path");

			xml.Load(Application.dataPath + "/Resources/Data/WoodmanProperties.xml");

			woodman.Init (xml["WoodmanProperties"]);
			return true;
		}
		catch (System.Exception ex)
		{
			UIPopup.Create ("LOAD_ERROR", "WoodmanProperties.xml");
		}

		return false;
		*/
	}
	private bool LoadGodData()
	{
		TextAsset xmlData = (TextAsset)Resources.Load("Data/GodProperties");

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", "GodProperties.xml");
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		god.Init (xml["GodProperties"]);

		return true;
	}
}
