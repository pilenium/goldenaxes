﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class DataEffect {
	public	string			type;
	public	string			url;
	public	Vector3			pos;

	public DataEffect()
	{
	}
}

public class DataAxeModel {
	public	int			id				{ private set; get; }
	private	string		m_prefab;
	public	string		prefab			{ get { return Data.axeModel.common_prefab + m_prefab; } }
	private	string		m_shadow;
	public	string		shadow			{ get { return Data.axeModel.common_shadow + m_shadow; } }
	public	string		type			{ private set; get; }
	public	float		degree			{ private set; get; }
	public	float		sin				{ private set; get; }
	public	float		cos				{ private set; get; }
	public	float		gravity			{ private set; get; }
	public	bool		trail_rot		{ private set; get; }

	public	Vector3		grip_pos		{ private set; get; }
	public	Vector3		size			{ private set; get; }
	public	Vector3		throw_rot		{ private set; get; }
	public	Vector3		throw_init_rot	{ private set; get; }

	public	DataEffect	effect			{ private set; get; }
	public	DataEffect	effect_hit		{ private set; get; }


	public DataAxeModel()
	{
	}

	~DataAxeModel()
	{
		effect = null;
		effect_hit = null;
	}

	public bool Parse(XmlElement xml)
	{
		id = (xml.HasAttribute("id"))? int.Parse(xml.GetAttribute("id")) : 0;
		m_prefab = (xml.HasAttribute("prefab"))? xml.GetAttribute("prefab") : "";
		m_shadow = (xml.HasAttribute("shadow"))? xml.GetAttribute("shadow") : "";
		type = (xml.HasAttribute("type"))? xml.GetAttribute("type") : "";

		// 각도는 라디안으로 변환
		degree = (xml.HasAttribute("degree"))? float.Parse(xml.GetAttribute("degree")) * Mathf.PI / 180.0f : 0f;	
		// 사인 코사인은 미리 계산 해놓자
		sin = Mathf.Sin (degree);
		cos = Mathf.Cos (degree);

		gravity = (xml.HasAttribute("gravity"))? float.Parse(xml.GetAttribute("gravity")) : 0f;
		trail_rot = (xml.HasAttribute("trail_rot"))? bool.Parse(xml.GetAttribute("trail_rot")) : false;

		Vector3 value;
		if(xml["GRIP_POS"] != null)
		{
			value.x = (xml["GRIP_POS"].HasAttribute("x"))? float.Parse(xml["GRIP_POS"].GetAttribute("x")) : 0f;
			value.y = (xml["GRIP_POS"].HasAttribute("y"))? float.Parse(xml["GRIP_POS"].GetAttribute("y")) : 0f;
			value.z = (xml["GRIP_POS"].HasAttribute("z"))? float.Parse(xml["GRIP_POS"].GetAttribute("z")) : 0f;
			grip_pos = value;
		}
		else
		{
			grip_pos = Vector3.zero;
		}

		if(xml["SIZE"] != null)
		{
			value.x = (xml["SIZE"].HasAttribute("x"))? float.Parse(xml["SIZE"].GetAttribute("x")) : 1f;
			value.y = (xml["SIZE"].HasAttribute("y"))? float.Parse(xml["SIZE"].GetAttribute("y")) : 1f;
			value.z = (xml["SIZE"].HasAttribute("z"))? float.Parse(xml["SIZE"].GetAttribute("z")) : 1f;
			size = value;
		}
		else
		{
			size = Vector3.one;
		}

		if(xml["THROW_ROT"] != null)
		{
			value.x = (xml["THROW_ROT"].HasAttribute("x"))? float.Parse(xml["THROW_ROT"].GetAttribute("x")) : 0f;
			value.y = (xml["THROW_ROT"].HasAttribute("y"))? float.Parse(xml["THROW_ROT"].GetAttribute("y")) : 0f;
			value.z = (xml["THROW_ROT"].HasAttribute("z"))? float.Parse(xml["THROW_ROT"].GetAttribute("z")) : 0f;
			throw_rot = value;
		}
		else
		{
			throw_rot = Vector3.zero;
		}

		if(xml["THROW_INIT_ROT"] != null)
		{
			value.x = (xml["THROW_INIT_ROT"].HasAttribute("x"))? float.Parse(xml["THROW_INIT_ROT"].GetAttribute("x")) : 0f;
			value.y = (xml["THROW_INIT_ROT"].HasAttribute("y"))? float.Parse(xml["THROW_INIT_ROT"].GetAttribute("y")) : 0f;
			value.z = (xml["THROW_INIT_ROT"].HasAttribute("z"))? float.Parse(xml["THROW_INIT_ROT"].GetAttribute("z")) : 0f;
			throw_init_rot = value;
		}
		else
		{
			throw_init_rot = Vector3.zero;
		}

		if(xml["EFFECT"] != null)
		{
			effect = new DataEffect();
			effect.type = (xml["EFFECT"].HasAttribute("type"))? xml["EFFECT"].GetAttribute("type") : "";
			effect.url = (xml["EFFECT"].HasAttribute("url"))? Data.axeModel.common_effect_url + xml["EFFECT"].GetAttribute("url") : "";
			effect.pos.x = (xml["EFFECT"].HasAttribute("x"))? float.Parse(xml["EFFECT"].GetAttribute("x")) : 0f;
			effect.pos.y = (xml["EFFECT"].HasAttribute("y"))? float.Parse(xml["EFFECT"].GetAttribute("y")) : 0f;
			effect.pos.z = (xml["EFFECT"].HasAttribute("z"))? float.Parse(xml["EFFECT"].GetAttribute("z")) : 0f;
		}
		else
		{
			effect = null;
		}

		if(xml["EFFECT_HIT"] != null)
		{
			effect_hit = new DataEffect();
			effect_hit.type = (xml["EFFECT_HIT"].HasAttribute("type"))? xml["EFFECT_HIT"].GetAttribute("type") : "";
			effect_hit.url = (xml["EFFECT_HIT"].HasAttribute("url"))? Data.axeModel.common_effect_hit_url + xml["EFFECT_HIT"].GetAttribute("url") : "";
			effect_hit.pos.x = (xml["EFFECT_HIT"].HasAttribute("x"))? float.Parse(xml["EFFECT_HIT"].GetAttribute("x")) : 0f;
			effect_hit.pos.y = (xml["EFFECT_HIT"].HasAttribute("y"))? float.Parse(xml["EFFECT_HIT"].GetAttribute("y")) : 0f;
			effect_hit.pos.z = (xml["EFFECT_HIT"].HasAttribute("z"))? float.Parse(xml["EFFECT_HIT"].GetAttribute("z")) : 0f;
		}
		else
		{
			effect_hit = null;
		}
		
		return true;
	}
}


public class DataAxeModels {
	private Dictionary<int, DataAxeModel>		m_axeModels;

	public	string		common_prefab			{ private set; get; }
	public	string		common_shadow			{ private set; get; }
	public	string		common_effect_url		{ private set; get; }
	public	string		common_effect_hit_url		{ private set; get; }

	public DataAxeModels()
	{
		m_axeModels = new Dictionary<int, DataAxeModel>();
	}

	~DataAxeModels()
	{
		m_axeModels.Clear();
		m_axeModels = null;
	}

	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml);
	}

	private bool ParseData(XmlNode xml)
	{
		common_prefab = xml["AXEMODELS"].GetAttribute("common_prefab");
		common_shadow = xml["AXEMODELS"].GetAttribute("common_shadow");
		common_effect_url = xml["AXEMODELS"].GetAttribute("common_effect_url");
		common_effect_hit_url = xml["AXEMODELS"].GetAttribute("common_effect_hit_url");

		int id;
		DataAxeModel data;
		foreach (XmlElement xmlAxeModel in xml["AXEMODELS"].ChildNodes)
		{
			id = int.Parse(xmlAxeModel.GetAttribute("id"));

			data = new DataAxeModel();
			data.Parse(xmlAxeModel);
			m_axeModels[id] = data;
		}


		return true;
	}

	public DataAxeModel GetData(int model_id)
	{
		if(m_axeModels.ContainsKey(model_id))
		{
			return m_axeModels[model_id];
		}
		else
		{
			return null;
		}
	}
}
