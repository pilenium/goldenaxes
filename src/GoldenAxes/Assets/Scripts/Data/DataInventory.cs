﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataInventory {

	protected Dictionary<int, ItemAxe>			m_axes;
	protected Dictionary<int, ItemGod>			m_gods;
	protected Dictionary<int, ItemHat>			m_hats;
	protected Dictionary<int, ItemCloth>		m_cloths;
	protected Dictionary<int, ItemShip>			m_ships;
	protected Dictionary<int, ItemTreasure>		m_treasures;

	public DataInventory()
	{
		m_axes = new Dictionary<int, ItemAxe>();
		m_gods = new Dictionary<int, ItemGod>();
		m_hats = new Dictionary<int, ItemHat>();
		m_cloths = new Dictionary<int, ItemCloth>();
		m_ships = new Dictionary<int, ItemShip>();
		m_treasures = new Dictionary<int, ItemTreasure>();
	}	

	~DataInventory()
	{
		foreach(KeyValuePair<int, ItemAxe> kv in m_axes) 
		{
			kv.Value.Clear();
		}
		m_axes.Clear();
		m_axes = null;

		foreach(KeyValuePair<int, ItemGod> kv in m_gods) 
		{
			kv.Value.Clear();
		}
		m_gods.Clear();
		m_gods = null;

		foreach(KeyValuePair<int, ItemHat> kv in m_hats) 
		{
			kv.Value.Clear();
		}
		m_hats.Clear();
		m_hats = null;

		foreach(KeyValuePair<int, ItemCloth> kv in m_cloths) 
		{
			kv.Value.Clear();
		}
		m_cloths.Clear();
		m_cloths = null;

		foreach(KeyValuePair<int, ItemShip> kv in m_ships) 
		{
			kv.Value.Clear();
		}
		m_ships.Clear();
		m_ships = null;

		foreach(KeyValuePair<int, ItemTreasure> kv in m_treasures) 
		{
			kv.Value.Clear();
		}
		m_treasures.Clear();
		m_treasures = null;
	}

	public void Clear()
	{

	}

	/////////////////////////////////////////////
	// AXE
	public ItemAxe AddAxe(int axe_id, int axe_level, int axe_exp)
	{
		ItemAxe ret = new ItemAxe(axe_id, axe_level, axe_exp);
		m_axes[axe_id] = ret;
		return ret;
	}
	public ItemAxe GetAxe(int axe_id)
	{
		return m_axes[axe_id];
	}

	public Dictionary<int, ItemAxe> GetAxes()
	{
		return m_axes;
	}

	/////////////////////////////////////////////
	// GOD
	public ItemGod AddGod(int god_id, int god_level, int god_exp)
	{
		ItemGod ret = new ItemGod(god_id, god_level, god_exp);
		m_gods[god_id] = ret;
		return ret;
	}
	public ItemGod GetGod(int god_id)
	{
		return m_gods[god_id];
	}

	public Dictionary<int, ItemGod> GetGods()
	{
		return m_gods;
	}

	/////////////////////////////////////////////
	// HAT
	public ItemHat AddHat(int hat_id, int hat_level, int hat_exp)
	{
		ItemHat ret = new ItemHat(hat_id, hat_level, hat_exp);
		m_hats[hat_id] = ret;
		return ret;
	}
	public ItemHat GetHat(int hat_id)
	{
		return m_hats[hat_id];
	}

	public Dictionary<int, ItemHat> GetHats()
	{
		return m_hats;
	}

	/////////////////////////////////////////////
	// CLOTH
	public ItemCloth AddCloth(int cloth_id, int cloth_level, int cloth_exp)
	{
		ItemCloth ret = new ItemCloth(cloth_id, cloth_level, cloth_exp);
		m_cloths[cloth_id] = ret;
		return ret;
	}
	public ItemCloth GetCloth(int cloth_id)
	{
		return m_cloths[cloth_id];
	}

	public Dictionary<int, ItemCloth> GetCloths()
	{
		return m_cloths;
	}

	/////////////////////////////////////////////
	// SHIP
	public ItemShip AddShip(int ship_id, int ship_level, int ship_exp)
	{
		ItemShip ret = new ItemShip(ship_id, ship_level, ship_exp);
		m_ships[ship_id] = ret;
		return ret;
	}
	public ItemShip GetShip(int ship_id)
	{
		return m_ships[ship_id];
	}

	public Dictionary<int, ItemShip> GetShips()
	{
		return m_ships;
	}


	/////////////////////////////////////////////
	// SHIP
	public ItemTreasure AddTreasure(int treasure_id, int treasure_level)
	{
		ItemTreasure ret = new ItemTreasure(treasure_id, treasure_level);
		m_treasures[treasure_id] = ret;
		return ret;
	}
	public ItemTreasure GetTreasure(int treasure_id)
	{
		return m_treasures[treasure_id];
	}

	public Dictionary<int, ItemTreasure> GetTreasures()
	{
		return m_treasures;
	}
}
