﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[System.Serializable]
public class DataZone  {
	public	uint			id;
	public	string			name;
	public	string			type;
	public	string			material;

	public	float			size;
	public	int				count;

}


[System.Serializable]
public class DataZones {
	public List<DataZone> zones;

	public DataZone getData(uint zone_id)
	{
		foreach(DataZone data in zones)
		{
			if (data.id == zone_id)
				return data;
		}

		return null;
	}

}
