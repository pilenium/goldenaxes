﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;

[System.Serializable]
public class DataHasItem {
	public int i;
	public int l; 
	public int e;
}

[System.Serializable]
public class DataHasItems {
	public double				relic;
	public List<DataHasItem>	axe;
	public List<DataHasItem>	god;
	public List<DataHasItem>	hat;
	public List<DataHasItem>	cloth;
	public List<DataHasItem>	ship;
	public List<DataHasItem>	treasure;

}


public class DataServer {

	public double	relic			{ private set; get; }

	public DataServer()
	{
	}

	public void Clear()
	{
	}

	~DataServer()
	{
	}

	public void LoadData()
	{
		string json = Data.LoadJson ("Data/Server");
		if (json == null) {
//			LoadError (m_load_index);
		}

		// 일단 시간은 로컬 시간으로
		int server_timestamp = GameTime.GetTimeStamp (System.DateTime.Now);
		GameTime.SetServerTimestamp (server_timestamp);


		// 일단 이걸로
		DataHasItems datas = JsonUtility.FromJson<DataHasItems> (json);

		relic = datas.relic;

		foreach(DataHasItem axe in datas.axe)
		{
			Data.inventory.AddAxe(axe.i, axe.l, axe.e);
		}

		foreach(DataHasItem god in datas.god)
		{
			Data.inventory.AddGod(god.i, god.l, god.e);
		}

		foreach(DataHasItem hat in datas.hat)
		{
			Data.inventory.AddHat(hat.i, hat.l, hat.e);
		}

		foreach(DataHasItem cloth in datas.cloth)
		{
			Data.inventory.AddCloth(cloth.i, cloth.l, cloth.e);
		}

		foreach(DataHasItem ship in datas.ship)
		{
			Data.inventory.AddShip(ship.i, ship.l, ship.e);
		}
		
		foreach(DataHasItem treasure in datas.treasure)
		{
			Data.inventory.AddTreasure(treasure.i, treasure.l);
		}
	}
}
