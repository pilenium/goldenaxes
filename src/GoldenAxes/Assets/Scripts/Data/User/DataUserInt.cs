﻿using System.Collections.Generic;
using UnityEngine;

public class DataUserInt {

	private string	m_key;
	private int		m_value;
	private int		m_bufferCount;
	private int		m_count;

	public DataUserInt(string key, int bufferCount = 1)
	{
		m_key			= key;
		m_value			= PlayerPrefs.GetInt (m_key);
		m_bufferCount	= bufferCount;
		m_count			= 0;
	}

	public void Clear()
	{

	}

	public	int Get()
	{
		return m_value;
	}

	public	void Set(int value)
	{
		m_value = value;

		m_count++;

		if (m_count >= m_bufferCount) {
			m_count = 0;
			PlayerPrefs.SetInt (m_key, m_value);
		}
	}

	public	void Add(int value)
	{
		Set (Get() + value);
	}
}
