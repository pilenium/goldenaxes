﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataUserIntList {

	private string		m_key;
	private int			m_max;
	private List<int>	m_values;
	private int			m_bufferCount;
	private int			m_count;


	public DataUserIntList(string key, int max = 1, int bufferCount = 1)
	{
		m_key = key;
		m_max = max;
		m_values = new List<int> (m_max);
		for (int i = 0; i < max ; ++i) {
			int val = PlayerPrefs.GetInt (m_key + i.ToString ());

			m_values.Add (val);
		}
		m_bufferCount	= bufferCount;
		m_count			= 0;
	}


	public void Clear()
	{
		m_values.Clear ();
		m_values = null;
	}

	public	int Get(int index)
	{
		if (index >= m_max)
			return 0;


		return m_values[index];
	}


	public	void Set(int index, int value)
	{
		if(index >= m_max)
			return;

		m_values[index] = value;

		m_count++;

		if (m_count >= m_bufferCount) {
			m_count = 0;
			PlayerPrefs.SetInt (m_key + index.ToString(), m_values[index]);
		}
	}

	public	void Add(int index, int value)
	{
		Set (index, Get(index) + value);
	}
}
