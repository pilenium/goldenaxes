﻿using System.Collections.Generic;
using UnityEngine;

public class DataUserString {

	private string	m_key;
	private string	m_value;
	private int		m_bufferCount;
	private int		m_count;

	public DataUserString(string key, int bufferCount = 1)
	{
		m_key			= key;
		m_value			= PlayerPrefs.GetString (m_key);
		m_bufferCount	= bufferCount;
		m_count			= 0;
	}

	public void Clear()
	{

	}

	public	string Get()
	{
		return m_value;
	}


	public	void Set(string value)
	{
		m_value = value;

		m_count++;

		if (m_count >= m_bufferCount) {
			m_count = 0;
			PlayerPrefs.SetString (m_key, m_value);
		}
	}
}
