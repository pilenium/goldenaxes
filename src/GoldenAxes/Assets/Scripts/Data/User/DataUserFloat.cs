﻿using System.Collections.Generic;
using UnityEngine;

public class DataUserFloat {

	private string	m_key;
	private float	m_value;
	private int		m_bufferCount;
	private int		m_count;

	public DataUserFloat(string key, int bufferCount = 1)
	{
		m_key			= key;
		m_value			= PlayerPrefs.GetFloat (m_key);
		m_bufferCount	= bufferCount;
		m_count			= 0;
	}

	public void Clear()
	{

	}

	public	float Get()
	{
		return m_value;
	}


	public	void Set(float value)
	{
		m_value = value;

		m_count++;

		if (m_count >= m_bufferCount) {
			m_count = 0;
			PlayerPrefs.SetFloat (m_key, m_value);
		}
	}
}
