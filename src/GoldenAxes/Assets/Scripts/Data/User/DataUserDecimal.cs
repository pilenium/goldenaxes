﻿using System.Collections.Generic;
using UnityEngine;

public class DataUserDecimal {

	private string	m_key;
	private decimal	m_value;
	private int		m_bufferCount;
	private int		m_count;

	public DataUserDecimal(string key, int bufferCount = 1)
	{
		m_key			= key;
		string strValue	= PlayerPrefs.GetString(m_key);
		m_value			= (strValue == "")? 0m : decimal.Parse(strValue);
		m_bufferCount	= bufferCount;
		m_count			= 0;
	}

	public void Clear()
	{

	}

	public	decimal Get()
	{
		return m_value;
	}


	public	void Set(decimal value)
	{
		m_value = value;

		m_count++;

		if (m_count >= m_bufferCount) {
			m_count = 0;

			PlayerPrefs.SetString (m_key, m_value.ToString());
		}

	}

	public	void Add(decimal value)
	{
		Set (Get() + value);
	}
}
