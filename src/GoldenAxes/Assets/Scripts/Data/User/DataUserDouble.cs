﻿using System.Collections.Generic;
using UnityEngine;

public class DataUserDouble {

	private string	m_key;
	private double	m_value;
	private int		m_bufferCount;
	private int		m_count;

	public DataUserDouble(string key, int bufferCount = 1)
	{
		m_key			= key;
		string strValue	= PlayerPrefs.GetString(m_key);
		m_value			= (strValue == "")? 0 : double.Parse(strValue);
		m_bufferCount	= bufferCount;
		m_count			= 0;
	}

	public void Clear()
	{

	}

	public	double Get()
	{
		return m_value;
	}


	public	void Set(double value)
	{
		m_value = value;

		m_count++;

		if (m_count >= m_bufferCount) {
			m_count = 0;

			PlayerPrefs.SetString (m_key, m_value.ToString());
		}

	}

	public	void Add(double value)
	{
		Set (Get() + value);
	}

	public	bool Subtract(double value)
	{
		if(Get() < value)	return false;

		Set(Get() - value);

		return true;
	}
}
