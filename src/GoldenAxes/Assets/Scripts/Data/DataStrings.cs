﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class DataString
{
	public string	kr;
	public string	en;

	public DataString()
	{

	}
	~DataString()
	{

	}

	public bool Parse(XmlElement xml)
	{
		kr = xml.GetAttribute("kr");
		en = xml.GetAttribute("en");

		kr = kr.Replace("\\n", "\n");
		en = en.Replace("\\n", "\n");


		return true;
	}

}


public class DataStrings {
	private Dictionary<string, DataString>	m_strings;

	public DataStrings()
	{
		m_strings = new Dictionary<string, DataString>();

	}
	~DataStrings()
	{
		m_strings.Clear();
		m_strings = null;
	}

	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml);
	}

	private bool ParseData(XmlNode xml)
	{
		string key;
		DataString dataString;
		foreach (XmlElement xmlString in xml["STRINGS"].ChildNodes)
		{
			key = xmlString.GetAttribute("key");

			dataString = new DataString();
			dataString.Parse(xmlString);
			m_strings[key] = dataString;
		}


		return true;
	}

	public string GetString(string key)
	{
		if(m_strings.ContainsKey(key))
		{
			DataString data = m_strings[key];

			switch(Data.user.language.Get())
			{
				case "en":
					return data.en;

				case "kr":
					return data.kr;

				default:
					Data.user.SetDefaultLanguage();
					return GetString(key);
			}
		}
		else
		{
			return "...";
		}
	}


	public void SetTextFromKey(UILabel label, string key)
	{
		SetText(label, GetString(key));
	}

	public void SetText(UILabel label, string text)
	{
		if(label == null)
		return;

		switch(Data.user.language.Get())
		{
			case "en":
				if(label.ambigiousFont is UnityEngine.Font)
				{
					if(label.effectStyle == UILabel.Effect.Outline)
					{
						label.fontSize = label.fontSize + 10;
					}
					else
					{
						label.fontSize = label.fontSize + 2;
						label.effectStyle = UILabel.Effect.None;
					}

					label.effectStyle = UILabel.Effect.None;
					label.spacingX = -1;
					label.spacingY = 0;
				}
			break;

			case "kr":
				if(label.ambigiousFont is UIFont)
				{
					if(label.bitmapFont.name == "GAFontS")
					{
						label.trueTypeFont = Resources.Load("Font/KOR") as Font; 

						//label.fontSize = label.fontSize - 2;
						label.fontSize = Mathf.RoundToInt((float)label.fontSize / 1.3f);

						label.effectStyle = UILabel.Effect.None;

						label.spacingX = 0;
						label.spacingY = 0;
					}
					else
					{
						label.trueTypeFont = Resources.Load("Font/KOR") as Font; 

						//label.fontSize = label.fontSize - 10;
						label.fontSize = Mathf.RoundToInt((float)label.fontSize / 1.4f);
								
						label.effectStyle = UILabel.Effect.Outline;
						label.effectColor = Color.black;
						label.effectDistance = new Vector2(1, 1);

						label.spacingX = 0;
						label.spacingY = 0;
					}
				}

			break;
		}

		label.text = text;
	}
}
