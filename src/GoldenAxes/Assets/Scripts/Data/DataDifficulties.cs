﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class DataDifficulty {
	public enum TYPE
	{
		BATTLE = 0,
		BOSS
	}

	public	TYPE			type			{ private set; get; }
	public	List<int>		levels			{ private set; get; }
	public DataDifficulty()
	{
		levels = new List<int>();
	}

	~DataDifficulty()
	{
		levels.Clear();
		levels = null;
	}

	public bool Parse(XmlElement xml)
	{
		string sType = xml.GetAttribute("type");
		if(sType.ToLower() == "boss")
		{
			type = TYPE.BOSS;
		}
		else
		{
			type = TYPE.BATTLE;
		}

		
		for(int i = 0 ; i < 10 ; ++i)
		{
			if(xml.HasAttribute("level"+i))
			{
				levels.Add(int.Parse(xml.GetAttribute("level"+i)));
			}
		}
		return true;
	}

	public int GetPatternLevel(float stack)
	{
		int index = Mathf.FloorToInt(levels.Count * stack);
		if(index >= levels.Count) index = levels.Count - 1;
		return levels[index];	
	}

	public bool IsBoss()
	{
		return type == TYPE.BOSS;
	}
}


public class DataDifficulties {

	private Dictionary<int, DataDifficulty>		m_difficulties;
	
	public DataDifficulties()
	{
		m_difficulties = new Dictionary<int, DataDifficulty>();
	}
	~DataDifficulties()
	{
		m_difficulties.Clear();
		m_difficulties = null;
	}

	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml);
	}
	private bool ParseData(XmlNode xml)
	{
		int difficulty;

		// 사이즈를 먼저 파싱 (챕터에 사이즈가 들어가야함)
		DataDifficulty dataDifficulty;
		foreach (XmlElement xmlSize in xml["DIFFICULTIES"].ChildNodes)
		{
			difficulty = int.Parse(xmlSize.GetAttribute("difficulty"));

			dataDifficulty = new DataDifficulty();
			dataDifficulty.Parse(xmlSize);
			m_difficulties[difficulty] = dataDifficulty;
		}

		return true;
	}

	public DataDifficulty GetData(int difficulty)
	{
		if(m_difficulties.ContainsKey(difficulty))
		{
			return m_difficulties[difficulty];
		}
		else
		{
			return null;
		}
	}
	
}
