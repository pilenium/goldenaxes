﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class DataAxeSlot {
	public	string			icon				{ private set; get; }
	public	string			iconSmall			{ get { return icon + "s"; } }
	public	string			name				{ private set; get; }
	public	string			info				{ private set; get; }
	public	string			type				{ private set; get; }
	public	string			control				{ private set; get; }
	public	int				model				{ private set; get; }
	public	float			coolTime			{ private set; get; }
	public	float			activeTime			{ private set; get; }
	private	float			param1, param2, param3;
	private	float			upgrade1, upgrade2, upgrade3;

	public bool Parse(XmlElement xml)
	{
		icon = (xml.HasAttribute("icon"))? xml.GetAttribute("icon") : "";
		name = (xml.HasAttribute("name"))? xml.GetAttribute("name") : "";
		info = (xml.HasAttribute("info"))? xml.GetAttribute("info") : "";
		type = (xml.HasAttribute("type"))? xml.GetAttribute("type") : "";
		control = (xml.HasAttribute("control"))? xml.GetAttribute("control") : null;

		model = (xml.HasAttribute("model"))? int.Parse(xml.GetAttribute("model")) : 0;

		coolTime = (xml.HasAttribute("coolTime"))? float.Parse(xml.GetAttribute("coolTime")) : 0f;
		activeTime = (xml.HasAttribute("activeTime"))? float.Parse(xml.GetAttribute("activeTime")) :0f;

		param1 = (xml.HasAttribute("param1"))? float.Parse(xml.GetAttribute("param1")) :0f;
		param2 = (xml.HasAttribute("param2"))? float.Parse(xml.GetAttribute("param2")) :0f;
		param3 = (xml.HasAttribute("param3"))? float.Parse(xml.GetAttribute("param3")) :0f;

		upgrade1 = (xml.HasAttribute("upgrade1"))? float.Parse(xml.GetAttribute("upgrade1")) :0f;
		upgrade2 = (xml.HasAttribute("upgrade2"))? float.Parse(xml.GetAttribute("upgrade2")) :0f;
		upgrade3 = (xml.HasAttribute("upgrade3"))? float.Parse(xml.GetAttribute("upgrade3")) :0f;
		
		return true;
	}

	public float GetParam1(int level)
	{
		return param1 + upgrade1 * level;
	}
	public float GetParam2(int level)
	{
		return param2 + upgrade2 * level;
	}
	public float GetParam3(int level)
	{
		return param3 + upgrade3 * level;
	}
}

public class DataAxe {
	public	int					id				{ private set; get; }
	public	string				info			{ private set; get; }
	public	int					grade			{ private set; get; }
	public	string				name			{ get { return throwSlot.name; } }

	public	DataAxeSlot			throwSlot		{ private set; get; }
	public	DataAxeSlot			skillSlot		{ private set; get; }


	public DataAxe()
	{
		throwSlot = new DataAxeSlot();
		skillSlot = new DataAxeSlot();
	}

	~DataAxe()
	{
		throwSlot = null;
		skillSlot = null;
	}

	public bool Parse(XmlElement xml)
	{
		id = int.Parse(xml.GetAttribute("id"));
		info = xml.GetAttribute("info");
		grade = int.Parse(xml.GetAttribute("grade"));

		if(throwSlot.Parse(xml["THROW"]) == false) return false;
		if(skillSlot.Parse(xml["SKILL"]) == false) return false;

		return true;
	}
}

public class DataAxes {
	private Dictionary<int, DataAxe>		m_axes;

	public DataAxes()
	{
		m_axes = new Dictionary<int, DataAxe>();

	}
	~DataAxes()
	{
		m_axes.Clear();
		m_axes = null;
	}

	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml);
	}

	private bool ParseData(XmlNode xml)
	{
		int id;
		DataAxe dataAxe;
		foreach (XmlElement xmlAxe in xml["AXES"].ChildNodes)
		{
			id = int.Parse(xmlAxe.GetAttribute("id"));

			dataAxe = new DataAxe();
			dataAxe.Parse(xmlAxe);
			m_axes[id] = dataAxe;
		}


		return true;
	}

	public DataAxe GetData(int axe_id)
	{
		if(m_axes.ContainsKey(axe_id))
		{
			return m_axes[axe_id];
		}
		else
		{
			return null;
		}
	}
	
	public int GetTotalCount()
	{
		return m_axes.Count;
	}
}
