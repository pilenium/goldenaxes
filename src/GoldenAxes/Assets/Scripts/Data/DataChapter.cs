﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class DataMonsterIDs {
	public	int		small			{ private set; get; }
	public	int		middle			{ private set; get; }
	public	int		large			{ private set; get; }
	public	int		boss			{ private set; get; }

	public bool Parse(XmlElement xml)
	{
		small = (xml.HasAttribute("small"))? int.Parse(xml.GetAttribute("small")) : 0;
		middle = (xml.HasAttribute("middle"))? int.Parse(xml.GetAttribute("middle")) : 0;
		large = (xml.HasAttribute("large"))? int.Parse(xml.GetAttribute("large")) : 0;
		boss = (xml.HasAttribute("boss"))? int.Parse(xml.GetAttribute("boss")) : 0;

		return true;
	}
}


public class DataChapterSpeed {
	public	float	battle			{ private set; get; }
	public	float	elite			{ private set; get; }
	public	float	boss			{ private set; get; }

	public bool Parse(XmlElement xml)
	{
		battle = (xml.HasAttribute("battle"))? float.Parse(xml.GetAttribute("battle")) : 0f;
		elite = (xml.HasAttribute("elite"))? float.Parse(xml.GetAttribute("elite")) : 0f;
		boss = (xml.HasAttribute("boss"))? float.Parse(xml.GetAttribute("boss")) : 0f;

		return true;
	}
}

public class DataChapter {
	public	int					chapter			{ private set; get; }
	public	string				name			{ private set; get; }
	public	float				stack_inc		{ private set; get; }
	public	DataChapterSpeed	speed			{ private set; get; }
	public	DataChapterSize		size			{ private set; get; }

	public	List<DataDifficulty>	difficulties	{ private set; get; }
	public	DataMonsterIDs			monsterIDs		{ private set; get; }

	private	string				m_bg_prefab;
	private	int					m_bg_index_min;
	private	int					m_bg_index_max;
	public	string				bg						{ get { return Data.chapters.common_prefab + m_bg_prefab + Random.Range(m_bg_index_min, m_bg_index_max).ToString(); } }


	private	string				m_mat_surface;
	private	string				m_mat_water;
	public	float				mat_size		{ private set; get; }
	public	string				mat_surface		{ get { return Data.chapters.common_surface + m_mat_surface; } }
	public	string				mat_water		{ get { return Data.chapters.common_water + m_mat_water; } }

	public	Color32				water_light_color	{ private set; get; }
	public	Color32				water_dark_color	{ private set; get; }


	public	float				water_width				{ get	{ return size.water_width; } }
	public	float				water_height			{ get	{ return size.water_height; } }
	public	float				stage_width				{ get	{ return size.stage_width; } }
	public	float				stage_height			{ get	{ return size.stage_height; } }
	public	Vector3				cameraPosition			{ get	{ return size.cameraPosition; } }
	public	Quaternion			cameraRotation			{ get	{ return size.cameraRotation; } }


	public DataChapter()
	{
		speed = new DataChapterSpeed();
		difficulties = new List<DataDifficulty>();
		monsterIDs = new DataMonsterIDs();
	}

	~DataChapter()
	{
		difficulties.Clear();

		speed = null;
		difficulties = null;
		monsterIDs = null;
	}

	public bool Parse(XmlElement xml)
	{
		name = xml.GetAttribute("name");
		speed.Parse(xml["SPEED"]);
		size = Data.chapters.GetSizeData(int.Parse(xml.GetAttribute("size")));
		stack_inc = float.Parse(xml.GetAttribute("stack_inc"));

		int difficulty;
		for(int i = 1 ; i <= DataChapters.STAGE_IN_CHAPTER ; ++i)
		{
			if(xml["DIFFICULTY"].HasAttribute("s"+i))
			{
				difficulty = int.Parse(xml["DIFFICULTY"].GetAttribute("s"+i));

				difficulties.Add(Data.difficulties.GetData(difficulty));
			}
		}

		monsterIDs.Parse(xml["MONSTER"]);

		m_bg_prefab = xml["BG"].GetAttribute("prefab");
		m_bg_index_min = int.Parse(xml["BG"].GetAttribute("index_min"));
		m_bg_index_max = int.Parse(xml["BG"].GetAttribute("index_max"));

		m_mat_surface = xml["MATERIAL"].GetAttribute("suface");
		m_mat_water = xml["MATERIAL"].GetAttribute("water");
		mat_size = float.Parse(xml["MATERIAL"].GetAttribute("size"));

		water_light_color = PGTool.StringManager.HexToColor(xml["WATER_COLOR"].GetAttribute("light"));
		water_dark_color = PGTool.StringManager.HexToColor(xml["WATER_COLOR"].GetAttribute("dark"));

		return true;
	}

	public DataDifficulty GetDifficulty(int stage)
	{
		return difficulties[(stage - 1)%DataChapters.STAGE_IN_CHAPTER];
	}
}


public class DataChapterSize {
	public	int					god_count		{ private set; get; }

	public	float				water_width		{ private set; get; }
	public	float				water_height	{ private set; get; }

	public	float				stage_width		{ private set; get; }
	public	float				stage_height	{ private set; get; }

	public	Vector3				cameraPosition	{ private set; get; }
	public	Quaternion			cameraRotation	{ private set; get; }

	public bool Parse(XmlElement xml)
	{
		god_count = int.Parse(xml.GetAttribute("god_count"));

		water_width = float.Parse(xml["WATER"].GetAttribute("width"));
		water_height = float.Parse(xml["WATER"].GetAttribute("height"));

		stage_width = float.Parse(xml["STAGE"].GetAttribute("width"));
		stage_height = float.Parse(xml["STAGE"].GetAttribute("height"));
		
		cameraPosition = new Vector3(
			float.Parse(xml["CAMERA_POSITION"].GetAttribute("x")),
			float.Parse(xml["CAMERA_POSITION"].GetAttribute("y")),
			float.Parse(xml["CAMERA_POSITION"].GetAttribute("z"))
		);

		cameraRotation = new Quaternion(
			float.Parse(xml["CAMERA_ROTATION"].GetAttribute("x")),
			float.Parse(xml["CAMERA_ROTATION"].GetAttribute("y")),
			float.Parse(xml["CAMERA_ROTATION"].GetAttribute("z")),
			float.Parse(xml["CAMERA_ROTATION"].GetAttribute("w"))
		);
		
		return true;
	}
}

public class DataChapters {

	private Dictionary<int, DataChapter>		m_chapters;
	private Dictionary<int, DataChapterSize>	m_sizes;


	public	string	common_prefab				{ private set; get; }
	public	string	common_surface				{ private set; get; }
	public	string	common_water				{ private set; get; }

	public	int		last_stage					{ private set; get; }

	public	static int	STAGE_IN_CHAPTER			{ private set; get; }
	
	public DataChapters()
	{
		STAGE_IN_CHAPTER = 10;

		m_chapters = new Dictionary<int, DataChapter>();
		m_sizes = new Dictionary<int, DataChapterSize>();
	}
	~DataChapters()
	{
		m_chapters.Clear();
		m_chapters = null;

		m_sizes.Clear();
		m_sizes = null;
	}



	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml["CHAPTER"]);
	}
	private bool ParseData(XmlNode xml)
	{
		int id, size;
		common_prefab = xml["CHAPTERS"].GetAttribute("common_prefab");
		common_surface = xml["CHAPTERS"].GetAttribute("common_surface");
		common_water = xml["CHAPTERS"].GetAttribute("common_water");


		// 사이즈를 먼저 파싱 (챕터에 사이즈가 들어가야함)
		DataChapterSize dataChapterSize;
		foreach (XmlElement xmlSize in xml["CHAPTER_SIZES"].ChildNodes)
		{
			size = int.Parse(xmlSize.GetAttribute("size"));

			dataChapterSize = new DataChapterSize();
			dataChapterSize.Parse(xmlSize);
			m_sizes[size] = dataChapterSize;

			
		}

		DataChapter dataChapter;
		foreach (XmlElement xmlChapter in xml["CHAPTERS"].ChildNodes)
		{
			id = int.Parse(xmlChapter.GetAttribute("id"));

			dataChapter = new DataChapter();
			dataChapter.Parse(xmlChapter);
			m_chapters[id] = dataChapter;

			last_stage = id * STAGE_IN_CHAPTER;
		}

		return true;
	}

	public int GetChapter(int stage)
	{
		return Mathf.FloorToInt((stage-1)/STAGE_IN_CHAPTER) + 1;
	}

	public int GetNextStage(int stage)
	{
		return (stage == last_stage)? last_stage : stage + 1;
	}

	public int GetPrevStage(int stage)
	{
		return (stage == 1)? 1 : stage - 1;
	}

	public bool IsBossStage(int stage)
	{
		int chapter = GetChapter(stage);
		DataChapter dataChapter = GetChapterData(chapter);
		DataDifficulty dataDifficulty = dataChapter.GetDifficulty(stage);
		return dataDifficulty.IsBoss();
	}

	public bool IsLastStageInChapter(int stage)
	{
		return (stage%10 == 0);
	}

	public bool IsSameChapter(int stageA, int stageB)
	{
		return Mathf.FloorToInt((stageA-1)/STAGE_IN_CHAPTER) == Mathf.FloorToInt((stageB-1)/STAGE_IN_CHAPTER);
	}
	

	public DataChapter GetChapterData(int chapter)
	{
		return m_chapters[chapter];
	}

	public DataChapterSize GetSizeData(int size)
	{
		return m_sizes[size];
	}
}
