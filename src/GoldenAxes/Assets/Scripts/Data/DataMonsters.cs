﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class DataMonster  {
	public enum TYPE
	{
		MINION = 0,
		ELITE,
		BOSS
	}

	public	string			name;
	public	TYPE			type;
	private	string			m_prefab;
	public	string			prefab		{ get { return Data.monster.common_prefab + m_prefab; } }
	public	float			health		{ private set; get; }
	public	float			armor		{ private set; get; }
	public	float			speed		{ private set; get; }
	public	string			size		{ private set; get; }
	public	float			scale		{ private set; get; }


	//public	DataMonsterAni	animations;

	public bool Parse(XmlElement xml)
	{
		name = xml.GetAttribute("name");
		string strType = xml.GetAttribute("type").ToLower();
		switch(strType)
		{
			case "boss":
			type = TYPE.BOSS;
			break;

			case "elite":
			type = TYPE.ELITE;
			break;

			default:
			type = TYPE.MINION;
			break;
		}
		m_prefab = xml.GetAttribute("prefab");

		health = (xml.HasAttribute("health"))? float.Parse(xml.GetAttribute("health")) : 0f;
		armor = (xml.HasAttribute("armor"))? float.Parse(xml.GetAttribute("armor")) : 0f;
		speed = (xml.HasAttribute("speed"))? float.Parse(xml.GetAttribute("speed")) : 0f;
		size = xml.GetAttribute("size");
		scale = (xml.HasAttribute("scale"))? float.Parse(xml.GetAttribute("scale")) : 0f;

		return true;
	}
}


public class DataMonsters {
	private	Dictionary<int, DataMonster>		m_monsters;
	public	string		common_prefab			{ private set; get; }

	public DataMonsters()
	{
		m_monsters = new Dictionary<int, DataMonster>();

	}
	~DataMonsters()
	{
		m_monsters.Clear();
		m_monsters = null;
	}

	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml);
	}
	
	private bool ParseData(XmlNode xml)
	{
		common_prefab = xml["MONSTERS"].GetAttribute("common_prefab");
		int id;
		DataMonster dataMonster;
		foreach (XmlElement xmlAxe in xml["MONSTERS"].ChildNodes)
		{
			id = int.Parse(xmlAxe.GetAttribute("id"));

			dataMonster = new DataMonster();
			dataMonster.Parse(xmlAxe);
			m_monsters[id] = dataMonster;
		}


		return true;
	}
	public DataMonster getData(int monster_id)
	{
		if(m_monsters.ContainsKey(monster_id))
		{
			return m_monsters[monster_id];
		}
		else
		{
			return null;
		}
	}

}
