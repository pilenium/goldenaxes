﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class DataPatternObject  {
	public	string			size;
	public	float			x;
	public	float			z;
}

[System.Serializable]
public class DataPattern  {
	public	float					width;
	public	float					height;
	public	List<DataPatternObject>	objects;
}

[System.Serializable]
public class DataPatternLevel  {
	public	int						level;
	public	List<DataPattern>		patterns;

	public DataPattern GetPattern(int index)
	{
		if (index < patterns.Count) {
			return patterns [index];
		}
		return null;
	}

	public DataPattern GetRandomPattern()
	{
		return patterns [Random.Range (0, patterns.Count)];
	}
}



[System.Serializable]
public class DataPatterns {
	public List<DataPatternLevel> patterns;

	public DataPatternLevel GetPatternLevel(int level)
	{
		foreach(DataPatternLevel patternLevel in patterns)
		{
			if (patternLevel.level == level)
			{
				return patternLevel;
			}
		}
		return null;
	}

	public DataPattern GetRandomPattern(int level)
	{
		DataPatternLevel patternLevel = GetPatternLevel(level);

		if (patternLevel != null) {
			return patternLevel.GetRandomPattern ();
		}
		return null;
	}

}
