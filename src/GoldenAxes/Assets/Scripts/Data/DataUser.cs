﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class DataUser {
	
	public DataUserString		id;

	public DataUserString		language;


	public DataUserDouble		coin;
	public DataUserInt			diamond;
	public DataUserInt			stage;

	// slot axe
	public DataUserInt			slotAxe;
	public DataUserInt			slotAxeLevel;
	public DataUserInt			slotAxeTime;
	
	// slot skills
	public DataUserIntList		slotSkillAxe;
	public DataUserIntList		slotSkillLevel;
	public DataUserIntList		slotSkillTime;

	// slot gods
	public DataUserIntList		slotGod;
	public DataUserIntList		slotGodLevel;


	public DataUserInt			slotHat;
	public DataUserInt			slotCloth;
	public DataUserInt			slotShip;

	public DataUserInt			buyCount;

	public DataUser()
	{
		id				= new DataUserString("id");
		language		= new DataUserString("lang");

		coin			= new DataUserDouble("coin");
		diamond			= new DataUserInt("diamond");
		stage			= new DataUserInt("stage");

		// slot axe
		slotAxe			= new DataUserInt("slotAxe");
		slotAxeLevel	= new DataUserInt("slotAxeLevel");
		slotAxeTime		= new DataUserInt("slotAxeTime");

		// slot skills
		slotSkillAxe	= new DataUserIntList("slotSkillAxe", Common.MAX_SKILL_SLOT);
		slotSkillLevel	= new DataUserIntList("slotSkillLevel", Common.MAX_SKILL_SLOT);
		slotSkillTime	= new DataUserIntList("slotSkillTime", Common.MAX_SKILL_SLOT);

		// slot gods
		slotGod			= new DataUserIntList("slotGod", Common.MAX_GOD_SLOT);
		slotGodLevel	= new DataUserIntList("slotGodLevel", Common.MAX_GOD_SLOT);

		// slot items
		slotHat			= new DataUserInt("slotHat");
		slotCloth		= new DataUserInt("slotCloth");
		slotShip		= new DataUserInt("slotShip");

		buyCount		= new DataUserInt("buyCount");

		//MakeNewData();
	}

	public void Clear()
	{
		id.Clear();

		coin.Clear();
		diamond.Clear();
		stage.Clear();

		// slot axe
		slotAxe.Clear();
		slotAxeLevel.Clear();
		slotAxeTime.Clear();

		// slot skills
		slotSkillAxe.Clear();
		slotSkillLevel.Clear();
		slotSkillTime.Clear();

		// slot gods
		slotGod.Clear();
		slotGodLevel.Clear();

		// slot items
		slotHat.Clear();
		slotCloth.Clear();
		slotShip.Clear();

		buyCount.Clear();

		id = null;

		coin = null;
		diamond = null;
		stage = null;

		// slot axe
		slotAxe = null;
		slotAxeLevel = null;
		slotAxeTime = null;

		// slot skills
		slotSkillAxe = null;
		slotSkillLevel = null;
		slotSkillTime = null;

		// slot gods
		slotGod = null;
		slotGodLevel = null;

		// slot items
		slotHat = null;
		slotCloth = null;
		slotShip = null;

		buyCount = null;
	}

	~DataUser()
	{
	}

	public void LoadData()
	{
		if (id.Get() == "") {
			MakeNewData ();
			return;
		}
	}

	private void MakeNewData()
	{
		PlayerPrefs.DeleteAll();

		id.Set(SystemInfo.deviceUniqueIdentifier);

		// TODO 빌드 설정으로 넘겨야 함
		SetDefaultLanguage();

		slotAxe.Set(1);
		slotAxeLevel.Set(1);
		slotHat.Set(1);
		slotCloth.Set(1);
		slotShip.Set(1);

		coin.Set(0);
		diamond.Set(0);
		stage.Set(1);

		buyCount.Set(1);

		PlayerPrefs.Save();


	}

	public void SetDefaultLanguage()
	{
		// TODO 빌드 설정으로 넘겨야 함
		language.Set("kr");
	}
	

}
