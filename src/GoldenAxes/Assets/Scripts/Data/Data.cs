﻿using System.Collections;
using UnityEngine;

public class Data : MonoBehaviour
{
	private enum PROGRESS {
		SETTING,
		PROPERTIES,
		DIFFICULTY,
		CHAPTER,
		AXE,
		AXE_MODEL,
		GOD,
		HAT,
		CLOTH,
		SHIP,
		TREASURE,
		MONSTER,
		ZONE,
		PATTERN,
		STRING,
		INVENTORY,
		USER,
		SERVER,
		END
	}

	public	UISprite		loadingIcon;
	public	UILabel			loadingLabel;
	public	UISprite		loadingProgressBack;
	public	UISprite		loadingProgressFore;
	public	UIProgressBar	loadingProgress;

	private PROGRESS m_load_index	= PROGRESS.SETTING;
	private PROGRESS m_load_max		= PROGRESS.END;

	[SerializeField]
	private	DataSetting			m_settings;
	private	DataProperties		m_properties;
	public	DataDifficulties	m_difficulties;
	private	DataChapters		m_chapters;
	
	private	DataAxes			m_axes;
	private DataAxeModels		m_axeModels;

	private	DataGods			m_gods;
	private	DataHats			m_hats;
	private	DataCloths			m_cloths;
	private	DataShips			m_ships;
	private DataTreasures		m_treasures;

	private	DataMonsters		m_monsters;
	[SerializeField]
	private	DataZones			m_zones;
	[SerializeField]
	private	DataPatterns		m_patterns;
	[SerializeField]
	private	DataStrings			m_strings;
	private DataInventory		m_inventory;
	[SerializeField]
	private	DataUser			m_user;
	[SerializeField]
	private	DataServer			m_server;


	// singleton
	private	static Data					instance;
	public	static DataSetting			settings		{ get { return Data.instance.m_settings; } }
	public	static DataProperties		properties		{ get { return Data.instance.m_properties; } }
	public	static DataDifficulties		difficulties	{ get { return Data.instance.m_difficulties; } }
	public	static DataChapters			chapters		{ get { return Data.instance.m_chapters; } }
	
	public	static DataAxes				axe				{ get { return Data.instance.m_axes; } }
	public	static DataAxeModels		axeModel		{ get { return Data.instance.m_axeModels; } }
	public	static DataGods				god				{ get { return Data.instance.m_gods; } }
	public	static DataHats				hat				{ get { return Data.instance.m_hats; } }
	public	static DataCloths			cloth			{ get { return Data.instance.m_cloths; } }
	public	static DataShips			ship			{ get { return Data.instance.m_ships; } }
	public	static DataTreasures		treasure		{ get { return Data.instance.m_treasures; } }
	public	static DataMonsters			monster			{ get { return Data.instance.m_monsters; } }
	public	static DataZones			zone			{ get { return Data.instance.m_zones; } }
	public	static DataPatterns			pattern			{ get { return Data.instance.m_patterns; } }
	public	static DataStrings			strings			{ get { return Data.instance.m_strings; } }
	public	static DataInventory		inventory		{ get { return Data.instance.m_inventory; } }
	public	static DataUser				user			{ get { return Data.instance.m_user; } }
	public	static DataServer			server			{ get { return Data.instance.m_server; } }


	void Awake () {
		Data.instance = this;

		DontDestroyOnLoad (gameObject);

		// start load
		m_load_index = PROGRESS.SETTING;
		StartCoroutine ("LoadData");

		//Color32 color = new Color(0.5F, 1, 0.5F, 1);
		//Debug.Log (JsonUtility.ToJson (color));
	}

	void OnDestory() {

		m_properties.Clear ();
		m_properties = null;

		m_user.Clear ();
		m_user = null;
	}


	IEnumerator LoadData()
	{
		string json;


		while (m_load_index < m_load_max) {
			loadingLabel.text = "LOADING " + "(" + (int)m_load_index + "/" + (int)m_load_max +  ")";
			loadingProgress.value = (float)m_load_index / (float)m_load_max;

			switch (m_load_index) {

			case PROGRESS.SETTING:
				json = Data.LoadJson ("Data/Setting");
				if (json == null) LoadError (m_load_index);

				m_settings = JsonUtility.FromJson<DataSetting>(json);
				break;

			case PROGRESS.PROPERTIES:
				m_properties = new DataProperties ();
				if (m_properties.LoadData () == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.DIFFICULTY:
				m_difficulties = new DataDifficulties();
				if (m_difficulties.LoadData ("Data/Difficulties") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.CHAPTER:
				m_chapters = new DataChapters();
				if (m_chapters.LoadData ("Data/Chapters") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.AXE:
				m_axes = new DataAxes();
				if (m_axes.LoadData ("Data/Axes") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.AXE_MODEL:
				m_axeModels = new DataAxeModels();
				if (m_axeModels.LoadData ("Data/AxeModels") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.GOD:
				m_gods = new DataGods();
				if (m_gods.LoadData ("Data/Gods") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.HAT:
				m_hats = new DataHats();
				if (m_hats.LoadData ("Data/Hats") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.CLOTH:
				m_cloths = new DataCloths();
				if (m_cloths.LoadData ("Data/Cloths") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.SHIP:
				m_ships = new DataShips();
				if (m_ships.LoadData ("Data/Ships") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.TREASURE:
				m_treasures = new DataTreasures();
				if (m_treasures.LoadData ("Data/Treasures") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.MONSTER:
				m_monsters = new DataMonsters();
				if (m_monsters.LoadData ("Data/Monsters") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.ZONE:
				json = Data.LoadJson ("Data/Zone");
				if (json == null) LoadError (m_load_index);

				m_zones = JsonUtility.FromJson<DataZones> (json);
				break;

			case PROGRESS.PATTERN:
				json = Data.LoadJson ("Data/Pattern");
				if (json == null) LoadError (m_load_index);

				m_patterns = JsonUtility.FromJson<DataPatterns> (json);
				break;

			case PROGRESS.STRING:
				m_strings = new DataStrings();
				if (m_strings.LoadData ("Data/Strings") == false) {
					LoadError (m_load_index);
				}
				break;

			case PROGRESS.INVENTORY:
				m_inventory = new DataInventory();
				break;

			case PROGRESS.USER:
				m_user = new DataUser ();
				m_user.LoadData ();
				break;

			case PROGRESS.SERVER:
				m_server = new DataServer ();
				m_server.LoadData ();
				break;

			case PROGRESS.END:
				break;
			}

			m_load_index++;
			yield return null;
		}
		loadingLabel.text = "LOADING " + "COMPLETE";
		loadingProgress.value = 1.0f;
		LoadComplete();
		StopCoroutine("LoadData");
	}

	private void LoadError(PROGRESS index)
	{
		Debug.Log("#ERROR!! : cannot load Stage Data : " + index);
		StopCoroutine ("LoadData");
	}

	private void LoadComplete()
	{
		TweenAlpha.Begin(loadingProgressFore.gameObject, 0.2f, 0.0f).method = UITweener.Method.EaseIn;
		TweenAlpha.Begin(loadingProgressBack.gameObject, 0.2f, 0.0f).method = UITweener.Method.EaseIn;
		TweenAlpha.Begin(loadingIcon.gameObject, 0.2f, 0.0f).method = UITweener.Method.EaseIn;
		TweenAlpha tween = TweenAlpha.Begin(loadingLabel.gameObject, 0.2f, 0.0f);
		tween.onFinished.Add(new EventDelegate(this, "OnTransitionComplete"));
	}
    private void OnTransitionComplete(UITweener tween)
    {
		UnityEngine.SceneManagement.SceneManager.LoadScene ("SCGamePlay");
    }

	public static string LoadJson(string url)
	{
		// 나중에 암호화 하자
		TextAsset bindata = Resources.Load(url) as TextAsset;

		if (bindata == null)
			return null;

		return bindata.text;
	}

	public void Remove()
	{

	}
}
