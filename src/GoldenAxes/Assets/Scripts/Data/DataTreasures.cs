﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class DataTreasure {
	public	int			id				{ private set; get; }
	public	int			grade			{ private set; get; }
	public	string		icon			{ private set; get; }
	public	string		name			{ private set; get; }
	public	string		info			{ private set; get; }
	public	string		passive			{ private set; get; }
	private	float		param1, param2, param3;
	private	float		upgrade1, upgrade2, upgrade3;

	public DataTreasure()
	{
	}

	~DataTreasure()
	{
	}

	public bool Parse(XmlElement xml)
	{
		id = (xml.HasAttribute("id"))? int.Parse(xml.GetAttribute("id")) : 0;
		grade = (xml.HasAttribute("grade"))? int.Parse(xml.GetAttribute("grade")) : 0;
		icon = (xml.HasAttribute("icon"))? xml.GetAttribute("icon") : "";
		name = (xml.HasAttribute("name"))? xml.GetAttribute("name") : "";
		info = (xml.HasAttribute("info"))? xml.GetAttribute("info") : "";

		passive = (xml.HasAttribute("passive"))? xml.GetAttribute("passive") : "";

		param1 = (xml.HasAttribute("param1"))? float.Parse(xml.GetAttribute("param1")) :0f;
		param2 = (xml.HasAttribute("param2"))? float.Parse(xml.GetAttribute("param2")) :0f;
		param3 = (xml.HasAttribute("param3"))? float.Parse(xml.GetAttribute("param3")) :0f;

		upgrade1 = (xml.HasAttribute("upgrade1"))? float.Parse(xml.GetAttribute("upgrade1")) :0f;
		upgrade2 = (xml.HasAttribute("upgrade2"))? float.Parse(xml.GetAttribute("upgrade2")) :0f;
		upgrade3 = (xml.HasAttribute("upgrade3"))? float.Parse(xml.GetAttribute("upgrade3")) :0f;

		return true;
	}

	public float GetParam1(int level)
	{
		return param1 + upgrade1 * level;
	}
	public float GetParam2(int level)
	{
		return param2 + upgrade2 * level;
	}
	public float GetParam3(int level)
	{
		return param3 + upgrade3 * level;
	}

}


public class DataTreasureProperties {

	public float upgradeBase			{ private set; get; }
	public float upgradeMultiplier		{ private set; get; }
	public float minimumStage			{ private set; get; }
	public float stageMinus				{ private set; get; }
	public float stageDivision			{ private set; get; }
	public float prestigeMultiplier		{ private set; get; }

	public DataTreasureProperties()
	{
	}

	~DataTreasureProperties()
	{
	}
	
	public void Clear()
	{
		
	}

	public bool Parse(XmlElement xml)
	{
		upgradeBase = float.Parse(xml.GetAttribute("upgradeBase"));
		upgradeMultiplier = float.Parse(xml.GetAttribute("upgradeMultiplier"));
		minimumStage = float.Parse(xml.GetAttribute("minimumStage"));
		stageMinus = float.Parse(xml.GetAttribute("stageMinus"));
		stageDivision = float.Parse(xml.GetAttribute("stageDivision"));
		prestigeMultiplier = float.Parse(xml.GetAttribute("prestigeMultiplier"));

		return true;
	}
}

public class DataTreasures {
	private Dictionary<int, DataTreasure>	m_treasures;
	private DataTreasureProperties			m_properties;
	public DataTreasureProperties				properties	{ get { return m_properties; } }

	public DataTreasures()
	{
		m_treasures = new Dictionary<int, DataTreasure>();
		m_properties = new DataTreasureProperties();
	}

	~DataTreasures()
	{
		m_treasures.Clear();
		m_treasures = null;

		m_properties.Clear();
		m_properties = null;
	}

	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml["TREASURE"]);
	}

	private bool ParseData(XmlNode xml)
	{
		int id;
		DataTreasure data;
		foreach (XmlElement xmlTreasure in xml["TREASURES"].ChildNodes)
		{
			id = int.Parse(xmlTreasure.GetAttribute("id"));

			data = new DataTreasure();
			data.Parse(xmlTreasure);
			m_treasures[id] = data;
		}

		m_properties.Parse(xml["PROPERTIES"]);


		return true;
	}

	public DataTreasure GetData(int treasure_id)
	{
		if(m_treasures.ContainsKey(treasure_id))
		{
			return m_treasures[treasure_id];
		}
		else
		{
			return null;
		}
	}

	public int GetTotalCount()
	{
		return m_treasures.Count;
	}
}