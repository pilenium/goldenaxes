﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class DataSetting {

	public	float			BG_SCROLL_TOP_GAP;
	public	float			BG_SCROLL_BOTTOM_GAP;

}

