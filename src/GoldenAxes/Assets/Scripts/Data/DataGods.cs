﻿using System.Xml;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;

public class DataGod {
	public	string			type				{ private set; get; }
	public	int				grade				{ private set; get; }
	public	string			icon				{ private set; get; }
	public	string			iconSmall			{ get { return icon + "s"; } }
	public	string			name				{ private set; get; }
	public	string			info				{ private set; get; }
	public	string			skill_name			{ private set; get; }
	public	string			skill_info			{ private set; get; }
	public	string			skill_icon			{ private set; get; }
	public	string			skill_iconSmall		{ get { return skill_icon + "s"; } }
	public	string			skill_type			{ private set; get; }
	public	int				model				{ private set; get; }
	public	float			coolTime			{ private set; get; }
	public	float			activeTime			{ private set; get; }
	private	float			param1, param2, param3;
	private	float			upgrade1, upgrade2, upgrade3;

	public bool Parse(XmlElement xml)
	{
		type = (xml.HasAttribute("type"))? xml.GetAttribute("type") : "";
		grade = (xml.HasAttribute("grade"))? int.Parse(xml.GetAttribute("grade")) : 0;
		icon = (xml.HasAttribute("icon"))? xml.GetAttribute("icon") : "";
		name = (xml.HasAttribute("name"))? xml.GetAttribute("name") : "";
		info = (xml.HasAttribute("info"))? xml.GetAttribute("info") : "";
		skill_name = (xml.HasAttribute("skill_name"))? xml.GetAttribute("skill_name") : null;
		skill_info = (xml.HasAttribute("skill_info"))? xml.GetAttribute("skill_info") : null;
		skill_icon = (xml.HasAttribute("skill_icon"))? xml.GetAttribute("skill_icon") : null;
		skill_type = (xml.HasAttribute("skill_type"))? xml.GetAttribute("skill_type") : null;

		model = (xml.HasAttribute("model"))? int.Parse(xml.GetAttribute("model")) : 0;

		coolTime = (xml.HasAttribute("coolTime"))? float.Parse(xml.GetAttribute("coolTime")) : 0f;
		activeTime = (xml.HasAttribute("activeTime"))? float.Parse(xml.GetAttribute("activeTime")) :0f;

		param1 = (xml.HasAttribute("param1"))? float.Parse(xml.GetAttribute("param1")) :0f;
		param2 = (xml.HasAttribute("param2"))? float.Parse(xml.GetAttribute("param2")) :0f;
		param3 = (xml.HasAttribute("param3"))? float.Parse(xml.GetAttribute("param3")) :0f;

		upgrade1 = (xml.HasAttribute("upgrade1"))? float.Parse(xml.GetAttribute("upgrade1")) :0f;
		upgrade2 = (xml.HasAttribute("upgrade2"))? float.Parse(xml.GetAttribute("upgrade2")) :0f;
		upgrade3 = (xml.HasAttribute("upgrade3"))? float.Parse(xml.GetAttribute("upgrade3")) :0f;

		return true;
	}

	public float GetParam1(int level)
	{
		return param1 + upgrade1 * level;
	}
	public float GetParam2(int level)
	{
		return param2 + upgrade2 * level;
	}
	public float GetParam3(int level)
	{
		return param3 + upgrade3 * level;
	}
}


public class DataGodSlot {
	public Vector3	position		{ private set; get; }
	public bool Parse(XmlElement xml)
	{
		float pos_x = (xml.HasAttribute("pos_x"))? float.Parse(xml.GetAttribute("pos_x")) : 0f;
		float pos_y = (xml.HasAttribute("pos_y"))? float.Parse(xml.GetAttribute("pos_y")) : 0f;
		float pos_z = (xml.HasAttribute("pos_z"))? float.Parse(xml.GetAttribute("pos_z")) : 0f;
		position = new Vector3(pos_x, pos_y, pos_z);

		return true;
	}
}


public class DataGods {

	private Dictionary<int, DataGod>		m_gods;
	private Dictionary<int, DataGodSlot>	m_slots;

	
	public DataGods()
	{
		m_gods = new Dictionary<int, DataGod>();
		m_slots = new Dictionary<int, DataGodSlot>();
	}
	~DataGods()
	{
		m_gods.Clear();
		m_gods = null;

		m_slots.Clear();
		m_slots = null;
	}

	public bool LoadData(string url)
	{
		TextAsset xmlData = (TextAsset)Resources.Load(url);

		if (xmlData == null) {
			UIPopup.Create ("LOAD_ERROR", url);
			return false;
		}

		XmlDocument xml = new XmlDocument();
		xml.LoadXml(xmlData.text);

		return ParseData(xml["GOD"]);
	}
	private bool ParseData(XmlNode xml)
	{
		// 사이즈를 먼저 파싱 (챕터에 사이즈가 들어가야함)
		DataGod dataGod;
		int id;
		foreach (XmlElement xmlSize in xml["GODS"].ChildNodes)
		{
			id = int.Parse(xmlSize.GetAttribute("id"));

			dataGod = new DataGod();
			dataGod.Parse(xmlSize);
			m_gods[id] = dataGod;
		}

		DataGodSlot dataSlot;
		int index;
		foreach (XmlElement xmlChapter in xml["GOD_SLOTS"].ChildNodes)
		{
			index = int.Parse(xmlChapter.GetAttribute("index"));

			dataSlot = new DataGodSlot();
			dataSlot.Parse(xmlChapter);
			m_slots[index] = dataSlot;
		}

		return true;
	}

	public DataGod GetGodData(int id)
	{
		return m_gods[id];
	}

	public DataGodSlot GetSlotData(int index)
	{
		return m_slots[index];
	}

	public int GetTotalCount()
	{
		return m_gods.Count;
	}
}
