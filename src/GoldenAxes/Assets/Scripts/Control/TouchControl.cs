﻿using UnityEngine;
using System.Collections;

public class TouchControl : Control {
	
	public override void EndTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
		RaycastHit hitInfo;

		if (GameUtil.ScreenToStage(position, out hitInfo)) {

			Vector3 target_pos = GameUtil.CheckStageBound (new Vector3 (hitInfo.point.x, 0.0f, hitInfo.point.z), GamePlay.instance.dataChapter.stage_width, GamePlay.instance.dataChapter.stage_height);

			Target (target_pos);
		}
	}
}
