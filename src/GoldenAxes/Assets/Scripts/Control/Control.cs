﻿using UnityEngine;
using System.Collections;

public class Control
{
	protected int MAX_AXES = 2;

	public delegate void OnTarget (Vector3 position);

	protected OnTarget	m_onTarget;

	protected virtual void Init()
	{
	}

	public virtual void Reset()
	{

	}

	public virtual void Clear()
	{
		RemoveTargetListener ();
	}

	public virtual void StartTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
	}
	
	public virtual void MovedTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
	}
	
	public virtual void EndTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
	}
	
	public virtual void CanceldTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
	}

	protected virtual void Update ()
	{
	}

	public virtual void Swipe(float delta_x, float delta_y)
	{

	}
	public virtual void Swipe(Vector2 deltaPosition)
	{

	}

	public void AddTargetListener(OnTarget del)
	{
		m_onTarget = del;
	}


	public void RemoveTargetListener()
	{
		m_onTarget = null;
	}


	protected virtual void Target(Vector3 position)
	{
		if (m_onTarget != null) {
			m_onTarget (position);
		}
	}

}
