﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SwipeControl : Control {

	private float		maxHeight	= 0.01f;		// input height per screen
	private float		powerDecrease;
	private float[]		powers;

	// Use this for initialization
	protected override void Init()
	{
		maxHeight = Screen.height * maxHeight;

		powerDecrease = Screen.height * 0.001f;

		powers = new float[5];
	}



	public override void StartTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
		if (id >= powers.Length) return;

		powers [id] = 0f;
	}
	
	public override void MovedTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
		if (id >= powers.Length) return;

		if (deltaPosition.y > 0)
		{
			powers [id] += deltaPosition.y;
		}
		else
		{
			powers [id] -= powerDecrease;
		}
//		powers [id] += deltaPosition.y;
		
		//Debug.Log ("powers [id]:" + powers [id]);
	}
	
	public override void EndTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
		if (id >= powers.Length) return;



		float width = position.x / Screen.width;
		float height = powers [id] / maxHeight;

		if (height < 0.0f)
		{
			height = 0.0f;
		}
		if (height > 1f) {
			height = 1;
		}
		
		throwAxe(width, height);
	}
	
	public override void CanceldTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
		if (id >= powers.Length) return;
	}

	private void throwAxe(float position, float power)
	{
	}

}
