﻿using UnityEngine;
using System.Collections;

public class Common  {

	// game condition
	public static int			MAX_TOUCH_THROW_COUNT	= 1;
	public static int			MAX_TOUCH_COUNT			= 2;

	public static int			MAX_SKILL_SLOT			= 4;
	public static int			MAX_GOD_SLOT			= 4;

	public static int			MAX_STACK				= 100;

	public	static float		SPEED_INTRO_NEXT		= 30f;
	public	static float		SPEED_INTRO_PREV		= -30f;

	// static value
	public static float			POS_SURFACE_EFFECT_Y	=  0.01f;
	public static float			POS_SURFACE_Y			=  0.00f;
	public static float			POS_MONSTER_Y			= -0.01f;
	public static float			POS_EFFECT_Y			= -0.02f;
	public static float			POS_ZONE_Y				= -0.03f;
	public static float			POS_WAVE_Y				= -0.04f;
	public static float			POS_DIVER_Y				= -0.05f;
	public static float			POS_WATER_Y				= -0.06f;
	public static float			POS_COIN_Y				= -1.00f;


	// screen & stage
	public static float			BASE_RATIO				= 2f;
	public static float			SCREEN_RATIO			= 2f;
	public static float			FOV_RAD					= 0f;
	public static float			FOV_RAD_HALF			= 0f;


	public static float			UI_INTRO_ANIMATION_TIME	= 0.8f;

	public static Vector3		SCALE_COIN				= Vector3.one * 0.7f;

	public static float			SIZE_OF_DIVER			= 2.0f;
	public static float			GAP_OF_DIVER			= 1.0f;

	public static string		TAG_MONSTER				= "MONSTER";
	public static string		TAG_ZONE				= "ZONE";
	public static string		TAG_AXE					= "AXE";

	public static string		LAYER_WATER				= "Water";


	public static Vector3		WOODMAN_BEGIN			= new Vector3(0.0f, 1.0f, -3.0f);
	public static Vector3		WOODMAN_POSITION		= new Vector3 (0.0f, 0.0f, 2.0f);
	public static float			SHIP_RADIUS				= 1.5f;
	public static float			ATTACK_DISTANCE			= 1.0f;
	public static Vector3		THROW_POSITION			= new Vector3 (0.0f, 2.0f, 1.4f);

	//public static float			MONSTER_RESPAWN_MIN_Z	= 0.85f;
	//public static float			MONSTER_RESPAWN_MAX_Z	= 0.95f;
	public static float			MONSTER_RESPAWN_MIN_Z	= 0.85f;
	public static float			MONSTER_RESPAWN_MAX_Z	= 0.95f;
	public static float			BOSS_RESPAWN_Z			= 0.95f;


	public static int			UI_EFFECT_DEPTH			= 100;
	public static int			UI_TOP_DEPTH			= 200;
	public static int			UI_AXE_DEPTH			= 300;
	public static int			UI_MANAGEMENT_DEPTH		= 400;
	public static int			UI_BOTTOM_DEPTH			= 500;

	public static int			UI_PANEL_BASE_DEPTH			= 1;
	public static int			UI_PANEL_BOTTOM_DEPTH		= 2;
	public static int			UI_PANEL_MANAGEMENT_DEPTH	= 3;
	public static int			UI_PANEL_POPUP_BASE_DEPTH	= 3;

	// direction
	public enum DIRECTION
	{
		E,
		NE,
		N,
		NW,
		W,
		SW,
		S,
		SE
	}

	public static DIRECTION	ConvertDirection(string direction)
	{
		switch (direction) {
		case "E":
			return DIRECTION.E;
		case "NE":
			return DIRECTION.NE;
		case "N":
			return DIRECTION.N;
		case "NW":
			return DIRECTION.NW;
		case "W":
			return DIRECTION.W;
		case "SW":
			return DIRECTION.SW;
		case "S":
			return DIRECTION.S;
		case "SE":
			return DIRECTION.SE;
		}
		return DIRECTION.E;
	}

	public static bool IsEast(DIRECTION direction)
	{
		return !(DIRECTION.N < direction && direction < DIRECTION.S);
	}
	public static bool IsNorth(DIRECTION direction)
	{
		return DIRECTION.E < direction && direction < DIRECTION.W;
	}

}