﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;
using PGTool;

/// <summary>
/// 캐릭터의 기본 기능 (리소스 상속)
/// AI Class
/// Move Class
/// </summary>
public class Character : Resource {

	protected	Transform	m_tr;
	public		Vector3		position		{ get { return m_tr.position; } }
//	public		float		speed			{ get ; protected set; }

	// navi
	public		NavMeshAgent			m_agent = null;


	public override void Awake() {
		base.Awake();
		// init basic value
		m_tr = GetComponent<Transform> ();

	//	speed = 0.0f;
	}

	public virtual void OnDestroy()
	{
		m_tr = null;
	}

	// nav mesh agent
	protected void InitNavMeshAgent(float radius, float speed)
	{
		if(m_agent == null)
		{
			m_agent = gameObject.AddComponent<NavMeshAgent>() as NavMeshAgent;

			m_agent.autoTraverseOffMeshLink = false;
			m_agent.obstacleAvoidanceType = ObstacleAvoidanceType.MedQualityObstacleAvoidance;
		}
		m_agent.ResetPath();
		m_agent.radius = radius;
		m_agent.speed = speed;
		m_agent.acceleration = 20;
	}

	public void SetDestination(Vector3 destination)
	{
		m_agent.SetDestination(destination);
	}

	public void RemoveDestination()
	{
		m_agent.ResetPath();
	}

	public float GetSpeed()
	{
		float ret = 0f;

		if(m_agent != null && m_agent.hasPath)
		{
			ret = m_agent.speed;
		}

		return ret;
	}

	public void SetSpeed(float speed)
	{
		if(m_agent != null && m_agent.hasPath)
		{
			m_agent.speed = speed;
		}
	}

	protected virtual void EndDestination()
	{
		RemoveDestination();
	}

	public virtual void Update()
	{
		if(m_agent != null)
		{
			if (m_agent.hasPath && m_agent.remainingDistance < m_agent.radius) {
				EndDestination();
			}
		}
	}


	public virtual void SetAnimation(string name, string next = null)
	{
	}

	public virtual Vector3 DeltaPosition(float dt)
	{
		return m_agent.velocity * dt;
	}
}
