﻿using UnityEngine;
using System.Collections;


public class Woodman : MonoBehaviour {

	public static Woodman Alloc()
	{
		GameObject obj = new GameObject();
		Woodman ret = obj.AddComponent<Woodman>() as Woodman;
		return ret;
	}

	private Transform		m_tr;
	private Rigidbody		m_rigidBody = null;

	private TweenPosition	m_tweenPos		= null;
	private EventDelegate	m_tweenPosDel	= null;

	private	WoodmanModel	m_model;
	private	Transform		m_model_tr;
	private	WoodmanShip		m_ship = null;
	private Transform		m_ship_tr;

	private GameObject		m_effectBuff	= null;


	// Use this for initialization
	void Awake () {
		name = "Woodman";
		m_tr = GetComponent<Transform> ();
		InitModel();
		m_tr.localPosition = Common.WOODMAN_POSITION;

		//RVO.Simulator.Instance.AddAgent(new Vector2(Common.WOODMAN_POSITION.x, Common.WOODMAN_POSITION.z), 1.0f, 10, 1.0f, 0.0f, Vector2.zero);
	}
	
	// Update is called once per frame
	void Update () {
/*
		float posy = Mathf.Sin (Time.deltaTime * 2) * 0.1f;
		m_tr.transform.localPosition = new Vector3 (0.0f, posy, 0.0f);
		*/


		m_tr.rotation = Quaternion.Lerp(m_tr.rotation, Quaternion.Euler(0.0f, 0.0f, 0.0f), Time.deltaTime * 2.0f);

		m_model_tr.rotation = Quaternion.Lerp(m_model_tr.rotation, Quaternion.Euler(20.0f, 0.0f, 0.0f), Time.deltaTime * 4.0f);
	}

	private void InitModel()
	{
		GameObject gameObj = PrefabManager.LoadPrefab("Prefaps/Woodman/WoodmanBase");
	//	m_model_tr = gameObject.GetComponent<Transform>();
		m_model_tr = gameObj.transform;
		m_model_tr.parent = m_tr;
	//	m_model_tr.localPosition = Vector3.zero;
		m_model = gameObj.GetComponent<WoodmanModel>();

		// 리지드 바디 설정해주자.
		if (m_rigidBody == null) {
			m_rigidBody = gameObject.AddComponent<Rigidbody> () as Rigidbody;
			m_rigidBody.useGravity = false;
			m_rigidBody.isKinematic = true;
		}
	}


	// axe
	public void SetAxe(int modelID)
	{
		m_model.SetAxe (modelID);
	}

	// hat
	public void SetHat(int hat_id)
	{
		DataHat data = Data.hat.GetData(hat_id);
		m_model.SetHair(data.mesh, data.mat);

	}

	// cloth
	public void SetCloth(int cloth_id)
	{
		DataCloth data = Data.cloth.GetData(cloth_id);
		m_model.SetBody(data.mesh, data.mat);
	}

	// ship
	public void SetShip(int ship_id)
	{
		DataShip data = Data.ship.GetData(ship_id);

		if(data == null)
		{
			// ERROR
		}

		if(m_ship != null)
		{
			GameObject.Destroy(m_ship.gameObject);
		}

		GameObject gameObj = PrefabManager.LoadPrefab(data.prefab);
		m_ship_tr = gameObj.GetComponent<Transform> ();
		m_ship_tr.parent = m_tr;
		m_ship_tr.localPosition = Vector3.zero;

		m_ship = gameObj.AddComponent<WoodmanShip>();

		m_model_tr.localPosition = data.ride_position;
	}


	// buff
	public void SetBuff (string prefab)
	{
		RemoveBuff ();

		m_effectBuff = PrefabManager.LoadPrefab (prefab) as GameObject;


		m_effectBuff.transform.parent = m_tr;
		m_effectBuff.transform.localPosition = Vector3.zero;
		m_effectBuff.transform.localRotation = Quaternion.identity;
		/*
		m_effectBuff.transform.Rotate (new Vector3 (-60.0f, 0.0f, 0.0f));
		*/

	}
	public void RemoveBuff()
	{
		if (m_effectBuff != null) {
			GameObject.Destroy (m_effectBuff);
			m_effectBuff = null;
		}
	}


	// animation

	public void EquipAxe (int index)
	{
		/*
		if (index == 0) {
			m_model.SetAnimation (WOODMAN_ANI.EQUIP_LEFT);
		} else {
			m_model.SetAnimation (WOODMAN_ANI.EQUIP_RIGHT);

		}
		*/

	}

	public void ThrowAxe (float rotation_Y = 0.0f)
	{
		m_model.SetAnimation (WOODMAN_ANI.THROW);
		m_model_tr.localRotation = Quaternion.Euler (20.0f, Mathf.Rad2Deg * rotation_Y, 0.0f);
	}

	public void SetAnimation(WOODMAN_ANI animation)
	{
		m_model.SetAnimation (animation);
	}

	public void Attack()
	{
		m_tr.localRotation = Quaternion.Euler(-12f, 0f, 0f);
	}

	//  움직임 연출 함수들
/*
	// move
	public void Move(Vector3 position, bool smooth = true, float time = 0.3f, UITweener.Method method = UITweener.Method.EaseOut)
	{
		if (m_tweenPos != null && m_tweenPosDel != null) {
			m_tweenPos.RemoveOnFinished (m_tweenPosDel);
		}
		if (smooth) {
			m_tweenPos = TweenPosition.Begin (gameObject, time, position) as TweenPosition;
			m_tweenPos.method = method;
		} else {
			m_tr.position = position;
		}
	}
	public void Appear ()
	{
		m_tr.position = Common.WOODMAN_POSITION;
	}

/*
	public void MoveIntro()
	{
		if (m_tweenPos != null && m_tweenPosDel != null) {
			m_tweenPos.RemoveOnFinished (m_tweenPosDel);
		}

		m_tweenPos = TweenPosition.Begin (gameObject, 1.0f, new Vector3 (0.0f, 10.0f, 3.2f));
		m_tweenPos.method = UITweener.Method.EaseOut;
		m_tweenPosDel = new EventDelegate (this, "MoveIntro2");
		m_tweenPos.AddOnFinished (m_tweenPosDel);
	}

	private void MoveIntro2()
	{
		m_tweenPos.RemoveOnFinished (m_tweenPosDel);
		TweenPosition.Begin (GamePlay.instance.woodman.gameObject, 1.0f, new Vector3 (0.0f, 10.0f, 2.5f)).method = UITweener.Method.EaseInOut;
	}
	*/
}
