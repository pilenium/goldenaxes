﻿using UnityEngine;
using System.Collections;

public class WoodmanCloud : MonoBehaviour {

	private TransformBillBoard[]	billBoards;

	void Awake()
	{
		billBoards = GetComponentsInChildren<TransformBillBoard> ();
	}

	public void UpdateDirection()
	{
		// Equip the item visually
		for (int i = 0 ; i < billBoards.Length; ++i) {
			billBoards [i].UpdateDirection ();
		}

	}
}
