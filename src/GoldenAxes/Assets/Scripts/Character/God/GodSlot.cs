﻿using System;
using System.Collections;

public class GodSlot
{
	public static GodSlot Create(int index)
	{
		GodSlot ret = new GodSlot (index);
		return ret;
	}

	protected	int				m_index;
	protected	God				m_god			= null;

	// getter
	public		int				level			{ get ; protected set; }
	public		double			dmg				{ get ; protected set; }
	public		bool			hasGod			{ get { return m_god != null; } }
	public		DataGodSlot		dataSlot		{ get; protected set; }
	public		ItemGod			itemGod			{ get { if (m_god == null) { return null; } else { return m_god.itemGod; } } }


	public GodSlot (int index)
	{
		m_index = index;
		RefreshProperties();
		dataSlot = Data.god.GetSlotData(index);
	}

	public virtual void Clear()
	{
		m_god = null;
		ResetGod ();
	}


	public virtual bool SetGod(int god_id)
	{
		// 널이면 만들어주고 아님 바로 셋
		if (m_god == null) {
			m_god = PGTool.ResourceManager.instance.Get<God> ("God"+m_index) as God;
			m_god.transform.localPosition = dataSlot.position;
		}

		m_god.SetGod(god_id, this);
		//newGod.Init (m_tr.position, GamePlay.instance.stage.speed);

		return true;
	}

	public virtual void ResetGod()
	{
		if (m_god != null) {
			PGTool.ResourceManager.instance.Return (m_god);
		}
		// delete god
	}

	public virtual void RefreshProperties()
	{
		level = Data.user.slotGodLevel.Get(m_index);
		dmg = (level == 0)? 0 : GamePlay.instance.properties.GetGodDamage(m_index, level);
	}

	public virtual void CheckMonster()
	{
		if (m_god != null) {
			m_god.CheckMonster();
		}
	}

	public virtual void Update(float dt)
	{
		if (m_god != null) {
			m_god.UpdateCharacter(dt);
		}
	}

}
