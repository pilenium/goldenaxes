﻿using UnityEngine;
using System.Collections;

public class God : Character {

	protected	GodModel				m_model = null;

	protected	Transform				m_god_tr		= null;
	protected	Animation				m_god_ani		= null;
	protected	Material				m_god_wave_mat	= null;
	protected	Rigidbody				m_rigidBody		= null;

	protected	TransfromTexMove		m_moveFlotsam	= null;
	protected	EffectColor				m_effectFlotsam	= null;

	public		ItemGod					itemGod				{ get; protected set; }
	public		DataAxeModel			dataAxeModel		{ get; protected set; }
	public		GodSlot					slot				{ get ; protected set; }


	protected	float					m_time = 0.0f;
	protected	float					cool_time = 1.0f;
	protected	Monster					m_targetMonster = null;

	public override void Awake() 
	{
		base.Awake ();
		InitModel ();

		m_time = 0.0f;
		m_targetMonster = null;
	}

	public override void OnDestroy()
	{
		base.OnDestroy ();
		itemGod = null;
		slot = null;
		dataAxeModel = null;
	}

	public void UpdateCharacter(float dt)
	{
		if (m_time < cool_time) {
			m_time += dt;
		} else {
			if (m_targetMonster != null) {
				ThrowAxe();
				m_time = 0.0f;
			}
		}


		if (m_targetMonster != null) {

//			Quaternion.LookRotation (m_targetMonster.position - m_tr.position);

			m_tr.rotation = Quaternion.Lerp(m_tr.rotation, Quaternion.LookRotation (m_targetMonster.position - m_tr.position), Time.deltaTime * 2.0f);
		
//			m_tr.localRotation = Quaternion.Euler (0.0f, Mathf.Rad2Deg * rotation_Y, 0.0f);
		} else {
			m_tr.rotation = Quaternion.Lerp(m_tr.rotation, Quaternion.Euler(0.0f, 0.0f, 0.0f), Time.deltaTime * 2.0f);
			CheckMonster();
		}
	}
	private void InitModel()
	{
		// 모델 불러오고
		GameObject gameObj = PrefabManager.LoadPrefab ("Prefaps/God/GodBase");
		m_model = gameObj.GetComponent<GodModel> ();
		m_model.transform.parent = m_tr;
		m_model.transform.rotation = Quaternion.Euler(10.0f, 0.0f, 0.0f);


		// 부유 움직임 이펙트 (풀 안에 있는거니깐 풀을 사용 하지 말자)
		if (m_effectFlotsam == null) {
			GameObject nObj = GameObject.Instantiate (Resources.Load("Prefaps/Effect/Effect_FlotsamMoveM")) as GameObject;
			m_moveFlotsam = nObj.AddComponent<TransfromTexMove> () as TransfromTexMove;
			m_moveFlotsam.SpeedX = 0.0f;
			m_moveFlotsam.SpeedY = - GamePlay.instance.dataChapter.speed.battle * 0.005f;

			m_effectFlotsam = nObj.AddComponent<EffectColor> () as EffectColor;
			m_effectFlotsam.transform.parent = m_tr;
			m_effectFlotsam.transform.localPosition = Vector3.up * Common.POS_SURFACE_EFFECT_Y;
			m_effectFlotsam.transform.localRotation = Quaternion.Euler (Vector3.left * 90);
			//m_effectFlotsam.transform.localScale = Vector3.one * m_data.scale;
		}

		// 리지드 바디 설정해주자.
		if (m_rigidBody == null) {
			m_rigidBody = gameObject.AddComponent<Rigidbody> () as Rigidbody;
			m_rigidBody.useGravity = false;
			m_rigidBody.isKinematic = true;
		}
	}

	public void SetGod(int god_id, GodSlot godSlot)
	{
		itemGod = Data.inventory.GetGod(god_id);

		if (itemGod == null) {
			// ERROR
			return;
		}

		cool_time = 2.0f + Random.value;

		m_model.SetAxe(itemGod.data.model);

		dataAxeModel = Data.axeModel.GetData(itemGod.data.model);

		if (dataAxeModel == null) {
			// ERROR
			return;
		}
		// TODO 그담에 옷 입혀야함

		slot = godSlot;

	}

	protected virtual void ThrowAxe ()
	{
		Vector3 throwPos = position;
		throwPos.y = 2.0f;

		float original_time = AxeCalculator.CalculateTime (throwPos, m_targetMonster.position, dataAxeModel.gravity, dataAxeModel.cos, dataAxeModel.sin);
		Vector3 targetPos = m_targetMonster.position + m_targetMonster.DeltaPosition (original_time);

		// 도끼 던지기

		Axe axe = Axe.Create(itemGod.data.model, itemGod.data.skill_type, itemGod.param1, itemGod.param2, itemGod.param3);
		// 이 부분 데미지를 산신령 속성으로 바꿔야함
		axe.Init(itemGod.data.model, slot.dmg, throwPos, targetPos); 
		GamePlay.instance.stage.AddAxe(axe);

		m_model.SetAnimation(GOD_ANI.THROW);
	}

	public virtual void CheckMonster()
	{
		if (m_targetMonster == null) {
			m_targetMonster = GamePlay.instance.monsterManager.GetEmptyMonster();
			if (m_targetMonster != null) {
				m_targetMonster.eventDie.AddListener(OnMonsterDie);
				m_targetMonster.eventDisappear.AddListener(OnMonsterDisappear);
			}
		}
	}

	public virtual void RemoveMonster()
	{
		m_targetMonster = null;
	}

	protected void OnMonsterDie()
	{
		m_targetMonster.eventDie.RemoveListener(OnMonsterDie);
		m_targetMonster = null;
	}
	protected void OnMonsterDisappear()
	{
		m_targetMonster.eventDisappear.RemoveListener(OnMonsterDisappear);
		m_targetMonster = null;
	}
}
