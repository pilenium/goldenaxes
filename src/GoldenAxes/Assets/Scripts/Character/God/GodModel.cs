﻿using UnityEngine;
using System.Collections;

public enum GOD_ANI {
	IDLE,
	BATTLE_IDLE,
	THROW
}


public class GodModel : MonoBehaviour {

	private Animation		m_ani = null;

	private AxeModel		m_axe = null;
	private Transform		m_tr = null;
	private Transform		m_mount = null;
	//	private Transform		m_mount0 = null;
	//	private Transform		m_mount1 = null;

	// Use this for initialization
	void Awake () {
		// 애니메이션 가져오고
		m_ani = GetComponent<Animation> ();

		m_tr = GetComponent<Transform> ();

		m_mount = m_tr.Find ("GodArmature").Find ("Pelvis").Find ("Right_Upper_Arm").Find ("Right_Lower_Arm").Find ("Right_Hand").Find ("Mount");
		//	m_mount0 = m_tr.Find ("WoodmanArmature").Find ("Torso").Find ("Chest").Find ("Upper_Arm_L").Find ("Lower_Arm_L").Find ("Hand_L").Find ("Axe_Mount0");
		//	m_mount1 = m_tr.Find ("WoodmanArmature").Find ("Torso").Find ("Chest").Find ("Upper_Arm_R").Find ("Lower_Arm_R").Find ("Hand_R").Find ("Axe_Mount1");



		SetAnimation (GOD_ANI.IDLE);
	}

	public void SetAnimation(GOD_ANI state)
	{
		switch (state) {
		case GOD_ANI.IDLE:
			m_ani.CrossFade ("IDLE", 0.2f);
			break;

		case GOD_ANI.THROW:
			m_ani.Stop();
			m_ani.Play("THROW");
			m_ani.PlayQueued ("IDLE");
			break;

		}


	}

	// axe
	public void SetAxe(int modelID)
	{
		DataAxeModel dataAxeModel = Data.axeModel.GetData(modelID);

		if (dataAxeModel == null) {
			// ERROR
		}

		if (m_axe != null) {
			DestroyObject (m_axe.gameObject);
		}

		// make axe;
		m_axe = AxeModel.Create(dataAxeModel.prefab);
		Transform axe_tr = m_axe.GetComponent<Transform>() as Transform;
		axe_tr.parent = m_mount;


		axe_tr.localPosition = dataAxeModel.grip_pos;
		axe_tr.localScale = Vector3.one;
		axe_tr.localRotation = Quaternion.Euler (0.0f, 0.0f, 0.0f);
	}

	public void SetCloth(string url, string part, string material_url = null)
	{




		//Load mesh from source object

		GameObject sourceObject = Resources.Load(url) as GameObject;

		Mesh mSource = sourceObject.GetComponentInChildren<SkinnedMeshRenderer>().sharedMesh;

		//Assign to final object



		GameObject clothSlotObject = gameObject.transform.Find(part).gameObject;


		//DestroyObject (clothSlotObject.GetComponent<SkinnedMeshRenderer>().sharedMesh);

		clothSlotObject.GetComponent<SkinnedMeshRenderer>().sharedMesh = mSource;

		//clothSlotObject.SetActive(true);    //Ensure that the cloth object is visibe

		if (material_url != null) {
			Renderer renderer = clothSlotObject.GetComponent<Renderer> () as Renderer;
			renderer.material = Resources.Load(material_url) as Material;
		}


		/*
		//Get the name of the object
		string modelName = tableCloth.GetModelObjectName (clothID, bodySubtype);

		//Seek for the object
		Transform objFound = actorBody.ActorModel.transform.Find (modelName);

		if (objFound != null) {

			//For body structure that use Full Object clothing method, delete the object
			if (actorBody.ClothingType == CSConfig.ClothingMethodEnum.FullObject) {
				if (objFound != null)
					DestroyObject (objFound.gameObject);
			} else {
				//Hide the oject
				objFound.gameObject.SetActive (false);
			}

		}

		//Remove the coth from the list even if the cloth object isn't found
		clothesList.RemoveAll (x => x.ClothID == clothID);
		*/
	}

	// Update is called once per frame
	void Update () {

		/*
		if (Input.GetMouseButtonDown(0))
		{
		//	UpdateCloth ();

		//	m_ani.CrossFade ("BATTLE_LEFT", 0.2f);
			m_ani.Stop();
			m_ani.Play("THROW_LEFT");
			m_ani.PlayQueued ("BATTLE_LEFT");


			UpdateCloth ("Prefaps/Woodman/WoodmanHead001", "Hair", "Materials/Woodman/WoodmanHead001");
		}
		if (Input.GetMouseButtonDown(1))
		{
			m_ani.Stop();
			m_ani.Play("THROW_RIGHT");
			m_ani.PlayQueued ("BATTLE_RIGHT");

			UpdateCloth ("Prefaps/Woodman/WoodmanHead002", "Hair", "Materials/Woodman/WoodmanHead002");
		}
		*/
	}
}
