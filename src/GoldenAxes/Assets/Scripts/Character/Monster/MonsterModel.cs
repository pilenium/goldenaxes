﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterModel : MonoBehaviour {

	private		Monster			m_monster = null;

	protected	Animation		m_monster_ani		= null;
	protected	string			m_nextAnimation		= null;


	protected	UVAnimation		m_monster_uv_ani	= null;

	protected	Material		m_monster_wave_mat	= null;
	protected	Material[]		m_monster_body_mats	= null;

	protected	BoxCollider		m_monster_collider	= null;
	public		BoxCollider		GetCollider()	{ return m_monster_collider; }

	public void Awake()
	{
		m_monster_ani = GetComponent<Animation> ();
		

		// 이 녀석만 특별하게 tag를 단다 (히트 확인) 
		this.tag = Common.TAG_MONSTER;

		Renderer rd = GetComponentInChildren<Renderer> ();
		Material[] mats = rd.materials;

		m_monster_body_mats = new Material[mats.Length - 1];
		int count = 0;
		foreach (Material mat in mats) {
			if (mat.name.Contains ("Wave")) {
				m_monster_wave_mat = mat;
			} else {
				m_monster_body_mats [count] = mat;
				count++;
			}
		}
		m_monster_uv_ani = GetComponent<UVAnimation> () as UVAnimation;

		m_monster_collider = GetComponent<BoxCollider> ();
		m_monster_collider.enabled = true;
	}

	public void OnDestroy()
	{
		m_monster_uv_ani = null;
	}

	public void Update()
	{
		if (m_monster_ani == null) {
			return;
		}

		if (m_monster_ani.isPlaying == false) {
			OnEndAnimation();
		}
	}


	public void SetParent(Monster monster)
	{
		m_monster = monster;
		this.transform.parent = monster.transform;
		this.transform.localPosition = Vector3.zero;
		this.transform.localRotation = Quaternion.identity;
	}

	public void SetWaveColor(Color32 color)
	{
		m_monster_wave_mat.color = color;	
	}

	// 애니메이션 관련
	public void SetAnimation(string name, string next = null)
	{
		m_monster_ani.Stop();
		m_monster_ani.Play(name);

		if (m_monster_uv_ani != null) {
			m_monster_uv_ani.SetStatus (name);
		}

		m_nextAnimation = next;
	}

	public void OnEndAnimation()
	{
		if(m_nextAnimation != null)
		{
			SetAnimation(m_nextAnimation, null);
		}
		m_monster.OnEndAnimation();
	}

	public void SetColor(Color color)
	{
		foreach (Material mat in m_monster_body_mats) {
			mat.color = color;
		}

		//StartCoroutine ("RemoveColor");
	}

	IEnumerator RemoveColor()
	{
		yield return new WaitForSeconds(0.1f);

		foreach (Material mat in m_monster_body_mats) {
			mat.color = Color.white;
		}

	}

	// animation frame events

	private void OnAttack()
	{
		if(m_monster != null)
		{
			m_monster.OnAttack();
		}
	}
}
