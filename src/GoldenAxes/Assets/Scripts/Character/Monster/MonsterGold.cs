﻿using UnityEngine;
using System.Collections;


public class MonsterGold : Monster
{
	public override void Awake() 
	{
		base.Awake ();
	}

	public override void OnDestroy()
	{
		base.OnDestroy ();
	}

	public override void Hit(double damage, Axe axe)
	{
		Vector3 coinPos = m_tr.position;

		GamePlay.instance.AddCoin(10, coinPos, Random.value * 2 * Mathf.PI, 2, 16, 40, 0f);
		GamePlay.instance.AddCoin(10, coinPos, Random.value * 2 * Mathf.PI, 2, 16, 40, 0f);
	}
}

