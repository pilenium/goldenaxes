﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;
using System.Collections;
using System.Collections.Generic;

public enum MONSTER_STATE {
	APPEAR,
	IDLE,
	STURN,
	DIE,
	ATTACK,
	DISAPPEAR
}

// 몬스터 이벤트들
public class MonsterEventHit : UnityEvent<double, Axe> {}
public class MonsterEventCondition : UnityEvent<Condition> {}
public class MonsterEventDie : UnityEvent {}
public class MonsterEventDisappear : UnityEvent {}

// 기본 몬스터는 Movefloat로 세팅
// 몬스터 기본 형 (MoveFloat로 세팅)
public class Monster : Character {
	
	protected	MonsterModel			m_monster			= null;
	protected	MonsterModel			m_originalMonster	= null;		// swap 용

	protected	Rigidbody				m_rigidBody			= null;
	//protected	EffectWaveSystem		m_waveSystem		= null;
	protected	MonsterProgress			m_progress			= null;


	
	// parent pattern
	protected	Pattern		m_pattern;
	public		Pattern		pattern		{ get { return m_pattern; } }



	// flotsam effect
	protected	TransfromTexMove		m_moveFlotsam		= null;
	protected	EffectColor				m_effectFlotsam		= null;

	protected	DataMonster				m_data;

	// 몬스터 변수들
	protected	MONSTER_STATE			m_state;
	protected	double					m_health;
	protected	double					m_healthMax;
	public		float					m_speed;

	protected	Condition				m_condition = null;

	// events
	public		MonsterEventHit			eventHit			{ get; protected set; }
	public		MonsterEventCondition	eventCondition		{ get; protected set; }
	public		MonsterEventDie			eventDie			{ get; protected set; }
	public		MonsterEventDisappear	eventDisappear		{ get; protected set; }



	// getter
	public		DataMonster				data			{ get { return m_data; } }
	public		double					health			{ get { return m_health; } }
	public		double					healthMax		{ get { return m_healthMax; } }
	//public		float					speed			{ get { return m_speed; } }




	// 잡다구리한 처리용
	public		int						godCount;

	public override void Awake() 
	{
		base.Awake();

		eventHit = new MonsterEventHit();
		eventCondition = new MonsterEventCondition();
		eventDie = new MonsterEventDie();
		eventDisappear = new MonsterEventDisappear();
	}

	public override void OnDestroy()
	{
		base.OnDestroy();

		m_pattern = null;

		m_condition = null;


		RemoveEvents();
		eventHit = null;
		eventCondition = null;
		eventDie = null;
		eventDisappear = null;
	}

	public override void Update()
	{
		base.Update();

		if(m_condition != null)
		{
			m_condition.Update(Time.deltaTime);
		}
	}

	public void SetPattern(Pattern pattern)
	{
		m_pattern = pattern;
	}

	
	// initiallize
	public virtual void Init(DataMonster data, double health, Vector3 position, float speed)
	{
		m_tr.localPosition = position;

		InitMonster (data, health, speed);
		InitProgress (data);
		SetState (MONSTER_STATE.APPEAR);
		SetColor(Color.white);

		InitNavMeshAgent(data.scale/2, speed);
		SetDestination(new Vector3(0, 0, Common.WOODMAN_POSITION.z + Common.SHIP_RADIUS));
	}

	protected virtual void InitMonster(DataMonster data, double health, float speed)
	{
		// 1. 데이터 입력
		m_data			= data;
		m_health		= health + health * data.armor;
		m_healthMax 	= m_health;


		// 2. 없으면 만들거나 세팅하거나 (1회)
		// monster;
		if (m_monster == null) {
			GameObject obj = GameObject.Instantiate (Resources.Load(data.prefab)) as GameObject;
			m_monster = obj.GetComponent<MonsterModel>();
			m_monster.SetParent(this);
		}
		
		// 리지드 바디 설정해주자.
		if (m_rigidBody == null) {
		//	m_rigidBody = gameObject.AddComponent<Rigidbody> () as Rigidbody;
		//	m_rigidBody.useGravity = false;
		//	m_rigidBody.isKinematic = true;
		}
		// 부유 움직임 이펙트 (풀 안에 있는거니깐 풀을 사용 하지 말자)
		if (m_effectFlotsam == null) {
			GameObject nObj = GameObject.Instantiate (Resources.Load("Prefaps/Effect/Effect_FlotsamMove" + m_data.size)) as GameObject;
			m_moveFlotsam = nObj.AddComponent<TransfromTexMove> () as TransfromTexMove;
			m_moveFlotsam.SpeedX = 0;

			m_effectFlotsam = nObj.AddComponent<EffectColor> () as EffectColor;
			m_effectFlotsam.transform.parent = m_tr;
			m_effectFlotsam.transform.localPosition = Vector3.up * Common.POS_SURFACE_EFFECT_Y;
			m_effectFlotsam.transform.localScale = Vector3.one * m_data.scale;
		}


		// 3. 그리고 값들 초기화
		m_tr.rotation = Quaternion.LookRotation(Vector3.back);
		m_monster.SetWaveColor(GamePlay.instance.dataChapter.water_light_color);
		m_monster.GetCollider().enabled = true;

		

		// TODO 스피드 이거는 몬스터 스피드로 해야하는데 Move로직을 좀 건드려서 해결하자
		m_moveFlotsam.SpeedY = - speed * 0.005f;
		m_effectFlotsam.SetColor (GamePlay.instance.dataChapter.water_light_color);
		m_effectFlotsam.FadeIn ();
	}

	public void ChangeModel(string url)
	{
		// 오리지날 백업해주고
		m_originalMonster = m_monster;

		// 새로운거 만들자
		GameObject obj = GameObject.Instantiate (Resources.Load(url)) as GameObject;
		m_monster = obj.GetComponent<MonsterModel>();
		m_monster.SetParent(this);
		m_monster.SetWaveColor(GamePlay.instance.dataChapter.water_light_color);

		m_originalMonster.gameObject.SetActive(false);

		SetState(MONSTER_STATE.IDLE);
	}

	public void ChangeOriginalModel()
	{
		GameObject.DestroyImmediate(m_monster.gameObject);

		m_monster = m_originalMonster;
		m_originalMonster = null;


		m_monster.gameObject.SetActive(true);
	}

	protected virtual void InitProgress(DataMonster data)
	{
		if (m_progress == null) {
			GameObject obj = GameObject.Instantiate (Resources.Load("Prefaps/Monster/MonsterProgress")) as GameObject;
			m_progress = obj.AddComponent<MonsterProgress> () as MonsterProgress;
			//m_progress.SetScale (data.scale * 0.9f);
			obj.transform.parent = m_tr;

			m_progress.transform.localPosition = Vector3.up * 2;
		}
		m_progress.SetProgress (1.0f);
		m_progress.gameObject.SetActive (true);
	}
	
	public void SetState (MONSTER_STATE state)
	{
		m_state = state;

		switch (m_state)
		{
			// 몬스터 등장!!
			case MONSTER_STATE.APPEAR:
				SetAnimation("APPEAR", "IDLE");
				
			break;

			// 아이들
			case MONSTER_STATE.IDLE:
				//SetAI(new AIMoveFloat(m_data.scale, m_tr.position, m_speed));
			break;

			case MONSTER_STATE.STURN:
				break;

			case MONSTER_STATE.DIE:
				// ai 지우자
				//ClearAI();

				m_monster.GetCollider().enabled = false;

				if(m_progress) m_progress.gameObject.SetActive (false);

				m_effectFlotsam.FadeOut();

				m_pattern.KillMonster(this);


				// 다잉 이펙트
				Vector3 effectPos = new Vector3 (m_tr.position.x, Common.POS_SURFACE_EFFECT_Y, m_tr.position.z);
				//GamePlay.instance.effectManager.AddEffect<EffectParticle> ("Prefaps/Effect/Effect_Hit_Star", effectPos, false);
				GamePlay.instance.effectManager.AddEffect<EffectAnimator> ("Prefaps/Effect/Effect_Hit_Die", effectPos, true);
				addCloud (m_data.scale);

				// 구름 이펙트
				Effect effect = GamePlay.instance.effectManager.AddEffect<EffectParticle> ("Prefaps/Effect/Effect_AppearSmoke", effectPos, true);
				effect.gameObject.transform.localScale = Vector3.one * m_data.scale;

				// 코인 추가
				GamePlay.instance.AddCoin(GamePlay.instance.properties.monsterGold, m_tr.position, Random.value * 2 * Mathf.PI, 2, 16, 40, 0f);

				/*
				EffectLabel eff = GamePlay.instance.effectManager.AddEffect<EffectLabel>("Prefaps/UI/EffectLabelCoin", Vector3.zero, false) as EffectLabel;
				eff.SetWorldPosition(m_tr.position);
				eff.SetText(PGTool.StringManager.ToGANumber(GamePlay.instance.properties.monsterGold));
				*/
				// 먼저는 죽는 애니메이션 나왔는데 바로 죽이자
				eventDie.Invoke();

				Remove ();
			break;

			case MONSTER_STATE.ATTACK:
				SetAnimation("ATTACK", "IDLE");
			break;


			case MONSTER_STATE.DISAPPEAR:
				SetAnimation("DISAPPEAR");

				m_monster.GetCollider().enabled = false;

				if (m_progress)		m_progress.gameObject.SetActive (false);

				m_pattern.ReleaseMonster(this);

				m_effectFlotsam.FadeOut ();

				eventDisappear.Invoke();

			break;
		}
	}
	public bool IsState (MONSTER_STATE state)
	{
		return m_state == state;
	}

	protected virtual void Remove()
	{
		if(m_originalMonster != null)
		{
			ChangeOriginalModel();
		}

		RemoveDestination();
		RemoveCondition();
		RemoveEvents();

		GamePlay.instance.monsterManager.RemoveMonster(this);
	}

	protected override void EndDestination()
	{
		RemoveDestination();
		
		if (IsState (MONSTER_STATE.DIE) == false) {
			SetState(MONSTER_STATE.ATTACK);
		}
	}

	// 애니메이션 관련
	public override void SetAnimation(string name, string next = null)
	{
		m_monster.SetAnimation(name, next);
	}

	public void OnEndAnimation()
	{
		switch (m_state)
		{
			// 이걸로 몬스터 시작이여
			case MONSTER_STATE.APPEAR:
				SetState(MONSTER_STATE.IDLE);
			break;

			// 이걸로 몬스터 시작이여
			case MONSTER_STATE.ATTACK:
				SetState(MONSTER_STATE.DISAPPEAR);
			break;

			case MONSTER_STATE.DISAPPEAR:
				// 파도 이펙트 추가
				Vector3 effectPos = new Vector3 (m_tr.position.x, Common.POS_SURFACE_EFFECT_Y, m_tr.position.z);
				EffectWave effectWave = GamePlay.instance.effectManager.AddEffect<EffectWave> ("Prefaps/Effect/Effect_Wave", effectPos, false) as EffectWave;
				effectWave.Init (GamePlay.instance.dataChapter.water_light_color, m_data.scale, m_data.scale * 2);

				Remove();
				
			break;
		}
	}


	public void OnAttack()
	{
		GamePlay.instance.woodman.Attack();
	}

	public bool IsLive()
	{
		if(IsState(MONSTER_STATE.APPEAR))
			return false;

		if(IsState(MONSTER_STATE.DISAPPEAR))
			return false;

		if(IsState(MONSTER_STATE.DIE))
			return false;

		if(m_health <= 0.0f)
			return false;

		return true;
	}

	public virtual bool IsHit(Bounds bounds)
	{
		if(IsLive() == false)
			return false;

		return bounds.Intersects(m_monster.GetCollider().bounds);
	}
	public virtual bool IsHit(Vector3 center, float radius)
	{
		if(IsLive() == false)
			return false;

		// 거리 구하고 직경 반만큼 더해준다 (크기 보정)
		float distance = Vector3.Distance(center, position);

		return distance < radius + m_data.scale/2;
	}

	public virtual void Hit(double damage, Axe axe)
	{
		// 컨디션 젤 먼저 해주자 
		if(m_condition != null)
		{
			m_condition.Hit(damage, axe);
		}

		// decrease health
		m_health -= damage;

		// notice to pattern
		m_pattern.HitMonster(this, axe);

		// make effect
		// 히트 이펙트만 여기서 하자 (도끼 위치 땜시롱)
		Vector3 effectPos = Vector3.Lerp (axe.transform.position, m_tr.position, 0.5f);
		float dist = Vector3.Distance (Camera.main.transform.position, effectPos);
		GamePlay.instance.effectManager.AddEffect(axe.dataModel.effect_hit, axe.transform.position, true);

		// processs health & status
		if (m_health <= 0.0f) {
			m_health = 0.0f;

			SetState (MONSTER_STATE.DIE);

		//	
		}
		else {
			if(IsState(MONSTER_STATE.IDLE))		SetAnimation("HIT", "IDLE");
		}

		if(m_progress) m_progress.SetProgress ((float)(m_health / m_healthMax));

		// color
		//SetColor (Color.red);


		// 크리티컬일때만 사용하자.. (눈아프다...)
		// effect
		/* 
		EffectLabel eff = GamePlay.instance.effectManager.AddEffect<EffectLabel>("Prefaps/UI/EffectLabelDmg", Vector3.zero, false) as EffectLabel;
		eff.SetWorldPosition(m_tr.position);
		eff.SetText(PGTool.StringManager.ToGANumber(damage));
		*/
	}

	public Condition SetCondition(Condition.TYPE type, float time)
	{
		RemoveCondition();
		
		m_condition = Condition.Create(this, type, time);
		return m_condition;
	}

	public void EndCondition()
	{
		RemoveCondition();
	}

	protected void RemoveCondition()
	{
		if(m_condition != null)
		{
			m_condition.Clear();
			m_condition = null;
		}
	}

	protected virtual void addCloud (float scale)
	{
	}

	public void SetColor(Color color)
	{
		m_monster.SetColor(color);
	}
	// events

	protected void RemoveEvents()
	{
		eventHit.RemoveAllListeners();
		eventCondition.RemoveAllListeners();
		eventDie.RemoveAllListeners();
		eventDisappear.RemoveAllListeners();
	}

}
