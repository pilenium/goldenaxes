﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PGTool;

public class MonsterManager {

	protected	List<Monster>		m_monsters;

	public int monsterCount			{ get { return m_monsters.Count; } }

	public static Monster Create(int monster_id, double health, Vector3 position, float speed)
	{
		Monster ret;

		DataMonster data = Data.monster.getData (monster_id);

		if(data == null) {
			// ERROR
			return null;
		}
		switch(data.type)
		{
			case DataMonster.TYPE.MINION:
				ret = ResourceManager.instance.Get<MonsterMinion> (data.name) as MonsterMinion;
			break;

			case DataMonster.TYPE.BOSS:
				ret = ResourceManager.instance.Get<MonsterBoss> (data.name) as MonsterBoss;
			break;

			/*
			case DataMonster.TYPE.ELITE:
			break;
			*/

			default:
				ret = ResourceManager.instance.Get<Monster> (data.name) as Monster;
			break;
		}
		ret.Init (data, health, position, speed);


		return ret;
	}

	public MonsterManager()
	{
		m_monsters = new List<Monster> ();
	}

	~MonsterManager()
	{
		Clear ();
	}

	public void Clear()
	{
		m_monsters.Clear ();
		m_monsters = null;
	}

	public void GetMonsters(out List<Monster> monsters)
	{
		monsters = m_monsters;
	}

	public Monster AddMonster(int monster_id, double health, Vector3 position, float speed)
	{
		Monster ret = MonsterManager.Create(monster_id, health, position, speed);

		m_monsters.Add(ret);

		return ret;
	}

	public void RemoveMonster(Monster monster)
	{
		m_monsters.Remove(monster);
		ResourceManager.instance.Return(monster);
	}

	public Monster GetEmptyMonster()
	{
		// TODO : 뒤지는 도중이나 나오는 도중 애들은 어쩌지
		
		// 한바퀴 돌리고 없는 놈은 리턴해주자
		// 있다면 작은 숫자 목록 만들어서
		// 젤 작은 녀석을 리턴해주자
		List<Monster> lessList = new List<Monster> ();
		int count = 0;
		int lessCount = int.MaxValue;


		for(int i = 0 ; i < m_monsters.Count ; ++i)
		{
			if(m_monsters[i].IsLive() == false)
				continue;

			if (m_monsters[i].godCount == 0) {
				return m_monsters[i];
			} else {
				if (count < lessCount) {
					lessList.Insert (0, m_monsters[i]);
					lessCount = count;
				}
			}
		}

		Monster ret = null;
		if (lessList.Count > 0) {
			if(lessList[0].IsLive())
			ret = lessList [0];
		}
		lessList.Clear ();
		lessList = null;

		return ret;
	}
	public Monster GetRandomMonster()
	{
		// TODO : 뒤지는 도중이나 나오는 도중 애들은 어쩌지
		
		if(m_monsters.Count == 0)	return null;

		for(int i = 0 ; i < m_monsters.Count ; ++i)
		{
			if(m_monsters[i].IsLive())
			return m_monsters[i];
		}
		
		return null;
	}

	public Monster Hit(Axe axe)
	{
		return Hit (axe.GetCollider().bounds);
	}

	public Monster Hit(Bounds bounds)
	{
		for(int i = 0 ; i < m_monsters.Count ; ++i)
		{
			if (m_monsters[i].IsHit(bounds)) {
				return m_monsters[i];
			}
		}
		return null;
	}

	public bool MultiHit(Axe axe, out List<Monster> monsters)
	{
		return MultiHit (axe.GetCollider().bounds, out monsters);
	}

	public bool MultiHit(Bounds bounds, out List<Monster> monsters)
	{
		monsters = new List<Monster> ();
		bool result = false;

		for(int i = 0 ; i < m_monsters.Count ; ++i)
		{
			if (m_monsters[i].IsHit(bounds)) {
				monsters.Add(m_monsters[i]);
				result = true;
			}
		}

		return result;
	}

	public bool MultiHit(Vector3 center, float radius, out List<Monster> monsters)
	{
		monsters = new List<Monster> ();
		bool result = false;

		for(int i = 0 ; i < m_monsters.Count ; ++i)
		{
			if (m_monsters[i].IsHit(center, radius)) {
				monsters.Add(m_monsters[i]);
				result = true;
			}
		}

		return result;
	}



}
