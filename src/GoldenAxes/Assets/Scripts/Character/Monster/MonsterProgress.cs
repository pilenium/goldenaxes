﻿using UnityEngine;
using System.Collections;

public class MonsterProgress : MonoBehaviour {

	private Transform	m_tr;
	private Renderer	m_progress_renderer	= null;

	// Use this for initialization
	void Awake () {
		m_tr = GetComponent<Transform> ();


		Renderer[] renderers = GetComponentsInChildren<Renderer> ();

		foreach (Renderer renderer in renderers)
		{
			if (renderer.gameObject.tag == "PROGRESS") {
				m_progress_renderer = renderer;
			}
		}
	}

	public void SetScale(float scale)
	{
		m_tr.localScale = Vector3.one * scale;
	}

	public void SetProgress(float progress)
	{
		if (m_progress_renderer) {
			m_progress_renderer.material.SetFloat ("_Progress", progress);

		}

	}

}
