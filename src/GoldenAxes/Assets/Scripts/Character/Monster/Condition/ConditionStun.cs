﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionStun : Condition {
	protected Effect	m_effect;
	protected float		m_resumeSpeed;

	public ConditionStun(Monster monster, TYPE type, float time) : base(monster, type, time)
	{
	}

	protected override void Start()
	{
		m_monster.SetAnimation("STUN");

		Vector3 effectPos = m_monster.position;
		effectPos.y += m_monster.data.scale;

		m_effect = GamePlay.instance.effectManager.AddEffect<Effect>("Prefaps/Effect/EffectStun", effectPos, false);

		m_resumeSpeed = m_monster.GetSpeed();
		m_monster.SetSpeed(0.0f);
	}

	protected override void End()
	{
		GamePlay.instance.effectManager.RemoveEffect(m_effect);
		m_monster.SetSpeed(m_resumeSpeed);
		m_monster.SetAnimation("IDLE");
	}
}