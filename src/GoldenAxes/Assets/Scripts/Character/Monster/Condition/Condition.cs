﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Condition {

	public enum TYPE {
		STUN,
		GOLDENING
	}

	public static Condition Create(Monster monster, TYPE type, float time)
	{
		Condition ret = null;

		switch(type)
		{
			case TYPE.STUN:
				ret = new ConditionStun(monster, type, time);
			break;

			case TYPE.GOLDENING:
				ret = new ConditionGoldening(monster, type, time);
			break;
		}

		return ret;
	}

	protected	Monster	m_monster;
	protected	TYPE	m_type;
	protected	float	m_time;

	public Condition(Monster monster, TYPE type, float time)
	{
		m_monster = monster;
		m_type = type;
		m_time = time;

		Start();
	}

	~Condition()
	{
	}

	public void Clear()
	{
		End();
	}


	public void Update(float dt)
	{
		m_time -= dt;

		if(m_time <= 0.0f)
		{
			m_monster.EndCondition();
		}
	}

	protected virtual void Start()
	{
	}

	protected virtual void End()
	{
	}

	public virtual void Hit(double damage, Axe axe)
	{
		
	}
}