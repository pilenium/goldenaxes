﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionGoldening : Condition {

	private float m_goldMultiplier = 0.0f;

	public ConditionGoldening(Monster monster, TYPE type, float time) : base(monster, type, time)
	{
	}

	protected override void Start()
	{
		m_monster.ChangeModel("Prefaps/Monster/CoinToad001");
//		m_monster.SetColor(new Color(0.73f, 0.62f, 0.23f, 1.0f));

		Vector3 effectPos = m_monster.position;
		effectPos.y = Common.POS_EFFECT_Y;

		Effect effect = GamePlay.instance.effectManager.AddEffect<EffectParticle> ("Prefaps/Effect/Effect_AppearSmoke", effectPos, true);
		effect.transform.localScale = Vector3.one * m_monster.data.scale;

	//	Effect effect = GamePlay.instance.effectManager.AddEffect<EffectAnimator>("Prefaps/Effect/EffectGoldening", effectPos, false);
		
		effect.transform.localScale = Vector3.one * m_monster.data.scale;
	}

	protected override void End()
	{
		//m_monster.ChangeOriginalModel();
//		m_monster.SetColor(new Color(0.5f, 0.5f, 0.5f, 1.0f));
	}

	public void SetGoldMultiplier(float multiplier)
	{
		m_goldMultiplier = multiplier;
	}

	public override void Hit(double damage, Axe axe)
	{
		GamePlay.instance.AddCoin(Math.Ceiling(GamePlay.instance.properties.monsterGold * m_goldMultiplier), axe.transform.position, UnityEngine.Random.value * 2 * Mathf.PI, 2, 10, 40, 0f);

	}
}
