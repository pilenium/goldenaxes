﻿using UnityEngine;
using System.Collections;

public class PrefabManager {

	public static GameObject LoadPrefab(string url)
	{
		GameObject obj = GameObject.Instantiate (Resources.Load(url)) as GameObject;

		return obj;
	}

	public static GameObject LoadNGUIPrefab(GameObject parent, string url)
	{
		GameObject go = LoadPrefab(url);

		Transform t = go.transform;
		t.parent = parent.transform;
		t.localPosition = Vector3.zero;
		t.localRotation = Quaternion.identity;
		t.localScale = Vector3.one;
		go.layer = parent.layer;

		return go;
	}
}
