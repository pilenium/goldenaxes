﻿using UnityEngine;
using System.Collections;

public class GameUtil {
	
	public static bool ScreenToStage(Vector3 position, out RaycastHit hitInfo)
	{
		Ray ray = Camera.main.ScreenPointToRay (position);

		int layerMask = 1 << LayerMask.NameToLayer (Common.LAYER_WATER);

		return Physics.Raycast (ray, out hitInfo, Mathf.Infinity, layerMask);
	}

	public static Vector3 CheckStageBound(Vector3 position, float width, float height)
	{
		// 좌우
		if (position.x < -width/2) {
			position.x = -width/2;
		} else if (position.x > width/2) {
			position.x = width/2;
		}

		// 상하
		if (position.z > height) {
			position.z = height;
		}

		return position;
	}
}
