﻿using UnityEngine;
using System.Collections;

public class InitShader : MonoBehaviour {

	public string shader_name = "Unlit/Transparent";

	// Use this for initialization
	void Awake () {
	//	Material material = new Material(Shader.Find(shader_name));

		GetComponent<Renderer> ().material.shader = Shader.Find (shader_name);

//		GetComponent<Renderer>().material = material;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
