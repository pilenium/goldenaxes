﻿using UnityEngine;
using System.Collections;

public class TransformBillBoard : MonoBehaviour {

	private Transform m_tr;

	public bool UpdateByFrame = false;

	void Awake()
	{
		m_tr = GetComponent<Transform> ();
	}

	void OnDestroy()
	{
		m_tr = null;
	}

	void Start () {
		UpdateDirection ();
	}
	
	// Update is called once per frame
	void Update () {
		if(UpdateByFrame) UpdateDirection ();
	}


	public void UpdateDirection()
	{
		if (Camera.main) {
			m_tr.LookAt (transform.position + Camera.main.transform.rotation * Vector3.forward, Camera.main.transform.rotation * Vector3.up);
		}
	}
}
