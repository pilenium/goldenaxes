﻿using UnityEngine;
using System.Collections;

public class TransfromTexMove : MonoBehaviour {

	public		bool UpdateByFrame	= true;
	public		float SpeedX		= 0.0001f;
	public		float SpeedY		= 0.0001f;

	protected	Transform 		m_tr;
	protected	Renderer		m_renderer;

	protected	float			m_offset_x;
	protected	float			m_offset_y;

	public virtual void Awake() 
	{
		m_tr = GetComponent<Transform> ();

		m_renderer = GetComponent<Renderer> ();

		m_renderer.sortingLayerName = "Ground";

		m_offset_x = 0;
		m_offset_y = 0;
	}

	public void Update()
	{
		if(UpdateByFrame) Move (SpeedX, SpeedY);
	}


	public void Move(float x, float y)
	{
		m_offset_x += x;
		m_offset_y += y;
		m_renderer.material.SetTextureOffset("_MainTex", new Vector2(m_offset_x, m_offset_y));
	}
}
