﻿using UnityEngine;
using System.Collections;

public class TransformRandomDegree : MonoBehaviour {

	private Transform m_tr;

	public bool RandomX = true;
	public bool RandomY = true;
	public bool RandomZ = true;

	// Use this for initialization
	void Awake () {
		m_tr = GetComponent<Transform> ();
		UpdateDegree ();
	}

	void OnDestroy()
	{
		m_tr = null;
	}

	public void UpdateDegree()
	{
		Vector3 euler = new Vector3 ();

		if (RandomX)
			euler.x = Random.Range (0.0f, 360.0f);

		if (RandomY)
			euler.y = Random.Range (0.0f, 360.0f);

		if (RandomZ)
			euler.z = Random.Range (0.0f, 360.0f);
		
		m_tr.localRotation = Quaternion.Euler (euler);
	}
}
