﻿using UnityEngine;
using System.Collections;

public class PatternBoss : Pattern {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	protected override Vector3 GetStartPosition(float width, float height)
	{
		// 일반적인 포지션
		float x = - width / 2;
		float z = GamePlay.instance.dataChapter.stage_height * Common.BOSS_RESPAWN_Z - height/2;

		return new Vector3(x, 0.0f, z);
	}
}
