﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Pattern {

	public enum TYPE
	{
		FLOAT = 0,
		GOLD
	}
	
	public static Pattern Create()
	{
		Pattern ret = new Pattern ();
		return ret;
	}



	protected	int					m_level		= 0;

	protected	DataPattern			m_data;

	public		int					level		{ get { return m_level; } }
	public		float				width		{ get { return m_data.width; } }
	public		float				height		{ get { return m_data.height; } }

	protected	List<Monster>		m_monsters;

	// success
	private		bool				m_success = true;
	public		bool				success		{ get { return m_success; } }

	public Pattern()
	{
		m_success = true;
		m_monsters = new List<Monster> ();

		Reset ();
	}

	public void Reset()
	{
		m_level = 0;
		Clear ();
	}

	public void Clear()
	{
		m_monsters.Clear ();
	}

	public void Destory()
	{
		foreach (Monster monster in m_monsters) {
			GamePlay.instance.monsterManager.RemoveMonster (monster);
		}

		Clear ();
	}

	public virtual void Init(int patternLevel, double monsterHealth, DataChapterSpeed speed, DataMonsterIDs monsterIds)
	{
		m_level = patternLevel;

		m_data = Data.pattern.GetRandomPattern(patternLevel);
		Vector3 startPos = GetStartPosition (m_data.width, m_data.height);

		if (m_data == null) {
			// ERROR 없는 패턴
		}
		Vector3 pos;

		foreach (DataPatternObject dataObject in m_data.objects) {
			
			pos = new Vector3(dataObject.x + startPos.x, 0.0f, dataObject.z + startPos.z);
			switch (dataObject.size) {
			case "S":
				AddMonster(monsterIds.small, monsterHealth, pos, speed.battle);
				break;

			case "M":
				AddMonster(monsterIds.middle, monsterHealth, pos, speed.battle);
				break;

			case "L":
				AddMonster(monsterIds.large, monsterHealth, pos, speed.elite);
				break;

			case "B":
				AddMonster(monsterIds.boss, monsterHealth, pos, speed.boss);
				break;
			}

		}
	}
	/*
	public virtual void Init (int level, double monsterHealth, float speed, int mosterID)
	{
		m_level = level;

		m_data = Data.pattern.GetRandomPattern (type, level);
		Vector3 startPos = GetStartPosition (m_data.width, m_data.height);

		if (m_data == null) {
			// ERROR 없는 패턴
		}

		foreach (DataPatternObject dataObject in m_data.objects) {
			AddMonster (mosterID, monsterHealth, dataObject.position + startPos, speed);
		}
	} */

	protected virtual Vector3 GetStartPosition(float width, float height)
	{
		// 일반적인 포지션
		float x = -GamePlay.instance.dataChapter.stage_width/2 + Random.Range (0.0f, GamePlay.instance.dataChapter.stage_width - width);
		float z = Random.Range (GamePlay.instance.dataChapter.stage_height * Common.MONSTER_RESPAWN_MIN_Z, GamePlay.instance.dataChapter.stage_height * Common.MONSTER_RESPAWN_MAX_Z) - height/2;

		//float x = GamePlay.instance.dataChapter.rect.x + Random.Range (0.0f, GamePlay.instance.dataChapter.rect.width - width);
		//float z = GamePlay.instance.dataChapter.rect.y + Random.Range (GamePlay.instance.dataChapter.rect.height * Common.MONSTER_RESPAWN_MIN_Z, GamePlay.instance.dataChapter.rect.height * Common.MONSTER_RESPAWN_MAX_Z) - height/2;



		return new Vector3(x, 0.0f, z);
	}



	// 몬스터 추가
	public virtual Monster AddMonster(int monster_id, double health, Vector3 position, float speed)
	{
		// 추가는 여기서 해주지만 제거는 몬스터 내에서 한다
		// TODO 나중에 추가/제거 시점 맞추는게 좋지 않으까?
		Monster monster = GamePlay.instance.monsterManager.AddMonster (monster_id, health, position, speed);
		monster.SetPattern (this);

		m_monsters.Add (monster);

		return monster;
	}


	// 몬스터에서 호출 

	/// <summary>
	/// 몬스터 히트 (kill이랑 별개 / combo 쌓음) 
	/// </summary>
	/// <param name="monster">몬스터</param>
	/// <param name="axe">히트한 도끼</param>
	public virtual void HitMonster(Monster monster, Axe axe)
	{
		GamePlay.instance.HitMonster (monster, axe);
	}

	/// <summary>
	/// 몬스터 죽음 (hit이랑 별개 comobo 쌓지 않음)
	/// </summary>
	/// <param name="monster">몬스터</param>
	public virtual void KillMonster(Monster monster)
	{
		m_monsters.Remove (monster);

		GamePlay.instance.KillMonster (monster);

		CheckEnd ();
	}

	/// <summary>
	/// 몬스터 못잡음
	/// </summary>
	/// <param name="monster">몬스터</param>
	public virtual void ReleaseMonster(Monster monster)
	{
		m_success = false;

		m_monsters.Remove (monster);

		GamePlay.instance.ReleaseMonster (monster);

		CheckEnd ();
	}
	public void CheckEnd()
	{
		if (m_monsters.Count == 0) {
			GamePlay.instance.mode.EndPattern (this);
		}
	}


}
