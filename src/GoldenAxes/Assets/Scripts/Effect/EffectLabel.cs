﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectLabel : EffectNGUI {

	protected	UILabel			m_label;
	protected	float			m_time;
	protected	float			m_delay;
	protected	float			m_deltaY;

	private		TweenPosition	m_tweenP = null;
	private		TweenAlpha		m_tweenA = null;
	private		EventDelegate	m_del;

	public override void Awake()
	{
		base.Awake ();

		m_label = GetComponent<UILabel>() as UILabel;
		m_time = 0.8f;
		m_delay = m_time - m_time/3;
		m_deltaY = 30.0f;

		m_del = new EventDelegate (this, "EndMove");
	}

	public override void Remove()
	{
		base.Remove();
		RemoveTweens();
	}
	public override void ReUse()
	{
		base.ReUse();
		m_label.alpha = 1.0f;
	}
	public override void EndUse()
	{
		base.EndUse();
		RemoveTweens();
	}

	private void RemoveTweens()
	{
		if(m_tweenP != null)
		{
			m_tweenP.enabled = false;
			m_tweenP.RemoveOnFinished(m_del);
			m_tweenP = null;
		}

		if(m_tweenA != null)
		{
			m_tweenA.enabled = false;
			m_tweenA = null;
		}
	}

	private IEnumerator StartTween(float time)
	{
		RemoveTweens();

		Vector3 targetPosition = m_tr.localPosition;
		targetPosition.y += m_deltaY * GamePlay.instance.ui.pixelSizeAdjustment;

		m_tweenP = TweenPosition.Begin(this.gameObject, m_time, targetPosition);
		m_tweenP.method = UITweener.Method.EaseOut;
		m_tweenP.AddOnFinished(new EventDelegate (this, "EndMove"));

        yield return new WaitForSeconds(m_delay);

		m_tweenA = TweenAlpha.Begin(this.gameObject, m_time - m_delay, 0.0f);
		m_tweenA.method = UITweener.Method.EaseOut;
	}

	public void SetText(string text)
	{
		m_label.text = text;
	}

	public override void SetPosition(Vector3 position)
	{
		base.SetPosition(position);
		StartCoroutine(StartTween(m_time));
	}

	public override void SetWorldPosition(Vector3 worldPosition)
	{
		base.SetWorldPosition(worldPosition);
		StartCoroutine(StartTween(m_time));
	}

	private void EndMove(UITweener tween, EventDelegate del)
	{
		Remove();
	}
}
