﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;




public class EffectManager {

	protected	List<Effect>	m_effects;

	public EffectManager()
	{
		m_effects	= new List<Effect> ();
	}

	~EffectManager()
	{
		Clear ();
	}

	public void Clear()
	{
		m_effects.Clear ();
		m_effects = null;
	}

	public void Update()
	{
	}

	private Effect Create<T> (string url) where T : Effect
	{
		// url을 키로 사용
		Effect effect = PGTool.ResourceManager.instance.Get<T> (url, url) as Effect;
		return effect;
	}

	private Effect Create<T> (string url, Vector3 position) where T : Effect
	{
		Effect effect = Create<T> (url);
		effect.gameObject.transform.position = position;
		return effect;
	}


	public Effect Create(DataEffect data, Vector3 position)
	{
		Effect effect = null;

		switch (data.type) {
		case "ANIMATOR":
			effect = Create <EffectAnimator> (data.url , data.pos + position);
			break;

		case "ANIMATION":
			effect = Create <EffectAnimation> (data.url , data.pos + position);
			break;

		case "PARTICLE":
			effect = Create <EffectParticle> (data.url , data.pos + position);
			break;
		}

		return effect;
	}


	// 이펙트는 절대 좌표 (다른 오브젝트에 child로 붙음 안됨)
	/// <summary>
	/// 이펙트 추가
	/// ** 이펙트는 절대좌표 (다른 오브젝트의 child가 되면 pooling에서 에러남) **
	/// </summary>
	/// <returns>The effect.</returns>
	/// <param name="url">리소스 URL</param>
	/// <param name="position">시작 </param>
	/// <param name="scroll">스테이지 스크롤에 맞춰서 스크롤 할 </param>
	/// <typeparam name="T">typeof Effect</typeparam>
	public Effect AddEffect<T> (string url, Vector3 position, bool scroll = true) where T : Effect
	{
		Effect effect = Create <T> (url, position);
		effect.autoScroll = scroll;
		effect.SetLive(true);

		m_effects.Add (effect);

		return effect;
	}

	public Effect AddEffect(DataEffect data, Vector3 position, bool scroll = true)
	{
		Effect effect = Create (data, position);
		effect.autoScroll = scroll;
		effect.SetLive(true);

		m_effects.Add (effect);

		return effect;
	}

	public void RemoveEffect(Effect effect)
	{
		effect.SetLive(false);
		m_effects.Remove (effect);
		PGTool.ResourceManager.instance.Return (effect);
	}
}
