﻿using UnityEngine;
using System.Collections;

public class EffectGod : MonoBehaviour {

	private Animation	m_animation;

	// Use this for initialization
	void Awake () {
		m_animation = GetComponentInChildren<Animation> () as Animation;


		if(Random.Range(0, 2) == 0)
		{
			m_animation.Play ("APPEAR_01");
		}
		else{
			m_animation.Play ("APPEAR_02");
		}
	}

	// Update is called once per frame
	void Update () {
		if (m_animation.isPlaying == false) {
			//GamePlay.instance.stage.RemoveEffect(gameObject);
		}
	}

}
