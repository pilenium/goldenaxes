﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectNGUI : Effect {

	public virtual void SetPosition(Vector3 position)
	{
		m_tr.parent = GamePlay.instance.ui.effect.transform;
		m_tr.localScale = Vector3.one;
		m_tr.localPosition = position;
	}

	public virtual void SetWorldPosition(Vector3 worldPosition)
	{
		m_tr.parent = GamePlay.instance.ui.effect.transform;
		m_tr.localScale = Vector3.one;
		m_tr.localPosition = Camera.main.WorldToScreenPoint(worldPosition) * GamePlay.instance.ui.pixelSizeAdjustment;
	}
}
