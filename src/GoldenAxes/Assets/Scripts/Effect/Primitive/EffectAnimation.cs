﻿using UnityEngine;
using System.Collections;

public class EffectAnimation : EffectColor {
	
	private Animation[]	m_animations;

	public override void Awake () {
		base.Awake ();
		m_animations = GetComponentsInChildren<Animation> ();
	}

	public override void Clear()
	{
		base.Clear ();
		m_animations = null;
	}

	public override bool isPlaying {
		get {
			if(m_animations == null) return false;

			foreach (Animation ani in m_animations)
			{
				if(ani.isPlaying) return true;
			}

			return false;
		}
	}
}
