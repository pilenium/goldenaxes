﻿using UnityEngine;
using System.Collections;

public class EffectColor : Effect {

	protected Renderer	m_renderer;

	// Use this for initialization
	public override void Awake () {
		base.Awake ();
		m_renderer = GetComponentInChildren<Renderer>();
	}

	public void SetColor(Color color)
	{
		m_renderer.material.color = color;
	}

	public Color GetColor()
	{
		return m_renderer.material.color;
	}

	public void FadeIn()
	{
		Color alphaColor = GetColor ();
		alphaColor.a = 0.0f;
		SetColor (alphaColor);
		TweenAlpha.Begin (gameObject, 0.5f, 1.0f).method = UITweener.Method.EaseOut;
	}

	public void FadeOut()
	{
		TweenAlpha.Begin (gameObject, 0.5f, 0.0f).method = UITweener.Method.EaseOut;
	}
}
