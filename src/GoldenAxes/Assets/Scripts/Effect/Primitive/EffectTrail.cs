﻿using UnityEngine;
using System.Collections;

public class EffectTrail : Effect {

	private TrailRenderer m_trail;

	// Use this for initialization
	public override void Awake () {
		base.Awake ();
		m_trail = GetComponent<TrailRenderer> () as TrailRenderer;
		m_trail.Clear();
	}

	public void OnDestroy() {
		//Remove();
	}

	public override void Clear()
	{
		base.Clear ();
		m_trail = null;
	}
	public override void ReUse()
	{
		base.ReUse();
		m_trail.Clear();
	}

	public override void Play()
	{
		// 이건 할수 있는게 없네
		//m_renderer.Play ();
	}

	public override void Stop()
	{
		// 이건 할수 있는게 없네
		//m_renderer.Stop ();
		isLive = false;
	}

	public override bool isPlaying {
		get {
			if(isLive)
				return true;
			
			if(m_trail.positionCount > 0)
				return true;

			return false;
		}
	}
}
