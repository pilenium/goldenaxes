﻿using UnityEngine;
using System.Collections;

public class EffectMove : EffectColor {

	protected	Vector3		m_targetPos;
	protected	float		m_moveSpeed;

	protected	bool		m_fadeout;
	protected	float		m_fadeSpeed;
	protected	Color		m_targetColor;

	protected	int			m_phase;

	// Use this for initialization
	public void InitMove(Vector3 targetPosition, float moveSpeed, bool fadeout = true, float fadeSpeed = 8.0f)
	{
		autoScroll = false;

		m_targetPos = targetPosition;
		m_moveSpeed = moveSpeed;

		m_fadeout = fadeout;
		m_fadeSpeed = fadeSpeed;
		m_targetColor = Color.white;
		m_targetColor.a = 0;

		m_phase = 0;

	}

	public override void ReUse()
	{
		base.ReUse ();
		SetColor (Color.white);
	}


	public override void Update () {
		base.Update ();

		if (m_phase == 0) {
			UpdateMove ();
		} else {
			UpdateDisappear ();
		}
	}

	protected virtual void UpdateMove ()
	{
		m_tr.position = Vector3.Lerp (m_tr.position, m_targetPos, m_moveSpeed * Time.deltaTime);

		if (m_tr.position.y + 0.1f >=  m_targetPos.y) {
			if (m_fadeout) {
				m_phase = 1;
			} else {
				Remove ();
			}
		}
	}

	protected virtual void UpdateDisappear ()
	{
		SetColor (Color.Lerp (GetColor(), m_targetColor, m_fadeSpeed * Time.deltaTime));

		if (GetColor().a < 0.01f)
		{
			Remove ();
		}

	}
}
