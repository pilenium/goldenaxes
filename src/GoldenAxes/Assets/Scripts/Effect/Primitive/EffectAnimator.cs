﻿using UnityEngine;
using System.Collections;

public class EffectAnimator : EffectColor {
	private Animator[]	m_animators;

	// Use this for initialization
	public override void Awake () {
		base.Awake ();
		m_animators = GetComponentsInChildren<Animator> ();
	}

	public override void Clear()
	{
		base.Clear ();
		m_animators = null;
	}

	public override bool isPlaying {
		get {

			if(m_animators == null) return false;

			foreach (Animator ani in m_animators)
			{
//				return false;

//				if(1 > ani.GetCurrentAnimatorStateInfo(0).normalizedTime) return true;

				if(ani.GetCurrentAnimatorStateInfo(0).length > ani.GetCurrentAnimatorStateInfo(0).normalizedTime) return true;
								
				
			}

			return false;
		}
	}
}
