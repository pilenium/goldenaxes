﻿using UnityEngine;
using System.Collections;

public class EffectParticle : Effect {

	private ParticleSystem m_system;

	// Use this for initialization
	public override void Awake () {
		base.Awake ();
		m_system = GetComponent<ParticleSystem> () as ParticleSystem;
	}

	public override void Clear()
	{
		base.Clear ();
		m_system = null;
	}

	public override void Play()
	{
		m_system.Play ();
	}

	public override void Stop()
	{
		m_system.Stop ();
	}

	public override bool isPlaying {
		get {
			return m_system.IsAlive ();
		}
	}
}
