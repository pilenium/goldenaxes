﻿using UnityEngine;
using System.Collections;

public class EffectMoveGravity : EffectColor {

	protected	Vector3			m_velocity;
	protected	float			m_gravity;
	protected	float			m_time = 0;

	public virtual void InitVelocity (float degree, float power_h, float power_v, float gravity, float delay)
	{
		autoScroll = false;

		m_time = delay;

		m_velocity = new Vector3 ();
		m_velocity.x = power_h * Mathf.Cos(degree);
		m_velocity.y = power_v;
		m_velocity.z = power_h * Mathf.Sin(degree);

		m_gravity = gravity;
	}

	public override void Update () {
		base.Update ();
	
		if (m_time >= 0.0f) {
			m_time -= Time.deltaTime;
		} else {
			m_tr.Translate (m_velocity * Time.deltaTime);
			if (m_tr.position.y <= Common.POS_COIN_Y) {
				OnCollisionWater ();
			}
			m_velocity.y -= m_gravity * Time.deltaTime;
		}
	}

	protected virtual void OnCollisionWater()
	{
		Remove ();
	}
}
