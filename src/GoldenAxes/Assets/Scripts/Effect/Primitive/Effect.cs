﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Effect : PGTool.Resource {

	public		bool			autoRemove	= true;
	public		bool 			autoScroll;
	public		bool			isLive		{ protected set; get; }

	protected	Transform		m_tr;


	public override void Awake()
	{
		base.Awake ();
		m_tr = GetComponent<Transform> ();

		// make rigidbody
		Rigidbody m_rigidBody = m_tr.gameObject.AddComponent<Rigidbody>() as Rigidbody;
		m_rigidBody.useGravity = false;
		m_rigidBody.isKinematic = true;
	}

	public override void Clear()
	{
		base.Clear ();
		m_tr = null;
	}

	public virtual void Remove()
	{
		GamePlay.instance.effectManager.RemoveEffect (this);
	}

	public virtual void Update () {
		if (autoRemove && isPlaying == false) {
			Remove ();
		}

		if (autoScroll) {
			m_tr.Translate (Vector3.back * GamePlay.instance.stage.deltaDistance);
		}
	}

	public virtual void SetLive(bool live)
	{
		isLive = live;
	}

	public virtual void Play()
	{
		// particle 같은 껏다 켜는 녀석들은 이걸 사용
	}

	public virtual void Stop()
	{
		// particle 같은 껏다 켜는 녀석들은 이걸 사용
	}

	public virtual bool isPlaying {
		get {
			return true;
		}
	}
}
