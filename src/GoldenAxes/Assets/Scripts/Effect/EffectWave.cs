﻿using UnityEngine;
using System.Collections;

public class EffectWave : EffectColor {
	
	private Transform	m_wave_tr;

	private Color		m_targetColor;

	private Vector3		m_initScale;
	private Vector3		m_targetScale;

	public	float		speed			= 2.0f;

	// Use this for initialization
	public override void Awake () {
		base.Awake ();

		m_wave_tr = m_tr.GetChild (0);
	}

	public override void Clear()
	{
		base.Clear ();
		m_wave_tr = null;
		m_renderer = null;
	}

	public override void ReUse()
	{
		base.ReUse ();
	//	m_wave_tr.localScale = m_initScale;
	}

	public void Init(Color color, float scale, float target_scale)
	{
		m_initScale = Vector3.one * scale;
		m_targetScale = Vector3.one * target_scale;

		m_wave_tr.localScale = m_initScale;

		m_targetColor = color;
		m_targetColor.a = 0.0f;
		SetColor (color);
	}
	
	// Update is called once per frame
	public override void Update () {
		m_wave_tr.localScale = Vector3.Lerp(m_wave_tr.localScale, m_targetScale, speed * Time.deltaTime);
		SetColor (Color.Lerp(GetColor(), m_targetColor, speed * Time.deltaTime));

		base.Update ();
	}

	public override bool isPlaying {
		get {
			return GetColor().a >= 0.01f;
		}
	}
}
