﻿using UnityEngine;
using System.Collections;

public class EffectWaveSystem : MonoBehaviour {
	
	private Transform	m_tr;
	private float		m_time;
	private float		m_size;
	private bool		m_enabled;

	// Use this for initialization
	void Awake () {
		m_tr = GetComponent<Transform>() as Transform;
	}

	void OnDestroy()
	{
		m_enabled = false;
		StopCoroutine ("AddWave");
	}

	public void Init(float time, float size)
	{
		m_time = time;
		m_size = size;
		m_enabled = true;
		StartCoroutine ("AddWave");
	}

	IEnumerator AddWave()
	{
		yield return new WaitForSeconds(0.1f);
		while (m_enabled) {
			Vector3 position = m_tr.position;
			position.y = Common.POS_SURFACE_EFFECT_Y;

			EffectWave effect = GamePlay.instance.effectManager.AddEffect<EffectWave> ("Prefaps/Effect/Effect_Wave", position, false) as EffectWave;
			effect.Init (GamePlay.instance.dataChapter.water_light_color, m_size, m_size * 1.5f);

			yield return new WaitForSeconds(m_time);
		}
	}

	// BG
	// Update is called once per frame
	void Update () {
	
	}
}
