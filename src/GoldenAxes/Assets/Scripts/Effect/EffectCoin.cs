﻿using UnityEngine;
using System.Collections;

public class EffectCoin : EffectMoveGravity {
	
	private double			m_coin = 0;


	public void InitCoin (double coin)
	{
		m_coin = coin;
	}

	protected override void OnCollisionWater()
	{
		Vector3 effectPos = new Vector3 (m_tr.position.x, Common.POS_SURFACE_EFFECT_Y, m_tr.position.z);
		Vector3 targetPos = new Vector3 (m_tr.position.x, Common.POS_SURFACE_EFFECT_Y + 1.0f, m_tr.position.z);

		EffectWave effectWave = GamePlay.instance.effectManager.AddEffect<EffectWave> ("Prefaps/Effect/Effect_Wave", effectPos, false) as EffectWave;
		effectWave.Init (GamePlay.instance.dataChapter.water_light_color, Common.SCALE_COIN.x, Common.SCALE_COIN.x * 2);

	/* 
		EffectCoinText effctText = GamePlay.instance.effectManager.AddEffect<EffectCoinText> ("Prefaps/Effect/Effect_Coin_Text", effectPos, false) as EffectCoinText;
		effctText.InitCoin (m_coin);
		effctText.InitMove (targetPos, 5.0f, true, 8.0f);*/

		EffectLabel eff = GamePlay.instance.effectManager.AddEffect<EffectLabel>("Prefaps/UI/EffectLabelCoin", Vector3.zero, false) as EffectLabel;
		eff.SetWorldPosition(effectPos);
		eff.SetText(PGTool.StringManager.ToGANumber(m_coin, "0.00", "0"));

		base.OnCollisionWater ();
	}
}
