﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameTouch : MonoBehaviour {

	protected	Vector3[]		m_startPos;
	protected	Vector3[]		m_prevPos;
	protected	float[]			m_startTime;
	protected	bool[]			m_uiTouched;

	protected	Control			m_control	= null;
	protected	bool			m_enabled	= true;


	protected	List<Control>	m_controls	= null;

	// Use this for initialization
	protected virtual void Awake ()
	{
		m_startPos = new Vector3[Common.MAX_TOUCH_COUNT];
		m_prevPos = new Vector3[Common.MAX_TOUCH_COUNT];
		m_startTime = new float[Common.MAX_TOUCH_COUNT];

		m_uiTouched = new bool[Common.MAX_TOUCH_COUNT];

		m_controls = new List<Control>();
	}

	protected void OnDestroy()
	{
		m_startPos = null;
		m_startTime = null;

		for (int i = 0; i < m_controls.Count ; ++i) {
			if (m_controls [i] != null) {
				m_controls [i].Clear ();
			}
		}
		m_controls.Clear();
		m_controls= null;
	}

	public void SetEnabled(bool enabled)
	{
		m_enabled = enabled;
	}

	public int CreateControl()
	{
		// 일단 널로 때려넣자
		m_controls.Add(null);
		return m_controls.Count - 1;
	}

	public Control InitControl(int index, string controlType)
	{
		RemoveControl(index);

		Control newControl = null;

		switch (controlType)
		{
		case "TIME":
			newControl = new TimeControl ();
			break;

		case "TOUCH":
			newControl = new TouchControl ();
			break;
		}

		m_controls[index] = newControl;

		return newControl;
	}

	public void RemoveControl(int index)
	{
		if (m_controls[index] != null) {
			m_controls[index].Clear ();
		}
	}

	public void SetControl(int index)
	{
		//m_control.Reset ();
		m_control = m_controls[index];
	}

	// 업데이트 순서가 UICamera보다 빠르게 Project Setting에 되어 있어야 함
	// Update is called once per frame
	protected void Update () {

		if (m_enabled == false) {
			return;
		}

		#if UNITY_EDITOR

		// only 2 buttons use
		for(int i = 0 ; i < 2 ; ++i)
		{
			if (Input.GetMouseButtonDown(i))
			{
				m_startPos[i] = Input.mousePosition;
				m_startTime[i] = Time.deltaTime;
				OnStartTouch(i, Input.mousePosition, Vector2.zero);
			}
			else if(Input.GetMouseButtonUp(i))
			{
				OnEndTouch(i, Input.mousePosition, Input.mousePosition - m_prevPos[i]);
			}
			else if(Input.GetMouseButton(i))
			{
				OnMovedTouch(i, Input.mousePosition, Input.mousePosition - m_prevPos[i]);
			}
			m_prevPos[i] = Input.mousePosition;
		}
			
		#else
		for(int i = 0 ; i < Input.touchCount ; ++i)
		{
			Touch touch = Input.GetTouch(i);

			if(touch.phase == TouchPhase.Began)
			{
				m_startPos[i] = touch.position;
				m_startTime[i] = Time.deltaTime;
				OnStartTouch(touch.fingerId, touch.position, touch.deltaPosition);
			}
			else if(touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
			{
				OnMovedTouch(touch.fingerId, touch.position, touch.deltaPosition);
			}
			else if(touch.phase == TouchPhase.Ended)
			{
				OnEndTouch(touch.fingerId, touch.position, touch.deltaPosition);
			}
			else if(touch.phase == TouchPhase.Canceled)
			{
				OnCanceldTouch(touch.fingerId, touch.position, touch.deltaPosition);
			}
		}

		#endif
	}


	protected void OnStartTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
		// 콘트롤이 안정해지면 리턴
		if (m_control == null)
			return;

		// 터치수보다 많으면 리턴
		if(id >= Common.MAX_TOUCH_COUNT)
			return;

		if (UICamera.Raycast (position) == true) 
		{
			m_uiTouched [id] = true;
			// NGUI 오브젝트가 선택되었음
			return;
		}

		m_uiTouched [id] = false;

		m_control.StartTouch (id, position, deltaPosition);
	}

	protected void OnMovedTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
		if (m_uiTouched [id]) {
			return;
		}

		if (m_control == null)
			return;

		if(id >= Common.MAX_TOUCH_COUNT)
			return;

		if (Input.touchCount - GetUITouchCount () > Common.MAX_TOUCH_THROW_COUNT) {
			return;
		}
		
		m_control.MovedTouch (id, position, deltaPosition);
	}

	protected void OnEndTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
		if (m_uiTouched [id]) {
			return;
		}

		m_uiTouched [id] = false;

		if (m_control == null)
			return;
		
		if (id >= Common.MAX_TOUCH_COUNT) {
			return;
		}

		#if UNITY_EDITOR
		#else
		if (Input.touchCount - GetUITouchCount () > Common.MAX_TOUCH_THROW_COUNT) {
			return;
		}
		#endif


		if (Mathf.Pow(position.x - m_startPos[id].x, 2.0f) + Mathf.Pow(position.y - m_startPos[id].y, 2.0f) > Mathf.Pow(Screen.width * 0.1f, 2.5f) && Time.deltaTime - m_startTime[id] < 0.3f) {

			m_control.Swipe (position.x - m_startPos[id].x, position.y - m_startPos[id].y);
			m_control.CanceldTouch (id, position, deltaPosition);

			return;
		}



		/*
		Debug.Log (deltaPosition + " : " + deltaPosition.magnitude);

		GamePlay.instance.ui.SetDebug (deltaPosition.x.ToString());

		if (deltaPosition.magnitude > Screen.dpi * 0.2f) {
//			Debug.Log (Screen.dpi / 2.5f);

			m_control.Swipe (deltaPosition);
			m_control.CanceldTouch (id, position, deltaPosition);
		}

		*/

		m_control.EndTouch (id, position, deltaPosition);
	}

	protected void OnCanceldTouch(int id, Vector2 position, Vector2 deltaPosition)
	{
		if (m_uiTouched [id]) {
			return;
		}
		m_uiTouched [id] = false;

		if (m_control == null)
			return;
		
		if (id >= Common.MAX_TOUCH_COUNT)
			return;


		#if UNITY_EDITOR
		#else
		if (Input.touchCount - GetUITouchCount () > Common.MAX_TOUCH_THROW_COUNT) {
			return;
		}
		#endif

		m_control.CanceldTouch (id, position, deltaPosition);
	}

	public int GetUITouchCount()
	{
		int count = 0;
		for (int i = 0; i < Common.MAX_TOUCH_COUNT; ++i) {
			if (m_uiTouched [i])
				count++;
		}

		return count;
	}

	public bool isUITouch(int id)
	{
		if (id >= Common.MAX_TOUCH_COUNT)

			// 터치 카운트 이상이면 그냥 ui터치로 인정
			// 그래야 스테이지에서 오작동 하지 않음
			return true;
		
		return m_uiTouched [id];
	}

}
