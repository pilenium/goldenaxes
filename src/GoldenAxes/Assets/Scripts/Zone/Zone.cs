﻿using UnityEngine;
using System.Collections;

public class Zone : MonoBehaviour {

	protected	DataZone		m_data;
	protected	Transform		m_tr;

	protected	GameObject		m_zone;
	protected	Transform		m_zone_tr;

	protected	Rigidbody		m_rigidBody;

	protected	int				m_count		= 0;

	protected virtual void Awake() 
	{
		m_tr = GetComponent<Transform> ();
	}

	protected virtual void Update()
	{
	}

	public virtual void Init(DataZone data)
	{
		// init data
		m_data		= data;
		name		= data.name;
		//tag			= Common.TAG_ZONE;

		m_zone = GameObject.CreatePrimitive(PrimitiveType.Quad);
		Renderer zoneRender = m_zone.GetComponent<Renderer> ();
		zoneRender.material = Resources.Load(data.material) as Material;

		m_zone_tr = m_zone.GetComponent<Transform> ();
		m_zone_tr.parent = m_tr;
		m_zone_tr.rotation = Quaternion.Euler (90.0f, 0, 0);
		m_zone_tr.localScale = Vector3.one * data.size;

		m_rigidBody = gameObject.AddComponent<Rigidbody> () as Rigidbody;
		m_rigidBody.useGravity = false;
		m_rigidBody.isKinematic = true;

		DataChapter chapterData = GamePlay.instance.dataChapter;

		float positionX = chapterData.stage_width/2 + data.size / 2 + Random.value * (chapterData.stage_width - data.size);
		float positionZ = chapterData.stage_height + Data.settings.BG_SCROLL_TOP_GAP;

		// align position
		m_tr.position = new Vector3 (positionX, Common.POS_ZONE_Y, positionZ);
	}

	public virtual bool IsHit(Vector3 point)
	{
		return false;
	}
}
