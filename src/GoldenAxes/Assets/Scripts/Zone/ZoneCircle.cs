﻿using UnityEngine;
using System.Collections;

public class ZoneCircle : Zone {
	

	protected override void Awake()
	{
		base.Awake ();
	}

	protected override void Update()
	{
		base.Update ();


		m_zone_tr.Rotate (Vector3.back,  Time.deltaTime * 50);
	}


	public override bool IsHit(Vector3 point)
	{
		if (Vector3.Distance (point, transform.position) <= m_data.size / 2) {
			
			return true;
		}

		return false;
	}
}
