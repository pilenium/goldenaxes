﻿using UnityEngine;
using System.Collections;

public class StageWater : MonoBehaviour {

	protected Transform 	m_tr;
	protected Renderer		m_renderer;

	protected float			m_width;
	protected float			m_height;
	protected float			m_tile_width;

	protected float			m_offset_x;
	protected float			m_offset_y;


	protected virtual void Awake() 
	{
		Init ();

		m_tr = GetComponent<Transform> ();
		//m_tr.Rotate (Vector3.right, 90);

		m_renderer = GetComponent<Renderer> ();

		m_renderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		m_renderer.receiveShadows = false;
		m_renderer.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
		m_renderer.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;


		m_renderer.sortingLayerName = "Ground";

	}

	protected virtual void Init()
	{
		name = Common.LAYER_WATER;
		gameObject.layer = LayerMask.NameToLayer(Common.LAYER_WATER);
	}

	public virtual void SetChapter(DataChapter data)
	{
		SetTransfrom (data.water_width, data.water_height, data.mat_size, Common.POS_WATER_Y);
		SetTexture (data.mat_water);
	}

	protected void SetTransfrom (float width, float height, float mat_width, float y)
	{
		m_width = width;
		m_height = height;
		m_tile_width = mat_width;

		m_tr.localScale = new Vector3 (m_width/10, 1f, m_height/10);
		m_tr.localPosition = new Vector3(0, y, m_height/2);
	}

	protected void SetTexture(string url)
	{
		m_renderer.material = Resources.Load(url) as Material;
		m_renderer.material.SetTextureScale ("_MainTex", new Vector2(m_width / m_tile_width, m_height / m_tile_width));

		m_offset_x = 0.0f;
		m_offset_y = 0.0f;
	}

	public void Move(float x, float y)
	{
		m_offset_x += x / m_tile_width;
		m_offset_y -= y / m_tile_width;
		m_renderer.material.SetTextureOffset("_MainTex", new Vector2(m_offset_x, m_offset_y));
	}
}
