﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroClouds : MonoBehaviour
{
    private Transform m_tr;

    private int X_COUNT = 6;
    private int Z_COUNT = 11;
    private float X_GAP = 4.0f;
    private float Z_GAP = 2.8f;

    private float POS_Y = 10.0f;
    private float POS_RAND = 1.0f;

    private float SIZE_MIN = 5.3f;
    private float SIZE_MAX = 7.0f;
    private float start_x, start_z, end_z;



    private float m_speed = 10.0f;
    private float m_target_speed = 10.0f;

    private List<List<GameObject>>  m_clouds;

    private bool m_open;

	// Use this for initialization
	public void Awake () {
        gameObject.name = "IntroCloud";
        m_tr = GetComponent<Transform>();
//        m_tr.localPosition = Vector3.up * POS_Y;
  
        m_tr.parent = Camera.main.transform;
        m_tr.localRotation = Quaternion.Euler(-56.21f, 0f, 0f);
        m_tr.localPosition = new Vector3(0f, -6.24f, 11.7f);
        InitClouds();
    }

    private void InitClouds()
    {
        m_speed = 10.0f;
        
        start_x = (float)(X_COUNT-1) / 2 * - X_GAP;
        start_z = -Z_GAP;
        end_z = start_z + Z_COUNT * Z_GAP;

        float random_x;

        m_clouds = new List<List<GameObject>>(X_COUNT);
        
        GameObject cloud;
        List<GameObject>    clouds;
        Vector3     pos = new Vector3();
        for(int j = 0 ; j < Z_COUNT ; j++)
        {
            clouds = new List<GameObject>(Z_COUNT);

            random_x = Random.Range(0.0f, X_GAP) - (float)(X_GAP)/2;

            for(int i = 0 ; i < X_COUNT ; ++i)
            {
                cloud = PrefabManager.LoadPrefab ("Prefaps/Effect/Effect_IntroCloud");
                cloud.transform.parent = m_tr;

                pos.x = random_x + start_x + i * X_GAP;
                pos.y = Random.value*POS_RAND -POS_RAND/2;
                pos.z = start_z + j * Z_GAP + (Random.value*POS_RAND -POS_RAND/2);
                cloud.transform.localPosition = pos;

                cloud.transform.localScale = Vector3.zero;//Vector3.one * Random.Range(SIZE_MIN, SIZE_MAX);

                clouds.Add(cloud);
            }
            m_clouds.Add(clouds);
        }

        // 만들고 바로 꺼주자

        m_open = true;
        this.enabled = false;

        // 시작은 닫힌 상태
        m_open = false;
        this.enabled = true;
        m_speed = 10f;
        for(int j = 0 ; j < Z_COUNT ; j++)
        {
            for(int i = 0 ; i < X_COUNT ; ++i)
            {
                TweenScale.Begin(m_clouds[j][i], 0.1f, Vector3.one * Random.Range(SIZE_MIN, SIZE_MAX)).method = UITweener.Method.EaseInOut;
            }
        }
        /* */
	}

    public void Update()
    {
		m_speed = m_speed + (m_target_speed - m_speed) * Time.deltaTime;

        for(int j = 0 ; j < Z_COUNT ; j++)
        {
            for(int i = 0 ; i < X_COUNT ; ++i)
            {
                m_clouds[j][i].transform.Translate(0.0f, 0.0f, m_speed * Time.deltaTime);

                if(m_clouds[j][i].transform.localPosition.z > end_z)
                {
                    m_clouds[j][i].transform.Translate(0.0f, 0.0f, -end_z- Z_GAP);
                }
            }
        }
    }

   public virtual void Open(float speed)
	{
        if(m_open)
            return;

        m_open = true;

        StopAllCoroutines();

        m_target_speed = speed;

        for(int j = 0 ; j < Z_COUNT ; j++)
        {
            for(int i = 0 ; i < X_COUNT ; ++i)
            {
                TweenScale.Begin(m_clouds[j][i], 1.0f, Vector3.zero).method = UITweener.Method.EaseInOut;
            }
        }
		StartCoroutine ("OpenCoroutine");
	}

    protected virtual IEnumerator OpenCoroutine()
    {
		yield return new WaitForSeconds(1.0f);
        this.enabled = false;
    }


   public virtual void Close(float speed)
	{
        if(!m_open)
            return;

        m_open = false;
        StopAllCoroutines();
        this.enabled = true;
        m_target_speed = speed;
        
		StartCoroutine ("CloudClose");
	}

	protected virtual IEnumerator CloudClose()
	{
		yield return new WaitForSeconds(0.5f);

        for(int j = 0 ; j < Z_COUNT ; j++)
        {
            for(int i = 0 ; i < X_COUNT ; ++i)
            {
                TweenScale.Begin(m_clouds[j][i], 1.0f, Vector3.one * Random.Range(SIZE_MIN, SIZE_MAX)).method = UITweener.Method.EaseInOut;
            }
        }
	}



}
