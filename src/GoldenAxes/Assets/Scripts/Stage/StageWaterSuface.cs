﻿using UnityEngine;
using System.Collections;

public class StageWaterSuface : StageWater
{
	protected override void Awake() 
	{
		base.Awake ();
	}


	protected override void Init()
	{
		name = "WaterSurface";
		gameObject.layer = LayerMask.NameToLayer("WaterSurface");

	}

	public override void SetChapter(DataChapter data)
	{
		SetTransfrom (data.water_width, data.water_height, data.mat_size, Common.POS_SURFACE_Y);
		SetTexture (data.mat_surface);
	}
}

