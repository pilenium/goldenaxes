﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StageNavMeshSurface : MonoBehaviour {

	private NavMeshSurface	m_surface;
	public	GameObject		m_mesh;

	// Use this for initialization
	void Awake() {
		m_surface = GetComponent<NavMeshSurface>() as NavMeshSurface;
		m_surface.BuildNavMesh();
		m_mesh.SetActive(false);
	}
	
	public void SetChapter(DataChapter data)
	{
		m_mesh.transform.localScale = new Vector3(data.stage_width, data.stage_height - Common.WOODMAN_POSITION.y, 1);
		m_mesh.transform.localPosition = new Vector3(0, Common.POS_WATER_Y, data.stage_height/2 + Common.WOODMAN_POSITION.z + Common.SHIP_RADIUS);

		

		m_mesh.SetActive(true);
		m_surface.BuildNavMesh();
		m_mesh.SetActive(false);
	}
}
