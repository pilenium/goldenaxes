﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Stage : MonoBehaviour {
	
	private StageWater			m_water;
	private StageWaterSuface	m_waterSurface;

	private StageNavMeshSurface	m_navSurface;

	private IntroClouds			m_introClouds;

	// data structures
	private List<StageTile>		m_tiles;
	private List<StageTile>		m_removingTiles;

	private List<Axe>			m_axes;
	private List<Axe>			m_removingAxes;

	// variables
	private DataChapter			m_chapterData;
	private	int					m_chapter		= -1;
	private int					m_stage			= -1;
	private float				m_speed			= 0.0f;
	private float				m_target_speed	= 0.0f;
	private float				m_distance		= 0.0f;
	private float				m_delta_dist	= 0.0f;

	// variables for calculate
	private float				m_tileHeight	= 0.0f;
	private float				m_tile_z		= 0.0f;

	// getter
	public	int					stage				{ get { return m_stage; } }
	public	DataChapter			dataChapter			{ get { return m_chapterData; } }
	public	float				speed				{ get { return m_speed; } }
	public	float				distance			{ get { return m_distance; } }
	public	float				deltaDistance		{ get { return m_delta_dist; } }
	public	int					axeCount			{ get { int ret = 0; foreach (Axe axe in m_axes) { if (axe.IsEnabled) ret++; } return ret; } }

	void Awake()
	{
		// create water
		GameObject waterSurfaceObject = GameObject.CreatePrimitive (PrimitiveType.Plane);
		m_waterSurface = waterSurfaceObject.AddComponent<StageWaterSuface> () as StageWaterSuface;

		GameObject waterObject = GameObject.CreatePrimitive (PrimitiveType.Plane);
		m_water = waterObject.AddComponent<StageWater> () as StageWater;

		m_tiles = new List<StageTile> ();
		m_removingTiles = new List<StageTile> ();

		m_axes = new List<Axe> ();
		m_removingAxes = new List<Axe> ();

		MeshCollider collider = GetComponent<MeshCollider> ();
		if (collider != null)
			collider.enabled = false;


		GameObject stageNavSurface = PrefabManager.LoadPrefab("Prefaps/Stage/StageNavMeshSurface");
		m_navSurface = stageNavSurface.GetComponent<StageNavMeshSurface>() as StageNavMeshSurface;

		// 작업중
		GameObject clouds = new GameObject();
		m_introClouds = clouds.AddComponent<IntroClouds>() as IntroClouds;
	}

	void OnDestroy()
	{
	//	Destroy (m_water);
	//	Destroy (m_waterSurface);
		m_water = null;
		m_waterSurface = null;

		m_tiles.Clear ();
		m_removingTiles.Clear ();

		m_axes.Clear ();
		m_removingAxes.Clear ();
	}

	public void InitCamera()
	{
		float camera_pos_y = 0f;
		float camera_pos_z = 0f;
		if(Common.SCREEN_RATIO <= Common.BASE_RATIO)
		{
			camera_pos_y = m_chapterData.stage_width * Common.BASE_RATIO / 2 / Mathf.Tan(Common.FOV_RAD_HALF) / Mathf.Cos(Common.FOV_RAD_HALF);
		}
		else
		{
			camera_pos_y = m_chapterData.stage_width * Common.SCREEN_RATIO / 2 / Mathf.Tan(Common.FOV_RAD_HALF) / Mathf.Cos(Common.FOV_RAD_HALF);

			RaycastHit hitInfo;

			GameUtil.ScreenToStage(Vector3.up * GamePlay.instance.ui.bottom.m_menu_height, out hitInfo);
			camera_pos_z = hitInfo.point.z;
		}



		//Camera.main.transform.l(m_water.gameObject.transform);
		Camera.main.transform.localPosition = new Vector3(0f, camera_pos_y, camera_pos_z);
		Camera.main.transform.rotation = m_chapterData.cameraRotation;

		Camera mainCamera = Camera.main;

		Vector3 pos = Camera.main.transform.position;
		Quaternion rot = Camera.main.transform.rotation;

		//float dist = 0f;

		//	if (mPlane.Raycast(ray, out dist))
		//Camera.main.ScreenToWorldPoint
//Camera.main.transform.LookAt(m_water.gameObject.transform);
		
	}


	public bool SetStage(int stage)
	{
		m_stage = stage;
		
		int new_chapter = Data.chapters.GetChapter(stage);

		if(m_chapter != new_chapter)
		{
			RemoveAllTiles();

			m_chapter = new_chapter;
			m_chapterData = Data.chapters.GetChapterData(m_chapter);

			m_water.SetChapter(m_chapterData);
			m_waterSurface.SetChapter(m_chapterData);
			m_navSurface.SetChapter(m_chapterData);


			//SetSpeed (m_chapterData.speed);
			InitCamera();

//			TweenPosition tweenPos = TweenPosition.Begin (Camera.main.gameObject, 2.0f, dataChapter.cameraPosition);
//			tweenPos.method = UITweener.Method.EaseInOut;
//			TweenRotation tweenRot = TweenRotation.Begin (Camera.main.gameObject, 2.0f, dataChapter.cameraRotation);
//			tweenRot.method = UITweener.Method.EaseInOut;
		}

		return true;
	}

	// change speed
	public void SetSpeed(float speed)
	{
		m_target_speed = speed;
	}
	/*
	// change speed
	public void SetSpeedDirect(float speed)
	{
		m_target_speed = speed;
		m_speed = speed;
	}
	*/

	// cloud
	public void OpenCloud(float speed)
	{
		SetSpeed(speed);
		m_introClouds.Open(speed);
	}

	public void CloseCloud()
	{
		SetSpeed(10.0f);
		m_introClouds.Close(10.0f);
	}


	public void AddAxe(Axe axe)
	{
		m_axes.Add(axe);
	}

	public bool RemoveAxe(Axe axe)
	{
		if (m_axes.Find(delegate(Axe find) {  return find == axe; } )) 
		{	
			m_removingAxes.Add(axe);
			return true;
		}
		else
		{
			PGTool.ResourceManager.instance.Return (axe);
			return false;
		}
	}

	// Update is called once per frame
	void Update () {
		
		m_speed = m_speed + (m_target_speed - m_speed) * Time.deltaTime * 5;

		UpdateMove();
		CheckDeleteTile();
	//	CheckNewTile();
		CheckTile();
		//CheckAxeCollision ();

/*
		if(dataChapter != null)
		{
			Camera.main.transform.position = Vector3.Lerp (Camera.main.transform.position, dataChapter.cameraPosition, Time.deltaTime);
			Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, dataChapter.cameraRotation, Time.deltaTime);
		}*/

		Vector3 pos = Camera.main.transform.position;
		Quaternion rot = Camera.main.transform.rotation;


	}

	private void UpdateMove()
	{
		m_delta_dist = m_speed * Time.deltaTime;

		m_distance += m_delta_dist;

		// 물 움직임
		m_water.Move(0.0f, m_delta_dist * 0.2f);
		m_waterSurface.Move(0.0f, m_delta_dist * 0.8f);

		foreach (StageTile tile in m_tiles)
		{
			// 타일 움직임
			tile.transform.Translate(Vector3.back * m_delta_dist);

			if(tile.transform.position.z < - tile.sizeZ - Data.settings.BG_SCROLL_BOTTOM_GAP)
			{
				m_removingTiles.Add(tile);
			}
			else if(tile.transform.position.z > m_chapterData.stage_height + Data.settings.BG_SCROLL_TOP_GAP)
			{
				m_removingTiles.Add(tile);

			}
		}
		m_tileHeight -= m_delta_dist;
	}

	private void RemoveAllTiles()
	{
		foreach (StageTile tile in m_tiles)
		{
			PGTool.ResourceManager.instance.Return(tile);
		}
		m_tiles.Clear();
	}

	private void CheckTile()
	{
		// check delete




		if(m_tiles.Count == 0) {
			AddTailTile();
		}

		// 아래로 (실제값은 -m_delta_dist)
		if(m_delta_dist > 0)
		{
			while(m_tiles[m_tiles.Count-1].transform.position.z + m_tiles[m_tiles.Count-1].sizeZ < m_chapterData.stage_height + Data.settings.BG_SCROLL_TOP_GAP)
			{
				AddTailTile();
			}
		}
		// 위로 (실제값은 m_delta_dist)
		else if(m_delta_dist < 0)
		{
			while(m_tiles[0].transform.position.z > -Data.settings.BG_SCROLL_BOTTOM_GAP)
			{
				AddHeadTile();
			}
		}
	}

	private void AddTailTile()
	{
		StageTile newTile = PGTool.ResourceManager.instance.Get (m_chapterData.bg, m_chapterData.bg) as StageTile;

		if(m_tiles.Count > 0)
		{
			StageTile lastTile = m_tiles[m_tiles.Count-1];
			newTile.transform.localPosition = lastTile.transform.localPosition;
			newTile.transform.Translate(Vector3.forward * lastTile.sizeZ);
		}
		m_tiles.Add(newTile);
	}

	private void AddHeadTile()
	{
		StageTile newTile = PGTool.ResourceManager.instance.Get (m_chapterData.bg, m_chapterData.bg) as StageTile;

		if(m_tiles.Count > 0)
		{
			StageTile firstTile = m_tiles[0];
			newTile.transform.localPosition = firstTile.transform.localPosition;
			newTile.transform.Translate(Vector3.back * firstTile.sizeZ);
		}
		m_tiles.Insert(0, newTile);
	}

	// BG
	private bool CheckNewTile()
	{
		// 높이 체크해서 새로운거 안 필요하면 종료
		if (m_tileHeight >= m_chapterData.stage_height + Data.settings.BG_SCROLL_TOP_GAP)
			return false;

		// url을 풀 키로 사용
		StageTile newTile = PGTool.ResourceManager.instance.Get (m_chapterData.bg, m_chapterData.bg) as StageTile;

		if (newTile == null) {
			//Error
			return false;
		}

		m_tileHeight += newTile.sizeZ;

		// align tile
		if (m_tiles.Count > 0) {
			StageTile prevScript = m_tiles[m_tiles.Count-1].GetComponent<StageTile> () as StageTile;

			newTile.transform.position = m_tiles[m_tiles.Count-1].transform.position;

			newTile.transform.Translate(Vector3.forward * prevScript.sizeZ);
		}

		m_tiles.Add (newTile);

		return true;
	}

	private void CheckDeleteTile()
	{
		foreach (StageTile tile in m_removingTiles) {
			m_tiles.Remove(tile);
			PGTool.ResourceManager.instance.Return (tile);
		}
		m_removingTiles.Clear ();

		foreach (Axe axe in m_removingAxes)
		{
			m_axes.Remove(axe);

			PGTool.ResourceManager.instance.Return (axe);
		}
		m_removingAxes.Clear ();
	}

}