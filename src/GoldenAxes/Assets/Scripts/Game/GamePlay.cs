﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class GamePlay : MonoBehaviour {
	
	public	static GamePlay instance;

//	public	UnityEngine.Component.Camera			camera;

	// game componets
	private	GameProperties	m_properties		= null;
	private	Stage			m_stage				= null;

	private	Woodman			m_woodman			= null;
	
	private AxeSlot				m_slotAxe		= null;
	private List<AxeSkillSlot>	m_slotSkills	= null;
	private List<GodSlot>		m_slotGods		= null;


	private MonsterManager	m_monsterManager	= null;
	private EffectManager	m_effectManager		= null;
	private EventManager	m_eventManager		= null;


	private	GameUI			m_ui				= null;

	private Mode			m_currentMode		= null;
	private List<Mode>		m_modes				= null;

	// touch
	private GameTouch		m_touch				= null;

	// variables
	//private int				m_current_axe_slot		= 1;
	//private AxeSkill		m_activated_axe_skill	= null;


	//private int m_testCount = 0;

	#if UNITY_EDITOR
	private	Console		m_console;

	public	Console		console { get { return m_console; } }
	#endif


	// getter
	public	GameProperties	properties			{ get	{ return m_properties;				} }
	public	Stage			stage				{ get	{ return m_stage;					} }
	public	Woodman			woodman				{ get	{ return m_woodman;					} }
	public	DataChapter		dataChapter			{ get	{ return stage.dataChapter;			} }
	public	GameUI			ui					{ get	{ return m_ui;						} }
	public	MonsterManager	monsterManager		{ get	{ return m_monsterManager;			} }
	public	EffectManager	effectManager		{ get	{ return m_effectManager;			} }
	public	Mode			mode				{ get	{ return m_currentMode;				} }

	public	GameTouch		touch				{ get { return m_touch; } }


	// 이녀석만 Start 나머진 Awake
	void Start() {

		GameObject data = GameObject.Find("Data");

		if (data == null) {
			UnityEngine.SceneManagement.SceneManager.LoadScene("SCTitle");
			return;
		}
		Init ();

//		RVO.Simulator.Instance.processObstacles();
	}

	void OnDestroy()
	{
		if (m_properties != null) {
			m_properties.Clear ();
			m_properties = null;
		}

		if (m_monsterManager != null) {
			m_monsterManager.Clear ();
			m_monsterManager = null;
		}

		if (m_effectManager != null) {
			m_effectManager.Clear ();
			m_effectManager = null;
		}

		if (m_eventManager != null) {
			m_eventManager.Clear ();
			m_eventManager = null;
		}

		if(m_slotAxe != null) {
			m_slotAxe.Clear();
			m_slotAxe = null;
		}

		if (m_slotSkills != null) {
			foreach (AxeSlot slot in m_slotSkills) {
				slot.Clear ();
			}
			m_slotSkills.Clear ();
		}

		if (m_slotGods != null) {
			foreach (GodSlot slot in m_slotGods) {
				slot.Clear ();
			}
			m_slotGods.Clear ();
		}

		if (m_modes != null) {
			foreach (Mode mode in m_modes) {
				mode.Clear ();
			}
			m_modes.Clear ();
		}
	}

	/*
	private IEnumerator InitWait(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		Init();
	}*/

	private void Init()
	{	
		// ================================================
		// INIT SYSTEM
		// ------------------------------------------------
		// Singletone
		instance = this;


		// ------------------------------------------------
		// Control
		m_touch = gameObject.AddComponent<GameTouch> () as GameTouch;


		// ------------------------------------------------
		// Event mask
		Camera.main.eventMask = 1 << LayerMask.NameToLayer ("Water");


		// ------------------------------------------------
		// Properties
		m_properties = new GameProperties ();


		// ------------------------------------------------
		// Managers
		m_monsterManager	= new MonsterManager ();
		m_effectManager		= new EffectManager ();
		m_eventManager		= new EventManager();
		// ------------------------------------------------
		// Stage
		m_stage = gameObject.AddComponent<Stage> () as Stage;


		// ------------------------------------------------
		// UI
		m_ui = GameObject.FindObjectOfType<GameUI>() as GameUI;
		m_ui.Init();
		GamePlay.instance.ui.top.SetStage(Data.user.stage.Get());

		// init resolution
		InitResolution();


		// ------------------------------------------------
		// Init Stage
		m_stage.SetStage(Data.user.stage.Get());


		// ------------------------------------------------
		// Woodman
		m_woodman = Woodman.Alloc();
		m_woodman.SetHat(Data.user.slotHat.Get());
		m_woodman.SetCloth(Data.user.slotCloth.Get());
		m_woodman.SetShip(Data.user.slotShip.Get());


		// ------------------------------------------------
		// Axe Slots
		m_slotAxe = new AxeSlot(Data.user.slotAxeTime.Get());
		m_ui.axe.InitAxeSlot(m_slotAxe);
		SetAxe(Data.user.slotAxe.Get());

		AxeSkillSlot axeSlot;
		int axe_id;
		int slot_level;
		int slot_time;
		m_slotSkills = new List<AxeSkillSlot>();
		for(int i = 0 ; i < Common.MAX_SKILL_SLOT ; ++i)
		{
			axe_id = Data.user.slotSkillAxe.Get(i);
			slot_level = Data.user.slotSkillLevel.Get(i);
			slot_time = Data.user.slotSkillTime.Get(i);

			axeSlot = new AxeSkillSlot(i, slot_time);
			m_slotSkills.Add(axeSlot);

			// ui에 슬롯 세팅 고쳐야해~
			m_ui.axe.InitSkillSlot(i, axeSlot);

			// 슬롯에 도끼 세팅
			if(axe_id > 0 && slot_level > 0)
			{
				SetAxeSkill(i, axe_id);
			}
		}
		m_properties.CalculateWoodman();
		// ------------------------------------------------
		// God Slots
		GodSlot godSlot;
		int god_id;
		m_slotGods = new List<GodSlot> ();
		for(int i = 0 ; i < Common.MAX_GOD_SLOT ; ++i)
		{
			god_id = Data.user.slotGod.Get(i);
			slot_level = Data.user.slotGodLevel.Get(i);

			godSlot = GodSlot.Create (i);
			m_slotGods.Add(godSlot);

			if(god_id > 0 && slot_level > 0)	SetGod(i, god_id);
		}
		m_properties.CaculateGods();


		// ------------------------------------------------
		// init Modes
		Mode mode;
		m_modes = new List<Mode> ();
		for(int i = 0 ; i <= (int)Mode.TYPE.BOSS_OUTRO ; ++i)
		{
			mode = Mode.Create((Mode.TYPE)i);
			mode.onAddPattern = this.OnAddPattern;
			m_modes.Add(mode);
		}

		// init UI values
		GamePlay.instance.ui.top.CheckCoin();
		GamePlay.instance.ui.top.CheckDamages();


		// ------------------------------------------------
		// Start Game
	//	m_stage.InitCamera();
		m_properties.CaculateMonsters();
		SetMode (Mode.TYPE.TITLE);

		#if UNITY_EDITOR
		InitConsole();
		#endif

	}

	// init resolution
	private void InitResolution()
	{
		Common.SCREEN_RATIO = (float)Screen.height / (float)Screen.width;
		Common.FOV_RAD = Camera.main.fieldOfView * Mathf.PI / 180;
		Common.FOV_RAD_HALF = Common.FOV_RAD / 2;

		if(Common.SCREEN_RATIO > Common.BASE_RATIO)
		{
			float height_margin = (Screen.height - Screen.width * Common.BASE_RATIO) / Screen.height;
			Camera.main.rect = new Rect(0, height_margin / 2, 1f, 1f - height_margin / 2);
		}

		ui.InitResolution();
	}

	// stage control

	public void ClearStage()
	{
		int nextStage = Data.chapters.GetNextStage(Data.user.stage.Get());
		
		SetStage(nextStage);
	}

	public void FailStage()
	{
		int prevStage = Data.chapters.GetPrevStage(Data.user.stage.Get());
		SetStage(prevStage, false);

	}

	public void SetStage(int stage, bool next = true)
	{
		bool sameChapter = Data.chapters.IsSameChapter(stage, Data.user.stage.Get());

		// save data
		Data.user.stage.Set(stage);
		// set ui
		GamePlay.instance.ui.top.SetStage(stage);
		// calulate properties
		m_properties.CaculateMonsters();


		if(sameChapter)
		{
			DataDifficulty dataDifficulty = dataChapter.GetDifficulty(stage);
			switch(dataDifficulty.type)
			{
				case DataDifficulty.TYPE.BATTLE:
					ModeBattleIntro modeBattelIntro = SetMode(Mode.TYPE.BATTLE_INTRO) as ModeBattleIntro;
					modeBattelIntro.SetNext(next);
				break;

				case DataDifficulty.TYPE.BOSS:
					ModeBossIntro modeBossIntro = SetMode(Mode.TYPE.BOSS_INTRO) as ModeBossIntro;
					modeBossIntro.SetNext(next);
				break;
			}
		}
		else
		{
			ModeChangeChapter modeChangeChapter = SetMode(Mode.TYPE.CHANGE_CHAPTER) as ModeChangeChapter;
			modeChangeChapter.SetNext(next);
		}
	}



	// axe slot
	public void SetAxe(int axe_id)
	{
		m_slotAxe.SetAxe(axe_id);
	}
	public void ResetAxe()
	{
		m_slotAxe.ResetAxe ();
	}

	public AxeSlot GetAxeSlot()
	{
		return m_slotAxe;
	}

	public void SetAxeSkill(int index, int axe_id)
	{
		if (index >= Common.MAX_SKILL_SLOT) {
			// ERROR
		}
		
		m_slotSkills[index].SetAxe(axe_id);
	}

	public void ResetSkill(int index)
	{
		m_slotSkills[index].ResetAxe ();
	}

	public AxeSkillSlot GetAxeSkill(int index)
	{
		return m_slotSkills[index];
	}

	public void ResetWaitControls(int ignoreControlIndex)
	{
		if(m_slotAxe.controlIndex != ignoreControlIndex)
		{
			m_slotAxe.ResetWaitControl();
		}
		
		for(int i = 0 ; i < Common.MAX_SKILL_SLOT ; ++i)
		{
			if (m_slotSkills[i].controlIndex != ignoreControlIndex) {
				m_slotSkills[i].ResetWaitControl();
			}
		}
	}

	// god slot
	public void SetGod(int index, int god_id)
	{
		if (index >= Common.MAX_GOD_SLOT) {
			// ERROR
		}
		m_slotGods[index].SetGod(god_id);
	}
	public void ResetGod(int index)
	{
		m_slotGods[index].ResetGod();
	}

	public GodSlot GetGodSlot(int index)
	{
		return m_slotGods[index];
	}

	public void RefreshGodProperties()
	{
		foreach(GodSlot slot in m_slotGods)
		{
			slot.RefreshProperties();
		}
	}

	// item slots
	public void SetHat(int hat_id)
	{
		Data.user.slotHat.Set(hat_id);
		m_woodman.SetHat(hat_id);
	}

	public void SetCloth(int cloth_id)
	{
		Data.user.slotCloth.Set(cloth_id);
		m_woodman.SetCloth(cloth_id);
	}

	public void SetShip(int ship_id)
	{
		Data.user.slotShip.Set(ship_id);
		m_woodman.SetShip(ship_id);
		
	}

	// mode
	public Mode SetMode(Mode.TYPE type)
	{
		if (m_currentMode != null) 
		{
			if (m_currentMode.type == type) return null;

			m_currentMode.Clear ();
		}

		m_currentMode = m_modes [(int)type];

		m_currentMode.Init ();

		return m_currentMode;
	}

	// event listeners
	private void OnAddPattern(Pattern pattern)
	{
		/*
		foreach (GodSlot slot in m_godSlots) {
			slot.CheckMonster();
		}
		*/
	}

	// coin
	public void AddCoin(double coin)
	{
		Data.user.coin.Add (coin);
		GamePlay.instance.ui.top.CheckCoin();
	}


	// Update is called once per frame
	void Update() {
		
		// 이벤트 돌려주고 (암것도 안하자나??)
		if (m_eventManager != null) {
			m_eventManager.Update (Time.deltaTime);
		}

		// 이펙트 돌려주고 (암것도 안하자나??)
		if (m_effectManager != null) {
			m_effectManager.Update ();
		}

		// 모드 돌려주고
		if (m_currentMode != null) {
			m_currentMode.Update(Time.deltaTime);
		}

		// 도끼 슬롯 돌려주고
		if(m_slotAxe != null)
		{
			m_slotAxe.Update(Time.deltaTime);
		}

		if (m_slotSkills != null) {
			foreach (AxeSlot slot in m_slotSkills) {
				slot.Update(Time.deltaTime);
			}
		}
		
		if(m_monsterManager != null) {
//			m_monsterManager.Update(Time.deltaTime);
		}

		// 산신령 슬롯 돌려주고
		if (m_slotGods != null) {
			foreach (GodSlot slot in m_slotGods) {
				slot.Update(Time.deltaTime);
			}
		}


	}


	// gameplay
	public void HitWater(Axe axe)
	{
		m_currentMode.HitWater();

		Vector3 effectPos = new Vector3 (axe.transform.position.x, Common.POS_EFFECT_Y, axe.transform.position.z);

		EffectAnimation effect = effectManager.AddEffect<EffectAnimation> ("Prefaps/Effect/Effect_Drop", effectPos, true) as EffectAnimation;
		effect.transform.localScale = axe.GetModel ().transform.localScale;
		effect.SetColor(dataChapter.water_light_color);

		// TODO 코인 값 넣어야 함
		//AddCoin(1, axe.transform.position, Random.Range(0.0f, Mathf.PI));

		//m_monsterManager.UpdateMonsterGoal(axe.transform.position);

	}

	public void AddCoin(double coin, Vector3 position, float degree, float power_h = 2.0f, float power_v = 12.0f, float gravity = 40.0f, float delay = 0f)
	{
		EffectCoin effect = effectManager.AddEffect<EffectCoin> ("Prefaps/Effect/EffectCoin", position, true) as EffectCoin;
		effect.InitCoin (coin);
		effect.InitVelocity(degree, power_h, power_v, gravity, delay);

//		effectManager.AddCoinEffect (coin, position, degree, power_h, power_v, gravity, delay);

		AddCoin (coin);
	}


	public void HitMonster(Monster monster, Axe axe)
	{
		// to-do
		m_currentMode.HitMonster (monster);
	}

	public void KillMonster(Monster monster)
	{
		// to-do
		m_currentMode.KillMonster (monster);
	}

	public void ReleaseMonster(Monster monster)
	{
		// to-do
		m_currentMode.ReleaseMonster (monster);
	}
	
	public void MonsterAttack()
	{
		Debug.Log("!!");
	}





	// DEBUG CONSOLE
	#if UNITY_EDITOR
	public void InitConsole()
	{
		m_console = gameObject.AddComponent<Console> () as Console;
	}

	public void ExcuteCommand(string cmd)
	{
		string command = "";
		string param1 = "";
		//string param2 = "";


		string[] strs = cmd.Split(' ');

		if (strs.Length >= 1) {
			command = strs [0];

			if (strs.Length >= 2) {
				param1 = strs [1];

				if (strs.Length >= 3) {
					//param2 = strs [2];
				}
			}
		} else {
			return;
		}

		if (command.ToLower() == "stage") {
			int stage = int.Parse (param1);
			SetStage (stage);
		}

		if (command.ToLower() == "coin") 
		{
			int coin = int.Parse (param1);
			Data.user.coin.Set(coin);
			GamePlay.instance.ui.top.CheckCoin();
		}

		if (command.ToLower() == "speed") {
			float speed = float.Parse (param1);
			stage.SetSpeed(speed);
		}

		if (command.ToLower() == "cloud") {
			if(param1.ToLower() == "open")
			{
				stage.OpenCloud(2.0f);
			}
			else if(param1.ToLower() == "close")
			{
				stage.CloseCloud();
			}
		}


		if (command.ToLower() == "reset") {
			PlayerPrefs.DeleteAll ();
			UnityEngine.SceneManagement.SceneManager.LoadScene ("SCTitle");
		}

	}
	#endif
}
