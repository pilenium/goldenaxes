﻿using System;
using System.Collections;
using UnityEngine;

public class GameTime {
	
	private static int m_server_timestamp;

	public static void SetServerTimestamp(int timestamp)
	{
		m_server_timestamp = timestamp;
	}

	public static int GetNowTimestamp()
	{
		return m_server_timestamp + (int) Mathf.Floor(Time.time);
	}

	public static int GetTimeStamp(DateTime date)
	{
		DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
		TimeSpan diff = date - origin;


		return (int) ( Math.Floor (diff.TotalSeconds) );
	}

	public static double ConvertToUnixTimestamp(DateTime date)
	{
		DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
		TimeSpan diff = date - origin;
		return Math.Floor(diff.TotalSeconds);
	}

}
