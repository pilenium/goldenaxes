﻿using UnityEngine;
using System.Collections;

public class ModeBossOutro : Mode {
	
	private float			m_time;

	public ModeBossOutro() : base (TYPE.BOSS_OUTRO)
	{
		
	}

	public override void Init()
	{
		m_time = 3.0f;
	}

	public override void Clear()
	{
	}

	public override void Update(float dt)
	{
		m_time -= dt;
		if(m_time <= 0.0f)
		{
			GamePlay.instance.SetMode(Mode.TYPE.CHAPTER_INTRO);
		}
	}
}