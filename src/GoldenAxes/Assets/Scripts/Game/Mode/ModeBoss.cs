﻿using UnityEngine;
using System.Collections;

public class ModeBoss : Mode {

	public ModeBoss() : base (TYPE.BOSS)
	{

	}

	public override void Init()
	{
		GamePlay.instance.ui.top.SetProgressBoss();
		GamePlay.instance.ui.top.SetProgress(1.0f);
		GamePlay.instance.stage.SetSpeed (0.0f);


		DataDifficulty difficulty = GamePlay.instance.dataChapter.GetDifficulty(Data.user.stage.Get());
		if(difficulty == null)
		{
			// TODO error
			return;
		}


		AddMonsterPattern(difficulty.GetPatternLevel(0f), GamePlay.instance.properties.bossHP, GamePlay.instance.dataChapter.speed, GamePlay.instance.dataChapter.monsterIDs);
	}

	public override void Clear()
	{
		RemoveAllPatterns ();
	}

	public override void Update(float dt)
	{

	}

	public override void HitMonster(Monster monster)
	{
		GamePlay.instance.ui.top.SetProgress ((float)(monster.health / monster.healthMax));
	}

	public override void KillMonster(Monster monster)
	{
		//	AddStack (m_data.stack_inc);
		GamePlay.instance.ui.top.SetProgress (0.0f);
	}


	public override void EndPattern (Pattern pattern)
	{
	//	ModeBossOutro mode = GamePlay.instance.SetMode(Mode.TYPE.BOSS_OUTRO) as ModeBossOutro;
		
		if (pattern.success) {
//			GamePlay.instance.SetMode(Mode.TYPE.BOSS_OUTRO);
			GamePlay.instance.ClearStage();
		} else {
//			GamePlay.instance.SetMode (Mode.TYPE.BATTLE);
			GamePlay.instance.FailStage();
		}
		
		base.EndPattern (pattern);
	}
}