﻿using UnityEngine;
using System.Collections;

public class ModeBossIntro : Mode {
	
	private UIBossIntro m_intro_ui = null;

	public ModeBossIntro() : base (TYPE.BOSS_INTRO)
	{

	}

	public override void Init()
	{
		GameObject ui = GamePlay.instance.ui.AddUI("Prefaps/UI/BossIntro");
		m_intro_ui = ui.GetComponent<UIBossIntro>() as UIBossIntro;
		m_intro_ui.onEndAnimation = this.OnEndAnimation;

		GamePlay.instance.ui.top.SetProgressBoss();
		GamePlay.instance.ui.top.SetProgress(1.0f);
	}

	public void SetNext(bool next)
	{
		if(next)
		{
			GamePlay.instance.stage.SetSpeed(Common.SPEED_INTRO_NEXT);
		}
		else
		{
			GamePlay.instance.stage.SetSpeed(Common.SPEED_INTRO_PREV);
		}
	}


	public override void Clear()
	{
		if(m_intro_ui != null)
		{
			GameObject.Destroy(m_intro_ui.gameObject);
			m_intro_ui = null;
		}
	}

	public void OnEndAnimation()
	{
		GamePlay.instance.SetMode(Mode.TYPE.BOSS);
	}
}