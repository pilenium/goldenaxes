﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Mode {
	public enum TYPE
	{
		TITLE,
		CHANGE_CHAPTER,
		CHAPTER_INTRO,
		BATTLE_INTRO,
		BATTLE,
		BOSS_INTRO,
		BOSS,
		BOSS_OUTRO
	}

	public static Mode Create(TYPE type)
	{
		Mode ret = null;

		switch (type) {
		case Mode.TYPE.TITLE:
			ret = new ModeTitle();
			break;

		case Mode.TYPE.CHANGE_CHAPTER:
			ret = new ModeChangeChapter();
		break;

		case Mode.TYPE.CHAPTER_INTRO:
			ret = new ModeChapterIntro();
			break;

		case Mode.TYPE.BATTLE_INTRO:
			ret = new ModeBattleIntro();
			break;
			
		case Mode.TYPE.BATTLE:
			ret = new ModeBattle();
			break;

		case Mode.TYPE.BOSS_INTRO:
			ret = new ModeBossIntro();
			break;

		case Mode.TYPE.BOSS:
			ret = new ModeBoss();
			break;

		case Mode.TYPE.BOSS_OUTRO:
			ret = new ModeBossOutro();
			break;
		}

		return ret;
	}


	// Event Listener
	public delegate void OnAddPattern(Pattern pattern);

	public OnAddPattern onAddPattern		= null;

	protected	TYPE		m_type;
	public		TYPE		type { get { return m_type; } }


	// TODO 풀 기능으로 pattern처리하면 더 좋다.
	private	List<Pattern>	m_patterns;



	public Mode(TYPE type)
	{
		m_type = type;
		m_patterns = new List<Pattern> ();
	}
	~Mode()
	{
		Clear ();
		m_patterns = null;
		onAddPattern = null;
	}
	public virtual void Init()
	{
		//Clear ();
	}

	public virtual void Clear()
	{
		foreach(Pattern pattern in m_patterns)
		{
			pattern.Clear ();
		}
		m_patterns.Clear ();
	}

	public virtual void Update(float dt)
	{
	}
	public virtual void HitWater()
	{
	}
	public virtual void HitDiver()
	{
	}
	public virtual void HitMonster(Monster monster)
	{
	}
	public virtual void KillMonster(Monster monster)
	{
	}
	public virtual void ReleaseMonster(Monster monster)
	{
	}
	public virtual void EndPattern (Pattern pattern)
	{
		m_patterns.Remove (pattern);
		pattern.Clear ();
	}
	public virtual void RemoveAllPatterns()
	{
		foreach (Pattern pattern in m_patterns) {
			if(pattern != null)
			{
				pattern.Destory ();
			}
		}
		m_patterns.Clear ();
	}
	public virtual void AddMonsterPattern(int patternLevel, double helath, DataChapterSpeed speed, DataMonsterIDs monsterIDs)
	{
		Pattern pattern = Pattern.Create();
		pattern.Init(patternLevel, helath, speed, monsterIDs);
		
		m_patterns.Add(pattern);

		if (onAddPattern != null) {
			onAddPattern (pattern);
		}
	}
	/*
	public virtual void AddMonsterPattern(string type, int level, double helath, float speed, int monsterID)
	{
		Pattern pattern = Pattern.Alloc (type);
		pattern.Init (type, level, helath, speed, monsterID);

		m_patterns.Add (pattern);

		if (onAddPattern != null) {
			onAddPattern (pattern);
		}
	} */
}
