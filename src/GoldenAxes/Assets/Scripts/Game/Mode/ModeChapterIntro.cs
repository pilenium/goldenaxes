﻿using UnityEngine;
using System.Collections;

public class ModeChapterIntro : Mode {
	
	private float			m_time;

	public ModeChapterIntro() : base (TYPE.CHAPTER_INTRO)
	{
		
	}

	public override void Init()
	{
		GamePlay.instance.stage.CloseCloud();
		m_time = 5.0f;
	}

	public override void Clear()
	{
	}

	public override void Update(float dt)
	{
		m_time -= dt;
		if(m_time <= 0.0f)
		{
			GamePlay.instance.stage.OpenCloud(2.0f);
			GamePlay.instance.SetMode(Mode.TYPE.BATTLE_INTRO);
		}
	}
}