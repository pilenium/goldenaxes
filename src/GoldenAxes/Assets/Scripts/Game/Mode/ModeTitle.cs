﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeTitle : Mode {

	private UITitle			m_titleUI = null;

	public ModeTitle() : base (TYPE.TITLE)
	{
	}

	public override void Init()
	{
		base.Init ();

		GamePlay.instance.stage.SetSpeed (GamePlay.instance.dataChapter.speed.battle);

		GamePlay.instance.touch.SetEnabled (false);

//		GamePlay.instance.ui.title.Show (true);

		// move woodman
//		GamePlay.instance.woodman.MoveIntro();


		// ui
		GameObject ui = GamePlay.instance.ui.AddUI("Prefaps/UI/Title");
		m_titleUI = ui.GetComponent<UITitle>() as UITitle;
	}

	public override void Clear()
	{
		base.Clear ();

		//GamePlay.instance.EquipAxe (0, false);

		GamePlay.instance.touch.SetEnabled (true);

		if(m_titleUI != null)
		{
			GameObject.Destroy(m_titleUI.gameObject);
			m_titleUI = null;
		}
	}

	public void ShowUI()
	{
		GamePlay.instance.stage.OpenCloud(2.0f);
		
		GamePlay.instance.ui.top.Show(true);
		GamePlay.instance.ui.axe.Show(true);
		GamePlay.instance.ui.bottom.Show(true);
		GamePlay.instance.ui.management.Show(true);
	}
	public void BeginGame()
	{
		// move woodman
		//GamePlay.instance.woodman.Move(Common.WOODMAN_POSITION, true, 1.5f, UITweener.Method.EaseInOut);
		// set mode
		GamePlay.instance.SetStage(Data.user.stage.Get());
	}

}
