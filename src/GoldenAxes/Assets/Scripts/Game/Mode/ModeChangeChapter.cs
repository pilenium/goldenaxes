﻿using UnityEngine;
using System.Collections;

public class ModeChangeChapter : Mode {

	private float m_time = 0f;
	private float m_intro_time = 2.0f;
	private float m_outro_time = 2.0f;

	private bool m_isIntro = true;
	private bool m_isNext = true;
	
	public ModeChangeChapter() : base (TYPE.CHANGE_CHAPTER)
	{
	}

	public override void Init()
	{
		m_isIntro = true;
		GamePlay.instance.stage.CloseCloud();
		m_time = m_intro_time;
	}

	public override void Clear()
	{
	}

	public void SetNext(bool next)
	{
		m_isNext = next;

		if(next)
		{
			GamePlay.instance.stage.SetSpeed(Common.SPEED_INTRO_NEXT);
		}
		else
		{
			GamePlay.instance.stage.SetSpeed(Common.SPEED_INTRO_PREV);
		}
	}

	public override void Update(float dt)
	{
		m_time -= dt;
		if(m_time <= 0.0f)
		{
			if(m_isIntro)
			{
				GamePlay.instance.stage.SetStage(Data.user.stage.Get());
				m_time = m_outro_time;
				m_isIntro = false;

			}
			else
			{
				GamePlay.instance.stage.OpenCloud(2.0f);

				DataDifficulty dataDifficulty = GamePlay.instance.dataChapter.GetDifficulty(Data.user.stage.Get());
				switch(dataDifficulty.type)
				{
					case DataDifficulty.TYPE.BATTLE:
						ModeBattleIntro modeBattelIntro = GamePlay.instance.SetMode(Mode.TYPE.BATTLE_INTRO) as ModeBattleIntro;
						modeBattelIntro.SetNext(m_isNext);
					break;

					case DataDifficulty.TYPE.BOSS:
						ModeBossIntro modeBossIntro = GamePlay.instance.SetMode(Mode.TYPE.BOSS_INTRO) as ModeBossIntro;
						modeBossIntro.SetNext(m_isNext);
					break;
				}
			}
		}
	}
}