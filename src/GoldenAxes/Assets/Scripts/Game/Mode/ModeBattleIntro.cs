﻿using UnityEngine;
using System.Collections;

public class ModeBattleIntro : Mode {

	private UIStageIntro m_intro_ui = null;
	
	public ModeBattleIntro() : base (TYPE.BATTLE_INTRO)
	{
	}

	public override void Init()
	{
		GameObject ui = GamePlay.instance.ui.AddUI("Prefaps/UI/StageIntro");
		m_intro_ui = ui.GetComponent<UIStageIntro>() as UIStageIntro;
		m_intro_ui.onEndAnimation = this.OnEndAnimation;

		GamePlay.instance.ui.top.SetProgressNormal();
		GamePlay.instance.ui.top.SetProgress (0.0f);
	}

	public void SetNext(bool next)
	{
		if(next)
		{
			GamePlay.instance.stage.SetSpeed(Common.SPEED_INTRO_NEXT);
		}
		else
		{
			GamePlay.instance.stage.SetSpeed(Common.SPEED_INTRO_PREV);
		}
	}

	public override void Clear()
	{
		if(m_intro_ui != null)
		{
			GameObject.Destroy(m_intro_ui.gameObject);
			m_intro_ui = null;
		}
	}

	public void OnEndAnimation()
	{
		GamePlay.instance.SetMode(Mode.TYPE.BATTLE);
	}
}