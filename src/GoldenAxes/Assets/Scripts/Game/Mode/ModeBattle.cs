﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModeBattle : Mode {

	private float			m_stack;
	private	float			m_combo;
	private bool			m_prefect = false;

	public ModeBattle () : base (TYPE.BATTLE)
	{

	}

	public override void Init()
	{
		m_stack = 0.0f;
		m_combo = 0;

		GamePlay.instance.ui.top.SetProgressNormal();
		GamePlay.instance.ui.top.SetProgress(0.0f);
		GamePlay.instance.stage.SetSpeed(GamePlay.instance.dataChapter.speed.battle / 2);

		AddNewPattern();
	}

	public override void Clear()
	{
		RemoveAllPatterns ();
	}

	public override void HitWater()
	{
		if (m_combo > 0) {
			ClearCombo ();
			//GamePlay.instance.ui.right.RemoveCombo (0.1f);
			m_prefect = false;
		}
	}

	public override void HitMonster(Monster monster)
	{
		AddCombo ();
	}


	public override void KillMonster(Monster monster)
	{
		//	AddStack (m_data.stack_inc);
	}

	public override void EndPattern (Pattern pattern)
	{
	//	ClearCombo ();

		if (pattern.success)
		{
			// TODO 이거이상해
			AddStack(GamePlay.instance.dataChapter.stack_inc);
		}
		else
		{
			RemoveStack(GamePlay.instance.dataChapter.stack_inc * 2);
		}


		// 스택이 작으면
		// 꽉 참
		if(m_stack <= 0)
		{
			RemoveAllPatterns();
			GamePlay.instance.FailStage();
		}
		else if(m_stack >= Common.MAX_STACK)
		{
			m_stack = Common.MAX_STACK;
			//GamePlay.instance.SetMode (Mode.TYPE.BOSS_INTRO);
			GamePlay.instance.ClearStage();
		}
		else
		{
			AddNewPattern();
		}
	}

	private void AddNewPattern()
	{
		m_combo = 0;
		m_prefect = true;


		DataDifficulty difficulty = GamePlay.instance.dataChapter.GetDifficulty(Data.user.stage.Get());
		if(difficulty == null)
		{
			// TODO error
			return;
		}


		AddMonsterPattern(difficulty.GetPatternLevel(m_stack / Common.MAX_STACK), GamePlay.instance.properties.monsterHP, GamePlay.instance.dataChapter.speed, GamePlay.instance.dataChapter.monsterIDs);
	}

	private void AddStack(float stack)
	{
		m_stack += stack;

		GamePlay.instance.ui.top.SetProgress (m_stack / Common.MAX_STACK);
	}

	private void RemoveStack(float stack)
	{
		m_stack -= stack;

		// 0됨
		if (m_stack <= 0) {
			m_stack = 0;
		}

		GamePlay.instance.ui.top.SetProgress (m_stack / Common.MAX_STACK);
	}

	private void AddCombo()
	{
		m_combo++;
		//GamePlay.instance.ui.right.SetCombo (m_combo);

		/*
		switch (m_combo) {
		case 5:
			GamePlay.instance.ui.right.SetResult ("good");
			break;
		case 10:
			GamePlay.instance.ui.right.SetResult ("nice");
			break;
		case 15:
			GamePlay.instance.ui.right.SetResult ("wow");
			break;
		case 20:
			GamePlay.instance.ui.right.SetResult ("awesome");
			break;
		}
		*/
	}

	private void ClearCombo()
	{
		m_combo = 0;
	}

}
