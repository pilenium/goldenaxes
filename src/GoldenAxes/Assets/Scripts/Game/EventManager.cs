﻿using UnityEngine;
using System.Collections;


public class EventManager {

	private float m_time = 0.0f;

	public EventManager()
	{
	}

	public void Clear()
	{
	}

	public void Update(float dt)
	{
		m_time -= dt;

		if (m_time < 0.0f) {
			m_time = 5.0f;
			//GamePlay.instance.monsterManager.AddDiver();
		}

	}

}
