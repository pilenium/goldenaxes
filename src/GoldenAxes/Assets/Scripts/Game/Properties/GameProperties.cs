﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameProperties {
	
	public double		damage		{ get; protected set; }

	// woodman
	public	double		throwDMG	{ get; private set; }


	// monster
	public	double		monsterHP	{ get; private set; }
	public	double		monsterGold	{ get; private set; }
	public	double		bossHP		{ get; private set; }
	public	double		bossGold	{ get; private set; }

	// buff
	private	List<Buff>		m_buffs;
	private	List<Abillity>	m_buffAbillities;


	public GameProperties()
	{
		m_buffs = new List<Buff>();
		m_buffAbillities = new List<Abillity>();
	}

	~GameProperties()
	{
		m_buffs = null;
		foreach(Abillity ability in m_buffAbillities)
		{
			ability.Clear();
		}
		m_buffAbillities.Clear();
	}

	public void Clear()
	{
	}

	public void CaculateAll()
	{
		CalculateWoodman();
		CaculateGods();
		CaculateMonsters();
	}




	// ----------------------------------------------------------
	// woodman properties

	public void CalculateWoodman()
	{
		throwDMG = GetWoodmanDamage(Data.user.slotAxeLevel.Get());
	}

	// woodman damage
	public double GetWoodmanDamage(int level)
	{
		double dmg = Data.properties.woodman.dmg
			* level
			* Data.properties.woodman.GetDmgMultiplier (level)
			* GetDamageBuff();
			
			//* GamePlay.instance.abilityManager.buff_dmg_mul
			//+ GamePlay.instance.abilityManager.buff_dmg_add;

		return dmg;
	}

	// woodman upgrade costs
	public double GetWoodmanUpgradePrice(int level, int buyCount)
	{
		double cost = Data.properties.woodman.coin
			* ( Math.Pow(Data.properties.woodman.coinMultiplier, level + buyCount)	- Math.Pow(Data.properties.woodman.coinMultiplier, level) )
			/ (Data.properties.woodman.coinMultiplier - 1);

		// 100언더면 올림 해주자 (0.1 방지)
		//if(cost < 100) cost = Math.Ceiling(cost);	
		return Math.Ceiling(cost);
		//return cost;
	}
	public int GetWoodmanUpgradeCount(int level, double coin)
	{
		double a = coin * (Data.properties.woodman.coinMultiplier - 1) / Data.properties.woodman.coin / Math.Pow(Data.properties.woodman.coinMultiplier, level) + 1;
		double count = Math.Log(a, Data.properties.woodman.coinMultiplier);
		return (int)Math.Floor(count);
	}

	public double GetWoodmanSkillUpgradePrice(int index, int level)
	{
		SlotProperty slotProperty = Data.properties.woodman.GetSlotProperty(index);		
		double cost = slotProperty.cost * Math.Pow(slotProperty.multiplier, level);
		return Math.Ceiling(cost);
	}

	public double GetWoodmanSkillUpgradePrice(int index, int level, int buyCount)
	{
		SlotProperty slotProperty = Data.properties.woodman.GetSlotProperty(index);		
		//double cost = slotProperty.cost * Math.Pow(slotProperty.multiplier, level);

		double cost = slotProperty.cost
			* ( Math.Pow(slotProperty.multiplier, level + buyCount)	- Math.Pow(slotProperty.multiplier, level) )
			/ (slotProperty.multiplier - 1);

		return Math.Ceiling(cost);
	}
	public int GetWoodmanSkillUpgradeCount(int index, int level, double coin)
	{
		SlotProperty slotProperty = Data.properties.woodman.GetSlotProperty(index);
		double a = coin * (slotProperty.multiplier - 1) / slotProperty.cost / Math.Pow(slotProperty.multiplier, level) + 1;
		double count = Math.Log(a, slotProperty.multiplier);
		return (int)Math.Floor(count);
		
	}



	// ----------------------------------------------------------
	public void CaculateGods()
	{
		// nothing to do
		GamePlay.instance.RefreshGodProperties();
	}

	public double GetGodDamage(int index, int level)
	{
		double dmg = Data.properties.god.GetSlotCost(index)
			* level
			* Data.properties.god.GetDmgMultiplier (level);
		return dmg;
	}

	public double GetGodUpgradePrice(int index, int level, int buyCount)
	{
		double cost = Data.properties.god.GetSlotCost(index)
			* ( Math.Pow(Data.properties.god.coinMultiplier, level + buyCount)	- Math.Pow(Data.properties.god.coinMultiplier, level) )
			/ (Data.properties.god.coinMultiplier - 1);

		return Math.Ceiling(cost);
	}
	public int GetGodUpgradeCount(int index, int level, double coin)
	{
		double a = coin * (Data.properties.god.coinMultiplier - 1) / Data.properties.god.GetSlotCost(index) / Math.Pow(Data.properties.god.coinMultiplier, level) + 1;
		double count = Math.Log(a, Data.properties.god.coinMultiplier);
		return (int)Math.Floor(count);
	}
	

	// ----------------------------------------------------------
	public double GetTreasureUpgradePrice(int level, int buyCount)
	{
		double cost = Data.treasure.properties.upgradeBase
			* ( Math.Pow(Data.treasure.properties.upgradeMultiplier, level + buyCount)	- Math.Pow(Data.treasure.properties.upgradeMultiplier, level) )
			/ (Data.treasure.properties.upgradeMultiplier - 1);

		return Math.Ceiling(cost);
	}
	public int GetTreasureUpgradeCount(int level, double relic)
	{
		double a = relic * (Data.treasure.properties.upgradeMultiplier - 1) / Data.treasure.properties.upgradeBase / Math.Pow(Data.treasure.properties.upgradeMultiplier, level) + 1;
		double count = Math.Log(a, Data.treasure.properties.upgradeMultiplier);
		return (int)Math.Floor(count);
	}

	// ----------------------------------------------------------

	public void CaculateMonsters()
	{
		int stage = Data.user.stage.Get();

		monsterHP =
			Data.properties.monster.hpBase
			* Mathf.Pow(Data.properties.monster.hpMultiplier, Mathf.Min(stage, Data.properties.monster.hpHurdleStage))
			* Mathf.Pow(Data.properties.monster.hpHurdleMultiplier, Mathf.Max(stage - Data.properties.monster.hpHurdleStage, 0));


		monsterGold =
			monsterHP
			* Data.properties.monster.goldBase;

		bossHP =
			monsterHP
			* Data.properties.monster.hpBoss.GetHP(stage);

		bossGold =
			bossHP
			* monsterGold
			* Data.properties.monster.goldBoss;

		monsterGold = Math.Ceiling(monsterGold);
		bossGold = Math.Ceiling(bossGold);
	}

	public void AddBuff(Buff buff)
	{
		m_buffs.Add(buff);
		CaculateAll();
	}
	public void RemoveBuff(Buff buff)
	{
		if(m_buffs.Remove(buff))
		{
			CaculateAll();
		}
	}

	public float GetDamageBuff()
	{
		float ret = 1f;
		foreach(Buff buff in m_buffs)
		{
			ret *= buff.dmg_multiplier;
		}
		return ret;
	}

	public void AddBuffAbillity(Abillity buffAbillity)
	{
		m_buffAbillities.Add(buffAbillity);
	}

	public void RemoveBuffAbillity(Abillity buffAbillity)
	{
		m_buffAbillities.Remove(buffAbillity);
	}

	public List<Abillity> GetBuffAbillities()
	{
		return m_buffAbillities;
	}


}
