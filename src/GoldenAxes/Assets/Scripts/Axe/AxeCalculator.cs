﻿using System;
using UnityEngine;

public class AxeCalculator
{
	// 계산 공식은
	// http://blog.naver.com/PostView.nhn?blogId=dlsrks107&logNo=20087535210
	// 에서 참고하였다.

	public static Vector3 CalculateVelocity(Vector3 throwPos, Vector3 targetPos, float gravity, float cos, float sin)
	{
		Vector3 retVelocity = new Vector3 ();


		float distanceX = targetPos.x - throwPos.x;
		float distanceZ = targetPos.z - throwPos.z;
		float distance = Mathf.Sqrt (Mathf.Pow (distanceX, 2.0f) + Mathf.Pow (distanceZ, 2.0f));

		// v0는 대각선 최단거리로 계산한다 (dz -> x, y -> y)
		float v0 = Mathf.Sqrt(gravity * Mathf.Pow(distance, 2) / 2 / (distance * sin * cos + throwPos.y * Mathf.Pow(cos, 2)));

		// v0를 xyz로 다시 나눠준다
		retVelocity.y = v0 * sin;
		retVelocity.x = v0 * cos * distanceX / distance;
		retVelocity.z = v0 * cos * distanceZ / distance;

		return retVelocity;
	}

	public static float CalculateTime(Vector3 throwPos, Vector3 targetPos, float gravity, float cos, float sin)
	{
		float distanceX = targetPos.x - throwPos.x;
		float distanceZ = targetPos.z - throwPos.z;
		float distance = Mathf.Sqrt (Mathf.Pow (distanceX, 2.0f) + Mathf.Pow (distanceZ, 2.0f));

		// v0는 대각선 최단거리로 계산한다 (dz -> x, y -> y)
		float v0 = Mathf.Sqrt(gravity * Mathf.Pow(distance, 2) / 2 / (distance * sin * cos + throwPos.y * Mathf.Pow(cos, 2)));

		return ( v0 * sin + Mathf.Sqrt ( Mathf.Pow (v0, 2) * Mathf.Pow (sin, 2) + 2 * gravity * throwPos.y) ) / gravity;
	}

}
