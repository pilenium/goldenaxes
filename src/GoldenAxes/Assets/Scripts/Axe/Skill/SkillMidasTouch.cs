﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
	
public class SkillMidasTouch : Skill {

	/**
	* <summary>화면상의 몬스터들을 골드 몬스터로 전환</summary>
	* <param name="m_param1">몬스터 골드 * param1 골드 타격시 획득</param>
	*/

	public SkillMidasTouch(DataAxeSlot data, int level) : base (data, level)
	{
	}

	~SkillMidasTouch()
	{
	}

	public override void Active(Vector3 position)
	{
		List<Monster> monsters;

		GamePlay.instance.monsterManager.GetMonsters(out monsters);
	
		foreach (Monster monster in monsters) {
		//	monster.Stun(m_param1);
			ConditionGoldening condition = monster.SetCondition(Condition.TYPE.GOLDENING, m_data.activeTime) as ConditionGoldening;
			condition.SetGoldMultiplier(m_param1);
		}
	}
}
