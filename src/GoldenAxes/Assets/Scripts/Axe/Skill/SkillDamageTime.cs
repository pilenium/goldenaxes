﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDamageTime : Skill {

	protected	Buff		m_buff;

	public SkillDamageTime(DataAxeSlot data, int level) : base (data, level)
	{
		m_buff = new Buff();
		m_buff.dmg_multiplier = m_param1;
	}

	~SkillDamageTime()
	{
		GamePlay.instance.properties.RemoveBuff(m_buff);
		m_buff = null;
	}

	public override void Active(Vector3 position)
	{
		GamePlay.instance.woodman.SetBuff ("Prefaps/Effect/Effect_BuffDmgMul");

		//dmg_mul = m_data.param1;
		GamePlay.instance.properties.AddBuff(m_buff);
	}
	public override void DeActive()
	{
		GamePlay.instance.woodman.RemoveBuff ();
		
		GamePlay.instance.properties.RemoveBuff(m_buff);
	}
}