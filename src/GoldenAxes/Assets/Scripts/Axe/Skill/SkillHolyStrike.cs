﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillHolyStrike : Skill {

	/**
	* <summary>범위 안의 몬스터들을 스턴</summary>
	* <param name="m_param1">스턴 시간</param>
	* <param name="m_param2">범위</param>
	*/
	public SkillHolyStrike(DataAxeSlot data, int level) : base (data, level)
	{
	}

	~SkillHolyStrike()
	{
	}

	public override void Active(Vector3 position)
	{
		List<Monster> monsters;


		GamePlay.instance.monsterManager.MultiHit (position, m_param2, out monsters);
	
		foreach (Monster monster in monsters) {
//			monster.Stun(m_param1);
			monster.SetCondition(Condition.TYPE.STUN, m_param1);
		}

		Effect effect = GamePlay.instance.effectManager.AddEffect<EffectAnimator>("Prefaps/Effect/EffectHolyStrike", position, false);

		effect.gameObject.transform.localScale = new Vector3(m_param2, 1.0f, m_param2);
	}
}
