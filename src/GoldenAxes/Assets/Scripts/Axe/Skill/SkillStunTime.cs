﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillStunTime : Skill {

	protected	Abillity		m_buffAbillity;

	public SkillStunTime(DataAxeSlot data, int level) : base (data, level)
	{
		m_buffAbillity = Abillity.Create("STUN", m_param1, m_param2, m_param3);
	}

	~SkillStunTime()
	{
		GamePlay.instance.properties.RemoveBuffAbillity(m_buffAbillity);
		m_buffAbillity.Clear();
		m_buffAbillity = null;
	}

	public override void Active(Vector3 position)
	{
		GamePlay.instance.woodman.SetBuff ("Prefaps/Effect/Effect_BuffDmgMul");

		//dmg_mul = m_data.param1;
		GamePlay.instance.properties.AddBuffAbillity(m_buffAbillity);
	}
	public override void DeActive()
	{
		GamePlay.instance.woodman.RemoveBuff ();
		
		GamePlay.instance.properties.RemoveBuffAbillity(m_buffAbillity);
	}
}
