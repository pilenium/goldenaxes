﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skill  {
	public enum TYPE
	{
		THROW,
		ASSIST,
		SPECIAL,
		ULTIMATE,
		COUNT
		
	}

	public static Skill Create(DataAxeSlot data, int level)
	{
		Skill ret = null;

		switch (data.type)
		{
		case "SCALE_THROW":
			ret = new SkillScaleThrow(data, level);
			break;

		case "HOLY_STRIKE":
			ret = new SkillHolyStrike(data, level);
			break;

		case "MIDAS_TOUCH":
			ret = new SkillMidasTouch(data, level);
			break;

		

		case "DMG_TIME":
			ret = new SkillDamageTime(data, level);
			break;

		case "AXE_RAIN":
			ret = new SkilllAxeRain(data, level);
			break;

		case "STUN_TIME":
			ret = new SkillStunTime(data, level);
			break;

		case "STUN_ALL":
			ret = new SkillStunAll(data, level);
			break;

		case "HIT_GOLD_TIME":
			ret = new SkillHitGoldTime(data, level);
			break;
		}

		return ret;
	}

	protected	DataAxeSlot		m_data;
	protected	int				m_level;
	protected	float			m_param1;
	protected	float			m_param2;
	protected	float			m_param3;

	public Skill(DataAxeSlot data, int level)
	{
		m_data = data;
		m_level = level;
		m_param1 = m_data.GetParam1(m_level);
		m_param2 = m_data.GetParam2(m_level);
		m_param3 = m_data.GetParam3(m_level);

	}

	~Skill()
	{
		m_data = null;
	}

	public virtual void Clear()
	{
	}

	public virtual void Active(Vector3 position)
	{
	}

	public virtual void UpdateActivate (float dt)
	{
	}

	public virtual void DeActive()
	{
	}
}
