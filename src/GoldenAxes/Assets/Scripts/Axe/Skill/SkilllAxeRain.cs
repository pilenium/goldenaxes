﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkilllAxeRain : Skill {

	private Monster			m_target;
	private DataAxeModel	m_modelData;

	private float			m_time;

	private float			m_height;

	

	public SkilllAxeRain(DataAxeSlot data, int level) : base (data, level)
	{
		m_modelData = Data.axeModel.GetData(m_data.model);
		// degree를 높이로 (라디안이였으니 다시 카테시안으로)
		m_height = m_modelData.degree / Mathf.PI * 180.0f;
	}

	~SkilllAxeRain()
	{
	}

	public override void UpdateActivate (float dt)
	{
		m_time -= dt;

		if(m_time <= 0f)
		{
			// TODO 업그레이드 반영해야함
			m_time = m_param1;
			DropAxe();
		}
		
	}

	private void DropAxe()
	{
		// 타겟 없거나 살아 있지 않으면 재 검색
		if(m_target == null || m_target.IsLive() == false)
		{
			m_target = GamePlay.instance.monsterManager.GetRandomMonster();
		}

		// 그래도 없는 경우 (스테이지에 몬스터가 없다.)
		// 다음 루프에서 바로 재 검색 하게 시간 0으로
		if(m_target == null)
		{
			m_time = 0.0f;
			return;
		}

		Vector3 throwPos = m_target.position;
		throwPos.y = m_height;

		float original_time = AxeCalculator.CalculateTime (throwPos, m_target.position, m_modelData.gravity, m_modelData.cos, m_modelData.sin);
		Vector3 targetPos = m_target.position + m_target.DeltaPosition (original_time);


		targetPos.y = m_height;

		DropAxe axe = Axe.Create(m_data, m_level) as DropAxe;
		// TODO 업그레이드 반영해야함
		axe.Init (m_data.model, GamePlay.instance.properties.throwDMG * m_param2, targetPos, targetPos);
		GamePlay.instance.stage.AddAxe(axe);
	}

	public override void Active(Vector3 position)
	{
		m_time = 0f;
	}

	public override void DeActive()
	{
		m_target = null;
	}
}
