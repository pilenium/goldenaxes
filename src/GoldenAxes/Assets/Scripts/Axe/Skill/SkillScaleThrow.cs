﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillScaleThrow : Skill {

	/**
	* <summary>커다란 도끼 던짐</summary>
	* <param name="m_param1">param1배 커짐</param>
	*/

	public SkillScaleThrow(DataAxeSlot data, int level) : base (data, level)
	{
		
	}

	~SkillScaleThrow()
	{

	}

	public override void Active(Vector3 position)
	{
		// TODO

		// 도끼 던지기
		ScaledAxe axe = Axe.Create(m_data, m_level) as ScaledAxe;
		axe.InitScale (m_param1);
		// 데미지는 같게 하자 일단
		axe.Init (m_data.model, GamePlay.instance.properties.throwDMG, Common.THROW_POSITION, position);
		GamePlay.instance.stage.AddAxe(axe);

		// 나무꾼 
		float deg = Mathf.Atan2(position.x - Common.THROW_POSITION.x, position.z - Common.THROW_POSITION.z);
		GamePlay.instance.woodman.ThrowAxe (deg);
	}
}
