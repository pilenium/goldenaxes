﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashAxe : Axe {

	protected override void CheckMonsterHit()
	{
		// 일단 맞은 1녀석을 찾는다.
		Monster targetMonster = GamePlay.instance.monsterManager.Hit(this);

		if (targetMonster == null) {
			return;
		}

		// 그담에 주변 외곽을 찾음..
		List<Monster> monsters;

		Bounds bounds = m_collider.bounds;
		Vector3 size = bounds.size;
		size.x *= 3.0f;
		size.z *= 3.0f;
		bounds.size = size;

		GamePlay.instance.monsterManager.MultiHit (bounds, out monsters);
	
		foreach (Monster monster in monsters) {
			if (monster == targetMonster) {
				Hit(monster);
			} else {
				monster.Hit (GamePlay.instance.properties.throwDMG / 2, this);
			}
		}
	}
}
