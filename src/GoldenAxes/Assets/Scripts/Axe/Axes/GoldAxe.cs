﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class GoldAxe : Axe {

	protected override void Hit(Monster monster)
	{
		base.Hit(monster);
		// 코인 추가
		GamePlay.instance.AddCoin(Math.Ceiling(GamePlay.instance.properties.monsterGold * m_param1), m_tr.position, UnityEngine.Random.value * 2 * Mathf.PI, 2, 10, 40, 0f);
	}
}
