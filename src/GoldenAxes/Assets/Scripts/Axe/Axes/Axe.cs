﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using PGTool;

/// <summary>
/// 스테이지에 던지는 도끼 기본형
/// </summary>

public class Axe : Resource  {
	
	public static Axe Create(DataAxeSlot dataSlot, int level)
	{
		return Create(dataSlot.model, dataSlot.type, dataSlot.GetParam1(level), dataSlot.GetParam2(level), dataSlot.GetParam3(level));
	}

	public static Axe Create(int modelID, string type, float param1, float param2, float param3)
	{
		Axe ret;
		DataAxeModel dataModel = Data.axeModel.GetData(modelID);

		// 모델 타입으로 
		switch(type)
		{
			// 자유낙하
			case "DROP":
				ret = ResourceManager.instance.Get<DropAxe>(type+"_AXE_"+modelID) as Axe;
			break;

			// 자유낙하
			case "SCALED":
				ret = ResourceManager.instance.Get<ScaledAxe> (type+"_AXE_"+modelID) as Axe;
			break;

			// 포물선
			default:
				ret = ResourceManager.instance.Get<Axe> (type+"_AXE_"+modelID) as Axe;
				break;
		}
		
		// add ability
		Abillity abillity = Abillity.Create(type, param1, param2, param3);
		ret.AddAbillity(abillity);

		return ret;
	}

/*	public static Axe Create(string type, int axeModelID)
	{
		Axe ret;

		switch (type)
		{
			case "STURN":
				ret = ResourceManager.instance.Get<SturnAxe> (type+"_AXE_"+axeModelID) as SturnAxe;
				break;

			case "HIT_GOLD":
				ret = ResourceManager.instance.Get<GoldAxe> (type+"_AXE_"+axeModelID) as GoldAxe;
				break;

			case "SPLASH":
				ret = ResourceManager.instance.Get<SplashAxe> (type+"_AXE_"+axeModelID) as SplashAxe;
				break;

			case "SCALED":
				ret = ResourceManager.instance.Get<ScaledAxe> (type+"_AXE_"+axeModelID) as ScaledAxe;
				break;

			case "DROP":
				ret = ResourceManager.instance.Get<DropAxe> (type+"_AXE_"+axeModelID) as DropAxe;
				break;

			default:
				ret = ResourceManager.instance.Get<Axe> (type+"_AXE_"+axeModelID) as Axe;
				break;
		}

		return ret;
	}*/

	protected	DataAxeModel	m_dataModel;
	public		DataAxeModel	dataModel	{ get { return m_dataModel; } }

	protected	Transform		m_tr		= null;

	protected	AxeModel		m_axe		= null;
	protected	Transform		m_axe_tr	= null;

	protected	GameObject		m_shadow	= null;
	protected	Transform		m_shadow_tr	= null;

	protected	Effect			m_effect	= null;

	protected	BoxCollider		m_collider	= null;
	protected	Rigidbody		m_rigidBody	= null;

	protected	bool			m_enabled	= false;
	public		bool			IsEnabled	{ get { return m_enabled; } }

	// 물리 관련
	protected	float			m_time		= 0.0f;
	protected	Vector3			m_velocity;

	protected	Vector3			m_rotate;
	protected	Vector3			m_delta_rotate;



	// 도끼 관련
	protected	double			m_damage;
	protected	float			m_param1;
	protected	float			m_param2;
	protected	float			m_param3;

	protected	List<Abillity>	m_abillities;

	public		AxeModel		GetModel()		{ return m_axe; }
	public		BoxCollider		GetCollider()	{ return m_collider; }

	public		double			GetDamage()		{ return m_damage; }

	

	public override void Awake() 
	{
		base.Awake ();
		m_tr = gameObject.GetComponent<Transform> ();
		m_abillities = new List<Abillity>();
	}

	protected virtual void OnDestory()
	{
		m_tr = null;
		m_abillities.Clear();
		m_abillities = null;
	}

	public override void EndUse ()
	{
		base.EndUse ();

		if (m_effect != null) {
			m_effect.Stop ();
			m_effect = null;
		}

		m_abillities.Clear();
	}

	public void Init(int modelID, double damage, Vector3 throwPosition, Vector3 targetPosition)
	{
		DataAxeModel data = Data.axeModel.GetData(modelID);

		if (data == null) {
			// ERROR
		}

		m_damage = damage;

		InitModel(data, throwPosition);

		targetPosition = AlignPosition(targetPosition);
		InitVelocity(throwPosition, targetPosition);
		InitRotate(m_velocity.z);
		InitEffect (dataModel.effect);
	}

	protected virtual void InitModel(DataAxeModel data, Vector3 throwPosition)
	{
		// init data
		m_dataModel = data;

		// init tag
		tag = Common.TAG_AXE;

		// make axe
		if (m_axe == null) {
			m_axe = AxeModel.Create (dataModel.prefab);
			m_axe_tr = m_axe.GetComponent<Transform> () as Transform;
			m_axe_tr.localRotation = Quaternion.Euler (dataModel.throw_init_rot);
			m_axe_tr.parent = m_tr;
		}

		// make shadow
		if (m_shadow == null) {
			m_shadow = GameObject.CreatePrimitive (PrimitiveType.Quad);
			Collider shadowCollider = m_shadow.GetComponent<Collider> () as Collider;
			Destroy (shadowCollider);

			Material newMat = Resources.Load (dataModel.shadow, typeof(Material)) as Material;
			MeshRenderer renderer = m_shadow.GetComponent<MeshRenderer> () as MeshRenderer;
			renderer.material = newMat;

			m_shadow_tr = m_shadow.GetComponent<Transform> () as Transform;
			m_shadow_tr.localScale = Vector3.one * dataModel.size.x;
			m_shadow_tr.Rotate (90, 0, 0);
			m_shadow_tr.parent = m_tr;
		}

		// make collidor
		if(m_collider == null) {
			m_collider = gameObject.AddComponent<BoxCollider>() as BoxCollider;
			m_collider.size = dataModel.size;
			m_collider.center = Vector3.zero;
			//m_collider.isTrigger = true;
		}

		// make rigidbody
		if (m_rigidBody == null) {
			m_rigidBody = gameObject.AddComponent<Rigidbody> () as Rigidbody;
			m_rigidBody.useGravity = false;
			m_rigidBody.isKinematic = true;
		}

		// init values
		m_time = 0.0f;
		m_enabled = true;
		m_collider.enabled = true;

		m_rotate = Vector3.one;

		m_tr.position = throwPosition;
		m_tr.localRotation = Quaternion.identity;
	}

	protected virtual Vector3 AlignPosition(Vector3 position)
	{
		Vector3 ret = position;

		// align position
		DataChapter chapterData = GamePlay.instance.stage.dataChapter;

		if (position.x < -(chapterData.stage_width / 2 - dataModel.size.x / 2)) ret.x = -(chapterData.stage_width / 2 - dataModel.size.x / 2);
		if (position.x > chapterData.stage_width / 2 - dataModel.size.x / 2) ret.x = chapterData.stage_width / 2 - dataModel.size.x / 2;

		if (position.z > chapterData.stage_height - dataModel.size.z / 2) ret.z = chapterData.stage_height - dataModel.size.z / 2;

		return ret;
	}

	protected virtual void InitVelocity(Vector3 throwPosition, Vector3 targetPosition)
	{
		m_velocity = AxeCalculator.CalculateVelocity (throwPosition, targetPosition, dataModel.gravity, dataModel.cos, dataModel.sin);
	}

	protected virtual void InitRotate(float velocity_z)
	{
		m_delta_rotate = velocity_z * dataModel.throw_rot;
	}


	protected virtual void InitEffect (DataEffect data)
	{
		if(data == null)
			return;
		
		switch(data.type) {

			case "TRAIL":
				m_effect = GamePlay.instance.effectManager.AddEffect<EffectTrail>(data.url, m_axe_tr.position, false);
				break;

			case "PARTICLE":
				m_effect = GamePlay.instance.effectManager.AddEffect<EffectParticle>(data.url, m_axe_tr.position, false);
				break;

		}
	}

	protected virtual void Update ()
	{
		m_time += Time.deltaTime;

		// update physics
		UpdateSimplePhysics ();
		//UpdateNormalPhysics ();

		// 도끼 회전
		m_rotate += m_delta_rotate * Time.deltaTime;
		m_axe_tr.localRotation = Quaternion.Euler (dataModel.throw_init_rot);
		m_axe_tr.Rotate (m_rotate);
		// 궤적 회전 있을경우
		if (dataModel.trail_rot) {
			m_axe_tr.Rotate (Quaternion.LookRotation (m_velocity).eulerAngles, Space.World);
		}


		// 그림자 회전
		m_shadow_tr.localPosition = new Vector3(0, - m_tr.position.y, 0);
		m_shadow_tr.rotation = Quaternion.Euler(90, m_axe_tr.localRotation.eulerAngles.y, 0);
		if (dataModel.trail_rot) {
			m_shadow_tr.rotation = Quaternion.Euler (90, m_rotate.y + Quaternion.LookRotation (m_velocity).eulerAngles.y, 0);
		}

		// 이펙트 회전
		if (m_effect != null) {
			m_effect.transform.localRotation = Quaternion.Euler (m_rotate);
			m_effect.transform.Rotate (m_rotate);

			// 궤적 회전 있을경우
			if (dataModel.trail_rot) {
				m_effect.transform.Rotate (Quaternion.LookRotation (m_velocity).eulerAngles, Space.World);
			}

			m_effect.transform.localRotation = Quaternion.LookRotation (m_velocity);
			m_effect.transform.position = m_effect.transform.localRotation * dataModel.effect.pos + m_tr.position;
		}


		if (m_enabled) {
			CheckMonsterHit ();
		}
		CheckWaterHit ();
	}

	protected virtual void CheckWaterHit()
	{
		// water check
		if (m_tr.position.y <= 0f) {
			OnHitWater();
		}
	}

	protected virtual void CheckMonsterHit()
	{
		// hitCheck
		Monster monster = GamePlay.instance.monsterManager.Hit(this);
		if (monster != null) {
			Hit (monster);
		}
	}


	protected virtual void UpdateSimplePhysics ()
	{
		// 오차가 0.1~0.2정도 있는데 이 부분은 나중에 수정해보자...

		Vector3 d_velocity = m_velocity * Time.deltaTime;

		// 보정
		if (d_velocity.y < - m_tr.position.y) {
			// xz부터 보정해주고 (계산 순서 땜시)
			d_velocity.x = -d_velocity.x * m_tr.position.y / d_velocity.y;
			d_velocity.z = - d_velocity.z * m_tr.position.y / d_velocity.y;

			// y를 마지막으로 보정해주자.
			d_velocity.y = - m_tr.position.y;
		}

		m_tr.Translate (d_velocity, Space.World);

		m_velocity.y -= dataModel.gravity * Time.deltaTime;
	}

	protected virtual void UpdateNormalPhysics ()
	{
		Vector3 d_velocity = new Vector3 ();
		d_velocity.x = m_velocity.x * Time.deltaTime;
		d_velocity.z = m_velocity.z * Time.deltaTime;
		d_velocity.y = m_velocity.y * Time.deltaTime + dataModel.gravity * Mathf.Pow(Time.deltaTime, 2) / 2;

		if (d_velocity.y < - m_tr.position.y) {
			// xz부터 보정해주고 (계산 순서 땜시)
			d_velocity.x = -d_velocity.x * m_tr.position.y / d_velocity.y;
			d_velocity.z = - d_velocity.z * m_tr.position.y / d_velocity.y;

			// y를 마지막으로 보정해주자.
			d_velocity.y = - m_tr.position.y;
		}

		m_tr.Translate (d_velocity, Space.World);

		m_velocity.y = m_velocity.y - dataModel.gravity *  Time.deltaTime;
	}

	protected virtual void OnHitWater()
	{
		if (m_enabled)
		{
			m_enabled = false;
		}

		GamePlay.instance.stage.RemoveAxe (this);

	}


	/*
	protected void OnTriggerEnter(Collider coll)
	{
		GameObject monster = coll.gameObject;

		if (m_enabled == false)
			return;

		if (monster.tag != Common.TAG_MONSTER) return;

		//Hit (monster);

	}
	*/

	protected virtual void Hit(Monster monster)
	{
		// 어빌리티 젤 먼저
		foreach(Abillity ability in m_abillities)
		{
			ability.Hit(monster);
		}

		//
		monster.Hit (m_damage, this);

		if(m_enabled) {
			// 도끼 초기화 
			m_collider.enabled = false;

			m_enabled = false;

			if (m_tr.position.x < 0) {
				m_velocity.x = -GamePlay.instance.dataChapter.stage_width * 1.5f;

			} else {
				m_velocity.x = GamePlay.instance.dataChapter.stage_width * 1.5f;
			}
			m_velocity.y = -m_velocity.y * 1.5f;
			m_velocity.z = m_velocity.z * 1.5f;
		}
	}

	public virtual Abillity AddAbillity(DataAxeSlot dataSlot, int level)
	{
		Abillity abillity = Abillity.Create(dataSlot.type, dataSlot.GetParam1(level), dataSlot.GetParam2(level), dataSlot.GetParam3(level));

		AddAbillity(abillity);

		return abillity;
	}
	public virtual void AddAbillity(Abillity abillity)
	{
		if(abillity != null)
		{
			m_abillities.Add(abillity);
			abillity.Init(this);
		}
	}
}