﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropAxe : Axe {

	private Vector3			m_throw_rot;

	protected override void InitModel(DataAxeModel data, Vector3 throwPosition)
	{
		base.InitModel (data, throwPosition);


		m_throw_rot = Vector3.zero;

		if (data.throw_rot.x != 0) {
			m_throw_rot.x = Random.Range(0.0f, 360.0f);
		}
		if (data.throw_rot.y != 0) {
			m_throw_rot.y = Random.Range(0.0f, 360.0f);
		}
		if (data.throw_rot.z != 0) {
			m_throw_rot.z = Random.Range(0.0f, 360.0f);
		}
	}

	protected override Vector3 AlignPosition(Vector3 position)
	{
		// 체크 하지 않는다 (초기값을 믿자)
		return position;
	}


	protected override void InitVelocity(Vector3 throwPosition, Vector3 targetPosition)
	{
		// 암것도 하지 않는다 (자유낙하)
		m_velocity = Vector3.down * 20;
	}

	protected override void InitRotate(float velocity_z)
	{
		m_delta_rotate = dataModel.throw_rot;
	}

	protected override void Update ()
	{
		base.Update();

		m_axe_tr.localRotation = Quaternion.Euler (dataModel.throw_init_rot);

		Vector3 delta_rotate = m_throw_rot + m_rotate;

		m_axe_tr.Rotate (delta_rotate);
		m_shadow_tr.localRotation = Quaternion.Euler(90, -delta_rotate.z, 0);
	}
}
