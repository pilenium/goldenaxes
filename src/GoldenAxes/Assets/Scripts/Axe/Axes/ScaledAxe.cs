﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PGTool;

public class ScaledAxe : Axe {

	private float		m_scale;

	public void InitScale(float scale)
	{
		m_scale = scale;
	}
	protected override void InitModel(DataAxeModel data, Vector3 throwPosition)
	{
		base.InitModel (data, throwPosition);

		m_axe_tr.transform.localScale = Vector3.one;
		m_shadow_tr.localScale = Vector3.one * dataModel.size.x;
		m_collider.size = data.size * m_scale;


		TweenScale.Begin (m_axe.gameObject, 0.4f, Vector3.one * m_scale).method = UITweener.Method.EaseOut;
		TweenScale.Begin (m_shadow, 0.4f, Vector3.one * data.size.x * m_scale).method = UITweener.Method.EaseOut;
	}

	public override void EndUse ()
	{
		base.EndUse ();

	}

	protected override Vector3 AlignPosition(Vector3 position)
	{
		Vector3 ret = position;

		// align position
		DataChapter chapterData = GamePlay.instance.stage.dataChapter;

		if (position.x < -(chapterData.stage_width / 2 - dataModel.size.x * m_scale / 2)) ret.x = -(chapterData.stage_width / 2 - dataModel.size.x * m_scale / 2);
		if (position.x > chapterData.stage_width / 2 - dataModel.size.x * m_scale / 2) ret.x = chapterData.stage_width / 2 - dataModel.size.x * m_scale / 2;

		if (position.z > chapterData.stage_height - dataModel.size.z * m_scale / 2) ret.z = chapterData.stage_height - dataModel.size.z * m_scale/ 2;

		return ret;
	}

	protected override void InitRotate(float velocity_z)
	{
		m_delta_rotate = velocity_z * dataModel.throw_rot  / m_scale / 2;
	}

	protected override void CheckMonsterHit()
	{
		List<Monster> monsters;

		if (GamePlay.instance.monsterManager.MultiHit (this, out monsters)) {

			foreach (Monster monster in monsters) {
				Hit (monster);
			}
		}

	}

}
