﻿using UnityEngine;
using System.Collections;

public class AxeModel : MonoBehaviour {
	
	public static AxeModel Create(string prefab)
	{
		GameObject obj = GameObject.Instantiate (Resources.Load(prefab)) as GameObject;

		AxeModel ret = obj.AddComponent<AxeModel> () as AxeModel;

		return ret;
	}

}
