﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbillityStun : Abillity {

	public AbillityStun(float param1, float param2, float param3) : base(param1, param2, param3)
	{
	}

	~AbillityStun()
	{
	}
	public override Abillity Clone()
	{
		AbillityStun ret = new AbillityStun(m_param1, m_param2, m_param3);
		return ret;
	}

	public override void Hit(Monster monster)
	{
		if(Random.value < m_param2)
		{
			monster.SetCondition(Condition.TYPE.STUN, m_param1);
		}
	}
}
