﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbillityHitGold : Abillity {

	public AbillityHitGold(float param1, float param2, float param3) : base(param1, param2, param3)
	{
	}

	~AbillityHitGold()
	{
	}
	
	public override Abillity Clone()
	{
		AbillityHitGold ret = new AbillityHitGold(m_param1, m_param2, m_param3);
		return ret;
	}

	public override void Hit(Monster monster)
	{
		GamePlay.instance.AddCoin(Math.Ceiling(GamePlay.instance.properties.monsterGold * m_param1), m_axe.transform.position, UnityEngine.Random.value * 2 * Mathf.PI, 2, 10, 40, 0f);
	}

}
