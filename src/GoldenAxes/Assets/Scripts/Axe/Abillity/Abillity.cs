﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abillity {

	public static Abillity Create(string type, float param1, float param2, float param3)
	{
		Abillity ret = null;

		switch (type)
		{
			case "STUN":
				ret = new AbillityStun(param1, param2, param3);
				break;

			case "HIT_GOLD":
				ret = new AbillityHitGold(param1, param2, param3);
				break;

			case "SPLASH":
				ret = new AbillitySplash(param1, param2, param3);
				break;
		}

		return ret;
	}

	protected	float	m_param1, m_param2, m_param3;
	protected	Axe		m_axe;

	public Abillity(float param1, float param2, float param3)
	{
		m_param1 = param1;
		m_param2 = param2;
		m_param3 = param3;
	}

	~Abillity()
	{
		m_axe = null;
	}

	public virtual void Init(Axe axe)
	{
		m_axe = axe;
	}

	public virtual void Hit(Monster monster)
	{

	}

	public virtual void Clear()
	{
		m_axe = null;
	}

	public virtual Abillity Clone()
	{
		Abillity ret = new Abillity(m_param1, m_param2, m_param3);
		return ret;
	}
}
