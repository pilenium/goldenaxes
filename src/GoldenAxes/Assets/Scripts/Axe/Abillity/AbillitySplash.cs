﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbillitySplash : Abillity {

	public AbillitySplash(float param1, float param2, float param3) : base(param1, param2, param3)
	{
	}

	~AbillitySplash()
	{
	}
	
	public override Abillity Clone()
	{
		AbillitySplash ret = new AbillitySplash(m_param1, m_param2, m_param3);
		return ret;
	}

	public override void Hit(Monster monster)
	{
		// 주변 외곽을 찾음..
		List<Monster> monsters;

		Bounds bounds = m_axe.GetCollider().bounds;
		Vector3 size = bounds.size;
		size.x *= m_param1;
		size.z *= m_param1;
		bounds.size = size;

		GamePlay.instance.monsterManager.MultiHit (bounds, out monsters);
	
		foreach (Monster extra_monster in monsters) {
			if (extra_monster != monster) {
				monster.Hit(m_axe.GetDamage() * m_param1, m_axe);
			}
		}
	}
}