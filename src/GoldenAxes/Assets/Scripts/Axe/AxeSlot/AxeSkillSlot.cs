﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxeSkillSlot : AxeSlot {

	protected	int		m_index;
	public		int		index		{ get { return m_index; } }
	protected	Skill	m_skill;


	public AxeSkillSlot(int index, int lastTime) : base (lastTime)
	{
		m_index = index;
		offline = true;
	}

	public override void UpdateActivate (float dt)
	{
		base.UpdateActivate(dt);
		
		if(m_skill != null)	m_skill.UpdateActivate(dt);
	}

	public override void SetDataUserTime(int time)
	{
		Data.user.slotSkillTime.Set(m_index, time);
	}

	public override bool SetAxe(ItemAxe itemAxe)
	{
		m_axe = itemAxe;

		SetData(m_axe);

		if(m_dataSlot == null)
		{
			return false;
		}

		m_control = GamePlay.instance.touch.InitControl(m_controlIndex, m_dataSlot.control);
		if(m_control != null)
		{
			m_control.AddTargetListener(OnTarget);
		}
		if (onSetAxe != null) {
			onSetAxe (m_axe);
		}

		InitStatus();

		// 스킬 만들어서 장착하자
		m_skill = Skill.Create(m_dataSlot, itemAxe.level);

		return true;
	}
	
	protected override void SetData(ItemAxe itemAxe)
	{
		m_dataSlot = itemAxe.data.skillSlot;
	}


	public override bool Active()
	{
		// base 랑 다르게 간다
		// 레디일 경우
		if (IsStatus (AxeSlot.STATUS.READY))
		{
			// 컨트롤이 있다.
			if (m_control != null) {
				// 상태 바꿔주고
				SetStatus (STATUS.WAIT_CONTROL);
				// 컨트롤 지금걸로 
				GamePlay.instance.touch.SetControl(m_controlIndex);
				// 다른 슬롯 컨트롤이 있으면 꺼주고
				GamePlay.instance.ResetWaitControls(m_controlIndex);
			}
			// 없으면 바로 시전
			else {
				if(m_skill != null)
				{
					SetStatus (AxeSlot.STATUS.ACTIVATE);
					m_skill.Active(Vector3.zero);
				}
			}

			return true;

		}
		// 컨트롤 대기 상태이면 다시 원래대로
		if (IsStatus (AxeSlot.STATUS.WAIT_CONTROL))
		{
			// 상태 바꿔주고
			ResetWaitControl();
			// 다시 컨트롤 원래대로
			GamePlay.instance.touch.SetControl(GamePlay.instance.GetAxeSlot().controlIndex);

			return true;
		}

		return false;
	}


	public override bool DeActive()
	{
		if (base.DeActive ()) {
			m_skill.DeActive();

			return true;
		}

		return false;
	}


	protected override void OnTarget(Vector3 position)
	{
		// 컨트롤이 들어왔음

		if (IsStatus(AxeSlot.STATUS.WAIT_CONTROL) == false) {
			return;
		}

		// 상태 바꿔주고
		SetStatus(AxeSlot.STATUS.ACTIVATE);
		// 컨트롤은 던지기로 돌아가주자
		GamePlay.instance.touch.SetControl(GamePlay.instance.GetAxeSlot().controlIndex);

		m_skill.Active(position);
	}
}
