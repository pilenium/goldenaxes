﻿using UnityEngine;
using System.Collections;


public class AxeSlot {
	public enum STATUS
	{
		NONE,
		COOL,
		READY,
		WAIT_CONTROL,
		ACTIVATE
	}


	// Control
	protected	int		m_controlIndex;
	public		int		controlIndex	{ get { return m_controlIndex; } }
	protected	Control	m_control		= null;

	// Event Listener
	public delegate void OnSetAxe (ItemAxe itemAxe);
	public delegate void OnResetAxe ();

	public delegate void OnReady ();
	public delegate void OnWaitControl();

	public delegate void OnCool (float time);
	public delegate void OnCooling (float time);

	public delegate void OnActivate (float time);
	public delegate void OnActivating (float time);

	public OnSetAxe			onSetAxe		= null;
	public OnResetAxe		onResetAxe		= null;

	public OnReady			onReady			= null;
	public OnWaitControl	onWaitControl	= null;

	public OnCool			onCool			= null;
	public OnCooling		onCooling		= null;

	public OnActivate		onActivate		= null;
	public OnActivating		onActivating	= null;

	// update
	protected delegate void UpdateDelegate (float dt);
	protected UpdateDelegate	m_update		= null;


	// data
	protected AxeSlot.STATUS	m_status;
	protected ItemAxe			m_axe			= null;
	protected DataAxeSlot		m_dataSlot		= null;
	protected int				m_axe_level;
	

	// getter
	public	AxeSlot.STATUS	status			{ get { return m_status; } }

	public	int				id				{ get { return m_axe.id; } }
	public	ItemAxe			axe				{ get { return m_axe;  }}
	public	DataAxeSlot		dataSlot		{ get { return m_dataSlot; }  }
	public	bool			hasAxe			{ get { return m_axe != null; } }


	// time
	protected float			m_time;
	protected int			m_lastTime;

	public	bool			offline			{ get; protected set; }
	public	float			coolTime		{ get { return dataSlot.coolTime; } }
	public	float			activeTime		{ get { return dataSlot.activeTime; } }
	public	float			time			{ get { return m_time; } }

	public AxeSlot(int lastTime)
	{
		m_controlIndex = GamePlay.instance.touch.CreateControl();

		m_lastTime = lastTime;
		m_update = null;
		offline = false;
	}

	public virtual void Clear()
	{
		m_update		= null;

		m_axe			= null;
		m_control		= null;

		onSetAxe		= null;
		onResetAxe		= null;
		onReady			= null;
		onWaitControl	= null;
		onCooling		= null;
	}

	public virtual void Update (float dt)
	{
		if (m_update != null) {
			m_update (dt);
		}
	}

	public virtual void UpdateCool (float dt)
	{
		m_time -= dt;

		if (m_time <= 0.0f) {
			SetStatus (AxeSlot.STATUS.READY);
			return;
		}

		if (onCooling != null) {
			onCooling (m_time);
		}
	}

	public virtual void UpdateActivate (float dt)
	{
		m_time -= dt;

		if (m_time <= 0.0f) {
			DeActive ();
			return;
		}

		if (onActivating != null) {
			onActivating (m_time);
		}
	}

	protected virtual void InitStatus()
	{

		// ready
		if (m_lastTime + coolTime <= GameTime.GetNowTimestamp ()) {
			SetStatus (AxeSlot.STATUS.READY);
			m_time = 0.0f;
		} else {
			SetStatus (AxeSlot.STATUS.COOL);
			m_time = m_lastTime + coolTime - GameTime.GetNowTimestamp ();
		}

		/*
		// cool


		// TODO 나중에 오프라인 모드 만들면 한번 더 손보자 여기
		// 액티브 상태는 없다.. 액티브 상황 없을거 같애 (그리고 없어야 하는거 아냐?)
		if (m_lastTime <= 0) {
			SetStatus (AxeSlot.STATUS.READY);
		}
		// 일단 넣어 놨지만 이런 상황이 있을까?
		else if(m_lastTime > coolTime)
		{
			m_lastTime = coolTime;
			SetStatus (AxeSlot.STATUS.COOL);
		}
		else {
			SetStatus (AxeSlot.STATUS.COOL);
		}
		*/
	}

	protected virtual void SetStatus(AxeSlot.STATUS status)
	{
		if (IsStatus(status)) {
			return;
		}
		m_status = status;

		switch (m_status) {
		case AxeSlot.STATUS.COOL:
			m_time = coolTime;
			m_update = this.UpdateCool;

			// 상태 트리거
			if (onCool != null) {
				onCool (m_time);
			}
			// 상태 트리거 없으면 업데이트
			else if (onCooling != null) {
				onCooling (m_time);
			}
			break;

		case AxeSlot.STATUS.READY:
			m_update = null;

			if (onReady != null) {
				onReady ();
			}
			break;

		case AxeSlot.STATUS.WAIT_CONTROL:
			m_update = null;

			if (onWaitControl != null) {
				onWaitControl ();
			}
			break;

		case AxeSlot.STATUS.ACTIVATE:
			m_time = activeTime;
			m_update = this.UpdateActivate;

			// 상태 트리거
			if (onActivate != null) {
				onActivate (m_time);
			}
			// 상태 트리거 없으면 업데이트
			if (onActivating != null) {
				onActivating (m_time);
			}

			if (offline) {
				m_lastTime = GameTime.GetNowTimestamp () + (int) activeTime;
				SetDataUserTime(m_lastTime);
			}

			break;

		}
	}

	public virtual void SetDataUserTime(int time)
	{
		Data.user.slotAxeTime.Set(time);
	}

	public virtual bool IsStatus(AxeSlot.STATUS status)
	{
		return m_status == status;
	}


	public virtual bool SetAxe(int axe_id)
	{
		ItemAxe itemAxe = Data.inventory.GetAxe(axe_id);

		if(itemAxe == null)
		{
			// 설치 못함
			return false;
		}

		return SetAxe(itemAxe);
	}

	public virtual bool SetAxe(ItemAxe itemAxe)
	{
		m_axe = itemAxe;

		SetData(m_axe);

		if(m_dataSlot == null)
		{
			return false;
		}

		m_control = GamePlay.instance.touch.InitControl(m_controlIndex, m_dataSlot.control);
		m_control.AddTargetListener(OnTarget);

		if (onSetAxe != null) {
			onSetAxe (m_axe);
		}

		InitStatus();

		// 내 컨트롤로 셋
		GamePlay.instance.touch.SetControl(m_controlIndex);

		// 집을때는 throw axe를 기준으로 이용한다
		GamePlay.instance.woodman.SetAxe(m_dataSlot.model);

		return true;
	}



	protected virtual void SetData(ItemAxe itemAxe)
	{
		m_dataSlot = itemAxe.data.throwSlot;
	}

	public virtual void ResetAxe()
	{
		m_axe = null;
		m_axe_level = 0;

		if (m_control != null) {
			m_control.RemoveTargetListener ();
			m_control = null;
		}

		if (onResetAxe != null) {
			onResetAxe ();
		}
	}

	protected virtual void OnTarget(Vector3 position)
	{
		if (IsStatus (AxeSlot.STATUS.READY) == false) {
			return;
		}

		SetStatus (AxeSlot.STATUS.ACTIVATE);

		// 도끼 던지기
		Axe axe = Axe.Create(m_dataSlot, m_axe.level);
		axe.Init (m_dataSlot.model, GamePlay.instance.properties.throwDMG, Common.THROW_POSITION, position);

		foreach(Abillity abillity in GamePlay.instance.properties.GetBuffAbillities())
		{
			axe.AddAbillity(abillity.Clone());
		}

		GamePlay.instance.stage.AddAxe(axe);

		// 나무꾼 
		float deg = Mathf.Atan2(position.x - Common.THROW_POSITION.x, position.z - Common.THROW_POSITION.z);
		GamePlay.instance.woodman.ThrowAxe (deg);
	}

	/// <summary>
	/// 슬롯 활성화 (UI에서 호출)
	/// </summary>
	public virtual bool Active()
	{
		if (IsStatus (AxeSlot.STATUS.READY) == false) {
			return false;
		}

		SetStatus (AxeSlot.STATUS.ACTIVATE);

		return true;
	}

	/// <summary>
	/// 슬롯 비활성화 (슬롯 자체에서 호출)
	/// </summary>
	public virtual bool DeActive ()
	{
		if (IsStatus (AxeSlot.STATUS.ACTIVATE) == false) {
			return false;
		}

		SetStatus (AxeSlot.STATUS.COOL);

		return true;
	}

	public virtual bool ResetWaitControl()
	{
		// 컨트롤 대기 상태이면 다시 원래대로
		if (IsStatus (AxeSlot.STATUS.WAIT_CONTROL)) {
			// 상태 바꿔주고
			SetStatus (AxeSlot.STATUS.READY);

			return true;
		}

		return false;
	}
}
