﻿using UnityEngine;
using System.Collections;

namespace PGTool {
	public class Resource : MonoBehaviour {
		private	bool	m_isUse;
		public	bool	IsUse		{ get { return m_isUse; } }

		private	string	m_key;
		public	string	key					{ get { return m_key; } }
		public	void	SetKey(string key)	{ m_key = key; name = key; }

		/// <summary>
		/// 초기화
		/// </summary>
		public virtual void Awake () {
			m_isUse = true;
		}

		/// <summary>
		/// 삭제 (Destroy 전에 호출됨)
		/// </summary>
		public virtual void Clear()
		{
			m_isUse = true;
		}

		/// <summary>
		/// 재사용 시작
		/// </summary>
		public virtual void ReUse()
		{
			m_isUse = true;
		}

		/// <summary>
		/// 사용 끝 (반납)
		/// </summary>
		public virtual void EndUse()
		{
			m_isUse = false;
		}
	}
}