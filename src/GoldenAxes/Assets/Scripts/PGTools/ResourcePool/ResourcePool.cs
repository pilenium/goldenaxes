﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PGTool {
	public class ResourcePool
	{
		private List<Resource>		m_pool;
		private string				m_key;
		private string				m_url;
		private int					m_max;
		private bool				m_shirink;

		/// <summary>
		/// ResourcePool 생성자 (new GameObject()로 생성, Resources.Load()가 아님)
		/// </summary>
		/// <param name="key">리소스 key</param>
		/// <param name="maxSize">풀 최대 갯수</param>
		/// <param name="autoShirink">반환시 최대 갯수 넘을 경우 자동으로 비워준다</param>
		public ResourcePool(string key, int maxSize, bool autoShirink)
		{
			m_pool = new List<Resource>();

			m_key = key;
			m_max = maxSize;
			m_url = null;
			m_shirink = autoShirink;
		}

		/// <summary>
		/// ResourcePool 생성자
		/// </summary>
		/// <param name="key">리소스 key</param>
		/// <param name="url">리소스 url</param>
		/// <param name="maxSize">풀 최대 갯수</param>
		/// <param name="autoShirink">반환시 최대 갯수 넘을 경우 자동으로 비워준다</param>
		public ResourcePool(string key, string url, int maxSize, bool autoShirink)
		{
			m_pool = new List<Resource>();

			m_key = key;
			m_max = maxSize;
			m_url = url;
			m_shirink = autoShirink;
		}

		/// <summary>
		/// 오브젝트 가져오기
		/// - 재사용 가능한 오브젝트가 있을 경우 가져옴
		/// - 재사용 가능한 오브젝트가 없을 경우 생성후 가져옴
		/// </summary>
		public Resource Get()
		{
			// 풀 안에 있을경우 반환
			foreach (Resource res in m_pool) {
				if (false == res.IsUse) {
					res.ReUse ();
					res.gameObject.SetActive (true);

					return res;
				}
			}

			// 없으면 생성 후 반환
			GameObject nObj;

			if(m_url == null)	nObj = new GameObject();
			else 				nObj = GameObject.Instantiate (Resources.Load(m_url)) as GameObject;

			GameObject.DontDestroyOnLoad(nObj);
			Resource nRes = nObj.GetComponent<Resource>() as Resource;
			nRes.SetKey (m_key);
			m_pool.Add (nRes);

			return nRes;
		}

		/// <summary>
		/// 오브젝트 가져오기
		/// - 재사용 가능한 오브젝트가 있을 경우 가져옴
		/// - 재사용 가능한 오브젝트가 없을 경우 생성후 가져옴
		/// </summary>
		/// <typeparam name="T">리소스 type</typeparam>
		public Resource Get<T>() where T : Resource
		{
			// 풀 안에 있을경우 반환
			foreach (Resource res in m_pool) {
				if (false == res.IsUse) {
					res.ReUse ();
					res.gameObject.SetActive (true);

					return res;
				}
			}

			// 없으면 생성 후 반환
			GameObject nObj;

			if(m_url == null)	nObj = new GameObject();
			else 				nObj = GameObject.Instantiate (Resources.Load(m_url)) as GameObject;

			GameObject.DontDestroyOnLoad(nObj);
			Resource nRes = nObj.AddComponent< T > () as Resource;
			nRes.SetKey (m_key);
			m_pool.Add (nRes);

			return nRes;
		}

		/// <summary>
		/// 오브젝트 반납
		/// - autoshirnk 일 경우 max치 넘으면 Destory.
		/// </summary>
		/// <param name="res">반납할 리소스</param>
		public void Return(Resource resouce)
		{
			resouce.EndUse ();
			resouce.gameObject.SetActive (false);

			// 갯수가 넘으면 지워주자 (auto shirnk인 경우)
			if (m_shirink && m_pool.Count > m_max) {
				m_pool.Remove (resouce);
				resouce.Clear ();
				GameObject.Destroy (resouce.gameObject);
				resouce = null;
			}
		}
	}
}