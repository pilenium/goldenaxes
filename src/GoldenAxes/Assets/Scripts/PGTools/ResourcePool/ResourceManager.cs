﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


namespace PGTool {
	public class ResourceManager {
		private	Dictionary<string, ResourcePool>	m_pools;

		// 단일코어라 쓸일이 있을까?
		//private	static volatile ResourceManager		m_instance = null;
		private	static ResourceManager		m_instance = null;


		// 설정값
		private int	m_max	= 10;
		private bool m_autoShirink = false;

		//object for locking
		//private static object syncRoot	= new System.Object();

		/// <summary> 생성자 </summary>
		private ResourceManager()
		{
			m_pools = new Dictionary<string, ResourcePool>();
		}

		public static ResourceManager instance
		{
			get
			{
				//check to see if it doesnt exist
				if (m_instance == null)
				{
					/*
					// 단일코어라 쓸일이 있을까?
					//lock access, if it is already locked, wait.
					lock (syncRoot)
					{
						//the instance could have been made between
						//checking and waiting for a lock to release.
						if (m_instance == null)
						{
							//create a new instance
							m_instance = new ResourceManager();
						}
					}
					*/
					//create a new instance
					m_instance = new ResourceManager();
				}
				//return either the new instance or the already built one.
				return m_instance;
			}
		}


		/// <summary>
		/// 오브젝트 가져오기 (new GameObject()로 생성, Resources.Load()가 아님)
		/// - 재사용 가능한 오브젝트가 있을 경우 가져옴
		/// - 재사용 가능한 오브젝트가 없을 경우 생성후 가져옴
		/// </summary>
		/// <param name="key">리소스 key</param>
		public Resource Get<T>(string key) where T : Resource
		{
			// 없는 키면 생성한다
			if (false == m_pools.ContainsKey (key)) {
				ResourcePool nPool = new ResourcePool(key, m_max, m_autoShirink);

				m_pools.Add(key, nPool);
			}

			return m_pools [key].Get<T> ();
		}

		/// <summary>
		/// 오브젝트 가져오기
		/// ** 타입 명시하지 않을경우 PGTool.Resource 타입이 prefab안에 있어야함 **
		/// - 재사용 가능한 오브젝트가 있을 경우 가져옴
		/// - 재사용 가능한 오브젝트가 없을 경우 생성후 가져옴
		/// </summary>
		/// <param name="key">리소스 key</param>
		/// <param name="url">리소스 url</param>
		public Resource Get(string key, string url)
		{
			// 없는 키면 생성한다
			if (false == m_pools.ContainsKey (key)) {
				ResourcePool nPool = new ResourcePool(key, url, m_max, m_autoShirink);

				m_pools.Add(key, nPool);
			}

			return m_pools [key].Get ();
		}

		/// <summary>
		/// 오브젝트 가져오기
		/// - 재사용 가능한 오브젝트가 있을 경우 가져옴
		/// - 재사용 가능한 오브젝트가 없을 경우 생성후 가져옴
		/// </summary>
		/// <param name="key">리소스 key</param>
		/// <typeparam name="T">리소스 type</typeparam>
		/// <param name="url">리소스 url</param>
		public Resource Get<T>(string key, string url) where T : Resource
		{
			// 없는 키면 생성한다
			if (false == m_pools.ContainsKey (key)) {
				ResourcePool nPool = new ResourcePool(key, url, m_max, m_autoShirink);

				m_pools.Add(key, nPool);
			}

			return m_pools [key].Get<T> ();
		}

		/// <summary>
		/// 오브젝트 반납
		/// - autoshirnk 일 경우 max치 넘으면 Destory.
		/// </summary>
		/// <param name="resouce">반납할 리소스</param>
		public bool Return(Resource resouce)
		{
			// 키가 있으면 반납
			if (m_pools.ContainsKey (resouce.key)) {
				m_pools [resouce.key].Return (resouce);

				return true;
			}
			// 없는 키면 걍 지워버려
			else {
				resouce.Clear ();
				GameObject.Destroy (resouce.gameObject);

				return false;
			}
		}
	}
}