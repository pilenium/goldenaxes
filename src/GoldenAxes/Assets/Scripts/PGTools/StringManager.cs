﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


namespace PGTool {
	public class StringManager {
		private static string[] exp_chars = {"", "K", "M", "B", "T"};

		public static string ToGANumber(float value, string fmt = "0.00", string fmtUnderK = "0.00")
		{
			// IFormatProvider 를 사용하면 더욱 스마트 하게 할수 있을거 같은데 일단 넘어가자
			string str = value.ToString("0.0000e0");

			string[] splits = str.Split('e');

			return ToGANumber(splits[0], splits[1], fmt, fmtUnderK);
		}

		public static string ToGANumber(double value, string fmt = "0.00", string fmtUnderK = "0.00")
		{
			// IFormatProvider 를 사용하면 더욱 스마트 하게 할수 있을거 같은데 일단 넘어가자
			string str = value.ToString("0.0000e0");

			string[] splits = str.Split('e');

			return ToGANumber(splits[0], splits[1], fmt, fmtUnderK);
		}

		private static string ToGANumber(string mantissa, string exponent, string fmt, string fmtUnderK)
		{
			float m = float.Parse(mantissa);
			int e = int.Parse(exponent);
			
			float ret;

			if(e < 3)
			{
				ret = m * Mathf.Pow(10, e);

				return ret.ToString(fmtUnderK);
			}
			else
			{
				int modulus = e % 3;
				m *= Mathf.Pow(10, modulus);
				e -= modulus;

				return m.ToString(fmt) + ToExpString(e);
			}
		}

		public static string To3GANumber(float value)
		{
			// IFormatProvider 를 사용하면 더욱 스마트 하게 할수 있을거 같은데 일단 넘어가자
			string str = value.ToString("0.0000e0");

			string[] splits = str.Split('e');

			return To3GANumber(splits[0], splits[1]);
		}

		public static string To3GANumber(double value)
		{
			// IFormatProvider 를 사용하면 더욱 스마트 하게 할수 있을거 같은데 일단 넘어가자
			string str = value.ToString("0.0000e0");

			string[] splits = str.Split('e');

			return To3GANumber(splits[0], splits[1]);
		}

		private static string To3GANumber(string mantissa, string exponent)
		{
			float m = float.Parse(mantissa);
			int e = int.Parse(exponent);
			
			float ret;
			string strExp = "";

			if(e < 3)
			{
				ret = m * Mathf.Pow(10, e);
			}
			else
			{
				int modulus = e % 3;
				ret = m * Mathf.Pow(10, modulus);
				e -= modulus;
				strExp = ToExpString(e);
			}

			if(ret >= 100)
			{
				return ret.ToString("000")+strExp;
			}
			else if(ret >= 10)
			{
				return ret.ToString("00.0")+strExp;
			}
			else
			{
				return ret.ToString("0.00")+strExp;
			}
		}

		private static string ToExpString(int exponent)
		{
			int e = exponent / 3;

			if(e < exp_chars.Length)
				return exp_chars[e];
			
			e -= exp_chars.Length;

			int first = e / 26;
			int last = e % 26;

			char cFirst;
			char cLast;

			cFirst = (char) (first + 97);
			cLast = (char) (last + 97);
			
			return cFirst.ToString() + cLast.ToString();
		}

		public static Color HexToColor(string hex)
		{
			hex = hex.Replace ("0x", "");//in case the string is formatted 0xFFFFFF
			hex = hex.Replace ("#", "");//in case the string is formatted #FFFFFF
			byte a = 255;//assume fully visible unless specified in hex
			byte r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
			byte g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
			byte b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
			//Only use alpha if the string has enough characters
			if(hex.Length == 8){
				a = byte.Parse(hex.Substring(6,2), System.Globalization.NumberStyles.HexNumber);
			}
			return new Color32(r,g,b,a);
		}

	}
}