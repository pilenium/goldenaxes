﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PGTool {
	public class NGUIManager {

		public static void SetDepths(GameObject go, int depth)
		{
			SetDepthWithChildren(go, depth);
		}
		
		private static void SetDepthWithChildren(GameObject go, int depth)
		{
			UIWidget[] widgets = go.GetComponents<UIWidget>();

			foreach(UIWidget w in widgets)
			{
				w.depth += depth;
			}

	//		GameObject[] gos = go.GetComponentInChildren<GameObject>();

			foreach (Transform child in go.transform)
			{
				SetDepthWithChildren(child.gameObject, depth);
			//child is your child transform
			}

/* 
			foreach(GameObject g in gos)
			{
				SetDepthWithChildren(g, depth);
			}*/
		}
	}
}
