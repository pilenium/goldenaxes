﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UVSequence
{
	public float time;
	public float tiling_x;
	public float tiling_y;
	public float offset_x;
	public float offset_y;
}

[System.Serializable]
public class UVStatus
{
	public string name;
	public bool random_start = false;
	public UVSequence[]	sequence;
}

public class UVAnimation : MonoBehaviour {

	public string		materialName;
	public string		propertyName;
	public string		default_status;
	public UVStatus[]	status;

	private Material	m_material	= null;
	private int			m_seq;
	private UVStatus	m_status	= null;
	private float		m_time;

	private bool		m_enabled;

	void Awake () {

		m_enabled = Init ();

		SetStatus (default_status);
	}

	private bool Init() {
		Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer> ();

		for (int i = 0; i < renderers.Length; i++)
		{
			for (int j = 0; j < renderers[i].materials.Length; j++)
			{
				if (renderers[i].materials [j].name.Contains(materialName)) {
					m_material = renderers [i].materials [j];
					return true;
				}
			}
		}

		return false;
	}

	public void SetStatus(string status_name)
	{
		if (m_enabled == false)
			return;
		
		m_status = GetStatus (status_name);

		if (m_status == null) {
			m_status = GetStatus (default_status);
		}

		InitSequence ();
	}

	private UVStatus GetStatus(string status_name)
	{
		if (m_enabled == false)
			return null;
		
		for (int i = 0; i < status.Length; ++i) {
			if (status [i].name == status_name) {
				return status [i];
			}
		}
		return null;
	}

	private void InitSequence()
	{
		if (m_enabled == false)
			return;

		if (m_status == null)
			return;

		if (m_status.sequence.Length == 0) {
			m_status = null;
			return;
		}

		if (m_material == null)
			return;
		
		if (m_status.random_start) {
			m_seq = Random.Range (0, m_status.sequence.Length);
			m_time = m_status.sequence [m_seq].time;
			SetSequence (m_seq, m_time);
		} else {
			SetSequence (0);
		}
	}

	private void SetSequence(int seq)
	{
		if (m_enabled == false)
			return;
		
		if (m_status == null)
			return;
		
		
		m_seq = seq;
		m_time = m_status.sequence [m_seq].time;


		m_material.SetTextureScale (propertyName, new Vector2( m_status.sequence [m_seq].tiling_x, m_status.sequence [m_seq].tiling_y ) );
		m_material.SetTextureOffset (propertyName, new Vector2 ( m_status.sequence [m_seq].offset_x, m_status.sequence [m_seq].offset_y));
	}

	private void SetSequence(int seq, float time)
	{
		if (m_enabled == false)
			return;
		
		if (m_status == null)
			return;
		

		m_seq = seq;
		m_time = time;


		m_material.SetTextureScale (propertyName, new Vector2( m_status.sequence [m_seq].tiling_x, m_status.sequence [m_seq].tiling_y ) );
		m_material.SetTextureOffset (propertyName, new Vector2 ( m_status.sequence [m_seq].offset_x, m_status.sequence [m_seq].offset_y));
	}

	private void NextSequence()
	{
		if (m_enabled == false)
			return;
		
		if (m_status == null)
			return;
		
		m_seq++;
		if (m_seq >= m_status.sequence.Length) {
			m_seq = 0;
		}
		SetSequence(m_seq);
	}
	
	// Update is called once per frame
	void Update () {

		if (m_enabled == false)
			return;
		
		if (m_status == null)
			return;

		m_time -= Time.deltaTime;

		if (m_time <= 0.0f) {
			NextSequence ();
		}
	}
}
