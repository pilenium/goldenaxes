﻿using System.Collections;
using UnityEngine;

public class RotateAnimation : MonoBehaviour {
	public Vector3		delta_deg	= Vector3.zero;
	public bool			random		= true;

	private Transform	m_tr;
	private float		m_time;


	void Awake()
	{
		m_tr = GetComponent<Transform> ();

		if(random) m_tr.Rotate(delta_deg * Random.Range (0, Mathf.PI*2));
	}

	// Update is called once per frame
	void Update()
	{
		m_tr.Rotate(delta_deg * Time.deltaTime);
	}
}
