﻿using UnityEngine;
using System.Collections;

public class ScaleAnimation : MonoBehaviour {

	public float		speed		= 1.0f;
	public float		min_size	= 1.0f;
	public float		max_size	= 1.1f;
	public bool			random		= true;

	private Transform	m_tr;
	private Vector3		m_defaultScale;
	private float		m_time;



	void Awake() {

		m_tr = GetComponent<Transform> ();
		//m_tr.position = new Vector3 (m_tr.position.x, Common.POS_WAVE_Y, m_tr.position.z);

		m_defaultScale = m_tr.localScale;

		if(random) m_time = Random.Range (0, Mathf.PI * 2);
	}

	// Update is called once per frame
	void Update () {
		m_time += speed * Time.deltaTime;

		float scale = Mathf.Sin(m_time);

		scale = scale * (max_size - min_size) / 2 + min_size;
		
		m_tr.localScale = m_defaultScale * scale;
	}
}
