﻿Shader "Unlit/Color (Alpha Blend)" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" { }
	}

	SubShader {    
	    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		//Tags {"Queue"="Transparent" "RenderType"="Transparent" }

		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Pass {
			CGPROGRAM
				#pragma vertex vert_img
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest

				#include "UnityCG.cginc"    

				uniform sampler2D _MainTex;
				uniform fixed4 _Color;

				fixed4 frag (v2f_img i) : SV_Target {  
					return tex2D(_MainTex, i.uv) * _Color;  
				}
			ENDCG
		}
	}
}