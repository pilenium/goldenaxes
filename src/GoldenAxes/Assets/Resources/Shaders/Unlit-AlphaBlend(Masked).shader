﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/Alpha Blend (Masked)"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" { }
		_AlphaTex ("Mask (RGB)", 2D) = "white" { }
	}
	 
	SubShader
	{   
	    //Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
		Tags {"Queue"="Transparent" "RenderType"="Transparent" }

		LOD 100

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Pass
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest

				#include "UnityCG.cginc"    

				struct appdata_t {
					float4 vertex : POSITION;
					float2 uv1 : TEXCOORD0;
					float2 uv2 : TEXCOORD1;
				};

				struct v2f {
					float4 vertex : SV_POSITION;
					float2 uv1 : TEXCOORD0;
					float2 uv2 : TEXCOORD1;
				};

		        uniform sampler2D _MainTex;
		        uniform sampler2D _AlphaTex;
		        uniform fixed4 _Color;

				float4 _MainTex_ST;
				float4 _AlphaTex_ST;

				v2f vert (appdata_t v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv1 = TRANSFORM_TEX(v.uv1, _MainTex);
					o.uv2 = TRANSFORM_TEX(v.uv2, _AlphaTex);
					return o;
				}

				fixed4 frag (v2f i) : SV_Target
				{
					fixed4 col = tex2D(_MainTex, i.uv1);
	                fixed4 col2 = tex2D(_AlphaTex, i.uv2);
	                
	                return fixed4(col.r, col.g, col.b, col.a * col2.r) * _Color;

				}
			ENDCG
		}
	}
}
