﻿Shader "Unlit/Color Tint (Cutout)" {
	Properties {
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" { }
		_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	}

	SubShader {    
	    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="TransparentCutoff"}

		LOD 100

		Pass {
	        Alphatest Greater [_Cutoff]
	        Lighting Off
	        SetTexture [_MainTex] {
	           constantColor [_Color]
			   combine texture * constant DOUBLE
	        }
		}
	}
}