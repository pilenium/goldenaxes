﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t881716807;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va648744748.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m528877432_gshared (Enumerator_t648744748 * __this, Dictionary_2_t881716807 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m528877432(__this, ___host0, method) ((  void (*) (Enumerator_t648744748 *, Dictionary_2_t881716807 *, const MethodInfo*))Enumerator__ctor_m528877432_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1432377961_gshared (Enumerator_t648744748 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1432377961(__this, method) ((  Il2CppObject * (*) (Enumerator_t648744748 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1432377961_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3798912573_gshared (Enumerator_t648744748 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3798912573(__this, method) ((  void (*) (Enumerator_t648744748 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3798912573_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::Dispose()
extern "C"  void Enumerator_Dispose_m1215241242_gshared (Enumerator_t648744748 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1215241242(__this, method) ((  void (*) (Enumerator_t648744748 *, const MethodInfo*))Enumerator_Dispose_m1215241242_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1616285481_gshared (Enumerator_t648744748 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1616285481(__this, method) ((  bool (*) (Enumerator_t648744748 *, const MethodInfo*))Enumerator_MoveNext_m1616285481_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.LogType,UnityEngine.Color>::get_Current()
extern "C"  Color_t1588175760  Enumerator_get_Current_m3722086329_gshared (Enumerator_t648744748 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3722086329(__this, method) ((  Color_t1588175760  (*) (Enumerator_t648744748 *, const MethodInfo*))Enumerator_get_Current_m3722086329_gshared)(__this, method)
