﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PatternManager
struct PatternManager_t2286577181;
// Pattern
struct Pattern_t873562992;
// System.String
struct String_t;
// Diver
struct Diver_t66044126;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_Pattern873562992.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void PatternManager::.ctor()
extern "C"  void PatternManager__ctor_m2154821406 (PatternManager_t2286577181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pattern PatternManager::Alloc(System.String)
extern "C"  Pattern_t873562992 * PatternManager_Alloc_m2816894612 (PatternManager_t2286577181 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PatternManager::Clear()
extern "C"  void PatternManager_Clear_m3855921993 (PatternManager_t2286577181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pattern PatternManager::AddPattern(System.String,System.Int32,System.Boolean)
extern "C"  Pattern_t873562992 * PatternManager_AddPattern_m3766005008 (PatternManager_t2286577181 * __this, String_t* ___type0, int32_t ___count1, bool ___isMonster2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PatternManager::EndPattern(Pattern)
extern "C"  void PatternManager_EndPattern_m4291913479 (PatternManager_t2286577181 * __this, Pattern_t873562992 * ___pattern0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Diver PatternManager::GetHitDiver(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Diver_t66044126 * PatternManager_GetHitDiver_m463736356 (PatternManager_t2286577181 * __this, Vector3_t3525329789  ___point0, Vector3_t3525329789  ___collider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
