﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Axe
struct Axe_t66286;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stage/<RemoveAxe>c__AnonStorey9
struct  U3CRemoveAxeU3Ec__AnonStorey9_t1964597890  : public Il2CppObject
{
public:
	// Axe Stage/<RemoveAxe>c__AnonStorey9::axe
	Axe_t66286 * ___axe_0;

public:
	inline static int32_t get_offset_of_axe_0() { return static_cast<int32_t>(offsetof(U3CRemoveAxeU3Ec__AnonStorey9_t1964597890, ___axe_0)); }
	inline Axe_t66286 * get_axe_0() const { return ___axe_0; }
	inline Axe_t66286 ** get_address_of_axe_0() { return &___axe_0; }
	inline void set_axe_0(Axe_t66286 * value)
	{
		___axe_0 = value;
		Il2CppCodeGenWriteBarrier(&___axe_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
