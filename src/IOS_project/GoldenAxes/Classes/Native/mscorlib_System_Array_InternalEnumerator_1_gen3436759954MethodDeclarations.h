﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3436759954.h"
#include "mscorlib_System_Array2840145358.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.LogType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1028794444_gshared (InternalEnumerator_1_t3436759954 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1028794444(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3436759954 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1028794444_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.LogType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2269557204_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2269557204(__this, method) ((  void (*) (InternalEnumerator_1_t3436759954 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2269557204_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.LogType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1378538186_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1378538186(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3436759954 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1378538186_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.LogType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2775277859_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2775277859(__this, method) ((  void (*) (InternalEnumerator_1_t3436759954 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2775277859_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.LogType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3061281668_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3061281668(__this, method) ((  bool (*) (InternalEnumerator_1_t3436759954 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3061281668_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.LogType>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m1348975477_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1348975477(__this, method) ((  int32_t (*) (InternalEnumerator_1_t3436759954 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1348975477_gshared)(__this, method)
