﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataMonster
struct DataMonster_t1423279280;

#include "codegen/il2cpp-codegen.h"

// System.Void DataMonster::.ctor()
extern "C"  void DataMonster__ctor_m1858616507 (DataMonster_t1423279280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
