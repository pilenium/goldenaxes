﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stage/<RemoveEffect>c__AnonStoreyA
struct  U3CRemoveEffectU3Ec__AnonStoreyA_t267048359  : public Il2CppObject
{
public:
	// UnityEngine.GameObject Stage/<RemoveEffect>c__AnonStoreyA::effect
	GameObject_t4012695102 * ___effect_0;

public:
	inline static int32_t get_offset_of_effect_0() { return static_cast<int32_t>(offsetof(U3CRemoveEffectU3Ec__AnonStoreyA_t267048359, ___effect_0)); }
	inline GameObject_t4012695102 * get_effect_0() const { return ___effect_0; }
	inline GameObject_t4012695102 ** get_address_of_effect_0() { return &___effect_0; }
	inline void set_effect_0(GameObject_t4012695102 * value)
	{
		___effect_0 = value;
		Il2CppCodeGenWriteBarrier(&___effect_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
