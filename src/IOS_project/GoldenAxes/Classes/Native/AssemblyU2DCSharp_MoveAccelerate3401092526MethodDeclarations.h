﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveAccelerate
struct MoveAccelerate_t3401092526;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveAccelerate::.ctor()
extern "C"  void MoveAccelerate__ctor_m2811675309 (MoveAccelerate_t3401092526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveAccelerate::Update()
extern "C"  void MoveAccelerate_Update_m2989450752 (MoveAccelerate_t3401092526 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveAccelerate::InitSpeed(System.Single)
extern "C"  void MoveAccelerate_InitSpeed_m509110985 (MoveAccelerate_t3401092526 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveAccelerate::SetSpeed(System.Single)
extern "C"  void MoveAccelerate_SetSpeed_m2743773551 (MoveAccelerate_t3401092526 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
