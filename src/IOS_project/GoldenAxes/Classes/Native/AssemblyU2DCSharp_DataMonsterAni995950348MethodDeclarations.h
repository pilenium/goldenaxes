﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataMonsterAni
struct DataMonsterAni_t995950348;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MONSTER_STATE3983724460.h"

// System.Void DataMonsterAni::.ctor()
extern "C"  void DataMonsterAni__ctor_m3445160719 (DataMonsterAni_t995950348 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DataMonsterAni::GetAnimation(MONSTER_STATE)
extern "C"  String_t* DataMonsterAni_GetAnimation_m2683680960 (DataMonsterAni_t995950348 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
