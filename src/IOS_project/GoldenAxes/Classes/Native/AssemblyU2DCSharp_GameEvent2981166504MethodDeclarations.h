﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameEvent
struct GameEvent_t2981166504;

#include "codegen/il2cpp-codegen.h"

// System.Void GameEvent::.ctor(System.Single)
extern "C"  void GameEvent__ctor_m1175921096 (GameEvent_t2981166504 * __this, float ___prob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameEvent::IsTrigger()
extern "C"  bool GameEvent_IsTrigger_m1587815235 (GameEvent_t2981166504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
