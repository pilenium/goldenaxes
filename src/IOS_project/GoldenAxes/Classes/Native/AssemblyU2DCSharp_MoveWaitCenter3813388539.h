﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataChapter
struct DataChapter_t925677859;

#include "AssemblyU2DCSharp_MoveAccelerate3401092526.h"
#include "AssemblyU2DCSharp_Common_DIRECTION1824003935.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveWaitCenter
struct  MoveWaitCenter_t3813388539  : public MoveAccelerate_t3401092526
{
public:
	// Common/DIRECTION MoveWaitCenter::m_direction
	int32_t ___m_direction_5;
	// System.Single MoveWaitCenter::m_idle
	float ___m_idle_6;
	// System.Single MoveWaitCenter::m_size
	float ___m_size_7;
	// System.Single MoveWaitCenter::m_initSpeed
	float ___m_initSpeed_8;
	// DataChapter MoveWaitCenter::m_chapterData
	DataChapter_t925677859 * ___m_chapterData_9;
	// System.Int32 MoveWaitCenter::m_appear_dir
	int32_t ___m_appear_dir_10;
	// System.Single MoveWaitCenter::m_state
	float ___m_state_11;

public:
	inline static int32_t get_offset_of_m_direction_5() { return static_cast<int32_t>(offsetof(MoveWaitCenter_t3813388539, ___m_direction_5)); }
	inline int32_t get_m_direction_5() const { return ___m_direction_5; }
	inline int32_t* get_address_of_m_direction_5() { return &___m_direction_5; }
	inline void set_m_direction_5(int32_t value)
	{
		___m_direction_5 = value;
	}

	inline static int32_t get_offset_of_m_idle_6() { return static_cast<int32_t>(offsetof(MoveWaitCenter_t3813388539, ___m_idle_6)); }
	inline float get_m_idle_6() const { return ___m_idle_6; }
	inline float* get_address_of_m_idle_6() { return &___m_idle_6; }
	inline void set_m_idle_6(float value)
	{
		___m_idle_6 = value;
	}

	inline static int32_t get_offset_of_m_size_7() { return static_cast<int32_t>(offsetof(MoveWaitCenter_t3813388539, ___m_size_7)); }
	inline float get_m_size_7() const { return ___m_size_7; }
	inline float* get_address_of_m_size_7() { return &___m_size_7; }
	inline void set_m_size_7(float value)
	{
		___m_size_7 = value;
	}

	inline static int32_t get_offset_of_m_initSpeed_8() { return static_cast<int32_t>(offsetof(MoveWaitCenter_t3813388539, ___m_initSpeed_8)); }
	inline float get_m_initSpeed_8() const { return ___m_initSpeed_8; }
	inline float* get_address_of_m_initSpeed_8() { return &___m_initSpeed_8; }
	inline void set_m_initSpeed_8(float value)
	{
		___m_initSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_chapterData_9() { return static_cast<int32_t>(offsetof(MoveWaitCenter_t3813388539, ___m_chapterData_9)); }
	inline DataChapter_t925677859 * get_m_chapterData_9() const { return ___m_chapterData_9; }
	inline DataChapter_t925677859 ** get_address_of_m_chapterData_9() { return &___m_chapterData_9; }
	inline void set_m_chapterData_9(DataChapter_t925677859 * value)
	{
		___m_chapterData_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_chapterData_9, value);
	}

	inline static int32_t get_offset_of_m_appear_dir_10() { return static_cast<int32_t>(offsetof(MoveWaitCenter_t3813388539, ___m_appear_dir_10)); }
	inline int32_t get_m_appear_dir_10() const { return ___m_appear_dir_10; }
	inline int32_t* get_address_of_m_appear_dir_10() { return &___m_appear_dir_10; }
	inline void set_m_appear_dir_10(int32_t value)
	{
		___m_appear_dir_10 = value;
	}

	inline static int32_t get_offset_of_m_state_11() { return static_cast<int32_t>(offsetof(MoveWaitCenter_t3813388539, ___m_state_11)); }
	inline float get_m_state_11() const { return ___m_state_11; }
	inline float* get_address_of_m_state_11() { return &___m_state_11; }
	inline void set_m_state_11(float value)
	{
		___m_state_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
