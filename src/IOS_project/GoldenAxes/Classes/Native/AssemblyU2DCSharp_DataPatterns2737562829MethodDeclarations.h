﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataPatterns
struct DataPatterns_t2737562829;
// DataPattern
struct DataPattern_t3690539110;

#include "codegen/il2cpp-codegen.h"

// System.Void DataPatterns::.ctor()
extern "C"  void DataPatterns__ctor_m1284252782 (DataPatterns_t2737562829 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataPattern DataPatterns::getData(System.UInt32)
extern "C"  DataPattern_t3690539110 * DataPatterns_getData_m4186287835 (DataPatterns_t2737562829 * __this, uint32_t ___pattern_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
