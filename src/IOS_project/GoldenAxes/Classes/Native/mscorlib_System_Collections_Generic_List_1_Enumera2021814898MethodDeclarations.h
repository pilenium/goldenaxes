﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<DataObject>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2544511228(__this, ___l0, method) ((  void (*) (Enumerator_t2021814898 *, List_1_t3936031906 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataObject>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1117142486(__this, method) ((  void (*) (Enumerator_t2021814898 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DataObject>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3922045964(__this, method) ((  Il2CppObject * (*) (Enumerator_t2021814898 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataObject>::Dispose()
#define Enumerator_Dispose_m2744366561(__this, method) ((  void (*) (Enumerator_t2021814898 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataObject>::VerifyState()
#define Enumerator_VerifyState_m239574170(__this, method) ((  void (*) (Enumerator_t2021814898 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DataObject>::MoveNext()
#define Enumerator_MoveNext_m1724784134(__this, method) ((  bool (*) (Enumerator_t2021814898 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DataObject>::get_Current()
#define Enumerator_get_Current_m2194463987(__this, method) ((  DataObject_t3139072937 * (*) (Enumerator_t2021814898 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
