﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// InvBaseItem
struct InvBaseItem_t1636452469;
// InvDatabase
struct InvDatabase_t852767852;
// InvStat
struct InvStat_t3624028613;
// InvGameItem
struct InvGameItem_t1588794646;
// InvAttachmentPoint
struct InvAttachmentPoint_t1265837596;
// UITweener
struct UITweener_t105489188;
// EventDelegate
struct EventDelegate_t4004424223;
// UIButton
struct UIButton_t179429094;
// UIButtonColor
struct UIButtonColor_t1546108957;
// UIWidgetContainer
struct UIWidgetContainer_t1520767337;
// UIDragDropItem
struct UIDragDropItem_t2087865514;
// UIKeyBinding
struct UIKeyBinding_t2313833690;
// UIKeyNavigation
struct UIKeyNavigation_t1837256607;
// UILabel
struct UILabel_t291504320;
// UIPlaySound
struct UIPlaySound_t532605447;
// UIWidget
struct UIWidget_t769069560;
// UIRect
struct UIRect_t2503437976;
// UIToggle
struct UIToggle_t688812808;
// UIScrollView
struct UIScrollView_t2113479878;
// UIPanel
struct UIPanel_t295209936;
// BMGlyph
struct BMGlyph_t719052705;
// EventDelegate/Parameter
struct Parameter_t3958428553;
// UICamera
struct UICamera_t189364953;
// UIRoot
struct UIRoot_t2503447958;
// UIDrawCall
struct UIDrawCall_t913273974;
// UISpriteData
struct UISpriteData_t3578345923;
// UIAtlas/Sprite
struct Sprite_t2483154661;
// UISprite
struct UISprite_t661437049;
// UIBasicSprite
struct UIBasicSprite_t2501337439;
// UIFont
struct UIFont_t2503090435;
// UICamera/MouseOrTouch
struct MouseOrTouch_t908473047;
// BMSymbol
struct BMSymbol_t1170982339;
// UITextList/Paragraph
struct Paragraph_t3953256782;
// BetterList`1<UITextList/Paragraph>
struct BetterList_1_t1155257498;
// DataAxe
struct DataAxe_t3107820260;
// DataChapter
struct DataChapter_t925677859;
// DataMonster
struct DataMonster_t1423279280;
// DataObject
struct DataObject_t3139072937;
// DataPattern
struct DataPattern_t3690539110;
// DataStage
struct DataStage_t1629502804;
// DataStageChapter
struct DataStageChapter_t1740173401;
// DataZone
struct DataZone_t1853884054;
// Mode
struct Mode_t2403781;
// Monster
struct Monster_t2901270458;
// Diver
struct Diver_t66044126;
// Pattern
struct Pattern_t873562992;
// Axe
struct Axe_t66286;

#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_InvBaseItem1636452469.h"
#include "AssemblyU2DCSharp_InvDatabase852767852.h"
#include "AssemblyU2DCSharp_InvStat3624028613.h"
#include "AssemblyU2DCSharp_InvGameItem1588794646.h"
#include "AssemblyU2DCSharp_InvAttachmentPoint1265837596.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry1095831350.h"
#include "AssemblyU2DCSharp_UIButton179429094.h"
#include "AssemblyU2DCSharp_UIButtonColor1546108957.h"
#include "AssemblyU2DCSharp_UIWidgetContainer1520767337.h"
#include "AssemblyU2DCSharp_UIDragDropItem2087865514.h"
#include "AssemblyU2DCSharp_UIKeyBinding2313833690.h"
#include "AssemblyU2DCSharp_UIKeyNavigation1837256607.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UIPlaySound532605447.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_BMGlyph719052705.h"
#include "AssemblyU2DCSharp_EventDelegate_Parameter3958428553.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"
#include "AssemblyU2DCSharp_UIRoot2503447958.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974.h"
#include "AssemblyU2DCSharp_UISpriteData3578345923.h"
#include "AssemblyU2DCSharp_UIAtlas_Sprite2483154661.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "AssemblyU2DCSharp_UICamera_MouseOrTouch908473047.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry2194697103.h"
#include "AssemblyU2DCSharp_BMSymbol1170982339.h"
#include "AssemblyU2DCSharp_UITextList_Paragraph3953256782.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1155257498.h"
#include "AssemblyU2DCSharp_DataAxe3107820260.h"
#include "AssemblyU2DCSharp_DataChapter925677859.h"
#include "AssemblyU2DCSharp_DataMonster1423279280.h"
#include "AssemblyU2DCSharp_DataObject3139072937.h"
#include "AssemblyU2DCSharp_DataPattern3690539110.h"
#include "AssemblyU2DCSharp_DataStage1629502804.h"
#include "AssemblyU2DCSharp_DataStageChapter1740173401.h"
#include "AssemblyU2DCSharp_DataZone1853884054.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"
#include "AssemblyU2DCSharp_Mode2403779.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "AssemblyU2DCSharp_Diver66044126.h"
#include "AssemblyU2DCSharp_Pattern873562992.h"
#include "AssemblyU2DCSharp_Axe66286.h"

#pragma once
// InvBaseItem[]
struct InvBaseItemU5BU5D_t2171078168  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) InvBaseItem_t1636452469 * m_Items[1];

public:
	inline InvBaseItem_t1636452469 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InvBaseItem_t1636452469 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InvBaseItem_t1636452469 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// InvDatabase[]
struct InvDatabaseU5BU5D_t3324850277  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) InvDatabase_t852767852 * m_Items[1];

public:
	inline InvDatabase_t852767852 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InvDatabase_t852767852 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InvDatabase_t852767852 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// InvStat[]
struct InvStatU5BU5D_t3540370824  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) InvStat_t3624028613 * m_Items[1];

public:
	inline InvStat_t3624028613 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InvStat_t3624028613 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InvStat_t3624028613 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// InvGameItem[]
struct InvGameItemU5BU5D_t1443296723  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) InvGameItem_t1588794646 * m_Items[1];

public:
	inline InvGameItem_t1588794646 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InvGameItem_t1588794646 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InvGameItem_t1588794646 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// InvAttachmentPoint[]
struct InvAttachmentPointU5BU5D_t2968998389  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) InvAttachmentPoint_t1265837596 * m_Items[1];

public:
	inline InvAttachmentPoint_t1265837596 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline InvAttachmentPoint_t1265837596 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, InvAttachmentPoint_t1265837596 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UITweener[]
struct UITweenerU5BU5D_t996366285  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UITweener_t105489188 * m_Items[1];

public:
	inline UITweener_t105489188 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UITweener_t105489188 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UITweener_t105489188 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// EventDelegate[]
struct EventDelegateU5BU5D_t1029252742  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) EventDelegate_t4004424223 * m_Items[1];

public:
	inline EventDelegate_t4004424223 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline EventDelegate_t4004424223 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, EventDelegate_t4004424223 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// TypewriterEffect/FadeEntry[]
struct FadeEntryU5BU5D_t4004785971  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) FadeEntry_t1095831350  m_Items[1];

public:
	inline FadeEntry_t1095831350  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline FadeEntry_t1095831350 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, FadeEntry_t1095831350  value)
	{
		m_Items[index] = value;
	}
};
// UIButton[]
struct UIButtonU5BU5D_t2312321731  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIButton_t179429094 * m_Items[1];

public:
	inline UIButton_t179429094 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIButton_t179429094 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIButton_t179429094 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIButtonColor[]
struct UIButtonColorU5BU5D_t3320165072  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIButtonColor_t1546108957 * m_Items[1];

public:
	inline UIButtonColor_t1546108957 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIButtonColor_t1546108957 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIButtonColor_t1546108957 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIWidgetContainer[]
struct UIWidgetContainerU5BU5D_t2093242068  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIWidgetContainer_t1520767337 * m_Items[1];

public:
	inline UIWidgetContainer_t1520767337 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIWidgetContainer_t1520767337 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIWidgetContainer_t1520767337 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIDragDropItem[]
struct UIDragDropItemU5BU5D_t1078929775  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIDragDropItem_t2087865514 * m_Items[1];

public:
	inline UIDragDropItem_t2087865514 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIDragDropItem_t2087865514 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIDragDropItem_t2087865514 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIKeyBinding[]
struct UIKeyBindingU5BU5D_t620081791  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIKeyBinding_t2313833690 * m_Items[1];

public:
	inline UIKeyBinding_t2313833690 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIKeyBinding_t2313833690 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIKeyBinding_t2313833690 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIKeyNavigation[]
struct UIKeyNavigationU5BU5D_t1243790086  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIKeyNavigation_t1837256607 * m_Items[1];

public:
	inline UIKeyNavigation_t1837256607 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIKeyNavigation_t1837256607 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIKeyNavigation_t1837256607 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UILabel[]
struct UILabelU5BU5D_t1494885441  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UILabel_t291504320 * m_Items[1];

public:
	inline UILabel_t291504320 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UILabel_t291504320 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UILabel_t291504320 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIPlaySound[]
struct UIPlaySoundU5BU5D_t1195418110  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIPlaySound_t532605447 * m_Items[1];

public:
	inline UIPlaySound_t532605447 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIPlaySound_t532605447 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIPlaySound_t532605447 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIWidget[]
struct UIWidgetU5BU5D_t4236988201  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIWidget_t769069560 * m_Items[1];

public:
	inline UIWidget_t769069560 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIWidget_t769069560 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIWidget_t769069560 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIRect[]
struct UIRectU5BU5D_t1524091913  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIRect_t2503437976 * m_Items[1];

public:
	inline UIRect_t2503437976 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIRect_t2503437976 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIRect_t2503437976 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIToggle[]
struct UIToggleU5BU5D_t2046261209  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIToggle_t688812808 * m_Items[1];

public:
	inline UIToggle_t688812808 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIToggle_t688812808 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIToggle_t688812808 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIScrollView[]
struct UIScrollViewU5BU5D_t4274241891  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIScrollView_t2113479878 * m_Items[1];

public:
	inline UIScrollView_t2113479878 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIScrollView_t2113479878 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIScrollView_t2113479878 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIPanel[]
struct UIPanelU5BU5D_t3742972657  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIPanel_t295209936 * m_Items[1];

public:
	inline UIPanel_t295209936 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIPanel_t295209936 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIPanel_t295209936 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BMGlyph[]
struct BMGlyphU5BU5D_t3436589244  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) BMGlyph_t719052705 * m_Items[1];

public:
	inline BMGlyph_t719052705 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BMGlyph_t719052705 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BMGlyph_t719052705 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// EventDelegate/Parameter[]
struct ParameterU5BU5D_t3433801780  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Parameter_t3958428553 * m_Items[1];

public:
	inline Parameter_t3958428553 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Parameter_t3958428553 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Parameter_t3958428553 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UICamera[]
struct UICameraU5BU5D_t3682822564  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UICamera_t189364953 * m_Items[1];

public:
	inline UICamera_t189364953 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UICamera_t189364953 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UICamera_t189364953 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIRoot[]
struct UIRootU5BU5D_t1337058131  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIRoot_t2503447958 * m_Items[1];

public:
	inline UIRoot_t2503447958 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIRoot_t2503447958 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIRoot_t2503447958 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIDrawCall[]
struct UIDrawCallU5BU5D_t3158789363  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIDrawCall_t913273974 * m_Items[1];

public:
	inline UIDrawCall_t913273974 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIDrawCall_t913273974 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIDrawCall_t913273974 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UISpriteData[]
struct UISpriteDataU5BU5D_t2292174802  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UISpriteData_t3578345923 * m_Items[1];

public:
	inline UISpriteData_t3578345923 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UISpriteData_t3578345923 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UISpriteData_t3578345923 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIAtlas/Sprite[]
struct SpriteU5BU5D_t779503592  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Sprite_t2483154661 * m_Items[1];

public:
	inline Sprite_t2483154661 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Sprite_t2483154661 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Sprite_t2483154661 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UISprite[]
struct UISpriteU5BU5D_t3727562628  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UISprite_t661437049 * m_Items[1];

public:
	inline UISprite_t661437049 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UISprite_t661437049 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UISprite_t661437049 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIBasicSprite[]
struct UIBasicSpriteU5BU5D_t3513018950  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIBasicSprite_t2501337439 * m_Items[1];

public:
	inline UIBasicSprite_t2501337439 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIBasicSprite_t2501337439 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIBasicSprite_t2501337439 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UIFont[]
struct UIFontU5BU5D_t3954451346  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) UIFont_t2503090435 * m_Items[1];

public:
	inline UIFont_t2503090435 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline UIFont_t2503090435 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, UIFont_t2503090435 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t2832174062  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) MouseOrTouch_t908473047 * m_Items[1];

public:
	inline MouseOrTouch_t908473047 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline MouseOrTouch_t908473047 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, MouseOrTouch_t908473047 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UICamera/DepthEntry[]
struct DepthEntryU5BU5D_t2046984534  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DepthEntry_t2194697103  m_Items[1];

public:
	inline DepthEntry_t2194697103  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DepthEntry_t2194697103 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DepthEntry_t2194697103  value)
	{
		m_Items[index] = value;
	}
};
// BMSymbol[]
struct BMSymbolU5BU5D_t484807634  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) BMSymbol_t1170982339 * m_Items[1];

public:
	inline BMSymbol_t1170982339 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BMSymbol_t1170982339 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BMSymbol_t1170982339 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UITextList/Paragraph[]
struct ParagraphU5BU5D_t1733423547  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Paragraph_t3953256782 * m_Items[1];

public:
	inline Paragraph_t3953256782 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Paragraph_t3953256782 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Paragraph_t3953256782 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// BetterList`1<UITextList/Paragraph>[]
struct BetterList_1U5BU5D_t2063356863  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) BetterList_1_t1155257498 * m_Items[1];

public:
	inline BetterList_1_t1155257498 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline BetterList_1_t1155257498 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, BetterList_1_t1155257498 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DataAxe[]
struct DataAxeU5BU5D_t3511218445  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataAxe_t3107820260 * m_Items[1];

public:
	inline DataAxe_t3107820260 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataAxe_t3107820260 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataAxe_t3107820260 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DataChapter[]
struct DataChapterU5BU5D_t546866930  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataChapter_t925677859 * m_Items[1];

public:
	inline DataChapter_t925677859 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataChapter_t925677859 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataChapter_t925677859 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DataMonster[]
struct DataMonsterU5BU5D_t2222973585  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataMonster_t1423279280 * m_Items[1];

public:
	inline DataMonster_t1423279280 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataMonster_t1423279280 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataMonster_t1423279280 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DataObject[]
struct DataObjectU5BU5D_t1004266388  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataObject_t3139072937 * m_Items[1];

public:
	inline DataObject_t3139072937 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataObject_t3139072937 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataObject_t3139072937 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DataPattern[]
struct DataPatternU5BU5D_t1968315203  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataPattern_t3690539110 * m_Items[1];

public:
	inline DataPattern_t3690539110 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataPattern_t3690539110 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataPattern_t3690539110 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DataStage[]
struct DataStageU5BU5D_t2439749341  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataStage_t1629502804 * m_Items[1];

public:
	inline DataStage_t1629502804 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataStage_t1629502804 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataStage_t1629502804 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DataStageChapter[]
struct DataStageChapterU5BU5D_t909635108  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataStageChapter_t1740173401 * m_Items[1];

public:
	inline DataStageChapter_t1740173401 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataStageChapter_t1740173401 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataStageChapter_t1740173401 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// DataZone[]
struct DataZoneU5BU5D_t1647087187  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) DataZone_t1853884054 * m_Items[1];

public:
	inline DataZone_t1853884054 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DataZone_t1853884054 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DataZone_t1853884054 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Console/Log[]
struct LogU5BU5D_t3480530893  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Log_t76580  m_Items[1];

public:
	inline Log_t76580  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Log_t76580 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Log_t76580  value)
	{
		m_Items[index] = value;
	}
};
// Mode[]
struct ModeU5BU5D_t1506020306  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Mode_t2403781 * m_Items[1];

public:
	inline Mode_t2403781 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Mode_t2403781 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Mode_t2403781 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Monster[]
struct MonsterU5BU5D_t1253447711  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Monster_t2901270458 * m_Items[1];

public:
	inline Monster_t2901270458 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Monster_t2901270458 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Monster_t2901270458 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Diver[]
struct DiverU5BU5D_t3069344235  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Diver_t66044126 * m_Items[1];

public:
	inline Diver_t66044126 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Diver_t66044126 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Diver_t66044126 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pattern[]
struct PatternU5BU5D_t998789329  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Pattern_t873562992 * m_Items[1];

public:
	inline Pattern_t873562992 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Pattern_t873562992 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Pattern_t873562992 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Axe[]
struct AxeU5BU5D_t2300845467  : public Il2CppArray
{
public:
	ALIGN_TYPE (8) Axe_t66286 * m_Items[1];

public:
	inline Axe_t66286 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Axe_t66286 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Axe_t66286 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
