﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StageWater
struct StageWater_t3105299673;
// DataChapter
struct DataChapter_t925677859;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DataChapter925677859.h"
#include "mscorlib_System_String968488902.h"

// System.Void StageWater::.ctor()
extern "C"  void StageWater__ctor_m1194647522 (StageWater_t3105299673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageWater::Awake()
extern "C"  void StageWater_Awake_m1432252741 (StageWater_t3105299673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageWater::Init()
extern "C"  void StageWater_Init_m267168306 (StageWater_t3105299673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageWater::SetChapter(DataChapter)
extern "C"  void StageWater_SetChapter_m3493029346 (StageWater_t3105299673 * __this, DataChapter_t925677859 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageWater::SetTransfrom(System.Single,System.Single,System.Single,System.Single)
extern "C"  void StageWater_SetTransfrom_m2820899304 (StageWater_t3105299673 * __this, float ___width0, float ___height1, float ___mat_width2, float ___y3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageWater::SetTexture(System.String)
extern "C"  void StageWater_SetTexture_m242181735 (StageWater_t3105299673 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageWater::Move(System.Single,System.Single)
extern "C"  void StageWater_Move_m841455805 (StageWater_t3105299673 * __this, float ___x0, float ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
