﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Data/<LoadData>c__Iterator4
struct U3CLoadDataU3Ec__Iterator4_t1112965621;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Data/<LoadData>c__Iterator4::.ctor()
extern "C"  void U3CLoadDataU3Ec__Iterator4__ctor_m2258043601 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Data/<LoadData>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadDataU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m308608673 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Data/<LoadData>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadDataU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2709677109 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Data/<LoadData>c__Iterator4::MoveNext()
extern "C"  bool U3CLoadDataU3Ec__Iterator4_MoveNext_m577577859 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data/<LoadData>c__Iterator4::Dispose()
extern "C"  void U3CLoadDataU3Ec__Iterator4_Dispose_m919699150 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data/<LoadData>c__Iterator4::Reset()
extern "C"  void U3CLoadDataU3Ec__Iterator4_Reset_m4199443838 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
