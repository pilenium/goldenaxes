﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Move2404337.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveAccelerate
struct  MoveAccelerate_t3401092526  : public Move_t2404337
{
public:
	// System.Single MoveAccelerate::m_speed
	float ___m_speed_2;
	// System.Single MoveAccelerate::m_target_speed
	float ___m_target_speed_3;
	// System.Boolean MoveAccelerate::m_isTweening
	bool ___m_isTweening_4;

public:
	inline static int32_t get_offset_of_m_speed_2() { return static_cast<int32_t>(offsetof(MoveAccelerate_t3401092526, ___m_speed_2)); }
	inline float get_m_speed_2() const { return ___m_speed_2; }
	inline float* get_address_of_m_speed_2() { return &___m_speed_2; }
	inline void set_m_speed_2(float value)
	{
		___m_speed_2 = value;
	}

	inline static int32_t get_offset_of_m_target_speed_3() { return static_cast<int32_t>(offsetof(MoveAccelerate_t3401092526, ___m_target_speed_3)); }
	inline float get_m_target_speed_3() const { return ___m_target_speed_3; }
	inline float* get_address_of_m_target_speed_3() { return &___m_target_speed_3; }
	inline void set_m_target_speed_3(float value)
	{
		___m_target_speed_3 = value;
	}

	inline static int32_t get_offset_of_m_isTweening_4() { return static_cast<int32_t>(offsetof(MoveAccelerate_t3401092526, ___m_isTweening_4)); }
	inline bool get_m_isTweening_4() const { return ___m_isTweening_4; }
	inline bool* get_address_of_m_isTweening_4() { return &___m_isTweening_4; }
	inline void set_m_isTweening_4(bool value)
	{
		___m_isTweening_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
