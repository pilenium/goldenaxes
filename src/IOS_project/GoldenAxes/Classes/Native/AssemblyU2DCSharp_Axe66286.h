﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataAxe
struct DataAxe_t3107820260;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.BoxCollider
struct BoxCollider_t131631884;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Axe
struct  Axe_t66286  : public MonoBehaviour_t3012272455
{
public:
	// DataAxe Axe::m_data
	DataAxe_t3107820260 * ___m_data_2;
	// UnityEngine.Transform Axe::m_tr
	Transform_t284553113 * ___m_tr_3;
	// UnityEngine.GameObject Axe::m_axe
	GameObject_t4012695102 * ___m_axe_4;
	// UnityEngine.Transform Axe::m_axe_tr
	Transform_t284553113 * ___m_axe_tr_5;
	// UnityEngine.GameObject Axe::m_shadow
	GameObject_t4012695102 * ___m_shadow_6;
	// UnityEngine.Transform Axe::m_shadow_tr
	Transform_t284553113 * ___m_shadow_tr_7;
	// UnityEngine.BoxCollider Axe::m_collider
	BoxCollider_t131631884 * ___m_collider_8;
	// UnityEngine.Rigidbody Axe::m_rigidBody
	Rigidbody_t1972007546 * ___m_rigidBody_9;
	// System.Boolean Axe::m_enabled
	bool ___m_enabled_10;
	// System.Single Axe::m_time
	float ___m_time_11;
	// UnityEngine.Vector3 Axe::m_velocity
	Vector3_t3525329789  ___m_velocity_12;
	// System.Int32 Axe::m_damage
	int32_t ___m_damage_13;
	// System.Single Axe::m_rotate
	float ___m_rotate_14;
	// System.Single Axe::m_power
	float ___m_power_15;

public:
	inline static int32_t get_offset_of_m_data_2() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_data_2)); }
	inline DataAxe_t3107820260 * get_m_data_2() const { return ___m_data_2; }
	inline DataAxe_t3107820260 ** get_address_of_m_data_2() { return &___m_data_2; }
	inline void set_m_data_2(DataAxe_t3107820260 * value)
	{
		___m_data_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_data_2, value);
	}

	inline static int32_t get_offset_of_m_tr_3() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_tr_3)); }
	inline Transform_t284553113 * get_m_tr_3() const { return ___m_tr_3; }
	inline Transform_t284553113 ** get_address_of_m_tr_3() { return &___m_tr_3; }
	inline void set_m_tr_3(Transform_t284553113 * value)
	{
		___m_tr_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_3, value);
	}

	inline static int32_t get_offset_of_m_axe_4() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_axe_4)); }
	inline GameObject_t4012695102 * get_m_axe_4() const { return ___m_axe_4; }
	inline GameObject_t4012695102 ** get_address_of_m_axe_4() { return &___m_axe_4; }
	inline void set_m_axe_4(GameObject_t4012695102 * value)
	{
		___m_axe_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_axe_4, value);
	}

	inline static int32_t get_offset_of_m_axe_tr_5() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_axe_tr_5)); }
	inline Transform_t284553113 * get_m_axe_tr_5() const { return ___m_axe_tr_5; }
	inline Transform_t284553113 ** get_address_of_m_axe_tr_5() { return &___m_axe_tr_5; }
	inline void set_m_axe_tr_5(Transform_t284553113 * value)
	{
		___m_axe_tr_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_axe_tr_5, value);
	}

	inline static int32_t get_offset_of_m_shadow_6() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_shadow_6)); }
	inline GameObject_t4012695102 * get_m_shadow_6() const { return ___m_shadow_6; }
	inline GameObject_t4012695102 ** get_address_of_m_shadow_6() { return &___m_shadow_6; }
	inline void set_m_shadow_6(GameObject_t4012695102 * value)
	{
		___m_shadow_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_shadow_6, value);
	}

	inline static int32_t get_offset_of_m_shadow_tr_7() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_shadow_tr_7)); }
	inline Transform_t284553113 * get_m_shadow_tr_7() const { return ___m_shadow_tr_7; }
	inline Transform_t284553113 ** get_address_of_m_shadow_tr_7() { return &___m_shadow_tr_7; }
	inline void set_m_shadow_tr_7(Transform_t284553113 * value)
	{
		___m_shadow_tr_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_shadow_tr_7, value);
	}

	inline static int32_t get_offset_of_m_collider_8() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_collider_8)); }
	inline BoxCollider_t131631884 * get_m_collider_8() const { return ___m_collider_8; }
	inline BoxCollider_t131631884 ** get_address_of_m_collider_8() { return &___m_collider_8; }
	inline void set_m_collider_8(BoxCollider_t131631884 * value)
	{
		___m_collider_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_collider_8, value);
	}

	inline static int32_t get_offset_of_m_rigidBody_9() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_rigidBody_9)); }
	inline Rigidbody_t1972007546 * get_m_rigidBody_9() const { return ___m_rigidBody_9; }
	inline Rigidbody_t1972007546 ** get_address_of_m_rigidBody_9() { return &___m_rigidBody_9; }
	inline void set_m_rigidBody_9(Rigidbody_t1972007546 * value)
	{
		___m_rigidBody_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_rigidBody_9, value);
	}

	inline static int32_t get_offset_of_m_enabled_10() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_enabled_10)); }
	inline bool get_m_enabled_10() const { return ___m_enabled_10; }
	inline bool* get_address_of_m_enabled_10() { return &___m_enabled_10; }
	inline void set_m_enabled_10(bool value)
	{
		___m_enabled_10 = value;
	}

	inline static int32_t get_offset_of_m_time_11() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_time_11)); }
	inline float get_m_time_11() const { return ___m_time_11; }
	inline float* get_address_of_m_time_11() { return &___m_time_11; }
	inline void set_m_time_11(float value)
	{
		___m_time_11 = value;
	}

	inline static int32_t get_offset_of_m_velocity_12() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_velocity_12)); }
	inline Vector3_t3525329789  get_m_velocity_12() const { return ___m_velocity_12; }
	inline Vector3_t3525329789 * get_address_of_m_velocity_12() { return &___m_velocity_12; }
	inline void set_m_velocity_12(Vector3_t3525329789  value)
	{
		___m_velocity_12 = value;
	}

	inline static int32_t get_offset_of_m_damage_13() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_damage_13)); }
	inline int32_t get_m_damage_13() const { return ___m_damage_13; }
	inline int32_t* get_address_of_m_damage_13() { return &___m_damage_13; }
	inline void set_m_damage_13(int32_t value)
	{
		___m_damage_13 = value;
	}

	inline static int32_t get_offset_of_m_rotate_14() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_rotate_14)); }
	inline float get_m_rotate_14() const { return ___m_rotate_14; }
	inline float* get_address_of_m_rotate_14() { return &___m_rotate_14; }
	inline void set_m_rotate_14(float value)
	{
		___m_rotate_14 = value;
	}

	inline static int32_t get_offset_of_m_power_15() { return static_cast<int32_t>(offsetof(Axe_t66286, ___m_power_15)); }
	inline float get_m_power_15() const { return ___m_power_15; }
	inline float* get_address_of_m_power_15() { return &___m_power_15; }
	inline void set_m_power_15(float value)
	{
		___m_power_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
