﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Console/Log>
struct DefaultComparer_t3632997608;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Console/Log>::.ctor()
extern "C"  void DefaultComparer__ctor_m408929264_gshared (DefaultComparer_t3632997608 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m408929264(__this, method) ((  void (*) (DefaultComparer_t3632997608 *, const MethodInfo*))DefaultComparer__ctor_m408929264_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Console/Log>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m4208653631_gshared (DefaultComparer_t3632997608 * __this, Log_t76580  ___x0, Log_t76580  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m4208653631(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t3632997608 *, Log_t76580 , Log_t76580 , const MethodInfo*))DefaultComparer_Compare_m4208653631_gshared)(__this, ___x0, ___y1, method)
