﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataUser
struct DataUser_t1853738677;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void DataUser::.ctor()
extern "C"  void DataUser__ctor_m1737993094 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DataUser::get_id()
extern "C"  String_t* DataUser_get_id_m1162814117 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::set_id(System.String)
extern "C"  void DataUser_set_id_m147616012 (DataUser_t1853738677 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataUser::get_coin()
extern "C"  int32_t DataUser_get_coin_m2506806470 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::set_coin(System.Int32)
extern "C"  void DataUser_set_coin_m154068605 (DataUser_t1853738677 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataUser::get_diamond()
extern "C"  int32_t DataUser_get_diamond_m1529789281 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::set_diamond(System.Int32)
extern "C"  void DataUser_set_diamond_m1515606028 (DataUser_t1853738677 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataUser::get_chapter()
extern "C"  int32_t DataUser_get_chapter_m1684683802 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::set_chapter(System.Int32)
extern "C"  void DataUser_set_chapter_m1124172613 (DataUser_t1853738677 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataUser::get_stage()
extern "C"  int32_t DataUser_get_stage_m1852353931 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::set_stage(System.Int32)
extern "C"  void DataUser_set_stage_m866959798 (DataUser_t1853738677 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::Clear()
extern "C"  void DataUser_Clear_m3439093681 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::Finalize()
extern "C"  void DataUser_Finalize_m824016668 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::LoadData()
extern "C"  void DataUser_LoadData_m2013187118 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::MakeNewData()
extern "C"  void DataUser_MakeNewData_m516225792 (DataUser_t1853738677 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::SetInt(System.String,System.Int32)
extern "C"  void DataUser_SetInt_m916432896 (DataUser_t1853738677 * __this, String_t* ___key0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::SetFloat(System.String,System.Single)
extern "C"  void DataUser_SetFloat_m4108343375 (DataUser_t1853738677 * __this, String_t* ___key0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataUser::SetString(System.String,System.String)
extern "C"  void DataUser_SetString_m3018843111 (DataUser_t1853738677 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataUser::GetInt(System.String)
extern "C"  int32_t DataUser_GetInt_m3777241181 (DataUser_t1853738677 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DataUser::GetFloat(System.String)
extern "C"  float DataUser_GetFloat_m1584966802 (DataUser_t1853738677 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DataUser::GetString(System.String)
extern "C"  String_t* DataUser_GetString_m3856822746 (DataUser_t1853738677 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
