﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UILabel/ModifierFunc
struct ModifierFunc_t3846285915;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UILabel/ModifierFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void ModifierFunc__ctor_m2438770417 (ModifierFunc_t3846285915 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UILabel/ModifierFunc::Invoke(System.String)
extern "C"  String_t* ModifierFunc_Invoke_m2948767828 (ModifierFunc_t3846285915 * __this, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" String_t* pinvoke_delegate_wrapper_ModifierFunc_t3846285915(Il2CppObject* delegate, String_t* ___s0);
// System.IAsyncResult UILabel/ModifierFunc::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ModifierFunc_BeginInvoke_m3790988764 (ModifierFunc_t3846285915 * __this, String_t* ___s0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UILabel/ModifierFunc::EndInvoke(System.IAsyncResult)
extern "C"  String_t* ModifierFunc_EndInvoke_m3981066276 (ModifierFunc_t3846285915 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
