﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<DataMonster>
struct List_1_t2220238249;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataMonsters
struct  DataMonsters_t1171984835  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<DataMonster> DataMonsters::monsters
	List_1_t2220238249 * ___monsters_0;

public:
	inline static int32_t get_offset_of_monsters_0() { return static_cast<int32_t>(offsetof(DataMonsters_t1171984835, ___monsters_0)); }
	inline List_1_t2220238249 * get_monsters_0() const { return ___monsters_0; }
	inline List_1_t2220238249 ** get_address_of_monsters_0() { return &___monsters_0; }
	inline void set_monsters_0(List_1_t2220238249 * value)
	{
		___monsters_0 = value;
		Il2CppCodeGenWriteBarrier(&___monsters_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
