﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataBossMode
struct DataBossMode_t3617900698;

#include "codegen/il2cpp-codegen.h"

// System.Void DataBossMode::.ctor()
extern "C"  void DataBossMode__ctor_m1563572033 (DataBossMode_t3617900698 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
