﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.String UnityEngine.SystemInfo::get_operatingSystem()
extern "C"  String_t* SystemInfo_get_operatingSystem_m2538828082 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_graphicsDeviceVersion()
extern "C"  String_t* SystemInfo_get_graphicsDeviceVersion_m3634129081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
extern "C"  int32_t SystemInfo_get_graphicsShaderLevel_m1169417593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
extern "C"  String_t* SystemInfo_get_deviceUniqueIdentifier_m983206480 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
