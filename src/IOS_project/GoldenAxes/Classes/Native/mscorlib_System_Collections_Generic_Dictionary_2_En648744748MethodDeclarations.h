﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t881716807;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En648744748.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_370248105.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1181059640_gshared (Enumerator_t648744749 * __this, Dictionary_2_t881716807 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1181059640(__this, ___dictionary0, method) ((  void (*) (Enumerator_t648744749 *, Dictionary_2_t881716807 *, const MethodInfo*))Enumerator__ctor_m1181059640_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3013273001_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3013273001(__this, method) ((  Il2CppObject * (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3013273001_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3653964669_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3653964669(__this, method) ((  void (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3653964669_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t130027246  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m941740038_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m941740038(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m941740038_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3299311557_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3299311557(__this, method) ((  Il2CppObject * (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3299311557_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2000978199_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2000978199(__this, method) ((  Il2CppObject * (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2000978199_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2720231529_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2720231529(__this, method) ((  bool (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_MoveNext_m2720231529_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::get_Current()
extern "C"  KeyValuePair_2_t370248105  Enumerator_get_Current_m2789164199_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2789164199(__this, method) ((  KeyValuePair_2_t370248105  (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_get_Current_m2789164199_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::get_CurrentKey()
extern "C"  int32_t Enumerator_get_CurrentKey_m2113011446_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m2113011446(__this, method) ((  int32_t (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_get_CurrentKey_m2113011446_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::get_CurrentValue()
extern "C"  Color_t1588175760  Enumerator_get_CurrentValue_m486625178_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m486625178(__this, method) ((  Color_t1588175760  (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_get_CurrentValue_m486625178_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::Reset()
extern "C"  void Enumerator_Reset_m561139850_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_Reset_m561139850(__this, method) ((  void (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_Reset_m561139850_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1822789907_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1822789907(__this, method) ((  void (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_VerifyState_m1822789907_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m2099315643_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m2099315643(__this, method) ((  void (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_VerifyCurrent_m2099315643_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.LogType,UnityEngine.Color>::Dispose()
extern "C"  void Enumerator_Dispose_m612945626_gshared (Enumerator_t648744749 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m612945626(__this, method) ((  void (*) (Enumerator_t648744749 *, const MethodInfo*))Enumerator_Dispose_m612945626_gshared)(__this, method)
