﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation
struct Animation_t350396337;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectGod
struct  EffectGod_t535283979  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Animation EffectGod::m_animation
	Animation_t350396337 * ___m_animation_2;

public:
	inline static int32_t get_offset_of_m_animation_2() { return static_cast<int32_t>(offsetof(EffectGod_t535283979, ___m_animation_2)); }
	inline Animation_t350396337 * get_m_animation_2() const { return ___m_animation_2; }
	inline Animation_t350396337 ** get_address_of_m_animation_2() { return &___m_animation_2; }
	inline void set_m_animation_2(Animation_t350396337 * value)
	{
		___m_animation_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_animation_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
