﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEvent
struct  GameEvent_t2981166504  : public Il2CppObject
{
public:
	// System.Single GameEvent::m_prob
	float ___m_prob_0;
	// System.Single GameEvent::m_event
	float ___m_event_1;

public:
	inline static int32_t get_offset_of_m_prob_0() { return static_cast<int32_t>(offsetof(GameEvent_t2981166504, ___m_prob_0)); }
	inline float get_m_prob_0() const { return ___m_prob_0; }
	inline float* get_address_of_m_prob_0() { return &___m_prob_0; }
	inline void set_m_prob_0(float value)
	{
		___m_prob_0 = value;
	}

	inline static int32_t get_offset_of_m_event_1() { return static_cast<int32_t>(offsetof(GameEvent_t2981166504, ___m_event_1)); }
	inline float get_m_event_1() const { return ___m_event_1; }
	inline float* get_address_of_m_event_1() { return &___m_event_1; }
	inline void set_m_event_1(float value)
	{
		___m_event_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
