﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen277738608.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_370248105.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2868838327_gshared (InternalEnumerator_1_t277738608 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2868838327(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t277738608 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2868838327_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3184071817_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3184071817(__this, method) ((  void (*) (InternalEnumerator_1_t277738608 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3184071817_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3517943285_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3517943285(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t277738608 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3517943285_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m231699022_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m231699022(__this, method) ((  void (*) (InternalEnumerator_1_t277738608 *, const MethodInfo*))InternalEnumerator_1_Dispose_m231699022_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2775239157_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2775239157(__this, method) ((  bool (*) (InternalEnumerator_1_t277738608 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2775239157_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::get_Current()
extern "C"  KeyValuePair_2_t370248105  InternalEnumerator_1_get_Current_m3903580542_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3903580542(__this, method) ((  KeyValuePair_2_t370248105  (*) (InternalEnumerator_1_t277738608 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3903580542_gshared)(__this, method)
