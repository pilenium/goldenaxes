﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScaleAnimation
struct  ScaleAnimation_t1299566970  : public MonoBehaviour_t3012272455
{
public:
	// System.Single ScaleAnimation::speed
	float ___speed_2;
	// System.Single ScaleAnimation::min_size
	float ___min_size_3;
	// System.Single ScaleAnimation::max_size
	float ___max_size_4;
	// System.Boolean ScaleAnimation::random
	bool ___random_5;
	// UnityEngine.Transform ScaleAnimation::m_tr
	Transform_t284553113 * ___m_tr_6;
	// UnityEngine.Vector3 ScaleAnimation::m_defaultScale
	Vector3_t3525329789  ___m_defaultScale_7;
	// System.Single ScaleAnimation::m_time
	float ___m_time_8;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(ScaleAnimation_t1299566970, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_min_size_3() { return static_cast<int32_t>(offsetof(ScaleAnimation_t1299566970, ___min_size_3)); }
	inline float get_min_size_3() const { return ___min_size_3; }
	inline float* get_address_of_min_size_3() { return &___min_size_3; }
	inline void set_min_size_3(float value)
	{
		___min_size_3 = value;
	}

	inline static int32_t get_offset_of_max_size_4() { return static_cast<int32_t>(offsetof(ScaleAnimation_t1299566970, ___max_size_4)); }
	inline float get_max_size_4() const { return ___max_size_4; }
	inline float* get_address_of_max_size_4() { return &___max_size_4; }
	inline void set_max_size_4(float value)
	{
		___max_size_4 = value;
	}

	inline static int32_t get_offset_of_random_5() { return static_cast<int32_t>(offsetof(ScaleAnimation_t1299566970, ___random_5)); }
	inline bool get_random_5() const { return ___random_5; }
	inline bool* get_address_of_random_5() { return &___random_5; }
	inline void set_random_5(bool value)
	{
		___random_5 = value;
	}

	inline static int32_t get_offset_of_m_tr_6() { return static_cast<int32_t>(offsetof(ScaleAnimation_t1299566970, ___m_tr_6)); }
	inline Transform_t284553113 * get_m_tr_6() const { return ___m_tr_6; }
	inline Transform_t284553113 ** get_address_of_m_tr_6() { return &___m_tr_6; }
	inline void set_m_tr_6(Transform_t284553113 * value)
	{
		___m_tr_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_6, value);
	}

	inline static int32_t get_offset_of_m_defaultScale_7() { return static_cast<int32_t>(offsetof(ScaleAnimation_t1299566970, ___m_defaultScale_7)); }
	inline Vector3_t3525329789  get_m_defaultScale_7() const { return ___m_defaultScale_7; }
	inline Vector3_t3525329789 * get_address_of_m_defaultScale_7() { return &___m_defaultScale_7; }
	inline void set_m_defaultScale_7(Vector3_t3525329789  value)
	{
		___m_defaultScale_7 = value;
	}

	inline static int32_t get_offset_of_m_time_8() { return static_cast<int32_t>(offsetof(ScaleAnimation_t1299566970, ___m_time_8)); }
	inline float get_m_time_8() const { return ___m_time_8; }
	inline float* get_address_of_m_time_8() { return &___m_time_8; }
	inline void set_m_time_8(float value)
	{
		___m_time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
