﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Monster>::.ctor()
#define List_1__ctor_m1497044697(__this, method) ((  void (*) (List_1_t3698229427 *, const MethodInfo*))List_1__ctor_m574172797_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Monster>::.ctor(System.Int32)
#define List_1__ctor_m3893167617(__this, ___capacity0, method) ((  void (*) (List_1_t3698229427 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Monster>::.cctor()
#define List_1__cctor_m2184186109(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Monster>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2549694722(__this, method) ((  Il2CppObject* (*) (List_1_t3698229427 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Monster>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3131124372(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3698229427 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Monster>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3134743823(__this, method) ((  Il2CppObject * (*) (List_1_t3698229427 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Monster>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m4283197890(__this, ___item0, method) ((  int32_t (*) (List_1_t3698229427 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Monster>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m4265831370(__this, ___item0, method) ((  bool (*) (List_1_t3698229427 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Monster>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m4208848666(__this, ___item0, method) ((  int32_t (*) (List_1_t3698229427 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Monster>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1275235653(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3698229427 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Monster>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m708571075(__this, ___item0, method) ((  void (*) (List_1_t3698229427 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Monster>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m4183467851(__this, method) ((  bool (*) (List_1_t3698229427 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Monster>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3884965714(__this, method) ((  bool (*) (List_1_t3698229427 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Monster>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2499747966(__this, method) ((  Il2CppObject * (*) (List_1_t3698229427 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Monster>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m2668772025(__this, method) ((  bool (*) (List_1_t3698229427 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Monster>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3311044896(__this, method) ((  bool (*) (List_1_t3698229427 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Monster>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2436688517(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3698229427 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Monster>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2049735644(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3698229427 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Monster>::Add(T)
#define List_1_Add_m2240958927(__this, ___item0, method) ((  void (*) (List_1_t3698229427 *, Monster_t2901270458 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Monster>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1235321482(__this, ___newCount0, method) ((  void (*) (List_1_t3698229427 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Monster>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m374681992(__this, ___collection0, method) ((  void (*) (List_1_t3698229427 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Monster>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m3930621512(__this, ___enumerable0, method) ((  void (*) (List_1_t3698229427 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Monster>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3544161103(__this, ___collection0, method) ((  void (*) (List_1_t3698229427 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Monster>::AsReadOnly()
#define List_1_AsReadOnly_m210083706(__this, method) ((  ReadOnlyCollection_1_t1769448510 * (*) (List_1_t3698229427 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Monster>::Clear()
#define List_1_Clear_m2756941019(__this, method) ((  void (*) (List_1_t3698229427 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Monster>::Contains(T)
#define List_1_Contains_m1167979273(__this, ___item0, method) ((  bool (*) (List_1_t3698229427 *, Monster_t2901270458 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Monster>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m132842495(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3698229427 *, MonsterU5BU5D_t1253447711*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Monster>::Find(System.Predicate`1<T>)
#define List_1_Find_m2129858038(__this, ___match0, method) ((  Monster_t2901270458 * (*) (List_1_t3698229427 *, Predicate_1_t3472234356 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Monster>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m1345464772(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3472234356 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Monster>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m1259128809(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3698229427 *, int32_t, int32_t, Predicate_1_t3472234356 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Monster>::GetEnumerator()
#define List_1_GetEnumerator_m757694428(__this, method) ((  Enumerator_t1784012419  (*) (List_1_t3698229427 *, const MethodInfo*))List_1_GetEnumerator_m2326457258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Monster>::IndexOf(T)
#define List_1_IndexOf_m3638079683(__this, ___item0, method) ((  int32_t (*) (List_1_t3698229427 *, Monster_t2901270458 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Monster>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3602541398(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3698229427 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Monster>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m4174176975(__this, ___index0, method) ((  void (*) (List_1_t3698229427 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Monster>::Insert(System.Int32,T)
#define List_1_Insert_m1479020086(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3698229427 *, int32_t, Monster_t2901270458 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Monster>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m4113777195(__this, ___collection0, method) ((  void (*) (List_1_t3698229427 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Monster>::Remove(T)
#define List_1_Remove_m1922425156(__this, ___item0, method) ((  bool (*) (List_1_t3698229427 *, Monster_t2901270458 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Monster>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m18297926(__this, ___match0, method) ((  int32_t (*) (List_1_t3698229427 *, Predicate_1_t3472234356 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Monster>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3647840252(__this, ___index0, method) ((  void (*) (List_1_t3698229427 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Monster>::Reverse()
#define List_1_Reverse_m1088184560(__this, method) ((  void (*) (List_1_t3698229427 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Monster>::Sort()
#define List_1_Sort_m2766931122(__this, method) ((  void (*) (List_1_t3698229427 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Monster>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m4065566597(__this, ___comparison0, method) ((  void (*) (List_1_t3698229427 *, Comparison_1_t1309978038 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Monster>::ToArray()
#define List_1_ToArray_m2770481199(__this, method) ((  MonsterU5BU5D_t1253447711* (*) (List_1_t3698229427 *, const MethodInfo*))List_1_ToArray_m238588755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Monster>::TrimExcess()
#define List_1_TrimExcess_m2367510219(__this, method) ((  void (*) (List_1_t3698229427 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Monster>::get_Capacity()
#define List_1_get_Capacity_m1205636979(__this, method) ((  int32_t (*) (List_1_t3698229427 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Monster>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m2740477596(__this, ___value0, method) ((  void (*) (List_1_t3698229427 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Monster>::get_Count()
#define List_1_get_Count_m928627992(__this, method) ((  int32_t (*) (List_1_t3698229427 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<Monster>::get_Item(System.Int32)
#define List_1_get_Item_m812737728(__this, ___index0, method) ((  Monster_t2901270458 * (*) (List_1_t3698229427 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Monster>::set_Item(System.Int32,T)
#define List_1_set_Item_m1513724301(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3698229427 *, int32_t, Monster_t2901270458 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
