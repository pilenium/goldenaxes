﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataMonsters
struct DataMonsters_t1171984835;
// DataMonster
struct DataMonster_t1423279280;

#include "codegen/il2cpp-codegen.h"

// System.Void DataMonsters::.ctor()
extern "C"  void DataMonsters__ctor_m2629905464 (DataMonsters_t1171984835 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataMonster DataMonsters::getData(System.UInt32)
extern "C"  DataMonster_t1423279280 * DataMonsters_getData_m2242462663 (DataMonsters_t1171984835 * __this, uint32_t ___monster_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
