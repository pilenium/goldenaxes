﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation[]
struct AnimationU5BU5D_t2512821868;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectAnimation
struct  EffectAnimation_t1201827027  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Animation[] EffectAnimation::m_animations
	AnimationU5BU5D_t2512821868* ___m_animations_2;

public:
	inline static int32_t get_offset_of_m_animations_2() { return static_cast<int32_t>(offsetof(EffectAnimation_t1201827027, ___m_animations_2)); }
	inline AnimationU5BU5D_t2512821868* get_m_animations_2() const { return ___m_animations_2; }
	inline AnimationU5BU5D_t2512821868** get_address_of_m_animations_2() { return &___m_animations_2; }
	inline void set_m_animations_2(AnimationU5BU5D_t2512821868* value)
	{
		___m_animations_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_animations_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
