﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pattern/<RemoveMonster>c__AnonStorey8
struct U3CRemoveMonsterU3Ec__AnonStorey8_t1166730293;
// Monster
struct Monster_t2901270458;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"

// System.Void Pattern/<RemoveMonster>c__AnonStorey8::.ctor()
extern "C"  void U3CRemoveMonsterU3Ec__AnonStorey8__ctor_m695866453 (U3CRemoveMonsterU3Ec__AnonStorey8_t1166730293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pattern/<RemoveMonster>c__AnonStorey8::<>m__9(Monster)
extern "C"  bool U3CRemoveMonsterU3Ec__AnonStorey8_U3CU3Em__9_m588340879 (U3CRemoveMonsterU3Ec__AnonStorey8_t1166730293 * __this, Monster_t2901270458 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
