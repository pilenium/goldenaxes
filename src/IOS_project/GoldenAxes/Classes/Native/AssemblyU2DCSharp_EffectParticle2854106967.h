﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.ParticleSystem
struct ParticleSystem_t56787138;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectParticle
struct  EffectParticle_t2854106967  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.ParticleSystem EffectParticle::m_system
	ParticleSystem_t56787138 * ___m_system_2;

public:
	inline static int32_t get_offset_of_m_system_2() { return static_cast<int32_t>(offsetof(EffectParticle_t2854106967, ___m_system_2)); }
	inline ParticleSystem_t56787138 * get_m_system_2() const { return ___m_system_2; }
	inline ParticleSystem_t56787138 ** get_address_of_m_system_2() { return &___m_system_2; }
	inline void set_m_system_2(ParticleSystem_t56787138 * value)
	{
		___m_system_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_system_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
