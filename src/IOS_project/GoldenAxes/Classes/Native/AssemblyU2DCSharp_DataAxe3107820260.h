﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataAxe
struct  DataAxe_t3107820260  : public Il2CppObject
{
public:
	// System.UInt32 DataAxe::id
	uint32_t ___id_0;
	// System.String DataAxe::name
	String_t* ___name_1;
	// System.String DataAxe::type
	String_t* ___type_2;
	// System.String DataAxe::url
	String_t* ___url_3;
	// System.Single DataAxe::size
	float ___size_4;
	// System.Single DataAxe::throw_height
	float ___throw_height_5;
	// System.String DataAxe::control
	String_t* ___control_6;
	// System.Int32 DataAxe::max_axe
	int32_t ___max_axe_7;
	// System.String DataAxe::shadow
	String_t* ___shadow_8;
	// UnityEngine.Vector3 DataAxe::shadowSize
	Vector3_t3525329789  ___shadowSize_9;
	// UnityEngine.Vector3 DataAxe::collider_size
	Vector3_t3525329789  ___collider_size_10;
	// UnityEngine.Vector3 DataAxe::collider_center
	Vector3_t3525329789  ___collider_center_11;
	// System.Single DataAxe::degree
	float ___degree_12;
	// System.Single DataAxe::degree_rad
	float ___degree_rad_13;
	// System.Single DataAxe::sin
	float ___sin_14;
	// System.Single DataAxe::cos
	float ___cos_15;
	// System.Single DataAxe::gravity
	float ___gravity_16;
	// System.Single DataAxe::rotation
	float ___rotation_17;
	// UnityEngine.Vector3 DataAxe::max_velocity
	Vector3_t3525329789  ___max_velocity_18;
	// UnityEngine.Vector3 DataAxe::min_velocity
	Vector3_t3525329789  ___min_velocity_19;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___id_0)); }
	inline uint32_t get_id_0() const { return ___id_0; }
	inline uint32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___type_2)); }
	inline String_t* get_type_2() const { return ___type_2; }
	inline String_t** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(String_t* value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_2, value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier(&___url_3, value);
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___size_4)); }
	inline float get_size_4() const { return ___size_4; }
	inline float* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(float value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_throw_height_5() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___throw_height_5)); }
	inline float get_throw_height_5() const { return ___throw_height_5; }
	inline float* get_address_of_throw_height_5() { return &___throw_height_5; }
	inline void set_throw_height_5(float value)
	{
		___throw_height_5 = value;
	}

	inline static int32_t get_offset_of_control_6() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___control_6)); }
	inline String_t* get_control_6() const { return ___control_6; }
	inline String_t** get_address_of_control_6() { return &___control_6; }
	inline void set_control_6(String_t* value)
	{
		___control_6 = value;
		Il2CppCodeGenWriteBarrier(&___control_6, value);
	}

	inline static int32_t get_offset_of_max_axe_7() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___max_axe_7)); }
	inline int32_t get_max_axe_7() const { return ___max_axe_7; }
	inline int32_t* get_address_of_max_axe_7() { return &___max_axe_7; }
	inline void set_max_axe_7(int32_t value)
	{
		___max_axe_7 = value;
	}

	inline static int32_t get_offset_of_shadow_8() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___shadow_8)); }
	inline String_t* get_shadow_8() const { return ___shadow_8; }
	inline String_t** get_address_of_shadow_8() { return &___shadow_8; }
	inline void set_shadow_8(String_t* value)
	{
		___shadow_8 = value;
		Il2CppCodeGenWriteBarrier(&___shadow_8, value);
	}

	inline static int32_t get_offset_of_shadowSize_9() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___shadowSize_9)); }
	inline Vector3_t3525329789  get_shadowSize_9() const { return ___shadowSize_9; }
	inline Vector3_t3525329789 * get_address_of_shadowSize_9() { return &___shadowSize_9; }
	inline void set_shadowSize_9(Vector3_t3525329789  value)
	{
		___shadowSize_9 = value;
	}

	inline static int32_t get_offset_of_collider_size_10() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___collider_size_10)); }
	inline Vector3_t3525329789  get_collider_size_10() const { return ___collider_size_10; }
	inline Vector3_t3525329789 * get_address_of_collider_size_10() { return &___collider_size_10; }
	inline void set_collider_size_10(Vector3_t3525329789  value)
	{
		___collider_size_10 = value;
	}

	inline static int32_t get_offset_of_collider_center_11() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___collider_center_11)); }
	inline Vector3_t3525329789  get_collider_center_11() const { return ___collider_center_11; }
	inline Vector3_t3525329789 * get_address_of_collider_center_11() { return &___collider_center_11; }
	inline void set_collider_center_11(Vector3_t3525329789  value)
	{
		___collider_center_11 = value;
	}

	inline static int32_t get_offset_of_degree_12() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___degree_12)); }
	inline float get_degree_12() const { return ___degree_12; }
	inline float* get_address_of_degree_12() { return &___degree_12; }
	inline void set_degree_12(float value)
	{
		___degree_12 = value;
	}

	inline static int32_t get_offset_of_degree_rad_13() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___degree_rad_13)); }
	inline float get_degree_rad_13() const { return ___degree_rad_13; }
	inline float* get_address_of_degree_rad_13() { return &___degree_rad_13; }
	inline void set_degree_rad_13(float value)
	{
		___degree_rad_13 = value;
	}

	inline static int32_t get_offset_of_sin_14() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___sin_14)); }
	inline float get_sin_14() const { return ___sin_14; }
	inline float* get_address_of_sin_14() { return &___sin_14; }
	inline void set_sin_14(float value)
	{
		___sin_14 = value;
	}

	inline static int32_t get_offset_of_cos_15() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___cos_15)); }
	inline float get_cos_15() const { return ___cos_15; }
	inline float* get_address_of_cos_15() { return &___cos_15; }
	inline void set_cos_15(float value)
	{
		___cos_15 = value;
	}

	inline static int32_t get_offset_of_gravity_16() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___gravity_16)); }
	inline float get_gravity_16() const { return ___gravity_16; }
	inline float* get_address_of_gravity_16() { return &___gravity_16; }
	inline void set_gravity_16(float value)
	{
		___gravity_16 = value;
	}

	inline static int32_t get_offset_of_rotation_17() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___rotation_17)); }
	inline float get_rotation_17() const { return ___rotation_17; }
	inline float* get_address_of_rotation_17() { return &___rotation_17; }
	inline void set_rotation_17(float value)
	{
		___rotation_17 = value;
	}

	inline static int32_t get_offset_of_max_velocity_18() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___max_velocity_18)); }
	inline Vector3_t3525329789  get_max_velocity_18() const { return ___max_velocity_18; }
	inline Vector3_t3525329789 * get_address_of_max_velocity_18() { return &___max_velocity_18; }
	inline void set_max_velocity_18(Vector3_t3525329789  value)
	{
		___max_velocity_18 = value;
	}

	inline static int32_t get_offset_of_min_velocity_19() { return static_cast<int32_t>(offsetof(DataAxe_t3107820260, ___min_velocity_19)); }
	inline Vector3_t3525329789  get_min_velocity_19() const { return ___min_velocity_19; }
	inline Vector3_t3525329789 * get_address_of_min_velocity_19() { return &___min_velocity_19; }
	inline void set_min_velocity_19(Vector3_t3525329789  value)
	{
		___min_velocity_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
