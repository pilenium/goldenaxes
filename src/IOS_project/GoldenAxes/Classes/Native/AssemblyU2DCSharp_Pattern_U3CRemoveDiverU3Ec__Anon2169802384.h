﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Diver
struct Diver_t66044126;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pattern/<RemoveDiver>c__AnonStorey7
struct  U3CRemoveDiverU3Ec__AnonStorey7_t2169802384  : public Il2CppObject
{
public:
	// Diver Pattern/<RemoveDiver>c__AnonStorey7::diver
	Diver_t66044126 * ___diver_0;

public:
	inline static int32_t get_offset_of_diver_0() { return static_cast<int32_t>(offsetof(U3CRemoveDiverU3Ec__AnonStorey7_t2169802384, ___diver_0)); }
	inline Diver_t66044126 * get_diver_0() const { return ___diver_0; }
	inline Diver_t66044126 ** get_address_of_diver_0() { return &___diver_0; }
	inline void set_diver_0(Diver_t66044126 * value)
	{
		___diver_0 = value;
		Il2CppCodeGenWriteBarrier(&___diver_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
