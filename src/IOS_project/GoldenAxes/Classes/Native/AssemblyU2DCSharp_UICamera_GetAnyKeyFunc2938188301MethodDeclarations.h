﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/GetAnyKeyFunc
struct GetAnyKeyFunc_t2938188301;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UICamera/GetAnyKeyFunc::.ctor(System.Object,System.IntPtr)
extern "C"  void GetAnyKeyFunc__ctor_m2645721214 (GetAnyKeyFunc_t2938188301 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera/GetAnyKeyFunc::Invoke()
extern "C"  bool GetAnyKeyFunc_Invoke_m2640178444 (GetAnyKeyFunc_t2938188301 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" bool pinvoke_delegate_wrapper_GetAnyKeyFunc_t2938188301(Il2CppObject* delegate);
// System.IAsyncResult UICamera/GetAnyKeyFunc::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetAnyKeyFunc_BeginInvoke_m2143801003 (GetAnyKeyFunc_t2938188301 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera/GetAnyKeyFunc::EndInvoke(System.IAsyncResult)
extern "C"  bool GetAnyKeyFunc_EndInvoke_m795164994 (GetAnyKeyFunc_t2938188301 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
