﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.Renderer
struct Renderer_t1092684080;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonsterProgress
struct  MonsterProgress_t2391190535  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform MonsterProgress::m_tr
	Transform_t284553113 * ___m_tr_2;
	// UnityEngine.Renderer MonsterProgress::m_progress_renderer
	Renderer_t1092684080 * ___m_progress_renderer_3;

public:
	inline static int32_t get_offset_of_m_tr_2() { return static_cast<int32_t>(offsetof(MonsterProgress_t2391190535, ___m_tr_2)); }
	inline Transform_t284553113 * get_m_tr_2() const { return ___m_tr_2; }
	inline Transform_t284553113 ** get_address_of_m_tr_2() { return &___m_tr_2; }
	inline void set_m_tr_2(Transform_t284553113 * value)
	{
		___m_tr_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_2, value);
	}

	inline static int32_t get_offset_of_m_progress_renderer_3() { return static_cast<int32_t>(offsetof(MonsterProgress_t2391190535, ___m_progress_renderer_3)); }
	inline Renderer_t1092684080 * get_m_progress_renderer_3() const { return ___m_progress_renderer_3; }
	inline Renderer_t1092684080 ** get_address_of_m_progress_renderer_3() { return &___m_progress_renderer_3; }
	inline void set_m_progress_renderer_3(Renderer_t1092684080 * value)
	{
		___m_progress_renderer_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_progress_renderer_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
