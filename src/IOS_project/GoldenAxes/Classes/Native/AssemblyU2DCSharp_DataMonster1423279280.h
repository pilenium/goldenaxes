﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// DataMonsterAni
struct DataMonsterAni_t995950348;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataMonster
struct  DataMonster_t1423279280  : public Il2CppObject
{
public:
	// System.UInt32 DataMonster::id
	uint32_t ___id_0;
	// System.String DataMonster::name
	String_t* ___name_1;
	// System.Int32 DataMonster::health
	int32_t ___health_2;
	// System.String DataMonster::type
	String_t* ___type_3;
	// System.String DataMonster::url
	String_t* ___url_4;
	// System.Single DataMonster::size
	float ___size_5;
	// System.Single DataMonster::scale
	float ___scale_6;
	// DataMonsterAni DataMonster::animations
	DataMonsterAni_t995950348 * ___animations_7;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(DataMonster_t1423279280, ___id_0)); }
	inline uint32_t get_id_0() const { return ___id_0; }
	inline uint32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(DataMonster_t1423279280, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_health_2() { return static_cast<int32_t>(offsetof(DataMonster_t1423279280, ___health_2)); }
	inline int32_t get_health_2() const { return ___health_2; }
	inline int32_t* get_address_of_health_2() { return &___health_2; }
	inline void set_health_2(int32_t value)
	{
		___health_2 = value;
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(DataMonster_t1423279280, ___type_3)); }
	inline String_t* get_type_3() const { return ___type_3; }
	inline String_t** get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(String_t* value)
	{
		___type_3 = value;
		Il2CppCodeGenWriteBarrier(&___type_3, value);
	}

	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(DataMonster_t1423279280, ___url_4)); }
	inline String_t* get_url_4() const { return ___url_4; }
	inline String_t** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(String_t* value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier(&___url_4, value);
	}

	inline static int32_t get_offset_of_size_5() { return static_cast<int32_t>(offsetof(DataMonster_t1423279280, ___size_5)); }
	inline float get_size_5() const { return ___size_5; }
	inline float* get_address_of_size_5() { return &___size_5; }
	inline void set_size_5(float value)
	{
		___size_5 = value;
	}

	inline static int32_t get_offset_of_scale_6() { return static_cast<int32_t>(offsetof(DataMonster_t1423279280, ___scale_6)); }
	inline float get_scale_6() const { return ___scale_6; }
	inline float* get_address_of_scale_6() { return &___scale_6; }
	inline void set_scale_6(float value)
	{
		___scale_6 = value;
	}

	inline static int32_t get_offset_of_animations_7() { return static_cast<int32_t>(offsetof(DataMonster_t1423279280, ___animations_7)); }
	inline DataMonsterAni_t995950348 * get_animations_7() const { return ___animations_7; }
	inline DataMonsterAni_t995950348 ** get_address_of_animations_7() { return &___animations_7; }
	inline void set_animations_7(DataMonsterAni_t995950348 * value)
	{
		___animations_7 = value;
		Il2CppCodeGenWriteBarrier(&___animations_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
