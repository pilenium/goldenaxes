﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Common
struct Common_t2024019467;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Common_DIRECTION1824003935.h"
#include "mscorlib_System_String968488902.h"

// System.Void Common::.ctor()
extern "C"  void Common__ctor_m453241584 (Common_t2024019467 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common::.cctor()
extern "C"  void Common__cctor_m683491005 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Common/DIRECTION Common::ConvertDirection(System.String)
extern "C"  int32_t Common_ConvertDirection_m1352663584 (Il2CppObject * __this /* static, unused */, String_t* ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common::IsEast(Common/DIRECTION)
extern "C"  bool Common_IsEast_m3103083476 (Il2CppObject * __this /* static, unused */, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Common::IsNorth(Common/DIRECTION)
extern "C"  bool Common_IsNorth_m1443366618 (Il2CppObject * __this /* static, unused */, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
