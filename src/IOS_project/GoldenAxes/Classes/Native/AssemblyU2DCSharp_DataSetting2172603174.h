﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataSetting
struct  DataSetting_t2172603174  : public Il2CppObject
{
public:
	// System.Int32 DataSetting::TOUCH_COUNT
	int32_t ___TOUCH_COUNT_0;
	// System.Single DataSetting::BG_SCROLL_TOP_GAP
	float ___BG_SCROLL_TOP_GAP_1;
	// System.Single DataSetting::BG_SCROLL_BOTTOM_GAP
	float ___BG_SCROLL_BOTTOM_GAP_2;
	// System.Single DataSetting::MAX_DISTANCE
	float ___MAX_DISTANCE_3;
	// System.Single DataSetting::MIN_DISTANCE
	float ___MIN_DISTANCE_4;

public:
	inline static int32_t get_offset_of_TOUCH_COUNT_0() { return static_cast<int32_t>(offsetof(DataSetting_t2172603174, ___TOUCH_COUNT_0)); }
	inline int32_t get_TOUCH_COUNT_0() const { return ___TOUCH_COUNT_0; }
	inline int32_t* get_address_of_TOUCH_COUNT_0() { return &___TOUCH_COUNT_0; }
	inline void set_TOUCH_COUNT_0(int32_t value)
	{
		___TOUCH_COUNT_0 = value;
	}

	inline static int32_t get_offset_of_BG_SCROLL_TOP_GAP_1() { return static_cast<int32_t>(offsetof(DataSetting_t2172603174, ___BG_SCROLL_TOP_GAP_1)); }
	inline float get_BG_SCROLL_TOP_GAP_1() const { return ___BG_SCROLL_TOP_GAP_1; }
	inline float* get_address_of_BG_SCROLL_TOP_GAP_1() { return &___BG_SCROLL_TOP_GAP_1; }
	inline void set_BG_SCROLL_TOP_GAP_1(float value)
	{
		___BG_SCROLL_TOP_GAP_1 = value;
	}

	inline static int32_t get_offset_of_BG_SCROLL_BOTTOM_GAP_2() { return static_cast<int32_t>(offsetof(DataSetting_t2172603174, ___BG_SCROLL_BOTTOM_GAP_2)); }
	inline float get_BG_SCROLL_BOTTOM_GAP_2() const { return ___BG_SCROLL_BOTTOM_GAP_2; }
	inline float* get_address_of_BG_SCROLL_BOTTOM_GAP_2() { return &___BG_SCROLL_BOTTOM_GAP_2; }
	inline void set_BG_SCROLL_BOTTOM_GAP_2(float value)
	{
		___BG_SCROLL_BOTTOM_GAP_2 = value;
	}

	inline static int32_t get_offset_of_MAX_DISTANCE_3() { return static_cast<int32_t>(offsetof(DataSetting_t2172603174, ___MAX_DISTANCE_3)); }
	inline float get_MAX_DISTANCE_3() const { return ___MAX_DISTANCE_3; }
	inline float* get_address_of_MAX_DISTANCE_3() { return &___MAX_DISTANCE_3; }
	inline void set_MAX_DISTANCE_3(float value)
	{
		___MAX_DISTANCE_3 = value;
	}

	inline static int32_t get_offset_of_MIN_DISTANCE_4() { return static_cast<int32_t>(offsetof(DataSetting_t2172603174, ___MIN_DISTANCE_4)); }
	inline float get_MIN_DISTANCE_4() const { return ___MIN_DISTANCE_4; }
	inline float* get_address_of_MIN_DISTANCE_4() { return &___MIN_DISTANCE_4; }
	inline void set_MIN_DISTANCE_4(float value)
	{
		___MIN_DISTANCE_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
