﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Console/Log>
struct Collection_1_t1969064310;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// Console/Log[]
struct LogU5BU5D_t3480530893;
// System.Collections.Generic.IEnumerator`1<Console/Log>
struct IEnumerator_1_t1483183028;
// System.Collections.Generic.IList`1<Console/Log>
struct IList_1_t2166568894;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"

// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::.ctor()
extern "C"  void Collection_1__ctor_m1872048655_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1__ctor_m1872048655(__this, method) ((  void (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1__ctor_m1872048655_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2511219724_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2511219724(__this, method) ((  bool (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2511219724_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m1103136277_gshared (Collection_1_t1969064310 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m1103136277(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1969064310 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1103136277_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m657567824_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m657567824(__this, method) ((  Il2CppObject * (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m657567824_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1450721441_gshared (Collection_1_t1969064310 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1450721441(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1969064310 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1450721441_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m682671563_gshared (Collection_1_t1969064310 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m682671563(__this, ___value0, method) ((  bool (*) (Collection_1_t1969064310 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m682671563_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2557820537_gshared (Collection_1_t1969064310 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2557820537(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1969064310 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2557820537_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1713784100_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1713784100(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1969064310 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1713784100_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3887061380_gshared (Collection_1_t1969064310 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3887061380(__this, ___value0, method) ((  void (*) (Collection_1_t1969064310 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3887061380_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m793362865_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m793362865(__this, method) ((  bool (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m793362865_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m1940460381_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m1940460381(__this, method) ((  Il2CppObject * (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1940460381_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2992726394_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2992726394(__this, method) ((  bool (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2992726394_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1243285055_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1243285055(__this, method) ((  bool (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1243285055_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1832231332_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1832231332(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1969064310 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1832231332_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2587998203_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2587998203(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1969064310 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2587998203_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::Add(T)
extern "C"  void Collection_1_Add_m1713098896_gshared (Collection_1_t1969064310 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1713098896(__this, ___item0, method) ((  void (*) (Collection_1_t1969064310 *, Log_t76580 , const MethodInfo*))Collection_1_Add_m1713098896_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::Clear()
extern "C"  void Collection_1_Clear_m3573149242_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_Clear_m3573149242(__this, method) ((  void (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_Clear_m3573149242_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::ClearItems()
extern "C"  void Collection_1_ClearItems_m980387592_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m980387592(__this, method) ((  void (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_ClearItems_m980387592_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::Contains(T)
extern "C"  bool Collection_1_Contains_m2551767144_gshared (Collection_1_t1969064310 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2551767144(__this, ___item0, method) ((  bool (*) (Collection_1_t1969064310 *, Log_t76580 , const MethodInfo*))Collection_1_Contains_m2551767144_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m566539392_gshared (Collection_1_t1969064310 * __this, LogU5BU5D_t3480530893* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m566539392(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1969064310 *, LogU5BU5D_t3480530893*, int32_t, const MethodInfo*))Collection_1_CopyTo_m566539392_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Console/Log>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2541798603_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2541798603(__this, method) ((  Il2CppObject* (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_GetEnumerator_m2541798603_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Console/Log>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m4249110020_gshared (Collection_1_t1969064310 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m4249110020(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1969064310 *, Log_t76580 , const MethodInfo*))Collection_1_IndexOf_m4249110020_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1912990455_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, Log_t76580  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1912990455(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1969064310 *, int32_t, Log_t76580 , const MethodInfo*))Collection_1_Insert_m1912990455_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m944817578_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, Log_t76580  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m944817578(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1969064310 *, int32_t, Log_t76580 , const MethodInfo*))Collection_1_InsertItem_m944817578_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::Remove(T)
extern "C"  bool Collection_1_Remove_m3814365795_gshared (Collection_1_t1969064310 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3814365795(__this, ___item0, method) ((  bool (*) (Collection_1_t1969064310 *, Log_t76580 , const MethodInfo*))Collection_1_Remove_m3814365795_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m4081810621_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m4081810621(__this, ___index0, method) ((  void (*) (Collection_1_t1969064310 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m4081810621_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m4214490525_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m4214490525(__this, ___index0, method) ((  void (*) (Collection_1_t1969064310 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m4214490525_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Console/Log>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m2690699255_gshared (Collection_1_t1969064310 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m2690699255(__this, method) ((  int32_t (*) (Collection_1_t1969064310 *, const MethodInfo*))Collection_1_get_Count_m2690699255_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Console/Log>::get_Item(System.Int32)
extern "C"  Log_t76580  Collection_1_get_Item_m1847826241_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1847826241(__this, ___index0, method) ((  Log_t76580  (*) (Collection_1_t1969064310 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1847826241_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1947421198_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, Log_t76580  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1947421198(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1969064310 *, int32_t, Log_t76580 , const MethodInfo*))Collection_1_set_Item_m1947421198_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m489751723_gshared (Collection_1_t1969064310 * __this, int32_t ___index0, Log_t76580  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m489751723(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1969064310 *, int32_t, Log_t76580 , const MethodInfo*))Collection_1_SetItem_m489751723_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m764261572_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m764261572(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m764261572_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Console/Log>::ConvertItem(System.Object)
extern "C"  Log_t76580  Collection_1_ConvertItem_m1263386016_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1263386016(__this /* static, unused */, ___item0, method) ((  Log_t76580  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1263386016_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Console/Log>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m66911808_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m66911808(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m66911808_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m2729587584_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m2729587584(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2729587584_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Console/Log>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3323249439_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3323249439(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3323249439_gshared)(__this /* static, unused */, ___list0, method)
