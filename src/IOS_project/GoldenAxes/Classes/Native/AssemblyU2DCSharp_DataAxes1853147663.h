﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<DataAxe>
struct List_1_t3904779229;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataAxes
struct  DataAxes_t1853147663  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<DataAxe> DataAxes::axes
	List_1_t3904779229 * ___axes_0;

public:
	inline static int32_t get_offset_of_axes_0() { return static_cast<int32_t>(offsetof(DataAxes_t1853147663, ___axes_0)); }
	inline List_1_t3904779229 * get_axes_0() const { return ___axes_0; }
	inline List_1_t3904779229 ** get_address_of_axes_0() { return &___axes_0; }
	inline void set_axes_0(List_1_t3904779229 * value)
	{
		___axes_0 = value;
		Il2CppCodeGenWriteBarrier(&___axes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
