﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataSetting
struct DataSetting_t2172603174;
// DataChapters
struct DataChapters_t2926209968;
// DataStages
struct DataStages_t3269946783;
// DataAxes
struct DataAxes_t1853147663;
// DataMonsters
struct DataMonsters_t1171984835;
// DataZones
struct DataZones_t1635830941;
// DataPatterns
struct DataPatterns_t2737562829;
// DataUser
struct DataUser_t1853738677;
// Data
struct Data_t2122699;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "AssemblyU2DCSharp_Data_PROGRESS4076515885.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Data
struct  Data_t2122699  : public MonoBehaviour_t3012272455
{
public:
	// Data/PROGRESS Data::m_load_index
	int32_t ___m_load_index_2;
	// Data/PROGRESS Data::m_load_max
	int32_t ___m_load_max_3;
	// DataSetting Data::m_setting
	DataSetting_t2172603174 * ___m_setting_4;
	// DataChapters Data::m_chapters
	DataChapters_t2926209968 * ___m_chapters_5;
	// DataStages Data::m_stages
	DataStages_t3269946783 * ___m_stages_6;
	// DataAxes Data::m_axes
	DataAxes_t1853147663 * ___m_axes_7;
	// DataMonsters Data::m_monsters
	DataMonsters_t1171984835 * ___m_monsters_8;
	// DataZones Data::m_zones
	DataZones_t1635830941 * ___m_zones_9;
	// DataPatterns Data::m_patterns
	DataPatterns_t2737562829 * ___m_patterns_10;
	// DataUser Data::m_user
	DataUser_t1853738677 * ___m_user_11;

public:
	inline static int32_t get_offset_of_m_load_index_2() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_load_index_2)); }
	inline int32_t get_m_load_index_2() const { return ___m_load_index_2; }
	inline int32_t* get_address_of_m_load_index_2() { return &___m_load_index_2; }
	inline void set_m_load_index_2(int32_t value)
	{
		___m_load_index_2 = value;
	}

	inline static int32_t get_offset_of_m_load_max_3() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_load_max_3)); }
	inline int32_t get_m_load_max_3() const { return ___m_load_max_3; }
	inline int32_t* get_address_of_m_load_max_3() { return &___m_load_max_3; }
	inline void set_m_load_max_3(int32_t value)
	{
		___m_load_max_3 = value;
	}

	inline static int32_t get_offset_of_m_setting_4() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_setting_4)); }
	inline DataSetting_t2172603174 * get_m_setting_4() const { return ___m_setting_4; }
	inline DataSetting_t2172603174 ** get_address_of_m_setting_4() { return &___m_setting_4; }
	inline void set_m_setting_4(DataSetting_t2172603174 * value)
	{
		___m_setting_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_setting_4, value);
	}

	inline static int32_t get_offset_of_m_chapters_5() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_chapters_5)); }
	inline DataChapters_t2926209968 * get_m_chapters_5() const { return ___m_chapters_5; }
	inline DataChapters_t2926209968 ** get_address_of_m_chapters_5() { return &___m_chapters_5; }
	inline void set_m_chapters_5(DataChapters_t2926209968 * value)
	{
		___m_chapters_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_chapters_5, value);
	}

	inline static int32_t get_offset_of_m_stages_6() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_stages_6)); }
	inline DataStages_t3269946783 * get_m_stages_6() const { return ___m_stages_6; }
	inline DataStages_t3269946783 ** get_address_of_m_stages_6() { return &___m_stages_6; }
	inline void set_m_stages_6(DataStages_t3269946783 * value)
	{
		___m_stages_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_stages_6, value);
	}

	inline static int32_t get_offset_of_m_axes_7() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_axes_7)); }
	inline DataAxes_t1853147663 * get_m_axes_7() const { return ___m_axes_7; }
	inline DataAxes_t1853147663 ** get_address_of_m_axes_7() { return &___m_axes_7; }
	inline void set_m_axes_7(DataAxes_t1853147663 * value)
	{
		___m_axes_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_axes_7, value);
	}

	inline static int32_t get_offset_of_m_monsters_8() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_monsters_8)); }
	inline DataMonsters_t1171984835 * get_m_monsters_8() const { return ___m_monsters_8; }
	inline DataMonsters_t1171984835 ** get_address_of_m_monsters_8() { return &___m_monsters_8; }
	inline void set_m_monsters_8(DataMonsters_t1171984835 * value)
	{
		___m_monsters_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_monsters_8, value);
	}

	inline static int32_t get_offset_of_m_zones_9() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_zones_9)); }
	inline DataZones_t1635830941 * get_m_zones_9() const { return ___m_zones_9; }
	inline DataZones_t1635830941 ** get_address_of_m_zones_9() { return &___m_zones_9; }
	inline void set_m_zones_9(DataZones_t1635830941 * value)
	{
		___m_zones_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_zones_9, value);
	}

	inline static int32_t get_offset_of_m_patterns_10() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_patterns_10)); }
	inline DataPatterns_t2737562829 * get_m_patterns_10() const { return ___m_patterns_10; }
	inline DataPatterns_t2737562829 ** get_address_of_m_patterns_10() { return &___m_patterns_10; }
	inline void set_m_patterns_10(DataPatterns_t2737562829 * value)
	{
		___m_patterns_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_patterns_10, value);
	}

	inline static int32_t get_offset_of_m_user_11() { return static_cast<int32_t>(offsetof(Data_t2122699, ___m_user_11)); }
	inline DataUser_t1853738677 * get_m_user_11() const { return ___m_user_11; }
	inline DataUser_t1853738677 ** get_address_of_m_user_11() { return &___m_user_11; }
	inline void set_m_user_11(DataUser_t1853738677 * value)
	{
		___m_user_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_user_11, value);
	}
};

struct Data_t2122699_StaticFields
{
public:
	// Data Data::instance
	Data_t2122699 * ___instance_12;

public:
	inline static int32_t get_offset_of_instance_12() { return static_cast<int32_t>(offsetof(Data_t2122699_StaticFields, ___instance_12)); }
	inline Data_t2122699 * get_instance_12() const { return ___instance_12; }
	inline Data_t2122699 ** get_address_of_instance_12() { return &___instance_12; }
	inline void set_instance_12(Data_t2122699 * value)
	{
		___instance_12 = value;
		Il2CppCodeGenWriteBarrier(&___instance_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
