﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<DataAxe>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1156586059(__this, ___l0, method) ((  void (*) (Enumerator_t1990562221 *, List_1_t3904779229 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataAxe>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3900995367(__this, method) ((  void (*) (Enumerator_t1990562221 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DataAxe>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m721320787(__this, method) ((  Il2CppObject * (*) (Enumerator_t1990562221 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataAxe>::Dispose()
#define Enumerator_Dispose_m1315154672(__this, method) ((  void (*) (Enumerator_t1990562221 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataAxe>::VerifyState()
#define Enumerator_VerifyState_m921203241(__this, method) ((  void (*) (Enumerator_t1990562221 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DataAxe>::MoveNext()
#define Enumerator_MoveNext_m3230144266(__this, method) ((  bool (*) (Enumerator_t1990562221 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DataAxe>::get_Current()
#define Enumerator_get_Current_m3574942278(__this, method) ((  DataAxe_t3107820260 * (*) (Enumerator_t1990562221 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
