﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator3<System.Single>
struct U3CGetEnumeratorU3Ec__Iterator3_t41752724;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m3804309706_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3__ctor_m3804309706(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t41752724 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3__ctor_m3804309706_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  float U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1047030993_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1047030993(__this, method) ((  float (*) (U3CGetEnumeratorU3Ec__Iterator3_t41752724 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1047030993_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1789359964_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1789359964(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t41752724 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1789359964_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m163521770_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m163521770(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator3_t41752724 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m163521770_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m822741639_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Dispose_m822741639(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t41752724 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Dispose_m822741639_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1450742647_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Reset_m1450742647(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t41752724 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Reset_m1450742647_gshared)(__this, method)
