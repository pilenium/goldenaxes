﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataStage
struct DataStage_t1629502804;

#include "codegen/il2cpp-codegen.h"

// System.Void DataStage::.ctor()
extern "C"  void DataStage__ctor_m134780567 (DataStage_t1629502804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
