﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<DataStage>
struct List_1_t2426461773;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataStageChapter
struct  DataStageChapter_t1740173401  : public Il2CppObject
{
public:
	// System.Int32 DataStageChapter::chapter
	int32_t ___chapter_0;
	// System.Collections.Generic.List`1<DataStage> DataStageChapter::stages
	List_1_t2426461773 * ___stages_1;

public:
	inline static int32_t get_offset_of_chapter_0() { return static_cast<int32_t>(offsetof(DataStageChapter_t1740173401, ___chapter_0)); }
	inline int32_t get_chapter_0() const { return ___chapter_0; }
	inline int32_t* get_address_of_chapter_0() { return &___chapter_0; }
	inline void set_chapter_0(int32_t value)
	{
		___chapter_0 = value;
	}

	inline static int32_t get_offset_of_stages_1() { return static_cast<int32_t>(offsetof(DataStageChapter_t1740173401, ___stages_1)); }
	inline List_1_t2426461773 * get_stages_1() const { return ___stages_1; }
	inline List_1_t2426461773 ** get_address_of_stages_1() { return &___stages_1; }
	inline void set_stages_1(List_1_t2426461773 * value)
	{
		___stages_1 = value;
		Il2CppCodeGenWriteBarrier(&___stages_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
