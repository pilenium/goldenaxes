﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Zone
struct Zone_t2791372;
// DataZone
struct DataZone_t1853884054;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DataZone1853884054.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void Zone::.ctor()
extern "C"  void Zone__ctor_m2295658575 (Zone_t2791372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zone::Awake()
extern "C"  void Zone_Awake_m2533263794 (Zone_t2791372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zone::Update()
extern "C"  void Zone_Update_m4172801182 (Zone_t2791372 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Zone::Init(DataZone)
extern "C"  void Zone_Init_m1862295631 (Zone_t2791372 * __this, DataZone_t1853884054 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Zone::IsHit(UnityEngine.Vector3)
extern "C"  bool Zone_IsHit_m458776983 (Zone_t2791372 * __this, Vector3_t3525329789  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
