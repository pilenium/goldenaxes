﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1755167990;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataNormalMode
struct  DataNormalMode_t1907108500  : public Il2CppObject
{
public:
	// System.Single DataNormalMode::speed
	float ___speed_0;
	// System.Single DataNormalMode::increase
	float ___increase_1;
	// System.Single DataNormalMode::decrease
	float ___decrease_2;
	// System.Single DataNormalMode::max
	float ___max_3;
	// System.Collections.Generic.List`1<System.String> DataNormalMode::type
	List_1_t1765447871 * ___type_4;
	// System.Int32 DataNormalMode::count_min
	int32_t ___count_min_5;
	// System.Int32 DataNormalMode::count_max
	int32_t ___count_max_6;
	// System.Collections.Generic.List`1<System.Single> DataNormalMode::delay
	List_1_t1755167990 * ___delay_7;

public:
	inline static int32_t get_offset_of_speed_0() { return static_cast<int32_t>(offsetof(DataNormalMode_t1907108500, ___speed_0)); }
	inline float get_speed_0() const { return ___speed_0; }
	inline float* get_address_of_speed_0() { return &___speed_0; }
	inline void set_speed_0(float value)
	{
		___speed_0 = value;
	}

	inline static int32_t get_offset_of_increase_1() { return static_cast<int32_t>(offsetof(DataNormalMode_t1907108500, ___increase_1)); }
	inline float get_increase_1() const { return ___increase_1; }
	inline float* get_address_of_increase_1() { return &___increase_1; }
	inline void set_increase_1(float value)
	{
		___increase_1 = value;
	}

	inline static int32_t get_offset_of_decrease_2() { return static_cast<int32_t>(offsetof(DataNormalMode_t1907108500, ___decrease_2)); }
	inline float get_decrease_2() const { return ___decrease_2; }
	inline float* get_address_of_decrease_2() { return &___decrease_2; }
	inline void set_decrease_2(float value)
	{
		___decrease_2 = value;
	}

	inline static int32_t get_offset_of_max_3() { return static_cast<int32_t>(offsetof(DataNormalMode_t1907108500, ___max_3)); }
	inline float get_max_3() const { return ___max_3; }
	inline float* get_address_of_max_3() { return &___max_3; }
	inline void set_max_3(float value)
	{
		___max_3 = value;
	}

	inline static int32_t get_offset_of_type_4() { return static_cast<int32_t>(offsetof(DataNormalMode_t1907108500, ___type_4)); }
	inline List_1_t1765447871 * get_type_4() const { return ___type_4; }
	inline List_1_t1765447871 ** get_address_of_type_4() { return &___type_4; }
	inline void set_type_4(List_1_t1765447871 * value)
	{
		___type_4 = value;
		Il2CppCodeGenWriteBarrier(&___type_4, value);
	}

	inline static int32_t get_offset_of_count_min_5() { return static_cast<int32_t>(offsetof(DataNormalMode_t1907108500, ___count_min_5)); }
	inline int32_t get_count_min_5() const { return ___count_min_5; }
	inline int32_t* get_address_of_count_min_5() { return &___count_min_5; }
	inline void set_count_min_5(int32_t value)
	{
		___count_min_5 = value;
	}

	inline static int32_t get_offset_of_count_max_6() { return static_cast<int32_t>(offsetof(DataNormalMode_t1907108500, ___count_max_6)); }
	inline int32_t get_count_max_6() const { return ___count_max_6; }
	inline int32_t* get_address_of_count_max_6() { return &___count_max_6; }
	inline void set_count_max_6(int32_t value)
	{
		___count_max_6 = value;
	}

	inline static int32_t get_offset_of_delay_7() { return static_cast<int32_t>(offsetof(DataNormalMode_t1907108500, ___delay_7)); }
	inline List_1_t1755167990 * get_delay_7() const { return ___delay_7; }
	inline List_1_t1755167990 ** get_address_of_delay_7() { return &___delay_7; }
	inline void set_delay_7(List_1_t1755167990 * value)
	{
		___delay_7 = value;
		Il2CppCodeGenWriteBarrier(&___delay_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
