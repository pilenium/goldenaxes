﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Log_t76580;
struct Log_t76580_marshaled_pinvoke;

extern "C" void Log_t76580_marshal_pinvoke(const Log_t76580& unmarshaled, Log_t76580_marshaled_pinvoke& marshaled);
extern "C" void Log_t76580_marshal_pinvoke_back(const Log_t76580_marshaled_pinvoke& marshaled, Log_t76580& unmarshaled);
extern "C" void Log_t76580_marshal_pinvoke_cleanup(Log_t76580_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Log_t76580;
struct Log_t76580_marshaled_com;

extern "C" void Log_t76580_marshal_com(const Log_t76580& unmarshaled, Log_t76580_marshaled_com& marshaled);
extern "C" void Log_t76580_marshal_com_back(const Log_t76580_marshaled_com& marshaled, Log_t76580& unmarshaled);
extern "C" void Log_t76580_marshal_com_cleanup(Log_t76580_marshaled_com& marshaled);
