﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "AssemblyU2DCSharp_Character3568163593.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Diver
struct  Diver_t66044126  : public Character_t3568163593
{
public:
	// UnityEngine.GameObject Diver::m_diver
	GameObject_t4012695102 * ___m_diver_5;
	// System.Single Diver::m_size
	float ___m_size_6;
	// System.Boolean Diver::m_isMonster
	bool ___m_isMonster_7;
	// System.UInt32 Diver::m_monster_id
	uint32_t ___m_monster_id_8;

public:
	inline static int32_t get_offset_of_m_diver_5() { return static_cast<int32_t>(offsetof(Diver_t66044126, ___m_diver_5)); }
	inline GameObject_t4012695102 * get_m_diver_5() const { return ___m_diver_5; }
	inline GameObject_t4012695102 ** get_address_of_m_diver_5() { return &___m_diver_5; }
	inline void set_m_diver_5(GameObject_t4012695102 * value)
	{
		___m_diver_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_diver_5, value);
	}

	inline static int32_t get_offset_of_m_size_6() { return static_cast<int32_t>(offsetof(Diver_t66044126, ___m_size_6)); }
	inline float get_m_size_6() const { return ___m_size_6; }
	inline float* get_address_of_m_size_6() { return &___m_size_6; }
	inline void set_m_size_6(float value)
	{
		___m_size_6 = value;
	}

	inline static int32_t get_offset_of_m_isMonster_7() { return static_cast<int32_t>(offsetof(Diver_t66044126, ___m_isMonster_7)); }
	inline bool get_m_isMonster_7() const { return ___m_isMonster_7; }
	inline bool* get_address_of_m_isMonster_7() { return &___m_isMonster_7; }
	inline void set_m_isMonster_7(bool value)
	{
		___m_isMonster_7 = value;
	}

	inline static int32_t get_offset_of_m_monster_id_8() { return static_cast<int32_t>(offsetof(Diver_t66044126, ___m_monster_id_8)); }
	inline uint32_t get_m_monster_id_8() const { return ___m_monster_id_8; }
	inline uint32_t* get_address_of_m_monster_id_8() { return &___m_monster_id_8; }
	inline void set_m_monster_id_8(uint32_t value)
	{
		___m_monster_id_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
