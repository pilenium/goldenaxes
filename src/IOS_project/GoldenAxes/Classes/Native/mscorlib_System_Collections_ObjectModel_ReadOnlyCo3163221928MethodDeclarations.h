﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>
struct ReadOnlyCollection_1_t3163221928;
// System.Collections.Generic.IList`1<Console/Log>
struct IList_1_t2166568894;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// Console/Log[]
struct LogU5BU5D_t3480530893;
// System.Collections.Generic.IEnumerator`1<Console/Log>
struct IEnumerator_1_t1483183028;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m1455448228_gshared (ReadOnlyCollection_1_t3163221928 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m1455448228(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1455448228_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1048289422_gshared (ReadOnlyCollection_1_t3163221928 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1048289422(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, Log_t76580 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m1048289422_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m143924732_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m143924732(__this, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m143924732_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3368765941_gshared (ReadOnlyCollection_1_t3163221928 * __this, int32_t ___index0, Log_t76580  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3368765941(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, int32_t, Log_t76580 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3368765941_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2655499813_gshared (ReadOnlyCollection_1_t3163221928 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2655499813(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3163221928 *, Log_t76580 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2655499813_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1242618811_gshared (ReadOnlyCollection_1_t3163221928 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1242618811(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1242618811_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Log_t76580  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2953843647_gshared (ReadOnlyCollection_1_t3163221928 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2953843647(__this, ___index0, method) ((  Log_t76580  (*) (ReadOnlyCollection_1_t3163221928 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m2953843647_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m788324748_gshared (ReadOnlyCollection_1_t3163221928 * __this, int32_t ___index0, Log_t76580  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m788324748(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, int32_t, Log_t76580 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m788324748_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2588066762_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2588066762(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2588066762_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17791315_gshared (ReadOnlyCollection_1_t3163221928 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17791315(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m17791315_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1091608846_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1091608846(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1091608846_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m3309854371_gshared (ReadOnlyCollection_1_t3163221928 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m3309854371(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3163221928 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m3309854371_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m1541313633_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m1541313633(__this, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m1541313633_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m4289612553_gshared (ReadOnlyCollection_1_t3163221928 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m4289612553(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3163221928 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m4289612553_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3324152699_gshared (ReadOnlyCollection_1_t3163221928 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3324152699(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3163221928 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3324152699_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m3162489638_gshared (ReadOnlyCollection_1_t3163221928 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m3162489638(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m3162489638_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m1920450626_gshared (ReadOnlyCollection_1_t3163221928 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m1920450626(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m1920450626_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m989782966_gshared (ReadOnlyCollection_1_t3163221928 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m989782966(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m989782966_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3680584627_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3680584627(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3680584627_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2198684127_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2198684127(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2198684127_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3303617848_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3303617848(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3303617848_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2777334465_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2777334465(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2777334465_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m877148070_gshared (ReadOnlyCollection_1_t3163221928 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m877148070(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3163221928 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m877148070_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m3224616317_gshared (ReadOnlyCollection_1_t3163221928 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m3224616317(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m3224616317_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m1384909930_gshared (ReadOnlyCollection_1_t3163221928 * __this, Log_t76580  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m1384909930(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3163221928 *, Log_t76580 , const MethodInfo*))ReadOnlyCollection_1_Contains_m1384909930_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1883651518_gshared (ReadOnlyCollection_1_t3163221928 * __this, LogU5BU5D_t3480530893* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1883651518(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3163221928 *, LogU5BU5D_t3480530893*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1883651518_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2220202445_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2220202445(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2220202445_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2369640386_gshared (ReadOnlyCollection_1_t3163221928 * __this, Log_t76580  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2369640386(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3163221928 *, Log_t76580 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2369640386_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m261715449_gshared (ReadOnlyCollection_1_t3163221928 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m261715449(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3163221928 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m261715449_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>::get_Item(System.Int32)
extern "C"  Log_t76580  ReadOnlyCollection_1_get_Item_m3961858303_gshared (ReadOnlyCollection_1_t3163221928 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m3961858303(__this, ___index0, method) ((  Log_t76580  (*) (ReadOnlyCollection_1_t3163221928 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m3961858303_gshared)(__this, ___index0, method)
