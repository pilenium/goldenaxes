﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.Renderer
struct Renderer_t1092684080;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectWave
struct  EffectWave_t3709365322  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform EffectWave::m_tr
	Transform_t284553113 * ___m_tr_2;
	// UnityEngine.Transform EffectWave::m_wave_tr
	Transform_t284553113 * ___m_wave_tr_3;
	// UnityEngine.Renderer EffectWave::m_renderer
	Renderer_t1092684080 * ___m_renderer_4;
	// UnityEngine.Color EffectWave::m_targetColor
	Color_t1588175760  ___m_targetColor_5;
	// UnityEngine.Vector3 EffectWave::m_targetScale
	Vector3_t3525329789  ___m_targetScale_6;
	// System.Single EffectWave::speed
	float ___speed_7;

public:
	inline static int32_t get_offset_of_m_tr_2() { return static_cast<int32_t>(offsetof(EffectWave_t3709365322, ___m_tr_2)); }
	inline Transform_t284553113 * get_m_tr_2() const { return ___m_tr_2; }
	inline Transform_t284553113 ** get_address_of_m_tr_2() { return &___m_tr_2; }
	inline void set_m_tr_2(Transform_t284553113 * value)
	{
		___m_tr_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_2, value);
	}

	inline static int32_t get_offset_of_m_wave_tr_3() { return static_cast<int32_t>(offsetof(EffectWave_t3709365322, ___m_wave_tr_3)); }
	inline Transform_t284553113 * get_m_wave_tr_3() const { return ___m_wave_tr_3; }
	inline Transform_t284553113 ** get_address_of_m_wave_tr_3() { return &___m_wave_tr_3; }
	inline void set_m_wave_tr_3(Transform_t284553113 * value)
	{
		___m_wave_tr_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_wave_tr_3, value);
	}

	inline static int32_t get_offset_of_m_renderer_4() { return static_cast<int32_t>(offsetof(EffectWave_t3709365322, ___m_renderer_4)); }
	inline Renderer_t1092684080 * get_m_renderer_4() const { return ___m_renderer_4; }
	inline Renderer_t1092684080 ** get_address_of_m_renderer_4() { return &___m_renderer_4; }
	inline void set_m_renderer_4(Renderer_t1092684080 * value)
	{
		___m_renderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_renderer_4, value);
	}

	inline static int32_t get_offset_of_m_targetColor_5() { return static_cast<int32_t>(offsetof(EffectWave_t3709365322, ___m_targetColor_5)); }
	inline Color_t1588175760  get_m_targetColor_5() const { return ___m_targetColor_5; }
	inline Color_t1588175760 * get_address_of_m_targetColor_5() { return &___m_targetColor_5; }
	inline void set_m_targetColor_5(Color_t1588175760  value)
	{
		___m_targetColor_5 = value;
	}

	inline static int32_t get_offset_of_m_targetScale_6() { return static_cast<int32_t>(offsetof(EffectWave_t3709365322, ___m_targetScale_6)); }
	inline Vector3_t3525329789  get_m_targetScale_6() const { return ___m_targetScale_6; }
	inline Vector3_t3525329789 * get_address_of_m_targetScale_6() { return &___m_targetScale_6; }
	inline void set_m_targetScale_6(Vector3_t3525329789  value)
	{
		___m_targetScale_6 = value;
	}

	inline static int32_t get_offset_of_speed_7() { return static_cast<int32_t>(offsetof(EffectWave_t3709365322, ___speed_7)); }
	inline float get_speed_7() const { return ___speed_7; }
	inline float* get_address_of_speed_7() { return &___speed_7; }
	inline void set_speed_7(float value)
	{
		___speed_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
