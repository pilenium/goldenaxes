﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_370248105.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m626179208_gshared (KeyValuePair_2_t370248105 * __this, int32_t ___key0, Color_t1588175760  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m626179208(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t370248105 *, int32_t, Color_t1588175760 , const MethodInfo*))KeyValuePair_2__ctor_m626179208_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m3902430688_gshared (KeyValuePair_2_t370248105 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m3902430688(__this, method) ((  int32_t (*) (KeyValuePair_2_t370248105 *, const MethodInfo*))KeyValuePair_2_get_Key_m3902430688_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m743106721_gshared (KeyValuePair_2_t370248105 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m743106721(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t370248105 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m743106721_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::get_Value()
extern "C"  Color_t1588175760  KeyValuePair_2_get_Value_m3171090436_gshared (KeyValuePair_2_t370248105 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m3171090436(__this, method) ((  Color_t1588175760  (*) (KeyValuePair_2_t370248105 *, const MethodInfo*))KeyValuePair_2_get_Value_m3171090436_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3117351969_gshared (KeyValuePair_2_t370248105 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3117351969(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t370248105 *, Color_t1588175760 , const MethodInfo*))KeyValuePair_2_set_Value_m3117351969_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3049619015_gshared (KeyValuePair_2_t370248105 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3049619015(__this, method) ((  String_t* (*) (KeyValuePair_2_t370248105 *, const MethodInfo*))KeyValuePair_2_ToString_m3049619015_gshared)(__this, method)
