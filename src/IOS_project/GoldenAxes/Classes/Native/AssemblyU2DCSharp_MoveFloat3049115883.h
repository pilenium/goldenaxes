﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Move2404337.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveFloat
struct  MoveFloat_t3049115883  : public Move_t2404337
{
public:
	// System.Single MoveFloat::m_size
	float ___m_size_2;
	// System.Single MoveFloat::m_end_z
	float ___m_end_z_3;
	// UnityEngine.Vector3 MoveFloat::m_position
	Vector3_t3525329789  ___m_position_4;

public:
	inline static int32_t get_offset_of_m_size_2() { return static_cast<int32_t>(offsetof(MoveFloat_t3049115883, ___m_size_2)); }
	inline float get_m_size_2() const { return ___m_size_2; }
	inline float* get_address_of_m_size_2() { return &___m_size_2; }
	inline void set_m_size_2(float value)
	{
		___m_size_2 = value;
	}

	inline static int32_t get_offset_of_m_end_z_3() { return static_cast<int32_t>(offsetof(MoveFloat_t3049115883, ___m_end_z_3)); }
	inline float get_m_end_z_3() const { return ___m_end_z_3; }
	inline float* get_address_of_m_end_z_3() { return &___m_end_z_3; }
	inline void set_m_end_z_3(float value)
	{
		___m_end_z_3 = value;
	}

	inline static int32_t get_offset_of_m_position_4() { return static_cast<int32_t>(offsetof(MoveFloat_t3049115883, ___m_position_4)); }
	inline Vector3_t3525329789  get_m_position_4() const { return ___m_position_4; }
	inline Vector3_t3525329789 * get_address_of_m_position_4() { return &___m_position_4; }
	inline void set_m_position_4(Vector3_t3525329789  value)
	{
		___m_position_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
