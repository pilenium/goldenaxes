﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StageWaterSuface
struct StageWaterSuface_t3854378488;
// DataChapter
struct DataChapter_t925677859;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DataChapter925677859.h"

// System.Void StageWaterSuface::.ctor()
extern "C"  void StageWaterSuface__ctor_m1807169443 (StageWaterSuface_t3854378488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageWaterSuface::Awake()
extern "C"  void StageWaterSuface_Awake_m2044774662 (StageWaterSuface_t3854378488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageWaterSuface::Init()
extern "C"  void StageWaterSuface_Init_m3889157713 (StageWaterSuface_t3854378488 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StageWaterSuface::SetChapter(DataChapter)
extern "C"  void StageWaterSuface_SetChapter_m993532835 (StageWaterSuface_t3854378488 * __this, DataChapter_t925677859 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
