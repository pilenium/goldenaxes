﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectCoin
struct  EffectCoin_t3708782562  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Vector3 EffectCoin::m_velocity
	Vector3_t3525329789  ___m_velocity_2;
	// System.Single EffectCoin::m_gravity
	float ___m_gravity_3;
	// UnityEngine.Transform EffectCoin::m_tr
	Transform_t284553113 * ___m_tr_4;
	// System.Int32 EffectCoin::m_coin
	int32_t ___m_coin_5;
	// System.Single EffectCoin::m_time
	float ___m_time_6;

public:
	inline static int32_t get_offset_of_m_velocity_2() { return static_cast<int32_t>(offsetof(EffectCoin_t3708782562, ___m_velocity_2)); }
	inline Vector3_t3525329789  get_m_velocity_2() const { return ___m_velocity_2; }
	inline Vector3_t3525329789 * get_address_of_m_velocity_2() { return &___m_velocity_2; }
	inline void set_m_velocity_2(Vector3_t3525329789  value)
	{
		___m_velocity_2 = value;
	}

	inline static int32_t get_offset_of_m_gravity_3() { return static_cast<int32_t>(offsetof(EffectCoin_t3708782562, ___m_gravity_3)); }
	inline float get_m_gravity_3() const { return ___m_gravity_3; }
	inline float* get_address_of_m_gravity_3() { return &___m_gravity_3; }
	inline void set_m_gravity_3(float value)
	{
		___m_gravity_3 = value;
	}

	inline static int32_t get_offset_of_m_tr_4() { return static_cast<int32_t>(offsetof(EffectCoin_t3708782562, ___m_tr_4)); }
	inline Transform_t284553113 * get_m_tr_4() const { return ___m_tr_4; }
	inline Transform_t284553113 ** get_address_of_m_tr_4() { return &___m_tr_4; }
	inline void set_m_tr_4(Transform_t284553113 * value)
	{
		___m_tr_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_4, value);
	}

	inline static int32_t get_offset_of_m_coin_5() { return static_cast<int32_t>(offsetof(EffectCoin_t3708782562, ___m_coin_5)); }
	inline int32_t get_m_coin_5() const { return ___m_coin_5; }
	inline int32_t* get_address_of_m_coin_5() { return &___m_coin_5; }
	inline void set_m_coin_5(int32_t value)
	{
		___m_coin_5 = value;
	}

	inline static int32_t get_offset_of_m_time_6() { return static_cast<int32_t>(offsetof(EffectCoin_t3708782562, ___m_time_6)); }
	inline float get_m_time_6() const { return ___m_time_6; }
	inline float* get_address_of_m_time_6() { return &___m_time_6; }
	inline void set_m_time_6(float value)
	{
		___m_time_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
