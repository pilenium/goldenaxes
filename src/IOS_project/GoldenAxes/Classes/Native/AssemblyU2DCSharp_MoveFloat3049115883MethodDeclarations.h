﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveFloat
struct MoveFloat_t3049115883;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void MoveFloat::.ctor(System.Single,UnityEngine.Vector3)
extern "C"  void MoveFloat__ctor_m389997278 (MoveFloat_t3049115883 * __this, float ___size0, Vector3_t3525329789  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveFloat::Start()
extern "C"  void MoveFloat_Start_m2559620576 (MoveFloat_t3049115883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveFloat::Update()
extern "C"  void MoveFloat_Update_m2044678701 (MoveFloat_t3049115883 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
