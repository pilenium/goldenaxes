﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Console/Log>
struct List_1_t797035549;
// System.Collections.Generic.IEnumerator`1<Console/Log>
struct IEnumerator_1_t1483183028;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Console/Log>
struct ICollection_1_t465907966;
// System.Collections.Generic.IEnumerable`1<Console/Log>
struct IEnumerable_1_t2872230936;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Console/Log>
struct ReadOnlyCollection_1_t3163221928;
// Console/Log[]
struct LogU5BU5D_t3480530893;
// System.Predicate`1<Console/Log>
struct Predicate_1_t571040478;
// System.Comparison`1<Console/Log>
struct Comparison_1_t2703751456;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3177785837.h"

// System.Void System.Collections.Generic.List`1<Console/Log>::.ctor()
extern "C"  void List_1__ctor_m1263066603_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1__ctor_m1263066603(__this, method) ((  void (*) (List_1_t797035549 *, const MethodInfo*))List_1__ctor_m1263066603_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3010805395_gshared (List_1_t797035549 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3010805395(__this, ___capacity0, method) ((  void (*) (List_1_t797035549 *, int32_t, const MethodInfo*))List_1__ctor_m3010805395_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::.cctor()
extern "C"  void List_1__cctor_m2037741483_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2037741483(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2037741483_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Console/Log>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2664213012_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2664213012(__this, method) ((  Il2CppObject* (*) (List_1_t797035549 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2664213012_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m1164097346_gshared (List_1_t797035549 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m1164097346(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t797035549 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m1164097346_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Console/Log>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m2001790525_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m2001790525(__this, method) ((  Il2CppObject * (*) (List_1_t797035549 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m2001790525_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Console/Log>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m684178004_gshared (List_1_t797035549 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m684178004(__this, ___item0, method) ((  int32_t (*) (List_1_t797035549 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m684178004_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Console/Log>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m988824440_gshared (List_1_t797035549 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m988824440(__this, ___item0, method) ((  bool (*) (List_1_t797035549 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m988824440_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Console/Log>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1580902060_gshared (List_1_t797035549 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1580902060(__this, ___item0, method) ((  int32_t (*) (List_1_t797035549 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1580902060_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3425389527_gshared (List_1_t797035549 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3425389527(__this, ___index0, ___item1, method) ((  void (*) (List_1_t797035549 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3425389527_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m2393304049_gshared (List_1_t797035549 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m2393304049(__this, ___item0, method) ((  void (*) (List_1_t797035549 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m2393304049_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Console/Log>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2493005177_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2493005177(__this, method) ((  bool (*) (List_1_t797035549 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2493005177_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Console/Log>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3178152164_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3178152164(__this, method) ((  bool (*) (List_1_t797035549 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3178152164_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Console/Log>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1500469520_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1500469520(__this, method) ((  Il2CppObject * (*) (List_1_t797035549 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1500469520_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Console/Log>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3043880167_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3043880167(__this, method) ((  bool (*) (List_1_t797035549 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3043880167_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Console/Log>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2630408498_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2630408498(__this, method) ((  bool (*) (List_1_t797035549 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2630408498_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Console/Log>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m4202776215_gshared (List_1_t797035549 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m4202776215(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t797035549 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m4202776215_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m2468339182_gshared (List_1_t797035549 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m2468339182(__this, ___index0, ___value1, method) ((  void (*) (List_1_t797035549 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m2468339182_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::Add(T)
extern "C"  void List_1_Add_m2870973437_gshared (List_1_t797035549 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define List_1_Add_m2870973437(__this, ___item0, method) ((  void (*) (List_1_t797035549 *, Log_t76580 , const MethodInfo*))List_1_Add_m2870973437_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3648381368_gshared (List_1_t797035549 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3648381368(__this, ___newCount0, method) ((  void (*) (List_1_t797035549 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3648381368_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m28399798_gshared (List_1_t797035549 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m28399798(__this, ___collection0, method) ((  void (*) (List_1_t797035549 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m28399798_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3584339318_gshared (List_1_t797035549 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3584339318(__this, ___enumerable0, method) ((  void (*) (List_1_t797035549 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3584339318_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3593940833_gshared (List_1_t797035549 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3593940833(__this, ___collection0, method) ((  void (*) (List_1_t797035549 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3593940833_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Console/Log>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3163221928 * List_1_AsReadOnly_m3937599016_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m3937599016(__this, method) ((  ReadOnlyCollection_1_t3163221928 * (*) (List_1_t797035549 *, const MethodInfo*))List_1_AsReadOnly_m3937599016_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::Clear()
extern "C"  void List_1_Clear_m812554349_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_Clear_m812554349(__this, method) ((  void (*) (List_1_t797035549 *, const MethodInfo*))List_1_Clear_m812554349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Console/Log>::Contains(T)
extern "C"  bool List_1_Contains_m3118157467_gshared (List_1_t797035549 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define List_1_Contains_m3118157467(__this, ___item0, method) ((  bool (*) (List_1_t797035549 *, Log_t76580 , const MethodInfo*))List_1_Contains_m3118157467_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2785629869_gshared (List_1_t797035549 * __this, LogU5BU5D_t3480530893* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2785629869(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t797035549 *, LogU5BU5D_t3480530893*, int32_t, const MethodInfo*))List_1_CopyTo_m2785629869_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Console/Log>::Find(System.Predicate`1<T>)
extern "C"  Log_t76580  List_1_Find_m2351512155_gshared (List_1_t797035549 * __this, Predicate_1_t571040478 * ___match0, const MethodInfo* method);
#define List_1_Find_m2351512155(__this, ___match0, method) ((  Log_t76580  (*) (List_1_t797035549 *, Predicate_1_t571040478 *, const MethodInfo*))List_1_Find_m2351512155_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1140521686_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t571040478 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1140521686(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t571040478 *, const MethodInfo*))List_1_CheckMatch_m1140521686_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Console/Log>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2443096315_gshared (List_1_t797035549 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t571040478 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2443096315(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t797035549 *, int32_t, int32_t, Predicate_1_t571040478 *, const MethodInfo*))List_1_GetIndex_m2443096315_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Console/Log>::GetEnumerator()
extern "C"  Enumerator_t3177785837  List_1_GetEnumerator_m2217311832_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2217311832(__this, method) ((  Enumerator_t3177785837  (*) (List_1_t797035549 *, const MethodInfo*))List_1_GetEnumerator_m2217311832_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Console/Log>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m167263729_gshared (List_1_t797035549 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m167263729(__this, ___item0, method) ((  int32_t (*) (List_1_t797035549 *, Log_t76580 , const MethodInfo*))List_1_IndexOf_m167263729_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m686436356_gshared (List_1_t797035549 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m686436356(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t797035549 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m686436356_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2531997053_gshared (List_1_t797035549 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2531997053(__this, ___index0, method) ((  void (*) (List_1_t797035549 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2531997053_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m225916004_gshared (List_1_t797035549 * __this, int32_t ___index0, Log_t76580  ___item1, const MethodInfo* method);
#define List_1_Insert_m225916004(__this, ___index0, ___item1, method) ((  void (*) (List_1_t797035549 *, int32_t, Log_t76580 , const MethodInfo*))List_1_Insert_m225916004_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2049070553_gshared (List_1_t797035549 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2049070553(__this, ___collection0, method) ((  void (*) (List_1_t797035549 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2049070553_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Console/Log>::Remove(T)
extern "C"  bool List_1_Remove_m2201549142_gshared (List_1_t797035549 * __this, Log_t76580  ___item0, const MethodInfo* method);
#define List_1_Remove_m2201549142(__this, ___item0, method) ((  bool (*) (List_1_t797035549 *, Log_t76580 , const MethodInfo*))List_1_Remove_m2201549142_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Console/Log>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2806549236_gshared (List_1_t797035549 * __this, Predicate_1_t571040478 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2806549236(__this, ___match0, method) ((  int32_t (*) (List_1_t797035549 *, Predicate_1_t571040478 *, const MethodInfo*))List_1_RemoveAll_m2806549236_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2394736170_gshared (List_1_t797035549 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2394736170(__this, ___index0, method) ((  void (*) (List_1_t797035549 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2394736170_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::Reverse()
extern "C"  void List_1_Reverse_m843368450_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_Reverse_m843368450(__this, method) ((  void (*) (List_1_t797035549 *, const MethodInfo*))List_1_Reverse_m843368450_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::Sort()
extern "C"  void List_1_Sort_m3396945632_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_Sort_m3396945632(__this, method) ((  void (*) (List_1_t797035549 *, const MethodInfo*))List_1_Sort_m3396945632_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m3733777203_gshared (List_1_t797035549 * __this, Comparison_1_t2703751456 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m3733777203(__this, ___comparison0, method) ((  void (*) (List_1_t797035549 *, Comparison_1_t2703751456 *, const MethodInfo*))List_1_Sort_m3733777203_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Console/Log>::ToArray()
extern "C"  LogU5BU5D_t3480530893* List_1_ToArray_m466918849_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_ToArray_m466918849(__this, method) ((  LogU5BU5D_t3480530893* (*) (List_1_t797035549 *, const MethodInfo*))List_1_ToArray_m466918849_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1905245817_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1905245817(__this, method) ((  void (*) (List_1_t797035549 *, const MethodInfo*))List_1_TrimExcess_m1905245817_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Console/Log>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2532693409_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2532693409(__this, method) ((  int32_t (*) (List_1_t797035549 *, const MethodInfo*))List_1_get_Capacity_m2532693409_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m858570186_gshared (List_1_t797035549 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m858570186(__this, ___value0, method) ((  void (*) (List_1_t797035549 *, int32_t, const MethodInfo*))List_1_set_Capacity_m858570186_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Console/Log>::get_Count()
extern "C"  int32_t List_1_get_Count_m707515818_gshared (List_1_t797035549 * __this, const MethodInfo* method);
#define List_1_get_Count_m707515818(__this, method) ((  int32_t (*) (List_1_t797035549 *, const MethodInfo*))List_1_get_Count_m707515818_gshared)(__this, method)
// T System.Collections.Generic.List`1<Console/Log>::get_Item(System.Int32)
extern "C"  Log_t76580  List_1_get_Item_m38204270_gshared (List_1_t797035549 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m38204270(__this, ___index0, method) ((  Log_t76580  (*) (List_1_t797035549 *, int32_t, const MethodInfo*))List_1_get_Item_m38204270_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Console/Log>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4166511675_gshared (List_1_t797035549 * __this, int32_t ___index0, Log_t76580  ___value1, const MethodInfo* method);
#define List_1_set_Item_m4166511675(__this, ___index0, ___value1, method) ((  void (*) (List_1_t797035549 *, int32_t, Log_t76580 , const MethodInfo*))List_1_set_Item_m4166511675_gshared)(__this, ___index0, ___value1, method)
