﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Console/Log>
struct DefaultComparer_t3632997607;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Console/Log>::.ctor()
extern "C"  void DefaultComparer__ctor_m2090828554_gshared (DefaultComparer_t3632997607 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2090828554(__this, method) ((  void (*) (DefaultComparer_t3632997607 *, const MethodInfo*))DefaultComparer__ctor_m2090828554_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Console/Log>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m1875125601_gshared (DefaultComparer_t3632997607 * __this, Log_t76580  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m1875125601(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3632997607 *, Log_t76580 , const MethodInfo*))DefaultComparer_GetHashCode_m1875125601_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Console/Log>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1081405659_gshared (DefaultComparer_t3632997607 * __this, Log_t76580  ___x0, Log_t76580  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1081405659(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3632997607 *, Log_t76580 , Log_t76580 , const MethodInfo*))DefaultComparer_Equals_m1081405659_gshared)(__this, ___x0, ___y1, method)
