﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataObject
struct  DataObject_t3139072937  : public Il2CppObject
{
public:
	// System.Boolean DataObject::dive
	bool ___dive_0;
	// System.String DataObject::type
	String_t* ___type_1;
	// System.UInt32 DataObject::id
	uint32_t ___id_2;
	// System.Single DataObject::size
	float ___size_3;
	// System.String DataObject::direction
	String_t* ___direction_4;
	// System.Single DataObject::time
	float ___time_5;

public:
	inline static int32_t get_offset_of_dive_0() { return static_cast<int32_t>(offsetof(DataObject_t3139072937, ___dive_0)); }
	inline bool get_dive_0() const { return ___dive_0; }
	inline bool* get_address_of_dive_0() { return &___dive_0; }
	inline void set_dive_0(bool value)
	{
		___dive_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(DataObject_t3139072937, ___type_1)); }
	inline String_t* get_type_1() const { return ___type_1; }
	inline String_t** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(String_t* value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(DataObject_t3139072937, ___id_2)); }
	inline uint32_t get_id_2() const { return ___id_2; }
	inline uint32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(uint32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(DataObject_t3139072937, ___size_3)); }
	inline float get_size_3() const { return ___size_3; }
	inline float* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(float value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_direction_4() { return static_cast<int32_t>(offsetof(DataObject_t3139072937, ___direction_4)); }
	inline String_t* get_direction_4() const { return ___direction_4; }
	inline String_t** get_address_of_direction_4() { return &___direction_4; }
	inline void set_direction_4(String_t* value)
	{
		___direction_4 = value;
		Il2CppCodeGenWriteBarrier(&___direction_4, value);
	}

	inline static int32_t get_offset_of_time_5() { return static_cast<int32_t>(offsetof(DataObject_t3139072937, ___time_5)); }
	inline float get_time_5() const { return ___time_5; }
	inline float* get_address_of_time_5() { return &___time_5; }
	inline void set_time_5(float value)
	{
		___time_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
