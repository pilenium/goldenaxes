﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// InitShader
struct InitShader_t2240846133;

#include "codegen/il2cpp-codegen.h"

// System.Void InitShader::.ctor()
extern "C"  void InitShader__ctor_m3490417414 (InitShader_t2240846133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitShader::Awake()
extern "C"  void InitShader_Awake_m3728022633 (InitShader_t2240846133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void InitShader::Update()
extern "C"  void InitShader_Update_m2555619527 (InitShader_t2240846133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
