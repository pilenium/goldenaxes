﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InitShader
struct  InitShader_t2240846133  : public MonoBehaviour_t3012272455
{
public:
	// System.String InitShader::shader_name
	String_t* ___shader_name_2;

public:
	inline static int32_t get_offset_of_shader_name_2() { return static_cast<int32_t>(offsetof(InitShader_t2240846133, ___shader_name_2)); }
	inline String_t* get_shader_name_2() const { return ___shader_name_2; }
	inline String_t** get_address_of_shader_name_2() { return &___shader_name_2; }
	inline void set_shader_name_2(String_t* value)
	{
		___shader_name_2 = value;
		Il2CppCodeGenWriteBarrier(&___shader_name_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
