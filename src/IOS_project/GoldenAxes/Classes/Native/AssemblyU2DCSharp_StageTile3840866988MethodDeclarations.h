﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StageTile
struct StageTile_t3840866988;

#include "codegen/il2cpp-codegen.h"

// System.Void StageTile::.ctor()
extern "C"  void StageTile__ctor_m498949183 (StageTile_t3840866988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
