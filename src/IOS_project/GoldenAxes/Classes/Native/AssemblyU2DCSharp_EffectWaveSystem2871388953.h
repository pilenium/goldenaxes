﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectWaveSystem
struct  EffectWaveSystem_t2871388953  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform EffectWaveSystem::m_tr
	Transform_t284553113 * ___m_tr_2;
	// System.Single EffectWaveSystem::m_time
	float ___m_time_3;
	// System.Single EffectWaveSystem::m_size
	float ___m_size_4;
	// System.Boolean EffectWaveSystem::m_enabled
	bool ___m_enabled_5;

public:
	inline static int32_t get_offset_of_m_tr_2() { return static_cast<int32_t>(offsetof(EffectWaveSystem_t2871388953, ___m_tr_2)); }
	inline Transform_t284553113 * get_m_tr_2() const { return ___m_tr_2; }
	inline Transform_t284553113 ** get_address_of_m_tr_2() { return &___m_tr_2; }
	inline void set_m_tr_2(Transform_t284553113 * value)
	{
		___m_tr_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_2, value);
	}

	inline static int32_t get_offset_of_m_time_3() { return static_cast<int32_t>(offsetof(EffectWaveSystem_t2871388953, ___m_time_3)); }
	inline float get_m_time_3() const { return ___m_time_3; }
	inline float* get_address_of_m_time_3() { return &___m_time_3; }
	inline void set_m_time_3(float value)
	{
		___m_time_3 = value;
	}

	inline static int32_t get_offset_of_m_size_4() { return static_cast<int32_t>(offsetof(EffectWaveSystem_t2871388953, ___m_size_4)); }
	inline float get_m_size_4() const { return ___m_size_4; }
	inline float* get_address_of_m_size_4() { return &___m_size_4; }
	inline void set_m_size_4(float value)
	{
		___m_size_4 = value;
	}

	inline static int32_t get_offset_of_m_enabled_5() { return static_cast<int32_t>(offsetof(EffectWaveSystem_t2871388953, ___m_enabled_5)); }
	inline bool get_m_enabled_5() const { return ___m_enabled_5; }
	inline bool* get_address_of_m_enabled_5() { return &___m_enabled_5; }
	inline void set_m_enabled_5(bool value)
	{
		___m_enabled_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
