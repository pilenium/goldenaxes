﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameTouch
struct GameTouch_t2994825805;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void GameTouch::.ctor()
extern "C"  void GameTouch__ctor_m4003983806 (GameTouch_t2994825805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameTouch::Awake()
extern "C"  void GameTouch_Awake_m4241589025 (GameTouch_t2994825805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameTouch::OnDestroy()
extern "C"  void GameTouch_OnDestroy_m563001271 (GameTouch_t2994825805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameTouch::RemoveControl()
extern "C"  void GameTouch_RemoveControl_m3656731093 (GameTouch_t2994825805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameTouch::SetControl(System.UInt32)
extern "C"  void GameTouch_SetControl_m2085859001 (GameTouch_t2994825805 * __this, uint32_t ___axe_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameTouch::Update()
extern "C"  void GameTouch_Update_m1296308495 (GameTouch_t2994825805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameTouch::OnStartTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void GameTouch_OnStartTouch_m911723323 (GameTouch_t2994825805 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameTouch::OnMovedTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void GameTouch_OnMovedTouch_m3475860042 (GameTouch_t2994825805 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameTouch::OnEndTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void GameTouch_OnEndTouch_m3751389218 (GameTouch_t2994825805 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameTouch::OnCanceldTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void GameTouch_OnCanceldTouch_m1121489011 (GameTouch_t2994825805 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
