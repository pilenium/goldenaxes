﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mode
struct Mode_t2403781;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mode_TYPE2590522.h"

// System.Void Mode::.ctor(Mode/TYPE)
extern "C"  void Mode__ctor_m949431732 (Mode_t2403781 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mode Mode::Alloc(Mode/TYPE)
extern "C"  Mode_t2403781 * Mode_Alloc_m965895771 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mode/TYPE Mode::get_type()
extern "C"  int32_t Mode_get_type_m1292918588 (Mode_t2403781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mode::Init()
extern "C"  void Mode_Init_m2113765148 (Mode_t2403781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mode::Clear()
extern "C"  void Mode_Clear_m10708067 (Mode_t2403781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mode::Update(System.Single)
extern "C"  void Mode_Update_m3347001846 (Mode_t2403781 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mode::HitWater()
extern "C"  void Mode_HitWater_m993912016 (Mode_t2403781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mode::HitDiver()
extern "C"  void Mode_HitDiver_m1542091511 (Mode_t2403781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mode::HitMonster()
extern "C"  void Mode_HitMonster_m565413075 (Mode_t2403781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mode::KillMonster()
extern "C"  void Mode_KillMonster_m2070561074 (Mode_t2403781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
