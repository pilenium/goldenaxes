﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// UnityEngine.Component
struct Component_t2126946602;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t3547103726;
// System.String
struct String_t;
// System.Predicate`1<System.Object>
struct Predicate_1_t1408070318;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3019176036;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t3123668047;
// UnityEngine.Object[]
struct ObjectU5BU5D_t3051965477;
// System.Converter`2<System.Object,System.Object>
struct Converter_2_t113996300;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<System.Object>
struct EventFunction_1_t3885370180;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1738289281.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_SByte2855346064.h"
#include "System_System_Security_Cryptography_X509Certificat1122151684.h"
#include "mscorlib_System_Single958209021.h"
#include "System_System_Text_RegularExpressions_Mark3725932776.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_UInt32985925326.h"
#include "mscorlib_System_UInt64985925421.h"
#include "System_System_Uri_UriScheme3266528785.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry1095831350.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry2194697103.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_Color324137084207.h"
#include "UnityEngine_UnityEngine_ContactPoint2951122365.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3963746319.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResu959898689.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"
#include "UnityEngine_UnityEngine_Keyframe2095052507.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4082783401.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo2591228609.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1317012096.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2223678307.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3429487928.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1278737203.h"
#include "UnityEngine_UnityEngine_UICharInfo403820581.h"
#include "UnityEngine_UnityEngine_UILineInfo156921283.h"
#include "UnityEngine_UnityEngine_UIVertex2260061605.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUITools237277134MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITweener105489188MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe2095052507MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3342907448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITweener_Style80227729.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_AnimationCurve3342907448.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "mscorlib_System_IntPtr676692020MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CastHelper_1_gen4244616972.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve4196265289.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD3547103726.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "UnityEngine_UnityEngine_JsonUtility1789555601.h"
#include "UnityEngine_UnityEngine_JsonUtility1789555601MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Resources1543782994.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScriptableObject184905905.h"
#include "UnityEngine_UnityEngine_ScriptableObject184905905MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown2916437562.h"
#include "AssemblyU2DCSharp_NGUITools237277134.h"
#include "mscorlib_System_Predicate_1_gen1408070318.h"
#include "mscorlib_System_ArgumentNullException3214793280MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3214793280.h"
#include "mscorlib_System_Predicate_1_gen1408070318MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeData2584644259.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu318735129.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgu560415562.h"
#include "mscorlib_System_Converter_2_gen113996300.h"
#include "mscorlib_System_Converter_2_gen113996300MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve3885370180.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEve4196265289MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1081512082.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1081512082MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"

// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397(__this, ___index0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTypeTag_t1738289281_m1311091868_gshared (Il2CppArray * __this, int32_t p0, uint8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTypeTag_t1738289281_m1311091868(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTypeTag_t1738289281_m1311091868_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230(__this, ___index0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.SByte>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSByte_t2855346064_m1632182611_gshared (Il2CppArray * __this, int32_t p0, int8_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSByte_t2855346064_m1632182611(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int8_t*, const MethodInfo*))Array_GetGenericValueImpl_TisSByte_t2855346064_m1632182611_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t1122151684  Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218(__this, ___index0, method) ((  X509ChainStatus_t1122151684  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisX509ChainStatus_t1122151684_m1175622045_gshared (Il2CppArray * __this, int32_t p0, X509ChainStatus_t1122151684 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisX509ChainStatus_t1122151684_m1175622045(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, X509ChainStatus_t1122151684 *, const MethodInfo*))Array_GetGenericValueImpl_TisX509ChainStatus_t1122151684_m1175622045_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775(__this, ___index0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Single>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisSingle_t958209021_m1387272912_gshared (Il2CppArray * __this, int32_t p0, float* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisSingle_t958209021_m1387272912(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, float*, const MethodInfo*))Array_GetGenericValueImpl_TisSingle_t958209021_m1387272912_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t3725932776  Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t3725932776_m160824484(__this, ___index0, method) ((  Mark_t3725932776  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Text.RegularExpressions.Mark>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisMark_t3725932776_m2494808835_gshared (Il2CppArray * __this, int32_t p0, Mark_t3725932776 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisMark_t3725932776_m2494808835(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Mark_t3725932776 *, const MethodInfo*))Array_GetGenericValueImpl_TisMark_t3725932776_m2494808835_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t763862892  Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760(__this, ___index0, method) ((  TimeSpan_t763862892  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.TimeSpan>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTimeSpan_t763862892_m2245336767_gshared (Il2CppArray * __this, int32_t p0, TimeSpan_t763862892 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTimeSpan_t763862892_m2245336767(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, TimeSpan_t763862892 *, const MethodInfo*))Array_GetGenericValueImpl_TisTimeSpan_t763862892_m2245336767_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256(__this, ___index0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt16>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt16_t985925268_m1140249575_gshared (Il2CppArray * __this, int32_t p0, uint16_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt16_t985925268_m1140249575(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint16_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt16_t985925268_m1140249575_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062(__this, ___index0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt32_t985925326_m513700641_gshared (Il2CppArray * __this, int32_t p0, uint32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt32_t985925326_m513700641(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt32_t985925326_m513700641_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503(__this, ___index0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.UInt64>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUInt64_t985925421_m3412168192_gshared (Il2CppArray * __this, int32_t p0, uint64_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUInt64_t985925421_m3412168192(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, uint64_t*, const MethodInfo*))Array_GetGenericValueImpl_TisUInt64_t985925421_m3412168192_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t3266528785  Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123(__this, ___index0, method) ((  UriScheme_t3266528785  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<System.Uri/UriScheme>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUriScheme_t3266528785_m4227749606_gshared (Il2CppArray * __this, int32_t p0, UriScheme_t3266528785 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUriScheme_t3266528785_m4227749606(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UriScheme_t3266528785 *, const MethodInfo*))Array_GetGenericValueImpl_TisUriScheme_t3266528785_m4227749606_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<TypewriterEffect/FadeEntry>(System.Int32)
extern "C"  FadeEntry_t1095831350  Array_InternalArray__get_Item_TisFadeEntry_t1095831350_m2179658461_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFadeEntry_t1095831350_m2179658461(__this, ___index0, method) ((  FadeEntry_t1095831350  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFadeEntry_t1095831350_m2179658461_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<TypewriterEffect/FadeEntry>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisFadeEntry_t1095831350_m371128732_gshared (Il2CppArray * __this, int32_t p0, FadeEntry_t1095831350 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisFadeEntry_t1095831350_m371128732(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, FadeEntry_t1095831350 *, const MethodInfo*))Array_GetGenericValueImpl_TisFadeEntry_t1095831350_m371128732_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UICamera/DepthEntry>(System.Int32)
extern "C"  DepthEntry_t2194697103  Array_InternalArray__get_Item_TisDepthEntry_t2194697103_m1694698009_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDepthEntry_t2194697103_m1694698009(__this, ___index0, method) ((  DepthEntry_t2194697103  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDepthEntry_t2194697103_m1694698009_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UICamera/DepthEntry>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisDepthEntry_t2194697103_m479175854_gshared (Il2CppArray * __this, int32_t p0, DepthEntry_t2194697103 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisDepthEntry_t2194697103_m479175854(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, DepthEntry_t2194697103 *, const MethodInfo*))Array_GetGenericValueImpl_TisDepthEntry_t2194697103_m479175854_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Bounds>(System.Int32)
extern "C"  Bounds_t3518514978  Array_InternalArray__get_Item_TisBounds_t3518514978_m3868888566_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBounds_t3518514978_m3868888566(__this, ___index0, method) ((  Bounds_t3518514978  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBounds_t3518514978_m3868888566_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Bounds>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisBounds_t3518514978_m1678516003_gshared (Il2CppArray * __this, int32_t p0, Bounds_t3518514978 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisBounds_t3518514978_m1678516003(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Bounds_t3518514978 *, const MethodInfo*))Array_GetGenericValueImpl_TisBounds_t3518514978_m1678516003_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t1588175760  Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850(__this, ___index0, method) ((  Color_t1588175760  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Color>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisColor_t1588175760_m855411061_gshared (Il2CppArray * __this, int32_t p0, Color_t1588175760 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisColor_t1588175760_m855411061(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Color_t1588175760 *, const MethodInfo*))Array_GetGenericValueImpl_TisColor_t1588175760_m855411061_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t4137084207  Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403(__this, ___index0, method) ((  Color32_t4137084207  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Color32>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisColor32_t4137084207_m3930958100_gshared (Il2CppArray * __this, int32_t p0, Color32_t4137084207 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisColor32_t4137084207_m3930958100(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Color32_t4137084207 *, const MethodInfo*))Array_GetGenericValueImpl_TisColor32_t4137084207_m3930958100_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t2951122365  Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859(__this, ___index0, method) ((  ContactPoint_t2951122365  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ContactPoint>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContactPoint_t2951122365_m2683850686_gshared (Il2CppArray * __this, int32_t p0, ContactPoint_t2951122365 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContactPoint_t2951122365_m2683850686(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ContactPoint_t2951122365 *, const MethodInfo*))Array_GetGenericValueImpl_TisContactPoint_t2951122365_m2683850686_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern "C"  ContactPoint2D_t3963746319  Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017(__this, ___index0, method) ((  ContactPoint2D_t3963746319  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.ContactPoint2D>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContactPoint2D_t3963746319_m1672251792_gshared (Il2CppArray * __this, int32_t p0, ContactPoint2D_t3963746319 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContactPoint2D_t3963746319_m1672251792(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, ContactPoint2D_t3963746319 *, const MethodInfo*))Array_GetGenericValueImpl_TisContactPoint2D_t3963746319_m1672251792_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern "C"  RaycastResult_t959898689  Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905(__this, ___index0, method) ((  RaycastResult_t959898689  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.EventSystems.RaycastResult>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastResult_t959898689_m1415359984_gshared (Il2CppArray * __this, int32_t p0, RaycastResult_t959898689 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastResult_t959898689_m1415359984(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastResult_t959898689 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastResult_t959898689_m1415359984_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.KeyCode>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisKeyCode_t2371581209_m3074538697_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyCode_t2371581209_m3074538697(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyCode_t2371581209_m3074538697_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.KeyCode>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyCode_t2371581209_m1812275198_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyCode_t2371581209_m1812275198(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisKeyCode_t2371581209_m1812275198_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t2095052507  Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013(__this, ___index0, method) ((  Keyframe_t2095052507  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Keyframe>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisKeyframe_t2095052507_m3275637980_gshared (Il2CppArray * __this, int32_t p0, Keyframe_t2095052507 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisKeyframe_t2095052507_m3275637980(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Keyframe_t2095052507 *, const MethodInfo*))Array_GetGenericValueImpl_TisKeyframe_t2095052507_m3275637980_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.LogType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisLogType_t3529269451_m2421798615_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLogType_t3529269451_m2421798615(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLogType_t3529269451_m2421798615_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.LogType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisLogType_t3529269451_m1555906224_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisLogType_t3529269451_m1555906224(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisLogType_t3529269451_m1555906224_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern "C"  RaycastHit_t46221527  Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793(__this, ___index0, method) ((  RaycastHit_t46221527  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.RaycastHit>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastHit_t46221527_m1822174552_gshared (Il2CppArray * __this, int32_t p0, RaycastHit_t46221527 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastHit_t46221527_m1822174552(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastHit_t46221527 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastHit_t46221527_m1822174552_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern "C"  RaycastHit2D_t4082783401  Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143(__this, ___index0, method) ((  RaycastHit2D_t4082783401  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.RaycastHit2D>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisRaycastHit2D_t4082783401_m2530175146_gshared (Il2CppArray * __this, int32_t p0, RaycastHit2D_t4082783401 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisRaycastHit2D_t4082783401_m2530175146(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, RaycastHit2D_t4082783401 *, const MethodInfo*))Array_GetGenericValueImpl_TisRaycastHit2D_t4082783401_m2530175146_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t2591228609  Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997(__this, ___index0, method) ((  HitInfo_t2591228609  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SendMouseEvents/HitInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisHitInfo_t2591228609_m3860792378_gshared (Il2CppArray * __this, int32_t p0, HitInfo_t2591228609 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisHitInfo_t2591228609_m3860792378(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, HitInfo_t2591228609 *, const MethodInfo*))Array_GetGenericValueImpl_TisHitInfo_t2591228609_m3860792378_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t1317012096  Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226(__this, ___index0, method) ((  GcAchievementData_t1317012096  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcAchievementData_t1317012096_m989090367_gshared (Il2CppArray * __this, int32_t p0, GcAchievementData_t1317012096 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcAchievementData_t1317012096_m989090367(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcAchievementData_t1317012096 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcAchievementData_t1317012096_m989090367_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t2223678307  Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207(__this, ___index0, method) ((  GcScoreData_t2223678307  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisGcScoreData_t2223678307_m4057734434_gshared (Il2CppArray * __this, int32_t p0, GcScoreData_t2223678307 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisGcScoreData_t2223678307_m4057734434(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, GcScoreData_t2223678307 *, const MethodInfo*))Array_GetGenericValueImpl_TisGcScoreData_t2223678307_m4057734434_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t3429487928_m4291319144_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTextEditOp_t3429487928_m4291319144(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTextEditOp_t3429487928_m4291319144_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.TextEditor/TextEditOp>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisTextEditOp_t3429487928_m4219827839_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisTextEditOp_t3429487928_m4219827839(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisTextEditOp_t3429487928_m4219827839_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015(__this, ___index0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UI.InputField/ContentType>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisContentType_t1278737203_m2904454496_gshared (Il2CppArray * __this, int32_t p0, int32_t* p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisContentType_t1278737203_m2904454496(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, int32_t*, const MethodInfo*))Array_GetGenericValueImpl_TisContentType_t1278737203_m2904454496_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern "C"  UICharInfo_t403820581  Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395(__this, ___index0, method) ((  UICharInfo_t403820581  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UICharInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUICharInfo_t403820581_m972947878_gshared (Il2CppArray * __this, int32_t p0, UICharInfo_t403820581 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUICharInfo_t403820581_m972947878(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UICharInfo_t403820581 *, const MethodInfo*))Array_GetGenericValueImpl_TisUICharInfo_t403820581_m972947878_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern "C"  UILineInfo_t156921283  Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045(__this, ___index0, method) ((  UILineInfo_t156921283  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UILineInfo>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUILineInfo_t156921283_m1279108164_gshared (Il2CppArray * __this, int32_t p0, UILineInfo_t156921283 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUILineInfo_t156921283_m1279108164(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UILineInfo_t156921283 *, const MethodInfo*))Array_GetGenericValueImpl_TisUILineInfo_t156921283_m1279108164_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern "C"  UIVertex_t2260061605  Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059(__this, ___index0, method) ((  UIVertex_t2260061605  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.UIVertex>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisUIVertex_t2260061605_m1585369766_gshared (Il2CppArray * __this, int32_t p0, UIVertex_t2260061605 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisUIVertex_t2260061605_m1585369766(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, UIVertex_t2260061605 *, const MethodInfo*))Array_GetGenericValueImpl_TisUIVertex_t2260061605_m1585369766_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t3525329788  Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166(__this, ___index0, method) ((  Vector2_t3525329788  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector2>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector2_t3525329788_m2608717537_gshared (Il2CppArray * __this, int32_t p0, Vector2_t3525329788 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector2_t3525329788_m2608717537(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector2_t3525329788 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector2_t3525329788_m2608717537_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern "C"  Vector3_t3525329789  Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989(__this, ___index0, method) ((  Vector3_t3525329789  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector3>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector3_t3525329789_m2820068450_gshared (Il2CppArray * __this, int32_t p0, Vector3_t3525329789 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector3_t3525329789_m2820068450(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector3_t3525329789 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector3_t3525329789_m2820068450_gshared)(__this, p0, p1, method)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern "C"  Vector4_t3525329790  Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812(__this, ___index0, method) ((  Vector4_t3525329790  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_gshared)(__this, ___index0, method)
// System.Void System.Array::GetGenericValueImpl<UnityEngine.Vector4>(System.Int32,!!0&)
extern "C"  void Array_GetGenericValueImpl_TisVector4_t3525329790_m3031419363_gshared (Il2CppArray * __this, int32_t p0, Vector4_t3525329790 * p1, const MethodInfo* method);
#define Array_GetGenericValueImpl_TisVector4_t3525329790_m3031419363(__this, p0, p1, method) ((  void (*) (Il2CppArray *, int32_t, Vector4_t3525329790 *, const MethodInfo*))Array_GetGenericValueImpl_TisVector4_t3525329790_m3031419363_gshared)(__this, p0, p1, method)
// T UITweener::Begin<System.Object>(UnityEngine.GameObject,System.Single)
extern "C"  Il2CppObject * UITweener_Begin_TisIl2CppObject_m2506168874_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___go0, float ___duration1, const MethodInfo* method);
#define UITweener_Begin_TisIl2CppObject_m2506168874(__this /* static, unused */, ___go0, ___duration1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, float, const MethodInfo*))UITweener_Begin_TisIl2CppObject_m2506168874_gshared)(__this /* static, unused */, ___go0, ___duration1, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponents_TisIl2CppObject_m313358914_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponents_TisIl2CppObject_m313358914(__this, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m313358914_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m337943659(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// T UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m833851745_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m833851745(__this, p0, method) ((  Il2CppObject * (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m833851745_gshared)(__this, p0, method)
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInParent_TisIl2CppObject_m1297875695(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared)(__this, method)
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
extern "C"  Il2CppObject * ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_gshared (Il2CppObject * __this /* static, unused */, BaseEventData_t3547103726 * ___data0, const MethodInfo* method);
#define ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176(__this /* static, unused */, ___data0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, BaseEventData_t3547103726 *, const MethodInfo*))ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_gshared)(__this /* static, unused */, ___data0, method)
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m782999868(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared (GameObject_t4012695102 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411(__this, p0, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, bool, const MethodInfo*))GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared)(__this, p0, method)
// T UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method);
#define JsonUtility_FromJson_TisIl2CppObject_m173634649(__this /* static, unused */, ___json0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, ___json0, method)
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m829214560_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisIl2CppObject_m829214560(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisIl2CppObject_m829214560_gshared)(__this /* static, unused */, method)
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, const MethodInfo* method);
#define Object_Instantiate_TisIl2CppObject_m3133387403(__this /* static, unused */, ___original0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m3133387403_gshared)(__this /* static, unused */, ___original0, method)
// T UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2208345422_gshared (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method);
#define Resources_Load_TisIl2CppObject_m2208345422(__this /* static, unused */, ___path0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, ___path0, method)
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ScriptableObject_CreateInstance_TisIl2CppObject_m512360883(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_gshared)(__this /* static, unused */, method)
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___go0, const MethodInfo* method);
#define Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758(__this /* static, unused */, ___go0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758_gshared)(__this /* static, unused */, ___go0, method)
// T[] NGUITools::FindActive<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* NGUITools_FindActive_TisIl2CppObject_m943247225_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define NGUITools_FindActive_TisIl2CppObject_m943247225(__this /* static, unused */, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NGUITools_FindActive_TisIl2CppObject_m943247225_gshared)(__this /* static, unused */, method)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern "C"  ObjectU5BU5D_t11523773* Array_FindAll_TisIl2CppObject_m3670613038_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array0, Predicate_1_t1408070318 * ___match1, const MethodInfo* method);
#define Array_FindAll_TisIl2CppObject_m3670613038(__this /* static, unused */, ___array0, ___match1, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Predicate_1_t1408070318 *, const MethodInfo*))Array_FindAll_TisIl2CppObject_m3670613038_gshared)(__this /* static, unused */, ___array0, ___match1, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m1039640932_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIl2CppObject_m1039640932(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m1039640932_gshared)(__this /* static, unused */, p0, p1, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t11523773* CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542(__this /* static, unused */, ___values0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared)(__this /* static, unused */, ___values0, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3019176036* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489(__this /* static, unused */, ___values0, method) ((  CustomAttributeNamedArgumentU5BU5D_t3019176036* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489_gshared)(__this /* static, unused */, ___values0, method)
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t3123668047* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method);
#define CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954(__this /* static, unused */, ___values0, method) ((  CustomAttributeTypedArgumentU5BU5D_t3123668047* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, const MethodInfo*))CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954_gshared)(__this /* static, unused */, ___values0, method)
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponents_TisIl2CppObject_m4264249070_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponents_TisIl2CppObject_m4264249070(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m4264249070_gshared)(__this, method)
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m2266473418(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m1896280001_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m1896280001(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m1896280001_gshared)(__this, p0, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_gshared (GameObject_t4012695102 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_gshared)(__this, p0, method)
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m1228840236(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInParent_TisIl2CppObject_m1225053603(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared)(__this, p0, method)
// !!0[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared (GameObject_t4012695102 * __this, bool p0, const MethodInfo* method);
#define GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared)(__this, p0, method)
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430(__this, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared)(__this, method)
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Object_FindObjectsOfType_TisIl2CppObject_m3888714627_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectsOfType_TisIl2CppObject_m3888714627(__this /* static, unused */, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Object_FindObjectsOfType_TisIl2CppObject_m3888714627_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t11523773* Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3051965477* p0, const MethodInfo* method);
#define Resources_ConvertObjects_TisIl2CppObject_m1537961554(__this /* static, unused */, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3051965477*, const MethodInfo*))Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared)(__this /* static, unused */, p0, method)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern "C"  ObjectU5BU5D_t11523773* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array0, Converter_2_t113996300 * ___converter1, const MethodInfo* method);
#define Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410(__this /* static, unused */, ___array0, ___converter1, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Converter_2_t113996300 *, const MethodInfo*))Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared)(__this /* static, unused */, ___array0, ___converter1, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern "C"  GameObject_t4012695102 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___root0, BaseEventData_t3547103726 * ___eventData1, EventFunction_1_t3885370180 * ___callbackFunction2, const MethodInfo* method);
#define ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293(__this /* static, unused */, ___root0, ___eventData1, ___callbackFunction2, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, BaseEventData_t3547103726 *, EventFunction_1_t3885370180 *, const MethodInfo*))ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_gshared)(__this /* static, unused */, ___root0, ___eventData1, ___callbackFunction2, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::Execute<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<!!0>)
extern "C"  bool ExecuteEvents_Execute_TisIl2CppObject_m1533897725_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * p0, BaseEventData_t3547103726 * p1, EventFunction_1_t3885370180 * p2, const MethodInfo* method);
#define ExecuteEvents_Execute_TisIl2CppObject_m1533897725(__this /* static, unused */, p0, p1, p2, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, BaseEventData_t3547103726 *, EventFunction_1_t3885370180 *, const MethodInfo*))ExecuteEvents_Execute_TisIl2CppObject_m1533897725_gshared)(__this /* static, unused */, p0, p1, p2, method)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern "C"  GameObject_t4012695102 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___root0, const MethodInfo* method);
#define ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506(__this /* static, unused */, ___root0, method) ((  GameObject_t4012695102 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_gshared)(__this /* static, unused */, ___root0, method)
// System.Boolean UnityEngine.EventSystems.ExecuteEvents::CanHandleEvent<System.Object>(UnityEngine.GameObject)
extern "C"  bool ExecuteEvents_CanHandleEvent_TisIl2CppObject_m2627025177_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * p0, const MethodInfo* method);
#define ExecuteEvents_CanHandleEvent_TisIl2CppObject_m2627025177(__this /* static, unused */, p0, method) ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))ExecuteEvents_CanHandleEvent_TisIl2CppObject_m2627025177_gshared)(__this /* static, unused */, p0, method)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_MetadataUsageId;
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint8_t*)(&V_0));
		uint8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
// T System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_MetadataUsageId;
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int8_t V_0 = 0x0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int8_t*)(&V_0));
		int8_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_MetadataUsageId;
extern "C"  X509ChainStatus_t1122151684  Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	X509ChainStatus_t1122151684  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (X509ChainStatus_t1122151684 *)(&V_0));
		X509ChainStatus_t1122151684  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_MetadataUsageId;
extern "C"  float Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (float*)(&V_0));
		float L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_MetadataUsageId;
extern "C"  Mark_t3725932776  Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mark_t3725932776  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Mark_t3725932776 *)(&V_0));
		Mark_t3725932776  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
// T System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_MetadataUsageId;
extern "C"  TimeSpan_t763862892  Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TimeSpan_t763862892  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (TimeSpan_t763862892 *)(&V_0));
		TimeSpan_t763862892  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_MetadataUsageId;
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint16_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint16_t*)(&V_0));
		uint16_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_MetadataUsageId;
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint32_t*)(&V_0));
		uint32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
// T System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_MetadataUsageId;
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint64_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (uint64_t*)(&V_0));
		uint64_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
// T System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_MetadataUsageId;
extern "C"  UriScheme_t3266528785  Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UriScheme_t3266528785  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UriScheme_t3266528785 *)(&V_0));
		UriScheme_t3266528785  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<TypewriterEffect/FadeEntry>(System.Int32)
// T System.Array::InternalArray__get_Item<TypewriterEffect/FadeEntry>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisFadeEntry_t1095831350_m2179658461_MetadataUsageId;
extern "C"  FadeEntry_t1095831350  Array_InternalArray__get_Item_TisFadeEntry_t1095831350_m2179658461_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisFadeEntry_t1095831350_m2179658461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FadeEntry_t1095831350  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (FadeEntry_t1095831350 *)(&V_0));
		FadeEntry_t1095831350  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UICamera/DepthEntry>(System.Int32)
// T System.Array::InternalArray__get_Item<UICamera/DepthEntry>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisDepthEntry_t2194697103_m1694698009_MetadataUsageId;
extern "C"  DepthEntry_t2194697103  Array_InternalArray__get_Item_TisDepthEntry_t2194697103_m1694698009_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisDepthEntry_t2194697103_m1694698009_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DepthEntry_t2194697103  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (DepthEntry_t2194697103 *)(&V_0));
		DepthEntry_t2194697103  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Bounds>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Bounds>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisBounds_t3518514978_m3868888566_MetadataUsageId;
extern "C"  Bounds_t3518514978  Array_InternalArray__get_Item_TisBounds_t3518514978_m3868888566_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisBounds_t3518514978_m3868888566_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t3518514978  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Bounds_t3518514978 *)(&V_0));
		Bounds_t3518514978  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_MetadataUsageId;
extern "C"  Color_t1588175760  Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color_t1588175760 *)(&V_0));
		Color_t1588175760  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_MetadataUsageId;
extern "C"  Color32_t4137084207  Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color32_t4137084207  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Color32_t4137084207 *)(&V_0));
		Color32_t4137084207  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_MetadataUsageId;
extern "C"  ContactPoint_t2951122365  Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ContactPoint_t2951122365  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint_t2951122365 *)(&V_0));
		ContactPoint_t2951122365  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_MetadataUsageId;
extern "C"  ContactPoint2D_t3963746319  Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ContactPoint2D_t3963746319  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (ContactPoint2D_t3963746319 *)(&V_0));
		ContactPoint2D_t3963746319  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_MetadataUsageId;
extern "C"  RaycastResult_t959898689  Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastResult_t959898689  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastResult_t959898689 *)(&V_0));
		RaycastResult_t959898689  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.KeyCode>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.KeyCode>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyCode_t2371581209_m3074538697_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisKeyCode_t2371581209_m3074538697_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyCode_t2371581209_m3074538697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_MetadataUsageId;
extern "C"  Keyframe_t2095052507  Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Keyframe_t2095052507  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Keyframe_t2095052507 *)(&V_0));
		Keyframe_t2095052507  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.LogType>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.LogType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisLogType_t3529269451_m2421798615_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisLogType_t3529269451_m2421798615_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisLogType_t3529269451_m2421798615_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_MetadataUsageId;
extern "C"  RaycastHit_t46221527  Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t46221527  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit_t46221527 *)(&V_0));
		RaycastHit_t46221527  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_MetadataUsageId;
extern "C"  RaycastHit2D_t4082783401  Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t4082783401  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (RaycastHit2D_t4082783401 *)(&V_0));
		RaycastHit2D_t4082783401  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_MetadataUsageId;
extern "C"  HitInfo_t2591228609  Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	HitInfo_t2591228609  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (HitInfo_t2591228609 *)(&V_0));
		HitInfo_t2591228609  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_MetadataUsageId;
extern "C"  GcAchievementData_t1317012096  Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcAchievementData_t1317012096  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcAchievementData_t1317012096 *)(&V_0));
		GcAchievementData_t1317012096  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_MetadataUsageId;
extern "C"  GcScoreData_t2223678307  Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GcScoreData_t2223678307  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (GcScoreData_t2223678307 *)(&V_0));
		GcScoreData_t2223678307  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisTextEditOp_t3429487928_m4291319144_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t3429487928_m4291319144_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisTextEditOp_t3429487928_m4291319144_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_MetadataUsageId;
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (int32_t*)(&V_0));
		int32_t L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_MetadataUsageId;
extern "C"  UICharInfo_t403820581  Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UICharInfo_t403820581  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UICharInfo_t403820581 *)(&V_0));
		UICharInfo_t403820581  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_MetadataUsageId;
extern "C"  UILineInfo_t156921283  Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UILineInfo_t156921283  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UILineInfo_t156921283 *)(&V_0));
		UILineInfo_t156921283  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_MetadataUsageId;
extern "C"  UIVertex_t2260061605  Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t2260061605  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (UIVertex_t2260061605 *)(&V_0));
		UIVertex_t2260061605  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_MetadataUsageId;
extern "C"  Vector2_t3525329788  Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector2_t3525329788 *)(&V_0));
		Vector2_t3525329788  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector3>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_MetadataUsageId;
extern "C"  Vector3_t3525329789  Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector3_t3525329789_m1333909989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector3_t3525329789 *)(&V_0));
		Vector3_t3525329789  L_4 = V_0;
		return L_4;
	}
}
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
// T System.Array::InternalArray__get_Item<UnityEngine.Vector4>(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_MetadataUsageId;
extern "C"  Vector4_t3525329790  Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_gshared (Il2CppArray * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_InternalArray__get_Item_TisVector4_t3525329790_m823375812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t3525329790  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = ___index0;
		NullCheck((Il2CppArray *)__this);
		int32_t L_1 = Array_get_Length_m1203127607((Il2CppArray *)__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_0017;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = ___index0;
		NullCheck((Il2CppArray *)__this);
		ArrayGetGenericValueImpl((Il2CppArray *)__this, (int32_t)L_3, (Vector4_t3525329790 *)(&V_0));
		Vector4_t3525329790  L_4 = V_0;
		return L_4;
	}
}
// T UITweener::Begin<System.Object>(UnityEngine.GameObject,System.Single)
// T UITweener::Begin<System.Object>(UnityEngine.GameObject,System.Single)
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* KeyframeU5BU5D_t2477572954_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3342907448_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2795194007;
extern Il2CppCodeGenString* _stringLiteral1068261;
extern const uint32_t UITweener_Begin_TisIl2CppObject_m2506168874_MetadataUsageId;
extern "C"  Il2CppObject * UITweener_Begin_TisIl2CppObject_m2506168874_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___go0, float ___duration1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Begin_TisIl2CppObject_m2506168874_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ObjectU5BU5D_t11523773* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		GameObject_t4012695102 * L_0 = ___go0;
		NullCheck((GameObject_t4012695102 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3878351788 *)L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0081;
		}
	}
	{
		Il2CppObject * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = (int32_t)((UITweener_t105489188 *)L_4)->get_tweenGroup_10();
		if (!L_5)
		{
			goto IL_0081;
		}
	}
	{
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		GameObject_t4012695102 * L_6 = ___go0;
		NullCheck((GameObject_t4012695102 *)L_6);
		ObjectU5BU5D_t11523773* L_7 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)((GameObject_t4012695102 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_1 = (ObjectU5BU5D_t11523773*)L_7;
		V_2 = (int32_t)0;
		ObjectU5BU5D_t11523773* L_8 = V_1;
		NullCheck(L_8);
		V_3 = (int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length))));
		goto IL_007a;
	}

IL_0041:
	{
		ObjectU5BU5D_t11523773* L_9 = V_1;
		int32_t L_10 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		V_0 = (Il2CppObject *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11)));
		Il2CppObject * L_12 = V_0;
		bool L_13 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3878351788 *)L_12, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_006f;
		}
	}
	{
		Il2CppObject * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = (int32_t)((UITweener_t105489188 *)L_14)->get_tweenGroup_10();
		if (L_15)
		{
			goto IL_006f;
		}
	}
	{
		goto IL_0081;
	}

IL_006f:
	{
		V_0 = (Il2CppObject *)((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
		int32_t L_16 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_007a:
	{
		int32_t L_17 = V_2;
		int32_t L_18 = V_3;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_0041;
		}
	}

IL_0081:
	{
		Il2CppObject * L_19 = V_0;
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3878351788 *)L_19, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00e8;
		}
	}
	{
		GameObject_t4012695102 * L_21 = ___go0;
		NullCheck((GameObject_t4012695102 *)L_21);
		Il2CppObject * L_22 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3)->method)((GameObject_t4012695102 *)L_21, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 3));
		V_0 = (Il2CppObject *)L_22;
		Il2CppObject * L_23 = V_0;
		bool L_24 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3878351788 *)L_23, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00e8;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_25 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		ArrayElementTypeCheck (L_25, _stringLiteral2795194007);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2795194007);
		ObjectU5BU5D_t11523773* L_26 = (ObjectU5BU5D_t11523773*)L_25;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_27 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 4)), /*hidden argument*/NULL);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 1);
		ArrayElementTypeCheck (L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_27);
		ObjectU5BU5D_t11523773* L_28 = (ObjectU5BU5D_t11523773*)L_26;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 2);
		ArrayElementTypeCheck (L_28, _stringLiteral1068261);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1068261);
		ObjectU5BU5D_t11523773* L_29 = (ObjectU5BU5D_t11523773*)L_28;
		GameObject_t4012695102 * L_30 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		String_t* L_31 = NGUITools_GetHierarchy_m3656632443(NULL /*static, unused*/, (GameObject_t4012695102 *)L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
		ArrayElementTypeCheck (L_29, L_31);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_31);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_32 = String_Concat_m3016520001(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_29, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_33 = ___go0;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m214246398(NULL /*static, unused*/, (Il2CppObject *)L_32, (Object_t3878351788 *)L_33, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(NULL, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}

IL_00e8:
	{
		Il2CppObject * L_34 = V_0;
		NullCheck(L_34);
		((UITweener_t105489188 *)L_34)->set_mStarted_14((bool)0);
		Il2CppObject * L_35 = V_0;
		float L_36 = ___duration1;
		NullCheck(L_35);
		((UITweener_t105489188 *)L_35)->set_duration_8(L_36);
		Il2CppObject * L_37 = V_0;
		NullCheck(L_37);
		((UITweener_t105489188 *)L_37)->set_mFactor_18((0.0f));
		Il2CppObject * L_38 = V_0;
		NullCheck((UITweener_t105489188 *)(*(&V_0)));
		float L_39 = UITweener_get_amountPerDelta_m342139981((UITweener_t105489188 *)(*(&V_0)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_40 = fabsf((float)L_39);
		NullCheck(L_38);
		((UITweener_t105489188 *)L_38)->set_mAmountPerDelta_17(L_40);
		Il2CppObject * L_41 = V_0;
		NullCheck(L_41);
		((UITweener_t105489188 *)L_41)->set_style_4(0);
		Il2CppObject * L_42 = V_0;
		KeyframeU5BU5D_t2477572954* L_43 = (KeyframeU5BU5D_t2477572954*)((KeyframeU5BU5D_t2477572954*)SZArrayNew(KeyframeU5BU5D_t2477572954_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 0);
		Keyframe_t2095052507  L_44;
		memset(&L_44, 0, sizeof(L_44));
		Keyframe__ctor_m3412708539(&L_44, (float)(0.0f), (float)(0.0f), (float)(0.0f), (float)(1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t2095052507 *)((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_44;
		KeyframeU5BU5D_t2477572954* L_45 = (KeyframeU5BU5D_t2477572954*)L_43;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 1);
		Keyframe_t2095052507  L_46;
		memset(&L_46, 0, sizeof(L_46));
		Keyframe__ctor_m3412708539(&L_46, (float)(1.0f), (float)(1.0f), (float)(1.0f), (float)(0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t2095052507 *)((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_46;
		AnimationCurve_t3342907448 * L_47 = (AnimationCurve_t3342907448 *)il2cpp_codegen_object_new(AnimationCurve_t3342907448_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2436282331(L_47, (KeyframeU5BU5D_t2477572954*)L_45, /*hidden argument*/NULL);
		NullCheck(L_42);
		((UITweener_t105489188 *)L_42)->set_animationCurve_5(L_47);
		Il2CppObject * L_48 = V_0;
		NullCheck(L_48);
		((UITweener_t105489188 *)L_48)->set_eventReceiver_12((GameObject_t4012695102 *)NULL);
		Il2CppObject * L_49 = V_0;
		NullCheck(L_49);
		((UITweener_t105489188 *)L_49)->set_callWhenFinished_13((String_t*)NULL);
		NullCheck((Behaviour_t3120504042 *)(*(&V_0)));
		Behaviour_set_enabled_m2046806933((Behaviour_t3120504042 *)(*(&V_0)), (bool)1, /*hidden argument*/NULL);
		Il2CppObject * L_50 = V_0;
		return L_50;
	}
}
// T UnityEngine.Component::GetComponent<System.Object>()
// T UnityEngine.Component::GetComponent<System.Object>()
extern Il2CppClass* CastHelper_1_t4244616972_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponent_TisIl2CppObject_m267839954_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponent_TisIl2CppObject_m267839954_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CastHelper_1_t4244616972  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CastHelper_1_t4244616972_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m2509422495(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((Component_t2126946602 *)__this);
		Component_GetComponentFastPath_m1455568887((Component_t2126946602 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		return L_3;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
// T UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((Component_t2126946602 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (Component_t2126946602 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Component_t2126946602 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
// T UnityEngine.Component::GetComponentInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInChildren_TisIl2CppObject_m833851745_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m833851745_gshared (Component_t2126946602 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInChildren_TisIl2CppObject_m833851745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((Component_t2126946602 *)__this);
		Component_t2126946602 * L_2 = Component_GetComponentInChildren_m1899663946((Component_t2126946602 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Component::GetComponentInParent<System.Object>()
// T UnityEngine.Component::GetComponentInParent<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Component_GetComponentInParent_TisIl2CppObject_m1297875695_MetadataUsageId;
extern "C"  Il2CppObject * Component_GetComponentInParent_TisIl2CppObject_m1297875695_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Component_GetComponentInParent_TisIl2CppObject_m1297875695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((Component_t2126946602 *)__this);
		Component_t2126946602 * L_1 = Component_GetComponentInParent_m1953645192((Component_t2126946602 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
// T UnityEngine.EventSystems.ExecuteEvents::ValidateEventData<System.Object>(UnityEngine.EventSystems.BaseEventData)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1477400244;
extern const uint32_t ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_MetadataUsageId;
extern "C"  Il2CppObject * ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_gshared (Il2CppObject * __this /* static, unused */, BaseEventData_t3547103726 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ValidateEventData_TisIl2CppObject_m1498065176_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BaseEventData_t3547103726 * L_0 = ___data0;
		if (((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_0, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0))))
		{
			goto IL_003a;
		}
	}
	{
		BaseEventData_t3547103726 * L_1 = ___data0;
		NullCheck((Il2CppObject *)L_1);
		Type_t * L_2 = Object_GetType_m2022236990((Il2CppObject *)L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 1)), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2398979370(NULL /*static, unused*/, (String_t*)_stringLiteral1477400244, (Il2CppObject *)L_2, (Il2CppObject *)L_3, /*hidden argument*/NULL);
		ArgumentException_t124305799 * L_5 = (ArgumentException_t124305799 *)il2cpp_codegen_object_new(ArgumentException_t124305799_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_003a:
	{
		BaseEventData_t3547103726 * L_6 = ___data0;
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_6, IL2CPP_RGCTX_DATA(method->rgctx_data, 0))), IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T UnityEngine.GameObject::AddComponent<System.Object>()
// T UnityEngine.GameObject::AddComponent<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_AddComponent_TisIl2CppObject_m337943659_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_AddComponent_TisIl2CppObject_m337943659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)__this);
		Component_t2126946602 * L_1 = GameObject_AddComponent_m2208780168((GameObject_t4012695102 *)__this, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(((Il2CppObject *)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.GameObject::GetComponent<System.Object>()
// T UnityEngine.GameObject::GetComponent<System.Object>()
extern Il2CppClass* CastHelper_1_t4244616972_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponent_TisIl2CppObject_m2447772384_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponent_TisIl2CppObject_m2447772384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CastHelper_1_t4244616972  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (CastHelper_1_t4244616972_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		IntPtr_t* L_1 = (IntPtr_t*)(&V_0)->get_address_of_onePointerFurtherThanT_1();
		IntPtr_t L_2;
		memset(&L_2, 0, sizeof(L_2));
		IntPtr__ctor_m2509422495(&L_2, (void*)(void*)L_1, /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)__this);
		GameObject_GetComponentFastPath_m2905716663((GameObject_t4012695102 *)__this, (Type_t *)L_0, (IntPtr_t)L_2, /*hidden argument*/NULL);
		Il2CppObject * L_3 = (Il2CppObject *)(&V_0)->get_t_0();
		return L_3;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m782999868_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = (bool)0;
		bool L_0 = V_0;
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)__this, (bool)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
// T UnityEngine.GameObject::GetComponentInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_MetadataUsageId;
extern "C"  Il2CppObject * GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_gshared (GameObject_t4012695102 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentInChildren_TisIl2CppObject_m4037889411_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)__this);
		Component_t2126946602 * L_2 = GameObject_GetComponentInChildren_m1490154500((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
// T UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t JsonUtility_FromJson_TisIl2CppObject_m173634649_MetadataUsageId;
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (JsonUtility_FromJson_TisIl2CppObject_m173634649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___json0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Il2CppObject * L_2 = JsonUtility_FromJson_m3245538665(NULL /*static, unused*/, (String_t*)L_0, (Type_t *)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Object::FindObjectOfType<System.Object>()
// T UnityEngine.Object::FindObjectOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectOfType_TisIl2CppObject_m829214560_MetadataUsageId;
extern "C"  Il2CppObject * Object_FindObjectOfType_TisIl2CppObject_m829214560_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectOfType_TisIl2CppObject_m829214560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.Object::Instantiate<System.Object>(T)
// T UnityEngine.Object::Instantiate<System.Object>(T)
extern Il2CppCodeGenString* _stringLiteral3473406;
extern const uint32_t Object_Instantiate_TisIl2CppObject_m3133387403_MetadataUsageId;
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m3133387403_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___original0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_Instantiate_TisIl2CppObject_m3133387403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = ___original0;
		Object_CheckNullArgument_m264735768(NULL /*static, unused*/, (Il2CppObject *)L_0, (String_t*)_stringLiteral3473406, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___original0;
		Object_t3878351788 * L_2 = Object_Internal_CloneSingle_m3129073756(NULL /*static, unused*/, (Object_t3878351788 *)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 0)));
	}
}
// T UnityEngine.Resources::Load<System.Object>(System.String)
// T UnityEngine.Resources::Load<System.Object>(System.String)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Resources_Load_TisIl2CppObject_m2208345422_MetadataUsageId;
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2208345422_gshared (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Resources_Load_TisIl2CppObject_m2208345422_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___path0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		Object_t3878351788 * L_2 = Resources_Load_m3601699608(NULL /*static, unused*/, (String_t*)L_0, (Type_t *)L_1, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
// T UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_MetadataUsageId;
extern "C"  Il2CppObject * ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScriptableObject_CreateInstance_TisIl2CppObject_m512360883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ScriptableObject_t184905905 * L_1 = ScriptableObject_CreateInstance_m3255479417(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((Il2CppObject *)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
// T UnityEngine.UI.Dropdown::GetOrAddComponent<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * Dropdown_GetOrAddComponent_TisIl2CppObject_m4155558758_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___go0, const MethodInfo* method)
{
	Il2CppObject * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = ___go0;
		NullCheck((GameObject_t4012695102 *)L_0);
		Il2CppObject * L_1 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		V_0 = (Il2CppObject *)L_1;
		Il2CppObject * L_2 = V_0;
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, (Object_t3878351788 *)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001e;
		}
	}
	{
		GameObject_t4012695102 * L_4 = ___go0;
		NullCheck((GameObject_t4012695102 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)((GameObject_t4012695102 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_5;
	}

IL_001e:
	{
		Il2CppObject * L_6 = V_0;
		return L_6;
	}
}
// T[] NGUITools::FindActive<System.Object>()
// T[] NGUITools::FindActive<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t NGUITools_FindActive_TisIl2CppObject_m943247225_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* NGUITools_FindActive_TisIl2CppObject_m943247225_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (NGUITools_FindActive_TisIl2CppObject_m943247225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ObjectU5BU5D_t3051965477* L_1 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)IsInst(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
// T[] System.Array::FindAll<System.Object>(T[],System.Predicate`1<T>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral103668165;
extern const uint32_t Array_FindAll_TisIl2CppObject_m3670613038_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Array_FindAll_TisIl2CppObject_m3670613038_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array0, Predicate_1_t1408070318 * ___match1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_FindAll_TisIl2CppObject_m3670613038_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t11523773* V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	ObjectU5BU5D_t11523773* V_3 = NULL;
	int32_t V_4 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Predicate_1_t1408070318 * L_2 = ___match1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral103668165, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		V_0 = (int32_t)0;
		ObjectU5BU5D_t11523773* L_4 = ___array0;
		NullCheck(L_4);
		V_1 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		ObjectU5BU5D_t11523773* L_5 = ___array0;
		V_3 = (ObjectU5BU5D_t11523773*)L_5;
		V_4 = (int32_t)0;
		goto IL_005e;
	}

IL_0037:
	{
		ObjectU5BU5D_t11523773* L_6 = V_3;
		int32_t L_7 = V_4;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		V_2 = (Il2CppObject *)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8)));
		Predicate_1_t1408070318 * L_9 = ___match1;
		Il2CppObject * L_10 = V_2;
		NullCheck((Predicate_1_t1408070318 *)L_9);
		bool L_11 = ((  bool (*) (Predicate_1_t1408070318 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Predicate_1_t1408070318 *)L_9, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		if (!L_11)
		{
			goto IL_0058;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_12 = V_1;
		int32_t L_13 = V_0;
		int32_t L_14 = (int32_t)L_13;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		Il2CppObject * L_15 = V_2;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)L_15);
	}

IL_0058:
	{
		int32_t L_16 = V_4;
		V_4 = (int32_t)((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_17 = V_4;
		ObjectU5BU5D_t11523773* L_18 = V_3;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_19 = V_0;
		((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773**)(&V_1), (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 2));
		ObjectU5BU5D_t11523773* L_20 = V_1;
		return L_20;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Object>(System.Object[])
extern "C"  ObjectU5BU5D_t11523773* CustomAttributeData_UnboxValues_TisIl2CppObject_m1206997542_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		ObjectU5BU5D_t11523773* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t11523773* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (Il2CppObject *)((Il2CppObject *)Castclass(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t11523773* L_8 = ___values0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_9 = V_0;
		return L_9;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeNamedArgument>(System.Object[])
extern "C"  CustomAttributeNamedArgumentU5BU5D_t3019176036* CustomAttributeData_UnboxValues_TisCustomAttributeNamedArgument_t318735129_m2964992489_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method)
{
	CustomAttributeNamedArgumentU5BU5D_t3019176036* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)((CustomAttributeNamedArgumentU5BU5D_t3019176036*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t11523773* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeNamedArgument_t318735129 )((*(CustomAttributeNamedArgument_t318735129 *)((CustomAttributeNamedArgument_t318735129 *)UnBox (((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t11523773* L_8 = ___values0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_9 = V_0;
		return L_9;
	}
}
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
// T[] System.Reflection.CustomAttributeData::UnboxValues<System.Reflection.CustomAttributeTypedArgument>(System.Object[])
extern "C"  CustomAttributeTypedArgumentU5BU5D_t3123668047* CustomAttributeData_UnboxValues_TisCustomAttributeTypedArgument_t560415562_m3125716954_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___values0, const MethodInfo* method)
{
	CustomAttributeTypedArgumentU5BU5D_t3123668047* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___values0;
		NullCheck(L_0);
		V_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)((CustomAttributeTypedArgumentU5BU5D_t3123668047*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_0023;
	}

IL_0010:
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = V_0;
		int32_t L_2 = V_1;
		ObjectU5BU5D_t11523773* L_3 = ___values0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (CustomAttributeTypedArgument_t560415562 )((*(CustomAttributeTypedArgument_t560415562 *)((CustomAttributeTypedArgument_t560415562 *)UnBox (((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))))));
		int32_t L_6 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_7 = V_1;
		ObjectU5BU5D_t11523773* L_8 = ___values0;
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0010;
		}
	}
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_9 = V_0;
		return L_9;
	}
}
// T[] UnityEngine.Component::GetComponents<System.Object>()
// T[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponents_TisIl2CppObject_m4264249070_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)L_0);
		ObjectU5BU5D_t11523773* L_1 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_1;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		ObjectU5BU5D_t11523773* L_0 = ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Component_t2126946602 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
// T[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m1896280001_gshared (Component_t2126946602 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)L_0);
		ObjectU5BU5D_t11523773* L_2 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1228840236_gshared (Component_t2126946602 * __this, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		ObjectU5BU5D_t11523773* L_0 = ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((Component_t2126946602 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
// T[] UnityEngine.Component::GetComponentsInParent<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInParent_TisIl2CppObject_m1225053603_gshared (Component_t2126946602 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	{
		NullCheck((Component_t2126946602 *)__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899((Component_t2126946602 *)__this, /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)L_0);
		ObjectU5BU5D_t11523773* L_2 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)L_0, (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_2;
	}
}
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
// T[] UnityEngine.GameObject::GetComponents<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponents_TisIl2CppObject_m313358914_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponents_TisIl2CppObject_m313358914_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponents_TisIl2CppObject_m313358914_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppArray * L_1 = GameObject_GetComponentsInternal_m181453881((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)1, (bool)0, (bool)1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)Castclass(L_1, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared (GameObject_t4012695102 * __this, const MethodInfo* method)
{
	{
		NullCheck((GameObject_t4012695102 *)__this);
		ObjectU5BU5D_t11523773* L_0 = ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)((GameObject_t4012695102 *)__this, (bool)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		return L_0;
	}
}
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
// T[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_gshared (GameObject_t4012695102 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInChildren_TisIl2CppObject_m2662950677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m181453881((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)0, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
// T[] UnityEngine.GameObject::GetComponentsInParent<System.Object>(System.Boolean)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_gshared (GameObject_t4012695102 * __this, bool ___includeInactive0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameObject_GetComponentsInParent_TisIl2CppObject_m1351944637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		bool L_1 = ___includeInactive0;
		NullCheck((GameObject_t4012695102 *)__this);
		Il2CppArray * L_2 = GameObject_GetComponentsInternal_m181453881((GameObject_t4012695102 *)__this, (Type_t *)L_0, (bool)1, (bool)1, (bool)L_1, (bool)1, (Il2CppObject *)NULL, /*hidden argument*/NULL);
		return ((ObjectU5BU5D_t11523773*)Castclass(L_2, IL2CPP_RGCTX_DATA(method->rgctx_data, 1)));
	}
}
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
// T[] UnityEngine.Object::FindObjectsOfType<System.Object>()
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Object_FindObjectsOfType_TisIl2CppObject_m3888714627_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Object_FindObjectsOfType_TisIl2CppObject_m3888714627_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Object_FindObjectsOfType_TisIl2CppObject_m3888714627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t1864875887 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->rgctx_data, 0)), /*hidden argument*/NULL);
		ObjectU5BU5D_t3051965477* L_1 = Object_FindObjectsOfType_m975740280(NULL /*static, unused*/, (Type_t *)L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t3051965477*, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t3051965477*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		return L_2;
	}
}
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
// T[] UnityEngine.Resources::ConvertObjects<System.Object>(UnityEngine.Object[])
extern "C"  ObjectU5BU5D_t11523773* Resources_ConvertObjects_TisIl2CppObject_m1537961554_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t3051965477* ___rawObjects0, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t3051965477* L_0 = ___rawObjects0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (ObjectU5BU5D_t11523773*)NULL;
	}

IL_0008:
	{
		ObjectU5BU5D_t3051965477* L_1 = ___rawObjects0;
		NullCheck(L_1);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_002b;
	}

IL_0018:
	{
		ObjectU5BU5D_t11523773* L_2 = V_0;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t3051965477* L_4 = ___rawObjects0;
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (Il2CppObject *)((Il2CppObject *)Castclass(((L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6))), IL2CPP_RGCTX_DATA(method->rgctx_data, 1))));
		int32_t L_7 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_002b:
	{
		int32_t L_8 = V_1;
		ObjectU5BU5D_t11523773* L_9 = V_0;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_0018;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_10 = V_0;
		return L_10;
	}
}
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
// TOutput[] System.Array::ConvertAll<System.Object,System.Object>(TInput[],System.Converter`2<TInput,TOutput>)
extern Il2CppClass* ArgumentNullException_t3214793280_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral3945236896;
extern const uint32_t Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_MetadataUsageId;
extern "C"  ObjectU5BU5D_t11523773* Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* ___array0, Converter_2_t113996300 * ___converter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Array_ConvertAll_TisIl2CppObject_TisIl2CppObject_m2583007410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_1 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		Converter_2_t113996300 * L_2 = ___converter1;
		if (L_2)
		{
			goto IL_0022;
		}
	}
	{
		ArgumentNullException_t3214793280 * L_3 = (ArgumentNullException_t3214793280 *)il2cpp_codegen_object_new(ArgumentNullException_t3214793280_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, (String_t*)_stringLiteral3945236896, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		ObjectU5BU5D_t11523773* L_4 = ___array0;
		NullCheck(L_4);
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(method->rgctx_data, 0), (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))));
		V_1 = (int32_t)0;
		goto IL_004a;
	}

IL_0032:
	{
		ObjectU5BU5D_t11523773* L_5 = V_0;
		int32_t L_6 = V_1;
		Converter_2_t113996300 * L_7 = ___converter1;
		ObjectU5BU5D_t11523773* L_8 = ___array0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck((Converter_2_t113996300 *)L_7);
		Il2CppObject * L_11 = ((  Il2CppObject * (*) (Converter_2_t113996300 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1)->method)((Converter_2_t113996300 *)L_7, (Il2CppObject *)((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 1));
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_11);
		int32_t L_12 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_004a:
	{
		int32_t L_13 = V_1;
		ObjectU5BU5D_t11523773* L_14 = ___array0;
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_15 = V_0;
		return L_15;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::ExecuteHierarchy<System.Object>(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
extern Il2CppClass* ExecuteEvents_t4196265289_il2cpp_TypeInfo_var;
extern const uint32_t ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_MetadataUsageId;
extern "C"  GameObject_t4012695102 * ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___root0, BaseEventData_t3547103726 * ___eventData1, EventFunction_1_t3885370180 * ___callbackFunction2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_ExecuteHierarchy_TisIl2CppObject_m2445823293_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Transform_t284553113 * V_1 = NULL;
	{
		GameObject_t4012695102 * L_0 = ___root0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t4196265289_il2cpp_TypeInfo_var);
		List_1_t1081512082 * L_1 = ((ExecuteEvents_t4196265289_StaticFields*)ExecuteEvents_t4196265289_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		ExecuteEvents_GetEventChain_m1321751930(NULL /*static, unused*/, (GameObject_t4012695102 *)L_0, (Il2CppObject*)L_1, /*hidden argument*/NULL);
		V_0 = (int32_t)0;
		goto IL_003b;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t4196265289_il2cpp_TypeInfo_var);
		List_1_t1081512082 * L_2 = ((ExecuteEvents_t4196265289_StaticFields*)ExecuteEvents_t4196265289_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		int32_t L_3 = V_0;
		NullCheck((List_1_t1081512082 *)L_2);
		Transform_t284553113 * L_4 = VirtFuncInvoker1< Transform_t284553113 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, (List_1_t1081512082 *)L_2, (int32_t)L_3);
		V_1 = (Transform_t284553113 *)L_4;
		Transform_t284553113 * L_5 = V_1;
		NullCheck((Component_t2126946602 *)L_5);
		GameObject_t4012695102 * L_6 = Component_get_gameObject_m1170635899((Component_t2126946602 *)L_5, /*hidden argument*/NULL);
		BaseEventData_t3547103726 * L_7 = ___eventData1;
		EventFunction_1_t3885370180 * L_8 = ___callbackFunction2;
		bool L_9 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, BaseEventData_t3547103726 *, EventFunction_1_t3885370180 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (GameObject_t4012695102 *)L_6, (BaseEventData_t3547103726 *)L_7, (EventFunction_1_t3885370180 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		Transform_t284553113 * L_10 = V_1;
		NullCheck((Component_t2126946602 *)L_10);
		GameObject_t4012695102 * L_11 = Component_get_gameObject_m1170635899((Component_t2126946602 *)L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0037:
	{
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t4196265289_il2cpp_TypeInfo_var);
		List_1_t1081512082 * L_14 = ((ExecuteEvents_t4196265289_StaticFields*)ExecuteEvents_t4196265289_il2cpp_TypeInfo_var->static_fields)->get_s_InternalTransformList_18();
		NullCheck((List_1_t1081512082 *)L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, (List_1_t1081512082 *)L_14);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0012;
		}
	}
	{
		return (GameObject_t4012695102 *)NULL;
	}
}
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
// UnityEngine.GameObject UnityEngine.EventSystems.ExecuteEvents::GetEventHandler<System.Object>(UnityEngine.GameObject)
extern Il2CppClass* ExecuteEvents_t4196265289_il2cpp_TypeInfo_var;
extern const uint32_t ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_MetadataUsageId;
extern "C"  GameObject_t4012695102 * ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * ___root0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExecuteEvents_GetEventHandler_TisIl2CppObject_m2285942506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t284553113 * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = ___root0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, (Object_t3878351788 *)L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (GameObject_t4012695102 *)NULL;
	}

IL_000e:
	{
		GameObject_t4012695102 * L_2 = ___root0;
		NullCheck((GameObject_t4012695102 *)L_2);
		Transform_t284553113 * L_3 = GameObject_get_transform_m1278640159((GameObject_t4012695102 *)L_2, /*hidden argument*/NULL);
		V_0 = (Transform_t284553113 *)L_3;
		goto IL_0038;
	}

IL_001a:
	{
		Transform_t284553113 * L_4 = V_0;
		NullCheck((Component_t2126946602 *)L_4);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899((Component_t2126946602 *)L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(ExecuteEvents_t4196265289_il2cpp_TypeInfo_var);
		bool L_6 = ((  bool (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0)->method)(NULL /*static, unused*/, (GameObject_t4012695102 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->rgctx_data, 0));
		if (!L_6)
		{
			goto IL_0031;
		}
	}
	{
		Transform_t284553113 * L_7 = V_0;
		NullCheck((Component_t2126946602 *)L_7);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899((Component_t2126946602 *)L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0031:
	{
		Transform_t284553113 * L_9 = V_0;
		NullCheck((Transform_t284553113 *)L_9);
		Transform_t284553113 * L_10 = Transform_get_parent_m2236876972((Transform_t284553113 *)L_9, /*hidden argument*/NULL);
		V_0 = (Transform_t284553113 *)L_10;
	}

IL_0038:
	{
		Transform_t284553113 * L_11 = V_0;
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, (Object_t3878351788 *)L_11, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_001a;
		}
	}
	{
		return (GameObject_t4012695102 *)NULL;
	}
}
