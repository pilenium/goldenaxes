﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScaleAnimation
struct ScaleAnimation_t1299566970;

#include "codegen/il2cpp-codegen.h"

// System.Void ScaleAnimation::.ctor()
extern "C"  void ScaleAnimation__ctor_m3432711265 (ScaleAnimation_t1299566970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScaleAnimation::Awake()
extern "C"  void ScaleAnimation_Awake_m3670316484 (ScaleAnimation_t1299566970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScaleAnimation::Update()
extern "C"  void ScaleAnimation_Update_m766728908 (ScaleAnimation_t1299566970 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
