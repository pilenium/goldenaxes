﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ZoneCircle
struct ZoneCircle_t627455932;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void ZoneCircle::.ctor()
extern "C"  void ZoneCircle__ctor_m1449284703 (ZoneCircle_t627455932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoneCircle::Awake()
extern "C"  void ZoneCircle_Awake_m1686889922 (ZoneCircle_t627455932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ZoneCircle::Update()
extern "C"  void ZoneCircle_Update_m3705014926 (ZoneCircle_t627455932 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ZoneCircle::IsHit(UnityEngine.Vector3)
extern "C"  bool ZoneCircle_IsHit_m3374716039 (ZoneCircle_t627455932 * __this, Vector3_t3525329789  ___point0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
