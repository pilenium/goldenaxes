﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameUI
struct GameUI_t2125598246;

#include "codegen/il2cpp-codegen.h"

// System.Void GameUI::.ctor()
extern "C"  void GameUI__ctor_m2095833397 (GameUI_t2125598246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::Awake()
extern "C"  void GameUI_Awake_m2333438616 (GameUI_t2125598246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::SetNormal()
extern "C"  void GameUI_SetNormal_m1645742844 (GameUI_t2125598246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::SetFever()
extern "C"  void GameUI_SetFever_m705982769 (GameUI_t2125598246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::SetEvent()
extern "C"  void GameUI_SetEvent_m289744839 (GameUI_t2125598246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::SetBoss()
extern "C"  void GameUI_SetBoss_m3796739330 (GameUI_t2125598246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::SetCoin(System.Int32)
extern "C"  void GameUI_SetCoin_m1898449783 (GameUI_t2125598246 * __this, int32_t ___coin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::SetStage(System.Int32,System.Int32)
extern "C"  void GameUI_SetStage_m4145363547 (GameUI_t2125598246 * __this, int32_t ___chapter0, int32_t ___stage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::SetScore(System.Int32)
extern "C"  void GameUI_SetScore_m3619087504 (GameUI_t2125598246 * __this, int32_t ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::SetPower(System.Single)
extern "C"  void GameUI_SetPower_m2496519033 (GameUI_t2125598246 * __this, float ___power0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::SetDistance(System.Single)
extern "C"  void GameUI_SetDistance_m919038049 (GameUI_t2125598246 * __this, float ___dist0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameUI::Update()
extern "C"  void GameUI_Update_m2273187960 (GameUI_t2125598246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
