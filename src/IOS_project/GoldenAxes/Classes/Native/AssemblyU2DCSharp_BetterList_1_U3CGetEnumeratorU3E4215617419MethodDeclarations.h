﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator3<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator3_t4215617419;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m4146347777_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3__ctor_m4146347777(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3__ctor_m4146347777_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2593626376_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2593626376(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2593626376_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1784268677_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1784268677(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1784268677_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m2157268819_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m2157268819(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m2157268819_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3103813374_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3103813374(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3103813374_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1792780718_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Reset_m1792780718(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Reset_m1792780718_gshared)(__this, method)
