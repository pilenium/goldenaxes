﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIToggle/Validate
struct Validate_t2938338614;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UIToggle/Validate::.ctor(System.Object,System.IntPtr)
extern "C"  void Validate__ctor_m3672608180 (Validate_t2938338614 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle/Validate::Invoke(System.Boolean)
extern "C"  bool Validate_Invoke_m2875745233 (Validate_t2938338614 * __this, bool ___choice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" bool pinvoke_delegate_wrapper_Validate_t2938338614(Il2CppObject* delegate, bool ___choice0);
// System.IAsyncResult UIToggle/Validate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Validate_BeginInvoke_m1410260394 (Validate_t2938338614 * __this, bool ___choice0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle/Validate::EndInvoke(System.IAsyncResult)
extern "C"  bool Validate_EndInvoke_m4229339536 (Validate_t2938338614 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
