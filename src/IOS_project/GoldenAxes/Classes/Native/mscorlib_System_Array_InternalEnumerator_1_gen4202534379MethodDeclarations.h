﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4202534379.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"

// System.Void System.Array/InternalEnumerator`1<Console/Log>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m947749713_gshared (InternalEnumerator_1_t4202534379 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m947749713(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4202534379 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m947749713_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Console/Log>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3147343535_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3147343535(__this, method) ((  void (*) (InternalEnumerator_1_t4202534379 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3147343535_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Console/Log>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1615871077_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1615871077(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4202534379 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1615871077_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Console/Log>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3633761896_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3633761896(__this, method) ((  void (*) (InternalEnumerator_1_t4202534379 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3633761896_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Console/Log>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1622233311_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1622233311(__this, method) ((  bool (*) (InternalEnumerator_1_t4202534379 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1622233311_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Console/Log>::get_Current()
extern "C"  Log_t76580  InternalEnumerator_1_get_Current_m1121726266_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1121726266(__this, method) ((  Log_t76580  (*) (InternalEnumerator_1_t4202534379 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1121726266_gshared)(__this, method)
