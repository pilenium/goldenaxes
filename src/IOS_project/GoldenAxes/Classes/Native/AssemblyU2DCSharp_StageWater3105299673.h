﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.Renderer
struct Renderer_t1092684080;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// StageWater
struct  StageWater_t3105299673  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform StageWater::m_tr
	Transform_t284553113 * ___m_tr_2;
	// UnityEngine.Renderer StageWater::m_renderer
	Renderer_t1092684080 * ___m_renderer_3;
	// System.Single StageWater::m_width
	float ___m_width_4;
	// System.Single StageWater::m_height
	float ___m_height_5;
	// System.Single StageWater::m_tile_width
	float ___m_tile_width_6;
	// System.Single StageWater::m_speed
	float ___m_speed_7;
	// System.Single StageWater::m_offset_x
	float ___m_offset_x_8;
	// System.Single StageWater::m_offset_y
	float ___m_offset_y_9;

public:
	inline static int32_t get_offset_of_m_tr_2() { return static_cast<int32_t>(offsetof(StageWater_t3105299673, ___m_tr_2)); }
	inline Transform_t284553113 * get_m_tr_2() const { return ___m_tr_2; }
	inline Transform_t284553113 ** get_address_of_m_tr_2() { return &___m_tr_2; }
	inline void set_m_tr_2(Transform_t284553113 * value)
	{
		___m_tr_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_2, value);
	}

	inline static int32_t get_offset_of_m_renderer_3() { return static_cast<int32_t>(offsetof(StageWater_t3105299673, ___m_renderer_3)); }
	inline Renderer_t1092684080 * get_m_renderer_3() const { return ___m_renderer_3; }
	inline Renderer_t1092684080 ** get_address_of_m_renderer_3() { return &___m_renderer_3; }
	inline void set_m_renderer_3(Renderer_t1092684080 * value)
	{
		___m_renderer_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_renderer_3, value);
	}

	inline static int32_t get_offset_of_m_width_4() { return static_cast<int32_t>(offsetof(StageWater_t3105299673, ___m_width_4)); }
	inline float get_m_width_4() const { return ___m_width_4; }
	inline float* get_address_of_m_width_4() { return &___m_width_4; }
	inline void set_m_width_4(float value)
	{
		___m_width_4 = value;
	}

	inline static int32_t get_offset_of_m_height_5() { return static_cast<int32_t>(offsetof(StageWater_t3105299673, ___m_height_5)); }
	inline float get_m_height_5() const { return ___m_height_5; }
	inline float* get_address_of_m_height_5() { return &___m_height_5; }
	inline void set_m_height_5(float value)
	{
		___m_height_5 = value;
	}

	inline static int32_t get_offset_of_m_tile_width_6() { return static_cast<int32_t>(offsetof(StageWater_t3105299673, ___m_tile_width_6)); }
	inline float get_m_tile_width_6() const { return ___m_tile_width_6; }
	inline float* get_address_of_m_tile_width_6() { return &___m_tile_width_6; }
	inline void set_m_tile_width_6(float value)
	{
		___m_tile_width_6 = value;
	}

	inline static int32_t get_offset_of_m_speed_7() { return static_cast<int32_t>(offsetof(StageWater_t3105299673, ___m_speed_7)); }
	inline float get_m_speed_7() const { return ___m_speed_7; }
	inline float* get_address_of_m_speed_7() { return &___m_speed_7; }
	inline void set_m_speed_7(float value)
	{
		___m_speed_7 = value;
	}

	inline static int32_t get_offset_of_m_offset_x_8() { return static_cast<int32_t>(offsetof(StageWater_t3105299673, ___m_offset_x_8)); }
	inline float get_m_offset_x_8() const { return ___m_offset_x_8; }
	inline float* get_address_of_m_offset_x_8() { return &___m_offset_x_8; }
	inline void set_m_offset_x_8(float value)
	{
		___m_offset_x_8 = value;
	}

	inline static int32_t get_offset_of_m_offset_y_9() { return static_cast<int32_t>(offsetof(StageWater_t3105299673, ___m_offset_y_9)); }
	inline float get_m_offset_y_9() const { return ___m_offset_y_9; }
	inline float* get_address_of_m_offset_y_9() { return &___m_offset_y_9; }
	inline void set_m_offset_y_9(float value)
	{
		___m_offset_y_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
