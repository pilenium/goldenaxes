﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FeverLights/UpdateFunction
struct UpdateFunction_t1249117153;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void FeverLights/UpdateFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateFunction__ctor_m3574849846 (UpdateFunction_t1249117153 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights/UpdateFunction::Invoke()
extern "C"  void UpdateFunction_Invoke_m1968219408 (UpdateFunction_t1249117153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateFunction_t1249117153(Il2CppObject* delegate);
// System.IAsyncResult FeverLights/UpdateFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateFunction_BeginInvoke_m3437820659 (UpdateFunction_t1249117153 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights/UpdateFunction::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateFunction_EndInvoke_m1959026246 (UpdateFunction_t1249117153 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
