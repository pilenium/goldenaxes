﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UnityEngine.LogType>
struct EqualityComparer_1_t2963660101;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.LogType>::.ctor()
extern "C"  void EqualityComparer_1__ctor_m2310379622_gshared (EqualityComparer_1_t2963660101 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m2310379622(__this, method) ((  void (*) (EqualityComparer_1_t2963660101 *, const MethodInfo*))EqualityComparer_1__ctor_m2310379622_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.LogType>::.cctor()
extern "C"  void EqualityComparer_1__cctor_m2420195335_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m2420195335(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m2420195335_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.LogType>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C"  int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1642775615_gshared (EqualityComparer_1_t2963660101 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1642775615(__this, ___obj0, method) ((  int32_t (*) (EqualityComparer_1_t2963660101 *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m1642775615_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.LogType>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C"  bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2354260779_gshared (EqualityComparer_1_t2963660101 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2354260779(__this, ___x0, ___y1, method) ((  bool (*) (EqualityComparer_1_t2963660101 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m2354260779_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.LogType>::get_Default()
extern "C"  EqualityComparer_1_t2963660101 * EqualityComparer_1_get_Default_m4275852688_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m4275852688(__this /* static, unused */, method) ((  EqualityComparer_1_t2963660101 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m4275852688_gshared)(__this /* static, unused */, method)
