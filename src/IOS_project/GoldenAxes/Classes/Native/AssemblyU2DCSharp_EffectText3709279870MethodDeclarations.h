﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectText
struct EffectText_t3709279870;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectText::.ctor()
extern "C"  void EffectText__ctor_m1610344413 (EffectText_t3709279870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectText::Start()
extern "C"  void EffectText_Start_m557482205 (EffectText_t3709279870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectText::Update()
extern "C"  void EffectText_Update_m107931344 (EffectText_t3709279870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
