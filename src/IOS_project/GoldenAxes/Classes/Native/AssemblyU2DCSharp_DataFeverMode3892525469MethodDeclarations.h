﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataFeverMode
struct DataFeverMode_t3892525469;

#include "codegen/il2cpp-codegen.h"

// System.Void DataFeverMode::.ctor()
extern "C"  void DataFeverMode__ctor_m2102503534 (DataFeverMode_t3892525469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
