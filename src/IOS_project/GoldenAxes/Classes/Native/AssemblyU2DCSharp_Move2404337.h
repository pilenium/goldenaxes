﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Character
struct Character_t3568163593;

#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_Move_TYPE2590522.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Move
struct  Move_t2404337  : public Il2CppObject
{
public:
	// Move/TYPE Move::m_type
	int32_t ___m_type_0;
	// Character Move::m_character
	Character_t3568163593 * ___m_character_1;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(Move_t2404337, ___m_type_0)); }
	inline int32_t get_m_type_0() const { return ___m_type_0; }
	inline int32_t* get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(int32_t value)
	{
		___m_type_0 = value;
	}

	inline static int32_t get_offset_of_m_character_1() { return static_cast<int32_t>(offsetof(Move_t2404337, ___m_character_1)); }
	inline Character_t3568163593 * get_m_character_1() const { return ___m_character_1; }
	inline Character_t3568163593 ** get_address_of_m_character_1() { return &___m_character_1; }
	inline void set_m_character_1(Character_t3568163593 * value)
	{
		___m_character_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_character_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
