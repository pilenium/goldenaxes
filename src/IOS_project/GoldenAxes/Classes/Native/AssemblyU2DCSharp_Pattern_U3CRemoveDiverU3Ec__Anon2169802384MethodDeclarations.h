﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pattern/<RemoveDiver>c__AnonStorey7
struct U3CRemoveDiverU3Ec__AnonStorey7_t2169802384;
// Diver
struct Diver_t66044126;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Diver66044126.h"

// System.Void Pattern/<RemoveDiver>c__AnonStorey7::.ctor()
extern "C"  void U3CRemoveDiverU3Ec__AnonStorey7__ctor_m2476658202 (U3CRemoveDiverU3Ec__AnonStorey7_t2169802384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pattern/<RemoveDiver>c__AnonStorey7::<>m__8(Diver)
extern "C"  bool U3CRemoveDiverU3Ec__AnonStorey7_U3CU3Em__8_m316660753 (U3CRemoveDiverU3Ec__AnonStorey7_t2169802384 * __this, Diver_t66044126 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
