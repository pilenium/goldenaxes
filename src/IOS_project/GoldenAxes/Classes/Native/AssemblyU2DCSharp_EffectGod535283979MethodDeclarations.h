﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectGod
struct EffectGod_t535283979;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectGod::.ctor()
extern "C"  void EffectGod__ctor_m2047690944 (EffectGod_t535283979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectGod::Awake()
extern "C"  void EffectGod_Awake_m2285296163 (EffectGod_t535283979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectGod::Update()
extern "C"  void EffectGod_Update_m780771917 (EffectGod_t535283979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
