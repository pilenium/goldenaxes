﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<DataPattern>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m672139085(__this, ___l0, method) ((  void (*) (Enumerator_t2573281071 *, List_1_t192530783 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataPattern>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m25227493(__this, method) ((  void (*) (Enumerator_t2573281071 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DataPattern>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1920041233(__this, method) ((  Il2CppObject * (*) (Enumerator_t2573281071 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataPattern>::Dispose()
#define Enumerator_Dispose_m2519038066(__this, method) ((  void (*) (Enumerator_t2573281071 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataPattern>::VerifyState()
#define Enumerator_VerifyState_m103001771(__this, method) ((  void (*) (Enumerator_t2573281071 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DataPattern>::MoveNext()
#define Enumerator_MoveNext_m2234898248(__this, method) ((  bool (*) (Enumerator_t2573281071 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DataPattern>::get_Current()
#define Enumerator_get_Current_m3978161224(__this, method) ((  DataPattern_t3690539110 * (*) (Enumerator_t2573281071 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
