﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>
struct ValueCollection_t2803853901;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t881716807;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color>
struct IEnumerator_1_t3071282208;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Object
struct Il2CppObject;
// UnityEngine.Color[]
struct ColorU5BU5D_t3477081137;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_Array2840145358.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va648744748.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1921072305_gshared (ValueCollection_t2803853901 * __this, Dictionary_2_t881716807 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1921072305(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2803853901 *, Dictionary_2_t881716807 *, const MethodInfo*))ValueCollection__ctor_m1921072305_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1039803745_gshared (ValueCollection_t2803853901 * __this, Color_t1588175760  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1039803745(__this, ___item0, method) ((  void (*) (ValueCollection_t2803853901 *, Color_t1588175760 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1039803745_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2538859178_gshared (ValueCollection_t2803853901 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2538859178(__this, method) ((  void (*) (ValueCollection_t2803853901 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2538859178_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3964298725_gshared (ValueCollection_t2803853901 * __this, Color_t1588175760  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3964298725(__this, ___item0, method) ((  bool (*) (ValueCollection_t2803853901 *, Color_t1588175760 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3964298725_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m635286474_gshared (ValueCollection_t2803853901 * __this, Color_t1588175760  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m635286474(__this, ___item0, method) ((  bool (*) (ValueCollection_t2803853901 *, Color_t1588175760 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m635286474_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3318039800_gshared (ValueCollection_t2803853901 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3318039800(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2803853901 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3318039800_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m51167022_gshared (ValueCollection_t2803853901 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m51167022(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2803853901 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m51167022_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4066282793_gshared (ValueCollection_t2803853901 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4066282793(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2803853901 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4066282793_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2239474584_gshared (ValueCollection_t2803853901 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2239474584(__this, method) ((  bool (*) (ValueCollection_t2803853901 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2239474584_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m372544888_gshared (ValueCollection_t2803853901 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m372544888(__this, method) ((  bool (*) (ValueCollection_t2803853901 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m372544888_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m4099260068_gshared (ValueCollection_t2803853901 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4099260068(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2803853901 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m4099260068_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2668207288_gshared (ValueCollection_t2803853901 * __this, ColorU5BU5D_t3477081137* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2668207288(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2803853901 *, ColorU5BU5D_t3477081137*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2668207288_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::GetEnumerator()
extern "C"  Enumerator_t648744748  ValueCollection_GetEnumerator_m882358107_gshared (ValueCollection_t2803853901 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m882358107(__this, method) ((  Enumerator_t648744748  (*) (ValueCollection_t2803853901 *, const MethodInfo*))ValueCollection_GetEnumerator_m882358107_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3664685630_gshared (ValueCollection_t2803853901 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3664685630(__this, method) ((  int32_t (*) (ValueCollection_t2803853901 *, const MethodInfo*))ValueCollection_get_Count_m3664685630_gshared)(__this, method)
