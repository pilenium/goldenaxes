﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<Monster>
struct List_1_t3698229427;
// System.Collections.Generic.List`1<Diver>
struct List_1_t863003095;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pattern
struct  Pattern_t873562992  : public Il2CppObject
{
public:
	// System.String Pattern::m_type
	String_t* ___m_type_0;
	// System.Int32 Pattern::m_count
	int32_t ___m_count_1;
	// System.Boolean Pattern::m_isMonster
	bool ___m_isMonster_2;
	// System.Single Pattern::m_height
	float ___m_height_3;
	// System.Collections.Generic.List`1<Monster> Pattern::m_monsters
	List_1_t3698229427 * ___m_monsters_4;
	// System.Collections.Generic.List`1<Diver> Pattern::m_divers
	List_1_t863003095 * ___m_divers_5;
	// System.Boolean Pattern::m_isSuccess
	bool ___m_isSuccess_6;

public:
	inline static int32_t get_offset_of_m_type_0() { return static_cast<int32_t>(offsetof(Pattern_t873562992, ___m_type_0)); }
	inline String_t* get_m_type_0() const { return ___m_type_0; }
	inline String_t** get_address_of_m_type_0() { return &___m_type_0; }
	inline void set_m_type_0(String_t* value)
	{
		___m_type_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_type_0, value);
	}

	inline static int32_t get_offset_of_m_count_1() { return static_cast<int32_t>(offsetof(Pattern_t873562992, ___m_count_1)); }
	inline int32_t get_m_count_1() const { return ___m_count_1; }
	inline int32_t* get_address_of_m_count_1() { return &___m_count_1; }
	inline void set_m_count_1(int32_t value)
	{
		___m_count_1 = value;
	}

	inline static int32_t get_offset_of_m_isMonster_2() { return static_cast<int32_t>(offsetof(Pattern_t873562992, ___m_isMonster_2)); }
	inline bool get_m_isMonster_2() const { return ___m_isMonster_2; }
	inline bool* get_address_of_m_isMonster_2() { return &___m_isMonster_2; }
	inline void set_m_isMonster_2(bool value)
	{
		___m_isMonster_2 = value;
	}

	inline static int32_t get_offset_of_m_height_3() { return static_cast<int32_t>(offsetof(Pattern_t873562992, ___m_height_3)); }
	inline float get_m_height_3() const { return ___m_height_3; }
	inline float* get_address_of_m_height_3() { return &___m_height_3; }
	inline void set_m_height_3(float value)
	{
		___m_height_3 = value;
	}

	inline static int32_t get_offset_of_m_monsters_4() { return static_cast<int32_t>(offsetof(Pattern_t873562992, ___m_monsters_4)); }
	inline List_1_t3698229427 * get_m_monsters_4() const { return ___m_monsters_4; }
	inline List_1_t3698229427 ** get_address_of_m_monsters_4() { return &___m_monsters_4; }
	inline void set_m_monsters_4(List_1_t3698229427 * value)
	{
		___m_monsters_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_monsters_4, value);
	}

	inline static int32_t get_offset_of_m_divers_5() { return static_cast<int32_t>(offsetof(Pattern_t873562992, ___m_divers_5)); }
	inline List_1_t863003095 * get_m_divers_5() const { return ___m_divers_5; }
	inline List_1_t863003095 ** get_address_of_m_divers_5() { return &___m_divers_5; }
	inline void set_m_divers_5(List_1_t863003095 * value)
	{
		___m_divers_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_divers_5, value);
	}

	inline static int32_t get_offset_of_m_isSuccess_6() { return static_cast<int32_t>(offsetof(Pattern_t873562992, ___m_isSuccess_6)); }
	inline bool get_m_isSuccess_6() const { return ___m_isSuccess_6; }
	inline bool* get_address_of_m_isSuccess_6() { return &___m_isSuccess_6; }
	inline void set_m_isSuccess_6(bool value)
	{
		___m_isSuccess_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
