﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataNormalMode
struct DataNormalMode_t1907108500;
// DataFeverMode
struct DataFeverMode_t3892525469;
// DataBossMode
struct DataBossMode_t3617900698;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataStage
struct  DataStage_t1629502804  : public Il2CppObject
{
public:
	// System.Int32 DataStage::stage
	int32_t ___stage_0;
	// DataNormalMode DataStage::normal
	DataNormalMode_t1907108500 * ___normal_1;
	// DataFeverMode DataStage::fever
	DataFeverMode_t3892525469 * ___fever_2;
	// DataBossMode DataStage::boss
	DataBossMode_t3617900698 * ___boss_3;

public:
	inline static int32_t get_offset_of_stage_0() { return static_cast<int32_t>(offsetof(DataStage_t1629502804, ___stage_0)); }
	inline int32_t get_stage_0() const { return ___stage_0; }
	inline int32_t* get_address_of_stage_0() { return &___stage_0; }
	inline void set_stage_0(int32_t value)
	{
		___stage_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(DataStage_t1629502804, ___normal_1)); }
	inline DataNormalMode_t1907108500 * get_normal_1() const { return ___normal_1; }
	inline DataNormalMode_t1907108500 ** get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(DataNormalMode_t1907108500 * value)
	{
		___normal_1 = value;
		Il2CppCodeGenWriteBarrier(&___normal_1, value);
	}

	inline static int32_t get_offset_of_fever_2() { return static_cast<int32_t>(offsetof(DataStage_t1629502804, ___fever_2)); }
	inline DataFeverMode_t3892525469 * get_fever_2() const { return ___fever_2; }
	inline DataFeverMode_t3892525469 ** get_address_of_fever_2() { return &___fever_2; }
	inline void set_fever_2(DataFeverMode_t3892525469 * value)
	{
		___fever_2 = value;
		Il2CppCodeGenWriteBarrier(&___fever_2, value);
	}

	inline static int32_t get_offset_of_boss_3() { return static_cast<int32_t>(offsetof(DataStage_t1629502804, ___boss_3)); }
	inline DataBossMode_t3617900698 * get_boss_3() const { return ___boss_3; }
	inline DataBossMode_t3617900698 ** get_address_of_boss_3() { return &___boss_3; }
	inline void set_boss_3(DataBossMode_t3617900698 * value)
	{
		___boss_3 = value;
		Il2CppCodeGenWriteBarrier(&___boss_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
