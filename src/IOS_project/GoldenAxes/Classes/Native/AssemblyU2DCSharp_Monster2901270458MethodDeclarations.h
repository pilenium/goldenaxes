﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Monster
struct Monster_t2901270458;
// DataMonster
struct DataMonster_t1423279280;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DataMonster1423279280.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharp_MONSTER_STATE3983724460.h"

// System.Void Monster::.ctor()
extern "C"  void Monster__ctor_m3066192113 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster Monster::Alloc(System.UInt32)
extern "C"  Monster_t2901270458 * Monster_Alloc_m1931690365 (Il2CppObject * __this /* static, unused */, uint32_t ___monster_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataMonster Monster::get_data()
extern "C"  DataMonster_t1423279280 * Monster_get_data_m1434582403 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::get_health()
extern "C"  int32_t Monster_get_health_m449321322 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Awake()
extern "C"  void Monster_Awake_m3303797332 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnDestroy()
extern "C"  void Monster_OnDestroy_m1806194026 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Update()
extern "C"  void Monster_Update_m2289537084 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Init(DataMonster)
extern "C"  void Monster_Init_m2722476575 (Monster_t2901270458 * __this, DataMonster_t1423279280 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::InitMonster(DataMonster)
extern "C"  void Monster_InitMonster_m4083682793 (Monster_t2901270458 * __this, DataMonster_t1423279280 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::InitPosition(DataMonster)
extern "C"  void Monster_InitPosition_m1177351542 (Monster_t2901270458 * __this, DataMonster_t1423279280 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::InitCloud(DataMonster)
extern "C"  void Monster_InitCloud_m1611133934 (Monster_t2901270458 * __this, DataMonster_t1423279280 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::InitProgress(DataMonster)
extern "C"  void Monster_InitProgress_m3865875186 (Monster_t2901270458 * __this, DataMonster_t1423279280 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnEndMove()
extern "C"  void Monster_OnEndMove_m1252359228 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::UpdateMove(UnityEngine.Vector3)
extern "C"  void Monster_UpdateMove_m4254352460 (Monster_t2901270458 * __this, Vector3_t3525329789  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::SetState(MONSTER_STATE)
extern "C"  void Monster_SetState_m3519019012 (Monster_t2901270458 * __this, int32_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Hit(System.Int32)
extern "C"  void Monster_Hit_m2005786291 (Monster_t2901270458 * __this, int32_t ___damage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
