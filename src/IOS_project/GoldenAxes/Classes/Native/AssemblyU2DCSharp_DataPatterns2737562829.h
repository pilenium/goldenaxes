﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<DataPattern>
struct List_1_t192530783;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataPatterns
struct  DataPatterns_t2737562829  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<DataPattern> DataPatterns::patterns
	List_1_t192530783 * ___patterns_0;

public:
	inline static int32_t get_offset_of_patterns_0() { return static_cast<int32_t>(offsetof(DataPatterns_t2737562829, ___patterns_0)); }
	inline List_1_t192530783 * get_patterns_0() const { return ___patterns_0; }
	inline List_1_t192530783 ** get_address_of_patterns_0() { return &___patterns_0; }
	inline void set_patterns_0(List_1_t192530783 * value)
	{
		___patterns_0 = value;
		Il2CppCodeGenWriteBarrier(&___patterns_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
