﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataUser
struct  DataUser_t1853738677  : public Il2CppObject
{
public:
	// System.String DataUser::m_id
	String_t* ___m_id_0;
	// System.Int32 DataUser::m_coin
	int32_t ___m_coin_1;
	// System.Int32 DataUser::m_diamond
	int32_t ___m_diamond_2;
	// System.Int32 DataUser::m_chapter
	int32_t ___m_chapter_3;
	// System.Int32 DataUser::m_stage
	int32_t ___m_stage_4;

public:
	inline static int32_t get_offset_of_m_id_0() { return static_cast<int32_t>(offsetof(DataUser_t1853738677, ___m_id_0)); }
	inline String_t* get_m_id_0() const { return ___m_id_0; }
	inline String_t** get_address_of_m_id_0() { return &___m_id_0; }
	inline void set_m_id_0(String_t* value)
	{
		___m_id_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_id_0, value);
	}

	inline static int32_t get_offset_of_m_coin_1() { return static_cast<int32_t>(offsetof(DataUser_t1853738677, ___m_coin_1)); }
	inline int32_t get_m_coin_1() const { return ___m_coin_1; }
	inline int32_t* get_address_of_m_coin_1() { return &___m_coin_1; }
	inline void set_m_coin_1(int32_t value)
	{
		___m_coin_1 = value;
	}

	inline static int32_t get_offset_of_m_diamond_2() { return static_cast<int32_t>(offsetof(DataUser_t1853738677, ___m_diamond_2)); }
	inline int32_t get_m_diamond_2() const { return ___m_diamond_2; }
	inline int32_t* get_address_of_m_diamond_2() { return &___m_diamond_2; }
	inline void set_m_diamond_2(int32_t value)
	{
		___m_diamond_2 = value;
	}

	inline static int32_t get_offset_of_m_chapter_3() { return static_cast<int32_t>(offsetof(DataUser_t1853738677, ___m_chapter_3)); }
	inline int32_t get_m_chapter_3() const { return ___m_chapter_3; }
	inline int32_t* get_address_of_m_chapter_3() { return &___m_chapter_3; }
	inline void set_m_chapter_3(int32_t value)
	{
		___m_chapter_3 = value;
	}

	inline static int32_t get_offset_of_m_stage_4() { return static_cast<int32_t>(offsetof(DataUser_t1853738677, ___m_stage_4)); }
	inline int32_t get_m_stage_4() const { return ___m_stage_4; }
	inline int32_t* get_address_of_m_stage_4() { return &___m_stage_4; }
	inline void set_m_stage_4(int32_t value)
	{
		___m_stage_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
