﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataPattern
struct DataPattern_t3690539110;

#include "codegen/il2cpp-codegen.h"

// System.Void DataPattern::.ctor()
extern "C"  void DataPattern__ctor_m2923587013 (DataPattern_t3690539110 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
