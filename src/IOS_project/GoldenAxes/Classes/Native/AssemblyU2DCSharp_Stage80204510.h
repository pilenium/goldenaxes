﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StageWater
struct StageWater_t3105299673;
// StageWaterSuface
struct StageWaterSuface_t3854378488;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t514686775;
// System.Collections.Generic.List`1<Axe>
struct List_1_t797025255;
// DataChapter
struct DataChapter_t925677859;
// DataStage
struct DataStage_t1629502804;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Stage
struct  Stage_t80204510  : public MonoBehaviour_t3012272455
{
public:
	// StageWater Stage::m_water
	StageWater_t3105299673 * ___m_water_2;
	// StageWaterSuface Stage::m_waterSurface
	StageWaterSuface_t3854378488 * ___m_waterSurface_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Stage::m_tiles
	List_1_t514686775 * ___m_tiles_4;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Stage::m_removingTiles
	List_1_t514686775 * ___m_removingTiles_5;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Stage::m_effects
	List_1_t514686775 * ___m_effects_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Stage::m_removingEffects
	List_1_t514686775 * ___m_removingEffects_7;
	// System.Collections.Generic.List`1<Axe> Stage::m_axes
	List_1_t797025255 * ___m_axes_8;
	// System.Collections.Generic.List`1<Axe> Stage::m_removingAxes
	List_1_t797025255 * ___m_removingAxes_9;
	// DataChapter Stage::m_chapterData
	DataChapter_t925677859 * ___m_chapterData_10;
	// DataStage Stage::m_stageData
	DataStage_t1629502804 * ___m_stageData_11;
	// System.Int32 Stage::m_chapter
	int32_t ___m_chapter_12;
	// System.Int32 Stage::m_stage
	int32_t ___m_stage_13;
	// System.Single Stage::m_speed
	float ___m_speed_14;
	// System.Single Stage::m_target_speed
	float ___m_target_speed_15;
	// System.Single Stage::m_distance
	float ___m_distance_16;
	// System.Single Stage::m_delta_dist
	float ___m_delta_dist_17;
	// System.Single Stage::m_tileHeight
	float ___m_tileHeight_18;

public:
	inline static int32_t get_offset_of_m_water_2() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_water_2)); }
	inline StageWater_t3105299673 * get_m_water_2() const { return ___m_water_2; }
	inline StageWater_t3105299673 ** get_address_of_m_water_2() { return &___m_water_2; }
	inline void set_m_water_2(StageWater_t3105299673 * value)
	{
		___m_water_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_water_2, value);
	}

	inline static int32_t get_offset_of_m_waterSurface_3() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_waterSurface_3)); }
	inline StageWaterSuface_t3854378488 * get_m_waterSurface_3() const { return ___m_waterSurface_3; }
	inline StageWaterSuface_t3854378488 ** get_address_of_m_waterSurface_3() { return &___m_waterSurface_3; }
	inline void set_m_waterSurface_3(StageWaterSuface_t3854378488 * value)
	{
		___m_waterSurface_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_waterSurface_3, value);
	}

	inline static int32_t get_offset_of_m_tiles_4() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_tiles_4)); }
	inline List_1_t514686775 * get_m_tiles_4() const { return ___m_tiles_4; }
	inline List_1_t514686775 ** get_address_of_m_tiles_4() { return &___m_tiles_4; }
	inline void set_m_tiles_4(List_1_t514686775 * value)
	{
		___m_tiles_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tiles_4, value);
	}

	inline static int32_t get_offset_of_m_removingTiles_5() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_removingTiles_5)); }
	inline List_1_t514686775 * get_m_removingTiles_5() const { return ___m_removingTiles_5; }
	inline List_1_t514686775 ** get_address_of_m_removingTiles_5() { return &___m_removingTiles_5; }
	inline void set_m_removingTiles_5(List_1_t514686775 * value)
	{
		___m_removingTiles_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_removingTiles_5, value);
	}

	inline static int32_t get_offset_of_m_effects_6() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_effects_6)); }
	inline List_1_t514686775 * get_m_effects_6() const { return ___m_effects_6; }
	inline List_1_t514686775 ** get_address_of_m_effects_6() { return &___m_effects_6; }
	inline void set_m_effects_6(List_1_t514686775 * value)
	{
		___m_effects_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_effects_6, value);
	}

	inline static int32_t get_offset_of_m_removingEffects_7() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_removingEffects_7)); }
	inline List_1_t514686775 * get_m_removingEffects_7() const { return ___m_removingEffects_7; }
	inline List_1_t514686775 ** get_address_of_m_removingEffects_7() { return &___m_removingEffects_7; }
	inline void set_m_removingEffects_7(List_1_t514686775 * value)
	{
		___m_removingEffects_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_removingEffects_7, value);
	}

	inline static int32_t get_offset_of_m_axes_8() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_axes_8)); }
	inline List_1_t797025255 * get_m_axes_8() const { return ___m_axes_8; }
	inline List_1_t797025255 ** get_address_of_m_axes_8() { return &___m_axes_8; }
	inline void set_m_axes_8(List_1_t797025255 * value)
	{
		___m_axes_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_axes_8, value);
	}

	inline static int32_t get_offset_of_m_removingAxes_9() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_removingAxes_9)); }
	inline List_1_t797025255 * get_m_removingAxes_9() const { return ___m_removingAxes_9; }
	inline List_1_t797025255 ** get_address_of_m_removingAxes_9() { return &___m_removingAxes_9; }
	inline void set_m_removingAxes_9(List_1_t797025255 * value)
	{
		___m_removingAxes_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_removingAxes_9, value);
	}

	inline static int32_t get_offset_of_m_chapterData_10() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_chapterData_10)); }
	inline DataChapter_t925677859 * get_m_chapterData_10() const { return ___m_chapterData_10; }
	inline DataChapter_t925677859 ** get_address_of_m_chapterData_10() { return &___m_chapterData_10; }
	inline void set_m_chapterData_10(DataChapter_t925677859 * value)
	{
		___m_chapterData_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_chapterData_10, value);
	}

	inline static int32_t get_offset_of_m_stageData_11() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_stageData_11)); }
	inline DataStage_t1629502804 * get_m_stageData_11() const { return ___m_stageData_11; }
	inline DataStage_t1629502804 ** get_address_of_m_stageData_11() { return &___m_stageData_11; }
	inline void set_m_stageData_11(DataStage_t1629502804 * value)
	{
		___m_stageData_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_stageData_11, value);
	}

	inline static int32_t get_offset_of_m_chapter_12() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_chapter_12)); }
	inline int32_t get_m_chapter_12() const { return ___m_chapter_12; }
	inline int32_t* get_address_of_m_chapter_12() { return &___m_chapter_12; }
	inline void set_m_chapter_12(int32_t value)
	{
		___m_chapter_12 = value;
	}

	inline static int32_t get_offset_of_m_stage_13() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_stage_13)); }
	inline int32_t get_m_stage_13() const { return ___m_stage_13; }
	inline int32_t* get_address_of_m_stage_13() { return &___m_stage_13; }
	inline void set_m_stage_13(int32_t value)
	{
		___m_stage_13 = value;
	}

	inline static int32_t get_offset_of_m_speed_14() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_speed_14)); }
	inline float get_m_speed_14() const { return ___m_speed_14; }
	inline float* get_address_of_m_speed_14() { return &___m_speed_14; }
	inline void set_m_speed_14(float value)
	{
		___m_speed_14 = value;
	}

	inline static int32_t get_offset_of_m_target_speed_15() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_target_speed_15)); }
	inline float get_m_target_speed_15() const { return ___m_target_speed_15; }
	inline float* get_address_of_m_target_speed_15() { return &___m_target_speed_15; }
	inline void set_m_target_speed_15(float value)
	{
		___m_target_speed_15 = value;
	}

	inline static int32_t get_offset_of_m_distance_16() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_distance_16)); }
	inline float get_m_distance_16() const { return ___m_distance_16; }
	inline float* get_address_of_m_distance_16() { return &___m_distance_16; }
	inline void set_m_distance_16(float value)
	{
		___m_distance_16 = value;
	}

	inline static int32_t get_offset_of_m_delta_dist_17() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_delta_dist_17)); }
	inline float get_m_delta_dist_17() const { return ___m_delta_dist_17; }
	inline float* get_address_of_m_delta_dist_17() { return &___m_delta_dist_17; }
	inline void set_m_delta_dist_17(float value)
	{
		___m_delta_dist_17 = value;
	}

	inline static int32_t get_offset_of_m_tileHeight_18() { return static_cast<int32_t>(offsetof(Stage_t80204510, ___m_tileHeight_18)); }
	inline float get_m_tileHeight_18() const { return ___m_tileHeight_18; }
	inline float* get_address_of_m_tileHeight_18() { return &___m_tileHeight_18; }
	inline void set_m_tileHeight_18(float value)
	{
		___m_tileHeight_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
