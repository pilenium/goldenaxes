﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Console
struct Console_t2616163639;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"

// System.Void Console::.ctor()
extern "C"  void Console__ctor_m1797103636 (Console_t2616163639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::.cctor()
extern "C"  void Console__cctor_m3688508953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::OnEnable()
extern "C"  void Console_OnEnable_m2818653938 (Console_t2616163639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::OnDisable()
extern "C"  void Console_OnDisable_m1919863419 (Console_t2616163639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::Update()
extern "C"  void Console_Update_m1602499961 (Console_t2616163639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::OnGUI()
extern "C"  void Console_OnGUI_m1292502286 (Console_t2616163639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::ConsoleWindow(System.Int32)
extern "C"  void Console_ConsoleWindow_m4108353130 (Console_t2616163639 * __this, int32_t ___windowID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::HandleLog(System.String,System.String,UnityEngine.LogType)
extern "C"  void Console_HandleLog_m3608954709 (Console_t2616163639 * __this, String_t* ___message0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Console::ExcuteCommand()
extern "C"  void Console_ExcuteCommand_m145081287 (Console_t2616163639 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
