﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveRotateCenter
struct MoveRotateCenter_t1395296289;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void MoveRotateCenter::.ctor(System.String,System.Single,System.Single,System.Single)
extern "C"  void MoveRotateCenter__ctor_m3529741847 (MoveRotateCenter_t1395296289 * __this, String_t* ___direction0, float ___speed1, float ___loop2, float ___size3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateCenter::Start()
extern "C"  void MoveRotateCenter_Start_m3342453914 (MoveRotateCenter_t1395296289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateCenter::Update()
extern "C"  void MoveRotateCenter_Update_m542708403 (MoveRotateCenter_t1395296289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateCenter::InitAppear()
extern "C"  void MoveRotateCenter_InitAppear_m2787221807 (MoveRotateCenter_t1395296289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateCenter::InitRotate()
extern "C"  void MoveRotateCenter_InitRotate_m1574098805 (MoveRotateCenter_t1395296289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateCenter::InitDisappear()
extern "C"  void MoveRotateCenter_InitDisappear_m721632619 (MoveRotateCenter_t1395296289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateCenter::UpdateAppear()
extern "C"  void MoveRotateCenter_UpdateAppear_m2087888296 (MoveRotateCenter_t1395296289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateCenter::UpdateRotate()
extern "C"  void MoveRotateCenter_UpdateRotate_m874765294 (MoveRotateCenter_t1395296289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveRotateCenter::UpdateDissapear()
extern "C"  void MoveRotateCenter_UpdateDissapear_m2641520945 (MoveRotateCenter_t1395296289 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
