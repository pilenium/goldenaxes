﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveWait
struct MoveWait_t4255274246;

#include "codegen/il2cpp-codegen.h"

// System.Void MoveWait::.ctor(System.Single)
extern "C"  void MoveWait__ctor_m1425949558 (MoveWait_t4255274246 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveWait::Update()
extern "C"  void MoveWait_Update_m3059559768 (MoveWait_t4255274246 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
