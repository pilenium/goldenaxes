﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Move
struct Move_t2404337;
// Character
struct Character_t3568163593;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Move_TYPE2590522.h"
#include "AssemblyU2DCSharp_Character3568163593.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void Move::.ctor()
extern "C"  void Move__ctor_m324221386 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Move/TYPE Move::get_type()
extern "C"  int32_t Move_get_type_m3871643672 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Init()
extern "C"  void Move_Init_m2871489354 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::SetCharacter(Character)
extern "C"  void Move_SetCharacter_m1420373256 (Move_t2404337 * __this, Character_t3568163593 * ___character0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Clear()
extern "C"  void Move_Clear_m2025321973 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Start()
extern "C"  void Move_Start_m3566326474 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::Update()
extern "C"  void Move_Update_m3187790467 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::End()
extern "C"  void Move_End_m3829705411 (Move_t2404337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Move::UpdateMove(UnityEngine.Vector3)
extern "C"  void Move_UpdateMove_m2014208549 (Move_t2404337 * __this, Vector3_t3525329789  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
