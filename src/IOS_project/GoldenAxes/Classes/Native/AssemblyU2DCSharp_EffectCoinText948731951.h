﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.TextMesh
struct TextMesh_t583678247;
// UnityEngine.Renderer
struct Renderer_t1092684080;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectCoinText
struct  EffectCoinText_t948731951  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Vector3 EffectCoinText::m_targetPos
	Vector3_t3525329789  ___m_targetPos_2;
	// UnityEngine.Color EffectCoinText::m_targetColor
	Color_t1588175760  ___m_targetColor_3;
	// UnityEngine.Transform EffectCoinText::m_tr
	Transform_t284553113 * ___m_tr_4;
	// UnityEngine.TextMesh EffectCoinText::m_textMesh
	TextMesh_t583678247 * ___m_textMesh_5;
	// UnityEngine.Renderer EffectCoinText::m_textRenderer
	Renderer_t1092684080 * ___m_textRenderer_6;
	// System.Int32 EffectCoinText::m_phase
	int32_t ___m_phase_7;
	// System.Single EffectCoinText::m_moveSpeed
	float ___m_moveSpeed_8;
	// System.Single EffectCoinText::m_fadeSpeed
	float ___m_fadeSpeed_9;

public:
	inline static int32_t get_offset_of_m_targetPos_2() { return static_cast<int32_t>(offsetof(EffectCoinText_t948731951, ___m_targetPos_2)); }
	inline Vector3_t3525329789  get_m_targetPos_2() const { return ___m_targetPos_2; }
	inline Vector3_t3525329789 * get_address_of_m_targetPos_2() { return &___m_targetPos_2; }
	inline void set_m_targetPos_2(Vector3_t3525329789  value)
	{
		___m_targetPos_2 = value;
	}

	inline static int32_t get_offset_of_m_targetColor_3() { return static_cast<int32_t>(offsetof(EffectCoinText_t948731951, ___m_targetColor_3)); }
	inline Color_t1588175760  get_m_targetColor_3() const { return ___m_targetColor_3; }
	inline Color_t1588175760 * get_address_of_m_targetColor_3() { return &___m_targetColor_3; }
	inline void set_m_targetColor_3(Color_t1588175760  value)
	{
		___m_targetColor_3 = value;
	}

	inline static int32_t get_offset_of_m_tr_4() { return static_cast<int32_t>(offsetof(EffectCoinText_t948731951, ___m_tr_4)); }
	inline Transform_t284553113 * get_m_tr_4() const { return ___m_tr_4; }
	inline Transform_t284553113 ** get_address_of_m_tr_4() { return &___m_tr_4; }
	inline void set_m_tr_4(Transform_t284553113 * value)
	{
		___m_tr_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_4, value);
	}

	inline static int32_t get_offset_of_m_textMesh_5() { return static_cast<int32_t>(offsetof(EffectCoinText_t948731951, ___m_textMesh_5)); }
	inline TextMesh_t583678247 * get_m_textMesh_5() const { return ___m_textMesh_5; }
	inline TextMesh_t583678247 ** get_address_of_m_textMesh_5() { return &___m_textMesh_5; }
	inline void set_m_textMesh_5(TextMesh_t583678247 * value)
	{
		___m_textMesh_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_textMesh_5, value);
	}

	inline static int32_t get_offset_of_m_textRenderer_6() { return static_cast<int32_t>(offsetof(EffectCoinText_t948731951, ___m_textRenderer_6)); }
	inline Renderer_t1092684080 * get_m_textRenderer_6() const { return ___m_textRenderer_6; }
	inline Renderer_t1092684080 ** get_address_of_m_textRenderer_6() { return &___m_textRenderer_6; }
	inline void set_m_textRenderer_6(Renderer_t1092684080 * value)
	{
		___m_textRenderer_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_textRenderer_6, value);
	}

	inline static int32_t get_offset_of_m_phase_7() { return static_cast<int32_t>(offsetof(EffectCoinText_t948731951, ___m_phase_7)); }
	inline int32_t get_m_phase_7() const { return ___m_phase_7; }
	inline int32_t* get_address_of_m_phase_7() { return &___m_phase_7; }
	inline void set_m_phase_7(int32_t value)
	{
		___m_phase_7 = value;
	}

	inline static int32_t get_offset_of_m_moveSpeed_8() { return static_cast<int32_t>(offsetof(EffectCoinText_t948731951, ___m_moveSpeed_8)); }
	inline float get_m_moveSpeed_8() const { return ___m_moveSpeed_8; }
	inline float* get_address_of_m_moveSpeed_8() { return &___m_moveSpeed_8; }
	inline void set_m_moveSpeed_8(float value)
	{
		___m_moveSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_fadeSpeed_9() { return static_cast<int32_t>(offsetof(EffectCoinText_t948731951, ___m_fadeSpeed_9)); }
	inline float get_m_fadeSpeed_9() const { return ___m_fadeSpeed_9; }
	inline float* get_address_of_m_fadeSpeed_9() { return &___m_fadeSpeed_9; }
	inline void set_m_fadeSpeed_9(float value)
	{
		___m_fadeSpeed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
