﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UISprite>
struct List_1_t1458396018;
// FeverLights/UpdateFunction
struct UpdateFunction_t1249117153;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FeverLights
struct  FeverLights_t2165513633  : public MonoBehaviour_t3012272455
{
public:
	// System.Collections.Generic.List`1<UISprite> FeverLights::lights
	List_1_t1458396018 * ___lights_2;
	// System.Single FeverLights::m_time
	float ___m_time_3;
	// System.Boolean FeverLights::m_enable
	bool ___m_enable_4;
	// System.Int32 FeverLights::m_seq
	int32_t ___m_seq_5;
	// System.Int32 FeverLights::m_max_seq
	int32_t ___m_max_seq_6;
	// FeverLights/UpdateFunction FeverLights::m_updateFunc
	UpdateFunction_t1249117153 * ___m_updateFunc_7;

public:
	inline static int32_t get_offset_of_lights_2() { return static_cast<int32_t>(offsetof(FeverLights_t2165513633, ___lights_2)); }
	inline List_1_t1458396018 * get_lights_2() const { return ___lights_2; }
	inline List_1_t1458396018 ** get_address_of_lights_2() { return &___lights_2; }
	inline void set_lights_2(List_1_t1458396018 * value)
	{
		___lights_2 = value;
		Il2CppCodeGenWriteBarrier(&___lights_2, value);
	}

	inline static int32_t get_offset_of_m_time_3() { return static_cast<int32_t>(offsetof(FeverLights_t2165513633, ___m_time_3)); }
	inline float get_m_time_3() const { return ___m_time_3; }
	inline float* get_address_of_m_time_3() { return &___m_time_3; }
	inline void set_m_time_3(float value)
	{
		___m_time_3 = value;
	}

	inline static int32_t get_offset_of_m_enable_4() { return static_cast<int32_t>(offsetof(FeverLights_t2165513633, ___m_enable_4)); }
	inline bool get_m_enable_4() const { return ___m_enable_4; }
	inline bool* get_address_of_m_enable_4() { return &___m_enable_4; }
	inline void set_m_enable_4(bool value)
	{
		___m_enable_4 = value;
	}

	inline static int32_t get_offset_of_m_seq_5() { return static_cast<int32_t>(offsetof(FeverLights_t2165513633, ___m_seq_5)); }
	inline int32_t get_m_seq_5() const { return ___m_seq_5; }
	inline int32_t* get_address_of_m_seq_5() { return &___m_seq_5; }
	inline void set_m_seq_5(int32_t value)
	{
		___m_seq_5 = value;
	}

	inline static int32_t get_offset_of_m_max_seq_6() { return static_cast<int32_t>(offsetof(FeverLights_t2165513633, ___m_max_seq_6)); }
	inline int32_t get_m_max_seq_6() const { return ___m_max_seq_6; }
	inline int32_t* get_address_of_m_max_seq_6() { return &___m_max_seq_6; }
	inline void set_m_max_seq_6(int32_t value)
	{
		___m_max_seq_6 = value;
	}

	inline static int32_t get_offset_of_m_updateFunc_7() { return static_cast<int32_t>(offsetof(FeverLights_t2165513633, ___m_updateFunc_7)); }
	inline UpdateFunction_t1249117153 * get_m_updateFunc_7() const { return ___m_updateFunc_7; }
	inline UpdateFunction_t1249117153 ** get_address_of_m_updateFunc_7() { return &___m_updateFunc_7; }
	inline void set_m_updateFunc_7(UpdateFunction_t1249117153 * value)
	{
		___m_updateFunc_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_updateFunc_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
