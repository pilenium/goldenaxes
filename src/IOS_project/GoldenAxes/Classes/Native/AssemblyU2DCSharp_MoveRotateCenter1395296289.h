﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataChapter
struct DataChapter_t925677859;

#include "AssemblyU2DCSharp_MoveAccelerate3401092526.h"
#include "AssemblyU2DCSharp_Common_DIRECTION1824003935.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveRotateCenter
struct  MoveRotateCenter_t1395296289  : public MoveAccelerate_t3401092526
{
public:
	// Common/DIRECTION MoveRotateCenter::m_direction
	int32_t ___m_direction_5;
	// System.Single MoveRotateCenter::m_loop
	float ___m_loop_6;
	// System.Single MoveRotateCenter::m_size
	float ___m_size_7;
	// System.Single MoveRotateCenter::m_initSpeed
	float ___m_initSpeed_8;
	// System.Single MoveRotateCenter::m_state
	float ___m_state_9;
	// System.Int32 MoveRotateCenter::m_appear_dir
	int32_t ___m_appear_dir_10;
	// System.Int32 MoveRotateCenter::m_circle_dir
	int32_t ___m_circle_dir_11;
	// System.Int32 MoveRotateCenter::m_disappear_dir
	int32_t ___m_disappear_dir_12;
	// System.Single MoveRotateCenter::m_degree
	float ___m_degree_13;
	// System.Single MoveRotateCenter::m_radius
	float ___m_radius_14;
	// System.Single MoveRotateCenter::m_startDegree
	float ___m_startDegree_15;
	// System.Single MoveRotateCenter::m_endDegree
	float ___m_endDegree_16;
	// DataChapter MoveRotateCenter::m_chapterData
	DataChapter_t925677859 * ___m_chapterData_17;

public:
	inline static int32_t get_offset_of_m_direction_5() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_direction_5)); }
	inline int32_t get_m_direction_5() const { return ___m_direction_5; }
	inline int32_t* get_address_of_m_direction_5() { return &___m_direction_5; }
	inline void set_m_direction_5(int32_t value)
	{
		___m_direction_5 = value;
	}

	inline static int32_t get_offset_of_m_loop_6() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_loop_6)); }
	inline float get_m_loop_6() const { return ___m_loop_6; }
	inline float* get_address_of_m_loop_6() { return &___m_loop_6; }
	inline void set_m_loop_6(float value)
	{
		___m_loop_6 = value;
	}

	inline static int32_t get_offset_of_m_size_7() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_size_7)); }
	inline float get_m_size_7() const { return ___m_size_7; }
	inline float* get_address_of_m_size_7() { return &___m_size_7; }
	inline void set_m_size_7(float value)
	{
		___m_size_7 = value;
	}

	inline static int32_t get_offset_of_m_initSpeed_8() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_initSpeed_8)); }
	inline float get_m_initSpeed_8() const { return ___m_initSpeed_8; }
	inline float* get_address_of_m_initSpeed_8() { return &___m_initSpeed_8; }
	inline void set_m_initSpeed_8(float value)
	{
		___m_initSpeed_8 = value;
	}

	inline static int32_t get_offset_of_m_state_9() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_state_9)); }
	inline float get_m_state_9() const { return ___m_state_9; }
	inline float* get_address_of_m_state_9() { return &___m_state_9; }
	inline void set_m_state_9(float value)
	{
		___m_state_9 = value;
	}

	inline static int32_t get_offset_of_m_appear_dir_10() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_appear_dir_10)); }
	inline int32_t get_m_appear_dir_10() const { return ___m_appear_dir_10; }
	inline int32_t* get_address_of_m_appear_dir_10() { return &___m_appear_dir_10; }
	inline void set_m_appear_dir_10(int32_t value)
	{
		___m_appear_dir_10 = value;
	}

	inline static int32_t get_offset_of_m_circle_dir_11() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_circle_dir_11)); }
	inline int32_t get_m_circle_dir_11() const { return ___m_circle_dir_11; }
	inline int32_t* get_address_of_m_circle_dir_11() { return &___m_circle_dir_11; }
	inline void set_m_circle_dir_11(int32_t value)
	{
		___m_circle_dir_11 = value;
	}

	inline static int32_t get_offset_of_m_disappear_dir_12() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_disappear_dir_12)); }
	inline int32_t get_m_disappear_dir_12() const { return ___m_disappear_dir_12; }
	inline int32_t* get_address_of_m_disappear_dir_12() { return &___m_disappear_dir_12; }
	inline void set_m_disappear_dir_12(int32_t value)
	{
		___m_disappear_dir_12 = value;
	}

	inline static int32_t get_offset_of_m_degree_13() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_degree_13)); }
	inline float get_m_degree_13() const { return ___m_degree_13; }
	inline float* get_address_of_m_degree_13() { return &___m_degree_13; }
	inline void set_m_degree_13(float value)
	{
		___m_degree_13 = value;
	}

	inline static int32_t get_offset_of_m_radius_14() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_radius_14)); }
	inline float get_m_radius_14() const { return ___m_radius_14; }
	inline float* get_address_of_m_radius_14() { return &___m_radius_14; }
	inline void set_m_radius_14(float value)
	{
		___m_radius_14 = value;
	}

	inline static int32_t get_offset_of_m_startDegree_15() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_startDegree_15)); }
	inline float get_m_startDegree_15() const { return ___m_startDegree_15; }
	inline float* get_address_of_m_startDegree_15() { return &___m_startDegree_15; }
	inline void set_m_startDegree_15(float value)
	{
		___m_startDegree_15 = value;
	}

	inline static int32_t get_offset_of_m_endDegree_16() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_endDegree_16)); }
	inline float get_m_endDegree_16() const { return ___m_endDegree_16; }
	inline float* get_address_of_m_endDegree_16() { return &___m_endDegree_16; }
	inline void set_m_endDegree_16(float value)
	{
		___m_endDegree_16 = value;
	}

	inline static int32_t get_offset_of_m_chapterData_17() { return static_cast<int32_t>(offsetof(MoveRotateCenter_t1395296289, ___m_chapterData_17)); }
	inline DataChapter_t925677859 * get_m_chapterData_17() const { return ___m_chapterData_17; }
	inline DataChapter_t925677859 ** get_address_of_m_chapterData_17() { return &___m_chapterData_17; }
	inline void set_m_chapterData_17(DataChapter_t925677859 * value)
	{
		___m_chapterData_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_chapterData_17, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
