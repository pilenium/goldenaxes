﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>
struct U3CGetEnumeratorU3Ec__Iterator3_t671719463;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m3409289007_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3__ctor_m3409289007(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t671719463 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3__ctor_m3409289007_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Color_t1588175760  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m283889078_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m283889078(__this, method) ((  Color_t1588175760  (*) (U3CGetEnumeratorU3Ec__Iterator3_t671719463 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m283889078_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1809604247_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1809604247(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t671719463 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1809604247_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m4124414565_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m4124414565(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator3_t671719463 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m4124414565_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3459939244_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3459939244(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t671719463 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3459939244_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1055721948_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Reset_m1055721948(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t671719463 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Reset_m1055721948_gshared)(__this, method)
