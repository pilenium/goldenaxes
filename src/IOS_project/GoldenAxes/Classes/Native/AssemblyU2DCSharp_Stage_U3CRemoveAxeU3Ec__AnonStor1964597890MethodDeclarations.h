﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stage/<RemoveAxe>c__AnonStorey9
struct U3CRemoveAxeU3Ec__AnonStorey9_t1964597890;
// Axe
struct Axe_t66286;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Axe66286.h"

// System.Void Stage/<RemoveAxe>c__AnonStorey9::.ctor()
extern "C"  void U3CRemoveAxeU3Ec__AnonStorey9__ctor_m842157050 (U3CRemoveAxeU3Ec__AnonStorey9_t1964597890 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stage/<RemoveAxe>c__AnonStorey9::<>m__A(Axe)
extern "C"  bool U3CRemoveAxeU3Ec__AnonStorey9_U3CU3Em__A_m863767288 (U3CRemoveAxeU3Ec__AnonStorey9_t1964597890 * __this, Axe_t66286 * ___find0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
