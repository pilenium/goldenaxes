﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPopupList/<UpdateTweenPosition>c__Iterator1
struct U3CUpdateTweenPositionU3Ec__Iterator1_t769271887;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UIPopupList/<UpdateTweenPosition>c__Iterator1::.ctor()
extern "C"  void U3CUpdateTweenPositionU3Ec__Iterator1__ctor_m3454211701 (U3CUpdateTweenPositionU3Ec__Iterator1_t769271887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupList/<UpdateTweenPosition>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateTweenPositionU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2772411581 (U3CUpdateTweenPositionU3Ec__Iterator1_t769271887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupList/<UpdateTweenPosition>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateTweenPositionU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3326394961 (U3CUpdateTweenPositionU3Ec__Iterator1_t769271887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList/<UpdateTweenPosition>c__Iterator1::MoveNext()
extern "C"  bool U3CUpdateTweenPositionU3Ec__Iterator1_MoveNext_m2919572063 (U3CUpdateTweenPositionU3Ec__Iterator1_t769271887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList/<UpdateTweenPosition>c__Iterator1::Dispose()
extern "C"  void U3CUpdateTweenPositionU3Ec__Iterator1_Dispose_m3680975218 (U3CUpdateTweenPositionU3Ec__Iterator1_t769271887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList/<UpdateTweenPosition>c__Iterator1::Reset()
extern "C"  void U3CUpdateTweenPositionU3Ec__Iterator1_Reset_m1100644642 (U3CUpdateTweenPositionU3Ec__Iterator1_t769271887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
