﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataObject
struct DataObject_t3139072937;

#include "codegen/il2cpp-codegen.h"

// System.Void DataObject::.ctor()
extern "C"  void DataObject__ctor_m4076306 (DataObject_t3139072937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
