﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataMonsterAni
struct  DataMonsterAni_t995950348  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<System.String> DataMonsterAni::idle
	List_1_t1765447871 * ___idle_0;
	// System.Collections.Generic.List`1<System.String> DataMonsterAni::hit
	List_1_t1765447871 * ___hit_1;
	// System.Collections.Generic.List`1<System.String> DataMonsterAni::critical_hit
	List_1_t1765447871 * ___critical_hit_2;
	// System.Collections.Generic.List`1<System.String> DataMonsterAni::die
	List_1_t1765447871 * ___die_3;

public:
	inline static int32_t get_offset_of_idle_0() { return static_cast<int32_t>(offsetof(DataMonsterAni_t995950348, ___idle_0)); }
	inline List_1_t1765447871 * get_idle_0() const { return ___idle_0; }
	inline List_1_t1765447871 ** get_address_of_idle_0() { return &___idle_0; }
	inline void set_idle_0(List_1_t1765447871 * value)
	{
		___idle_0 = value;
		Il2CppCodeGenWriteBarrier(&___idle_0, value);
	}

	inline static int32_t get_offset_of_hit_1() { return static_cast<int32_t>(offsetof(DataMonsterAni_t995950348, ___hit_1)); }
	inline List_1_t1765447871 * get_hit_1() const { return ___hit_1; }
	inline List_1_t1765447871 ** get_address_of_hit_1() { return &___hit_1; }
	inline void set_hit_1(List_1_t1765447871 * value)
	{
		___hit_1 = value;
		Il2CppCodeGenWriteBarrier(&___hit_1, value);
	}

	inline static int32_t get_offset_of_critical_hit_2() { return static_cast<int32_t>(offsetof(DataMonsterAni_t995950348, ___critical_hit_2)); }
	inline List_1_t1765447871 * get_critical_hit_2() const { return ___critical_hit_2; }
	inline List_1_t1765447871 ** get_address_of_critical_hit_2() { return &___critical_hit_2; }
	inline void set_critical_hit_2(List_1_t1765447871 * value)
	{
		___critical_hit_2 = value;
		Il2CppCodeGenWriteBarrier(&___critical_hit_2, value);
	}

	inline static int32_t get_offset_of_die_3() { return static_cast<int32_t>(offsetof(DataMonsterAni_t995950348, ___die_3)); }
	inline List_1_t1765447871 * get_die_3() const { return ___die_3; }
	inline List_1_t1765447871 ** get_address_of_die_3() { return &___die_3; }
	inline void set_die_3(List_1_t1765447871 * value)
	{
		___die_3 = value;
		Il2CppCodeGenWriteBarrier(&___die_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
