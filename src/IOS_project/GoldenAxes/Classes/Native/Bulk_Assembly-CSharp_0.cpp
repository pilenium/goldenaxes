﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t3053238938;
// ActiveAnimation
struct ActiveAnimation_t557316862;
// System.String
struct String_t;
// UnityEngine.Animation
struct Animation_t350396337;
// UIPanel[]
struct UIPanelU5BU5D_t3742972657;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// System.Object
struct Il2CppObject;
// UnityEngine.Animator
struct Animator_t792326996;
// AlphaAnimation
struct AlphaAnimation_t489226214;
// AnimatedAlpha
struct AnimatedAlpha_t3219346779;
// UIWidget
struct UIWidget_t769069560;
// UIPanel
struct UIPanel_t295209936;
// AnimatedColor
struct AnimatedColor_t3221279584;
// AnimatedWidget
struct AnimatedWidget_t1642214887;
// AutoControl
struct AutoControl_t1228330126;
// Axe
struct Axe_t66286;
// DataAxe
struct DataAxe_t3107820260;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.Collider
struct Collider_t955670625;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1217738301;
// UnityEngine.BoxCollider
struct BoxCollider_t131631884;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// AxeSlot
struct AxeSlot_t1089550860;
// BMFont
struct BMFont_t1962830650;
// System.Collections.Generic.List`1<BMGlyph>
struct List_1_t1516011674;
// BMGlyph
struct BMGlyph_t719052705;
// BMSymbol
struct BMSymbol_t1170982339;
// UIAtlas
struct UIAtlas_t281921111;
// ByteReader
struct ByteReader_t2446302219;
// System.Byte[]
struct ByteU5BU5D_t58506160;
// UnityEngine.TextAsset
struct TextAsset_t2461560304;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t2606186806;
// BetterList`1<System.String>
struct BetterList_1_t2465456914;
// Character
struct Character_t3568163593;
// Pattern
struct Pattern_t873562992;
// Move
struct Move_t2404337;
// ChatInput
struct ChatInput_t3603581746;
// UIInput
struct UIInput_t289134998;
// Common
struct Common_t2024019467;
// Console
struct Console_t2616163639;
// Control
struct Control_t2616196413;
// Data
struct Data_t2122699;
// DataSetting
struct DataSetting_t2172603174;
// DataChapters
struct DataChapters_t2926209968;
// DataStages
struct DataStages_t3269946783;
// DataAxes
struct DataAxes_t1853147663;
// DataMonsters
struct DataMonsters_t1171984835;
// DataZones
struct DataZones_t1635830941;
// DataPatterns
struct DataPatterns_t2737562829;
// DataUser
struct DataUser_t1853738677;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// Data/<LoadData>c__Iterator4
struct U3CLoadDataU3Ec__Iterator4_t1112965621;
// DataBossMode
struct DataBossMode_t3617900698;
// DataChapter
struct DataChapter_t925677859;
// DataFeverMode
struct DataFeverMode_t3892525469;
// DataMonster
struct DataMonster_t1423279280;
// DataMonsterAni
struct DataMonsterAni_t995950348;
// DataNormalMode
struct DataNormalMode_t1907108500;
// DataObject
struct DataObject_t3139072937;
// DataPattern
struct DataPattern_t3690539110;
// DataStage
struct DataStage_t1629502804;
// DataStageChapter
struct DataStageChapter_t1740173401;
// DataZone
struct DataZone_t1853884054;
// Diver
struct Diver_t66044126;
// EffectWaveSystem
struct EffectWaveSystem_t2871388953;
// DownloadTexture
struct DownloadTexture_t2275226451;
// DownloadTexture/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t4138896567;
// UITexture
struct UITexture_t3903132647;
// EffectAnimation
struct EffectAnimation_t1201827027;
// UnityEngine.Animation[]
struct AnimationU5BU5D_t2512821868;
// EffectCircle
struct EffectCircle_t3606873569;
// UnityEngine.Renderer
struct Renderer_t1092684080;
// EffectCoin
struct EffectCoin_t3708782562;
// EffectWave
struct EffectWave_t3709365322;
// EffectCoinText
struct EffectCoinText_t948731951;
// UnityEngine.TextMesh
struct TextMesh_t583678247;
// EffectGod
struct EffectGod_t535283979;
// EffectParticle
struct EffectParticle_t2854106967;
// UnityEngine.ParticleSystem
struct ParticleSystem_t56787138;
// EffectText
struct EffectText_t3709279870;
// EffectWaveSystem/<AddWave>c__Iterator5
struct U3CAddWaveU3Ec__Iterator5_t4079190500;
// EnvelopContent
struct EnvelopContent_t239872932;
// EquipItems
struct EquipItems_t967969168;
// InvEquipment
struct InvEquipment_t2103517373;
// EquipRandomItem
struct EquipRandomItem_t3062381222;
// EventDelegate
struct EventDelegate_t4004424223;
// EventDelegate/Callback
struct Callback_t4187391077;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3012272455;
// EventDelegate/Parameter[]
struct ParameterU5BU5D_t3433801780;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t506415896;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// EventDelegate/Parameter
struct Parameter_t3958428553;
// UnityEngine.Object
struct Object_t3878351788;
struct Object_t3878351788_marshaled_pinvoke;
// System.Type
struct Type_t;
// EventManager
struct EventManager_t1907836883;
// ExampleDragDropItem
struct ExampleDragDropItem_t1551469152;
// ExampleDragDropSurface
struct ExampleDragDropSurface_t2000059968;
// FeverLights
struct FeverLights_t2165513633;
// FeverLights/<ChangeLight>c__Iterator6
struct U3CChangeLightU3Ec__Iterator6_t2129571673;
// FeverLights/UpdateFunction
struct UpdateFunction_t1249117153;
// GameEvent
struct GameEvent_t2981166504;
// GamePlay
struct GamePlay_t2590336614;
// Stage
struct Stage_t80204510;
// GameUI
struct GameUI_t2125598246;
// PatternManager
struct PatternManager_t2286577181;
// Mode
struct Mode_t2403781;
// GameTouch
struct GameTouch_t2994825805;
// Monster
struct Monster_t2901270458;
// TimeControl
struct TimeControl_t3323199920;
// TouchControl
struct TouchControl_t3773144382;
// GoldMonster
struct GoldMonster_t84400954;
// InitShader
struct InitShader_t2240846133;
// InvAttachmentPoint
struct InvAttachmentPoint_t1265837596;
// InvBaseItem
struct InvBaseItem_t1636452469;
// InvDatabase
struct InvDatabase_t852767852;
// InvDatabase[]
struct InvDatabaseU5BU5D_t3324850277;
// InvGameItem[]
struct InvGameItemU5BU5D_t1443296723;
// InvGameItem
struct InvGameItem_t1588794646;
// InvAttachmentPoint[]
struct InvAttachmentPointU5BU5D_t2968998389;
// System.Collections.Generic.List`1<InvStat>
struct List_1_t126020286;
// InvStat
struct InvStat_t3624028613;
// LagPosition
struct LagPosition_t2611909819;
// LagRotation
struct LagRotation_t1823804176;
// LanguageSelection
struct LanguageSelection_t764444916;
// UIPopupList
struct UIPopupList_t1804933942;
// LoadLevelOnClick
struct LoadLevelOnClick_t3005058795;
// System.Collections.Generic.Dictionary`2<System.String,System.String[]>
struct Dictionary_2_t299600851;
// System.String[]
struct StringU5BU5D_t2956870243;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;
// Localization/LoadFunction
struct LoadFunction_t2829036286;
// Localization/OnLocalizeNotification
struct OnLocalizeNotification_t1642290739;
// LookAtTarget
struct LookAtTarget_t2847647619;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3053238933MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3214874548.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU3214874548MethodDeclarations.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2777878083.h"
#include "AssemblyU2DCSharp_U3CPrivateImplementationDetailsU2777878083MethodDeclarations.h"
#include "AssemblyU2DCSharp_ActiveAnimation557316862.h"
#include "AssemblyU2DCSharp_ActiveAnimation557316862MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen506415896MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen506415896.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_Animator792326996MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo4162640357MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo4162640357.h"
#include "UnityEngine_UnityEngine_Animator792326996.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation350396337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState3357637594MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState3357637594.h"
#include "UnityEngine_UnityEngine_Animation350396337.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction259989387.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "AssemblyU2DCSharp_RealTime3499460011MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUITools237277134MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions2623293100.h"
#include "AssemblyU2DCSharp_AnimationOrTween_EnableCondition3725892324.h"
#include "AssemblyU2DCSharp_AnimationOrTween_DisableConditio3573771103.h"
#include "AssemblyU2DCSharp_UIPanel295209936MethodDeclarations.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "AssemblyU2DCSharp_AlphaAnimation489226214.h"
#include "AssemblyU2DCSharp_AlphaAnimation489226214MethodDeclarations.h"
#include "AssemblyU2DCSharp_TweenAlpha2920325587MethodDeclarations.h"
#include "AssemblyU2DCSharp_TweenAlpha2920325587.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_UITweener_Style80227729.h"
#include "AssemblyU2DCSharp_AnimatedAlpha3219346779.h"
#include "AssemblyU2DCSharp_AnimatedAlpha3219346779MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIWidget769069560MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimatedColor3221279584.h"
#include "AssemblyU2DCSharp_AnimatedColor3221279584MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharp_AnimatedWidget1642214887.h"
#include "AssemblyU2DCSharp_AnimatedWidget1642214887MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction259989387MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationOrTween_DisableConditio3573771103MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationOrTween_EnableCondition3725892324MethodDeclarations.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Trigger4118341060.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Trigger4118341060MethodDeclarations.h"
#include "AssemblyU2DCSharp_AutoControl1228330126.h"
#include "AssemblyU2DCSharp_AutoControl1228330126MethodDeclarations.h"
#include "AssemblyU2DCSharp_Control2616196413MethodDeclarations.h"
#include "AssemblyU2DCSharp_Control2616196413.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3963434288MethodDeclarations.h"
#include "AssemblyU2DCSharp_GamePlay2590336614MethodDeclarations.h"
#include "AssemblyU2DCSharp_Stage80204510MethodDeclarations.h"
#include "AssemblyU2DCSharp_GamePlay2590336614.h"
#include "AssemblyU2DCSharp_Stage80204510.h"
#include "AssemblyU2DCSharp_Axe66286.h"
#include "AssemblyU2DCSharp_Axe66286MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataAxe3107820260.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider131631884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider955670625MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "UnityEngine_UnityEngine_Material1886596500.h"
#include "UnityEngine_UnityEngine_MeshRenderer1217738301.h"
#include "AssemblyU2DCSharp_Common2024019467.h"
#include "AssemblyU2DCSharp_Common2024019467MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PrimitiveType3380664558.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "UnityEngine_UnityEngine_BoxCollider131631884.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataChapter925677859.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharp_PatternManager2286577181MethodDeclarations.h"
#include "AssemblyU2DCSharp_Character3568163593MethodDeclarations.h"
#include "AssemblyU2DCSharp_Pattern873562992MethodDeclarations.h"
#include "AssemblyU2DCSharp_Diver66044126.h"
#include "AssemblyU2DCSharp_PatternManager2286577181.h"
#include "AssemblyU2DCSharp_Pattern873562992.h"
#include "AssemblyU2DCSharp_Monster2901270458MethodDeclarations.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "AssemblyU2DCSharp_Axe_CONTROL1669525821.h"
#include "AssemblyU2DCSharp_Axe_CONTROL1669525821MethodDeclarations.h"
#include "AssemblyU2DCSharp_AxeSlot1089550860.h"
#include "AssemblyU2DCSharp_AxeSlot1089550860MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "AssemblyU2DCSharp_Data2122698MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataAxes1853147663MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataAxes1853147663.h"
#include "AssemblyU2DCSharp_DataAxe3107820260MethodDeclarations.h"
#include "AssemblyU2DCSharp_BMFont1962830650.h"
#include "AssemblyU2DCSharp_BMFont1962830650MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1516011674MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1209863488MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1516011674.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1209863488.h"
#include "AssemblyU2DCSharp_BMGlyph719052705.h"
#include "AssemblyU2DCSharp_BMGlyph719052705MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756MethodDeclarations.h"
#include "AssemblyU2DCSharp_BMSymbol1170982339.h"
#include "AssemblyU2DCSharp_BMSymbol1170982339MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"
#include "AssemblyU2DCSharp_UIAtlas281921111MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUIMath3886757557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture1769722184.h"
#include "AssemblyU2DCSharp_UISpriteData3578345923.h"
#include "UnityEngine_UnityEngine_Texture1769722184MethodDeclarations.h"
#include "AssemblyU2DCSharp_ByteReader2446302219.h"
#include "AssemblyU2DCSharp_ByteReader2446302219MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2778693821.h"
#include "UnityEngine_UnityEngine_TextAsset2461560304.h"
#include "UnityEngine_UnityEngine_TextAsset2461560304MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2465456914MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2465456914.h"
#include "mscorlib_System_IO_File2029342275MethodDeclarations.h"
#include "mscorlib_System_IO_FileStream1527309539.h"
#include "mscorlib_System_IO_FileStream1527309539MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_IO_SeekOrigin3506139269.h"
#include "mscorlib_System_IO_Stream219029575MethodDeclarations.h"
#include "mscorlib_System_IO_Stream219029575.h"
#include "mscorlib_System_Text_Encoding180559927MethodDeclarations.h"
#include "mscorlib_System_Text_Encoding180559927.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2606186806MethodDeclarations.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_StringSplitOptions3963075722.h"
#include "AssemblyU2DCSharp_Character3568163593.h"
#include "AssemblyU2DCSharp_Move2404337.h"
#include "AssemblyU2DCSharp_Move2404337MethodDeclarations.h"
#include "AssemblyU2DCSharp_ChatInput3603581746.h"
#include "AssemblyU2DCSharp_ChatInput3603581746MethodDeclarations.h"
#include "AssemblyU2DCSharp_UILabel291504320MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITextList736798239MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIInput289134998.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UITextList736798239.h"
#include "AssemblyU2DCSharp_UIInput289134998MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUIText3886970074MethodDeclarations.h"
#include "AssemblyU2DCSharp_Common_DIRECTION1824003935.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge190145395.h"
#include "AssemblyU2DCSharp_Common_DIRECTION1824003935MethodDeclarations.h"
#include "AssemblyU2DCSharp_Console2616163639.h"
#include "AssemblyU2DCSharp_Console2616163639MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen797035549MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUIContent2432841515MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"
#include "mscorlib_System_Collections_Generic_List_1_gen797035549.h"
#include "UnityEngine_UnityEngine_GUIContent2432841515.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge881716807MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge881716807.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "UnityEngine_UnityEngine_Input1593691127MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction999919624MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUILayout2490032242MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI_WindowFunction999919624.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GUILayoutOption3151226183.h"
#include "UnityEngine_UnityEngine_GUI1522956648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Event1590224583MethodDeclarations.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"
#include "UnityEngine_UnityEngine_Event1590224583.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharp_Console_Log76580MethodDeclarations.h"
#include "AssemblyU2DCSharp_Data2122698.h"
#include "AssemblyU2DCSharp_Data_PROGRESS4076515885.h"
#include "AssemblyU2DCSharp_DataSetting2172603174.h"
#include "AssemblyU2DCSharp_DataChapters2926209968.h"
#include "AssemblyU2DCSharp_DataStages3269946783.h"
#include "AssemblyU2DCSharp_DataMonsters1171984835.h"
#include "AssemblyU2DCSharp_DataZones1635830941.h"
#include "AssemblyU2DCSharp_DataPatterns2737562829.h"
#include "AssemblyU2DCSharp_DataUser1853738677.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "AssemblyU2DCSharp_DataUser1853738677MethodDeclarations.h"
#include "AssemblyU2DCSharp_Data_U3CLoadDataU3Ec__Iterator41112965621MethodDeclarations.h"
#include "AssemblyU2DCSharp_Data_U3CLoadDataU3Ec__Iterator41112965621.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag1523288937MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility1789555601MethodDeclarations.h"
#include "UnityEngine_UnityEngine_JsonUtility1789555601.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_Data_PROGRESS4076515885MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3904779229MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1990562221MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1990562221.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3904779229.h"
#include "AssemblyU2DCSharp_DataBossMode3617900698.h"
#include "AssemblyU2DCSharp_DataBossMode3617900698MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataChapter925677859MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataChapters2926209968MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1722636828MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4103387116MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4103387116.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1722636828.h"
#include "AssemblyU2DCSharp_DataFeverMode3892525469.h"
#include "AssemblyU2DCSharp_DataFeverMode3892525469MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataMonster1423279280.h"
#include "AssemblyU2DCSharp_DataMonster1423279280MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataMonsterAni995950348.h"
#include "AssemblyU2DCSharp_DataMonsterAni995950348MethodDeclarations.h"
#include "AssemblyU2DCSharp_MONSTER_STATE3983724460.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataMonsters1171984835MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2220238249MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat306021241MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat306021241.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2220238249.h"
#include "AssemblyU2DCSharp_DataNormalMode1907108500.h"
#include "AssemblyU2DCSharp_DataNormalMode1907108500MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1755167990.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1755167990MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataObject3139072937.h"
#include "AssemblyU2DCSharp_DataObject3139072937MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataPattern3690539110.h"
#include "AssemblyU2DCSharp_DataPattern3690539110MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataPatterns2737562829MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen192530783MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2573281071MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2573281071.h"
#include "mscorlib_System_Collections_Generic_List_1_gen192530783.h"
#include "AssemblyU2DCSharp_DataSetting2172603174MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataStage1629502804.h"
#include "AssemblyU2DCSharp_DataStage1629502804MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataStageChapter1740173401.h"
#include "AssemblyU2DCSharp_DataStageChapter1740173401MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2426461773MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat512244765MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat512244765.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2426461773.h"
#include "AssemblyU2DCSharp_DataStages3269946783MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2537132370MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat622915362MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat622915362.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2537132370.h"
#include "UnityEngine_UnityEngine_SystemInfo4158905322MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3733964924MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataZone1853884054.h"
#include "AssemblyU2DCSharp_DataZone1853884054MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataZones1635830941MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2650843023MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat736626015MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat736626015.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2650843023.h"
#include "AssemblyU2DCSharp_Diver66044126MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectWaveSystem2871388953MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectWaveSystem2871388953.h"
#include "AssemblyU2DCSharp_DownloadTexture2275226451.h"
#include "AssemblyU2DCSharp_DownloadTexture2275226451MethodDeclarations.h"
#include "AssemblyU2DCSharp_DownloadTexture_U3CStartU3Ec__It4138896567MethodDeclarations.h"
#include "AssemblyU2DCSharp_DownloadTexture_U3CStartU3Ec__It4138896567.h"
#include "UnityEngine_UnityEngine_Texture2D2509538522.h"
#include "UnityEngine_UnityEngine_WWW1522972100MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWW1522972100.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "AssemblyU2DCSharp_UITexture3903132647MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectAnimation1201827027.h"
#include "AssemblyU2DCSharp_EffectAnimation1201827027MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectCircle3606873569.h"
#include "AssemblyU2DCSharp_EffectCircle3606873569MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080.h"
#include "UnityEngine_UnityEngine_Material1886596500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectCoin3708782562.h"
#include "AssemblyU2DCSharp_EffectCoin3708782562MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "AssemblyU2DCSharp_EffectWave3709365322MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectWave3709365322.h"
#include "AssemblyU2DCSharp_EffectCoinText948731951.h"
#include "AssemblyU2DCSharp_EffectCoinText948731951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh583678247.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextMesh583678247MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectGod535283979.h"
#include "AssemblyU2DCSharp_EffectGod535283979MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectParticle2854106967.h"
#include "AssemblyU2DCSharp_EffectParticle2854106967MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem56787138MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem56787138.h"
#include "AssemblyU2DCSharp_EffectText3709279870.h"
#include "AssemblyU2DCSharp_EffectText3709279870MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectWaveSystem_U3CAddWaveU3Ec_4079190500MethodDeclarations.h"
#include "AssemblyU2DCSharp_EffectWaveSystem_U3CAddWaveU3Ec_4079190500.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240.h"
#include "AssemblyU2DCSharp_EnvelopContent239872932.h"
#include "AssemblyU2DCSharp_EnvelopContent239872932MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "AssemblyU2DCSharp_EquipItems967969168.h"
#include "AssemblyU2DCSharp_EquipItems967969168MethodDeclarations.h"
#include "AssemblyU2DCSharp_InvDatabase852767852MethodDeclarations.h"
#include "AssemblyU2DCSharp_InvGameItem1588794646MethodDeclarations.h"
#include "AssemblyU2DCSharp_InvEquipment2103517373MethodDeclarations.h"
#include "AssemblyU2DCSharp_InvEquipment2103517373.h"
#include "AssemblyU2DCSharp_InvBaseItem1636452469.h"
#include "AssemblyU2DCSharp_InvGameItem1588794646.h"
#include "AssemblyU2DCSharp_InvGameItem_Quality2315868383.h"
#include "AssemblyU2DCSharp_EquipRandomItem3062381222.h"
#include "AssemblyU2DCSharp_EquipRandomItem3062381222MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2433411438.h"
#include "AssemblyU2DCSharp_InvDatabase852767852.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2433411438MethodDeclarations.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback4187391077.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277.h"
#include "mscorlib_System_Reflection_ParameterInfo2610273829.h"
#include "AssemblyU2DCSharp_EventDelegate_Parameter3958428553.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814MethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo2843033814.h"
#include "mscorlib_System_MulticastDelegate2585444626MethodDeclarations.h"
#include "mscorlib_System_MulticastDelegate2585444626.h"
#include "AssemblyU2DCSharp_EventDelegate_Parameter3958428553MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Reflection_BindingFlags2090192240.h"
#include "mscorlib_System_Reflection_MethodInfo3461221277MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase3461000640MethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase3461000640.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "mscorlib_System_Reflection_ParameterInfo2610273829MethodDeclarations.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback4187391077MethodDeclarations.h"
#include "mscorlib_System_ArgumentException124305799.h"
#include "mscorlib_System_ArgumentException124305799MethodDeclarations.h"
#include "mscorlib_System_Exception1967233988MethodDeclarations.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "mscorlib_System_Convert1097883944MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369.h"
#include "mscorlib_System_Reflection_PropertyInfo1490548369MethodDeclarations.h"
#include "mscorlib_System_Reflection_FieldInfo1164929782MethodDeclarations.h"
#include "AssemblyU2DCSharp_EventManager1907836883.h"
#include "AssemblyU2DCSharp_EventManager1907836883MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExampleDragDropItem1551469152.h"
#include "AssemblyU2DCSharp_ExampleDragDropItem1551469152MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIDragDropItem2087865514MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527MethodDeclarations.h"
#include "AssemblyU2DCSharp_ExampleDragDropSurface2000059968.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"
#include "AssemblyU2DCSharp_UICamera189364953MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527.h"
#include "AssemblyU2DCSharp_UIDragDropItem2087865514.h"
#include "AssemblyU2DCSharp_ExampleDragDropSurface2000059968MethodDeclarations.h"
#include "AssemblyU2DCSharp_FeverLights2165513633.h"
#include "AssemblyU2DCSharp_FeverLights2165513633MethodDeclarations.h"
#include "AssemblyU2DCSharp_FeverLights_U3CChangeLightU3Ec__2129571673MethodDeclarations.h"
#include "AssemblyU2DCSharp_FeverLights_U3CChangeLightU3Ec__2129571673.h"
#include "AssemblyU2DCSharp_FeverLights_UpdateFunction1249117153MethodDeclarations.h"
#include "AssemblyU2DCSharp_FeverLights_UpdateFunction1249117153.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1458396018.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1458396018MethodDeclarations.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_GameEvent2981166504.h"
#include "AssemblyU2DCSharp_GameEvent2981166504MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameUI2125598246.h"
#include "AssemblyU2DCSharp_Mode2403779.h"
#include "mscorlib_System_Collections_Generic_List_1_gen799362748MethodDeclarations.h"
#include "AssemblyU2DCSharp_Mode2403779MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameUI2125598246MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameTouch2994825805.h"
#include "mscorlib_System_Collections_Generic_List_1_gen799362748.h"
#include "AssemblyU2DCSharp_Mode_TYPE2590522.h"
#include "AssemblyU2DCSharp_GameTouch2994825805MethodDeclarations.h"
#include "AssemblyU2DCSharp_TimeControl3323199920.h"
#include "AssemblyU2DCSharp_TouchControl3773144382.h"
#include "UnityEngine_UnityEngine_Touch1603883884MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch1603883884.h"
#include "UnityEngine_UnityEngine_TouchPhase1905076713.h"
#include "AssemblyU2DCSharp_GoldMonster84400954.h"
#include "AssemblyU2DCSharp_GoldMonster84400954MethodDeclarations.h"
#include "AssemblyU2DCSharp_MoveFloat3049115883MethodDeclarations.h"
#include "AssemblyU2DCSharp_Move_TYPE2590522.h"
#include "AssemblyU2DCSharp_MoveFloat3049115883.h"
#include "AssemblyU2DCSharp_InitShader2240846133.h"
#include "AssemblyU2DCSharp_InitShader2240846133MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3998140498MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader3998140498.h"
#include "AssemblyU2DCSharp_InvAttachmentPoint1265837596.h"
#include "AssemblyU2DCSharp_InvAttachmentPoint1265837596MethodDeclarations.h"
#include "AssemblyU2DCSharp_InvBaseItem1636452469MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen126020286MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen126020286.h"
#include "AssemblyU2DCSharp_InvBaseItem_Slot2579998.h"
#include "AssemblyU2DCSharp_InvBaseItem_Slot2579998MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUITools237277134.h"
#include "mscorlib_System_Enum2778772662MethodDeclarations.h"
#include "mscorlib_System_Enum2778772662.h"
#include "AssemblyU2DCSharp_InvStat3624028613MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen2032736193MethodDeclarations.h"
#include "AssemblyU2DCSharp_InvStat3624028613.h"
#include "AssemblyU2DCSharp_InvStat_Identifier375032009.h"
#include "AssemblyU2DCSharp_InvStat_Modifier3744098039.h"
#include "mscorlib_System_Comparison_1_gen2032736193.h"
#include "AssemblyU2DCSharp_InvGameItem_Quality2315868383MethodDeclarations.h"
#include "AssemblyU2DCSharp_InvStat_Identifier375032009MethodDeclarations.h"
#include "AssemblyU2DCSharp_InvStat_Modifier3744098039MethodDeclarations.h"
#include "AssemblyU2DCSharp_LagPosition2611909819.h"
#include "AssemblyU2DCSharp_LagPosition2611909819MethodDeclarations.h"
#include "AssemblyU2DCSharp_LagRotation1823804176.h"
#include "AssemblyU2DCSharp_LagRotation1823804176MethodDeclarations.h"
#include "AssemblyU2DCSharp_LanguageSelection764444916.h"
#include "AssemblyU2DCSharp_LanguageSelection764444916MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIPopupList1804933942.h"
#include "AssemblyU2DCSharp_Localization3647281849MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIPopupList1804933942MethodDeclarations.h"
#include "AssemblyU2DCSharp_LoadLevelOnClick3005058795.h"
#include "AssemblyU2DCSharp_LoadLevelOnClick3005058795MethodDeclarations.h"
#include "AssemblyU2DCSharp_Localization3647281849.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge299600851MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge299600851.h"
#include "AssemblyU2DCSharp_Localization_LoadFunction2829036286MethodDeclarations.h"
#include "AssemblyU2DCSharp_Localization_LoadFunction2829036286.h"
#include "UnityEngine_UnityEngine_Resources1543782994.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu66628792MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24083099445MethodDeclarations.h"
#include "AssemblyU2DCSharp_Localization_OnLocalizeNotificat1642290739MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24083099445.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enu66628792.h"
#include "AssemblyU2DCSharp_Localization_OnLocalizeNotificat1642290739.h"
#include "AssemblyU2DCSharp_UIRoot2503447958MethodDeclarations.h"
#include "AssemblyU2DCSharp_UICamera_ControlScheme1667267906.h"
#include "AssemblyU2DCSharp_LookAtTarget2847647619.h"
#include "AssemblyU2DCSharp_LookAtTarget2847647619MethodDeclarations.h"

// !!0[] UnityEngine.GameObject::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430(__this, method) ((  ObjectU5BU5D_t11523773* (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared)(__this, method)
// !!0[] UnityEngine.GameObject::GetComponentsInChildren<UIPanel>()
#define GameObject_GetComponentsInChildren_TisUIPanel_t295209936_m1480394412(__this, method) ((  UIPanelU5BU5D_t3742972657* (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponentsInChildren_TisIl2CppObject_m3418406430_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<ActiveAnimation>()
#define Component_GetComponent_TisActiveAnimation_t557316862_m1742925435(__this, method) ((  ActiveAnimation_t557316862 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m337943659(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<ActiveAnimation>()
#define GameObject_AddComponent_TisActiveAnimation_t557316862_m1120074734(__this, method) ((  ActiveAnimation_t557316862 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIWidget>()
#define Component_GetComponent_TisUIWidget_t769069560_m2158946701(__this, method) ((  UIWidget_t769069560 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIPanel>()
#define Component_GetComponent_TisUIPanel_t295209936_m1836641897(__this, method) ((  UIPanel_t295209936 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Axe>()
#define GameObject_AddComponent_TisAxe_t66286_m295135614(__this, method) ((  Axe_t66286 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Transform>()
#define GameObject_GetComponent_TisTransform_t284553113_m3795369772(__this, method) ((  Transform_t284553113 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider>()
#define GameObject_GetComponent_TisCollider_t955670625_m2553179602(__this, method) ((  Collider_t955670625 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(__this, method) ((  MeshRenderer_t1217738301 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.BoxCollider>()
#define GameObject_AddComponent_TisBoxCollider_t131631884_m1849671780(__this, method) ((  BoxCollider_t131631884 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody>()
#define GameObject_AddComponent_TisRigidbody_t1972007546_m686365494(__this, method) ((  Rigidbody_t1972007546 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t284553113_m811718087(__this, method) ((  Transform_t284553113 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIInput>()
#define Component_GetComponent_TisUIInput_t289134998_m1246790371(__this, method) ((  UIInput_t289134998 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.JsonUtility::FromJson<System.Object>(System.String)
extern "C"  Il2CppObject * JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define JsonUtility_FromJson_TisIl2CppObject_m173634649(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<DataSetting>(System.String)
#define JsonUtility_FromJson_TisDataSetting_t2172603174_m496604577(__this /* static, unused */, p0, method) ((  DataSetting_t2172603174 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<DataChapters>(System.String)
#define JsonUtility_FromJson_TisDataChapters_t2926209968_m2045579131(__this /* static, unused */, p0, method) ((  DataChapters_t2926209968 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<DataStages>(System.String)
#define JsonUtility_FromJson_TisDataStages_t3269946783_m3591454058(__this /* static, unused */, p0, method) ((  DataStages_t3269946783 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<DataAxes>(System.String)
#define JsonUtility_FromJson_TisDataAxes_t1853147663_m2862425050(__this /* static, unused */, p0, method) ((  DataAxes_t1853147663 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<DataMonsters>(System.String)
#define JsonUtility_FromJson_TisDataMonsters_t1171984835_m1408053134(__this /* static, unused */, p0, method) ((  DataMonsters_t1171984835 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<DataZones>(System.String)
#define JsonUtility_FromJson_TisDataZones_t1635830941_m53944088(__this /* static, unused */, p0, method) ((  DataZones_t1635830941 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.JsonUtility::FromJson<DataPatterns>(System.String)
#define JsonUtility_FromJson_TisDataPatterns_t2737562829_m970632856(__this /* static, unused */, p0, method) ((  DataPatterns_t2737562829 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))JsonUtility_FromJson_TisIl2CppObject_m173634649_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::AddComponent<Diver>()
#define GameObject_AddComponent_TisDiver_t66044126_m1188232782(__this, method) ((  Diver_t66044126 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<EffectWaveSystem>()
#define GameObject_AddComponent_TisEffectWaveSystem_t2871388953_m1215556537(__this, method) ((  EffectWaveSystem_t2871388953 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UITexture>()
#define Component_GetComponent_TisUITexture_t3903132647_m1338778738(__this, method) ((  UITexture_t3903132647 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m2266473418(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UnityEngine.Animation>()
#define Component_GetComponentsInChildren_TisAnimation_t350396337_m1284719643(__this, method) ((  AnimationU5BU5D_t2512821868* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t1092684080_m500377675(__this, method) ((  Renderer_t1092684080 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<EffectWave>()
#define GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139(__this, method) ((  EffectWave_t3709365322 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.TextMesh>()
#define GameObject_GetComponent_TisTextMesh_t583678247_m1543870284(__this, method) ((  TextMesh_t583678247 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t1092684080_m4102086307(__this, method) ((  Renderer_t1092684080 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Animation>()
#define Component_GetComponentInChildren_TisAnimation_t350396337_m2209026056(__this, method) ((  Animation_t350396337 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.ParticleSystem>()
#define Component_GetComponent_TisParticleSystem_t56787138_m423524409(__this, method) ((  ParticleSystem_t56787138 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Renderer>()
#define Component_GetComponentInChildren_TisRenderer_t1092684080_m2983327791(__this, method) ((  Renderer_t1092684080 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Transform>()
#define Component_GetComponentInChildren_TisTransform_t284553113_m3473594144(__this, method) ((  Transform_t284553113 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<InvEquipment>()
#define Component_GetComponent_TisInvEquipment_t2103517373_m1342821800(__this, method) ((  InvEquipment_t2103517373 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<InvEquipment>()
#define GameObject_AddComponent_TisInvEquipment_t2103517373_m2731734293(__this, method) ((  InvEquipment_t2103517373 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<ExampleDragDropSurface>()
#define GameObject_GetComponent_TisExampleDragDropSurface_t2000059968_m366110237(__this, method) ((  ExampleDragDropSurface_t2000059968 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<Stage>()
#define GameObject_AddComponent_TisStage_t80204510_m2133437518(__this, method) ((  Stage_t80204510 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<GameTouch>()
#define GameObject_AddComponent_TisGameTouch_t2994825805_m3778956287(__this, method) ((  GameTouch_t2994825805 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<TimeControl>()
#define GameObject_AddComponent_TisTimeControl_t3323199920_m4267318908(__this, method) ((  TimeControl_t3323199920 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<TouchControl>()
#define GameObject_AddComponent_TisTouchControl_t3773144382_m2573704436(__this, method) ((  TouchControl_t3773144382 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<AutoControl>()
#define GameObject_AddComponent_TisAutoControl_t1228330126_m1876096734(__this, method) ((  AutoControl_t1228330126 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.BoxCollider>()
#define GameObject_GetComponent_TisBoxCollider_t131631884_m3222626457(__this, method) ((  BoxCollider_t131631884 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0[] NGUITools::FindActive<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* NGUITools_FindActive_TisIl2CppObject_m943247225_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define NGUITools_FindActive_TisIl2CppObject_m943247225(__this /* static, unused */, method) ((  ObjectU5BU5D_t11523773* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NGUITools_FindActive_TisIl2CppObject_m943247225_gshared)(__this /* static, unused */, method)
// !!0[] NGUITools::FindActive<InvDatabase>()
#define NGUITools_FindActive_TisInvDatabase_t852767852_m1589005867(__this /* static, unused */, method) ((  InvDatabaseU5BU5D_t3324850277* (*) (Il2CppObject * /* static, unused */, const MethodInfo*))NGUITools_FindActive_TisIl2CppObject_m943247225_gshared)(__this /* static, unused */, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<InvAttachmentPoint>()
#define Component_GetComponentsInChildren_TisInvAttachmentPoint_t1265837596_m2807949594(__this, method) ((  InvAttachmentPointU5BU5D_t2968998389* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIPopupList>()
#define Component_GetComponent_TisUIPopupList_t1804933942_m1506054211(__this, method) ((  UIPopupList_t1804933942 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
extern "C"  Il2CppObject * Resources_Load_TisIl2CppObject_m2208345422_gshared (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method);
#define Resources_Load_TisIl2CppObject_m2208345422(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Resources::Load<UnityEngine.TextAsset>(System.String)
#define Resources_Load_TisTextAsset_t2461560304_m585700060(__this /* static, unused */, p0, method) ((  TextAsset_t2461560304 * (*) (Il2CppObject * /* static, unused */, String_t*, const MethodInfo*))Resources_Load_TisIl2CppObject_m2208345422_gshared)(__this /* static, unused */, p0, method)
// System.Void System.Array::Resize<System.Object>(!!0[]&,System.Int32)
extern "C"  void Array_Resize_TisIl2CppObject_m1039640932_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773** p0, int32_t p1, const MethodInfo* method);
#define Array_Resize_TisIl2CppObject_m1039640932(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m1039640932_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void System.Array::Resize<System.String>(!!0[]&,System.Int32)
#define Array_Resize_TisString_t_m939990198(__this /* static, unused */, p0, p1, method) ((  void (*) (Il2CppObject * /* static, unused */, StringU5BU5D_t2956870243**, int32_t, const MethodInfo*))Array_Resize_TisIl2CppObject_m1039640932_gshared)(__this /* static, unused */, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void <PrivateImplementationDetails>::.ctor()
extern "C"  void U3CPrivateImplementationDetailsU3E__ctor_m795736486 (U3CPrivateImplementationDetailsU3E_t3053238938 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$32
extern "C" void U24ArrayTypeU2432_t214874550_marshal_pinvoke(const U24ArrayTypeU2432_t214874550& unmarshaled, U24ArrayTypeU2432_t214874550_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU2432_t214874550_marshal_pinvoke_back(const U24ArrayTypeU2432_t214874550_marshaled_pinvoke& marshaled, U24ArrayTypeU2432_t214874550& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$32
extern "C" void U24ArrayTypeU2432_t214874550_marshal_pinvoke_cleanup(U24ArrayTypeU2432_t214874550_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$32
extern "C" void U24ArrayTypeU2432_t214874550_marshal_com(const U24ArrayTypeU2432_t214874550& unmarshaled, U24ArrayTypeU2432_t214874550_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU2432_t214874550_marshal_com_back(const U24ArrayTypeU2432_t214874550_marshaled_com& marshaled, U24ArrayTypeU2432_t214874550& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$32
extern "C" void U24ArrayTypeU2432_t214874550_marshal_com_cleanup(U24ArrayTypeU2432_t214874550_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal_pinvoke(const U24ArrayTypeU248_t2777878084& unmarshaled, U24ArrayTypeU248_t2777878084_marshaled_pinvoke& marshaled)
{
}
extern "C" void U24ArrayTypeU248_t2777878084_marshal_pinvoke_back(const U24ArrayTypeU248_t2777878084_marshaled_pinvoke& marshaled, U24ArrayTypeU248_t2777878084& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal_pinvoke_cleanup(U24ArrayTypeU248_t2777878084_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal_com(const U24ArrayTypeU248_t2777878084& unmarshaled, U24ArrayTypeU248_t2777878084_marshaled_com& marshaled)
{
}
extern "C" void U24ArrayTypeU248_t2777878084_marshal_com_back(const U24ArrayTypeU248_t2777878084_marshaled_com& marshaled, U24ArrayTypeU248_t2777878084& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>/$ArrayType$8
extern "C" void U24ArrayTypeU248_t2777878084_marshal_com_cleanup(U24ArrayTypeU248_t2777878084_marshaled_com& marshaled)
{
}
// System.Void ActiveAnimation::.ctor()
extern Il2CppClass* List_1_t506415896_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1287572350_MethodInfo_var;
extern const uint32_t ActiveAnimation__ctor_m942098989_MetadataUsageId;
extern "C"  void ActiveAnimation__ctor_m942098989 (ActiveAnimation_t557316862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation__ctor_m942098989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t506415896 * L_0 = (List_1_t506415896 *)il2cpp_codegen_object_new(List_1_t506415896_il2cpp_TypeInfo_var);
		List_1__ctor_m1287572350(L_0, /*hidden argument*/List_1__ctor_m1287572350_MethodInfo_var);
		__this->set_onFinished_3(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mClip_11(L_1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single ActiveAnimation::get_playbackTime()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t ActiveAnimation_get_playbackTime_m2980075932_MetadataUsageId;
extern "C"  float ActiveAnimation_get_playbackTime_m2980075932 (ActiveAnimation_t557316862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation_get_playbackTime_m2980075932_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimatorStateInfo_t4162640357  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animator_t792326996 * L_0 = __this->get_mAnimator_10();
		NullCheck(L_0);
		AnimatorStateInfo_t4162640357  L_1 = Animator_GetCurrentAnimatorStateInfo_m3061859448(L_0, 0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = AnimatorStateInfo_get_normalizedTime_m2531821060((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_3 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean ActiveAnimation::get_isPlaying()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationState_t3357637594_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ActiveAnimation_get_isPlaying_m1988265818_MetadataUsageId;
extern "C"  bool ActiveAnimation_get_isPlaying_m1988265818 (ActiveAnimation_t557316862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation_get_isPlaying_m1988265818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimationState_t3357637594 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	bool V_2 = false;
	Il2CppObject * V_3 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Animation_t350396337 * L_0 = __this->get_mAnim_6();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005b;
		}
	}
	{
		Animator_t792326996 * L_2 = __this->get_mAnimator_10();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_4 = __this->get_mLastDirection_7();
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_0045;
		}
	}
	{
		float L_5 = ActiveAnimation_get_playbackTime_m2980075932(__this, /*hidden argument*/NULL);
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			goto IL_0040;
		}
	}
	{
		return (bool)0;
	}

IL_0040:
	{
		goto IL_0057;
	}

IL_0045:
	{
		float L_6 = ActiveAnimation_get_playbackTime_m2980075932(__this, /*hidden argument*/NULL);
		if ((!(((float)L_6) == ((float)(1.0f)))))
		{
			goto IL_0057;
		}
	}
	{
		return (bool)0;
	}

IL_0057:
	{
		return (bool)1;
	}

IL_0059:
	{
		return (bool)0;
	}

IL_005b:
	{
		Animation_t350396337 * L_7 = __this->get_mAnim_6();
		NullCheck(L_7);
		Il2CppObject * L_8 = Animation_GetEnumerator_m3015582503(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
	}

IL_0067:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00eb;
		}

IL_006c:
		{
			Il2CppObject * L_9 = V_1;
			NullCheck(L_9);
			Il2CppObject * L_10 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_9);
			V_0 = ((AnimationState_t3357637594 *)CastclassSealed(L_10, AnimationState_t3357637594_il2cpp_TypeInfo_var));
			Animation_t350396337 * L_11 = __this->get_mAnim_6();
			AnimationState_t3357637594 * L_12 = V_0;
			NullCheck(L_12);
			String_t* L_13 = AnimationState_get_name_m1230036347(L_12, /*hidden argument*/NULL);
			NullCheck(L_11);
			bool L_14 = Animation_IsPlaying_m1471515685(L_11, L_13, /*hidden argument*/NULL);
			if (L_14)
			{
				goto IL_0093;
			}
		}

IL_008e:
		{
			goto IL_00eb;
		}

IL_0093:
		{
			int32_t L_15 = __this->get_mLastDirection_7();
			if ((!(((uint32_t)L_15) == ((uint32_t)1))))
			{
				goto IL_00bc;
			}
		}

IL_009f:
		{
			AnimationState_t3357637594 * L_16 = V_0;
			NullCheck(L_16);
			float L_17 = AnimationState_get_time_m2290731302(L_16, /*hidden argument*/NULL);
			AnimationState_t3357637594 * L_18 = V_0;
			NullCheck(L_18);
			float L_19 = AnimationState_get_length_m2089699583(L_18, /*hidden argument*/NULL);
			if ((!(((float)L_17) < ((float)L_19))))
			{
				goto IL_00b7;
			}
		}

IL_00b0:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x10F, FINALLY_00fb);
		}

IL_00b7:
		{
			goto IL_00eb;
		}

IL_00bc:
		{
			int32_t L_20 = __this->get_mLastDirection_7();
			if ((!(((uint32_t)L_20) == ((uint32_t)(-1)))))
			{
				goto IL_00e4;
			}
		}

IL_00c8:
		{
			AnimationState_t3357637594 * L_21 = V_0;
			NullCheck(L_21);
			float L_22 = AnimationState_get_time_m2290731302(L_21, /*hidden argument*/NULL);
			if ((!(((float)L_22) > ((float)(0.0f)))))
			{
				goto IL_00df;
			}
		}

IL_00d8:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x10F, FINALLY_00fb);
		}

IL_00df:
		{
			goto IL_00eb;
		}

IL_00e4:
		{
			V_2 = (bool)1;
			IL2CPP_LEAVE(0x10F, FINALLY_00fb);
		}

IL_00eb:
		{
			Il2CppObject * L_23 = V_1;
			NullCheck(L_23);
			bool L_24 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_006c;
			}
		}

IL_00f6:
		{
			IL2CPP_LEAVE(0x10D, FINALLY_00fb);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00fb;
	}

FINALLY_00fb:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_25 = V_1;
			V_3 = ((Il2CppObject *)IsInst(L_25, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_26 = V_3;
			if (L_26)
			{
				goto IL_0106;
			}
		}

IL_0105:
		{
			IL2CPP_END_FINALLY(251)
		}

IL_0106:
		{
			Il2CppObject * L_27 = V_3;
			NullCheck(L_27);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_27);
			IL2CPP_END_FINALLY(251)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(251)
	{
		IL2CPP_JUMP_TBL(0x10F, IL_010f)
		IL2CPP_JUMP_TBL(0x10D, IL_010d)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_010d:
	{
		return (bool)0;
	}

IL_010f:
	{
		bool L_28 = V_2;
		return L_28;
	}
}
// System.Void ActiveAnimation::Finish()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationState_t3357637594_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ActiveAnimation_Finish_m3165870026_MetadataUsageId;
extern "C"  void ActiveAnimation_Finish_m3165870026 (ActiveAnimation_t557316862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation_Finish_m3165870026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimationState_t3357637594 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B16_0 = 0;
	String_t* G_B16_1 = NULL;
	Animator_t792326996 * G_B16_2 = NULL;
	int32_t G_B15_0 = 0;
	String_t* G_B15_1 = NULL;
	Animator_t792326996 * G_B15_2 = NULL;
	float G_B17_0 = 0.0f;
	int32_t G_B17_1 = 0;
	String_t* G_B17_2 = NULL;
	Animator_t792326996 * G_B17_3 = NULL;
	{
		Animation_t350396337 * L_0 = __this->get_mAnim_6();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0094;
		}
	}
	{
		Animation_t350396337 * L_2 = __this->get_mAnim_6();
		NullCheck(L_2);
		Il2CppObject * L_3 = Animation_GetEnumerator_m3015582503(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0062;
		}

IL_0022:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck(L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_4);
			V_0 = ((AnimationState_t3357637594 *)CastclassSealed(L_5, AnimationState_t3357637594_il2cpp_TypeInfo_var));
			int32_t L_6 = __this->get_mLastDirection_7();
			if ((!(((uint32_t)L_6) == ((uint32_t)1))))
			{
				goto IL_004b;
			}
		}

IL_003a:
		{
			AnimationState_t3357637594 * L_7 = V_0;
			AnimationState_t3357637594 * L_8 = V_0;
			NullCheck(L_8);
			float L_9 = AnimationState_get_length_m2089699583(L_8, /*hidden argument*/NULL);
			NullCheck(L_7);
			AnimationState_set_time_m3547497437(L_7, L_9, /*hidden argument*/NULL);
			goto IL_0062;
		}

IL_004b:
		{
			int32_t L_10 = __this->get_mLastDirection_7();
			if ((!(((uint32_t)L_10) == ((uint32_t)(-1)))))
			{
				goto IL_0062;
			}
		}

IL_0057:
		{
			AnimationState_t3357637594 * L_11 = V_0;
			NullCheck(L_11);
			AnimationState_set_time_m3547497437(L_11, (0.0f), /*hidden argument*/NULL);
		}

IL_0062:
		{
			Il2CppObject * L_12 = V_1;
			NullCheck(L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0022;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x84, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_14 = V_1;
			V_2 = ((Il2CppObject *)IsInst(L_14, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_15 = V_2;
			if (L_15)
			{
				goto IL_007d;
			}
		}

IL_007c:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_007d:
		{
			Il2CppObject * L_16 = V_2;
			NullCheck(L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_16);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0084:
	{
		Animation_t350396337 * L_17 = __this->get_mAnim_6();
		NullCheck(L_17);
		Animation_Sample_m2214901881(L_17, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_0094:
	{
		Animator_t792326996 * L_18 = __this->get_mAnimator_10();
		bool L_19 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_18, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00d2;
		}
	}
	{
		Animator_t792326996 * L_20 = __this->get_mAnimator_10();
		String_t* L_21 = __this->get_mClip_11();
		int32_t L_22 = __this->get_mLastDirection_7();
		G_B15_0 = 0;
		G_B15_1 = L_21;
		G_B15_2 = L_20;
		if ((!(((uint32_t)L_22) == ((uint32_t)1))))
		{
			G_B16_0 = 0;
			G_B16_1 = L_21;
			G_B16_2 = L_20;
			goto IL_00c8;
		}
	}
	{
		G_B17_0 = (1.0f);
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		goto IL_00cd;
	}

IL_00c8:
	{
		G_B17_0 = (0.0f);
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
	}

IL_00cd:
	{
		NullCheck(G_B17_3);
		Animator_Play_m3631644044(G_B17_3, G_B17_2, G_B17_1, G_B17_0, /*hidden argument*/NULL);
	}

IL_00d2:
	{
		return;
	}
}
// System.Void ActiveAnimation::Reset()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationState_t3357637594_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ActiveAnimation_Reset_m2883499226_MetadataUsageId;
extern "C"  void ActiveAnimation_Reset_m2883499226 (ActiveAnimation_t557316862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation_Reset_m2883499226_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	AnimationState_t3357637594 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B16_0 = 0;
	String_t* G_B16_1 = NULL;
	Animator_t792326996 * G_B16_2 = NULL;
	int32_t G_B15_0 = 0;
	String_t* G_B15_1 = NULL;
	Animator_t792326996 * G_B15_2 = NULL;
	float G_B17_0 = 0.0f;
	int32_t G_B17_1 = 0;
	String_t* G_B17_2 = NULL;
	Animator_t792326996 * G_B17_3 = NULL;
	{
		Animation_t350396337 * L_0 = __this->get_mAnim_6();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0089;
		}
	}
	{
		Animation_t350396337 * L_2 = __this->get_mAnim_6();
		NullCheck(L_2);
		Il2CppObject * L_3 = Animation_GetEnumerator_m3015582503(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0062;
		}

IL_0022:
		{
			Il2CppObject * L_4 = V_1;
			NullCheck(L_4);
			Il2CppObject * L_5 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_4);
			V_0 = ((AnimationState_t3357637594 *)CastclassSealed(L_5, AnimationState_t3357637594_il2cpp_TypeInfo_var));
			int32_t L_6 = __this->get_mLastDirection_7();
			if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
			{
				goto IL_004b;
			}
		}

IL_003a:
		{
			AnimationState_t3357637594 * L_7 = V_0;
			AnimationState_t3357637594 * L_8 = V_0;
			NullCheck(L_8);
			float L_9 = AnimationState_get_length_m2089699583(L_8, /*hidden argument*/NULL);
			NullCheck(L_7);
			AnimationState_set_time_m3547497437(L_7, L_9, /*hidden argument*/NULL);
			goto IL_0062;
		}

IL_004b:
		{
			int32_t L_10 = __this->get_mLastDirection_7();
			if ((!(((uint32_t)L_10) == ((uint32_t)1))))
			{
				goto IL_0062;
			}
		}

IL_0057:
		{
			AnimationState_t3357637594 * L_11 = V_0;
			NullCheck(L_11);
			AnimationState_set_time_m3547497437(L_11, (0.0f), /*hidden argument*/NULL);
		}

IL_0062:
		{
			Il2CppObject * L_12 = V_1;
			NullCheck(L_12);
			bool L_13 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0022;
			}
		}

IL_006d:
		{
			IL2CPP_LEAVE(0x84, FINALLY_0072);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0072;
	}

FINALLY_0072:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_14 = V_1;
			V_2 = ((Il2CppObject *)IsInst(L_14, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_15 = V_2;
			if (L_15)
			{
				goto IL_007d;
			}
		}

IL_007c:
		{
			IL2CPP_END_FINALLY(114)
		}

IL_007d:
		{
			Il2CppObject * L_16 = V_2;
			NullCheck(L_16);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_16);
			IL2CPP_END_FINALLY(114)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(114)
	{
		IL2CPP_JUMP_TBL(0x84, IL_0084)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0084:
	{
		goto IL_00c7;
	}

IL_0089:
	{
		Animator_t792326996 * L_17 = __this->get_mAnimator_10();
		bool L_18 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_17, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00c7;
		}
	}
	{
		Animator_t792326996 * L_19 = __this->get_mAnimator_10();
		String_t* L_20 = __this->get_mClip_11();
		int32_t L_21 = __this->get_mLastDirection_7();
		G_B15_0 = 0;
		G_B15_1 = L_20;
		G_B15_2 = L_19;
		if ((!(((uint32_t)L_21) == ((uint32_t)(-1)))))
		{
			G_B16_0 = 0;
			G_B16_1 = L_20;
			G_B16_2 = L_19;
			goto IL_00bd;
		}
	}
	{
		G_B17_0 = (1.0f);
		G_B17_1 = G_B15_0;
		G_B17_2 = G_B15_1;
		G_B17_3 = G_B15_2;
		goto IL_00c2;
	}

IL_00bd:
	{
		G_B17_0 = (0.0f);
		G_B17_1 = G_B16_0;
		G_B17_2 = G_B16_1;
		G_B17_3 = G_B16_2;
	}

IL_00c2:
	{
		NullCheck(G_B17_3);
		Animator_Play_m3631644044(G_B17_3, G_B17_2, G_B17_1, G_B17_0, /*hidden argument*/NULL);
	}

IL_00c7:
	{
		return;
	}
}
// System.Void ActiveAnimation::Start()
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t ActiveAnimation_Start_m4184204077_MetadataUsageId;
extern "C"  void ActiveAnimation_Start_m4184204077 (ActiveAnimation_t557316862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation_Start_m4184204077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = __this->get_eventReceiver_4();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002f;
		}
	}
	{
		List_1_t506415896 * L_2 = __this->get_onFinished_3();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		bool L_3 = EventDelegate_IsValid_m979192339(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002f;
		}
	}
	{
		__this->set_eventReceiver_4((GameObject_t4012695102 *)NULL);
		__this->set_callWhenFinished_5((String_t*)NULL);
	}

IL_002f:
	{
		return;
	}
}
// System.Void ActiveAnimation::Update()
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationState_t3357637594_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* ActiveAnimation_t557316862_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t ActiveAnimation_Update_m867159680_MetadataUsageId;
extern "C"  void ActiveAnimation_Update_m867159680 (ActiveAnimation_t557316862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation_Update_m867159680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	bool V_1 = false;
	AnimationState_t3357637594 * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	float V_4 = 0.0f;
	Il2CppObject * V_5 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	Animator_t792326996 * G_B5_0 = NULL;
	Animator_t792326996 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	Animator_t792326996 * G_B6_1 = NULL;
	{
		float L_0 = RealTime_get_deltaTime_m2274453566(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) == ((float)(0.0f)))))
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		Animator_t792326996 * L_2 = __this->get_mAnimator_10();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0066;
		}
	}
	{
		Animator_t792326996 * L_4 = __this->get_mAnimator_10();
		int32_t L_5 = __this->get_mLastDirection_7();
		G_B4_0 = L_4;
		if ((!(((uint32_t)L_5) == ((uint32_t)(-1)))))
		{
			G_B5_0 = L_4;
			goto IL_003c;
		}
	}
	{
		float L_6 = V_0;
		G_B6_0 = ((-L_6));
		G_B6_1 = G_B4_0;
		goto IL_003d;
	}

IL_003c:
	{
		float L_7 = V_0;
		G_B6_0 = L_7;
		G_B6_1 = G_B5_0;
	}

IL_003d:
	{
		NullCheck(G_B6_1);
		Animator_Update_m1710797732(G_B6_1, G_B6_0, /*hidden argument*/NULL);
		bool L_8 = ActiveAnimation_get_isPlaying_m1988265818(__this, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004e;
		}
	}
	{
		return;
	}

IL_004e:
	{
		Animator_t792326996 * L_9 = __this->get_mAnimator_10();
		NullCheck(L_9);
		Behaviour_set_enabled_m2046806933(L_9, (bool)0, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_0066:
	{
		Animation_t350396337 * L_10 = __this->get_mAnim_6();
		bool L_11 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_10, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0164;
		}
	}
	{
		V_1 = (bool)0;
		Animation_t350396337 * L_12 = __this->get_mAnim_6();
		NullCheck(L_12);
		Il2CppObject * L_13 = Animation_GetEnumerator_m3015582503(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
	}

IL_0085:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0121;
		}

IL_008a:
		{
			Il2CppObject * L_14 = V_3;
			NullCheck(L_14);
			Il2CppObject * L_15 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_14);
			V_2 = ((AnimationState_t3357637594 *)CastclassSealed(L_15, AnimationState_t3357637594_il2cpp_TypeInfo_var));
			Animation_t350396337 * L_16 = __this->get_mAnim_6();
			AnimationState_t3357637594 * L_17 = V_2;
			NullCheck(L_17);
			String_t* L_18 = AnimationState_get_name_m1230036347(L_17, /*hidden argument*/NULL);
			NullCheck(L_16);
			bool L_19 = Animation_IsPlaying_m1471515685(L_16, L_18, /*hidden argument*/NULL);
			if (L_19)
			{
				goto IL_00b1;
			}
		}

IL_00ac:
		{
			goto IL_0121;
		}

IL_00b1:
		{
			AnimationState_t3357637594 * L_20 = V_2;
			NullCheck(L_20);
			float L_21 = AnimationState_get_speed_m1598763504(L_20, /*hidden argument*/NULL);
			float L_22 = V_0;
			V_4 = ((float)((float)L_21*(float)L_22));
			AnimationState_t3357637594 * L_23 = V_2;
			AnimationState_t3357637594 * L_24 = L_23;
			NullCheck(L_24);
			float L_25 = AnimationState_get_time_m2290731302(L_24, /*hidden argument*/NULL);
			float L_26 = V_4;
			NullCheck(L_24);
			AnimationState_set_time_m3547497437(L_24, ((float)((float)L_25+(float)L_26)), /*hidden argument*/NULL);
			float L_27 = V_4;
			if ((!(((float)L_27) < ((float)(0.0f)))))
			{
				goto IL_00fd;
			}
		}

IL_00d6:
		{
			AnimationState_t3357637594 * L_28 = V_2;
			NullCheck(L_28);
			float L_29 = AnimationState_get_time_m2290731302(L_28, /*hidden argument*/NULL);
			if ((!(((float)L_29) > ((float)(0.0f)))))
			{
				goto IL_00ed;
			}
		}

IL_00e6:
		{
			V_1 = (bool)1;
			goto IL_00f8;
		}

IL_00ed:
		{
			AnimationState_t3357637594 * L_30 = V_2;
			NullCheck(L_30);
			AnimationState_set_time_m3547497437(L_30, (0.0f), /*hidden argument*/NULL);
		}

IL_00f8:
		{
			goto IL_0121;
		}

IL_00fd:
		{
			AnimationState_t3357637594 * L_31 = V_2;
			NullCheck(L_31);
			float L_32 = AnimationState_get_time_m2290731302(L_31, /*hidden argument*/NULL);
			AnimationState_t3357637594 * L_33 = V_2;
			NullCheck(L_33);
			float L_34 = AnimationState_get_length_m2089699583(L_33, /*hidden argument*/NULL);
			if ((!(((float)L_32) < ((float)L_34))))
			{
				goto IL_0115;
			}
		}

IL_010e:
		{
			V_1 = (bool)1;
			goto IL_0121;
		}

IL_0115:
		{
			AnimationState_t3357637594 * L_35 = V_2;
			AnimationState_t3357637594 * L_36 = V_2;
			NullCheck(L_36);
			float L_37 = AnimationState_get_length_m2089699583(L_36, /*hidden argument*/NULL);
			NullCheck(L_35);
			AnimationState_set_time_m3547497437(L_35, L_37, /*hidden argument*/NULL);
		}

IL_0121:
		{
			Il2CppObject * L_38 = V_3;
			NullCheck(L_38);
			bool L_39 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_38);
			if (L_39)
			{
				goto IL_008a;
			}
		}

IL_012c:
		{
			IL2CPP_LEAVE(0x146, FINALLY_0131);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0131;
	}

FINALLY_0131:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_40 = V_3;
			V_5 = ((Il2CppObject *)IsInst(L_40, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_41 = V_5;
			if (L_41)
			{
				goto IL_013e;
			}
		}

IL_013d:
		{
			IL2CPP_END_FINALLY(305)
		}

IL_013e:
		{
			Il2CppObject * L_42 = V_5;
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_42);
			IL2CPP_END_FINALLY(305)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(305)
	{
		IL2CPP_JUMP_TBL(0x146, IL_0146)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0146:
	{
		Animation_t350396337 * L_43 = __this->get_mAnim_6();
		NullCheck(L_43);
		Animation_Sample_m2214901881(L_43, /*hidden argument*/NULL);
		bool L_44 = V_1;
		if (!L_44)
		{
			goto IL_0158;
		}
	}
	{
		return;
	}

IL_0158:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_016c;
	}

IL_0164:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}

IL_016c:
	{
		bool L_45 = __this->get_mNotify_9();
		if (!L_45)
		{
			goto IL_0200;
		}
	}
	{
		__this->set_mNotify_9((bool)0);
		ActiveAnimation_t557316862 * L_46 = ((ActiveAnimation_t557316862_StaticFields*)ActiveAnimation_t557316862_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		bool L_47 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_46, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_01d8;
		}
	}
	{
		((ActiveAnimation_t557316862_StaticFields*)ActiveAnimation_t557316862_il2cpp_TypeInfo_var->static_fields)->set_current_2(__this);
		List_1_t506415896 * L_48 = __this->get_onFinished_3();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Execute_m895247138(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_49 = __this->get_eventReceiver_4();
		bool L_50 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_49, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_50)
		{
			goto IL_01d2;
		}
	}
	{
		String_t* L_51 = __this->get_callWhenFinished_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_52 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_51, /*hidden argument*/NULL);
		if (L_52)
		{
			goto IL_01d2;
		}
	}
	{
		GameObject_t4012695102 * L_53 = __this->get_eventReceiver_4();
		String_t* L_54 = __this->get_callWhenFinished_5();
		NullCheck(L_53);
		GameObject_SendMessage_m3923684423(L_53, L_54, 1, /*hidden argument*/NULL);
	}

IL_01d2:
	{
		((ActiveAnimation_t557316862_StaticFields*)ActiveAnimation_t557316862_il2cpp_TypeInfo_var->static_fields)->set_current_2((ActiveAnimation_t557316862 *)NULL);
	}

IL_01d8:
	{
		int32_t L_55 = __this->get_mDisableDirection_8();
		if (!L_55)
		{
			goto IL_0200;
		}
	}
	{
		int32_t L_56 = __this->get_mLastDirection_7();
		int32_t L_57 = __this->get_mDisableDirection_8();
		if ((!(((uint32_t)L_56) == ((uint32_t)L_57))))
		{
			goto IL_0200;
		}
	}
	{
		GameObject_t4012695102 * L_58 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m3941650786(NULL /*static, unused*/, L_58, (bool)0, /*hidden argument*/NULL);
	}

IL_0200:
	{
		return;
	}
}
// System.Void ActiveAnimation::Play(System.String,AnimationOrTween.Direction)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationState_t3357637594_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t ActiveAnimation_Play_m1105978650_MetadataUsageId;
extern "C"  void ActiveAnimation_Play_m1105978650 (ActiveAnimation_t557316862 * __this, String_t* ___clipName0, int32_t ___playDirection1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation_Play_m1105978650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	AnimationState_t3357637594 * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	float V_3 = 0.0f;
	Il2CppObject * V_4 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B4_0 = 0;
	int32_t G_B35_0 = 0;
	String_t* G_B35_1 = NULL;
	Animator_t792326996 * G_B35_2 = NULL;
	int32_t G_B34_0 = 0;
	String_t* G_B34_1 = NULL;
	Animator_t792326996 * G_B34_2 = NULL;
	float G_B36_0 = 0.0f;
	int32_t G_B36_1 = 0;
	String_t* G_B36_2 = NULL;
	Animator_t792326996 * G_B36_3 = NULL;
	{
		int32_t L_0 = ___playDirection1;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_1 = __this->get_mLastDirection_7();
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0018;
		}
	}
	{
		G_B4_0 = 1;
		goto IL_0019;
	}

IL_0018:
	{
		G_B4_0 = (-1);
	}

IL_0019:
	{
		___playDirection1 = G_B4_0;
	}

IL_001b:
	{
		Animation_t350396337 * L_2 = __this->get_mAnim_6();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0168;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)1, /*hidden argument*/NULL);
		Animation_t350396337 * L_4 = __this->get_mAnim_6();
		NullCheck(L_4);
		Behaviour_set_enabled_m2046806933(L_4, (bool)0, /*hidden argument*/NULL);
		String_t* L_5 = ___clipName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		Animation_t350396337 * L_8 = __this->get_mAnim_6();
		NullCheck(L_8);
		bool L_9 = Animation_get_isPlaying_m3295833780(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_0068;
		}
	}
	{
		Animation_t350396337 * L_10 = __this->get_mAnim_6();
		NullCheck(L_10);
		Animation_Play_m4273654237(L_10, /*hidden argument*/NULL);
	}

IL_0068:
	{
		goto IL_008b;
	}

IL_006d:
	{
		Animation_t350396337 * L_11 = __this->get_mAnim_6();
		String_t* L_12 = ___clipName0;
		NullCheck(L_11);
		bool L_13 = Animation_IsPlaying_m1471515685(L_11, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_008b;
		}
	}
	{
		Animation_t350396337 * L_14 = __this->get_mAnim_6();
		String_t* L_15 = ___clipName0;
		NullCheck(L_14);
		Animation_Play_m900498501(L_14, L_15, /*hidden argument*/NULL);
	}

IL_008b:
	{
		Animation_t350396337 * L_16 = __this->get_mAnim_6();
		NullCheck(L_16);
		Il2CppObject * L_17 = Animation_GetEnumerator_m3015582503(L_16, /*hidden argument*/NULL);
		V_2 = L_17;
	}

IL_0097:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0125;
		}

IL_009c:
		{
			Il2CppObject * L_18 = V_2;
			NullCheck(L_18);
			Il2CppObject * L_19 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_18);
			V_1 = ((AnimationState_t3357637594 *)CastclassSealed(L_19, AnimationState_t3357637594_il2cpp_TypeInfo_var));
			String_t* L_20 = ___clipName0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_21 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
			if (L_21)
			{
				goto IL_00c4;
			}
		}

IL_00b3:
		{
			AnimationState_t3357637594 * L_22 = V_1;
			NullCheck(L_22);
			String_t* L_23 = AnimationState_get_name_m1230036347(L_22, /*hidden argument*/NULL);
			String_t* L_24 = ___clipName0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_25 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
			if (!L_25)
			{
				goto IL_0125;
			}
		}

IL_00c4:
		{
			AnimationState_t3357637594 * L_26 = V_1;
			NullCheck(L_26);
			float L_27 = AnimationState_get_speed_m1598763504(L_26, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
			float L_28 = fabsf(L_27);
			V_3 = L_28;
			AnimationState_t3357637594 * L_29 = V_1;
			float L_30 = V_3;
			int32_t L_31 = ___playDirection1;
			NullCheck(L_29);
			AnimationState_set_speed_m4187319075(L_29, ((float)((float)L_30*(float)(((float)((float)L_31))))), /*hidden argument*/NULL);
			int32_t L_32 = ___playDirection1;
			if ((!(((uint32_t)L_32) == ((uint32_t)(-1)))))
			{
				goto IL_0102;
			}
		}

IL_00e1:
		{
			AnimationState_t3357637594 * L_33 = V_1;
			NullCheck(L_33);
			float L_34 = AnimationState_get_time_m2290731302(L_33, /*hidden argument*/NULL);
			if ((!(((float)L_34) == ((float)(0.0f)))))
			{
				goto IL_0102;
			}
		}

IL_00f1:
		{
			AnimationState_t3357637594 * L_35 = V_1;
			AnimationState_t3357637594 * L_36 = V_1;
			NullCheck(L_36);
			float L_37 = AnimationState_get_length_m2089699583(L_36, /*hidden argument*/NULL);
			NullCheck(L_35);
			AnimationState_set_time_m3547497437(L_35, L_37, /*hidden argument*/NULL);
			goto IL_0125;
		}

IL_0102:
		{
			int32_t L_38 = ___playDirection1;
			if ((!(((uint32_t)L_38) == ((uint32_t)1))))
			{
				goto IL_0125;
			}
		}

IL_0109:
		{
			AnimationState_t3357637594 * L_39 = V_1;
			NullCheck(L_39);
			float L_40 = AnimationState_get_time_m2290731302(L_39, /*hidden argument*/NULL);
			AnimationState_t3357637594 * L_41 = V_1;
			NullCheck(L_41);
			float L_42 = AnimationState_get_length_m2089699583(L_41, /*hidden argument*/NULL);
			if ((!(((float)L_40) == ((float)L_42))))
			{
				goto IL_0125;
			}
		}

IL_011a:
		{
			AnimationState_t3357637594 * L_43 = V_1;
			NullCheck(L_43);
			AnimationState_set_time_m3547497437(L_43, (0.0f), /*hidden argument*/NULL);
		}

IL_0125:
		{
			Il2CppObject * L_44 = V_2;
			NullCheck(L_44);
			bool L_45 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_44);
			if (L_45)
			{
				goto IL_009c;
			}
		}

IL_0130:
		{
			IL2CPP_LEAVE(0x14A, FINALLY_0135);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0135;
	}

FINALLY_0135:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_46 = V_2;
			V_4 = ((Il2CppObject *)IsInst(L_46, IDisposable_t1628921374_il2cpp_TypeInfo_var));
			Il2CppObject * L_47 = V_4;
			if (L_47)
			{
				goto IL_0142;
			}
		}

IL_0141:
		{
			IL2CPP_END_FINALLY(309)
		}

IL_0142:
		{
			Il2CppObject * L_48 = V_4;
			NullCheck(L_48);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_48);
			IL2CPP_END_FINALLY(309)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(309)
	{
		IL2CPP_JUMP_TBL(0x14A, IL_014a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_014a:
	{
		int32_t L_49 = ___playDirection1;
		__this->set_mLastDirection_7(L_49);
		__this->set_mNotify_9((bool)1);
		Animation_t350396337 * L_50 = __this->get_mAnim_6();
		NullCheck(L_50);
		Animation_Sample_m2214901881(L_50, /*hidden argument*/NULL);
		goto IL_01ec;
	}

IL_0168:
	{
		Animator_t792326996 * L_51 = __this->get_mAnimator_10();
		bool L_52 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_51, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_01ec;
		}
	}
	{
		bool L_53 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_01a8;
		}
	}
	{
		bool L_54 = ActiveAnimation_get_isPlaying_m1988265818(__this, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_01a8;
		}
	}
	{
		String_t* L_55 = __this->get_mClip_11();
		String_t* L_56 = ___clipName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_57 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		if (!L_57)
		{
			goto IL_01a8;
		}
	}
	{
		int32_t L_58 = ___playDirection1;
		__this->set_mLastDirection_7(L_58);
		return;
	}

IL_01a8:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)1, /*hidden argument*/NULL);
		__this->set_mNotify_9((bool)1);
		int32_t L_59 = ___playDirection1;
		__this->set_mLastDirection_7(L_59);
		String_t* L_60 = ___clipName0;
		__this->set_mClip_11(L_60);
		Animator_t792326996 * L_61 = __this->get_mAnimator_10();
		String_t* L_62 = __this->get_mClip_11();
		int32_t L_63 = ___playDirection1;
		G_B34_0 = 0;
		G_B34_1 = L_62;
		G_B34_2 = L_61;
		if ((!(((uint32_t)L_63) == ((uint32_t)1))))
		{
			G_B35_0 = 0;
			G_B35_1 = L_62;
			G_B35_2 = L_61;
			goto IL_01e2;
		}
	}
	{
		G_B36_0 = (0.0f);
		G_B36_1 = G_B34_0;
		G_B36_2 = G_B34_1;
		G_B36_3 = G_B34_2;
		goto IL_01e7;
	}

IL_01e2:
	{
		G_B36_0 = (1.0f);
		G_B36_1 = G_B35_0;
		G_B36_2 = G_B35_1;
		G_B36_3 = G_B35_2;
	}

IL_01e7:
	{
		NullCheck(G_B36_3);
		Animator_Play_m3631644044(G_B36_3, G_B36_2, G_B36_1, G_B36_0, /*hidden argument*/NULL);
	}

IL_01ec:
	{
		return;
	}
}
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,System.String,AnimationOrTween.Direction,AnimationOrTween.EnableCondition,AnimationOrTween.DisableCondition)
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisUIPanel_t295209936_m1480394412_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisActiveAnimation_t557316862_m1742925435_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisActiveAnimation_t557316862_m1120074734_MethodInfo_var;
extern const uint32_t ActiveAnimation_Play_m2459698113_MetadataUsageId;
extern "C"  ActiveAnimation_t557316862 * ActiveAnimation_Play_m2459698113 (Il2CppObject * __this /* static, unused */, Animation_t350396337 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, int32_t ___enableBeforePlay3, int32_t ___disableCondition4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation_Play_m2459698113_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIPanelU5BU5D_t3742972657* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ActiveAnimation_t557316862 * V_3 = NULL;
	{
		Animation_t350396337 * L_0 = ___anim0;
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_2 = NGUITools_GetActive_m3605198179(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_3 = ___enableBeforePlay3;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0019;
		}
	}
	{
		return (ActiveAnimation_t557316862 *)NULL;
	}

IL_0019:
	{
		Animation_t350396337 * L_4 = ___anim0;
		NullCheck(L_4);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m3941650786(NULL /*static, unused*/, L_5, (bool)1, /*hidden argument*/NULL);
		Animation_t350396337 * L_6 = ___anim0;
		NullCheck(L_6);
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		UIPanelU5BU5D_t3742972657* L_8 = GameObject_GetComponentsInChildren_TisUIPanel_t295209936_m1480394412(L_7, /*hidden argument*/GameObject_GetComponentsInChildren_TisUIPanel_t295209936_m1480394412_MethodInfo_var);
		V_0 = L_8;
		V_1 = 0;
		UIPanelU5BU5D_t3742972657* L_9 = V_0;
		NullCheck(L_9);
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))));
		goto IL_0048;
	}

IL_003c:
	{
		UIPanelU5BU5D_t3742972657* L_10 = V_0;
		int32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		NullCheck(((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12))));
		UIPanel_Refresh_m3396930772(((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12))), /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_14 = V_1;
		int32_t L_15 = V_2;
		if ((((int32_t)L_14) < ((int32_t)L_15)))
		{
			goto IL_003c;
		}
	}

IL_004f:
	{
		Animation_t350396337 * L_16 = ___anim0;
		NullCheck(L_16);
		ActiveAnimation_t557316862 * L_17 = Component_GetComponent_TisActiveAnimation_t557316862_m1742925435(L_16, /*hidden argument*/Component_GetComponent_TisActiveAnimation_t557316862_m1742925435_MethodInfo_var);
		V_3 = L_17;
		ActiveAnimation_t557316862 * L_18 = V_3;
		bool L_19 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_18, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_006e;
		}
	}
	{
		Animation_t350396337 * L_20 = ___anim0;
		NullCheck(L_20);
		GameObject_t4012695102 * L_21 = Component_get_gameObject_m1170635899(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		ActiveAnimation_t557316862 * L_22 = GameObject_AddComponent_TisActiveAnimation_t557316862_m1120074734(L_21, /*hidden argument*/GameObject_AddComponent_TisActiveAnimation_t557316862_m1120074734_MethodInfo_var);
		V_3 = L_22;
	}

IL_006e:
	{
		ActiveAnimation_t557316862 * L_23 = V_3;
		Animation_t350396337 * L_24 = ___anim0;
		NullCheck(L_23);
		L_23->set_mAnim_6(L_24);
		ActiveAnimation_t557316862 * L_25 = V_3;
		int32_t L_26 = ___disableCondition4;
		NullCheck(L_25);
		L_25->set_mDisableDirection_8(L_26);
		ActiveAnimation_t557316862 * L_27 = V_3;
		NullCheck(L_27);
		List_1_t506415896 * L_28 = L_27->get_onFinished_3();
		NullCheck(L_28);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<EventDelegate>::Clear() */, L_28);
		ActiveAnimation_t557316862 * L_29 = V_3;
		String_t* L_30 = ___clipName1;
		int32_t L_31 = ___playDirection2;
		NullCheck(L_29);
		ActiveAnimation_Play_m1105978650(L_29, L_30, L_31, /*hidden argument*/NULL);
		ActiveAnimation_t557316862 * L_32 = V_3;
		NullCheck(L_32);
		Animation_t350396337 * L_33 = L_32->get_mAnim_6();
		bool L_34 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_33, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00b1;
		}
	}
	{
		ActiveAnimation_t557316862 * L_35 = V_3;
		NullCheck(L_35);
		Animation_t350396337 * L_36 = L_35->get_mAnim_6();
		NullCheck(L_36);
		Animation_Sample_m2214901881(L_36, /*hidden argument*/NULL);
		goto IL_00d2;
	}

IL_00b1:
	{
		ActiveAnimation_t557316862 * L_37 = V_3;
		NullCheck(L_37);
		Animator_t792326996 * L_38 = L_37->get_mAnimator_10();
		bool L_39 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_38, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_00d2;
		}
	}
	{
		ActiveAnimation_t557316862 * L_40 = V_3;
		NullCheck(L_40);
		Animator_t792326996 * L_41 = L_40->get_mAnimator_10();
		NullCheck(L_41);
		Animator_Update_m1710797732(L_41, (0.0f), /*hidden argument*/NULL);
	}

IL_00d2:
	{
		ActiveAnimation_t557316862 * L_42 = V_3;
		return L_42;
	}
}
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,System.String,AnimationOrTween.Direction)
extern "C"  ActiveAnimation_t557316862 * ActiveAnimation_Play_m479659302 (Il2CppObject * __this /* static, unused */, Animation_t350396337 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, const MethodInfo* method)
{
	{
		Animation_t350396337 * L_0 = ___anim0;
		String_t* L_1 = ___clipName1;
		int32_t L_2 = ___playDirection2;
		ActiveAnimation_t557316862 * L_3 = ActiveAnimation_Play_m2459698113(NULL /*static, unused*/, L_0, L_1, L_2, 0, 0, /*hidden argument*/NULL);
		return L_3;
	}
}
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,AnimationOrTween.Direction)
extern "C"  ActiveAnimation_t557316862 * ActiveAnimation_Play_m3105139874 (Il2CppObject * __this /* static, unused */, Animation_t350396337 * ___anim0, int32_t ___playDirection1, const MethodInfo* method)
{
	{
		Animation_t350396337 * L_0 = ___anim0;
		int32_t L_1 = ___playDirection1;
		ActiveAnimation_t557316862 * L_2 = ActiveAnimation_Play_m2459698113(NULL /*static, unused*/, L_0, (String_t*)NULL, L_1, 0, 0, /*hidden argument*/NULL);
		return L_2;
	}
}
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animator,System.String,AnimationOrTween.Direction,AnimationOrTween.EnableCondition,AnimationOrTween.DisableCondition)
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponentsInChildren_TisUIPanel_t295209936_m1480394412_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisActiveAnimation_t557316862_m1742925435_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisActiveAnimation_t557316862_m1120074734_MethodInfo_var;
extern const uint32_t ActiveAnimation_Play_m2827471594_MetadataUsageId;
extern "C"  ActiveAnimation_t557316862 * ActiveAnimation_Play_m2827471594 (Il2CppObject * __this /* static, unused */, Animator_t792326996 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, int32_t ___enableBeforePlay3, int32_t ___disableCondition4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ActiveAnimation_Play_m2827471594_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIPanelU5BU5D_t3742972657* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ActiveAnimation_t557316862 * V_3 = NULL;
	{
		int32_t L_0 = ___enableBeforePlay3;
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0056;
		}
	}
	{
		Animator_t792326996 * L_1 = ___anim0;
		NullCheck(L_1);
		GameObject_t4012695102 * L_2 = Component_get_gameObject_m1170635899(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_3 = NGUITools_GetActive_m3605198179(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0056;
		}
	}
	{
		int32_t L_4 = ___enableBeforePlay3;
		if ((((int32_t)L_4) == ((int32_t)1)))
		{
			goto IL_0020;
		}
	}
	{
		return (ActiveAnimation_t557316862 *)NULL;
	}

IL_0020:
	{
		Animator_t792326996 * L_5 = ___anim0;
		NullCheck(L_5);
		GameObject_t4012695102 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m3941650786(NULL /*static, unused*/, L_6, (bool)1, /*hidden argument*/NULL);
		Animator_t792326996 * L_7 = ___anim0;
		NullCheck(L_7);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		UIPanelU5BU5D_t3742972657* L_9 = GameObject_GetComponentsInChildren_TisUIPanel_t295209936_m1480394412(L_8, /*hidden argument*/GameObject_GetComponentsInChildren_TisUIPanel_t295209936_m1480394412_MethodInfo_var);
		V_0 = L_9;
		V_1 = 0;
		UIPanelU5BU5D_t3742972657* L_10 = V_0;
		NullCheck(L_10);
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))));
		goto IL_004f;
	}

IL_0043:
	{
		UIPanelU5BU5D_t3742972657* L_11 = V_0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		NullCheck(((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		UIPanel_Refresh_m3396930772(((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))), /*hidden argument*/NULL);
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_15 = V_1;
		int32_t L_16 = V_2;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0043;
		}
	}

IL_0056:
	{
		Animator_t792326996 * L_17 = ___anim0;
		NullCheck(L_17);
		ActiveAnimation_t557316862 * L_18 = Component_GetComponent_TisActiveAnimation_t557316862_m1742925435(L_17, /*hidden argument*/Component_GetComponent_TisActiveAnimation_t557316862_m1742925435_MethodInfo_var);
		V_3 = L_18;
		ActiveAnimation_t557316862 * L_19 = V_3;
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_19, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0075;
		}
	}
	{
		Animator_t792326996 * L_21 = ___anim0;
		NullCheck(L_21);
		GameObject_t4012695102 * L_22 = Component_get_gameObject_m1170635899(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		ActiveAnimation_t557316862 * L_23 = GameObject_AddComponent_TisActiveAnimation_t557316862_m1120074734(L_22, /*hidden argument*/GameObject_AddComponent_TisActiveAnimation_t557316862_m1120074734_MethodInfo_var);
		V_3 = L_23;
	}

IL_0075:
	{
		ActiveAnimation_t557316862 * L_24 = V_3;
		Animator_t792326996 * L_25 = ___anim0;
		NullCheck(L_24);
		L_24->set_mAnimator_10(L_25);
		ActiveAnimation_t557316862 * L_26 = V_3;
		int32_t L_27 = ___disableCondition4;
		NullCheck(L_26);
		L_26->set_mDisableDirection_8(L_27);
		ActiveAnimation_t557316862 * L_28 = V_3;
		NullCheck(L_28);
		List_1_t506415896 * L_29 = L_28->get_onFinished_3();
		NullCheck(L_29);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<EventDelegate>::Clear() */, L_29);
		ActiveAnimation_t557316862 * L_30 = V_3;
		String_t* L_31 = ___clipName1;
		int32_t L_32 = ___playDirection2;
		NullCheck(L_30);
		ActiveAnimation_Play_m1105978650(L_30, L_31, L_32, /*hidden argument*/NULL);
		ActiveAnimation_t557316862 * L_33 = V_3;
		NullCheck(L_33);
		Animation_t350396337 * L_34 = L_33->get_mAnim_6();
		bool L_35 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_34, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00b8;
		}
	}
	{
		ActiveAnimation_t557316862 * L_36 = V_3;
		NullCheck(L_36);
		Animation_t350396337 * L_37 = L_36->get_mAnim_6();
		NullCheck(L_37);
		Animation_Sample_m2214901881(L_37, /*hidden argument*/NULL);
		goto IL_00d9;
	}

IL_00b8:
	{
		ActiveAnimation_t557316862 * L_38 = V_3;
		NullCheck(L_38);
		Animator_t792326996 * L_39 = L_38->get_mAnimator_10();
		bool L_40 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_39, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_00d9;
		}
	}
	{
		ActiveAnimation_t557316862 * L_41 = V_3;
		NullCheck(L_41);
		Animator_t792326996 * L_42 = L_41->get_mAnimator_10();
		NullCheck(L_42);
		Animator_Update_m1710797732(L_42, (0.0f), /*hidden argument*/NULL);
	}

IL_00d9:
	{
		ActiveAnimation_t557316862 * L_43 = V_3;
		return L_43;
	}
}
// System.Void AlphaAnimation::.ctor()
extern "C"  void AlphaAnimation__ctor_m932663157 (AlphaAnimation_t489226214 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AlphaAnimation::Awake()
extern "C"  void AlphaAnimation_Awake_m1170268376 (AlphaAnimation_t489226214 * __this, const MethodInfo* method)
{
	TweenAlpha_t2920325587 * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		TweenAlpha_t2920325587 * L_1 = TweenAlpha_Begin_m3125886119(NULL /*static, unused*/, L_0, (0.7f), (0.0f), /*hidden argument*/NULL);
		V_0 = L_1;
		TweenAlpha_t2920325587 * L_2 = V_0;
		NullCheck(L_2);
		((UITweener_t105489188 *)L_2)->set_style_4(2);
		return;
	}
}
// System.Void AnimatedAlpha::.ctor()
extern "C"  void AnimatedAlpha__ctor_m2397168240 (AnimatedAlpha_t3219346779 * __this, const MethodInfo* method)
{
	{
		__this->set_alpha_2((1.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatedAlpha::OnEnable()
extern const MethodInfo* Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIPanel_t295209936_m1836641897_MethodInfo_var;
extern const uint32_t AnimatedAlpha_OnEnable_m3689385750_MetadataUsageId;
extern "C"  void AnimatedAlpha_OnEnable_m3689385750 (AnimatedAlpha_t3219346779 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatedAlpha_OnEnable_m3689385750_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIWidget_t769069560 * L_0 = Component_GetComponent_TisUIWidget_t769069560_m2158946701(__this, /*hidden argument*/Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var);
		__this->set_mWidget_3(L_0);
		UIPanel_t295209936 * L_1 = Component_GetComponent_TisUIPanel_t295209936_m1836641897(__this, /*hidden argument*/Component_GetComponent_TisUIPanel_t295209936_m1836641897_MethodInfo_var);
		__this->set_mPanel_4(L_1);
		AnimatedAlpha_LateUpdate_m1961848291(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatedAlpha::LateUpdate()
extern "C"  void AnimatedAlpha_LateUpdate_m1961848291 (AnimatedAlpha_t3219346779 * __this, const MethodInfo* method)
{
	{
		UIWidget_t769069560 * L_0 = __this->get_mWidget_3();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		UIWidget_t769069560 * L_2 = __this->get_mWidget_3();
		float L_3 = __this->get_alpha_2();
		NullCheck(L_2);
		VirtActionInvoker1< float >::Invoke(8 /* System.Void UIWidget::set_alpha(System.Single) */, L_2, L_3);
	}

IL_0022:
	{
		UIPanel_t295209936 * L_4 = __this->get_mPanel_4();
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		UIPanel_t295209936 * L_6 = __this->get_mPanel_4();
		float L_7 = __this->get_alpha_2();
		NullCheck(L_6);
		VirtActionInvoker1< float >::Invoke(8 /* System.Void UIPanel::set_alpha(System.Single) */, L_6, L_7);
	}

IL_0044:
	{
		return;
	}
}
// System.Void AnimatedColor::.ctor()
extern "C"  void AnimatedColor__ctor_m1249991179 (AnimatedColor_t3221279584 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_color_2(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatedColor::OnEnable()
extern const MethodInfo* Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var;
extern const uint32_t AnimatedColor_OnEnable_m3192335771_MetadataUsageId;
extern "C"  void AnimatedColor_OnEnable_m3192335771 (AnimatedColor_t3221279584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatedColor_OnEnable_m3192335771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIWidget_t769069560 * L_0 = Component_GetComponent_TisUIWidget_t769069560_m2158946701(__this, /*hidden argument*/Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var);
		__this->set_mWidget_3(L_0);
		AnimatedColor_LateUpdate_m1038188328(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatedColor::LateUpdate()
extern "C"  void AnimatedColor_LateUpdate_m1038188328 (AnimatedColor_t3221279584 * __this, const MethodInfo* method)
{
	{
		UIWidget_t769069560 * L_0 = __this->get_mWidget_3();
		Color_t1588175760  L_1 = __this->get_color_2();
		NullCheck(L_0);
		UIWidget_set_color_m1905035359(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatedWidget::.ctor()
extern "C"  void AnimatedWidget__ctor_m2770619028 (AnimatedWidget_t1642214887 * __this, const MethodInfo* method)
{
	{
		__this->set_width_2((1.0f));
		__this->set_height_3((1.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatedWidget::OnEnable()
extern const MethodInfo* Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var;
extern const uint32_t AnimatedWidget_OnEnable_m901547122_MetadataUsageId;
extern "C"  void AnimatedWidget_OnEnable_m901547122 (AnimatedWidget_t1642214887 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatedWidget_OnEnable_m901547122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIWidget_t769069560 * L_0 = Component_GetComponent_TisUIWidget_t769069560_m2158946701(__this, /*hidden argument*/Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var);
		__this->set_mWidget_4(L_0);
		AnimatedWidget_LateUpdate_m2908519487(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AnimatedWidget::LateUpdate()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t AnimatedWidget_LateUpdate_m2908519487_MetadataUsageId;
extern "C"  void AnimatedWidget_LateUpdate_m2908519487 (AnimatedWidget_t1642214887 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AnimatedWidget_LateUpdate_m2908519487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIWidget_t769069560 * L_0 = __this->get_mWidget_4();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003d;
		}
	}
	{
		UIWidget_t769069560 * L_2 = __this->get_mWidget_4();
		float L_3 = __this->get_width_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		UIWidget_set_width_m3480390811(L_2, L_4, /*hidden argument*/NULL);
		UIWidget_t769069560 * L_5 = __this->get_mWidget_4();
		float L_6 = __this->get_height_3();
		int32_t L_7 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		UIWidget_set_height_m1838784918(L_5, L_7, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void AutoControl::.ctor()
extern "C"  void AutoControl__ctor_m1495262877 (AutoControl_t1228330126 * __this, const MethodInfo* method)
{
	{
		__this->set_m_time_3((10.0f));
		Control__ctor_m3714434766(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoControl::Awake()
extern "C"  void AutoControl_Awake_m1732868096 (AutoControl_t1228330126 * __this, const MethodInfo* method)
{
	{
		Control_Awake_m3952039985(__this, /*hidden argument*/NULL);
		__this->set_m_time_3((0.5f));
		return;
	}
}
// System.Void AutoControl::OnDestroy()
extern "C"  void AutoControl_OnDestroy_m4080215318 (AutoControl_t1228330126 * __this, const MethodInfo* method)
{
	{
		Control_OnDestroy_m607880391(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoControl::Update()
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t AutoControl_Update_m835371024_MetadataUsageId;
extern "C"  void AutoControl_Update_m835371024 (AutoControl_t1228330126 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoControl_Update_m835371024_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Control_Update_m910222847(__this, /*hidden argument*/NULL);
		float L_0 = __this->get_m_time_3();
		float L_1 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_time_3(((float)((float)L_0-(float)L_1)));
		float L_2 = __this->get_m_time_3();
		if ((!(((float)L_2) < ((float)(0.0f)))))
		{
			goto IL_0062;
		}
	}
	{
		float L_3 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_4;
		GamePlay_t2590336614 * L_5 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_5);
		Stage_t80204510 * L_6 = GamePlay_get_stage_m3096959283(L_5, /*hidden argument*/NULL);
		float L_7 = V_0;
		float L_8 = V_1;
		NullCheck(L_6);
		Stage_AddAxe_m3412358540(L_6, L_7, L_8, /*hidden argument*/NULL);
		__this->set_m_time_3((1.0f));
		float L_9 = __this->get_m_p_4();
		__this->set_m_p_4(((float)((float)L_9+(float)(0.1f))));
	}

IL_0062:
	{
		return;
	}
}
// System.Void Axe::.ctor()
extern "C"  void Axe__ctor_m476662845 (Axe_t66286 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// Axe Axe::Alloc(DataAxe)
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisAxe_t66286_m295135614_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1965091464;
extern const uint32_t Axe_Alloc_m2968844089_MetadataUsageId;
extern "C"  Axe_t66286 * Axe_Alloc_m2968844089 (Il2CppObject * __this /* static, unused */, DataAxe_t3107820260 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_Alloc_m2968844089_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Axe_t66286 * V_1 = NULL;
	{
		GameObject_t4012695102 * L_0 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
		GameObject__ctor_m845034556(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		DataAxe_t3107820260 * L_1 = ___data0;
		NullCheck(L_1);
		String_t* L_2 = L_1->get_type_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, _stringLiteral1965091464, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		GameObject_t4012695102 * L_4 = V_0;
		NullCheck(L_4);
		Axe_t66286 * L_5 = GameObject_AddComponent_TisAxe_t66286_m295135614(L_4, /*hidden argument*/GameObject_AddComponent_TisAxe_t66286_m295135614_MethodInfo_var);
		V_1 = L_5;
		goto IL_002e;
	}

IL_0027:
	{
		GameObject_t4012695102 * L_6 = V_0;
		NullCheck(L_6);
		Axe_t66286 * L_7 = GameObject_AddComponent_TisAxe_t66286_m295135614(L_6, /*hidden argument*/GameObject_AddComponent_TisAxe_t66286_m295135614_MethodInfo_var);
		V_1 = L_7;
	}

IL_002e:
	{
		Axe_t66286 * L_8 = V_1;
		return L_8;
	}
}
// System.Boolean Axe::get_IsEnabled()
extern "C"  bool Axe_get_IsEnabled_m3084702109 (Axe_t66286 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_enabled_10();
		return L_0;
	}
}
// System.Void Axe::Awake()
extern const MethodInfo* GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var;
extern const uint32_t Axe_Awake_m714268064_MetadataUsageId;
extern "C"  void Axe_Awake_m714268064 (Axe_t66286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_Awake_m714268064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t284553113 * L_1 = GameObject_GetComponent_TisTransform_t284553113_m3795369772(L_0, /*hidden argument*/GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var);
		__this->set_m_tr_3(L_1);
		return;
	}
}
// System.Void Axe::OnDestory()
extern "C"  void Axe_OnDestory_m2179453264 (Axe_t66286 * __this, const MethodInfo* method)
{
	{
		__this->set_m_tr_3((Transform_t284553113 *)NULL);
		return;
	}
}
// System.Void Axe::Init(DataAxe,System.Single,System.Single)
extern "C"  void Axe_Init_m633038913 (Axe_t66286 * __this, DataAxe_t3107820260 * ___data0, float ___width1, float ___power2, const MethodInfo* method)
{
	{
		DataAxe_t3107820260 * L_0 = ___data0;
		VirtActionInvoker1< DataAxe_t3107820260 * >::Invoke(6 /* System.Void Axe::InitData(DataAxe) */, __this, L_0);
		float L_1 = ___width1;
		VirtActionInvoker1< float >::Invoke(7 /* System.Void Axe::InitPositionByWidth(System.Single) */, __this, L_1);
		float L_2 = ___power2;
		VirtActionInvoker1< float >::Invoke(9 /* System.Void Axe::InitVelocityByPower(System.Single) */, __this, L_2);
		Vector3_t3525329789 * L_3 = __this->get_address_of_m_velocity_12();
		float L_4 = L_3->get_z_3();
		VirtActionInvoker1< float >::Invoke(11 /* System.Void Axe::InitRotate(System.Single) */, __this, L_4);
		return;
	}
}
// System.Void Axe::Init(DataAxe,UnityEngine.Vector3)
extern "C"  void Axe_Init_m3101774794 (Axe_t66286 * __this, DataAxe_t3107820260 * ___data0, Vector3_t3525329789  ___position1, const MethodInfo* method)
{
	{
		DataAxe_t3107820260 * L_0 = ___data0;
		VirtActionInvoker1< DataAxe_t3107820260 * >::Invoke(6 /* System.Void Axe::InitData(DataAxe) */, __this, L_0);
		float L_1 = (&___position1)->get_x_1();
		VirtActionInvoker1< float >::Invoke(8 /* System.Void Axe::InitPositionByPosition(System.Single) */, __this, L_1);
		float L_2 = (&___position1)->get_z_3();
		VirtActionInvoker1< float >::Invoke(10 /* System.Void Axe::InitVelocityByDistance(System.Single) */, __this, L_2);
		Vector3_t3525329789 * L_3 = __this->get_address_of_m_velocity_12();
		float L_4 = L_3->get_z_3();
		VirtActionInvoker1< float >::Invoke(11 /* System.Void Axe::InitRotate(System.Single) */, __this, L_4);
		return;
	}
}
// System.Void Axe::InitData(DataAxe)
extern const Il2CppType* Material_t1886596500_0_0_0_var;
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t1886596500_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCollider_t955670625_m2553179602_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisBoxCollider_t131631884_m1849671780_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody_t1972007546_m686365494_MethodInfo_var;
extern const uint32_t Axe_InitData_m801913389_MetadataUsageId;
extern "C"  void Axe_InitData_m801913389 (Axe_t66286 * __this, DataAxe_t3107820260 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_InitData_m801913389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Collider_t955670625 * V_0 = NULL;
	Material_t1886596500 * V_1 = NULL;
	MeshRenderer_t1217738301 * V_2 = NULL;
	{
		DataAxe_t3107820260 * L_0 = ___data0;
		__this->set_m_data_2(L_0);
		DataAxe_t3107820260 * L_1 = ___data0;
		NullCheck(L_1);
		String_t* L_2 = L_1->get_name_1();
		Object_set_name_m1123518500(__this, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		String_t* L_3 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_TAG_AXE_13();
		Component_set_tag_m3240989163(__this, L_3, /*hidden argument*/NULL);
		__this->set_m_damage_13(1);
		DataAxe_t3107820260 * L_4 = ___data0;
		NullCheck(L_4);
		String_t* L_5 = L_4->get_url_3();
		Object_t3878351788 * L_6 = Resources_Load_m2187391845(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		Object_t3878351788 * L_7 = Object_Instantiate_m3040600263(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_m_axe_4(((GameObject_t4012695102 *)IsInstSealed(L_7, GameObject_t4012695102_il2cpp_TypeInfo_var)));
		GameObject_t4012695102 * L_8 = __this->get_m_axe_4();
		NullCheck(L_8);
		Transform_t284553113 * L_9 = GameObject_GetComponent_TisTransform_t284553113_m3795369772(L_8, /*hidden argument*/GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var);
		__this->set_m_axe_tr_5(L_9);
		Transform_t284553113 * L_10 = __this->get_m_axe_tr_5();
		Transform_t284553113 * L_11 = __this->get_m_tr_3();
		NullCheck(L_10);
		Transform_set_parent_m3231272063(L_10, L_11, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_12 = GameObject_CreatePrimitive_m3259337130(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		__this->set_m_shadow_6(L_12);
		GameObject_t4012695102 * L_13 = __this->get_m_shadow_6();
		NullCheck(L_13);
		Collider_t955670625 * L_14 = GameObject_GetComponent_TisCollider_t955670625_m2553179602(L_13, /*hidden argument*/GameObject_GetComponent_TisCollider_t955670625_m2553179602_MethodInfo_var);
		V_0 = L_14;
		Collider_t955670625 * L_15 = V_0;
		Object_Destroy_m176400816(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		DataAxe_t3107820260 * L_16 = ___data0;
		NullCheck(L_16);
		String_t* L_17 = L_16->get_shadow_8();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_18 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Material_t1886596500_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_19 = Resources_Load_m3601699608(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		V_1 = ((Material_t1886596500 *)IsInstClass(L_19, Material_t1886596500_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_20 = __this->get_m_shadow_6();
		NullCheck(L_20);
		MeshRenderer_t1217738301 * L_21 = GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910(L_20, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t1217738301_m2686897910_MethodInfo_var);
		V_2 = L_21;
		MeshRenderer_t1217738301 * L_22 = V_2;
		Material_t1886596500 * L_23 = V_1;
		NullCheck(L_22);
		Renderer_set_material_m1012580896(L_22, L_23, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_24 = __this->get_m_shadow_6();
		NullCheck(L_24);
		Transform_t284553113 * L_25 = GameObject_GetComponent_TisTransform_t284553113_m3795369772(L_24, /*hidden argument*/GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var);
		__this->set_m_shadow_tr_7(L_25);
		Transform_t284553113 * L_26 = __this->get_m_shadow_tr_7();
		DataAxe_t3107820260 * L_27 = ___data0;
		NullCheck(L_27);
		Vector3_t3525329789  L_28 = L_27->get_shadowSize_9();
		NullCheck(L_26);
		Transform_set_localScale_m310756934(L_26, L_28, /*hidden argument*/NULL);
		Transform_t284553113 * L_29 = __this->get_m_shadow_tr_7();
		NullCheck(L_29);
		Transform_Rotate_m3498734243(L_29, (90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Transform_t284553113 * L_30 = __this->get_m_shadow_tr_7();
		Transform_t284553113 * L_31 = __this->get_m_tr_3();
		NullCheck(L_30);
		Transform_set_parent_m3231272063(L_30, L_31, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_32 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		BoxCollider_t131631884 * L_33 = GameObject_AddComponent_TisBoxCollider_t131631884_m1849671780(L_32, /*hidden argument*/GameObject_AddComponent_TisBoxCollider_t131631884_m1849671780_MethodInfo_var);
		__this->set_m_collider_8(L_33);
		BoxCollider_t131631884 * L_34 = __this->get_m_collider_8();
		DataAxe_t3107820260 * L_35 = ___data0;
		NullCheck(L_35);
		Vector3_t3525329789  L_36 = L_35->get_collider_size_10();
		NullCheck(L_34);
		BoxCollider_set_size_m2674293393(L_34, L_36, /*hidden argument*/NULL);
		BoxCollider_t131631884 * L_37 = __this->get_m_collider_8();
		DataAxe_t3107820260 * L_38 = ___data0;
		NullCheck(L_38);
		Vector3_t3525329789  L_39 = L_38->get_collider_center_11();
		NullCheck(L_37);
		BoxCollider_set_center_m1759609917(L_37, L_39, /*hidden argument*/NULL);
		BoxCollider_t131631884 * L_40 = __this->get_m_collider_8();
		NullCheck(L_40);
		Collider_set_isTrigger_m2057864191(L_40, (bool)1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_41 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_41);
		Rigidbody_t1972007546 * L_42 = GameObject_AddComponent_TisRigidbody_t1972007546_m686365494(L_41, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t1972007546_m686365494_MethodInfo_var);
		__this->set_m_rigidBody_9(L_42);
		Rigidbody_t1972007546 * L_43 = __this->get_m_rigidBody_9();
		NullCheck(L_43);
		Rigidbody_set_useGravity_m2620827635(L_43, (bool)0, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_44 = __this->get_m_rigidBody_9();
		NullCheck(L_44);
		Rigidbody_set_isKinematic_m294703295(L_44, (bool)1, /*hidden argument*/NULL);
		__this->set_m_time_11((0.0f));
		__this->set_m_enabled_10((bool)1);
		return;
	}
}
// System.Void Axe::InitPositionByWidth(System.Single)
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t Axe_InitPositionByWidth_m2638844890_MetadataUsageId;
extern "C"  void Axe_InitPositionByWidth_m2638844890 (Axe_t66286 * __this, float ___width0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_InitPositionByWidth_m2638844890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataChapter_t925677859 * V_0 = NULL;
	{
		GamePlay_t2590336614 * L_0 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_0);
		DataChapter_t925677859 * L_1 = GamePlay_get_dataChapter_m1400870973(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t284553113 * L_2 = __this->get_m_tr_3();
		float L_3 = ___width0;
		DataChapter_t925677859 * L_4 = V_0;
		NullCheck(L_4);
		Rect_t1525428817 * L_5 = L_4->get_address_of_rect_2();
		float L_6 = Rect_get_width_m2824209432(L_5, /*hidden argument*/NULL);
		DataAxe_t3107820260 * L_7 = __this->get_m_data_2();
		NullCheck(L_7);
		float L_8 = L_7->get_size_4();
		DataChapter_t925677859 * L_9 = V_0;
		NullCheck(L_9);
		Rect_t1525428817 * L_10 = L_9->get_address_of_rect_2();
		float L_11 = Rect_get_x_m982385354(L_10, /*hidden argument*/NULL);
		DataAxe_t3107820260 * L_12 = __this->get_m_data_2();
		NullCheck(L_12);
		float L_13 = L_12->get_size_4();
		DataAxe_t3107820260 * L_14 = __this->get_m_data_2();
		NullCheck(L_14);
		float L_15 = L_14->get_throw_height_5();
		Vector3_t3525329789  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m2926210380(&L_16, ((float)((float)((float)((float)((float)((float)L_3*(float)((float)((float)L_6-(float)L_8))))+(float)L_11))+(float)((float)((float)L_13/(float)(2.0f))))), L_15, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_position_m3111394108(L_2, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Axe::InitPositionByPosition(System.Single)
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t Axe_InitPositionByPosition_m3253655083_MetadataUsageId;
extern "C"  void Axe_InitPositionByPosition_m3253655083 (Axe_t66286 * __this, float ___position_x0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_InitPositionByPosition_m3253655083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataChapter_t925677859 * V_0 = NULL;
	float V_1 = 0.0f;
	{
		GamePlay_t2590336614 * L_0 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_0);
		Stage_t80204510 * L_1 = GamePlay_get_stage_m3096959283(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		DataChapter_t925677859 * L_2 = Stage_get_dataChapter_m2108362549(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ___position_x0;
		V_1 = L_3;
		float L_4 = V_1;
		DataChapter_t925677859 * L_5 = V_0;
		NullCheck(L_5);
		Rect_t1525428817 * L_6 = L_5->get_address_of_rect_2();
		float L_7 = Rect_get_width_m2824209432(L_6, /*hidden argument*/NULL);
		DataAxe_t3107820260 * L_8 = __this->get_m_data_2();
		NullCheck(L_8);
		float L_9 = L_8->get_size_4();
		if ((!(((float)L_4) < ((float)((-((float)((float)((float)((float)L_7/(float)(2.0f)))-(float)((float)((float)L_9/(float)(2.0f)))))))))))
		{
			goto IL_0061;
		}
	}
	{
		DataChapter_t925677859 * L_10 = V_0;
		NullCheck(L_10);
		Rect_t1525428817 * L_11 = L_10->get_address_of_rect_2();
		float L_12 = Rect_get_width_m2824209432(L_11, /*hidden argument*/NULL);
		DataAxe_t3107820260 * L_13 = __this->get_m_data_2();
		NullCheck(L_13);
		float L_14 = L_13->get_size_4();
		V_1 = ((-((float)((float)((float)((float)L_12/(float)(2.0f)))-(float)((float)((float)L_14/(float)(2.0f)))))));
	}

IL_0061:
	{
		float L_15 = V_1;
		DataChapter_t925677859 * L_16 = V_0;
		NullCheck(L_16);
		Rect_t1525428817 * L_17 = L_16->get_address_of_rect_2();
		float L_18 = Rect_get_width_m2824209432(L_17, /*hidden argument*/NULL);
		DataAxe_t3107820260 * L_19 = __this->get_m_data_2();
		NullCheck(L_19);
		float L_20 = L_19->get_size_4();
		if ((!(((float)L_15) > ((float)((float)((float)((float)((float)L_18/(float)(2.0f)))-(float)((float)((float)L_20/(float)(2.0f)))))))))
		{
			goto IL_00ae;
		}
	}
	{
		DataChapter_t925677859 * L_21 = V_0;
		NullCheck(L_21);
		Rect_t1525428817 * L_22 = L_21->get_address_of_rect_2();
		float L_23 = Rect_get_width_m2824209432(L_22, /*hidden argument*/NULL);
		DataAxe_t3107820260 * L_24 = __this->get_m_data_2();
		NullCheck(L_24);
		float L_25 = L_24->get_size_4();
		V_1 = ((float)((float)((float)((float)L_23/(float)(2.0f)))-(float)((float)((float)L_25/(float)(2.0f)))));
	}

IL_00ae:
	{
		Transform_t284553113 * L_26 = __this->get_m_tr_3();
		float L_27 = V_1;
		DataAxe_t3107820260 * L_28 = __this->get_m_data_2();
		NullCheck(L_28);
		float L_29 = L_28->get_throw_height_5();
		Vector3_t3525329789  L_30;
		memset(&L_30, 0, sizeof(L_30));
		Vector3__ctor_m2926210380(&L_30, L_27, L_29, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m3111394108(L_26, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Axe::InitVelocityByPower(System.Single)
extern "C"  void Axe_InitVelocityByPower_m3963918191 (Axe_t66286 * __this, float ___power0, const MethodInfo* method)
{
	{
		DataAxe_t3107820260 * L_0 = __this->get_m_data_2();
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = L_0->get_min_velocity_19();
		DataAxe_t3107820260 * L_2 = __this->get_m_data_2();
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = L_2->get_max_velocity_18();
		DataAxe_t3107820260 * L_4 = __this->get_m_data_2();
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = L_4->get_min_velocity_19();
		Vector3_t3525329789  L_6 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		float L_7 = ___power0;
		Vector3_t3525329789  L_8 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t3525329789  L_9 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_1, L_8, /*hidden argument*/NULL);
		__this->set_m_velocity_12(L_9);
		return;
	}
}
// System.Void Axe::InitVelocityByDistance(System.Single)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t Axe_InitVelocityByDistance_m2030215339_MetadataUsageId;
extern "C"  void Axe_InitVelocityByDistance_m2030215339 (Axe_t66286 * __this, float ___distance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_InitVelocityByDistance_m2030215339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		DataAxe_t3107820260 * L_0 = __this->get_m_data_2();
		NullCheck(L_0);
		float L_1 = L_0->get_gravity_16();
		float L_2 = ___distance0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_3 = powf(L_2, (2.0f));
		float L_4 = ___distance0;
		DataAxe_t3107820260 * L_5 = __this->get_m_data_2();
		NullCheck(L_5);
		float L_6 = L_5->get_sin_14();
		DataAxe_t3107820260 * L_7 = __this->get_m_data_2();
		NullCheck(L_7);
		float L_8 = L_7->get_cos_15();
		DataAxe_t3107820260 * L_9 = __this->get_m_data_2();
		NullCheck(L_9);
		float L_10 = L_9->get_throw_height_5();
		DataAxe_t3107820260 * L_11 = __this->get_m_data_2();
		NullCheck(L_11);
		float L_12 = L_11->get_cos_15();
		float L_13 = powf(L_12, (2.0f));
		float L_14 = sqrtf(((float)((float)((float)((float)((float)((float)L_1*(float)L_3))/(float)(2.0f)))/(float)((float)((float)((float)((float)((float)((float)L_4*(float)L_6))*(float)L_8))+(float)((float)((float)L_10*(float)L_13)))))));
		V_0 = L_14;
		Vector3_t3525329789 * L_15 = __this->get_address_of_m_velocity_12();
		L_15->set_x_1((0.0f));
		Vector3_t3525329789 * L_16 = __this->get_address_of_m_velocity_12();
		float L_17 = V_0;
		DataAxe_t3107820260 * L_18 = __this->get_m_data_2();
		NullCheck(L_18);
		float L_19 = L_18->get_sin_14();
		L_16->set_y_2(((float)((float)L_17*(float)L_19)));
		Vector3_t3525329789 * L_20 = __this->get_address_of_m_velocity_12();
		float L_21 = V_0;
		DataAxe_t3107820260 * L_22 = __this->get_m_data_2();
		NullCheck(L_22);
		float L_23 = L_22->get_cos_15();
		L_20->set_z_3(((float)((float)L_21*(float)L_23)));
		return;
	}
}
// System.Void Axe::InitRotate(System.Single)
extern "C"  void Axe_InitRotate_m3977709113 (Axe_t66286 * __this, float ___velocity_z0, const MethodInfo* method)
{
	{
		float L_0 = ___velocity_z0;
		DataAxe_t3107820260 * L_1 = __this->get_m_data_2();
		NullCheck(L_1);
		float L_2 = L_1->get_rotation_17();
		__this->set_m_rotate_14(((float)((float)L_0*(float)L_2)));
		return;
	}
}
// System.Void Axe::Update()
extern "C"  void Axe_Update_m3618508400 (Axe_t66286 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t1891715979  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		float L_0 = __this->get_m_time_11();
		float L_1 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_time_11(((float)((float)L_0+(float)L_1)));
		VirtActionInvoker0::Invoke(13 /* System.Void Axe::UpdateSimplePhysics() */, __this);
		Transform_t284553113 * L_2 = __this->get_m_axe_tr_5();
		Vector3_t3525329789  L_3 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_m_rotate_14();
		NullCheck(L_2);
		Transform_Rotate_m4090005356(L_2, L_3, ((float)((float)L_4*(float)((-L_5)))), /*hidden argument*/NULL);
		Transform_t284553113 * L_6 = __this->get_m_shadow_tr_7();
		Transform_t284553113 * L_7 = __this->get_m_tr_3();
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = (&V_0)->get_y_2();
		Vector3_t3525329789  L_10;
		memset(&L_10, 0, sizeof(L_10));
		Vector3__ctor_m2926210380(&L_10, (0.0f), ((-L_9)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localPosition_m3504330903(L_6, L_10, /*hidden argument*/NULL);
		Transform_t284553113 * L_11 = __this->get_m_shadow_tr_7();
		Transform_t284553113 * L_12 = __this->get_m_axe_tr_5();
		NullCheck(L_12);
		Quaternion_t1891715979  L_13 = Transform_get_rotation_m11483428(L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		Vector3_t3525329789  L_14 = Quaternion_get_eulerAngles_m997303795((&V_1), /*hidden argument*/NULL);
		V_2 = L_14;
		float L_15 = (&V_2)->get_y_2();
		Quaternion_t1891715979  L_16 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (90.0f), L_15, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_rotation_m1525803229(L_11, L_16, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Axe::UpdateSimplePhysics()
extern "C"  void Axe_UpdateSimplePhysics_m2236081863 (Axe_t66286 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector3_t3525329789  L_0 = __this->get_m_velocity_12();
		float L_1 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = (&V_0)->get_y_2();
		Transform_t284553113 * L_4 = __this->get_m_tr_3();
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_y_2();
		if ((!(((float)((-L_3))) > ((float)L_6))))
		{
			goto IL_0077;
		}
	}
	{
		Transform_t284553113 * L_7 = __this->get_m_tr_3();
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = (&V_2)->get_y_2();
		float L_10 = (&V_0)->get_z_3();
		float L_11 = (&V_0)->get_y_2();
		(&V_0)->set_z_3(((float)((float)((float)((float)((-L_9))*(float)L_10))/(float)L_11)));
		Transform_t284553113 * L_12 = __this->get_m_tr_3();
		NullCheck(L_12);
		Vector3_t3525329789  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		float L_14 = (&V_3)->get_y_2();
		(&V_0)->set_y_2(((-L_14)));
	}

IL_0077:
	{
		Transform_t284553113 * L_15 = __this->get_m_tr_3();
		Vector3_t3525329789  L_16 = V_0;
		NullCheck(L_15);
		Transform_Translate_m2849099360(L_15, L_16, /*hidden argument*/NULL);
		Transform_t284553113 * L_17 = __this->get_m_tr_3();
		NullCheck(L_17);
		Vector3_t3525329789  L_18 = Transform_get_position_m2211398607(L_17, /*hidden argument*/NULL);
		V_4 = L_18;
		float L_19 = (&V_4)->get_y_2();
		if ((!(((float)L_19) <= ((float)(0.0f)))))
		{
			goto IL_00a7;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void Axe::OnCollisionWater() */, __this);
	}

IL_00a7:
	{
		Vector3_t3525329789 * L_20 = __this->get_address_of_m_velocity_12();
		Vector3_t3525329789 * L_21 = L_20;
		float L_22 = L_21->get_y_2();
		DataAxe_t3107820260 * L_23 = __this->get_m_data_2();
		NullCheck(L_23);
		float L_24 = L_23->get_gravity_16();
		float L_25 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_21->set_y_2(((float)((float)L_22-(float)((float)((float)L_24*(float)L_25)))));
		return;
	}
}
// System.Void Axe::UpdateNormalPhysics()
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t Axe_UpdateNormalPhysics_m3906610994_MetadataUsageId;
extern "C"  void Axe_UpdateNormalPhysics_m3906610994 (Axe_t66286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_UpdateNormalPhysics_m3906610994_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t3525329789 * L_0 = __this->get_address_of_m_velocity_12();
		float L_1 = L_0->get_x_1();
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_x_1(((float)((float)L_1*(float)L_2)));
		Vector3_t3525329789 * L_3 = __this->get_address_of_m_velocity_12();
		float L_4 = L_3->get_z_3();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_z_3(((float)((float)L_4*(float)L_5)));
		Vector3_t3525329789 * L_6 = __this->get_address_of_m_velocity_12();
		float L_7 = L_6->get_y_2();
		float L_8 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataAxe_t3107820260 * L_9 = __this->get_m_data_2();
		NullCheck(L_9);
		float L_10 = L_9->get_gravity_16();
		float L_11 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_12 = powf(L_11, (2.0f));
		(&V_0)->set_y_2(((float)((float)((float)((float)L_7*(float)L_8))+(float)((float)((float)((float)((float)L_10*(float)L_12))/(float)(2.0f))))));
		float L_13 = (&V_0)->get_y_2();
		Transform_t284553113 * L_14 = __this->get_m_tr_3();
		NullCheck(L_14);
		Vector3_t3525329789  L_15 = Transform_get_position_m2211398607(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = (&V_1)->get_y_2();
		if ((!(((float)((-L_13))) > ((float)L_16))))
		{
			goto IL_00d8;
		}
	}
	{
		Transform_t284553113 * L_17 = __this->get_m_tr_3();
		NullCheck(L_17);
		Vector3_t3525329789  L_18 = Transform_get_position_m2211398607(L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		float L_19 = (&V_2)->get_y_2();
		float L_20 = (&V_0)->get_z_3();
		float L_21 = (&V_0)->get_y_2();
		(&V_0)->set_z_3(((float)((float)((float)((float)((-L_19))*(float)L_20))/(float)L_21)));
		Transform_t284553113 * L_22 = __this->get_m_tr_3();
		NullCheck(L_22);
		Vector3_t3525329789  L_23 = Transform_get_position_m2211398607(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		float L_24 = (&V_3)->get_y_2();
		(&V_0)->set_y_2(((-L_24)));
	}

IL_00d8:
	{
		Transform_t284553113 * L_25 = __this->get_m_tr_3();
		Vector3_t3525329789  L_26 = V_0;
		NullCheck(L_25);
		Transform_Translate_m2849099360(L_25, L_26, /*hidden argument*/NULL);
		Transform_t284553113 * L_27 = __this->get_m_tr_3();
		NullCheck(L_27);
		Vector3_t3525329789  L_28 = Transform_get_position_m2211398607(L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		float L_29 = (&V_4)->get_y_2();
		if ((!(((float)L_29) <= ((float)(0.0f)))))
		{
			goto IL_0108;
		}
	}
	{
		VirtActionInvoker0::Invoke(15 /* System.Void Axe::OnCollisionWater() */, __this);
	}

IL_0108:
	{
		Vector3_t3525329789 * L_30 = __this->get_address_of_m_velocity_12();
		Vector3_t3525329789 * L_31 = __this->get_address_of_m_velocity_12();
		float L_32 = L_31->get_y_2();
		DataAxe_t3107820260 * L_33 = __this->get_m_data_2();
		NullCheck(L_33);
		float L_34 = L_33->get_gravity_16();
		float L_35 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_30->set_y_2(((float)((float)L_32-(float)((float)((float)L_34*(float)L_35)))));
		return;
	}
}
// System.Void Axe::OnCollisionWater()
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t Axe_OnCollisionWater_m255246379_MetadataUsageId;
extern "C"  void Axe_OnCollisionWater_m255246379 (Axe_t66286 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_OnCollisionWater_m255246379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Diver_t66044126 * V_0 = NULL;
	{
		bool L_0 = __this->get_m_enabled_10();
		if (!L_0)
		{
			goto IL_0071;
		}
	}
	{
		__this->set_m_enabled_10((bool)0);
		GamePlay_t2590336614 * L_1 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_1);
		PatternManager_t2286577181 * L_2 = GamePlay_get_patternManager_m3261677077(L_1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t284553113 * L_4 = GameObject_get_transform_m1278640159(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		DataAxe_t3107820260 * L_6 = __this->get_m_data_2();
		NullCheck(L_6);
		Vector3_t3525329789  L_7 = L_6->get_collider_size_10();
		NullCheck(L_2);
		Diver_t66044126 * L_8 = PatternManager_GetHitDiver_m463736356(L_2, L_5, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Diver_t66044126 * L_9 = V_0;
		bool L_10 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_9, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0059;
		}
	}
	{
		GamePlay_t2590336614 * L_11 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_11);
		GamePlay_HitWater_m3876954609(L_11, __this, /*hidden argument*/NULL);
		goto IL_0071;
	}

IL_0059:
	{
		GamePlay_t2590336614 * L_12 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		Diver_t66044126 * L_13 = V_0;
		NullCheck(L_12);
		GamePlay_HitDiver_m148186040(L_12, __this, L_13, /*hidden argument*/NULL);
		Diver_t66044126 * L_14 = V_0;
		NullCheck(L_14);
		Pattern_t873562992 * L_15 = Character_get_pattern_m1274480394(L_14, /*hidden argument*/NULL);
		Diver_t66044126 * L_16 = V_0;
		NullCheck(L_15);
		Pattern_RemoveDiver_m455503873(L_15, L_16, /*hidden argument*/NULL);
	}

IL_0071:
	{
		GamePlay_t2590336614 * L_17 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_17);
		Stage_t80204510 * L_18 = GamePlay_get_stage_m3096959283(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Stage_RemoveAxe_m300927899(L_18, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Axe::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Axe_OnTriggerEnter_m1508574299_MetadataUsageId;
extern "C"  void Axe_OnTriggerEnter_m1508574299 (Axe_t66286 * __this, Collider_t955670625 * ___coll0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_OnTriggerEnter_m1508574299_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		Collider_t955670625 * L_0 = ___coll0;
		NullCheck(L_0);
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = __this->get_m_enabled_10();
		if (L_2)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		GameObject_t4012695102 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = GameObject_get_tag_m211612200(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		String_t* L_5 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_TAG_MONSTER_11();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0029;
		}
	}
	{
		return;
	}

IL_0029:
	{
		GameObject_t4012695102 * L_7 = V_0;
		VirtActionInvoker1< GameObject_t4012695102 * >::Invoke(16 /* System.Void Axe::Hit(UnityEngine.GameObject) */, __this, L_7);
		return;
	}
}
// System.Void Axe::Hit(UnityEngine.GameObject)
extern const Il2CppType* Monster_t2901270458_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Monster_t2901270458_il2cpp_TypeInfo_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t Axe_Hit_m2761954214_MetadataUsageId;
extern "C"  void Axe_Hit_m2761954214 (Axe_t66286 * __this, GameObject_t4012695102 * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Axe_Hit_m2761954214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Monster_t2901270458 * V_0 = NULL;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		GameObject_t4012695102 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Monster_t2901270458_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t2126946602 * L_2 = GameObject_GetComponentInParent_m434474382(L_0, L_1, /*hidden argument*/NULL);
		V_0 = ((Monster_t2901270458 *)IsInstClass(L_2, Monster_t2901270458_il2cpp_TypeInfo_var));
		Monster_t2901270458 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0023;
		}
	}
	{
		return;
	}

IL_0023:
	{
		Monster_t2901270458 * L_5 = V_0;
		int32_t L_6 = __this->get_m_damage_13();
		NullCheck(L_5);
		VirtActionInvoker1< int32_t >::Invoke(14 /* System.Void Monster::Hit(System.Int32) */, L_5, L_6);
		Monster_t2901270458 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = Monster_get_health_m449321322(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004b;
		}
	}
	{
		GamePlay_t2590336614 * L_9 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		Monster_t2901270458 * L_10 = V_0;
		NullCheck(L_9);
		GamePlay_KillMonster_m2354964455(L_9, L_10, __this, /*hidden argument*/NULL);
		goto IL_0057;
	}

IL_004b:
	{
		GamePlay_t2590336614 * L_11 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		Monster_t2901270458 * L_12 = V_0;
		NullCheck(L_11);
		GamePlay_HitMonster_m4135026560(L_11, L_12, __this, /*hidden argument*/NULL);
	}

IL_0057:
	{
		BoxCollider_t131631884 * L_13 = __this->get_m_collider_8();
		NullCheck(L_13);
		Collider_set_enabled_m2575670866(L_13, (bool)0, /*hidden argument*/NULL);
		__this->set_m_enabled_10((bool)0);
		Transform_t284553113 * L_14 = __this->get_m_tr_3();
		NullCheck(L_14);
		Vector3_t3525329789  L_15 = Transform_get_position_m2211398607(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		float L_16 = (&V_1)->get_x_1();
		if ((!(((float)L_16) < ((float)(0.0f)))))
		{
			goto IL_00b2;
		}
	}
	{
		Vector3_t3525329789 * L_17 = __this->get_address_of_m_velocity_12();
		GamePlay_t2590336614 * L_18 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_18);
		DataChapter_t925677859 * L_19 = GamePlay_get_dataChapter_m1400870973(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		Rect_t1525428817 * L_20 = L_19->get_address_of_rect_2();
		float L_21 = Rect_get_width_m2824209432(L_20, /*hidden argument*/NULL);
		L_17->set_x_1(((float)((float)((-L_21))*(float)(1.5f))));
		goto IL_00d7;
	}

IL_00b2:
	{
		Vector3_t3525329789 * L_22 = __this->get_address_of_m_velocity_12();
		GamePlay_t2590336614 * L_23 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_23);
		DataChapter_t925677859 * L_24 = GamePlay_get_dataChapter_m1400870973(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Rect_t1525428817 * L_25 = L_24->get_address_of_rect_2();
		float L_26 = Rect_get_width_m2824209432(L_25, /*hidden argument*/NULL);
		L_22->set_x_1(((float)((float)L_26*(float)(1.5f))));
	}

IL_00d7:
	{
		Vector3_t3525329789 * L_27 = __this->get_address_of_m_velocity_12();
		Vector3_t3525329789 * L_28 = __this->get_address_of_m_velocity_12();
		float L_29 = L_28->get_y_2();
		L_27->set_y_2(((float)((float)((-L_29))*(float)(1.5f))));
		Vector3_t3525329789 * L_30 = __this->get_address_of_m_velocity_12();
		Vector3_t3525329789 * L_31 = __this->get_address_of_m_velocity_12();
		float L_32 = L_31->get_z_3();
		L_30->set_z_3(((float)((float)L_32*(float)(1.5f))));
		return;
	}
}
// System.Void AxeSlot::.ctor()
extern "C"  void AxeSlot__ctor_m124420831 (AxeSlot_t1089550860 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.UInt32 AxeSlot::get_id()
extern "C"  uint32_t AxeSlot_get_id_m1552903246 (AxeSlot_t1089550860 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_m_id_0();
		return L_0;
	}
}
// DataAxe AxeSlot::get_data()
extern "C"  DataAxe_t3107820260 * AxeSlot_get_data_m3596026761 (AxeSlot_t1089550860 * __this, const MethodInfo* method)
{
	{
		DataAxe_t3107820260 * L_0 = __this->get_m_data_1();
		return L_0;
	}
}
// System.Void AxeSlot::Clear()
extern "C"  void AxeSlot_Clear_m1825521418 (AxeSlot_t1089550860 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Boolean AxeSlot::SetAxe(System.UInt32)
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t AxeSlot_SetAxe_m301184637_MetadataUsageId;
extern "C"  bool AxeSlot_SetAxe_m301184637 (AxeSlot_t1089550860 * __this, uint32_t ___axe_id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AxeSlot_SetAxe_m301184637_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = ___axe_id0;
		__this->set_m_id_0(L_0);
		DataAxes_t1853147663 * L_1 = Data_get_axe_m1914057446(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint32_t L_2 = ___axe_id0;
		NullCheck(L_1);
		DataAxe_t3107820260 * L_3 = DataAxes_getData_m1458414431(L_1, L_2, /*hidden argument*/NULL);
		__this->set_m_data_1(L_3);
		DataAxe_t3107820260 * L_4 = __this->get_m_data_1();
		if (L_4)
		{
			goto IL_0025;
		}
	}
	{
		return (bool)0;
	}

IL_0025:
	{
		GamePlay_t2590336614 * L_5 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_5);
		DataChapter_t925677859 * L_6 = GamePlay_get_dataChapter_m1400870973(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		Rect_t1525428817 * L_7 = L_6->get_address_of_rect_2();
		float L_8 = Rect_get_height_m2154960823(L_7, /*hidden argument*/NULL);
		AxeSlot_CalculateVelocity_m475350539(__this, L_8, /*hidden argument*/NULL);
		return (bool)1;
	}
}
// System.Void AxeSlot::CalculateVelocity(System.Single)
extern "C"  void AxeSlot_CalculateVelocity_m475350539 (AxeSlot_t1089550860 * __this, float ___height0, const MethodInfo* method)
{
	{
		DataAxe_t3107820260 * L_0 = __this->get_m_data_1();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		DataAxe_t3107820260 * L_1 = __this->get_m_data_1();
		float L_2 = ___height0;
		NullCheck(L_1);
		DataAxe_CalculateVelocity_m3396167907(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void BMFont::.ctor()
extern Il2CppClass* List_1_t1516011674_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t1209863488_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m995336704_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2439733974_MethodInfo_var;
extern const uint32_t BMFont__ctor_m2683632289_MetadataUsageId;
extern "C"  void BMFont__ctor_m2683632289 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BMFont__ctor_m2683632289_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_mSize_0(((int32_t)16));
		List_1_t1516011674 * L_0 = (List_1_t1516011674 *)il2cpp_codegen_object_new(List_1_t1516011674_il2cpp_TypeInfo_var);
		List_1__ctor_m995336704(L_0, /*hidden argument*/List_1__ctor_m995336704_MethodInfo_var);
		__this->set_mSaved_5(L_0);
		Dictionary_2_t1209863488 * L_1 = (Dictionary_2_t1209863488 *)il2cpp_codegen_object_new(Dictionary_2_t1209863488_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2439733974(L_1, /*hidden argument*/Dictionary_2__ctor_m2439733974_MethodInfo_var);
		__this->set_mDict_6(L_1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean BMFont::get_isValid()
extern "C"  bool BMFont_get_isValid_m1354584564 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	{
		List_1_t1516011674 * L_0 = __this->get_mSaved_5();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<BMGlyph>::get_Count() */, L_0);
		return (bool)((((int32_t)L_1) > ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 BMFont::get_charSize()
extern "C"  int32_t BMFont_get_charSize_m3324309297 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mSize_0();
		return L_0;
	}
}
// System.Void BMFont::set_charSize(System.Int32)
extern "C"  void BMFont_set_charSize_m3463653864 (BMFont_t1962830650 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_mSize_0(L_0);
		return;
	}
}
// System.Int32 BMFont::get_baseOffset()
extern "C"  int32_t BMFont_get_baseOffset_m4243423230 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mBase_1();
		return L_0;
	}
}
// System.Void BMFont::set_baseOffset(System.Int32)
extern "C"  void BMFont_set_baseOffset_m3493449141 (BMFont_t1962830650 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_mBase_1(L_0);
		return;
	}
}
// System.Int32 BMFont::get_texWidth()
extern "C"  int32_t BMFont_get_texWidth_m678192793 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mWidth_2();
		return L_0;
	}
}
// System.Void BMFont::set_texWidth(System.Int32)
extern "C"  void BMFont_set_texWidth_m3413820752 (BMFont_t1962830650 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_mWidth_2(L_0);
		return;
	}
}
// System.Int32 BMFont::get_texHeight()
extern "C"  int32_t BMFont_get_texHeight_m52954454 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mHeight_3();
		return L_0;
	}
}
// System.Void BMFont::set_texHeight(System.Int32)
extern "C"  void BMFont_set_texHeight_m4070080385 (BMFont_t1962830650 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_mHeight_3(L_0);
		return;
	}
}
// System.Int32 BMFont::get_glyphCount()
extern "C"  int32_t BMFont_get_glyphCount_m2598502589 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = BMFont_get_isValid_m1354584564(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		List_1_t1516011674 * L_1 = __this->get_mSaved_5();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<BMGlyph>::get_Count() */, L_1);
		G_B3_0 = L_2;
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.String BMFont::get_spriteName()
extern "C"  String_t* BMFont_get_spriteName_m3156205119 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mSpriteName_4();
		return L_0;
	}
}
// System.Void BMFont::set_spriteName(System.String)
extern "C"  void BMFont_set_spriteName_m3412845938 (BMFont_t1962830650 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_mSpriteName_4(L_0);
		return;
	}
}
// System.Collections.Generic.List`1<BMGlyph> BMFont::get_glyphs()
extern "C"  List_1_t1516011674 * BMFont_get_glyphs_m1206383509 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	{
		List_1_t1516011674 * L_0 = __this->get_mSaved_5();
		return L_0;
	}
}
// BMGlyph BMFont::GetGlyph(System.Int32,System.Boolean)
extern Il2CppClass* BMGlyph_t719052705_il2cpp_TypeInfo_var;
extern const uint32_t BMFont_GetGlyph_m3108008609_MetadataUsageId;
extern "C"  BMGlyph_t719052705 * BMFont_GetGlyph_m3108008609 (BMFont_t1962830650 * __this, int32_t ___index0, bool ___createIfMissing1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BMFont_GetGlyph_m3108008609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BMGlyph_t719052705 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	BMGlyph_t719052705 * V_3 = NULL;
	{
		V_0 = (BMGlyph_t719052705 *)NULL;
		Dictionary_2_t1209863488 * L_0 = __this->get_mDict_6();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,BMGlyph>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_004f;
		}
	}
	{
		V_1 = 0;
		List_1_t1516011674 * L_2 = __this->get_mSaved_5();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<BMGlyph>::get_Count() */, L_2);
		V_2 = L_3;
		goto IL_0048;
	}

IL_0025:
	{
		List_1_t1516011674 * L_4 = __this->get_mSaved_5();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		BMGlyph_t719052705 * L_6 = VirtFuncInvoker1< BMGlyph_t719052705 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<BMGlyph>::get_Item(System.Int32) */, L_4, L_5);
		V_3 = L_6;
		Dictionary_2_t1209863488 * L_7 = __this->get_mDict_6();
		BMGlyph_t719052705 * L_8 = V_3;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_index_0();
		BMGlyph_t719052705 * L_10 = V_3;
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, BMGlyph_t719052705 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,BMGlyph>::Add(!0,!1) */, L_7, L_9, L_10);
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0048:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_2;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0025;
		}
	}

IL_004f:
	{
		Dictionary_2_t1209863488 * L_14 = __this->get_mDict_6();
		int32_t L_15 = ___index0;
		NullCheck(L_14);
		bool L_16 = VirtFuncInvoker2< bool, int32_t, BMGlyph_t719052705 ** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,BMGlyph>::TryGetValue(!0,!1&) */, L_14, L_15, (&V_0));
		if (L_16)
		{
			goto IL_008e;
		}
	}
	{
		bool L_17 = ___createIfMissing1;
		if (!L_17)
		{
			goto IL_008e;
		}
	}
	{
		BMGlyph_t719052705 * L_18 = (BMGlyph_t719052705 *)il2cpp_codegen_object_new(BMGlyph_t719052705_il2cpp_TypeInfo_var);
		BMGlyph__ctor_m694703338(L_18, /*hidden argument*/NULL);
		V_0 = L_18;
		BMGlyph_t719052705 * L_19 = V_0;
		int32_t L_20 = ___index0;
		NullCheck(L_19);
		L_19->set_index_0(L_20);
		List_1_t1516011674 * L_21 = __this->get_mSaved_5();
		BMGlyph_t719052705 * L_22 = V_0;
		NullCheck(L_21);
		VirtActionInvoker1< BMGlyph_t719052705 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<BMGlyph>::Add(!0) */, L_21, L_22);
		Dictionary_2_t1209863488 * L_23 = __this->get_mDict_6();
		int32_t L_24 = ___index0;
		BMGlyph_t719052705 * L_25 = V_0;
		NullCheck(L_23);
		VirtActionInvoker2< int32_t, BMGlyph_t719052705 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,BMGlyph>::Add(!0,!1) */, L_23, L_24, L_25);
	}

IL_008e:
	{
		BMGlyph_t719052705 * L_26 = V_0;
		return L_26;
	}
}
// BMGlyph BMFont::GetGlyph(System.Int32)
extern "C"  BMGlyph_t719052705 * BMFont_GetGlyph_m1989279068 (BMFont_t1962830650 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___index0;
		BMGlyph_t719052705 * L_1 = BMFont_GetGlyph_m3108008609(__this, L_0, (bool)0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void BMFont::Clear()
extern "C"  void BMFont_Clear_m89765580 (BMFont_t1962830650 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t1209863488 * L_0 = __this->get_mDict_6();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,BMGlyph>::Clear() */, L_0);
		List_1_t1516011674 * L_1 = __this->get_mSaved_5();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<BMGlyph>::Clear() */, L_1);
		return;
	}
}
// System.Void BMFont::Trim(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void BMFont_Trim_m3028559553 (BMFont_t1962830650 * __this, int32_t ___xMin0, int32_t ___yMin1, int32_t ___xMax2, int32_t ___yMax3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	BMGlyph_t719052705 * V_2 = NULL;
	{
		bool L_0 = BMFont_get_isValid_m1354584564(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0047;
		}
	}
	{
		V_0 = 0;
		List_1_t1516011674 * L_1 = __this->get_mSaved_5();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<BMGlyph>::get_Count() */, L_1);
		V_1 = L_2;
		goto IL_0040;
	}

IL_001e:
	{
		List_1_t1516011674 * L_3 = __this->get_mSaved_5();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		BMGlyph_t719052705 * L_5 = VirtFuncInvoker1< BMGlyph_t719052705 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<BMGlyph>::get_Item(System.Int32) */, L_3, L_4);
		V_2 = L_5;
		BMGlyph_t719052705 * L_6 = V_2;
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		BMGlyph_t719052705 * L_7 = V_2;
		int32_t L_8 = ___xMin0;
		int32_t L_9 = ___yMin1;
		int32_t L_10 = ___xMax2;
		int32_t L_11 = ___yMax3;
		NullCheck(L_7);
		BMGlyph_Trim_m2253946058(L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_001e;
		}
	}

IL_0047:
	{
		return;
	}
}
// System.Void BMGlyph::.ctor()
extern "C"  void BMGlyph__ctor_m694703338 (BMGlyph_t719052705 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 BMGlyph::GetKerning(System.Int32)
extern "C"  int32_t BMGlyph_GetKerning_m2944207539 (BMGlyph_t719052705 * __this, int32_t ___previousChar0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		List_1_t3644373756 * L_0 = __this->get_kerning_9();
		if (!L_0)
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_1 = ___previousChar0;
		if (!L_1)
		{
			goto IL_0050;
		}
	}
	{
		V_0 = 0;
		List_1_t3644373756 * L_2 = __this->get_kerning_9();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count() */, L_2);
		V_1 = L_3;
		goto IL_0049;
	}

IL_0024:
	{
		List_1_t3644373756 * L_4 = __this->get_kerning_9();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		int32_t L_6 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32) */, L_4, L_5);
		int32_t L_7 = ___previousChar0;
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0045;
		}
	}
	{
		List_1_t3644373756 * L_8 = __this->get_kerning_9();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		int32_t L_10 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32) */, L_8, ((int32_t)((int32_t)L_9+(int32_t)1)));
		return L_10;
	}

IL_0045:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)2));
	}

IL_0049:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0024;
		}
	}

IL_0050:
	{
		return 0;
	}
}
// System.Void BMGlyph::SetKerning(System.Int32,System.Int32)
extern Il2CppClass* List_1_t3644373756_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1634217978_MethodInfo_var;
extern const uint32_t BMGlyph_SetKerning_m504528258_MetadataUsageId;
extern "C"  void BMGlyph_SetKerning_m504528258 (BMGlyph_t719052705 * __this, int32_t ___previousChar0, int32_t ___amount1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BMGlyph_SetKerning_m504528258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t3644373756 * L_0 = __this->get_kerning_9();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		List_1_t3644373756 * L_1 = (List_1_t3644373756 *)il2cpp_codegen_object_new(List_1_t3644373756_il2cpp_TypeInfo_var);
		List_1__ctor_m1634217978(L_1, /*hidden argument*/List_1__ctor_m1634217978_MethodInfo_var);
		__this->set_kerning_9(L_1);
	}

IL_0016:
	{
		V_0 = 0;
		goto IL_0043;
	}

IL_001d:
	{
		List_1_t3644373756 * L_2 = __this->get_kerning_9();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		int32_t L_4 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32) */, L_2, L_3);
		int32_t L_5 = ___previousChar0;
		if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
		{
			goto IL_003f;
		}
	}
	{
		List_1_t3644373756 * L_6 = __this->get_kerning_9();
		int32_t L_7 = V_0;
		int32_t L_8 = ___amount1;
		NullCheck(L_6);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(32 /* System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,!0) */, L_6, ((int32_t)((int32_t)L_7+(int32_t)1)), L_8);
		return;
	}

IL_003f:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)2));
	}

IL_0043:
	{
		int32_t L_10 = V_0;
		List_1_t3644373756 * L_11 = __this->get_kerning_9();
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count() */, L_11);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001d;
		}
	}
	{
		List_1_t3644373756 * L_13 = __this->get_kerning_9();
		int32_t L_14 = ___previousChar0;
		NullCheck(L_13);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_13, L_14);
		List_1_t3644373756 * L_15 = __this->get_kerning_9();
		int32_t L_16 = ___amount1;
		NullCheck(L_15);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_15, L_16);
		return;
	}
}
// System.Void BMGlyph::Trim(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void BMGlyph_Trim_m2253946058 (BMGlyph_t719052705 * __this, int32_t ___xMin0, int32_t ___yMin1, int32_t ___xMax2, int32_t ___yMax3, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_x_1();
		int32_t L_1 = __this->get_width_3();
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		int32_t L_2 = __this->get_y_2();
		int32_t L_3 = __this->get_height_4();
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)L_3));
		int32_t L_4 = __this->get_x_1();
		int32_t L_5 = ___xMin0;
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_005b;
		}
	}
	{
		int32_t L_6 = ___xMin0;
		int32_t L_7 = __this->get_x_1();
		V_2 = ((int32_t)((int32_t)L_6-(int32_t)L_7));
		int32_t L_8 = __this->get_x_1();
		int32_t L_9 = V_2;
		__this->set_x_1(((int32_t)((int32_t)L_8+(int32_t)L_9)));
		int32_t L_10 = __this->get_width_3();
		int32_t L_11 = V_2;
		__this->set_width_3(((int32_t)((int32_t)L_10-(int32_t)L_11)));
		int32_t L_12 = __this->get_offsetX_5();
		int32_t L_13 = V_2;
		__this->set_offsetX_5(((int32_t)((int32_t)L_12+(int32_t)L_13)));
	}

IL_005b:
	{
		int32_t L_14 = __this->get_y_2();
		int32_t L_15 = ___yMin1;
		if ((((int32_t)L_14) >= ((int32_t)L_15)))
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_16 = ___yMin1;
		int32_t L_17 = __this->get_y_2();
		V_3 = ((int32_t)((int32_t)L_16-(int32_t)L_17));
		int32_t L_18 = __this->get_y_2();
		int32_t L_19 = V_3;
		__this->set_y_2(((int32_t)((int32_t)L_18+(int32_t)L_19)));
		int32_t L_20 = __this->get_height_4();
		int32_t L_21 = V_3;
		__this->set_height_4(((int32_t)((int32_t)L_20-(int32_t)L_21)));
		int32_t L_22 = __this->get_offsetY_6();
		int32_t L_23 = V_3;
		__this->set_offsetY_6(((int32_t)((int32_t)L_22+(int32_t)L_23)));
	}

IL_009a:
	{
		int32_t L_24 = V_0;
		int32_t L_25 = ___xMax2;
		if ((((int32_t)L_24) <= ((int32_t)L_25)))
		{
			goto IL_00b1;
		}
	}
	{
		int32_t L_26 = __this->get_width_3();
		int32_t L_27 = V_0;
		int32_t L_28 = ___xMax2;
		__this->set_width_3(((int32_t)((int32_t)L_26-(int32_t)((int32_t)((int32_t)L_27-(int32_t)L_28)))));
	}

IL_00b1:
	{
		int32_t L_29 = V_1;
		int32_t L_30 = ___yMax3;
		if ((((int32_t)L_29) <= ((int32_t)L_30)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_31 = __this->get_height_4();
		int32_t L_32 = V_1;
		int32_t L_33 = ___yMax3;
		__this->set_height_4(((int32_t)((int32_t)L_31-(int32_t)((int32_t)((int32_t)L_32-(int32_t)L_33)))));
	}

IL_00ca:
	{
		return;
	}
}
// System.Void BMSymbol::.ctor()
extern "C"  void BMSymbol__ctor_m2937308216 (BMSymbol_t1170982339 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 BMSymbol::get_length()
extern "C"  int32_t BMSymbol_get_length_m2924452201 (BMSymbol_t1170982339 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mLength_4();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1 = __this->get_sequence_0();
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m2979997331(L_1, /*hidden argument*/NULL);
		__this->set_mLength_4(L_2);
	}

IL_001c:
	{
		int32_t L_3 = __this->get_mLength_4();
		return L_3;
	}
}
// System.Int32 BMSymbol::get_offsetX()
extern "C"  int32_t BMSymbol_get_offsetX_m2899125028 (BMSymbol_t1170982339 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mOffsetX_5();
		return L_0;
	}
}
// System.Int32 BMSymbol::get_offsetY()
extern "C"  int32_t BMSymbol_get_offsetY_m2899125989 (BMSymbol_t1170982339 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mOffsetY_6();
		return L_0;
	}
}
// System.Int32 BMSymbol::get_width()
extern "C"  int32_t BMSymbol_get_width_m3035166213 (BMSymbol_t1170982339 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mWidth_7();
		return L_0;
	}
}
// System.Int32 BMSymbol::get_height()
extern "C"  int32_t BMSymbol_get_height_m104686442 (BMSymbol_t1170982339 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mHeight_8();
		return L_0;
	}
}
// System.Int32 BMSymbol::get_advance()
extern "C"  int32_t BMSymbol_get_advance_m4050680385 (BMSymbol_t1170982339 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mAdvance_9();
		return L_0;
	}
}
// UnityEngine.Rect BMSymbol::get_uvRect()
extern "C"  Rect_t1525428817  BMSymbol_get_uvRect_m3961952604 (BMSymbol_t1170982339 * __this, const MethodInfo* method)
{
	{
		Rect_t1525428817  L_0 = __this->get_mUV_10();
		return L_0;
	}
}
// System.Void BMSymbol::MarkAsChanged()
extern "C"  void BMSymbol_MarkAsChanged_m50955499 (BMSymbol_t1170982339 * __this, const MethodInfo* method)
{
	{
		__this->set_mIsValid_3((bool)0);
		return;
	}
}
// System.Boolean BMSymbol::Validate(UIAtlas)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t BMSymbol_Validate_m3490388837_MetadataUsageId;
extern "C"  bool BMSymbol_Validate_m3490388837 (BMSymbol_t1170982339 * __this, UIAtlas_t281921111 * ___atlas0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BMSymbol_Validate_m3490388837_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Texture_t1769722184 * V_0 = NULL;
	BMSymbol_t1170982339 * G_B7_0 = NULL;
	BMSymbol_t1170982339 * G_B6_0 = NULL;
	UISpriteData_t3578345923 * G_B8_0 = NULL;
	BMSymbol_t1170982339 * G_B8_1 = NULL;
	{
		UIAtlas_t281921111 * L_0 = ___atlas0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		return (bool)0;
	}

IL_000e:
	{
		bool L_2 = __this->get_mIsValid_3();
		if (L_2)
		{
			goto IL_0145;
		}
	}
	{
		String_t* L_3 = __this->get_spriteName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002b;
		}
	}
	{
		return (bool)0;
	}

IL_002b:
	{
		UIAtlas_t281921111 * L_5 = ___atlas0;
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		G_B6_0 = __this;
		if (!L_6)
		{
			G_B7_0 = __this;
			goto IL_0049;
		}
	}
	{
		UIAtlas_t281921111 * L_7 = ___atlas0;
		String_t* L_8 = __this->get_spriteName_1();
		NullCheck(L_7);
		UISpriteData_t3578345923 * L_9 = UIAtlas_GetSprite_m2191383547(L_7, L_8, /*hidden argument*/NULL);
		G_B8_0 = L_9;
		G_B8_1 = G_B6_0;
		goto IL_004a;
	}

IL_0049:
	{
		G_B8_0 = ((UISpriteData_t3578345923 *)(NULL));
		G_B8_1 = G_B7_0;
	}

IL_004a:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_mSprite_2(G_B8_0);
		UISpriteData_t3578345923 * L_10 = __this->get_mSprite_2();
		if (!L_10)
		{
			goto IL_0145;
		}
	}
	{
		UIAtlas_t281921111 * L_11 = ___atlas0;
		NullCheck(L_11);
		Texture_t1769722184 * L_12 = UIAtlas_get_texture_m4141329395(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		Texture_t1769722184 * L_13 = V_0;
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_13, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0079;
		}
	}
	{
		__this->set_mSprite_2((UISpriteData_t3578345923 *)NULL);
		goto IL_0145;
	}

IL_0079:
	{
		UISpriteData_t3578345923 * L_15 = __this->get_mSprite_2();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_x_1();
		UISpriteData_t3578345923 * L_17 = __this->get_mSprite_2();
		NullCheck(L_17);
		int32_t L_18 = L_17->get_y_2();
		UISpriteData_t3578345923 * L_19 = __this->get_mSprite_2();
		NullCheck(L_19);
		int32_t L_20 = L_19->get_width_3();
		UISpriteData_t3578345923 * L_21 = __this->get_mSprite_2();
		NullCheck(L_21);
		int32_t L_22 = L_21->get_height_4();
		Rect_t1525428817  L_23;
		memset(&L_23, 0, sizeof(L_23));
		Rect__ctor_m3291325233(&L_23, (((float)((float)L_16))), (((float)((float)L_18))), (((float)((float)L_20))), (((float)((float)L_22))), /*hidden argument*/NULL);
		__this->set_mUV_10(L_23);
		Rect_t1525428817  L_24 = __this->get_mUV_10();
		Texture_t1769722184 * L_25 = V_0;
		NullCheck(L_25);
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_25);
		Texture_t1769722184 * L_27 = V_0;
		NullCheck(L_27);
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_27);
		Rect_t1525428817  L_29 = NGUIMath_ConvertToTexCoords_m3130058204(NULL /*static, unused*/, L_24, L_26, L_28, /*hidden argument*/NULL);
		__this->set_mUV_10(L_29);
		UISpriteData_t3578345923 * L_30 = __this->get_mSprite_2();
		NullCheck(L_30);
		int32_t L_31 = L_30->get_paddingLeft_9();
		__this->set_mOffsetX_5(L_31);
		UISpriteData_t3578345923 * L_32 = __this->get_mSprite_2();
		NullCheck(L_32);
		int32_t L_33 = L_32->get_paddingTop_11();
		__this->set_mOffsetY_6(L_33);
		UISpriteData_t3578345923 * L_34 = __this->get_mSprite_2();
		NullCheck(L_34);
		int32_t L_35 = L_34->get_width_3();
		__this->set_mWidth_7(L_35);
		UISpriteData_t3578345923 * L_36 = __this->get_mSprite_2();
		NullCheck(L_36);
		int32_t L_37 = L_36->get_height_4();
		__this->set_mHeight_8(L_37);
		UISpriteData_t3578345923 * L_38 = __this->get_mSprite_2();
		NullCheck(L_38);
		int32_t L_39 = L_38->get_width_3();
		UISpriteData_t3578345923 * L_40 = __this->get_mSprite_2();
		NullCheck(L_40);
		int32_t L_41 = L_40->get_paddingLeft_9();
		UISpriteData_t3578345923 * L_42 = __this->get_mSprite_2();
		NullCheck(L_42);
		int32_t L_43 = L_42->get_paddingRight_10();
		__this->set_mAdvance_9(((int32_t)((int32_t)L_39+(int32_t)((int32_t)((int32_t)L_41+(int32_t)L_43)))));
		__this->set_mIsValid_3((bool)1);
	}

IL_0145:
	{
		UISpriteData_t3578345923 * L_44 = __this->get_mSprite_2();
		return (bool)((((int32_t)((((Il2CppObject*)(UISpriteData_t3578345923 *)L_44) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void ByteReader::.ctor(System.Byte[])
extern "C"  void ByteReader__ctor_m417508569 (ByteReader_t2446302219 * __this, ByteU5BU5D_t58506160* ___bytes0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_0 = ___bytes0;
		__this->set_mBuffer_0(L_0);
		return;
	}
}
// System.Void ByteReader::.ctor(UnityEngine.TextAsset)
extern "C"  void ByteReader__ctor_m3209705686 (ByteReader_t2446302219 * __this, TextAsset_t2461560304 * ___asset0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		TextAsset_t2461560304 * L_0 = ___asset0;
		NullCheck(L_0);
		ByteU5BU5D_t58506160* L_1 = TextAsset_get_bytes_m2395427488(L_0, /*hidden argument*/NULL);
		__this->set_mBuffer_0(L_1);
		return;
	}
}
// System.Void ByteReader::.cctor()
extern Il2CppClass* BetterList_1_t2465456914_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteReader_t2446302219_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1__ctor_m1660041216_MethodInfo_var;
extern const uint32_t ByteReader__cctor_m1589350589_MetadataUsageId;
extern "C"  void ByteReader__cctor_m1589350589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ByteReader__cctor_m1589350589_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BetterList_1_t2465456914 * L_0 = (BetterList_1_t2465456914 *)il2cpp_codegen_object_new(BetterList_1_t2465456914_il2cpp_TypeInfo_var);
		BetterList_1__ctor_m1660041216(L_0, /*hidden argument*/BetterList_1__ctor_m1660041216_MethodInfo_var);
		((ByteReader_t2446302219_StaticFields*)ByteReader_t2446302219_il2cpp_TypeInfo_var->static_fields)->set_mTemp_2(L_0);
		return;
	}
}
// ByteReader ByteReader::Open(System.String)
extern Il2CppClass* ByteU5BU5D_t58506160_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteReader_t2446302219_il2cpp_TypeInfo_var;
extern const uint32_t ByteReader_Open_m175599250_MetadataUsageId;
extern "C"  ByteReader_t2446302219 * ByteReader_Open_m175599250 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ByteReader_Open_m175599250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FileStream_t1527309539 * V_0 = NULL;
	ByteU5BU5D_t58506160* V_1 = NULL;
	{
		String_t* L_0 = ___path0;
		FileStream_t1527309539 * L_1 = File_OpenRead_m3104031109(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		FileStream_t1527309539 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0047;
		}
	}
	{
		FileStream_t1527309539 * L_3 = V_0;
		NullCheck(L_3);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(16 /* System.Int64 System.IO.FileStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_3, (((int64_t)((int64_t)0))), 2);
		FileStream_t1527309539 * L_4 = V_0;
		NullCheck(L_4);
		int64_t L_5 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.FileStream::get_Position() */, L_4);
		if ((int64_t)(L_5) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		V_1 = ((ByteU5BU5D_t58506160*)SZArrayNew(ByteU5BU5D_t58506160_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_5))));
		FileStream_t1527309539 * L_6 = V_0;
		NullCheck(L_6);
		VirtFuncInvoker2< int64_t, int64_t, int32_t >::Invoke(16 /* System.Int64 System.IO.FileStream::Seek(System.Int64,System.IO.SeekOrigin) */, L_6, (((int64_t)((int64_t)0))), 0);
		FileStream_t1527309539 * L_7 = V_0;
		ByteU5BU5D_t58506160* L_8 = V_1;
		ByteU5BU5D_t58506160* L_9 = V_1;
		NullCheck(L_9);
		NullCheck(L_7);
		VirtFuncInvoker3< int32_t, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(14 /* System.Int32 System.IO.FileStream::Read(System.Byte[],System.Int32,System.Int32) */, L_7, L_8, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))));
		FileStream_t1527309539 * L_10 = V_0;
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_10);
		ByteU5BU5D_t58506160* L_11 = V_1;
		ByteReader_t2446302219 * L_12 = (ByteReader_t2446302219 *)il2cpp_codegen_object_new(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		ByteReader__ctor_m417508569(L_12, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_0047:
	{
		return (ByteReader_t2446302219 *)NULL;
	}
}
// System.Boolean ByteReader::get_canRead()
extern "C"  bool ByteReader_get_canRead_m759072407 (ByteReader_t2446302219 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		ByteU5BU5D_t58506160* L_0 = __this->get_mBuffer_0();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_1 = __this->get_mOffset_1();
		ByteU5BU5D_t58506160* L_2 = __this->get_mBuffer_0();
		NullCheck(L_2);
		G_B3_0 = ((((int32_t)L_1) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))? 1 : 0);
		goto IL_001e;
	}

IL_001d:
	{
		G_B3_0 = 0;
	}

IL_001e:
	{
		return (bool)G_B3_0;
	}
}
// System.String ByteReader::ReadLine(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* Encoding_t180559927_il2cpp_TypeInfo_var;
extern const uint32_t ByteReader_ReadLine_m416558568_MetadataUsageId;
extern "C"  String_t* ByteReader_ReadLine_m416558568 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___buffer0, int32_t ___start1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ByteReader_ReadLine_m416558568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Encoding_t180559927_il2cpp_TypeInfo_var);
		Encoding_t180559927 * L_0 = Encoding_get_UTF8_m619558519(NULL /*static, unused*/, /*hidden argument*/NULL);
		ByteU5BU5D_t58506160* L_1 = ___buffer0;
		int32_t L_2 = ___start1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		String_t* L_4 = VirtFuncInvoker3< String_t*, ByteU5BU5D_t58506160*, int32_t, int32_t >::Invoke(21 /* System.String System.Text.Encoding::GetString(System.Byte[],System.Int32,System.Int32) */, L_0, L_1, L_2, L_3);
		return L_4;
	}
}
// System.String ByteReader::ReadLine()
extern "C"  String_t* ByteReader_ReadLine_m3908010721 (ByteReader_t2446302219 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = ByteReader_ReadLine_m2195921560(__this, (bool)1, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.String ByteReader::ReadLine(System.Boolean)
extern Il2CppClass* ByteReader_t2446302219_il2cpp_TypeInfo_var;
extern const uint32_t ByteReader_ReadLine_m2195921560_MetadataUsageId;
extern "C"  String_t* ByteReader_ReadLine_m2195921560 (ByteReader_t2446302219 * __this, bool ___skipEmptyLines0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ByteReader_ReadLine_m2195921560_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	{
		ByteU5BU5D_t58506160* L_0 = __this->get_mBuffer_0();
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		bool L_1 = ___skipEmptyLines0;
		if (!L_1)
		{
			goto IL_0042;
		}
	}
	{
		goto IL_0022;
	}

IL_0014:
	{
		int32_t L_2 = __this->get_mOffset_1();
		__this->set_mOffset_1(((int32_t)((int32_t)L_2+(int32_t)1)));
	}

IL_0022:
	{
		int32_t L_3 = __this->get_mOffset_1();
		int32_t L_4 = V_0;
		if ((((int32_t)L_3) >= ((int32_t)L_4)))
		{
			goto IL_0042;
		}
	}
	{
		ByteU5BU5D_t58506160* L_5 = __this->get_mBuffer_0();
		int32_t L_6 = __this->get_mOffset_1();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		if ((((int32_t)((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)))) < ((int32_t)((int32_t)32))))
		{
			goto IL_0014;
		}
	}

IL_0042:
	{
		int32_t L_8 = __this->get_mOffset_1();
		V_1 = L_8;
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		if ((((int32_t)L_9) >= ((int32_t)L_10)))
		{
			goto IL_00b1;
		}
	}
	{
		goto IL_00ac;
	}

IL_0055:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) >= ((int32_t)L_12)))
		{
			goto IL_0083;
		}
	}
	{
		ByteU5BU5D_t58506160* L_13 = __this->get_mBuffer_0();
		int32_t L_14 = V_1;
		int32_t L_15 = L_14;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_15);
		int32_t L_16 = L_15;
		V_2 = ((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_16)));
		int32_t L_17 = V_2;
		if ((((int32_t)L_17) == ((int32_t)((int32_t)10))))
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_18 = V_2;
		if ((((int32_t)L_18) == ((int32_t)((int32_t)13))))
		{
			goto IL_007e;
		}
	}
	{
		goto IL_00ac;
	}

IL_007e:
	{
		goto IL_0087;
	}

IL_0083:
	{
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_0087:
	{
		ByteU5BU5D_t58506160* L_20 = __this->get_mBuffer_0();
		int32_t L_21 = __this->get_mOffset_1();
		int32_t L_22 = V_1;
		int32_t L_23 = __this->get_mOffset_1();
		IL2CPP_RUNTIME_CLASS_INIT(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		String_t* L_24 = ByteReader_ReadLine_m416558568(NULL /*static, unused*/, L_20, L_21, ((int32_t)((int32_t)((int32_t)((int32_t)L_22-(int32_t)L_23))-(int32_t)1)), /*hidden argument*/NULL);
		V_3 = L_24;
		int32_t L_25 = V_1;
		__this->set_mOffset_1(L_25);
		String_t* L_26 = V_3;
		return L_26;
	}

IL_00ac:
	{
		goto IL_0055;
	}

IL_00b1:
	{
		int32_t L_27 = V_0;
		__this->set_mOffset_1(L_27);
		return (String_t*)NULL;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String> ByteReader::ReadDictionary()
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1597524044_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1504;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t ByteReader_ReadDictionary_m2502625102_MetadataUsageId;
extern "C"  Dictionary_2_t2606186806 * ByteReader_ReadDictionary_m2502625102 (ByteReader_t2446302219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ByteReader_ReadDictionary_m2502625102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t2606186806 * V_0 = NULL;
	CharU5BU5D_t3416858730* V_1 = NULL;
	String_t* V_2 = NULL;
	StringU5BU5D_t2956870243* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	{
		Dictionary_2_t2606186806 * L_0 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1597524044(L_0, /*hidden argument*/Dictionary_2__ctor_m1597524044_MethodInfo_var);
		V_0 = L_0;
		CharU5BU5D_t3416858730* L_1 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)61));
		V_1 = L_1;
		goto IL_007e;
	}

IL_0017:
	{
		String_t* L_2 = ByteReader_ReadLine_m3908010721(__this, /*hidden argument*/NULL);
		V_2 = L_2;
		String_t* L_3 = V_2;
		if (L_3)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_0089;
	}

IL_0029:
	{
		String_t* L_4 = V_2;
		NullCheck(L_4);
		bool L_5 = String_StartsWith_m1500793453(L_4, _stringLiteral1504, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_007e;
	}

IL_003e:
	{
		String_t* L_6 = V_2;
		CharU5BU5D_t3416858730* L_7 = V_1;
		NullCheck(L_6);
		StringU5BU5D_t2956870243* L_8 = String_Split_m1407702193(L_6, L_7, 2, 1, /*hidden argument*/NULL);
		V_3 = L_8;
		StringU5BU5D_t2956870243* L_9 = V_3;
		NullCheck(L_9);
		if ((!(((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))) == ((uint32_t)2))))
		{
			goto IL_007e;
		}
	}
	{
		StringU5BU5D_t2956870243* L_10 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		int32_t L_11 = 0;
		NullCheck(((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		String_t* L_12 = String_Trim_m1030489823(((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_11))), /*hidden argument*/NULL);
		V_4 = L_12;
		StringU5BU5D_t2956870243* L_13 = V_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		int32_t L_14 = 1;
		NullCheck(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14))));
		String_t* L_15 = String_Trim_m1030489823(((L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_14))), /*hidden argument*/NULL);
		NullCheck(L_15);
		String_t* L_16 = String_Replace_m2915759397(L_15, _stringLiteral2962, _stringLiteral10, /*hidden argument*/NULL);
		V_5 = L_16;
		Dictionary_2_t2606186806 * L_17 = V_0;
		String_t* L_18 = V_4;
		String_t* L_19 = V_5;
		NullCheck(L_17);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(25 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_17, L_18, L_19);
	}

IL_007e:
	{
		bool L_20 = ByteReader_get_canRead_m759072407(__this, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_0017;
		}
	}

IL_0089:
	{
		Dictionary_2_t2606186806 * L_21 = V_0;
		return L_21;
	}
}
// BetterList`1<System.String> ByteReader::ReadCSV()
extern Il2CppClass* ByteReader_t2446302219_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1_Clear_m3361141803_MethodInfo_var;
extern const MethodInfo* BetterList_1_Add_m2398996607_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2962;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral1088;
extern Il2CppCodeGenString* _stringLiteral34;
extern const uint32_t ByteReader_ReadCSV_m2254485814_MetadataUsageId;
extern "C"  BetterList_1_t2465456914 * ByteReader_ReadCSV_m2254485814 (ByteReader_t2446302219 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ByteReader_ReadCSV_m2254485814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	uint16_t V_6 = 0x0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		BetterList_1_t2465456914 * L_0 = ((ByteReader_t2446302219_StaticFields*)ByteReader_t2446302219_il2cpp_TypeInfo_var->static_fields)->get_mTemp_2();
		NullCheck(L_0);
		BetterList_1_Clear_m3361141803(L_0, /*hidden argument*/BetterList_1_Clear_m3361141803_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		V_0 = L_1;
		V_1 = (bool)0;
		V_2 = 0;
		goto IL_01b0;
	}

IL_0019:
	{
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0052;
		}
	}
	{
		String_t* L_3 = ByteReader_ReadLine_m2195921560(__this, (bool)0, /*hidden argument*/NULL);
		V_3 = L_3;
		String_t* L_4 = V_3;
		if (L_4)
		{
			goto IL_002f;
		}
	}
	{
		return (BetterList_1_t2465456914 *)NULL;
	}

IL_002f:
	{
		String_t* L_5 = V_3;
		NullCheck(L_5);
		String_t* L_6 = String_Replace_m2915759397(L_5, _stringLiteral2962, _stringLiteral10, /*hidden argument*/NULL);
		V_3 = L_6;
		String_t* L_7 = V_0;
		String_t* L_8 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m1825781833(NULL /*static, unused*/, L_7, _stringLiteral10, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0075;
	}

IL_0052:
	{
		String_t* L_10 = ByteReader_ReadLine_m2195921560(__this, (bool)1, /*hidden argument*/NULL);
		V_0 = L_10;
		String_t* L_11 = V_0;
		if (L_11)
		{
			goto IL_0062;
		}
	}
	{
		return (BetterList_1_t2465456914 *)NULL;
	}

IL_0062:
	{
		String_t* L_12 = V_0;
		NullCheck(L_12);
		String_t* L_13 = String_Replace_m2915759397(L_12, _stringLiteral2962, _stringLiteral10, /*hidden argument*/NULL);
		V_0 = L_13;
		V_2 = 0;
	}

IL_0075:
	{
		int32_t L_14 = V_2;
		V_4 = L_14;
		String_t* L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = String_get_Length_m2979997331(L_15, /*hidden argument*/NULL);
		V_5 = L_16;
		goto IL_0171;
	}

IL_0085:
	{
		String_t* L_17 = V_0;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		uint16_t L_19 = String_get_Chars_m3015341861(L_17, L_18, /*hidden argument*/NULL);
		V_6 = L_19;
		uint16_t L_20 = V_6;
		if ((!(((uint32_t)L_20) == ((uint32_t)((int32_t)44)))))
		{
			goto IL_00bd;
		}
	}
	{
		bool L_21 = V_1;
		if (L_21)
		{
			goto IL_00b8;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		BetterList_1_t2465456914 * L_22 = ((ByteReader_t2446302219_StaticFields*)ByteReader_t2446302219_il2cpp_TypeInfo_var->static_fields)->get_mTemp_2();
		String_t* L_23 = V_0;
		int32_t L_24 = V_2;
		int32_t L_25 = V_4;
		int32_t L_26 = V_2;
		NullCheck(L_23);
		String_t* L_27 = String_Substring_m675079568(L_23, L_24, ((int32_t)((int32_t)L_25-(int32_t)L_26)), /*hidden argument*/NULL);
		NullCheck(L_22);
		BetterList_1_Add_m2398996607(L_22, L_27, /*hidden argument*/BetterList_1_Add_m2398996607_MethodInfo_var);
		int32_t L_28 = V_4;
		V_2 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00b8:
	{
		goto IL_016b;
	}

IL_00bd:
	{
		uint16_t L_29 = V_6;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_016b;
		}
	}
	{
		bool L_30 = V_1;
		if (!L_30)
		{
			goto IL_0164;
		}
	}
	{
		int32_t L_31 = V_4;
		int32_t L_32 = V_5;
		if ((((int32_t)((int32_t)((int32_t)L_31+(int32_t)1))) < ((int32_t)L_32)))
		{
			goto IL_0101;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		BetterList_1_t2465456914 * L_33 = ((ByteReader_t2446302219_StaticFields*)ByteReader_t2446302219_il2cpp_TypeInfo_var->static_fields)->get_mTemp_2();
		String_t* L_34 = V_0;
		int32_t L_35 = V_2;
		int32_t L_36 = V_4;
		int32_t L_37 = V_2;
		NullCheck(L_34);
		String_t* L_38 = String_Substring_m675079568(L_34, L_35, ((int32_t)((int32_t)L_36-(int32_t)L_37)), /*hidden argument*/NULL);
		NullCheck(L_38);
		String_t* L_39 = String_Replace_m2915759397(L_38, _stringLiteral1088, _stringLiteral34, /*hidden argument*/NULL);
		NullCheck(L_33);
		BetterList_1_Add_m2398996607(L_33, L_39, /*hidden argument*/BetterList_1_Add_m2398996607_MethodInfo_var);
		BetterList_1_t2465456914 * L_40 = ((ByteReader_t2446302219_StaticFields*)ByteReader_t2446302219_il2cpp_TypeInfo_var->static_fields)->get_mTemp_2();
		return L_40;
	}

IL_0101:
	{
		String_t* L_41 = V_0;
		int32_t L_42 = V_4;
		NullCheck(L_41);
		uint16_t L_43 = String_get_Chars_m3015341861(L_41, ((int32_t)((int32_t)L_42+(int32_t)1)), /*hidden argument*/NULL);
		if ((((int32_t)L_43) == ((int32_t)((int32_t)34))))
		{
			goto IL_0159;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		BetterList_1_t2465456914 * L_44 = ((ByteReader_t2446302219_StaticFields*)ByteReader_t2446302219_il2cpp_TypeInfo_var->static_fields)->get_mTemp_2();
		String_t* L_45 = V_0;
		int32_t L_46 = V_2;
		int32_t L_47 = V_4;
		int32_t L_48 = V_2;
		NullCheck(L_45);
		String_t* L_49 = String_Substring_m675079568(L_45, L_46, ((int32_t)((int32_t)L_47-(int32_t)L_48)), /*hidden argument*/NULL);
		NullCheck(L_49);
		String_t* L_50 = String_Replace_m2915759397(L_49, _stringLiteral1088, _stringLiteral34, /*hidden argument*/NULL);
		NullCheck(L_44);
		BetterList_1_Add_m2398996607(L_44, L_50, /*hidden argument*/BetterList_1_Add_m2398996607_MethodInfo_var);
		V_1 = (bool)0;
		String_t* L_51 = V_0;
		int32_t L_52 = V_4;
		NullCheck(L_51);
		uint16_t L_53 = String_get_Chars_m3015341861(L_51, ((int32_t)((int32_t)L_52+(int32_t)1)), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_53) == ((uint32_t)((int32_t)44)))))
		{
			goto IL_0154;
		}
	}
	{
		int32_t L_54 = V_4;
		V_4 = ((int32_t)((int32_t)L_54+(int32_t)1));
		int32_t L_55 = V_4;
		V_2 = ((int32_t)((int32_t)L_55+(int32_t)1));
	}

IL_0154:
	{
		goto IL_015f;
	}

IL_0159:
	{
		int32_t L_56 = V_4;
		V_4 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_015f:
	{
		goto IL_016b;
	}

IL_0164:
	{
		int32_t L_57 = V_4;
		V_2 = ((int32_t)((int32_t)L_57+(int32_t)1));
		V_1 = (bool)1;
	}

IL_016b:
	{
		int32_t L_58 = V_4;
		V_4 = ((int32_t)((int32_t)L_58+(int32_t)1));
	}

IL_0171:
	{
		int32_t L_59 = V_4;
		int32_t L_60 = V_5;
		if ((((int32_t)L_59) < ((int32_t)L_60)))
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_61 = V_2;
		String_t* L_62 = V_0;
		NullCheck(L_62);
		int32_t L_63 = String_get_Length_m2979997331(L_62, /*hidden argument*/NULL);
		if ((((int32_t)L_61) >= ((int32_t)L_63)))
		{
			goto IL_01aa;
		}
	}
	{
		bool L_64 = V_1;
		if (!L_64)
		{
			goto IL_0191;
		}
	}
	{
		goto IL_01b0;
	}

IL_0191:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		BetterList_1_t2465456914 * L_65 = ((ByteReader_t2446302219_StaticFields*)ByteReader_t2446302219_il2cpp_TypeInfo_var->static_fields)->get_mTemp_2();
		String_t* L_66 = V_0;
		int32_t L_67 = V_2;
		String_t* L_68 = V_0;
		NullCheck(L_68);
		int32_t L_69 = String_get_Length_m2979997331(L_68, /*hidden argument*/NULL);
		int32_t L_70 = V_2;
		NullCheck(L_66);
		String_t* L_71 = String_Substring_m675079568(L_66, L_67, ((int32_t)((int32_t)L_69-(int32_t)L_70)), /*hidden argument*/NULL);
		NullCheck(L_65);
		BetterList_1_Add_m2398996607(L_65, L_71, /*hidden argument*/BetterList_1_Add_m2398996607_MethodInfo_var);
	}

IL_01aa:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		BetterList_1_t2465456914 * L_72 = ((ByteReader_t2446302219_StaticFields*)ByteReader_t2446302219_il2cpp_TypeInfo_var->static_fields)->get_mTemp_2();
		return L_72;
	}

IL_01b0:
	{
		bool L_73 = ByteReader_get_canRead_m759072407(__this, /*hidden argument*/NULL);
		if (L_73)
		{
			goto IL_0019;
		}
	}
	{
		return (BetterList_1_t2465456914 *)NULL;
	}
}
// System.Void Character::.ctor()
extern "C"  void Character__ctor_m90739842 (Character_t3568163593 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// Pattern Character::get_pattern()
extern "C"  Pattern_t873562992 * Character_get_pattern_m1274480394 (Character_t3568163593 * __this, const MethodInfo* method)
{
	{
		Pattern_t873562992 * L_0 = __this->get_m_pattern_4();
		return L_0;
	}
}
// System.Void Character::Awake()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const uint32_t Character_Awake_m328345061_MetadataUsageId;
extern "C"  void Character_Awake_m328345061 (Character_t3568163593 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Character_Awake_m328345061_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set_m_tr_2(L_0);
		return;
	}
}
// System.Void Character::OnDestroy()
extern "C"  void Character_OnDestroy_m3455604859 (Character_t3568163593 * __this, const MethodInfo* method)
{
	{
		__this->set_m_tr_2((Transform_t284553113 *)NULL);
		__this->set_m_move_3((Move_t2404337 *)NULL);
		__this->set_m_pattern_4((Pattern_t873562992 *)NULL);
		return;
	}
}
// System.Void Character::Update()
extern "C"  void Character_Update_m244829899 (Character_t3568163593 * __this, const MethodInfo* method)
{
	{
		Move_t2404337 * L_0 = __this->get_m_move_3();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Move_t2404337 * L_1 = __this->get_m_move_3();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(8 /* System.Void Move::Update() */, L_1);
	}

IL_0016:
	{
		return;
	}
}
// System.Void Character::SetMove(Move)
extern "C"  void Character_SetMove_m4254943650 (Character_t3568163593 * __this, Move_t2404337 * ___move0, const MethodInfo* method)
{
	{
		Move_t2404337 * L_0 = __this->get_m_move_3();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Move_t2404337 * L_1 = __this->get_m_move_3();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(6 /* System.Void Move::Clear() */, L_1);
		__this->set_m_move_3((Move_t2404337 *)NULL);
	}

IL_001d:
	{
		Move_t2404337 * L_2 = ___move0;
		__this->set_m_move_3(L_2);
		Move_t2404337 * L_3 = __this->get_m_move_3();
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		Move_t2404337 * L_4 = ___move0;
		NullCheck(L_4);
		VirtActionInvoker1< Character_t3568163593 * >::Invoke(5 /* System.Void Move::SetCharacter(Character) */, L_4, __this);
		Move_t2404337 * L_5 = ___move0;
		NullCheck(L_5);
		VirtActionInvoker0::Invoke(7 /* System.Void Move::Start() */, L_5);
	}

IL_003c:
	{
		return;
	}
}
// System.Void Character::OnEndMove()
extern "C"  void Character_OnEndMove_m2901770061 (Character_t3568163593 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Character::UpdateMove(UnityEngine.Vector3)
extern "C"  void Character_UpdateMove_m3102545117 (Character_t3568163593 * __this, Vector3_t3525329789  ___delta0, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = __this->get_m_tr_2();
		Vector3_t3525329789  L_1 = ___delta0;
		NullCheck(L_0);
		Transform_Translate_m2849099360(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Character::SetPattern(Pattern)
extern "C"  void Character_SetPattern_m3083031538 (Character_t3568163593 * __this, Pattern_t873562992 * ___pattern0, const MethodInfo* method)
{
	{
		Pattern_t873562992 * L_0 = ___pattern0;
		__this->set_m_pattern_4(L_0);
		return;
	}
}
// System.Void ChatInput::.ctor()
extern "C"  void ChatInput__ctor_m3826074233 (ChatInput_t3603581746 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ChatInput::Start()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIInput_t289134998_m1246790371_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3748508002;
extern Il2CppCodeGenString* _stringLiteral3458039618;
extern Il2CppCodeGenString* _stringLiteral1028334852;
extern Il2CppCodeGenString* _stringLiteral88939;
extern const uint32_t ChatInput_Start_m2773212025_MetadataUsageId;
extern "C"  void ChatInput_Start_m2773212025 (ChatInput_t3603581746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChatInput_Start_m2773212025_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t11523773* G_B5_1 = NULL;
	ObjectU5BU5D_t11523773* G_B5_2 = NULL;
	UITextList_t736798239 * G_B5_3 = NULL;
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t11523773* G_B4_1 = NULL;
	ObjectU5BU5D_t11523773* G_B4_2 = NULL;
	UITextList_t736798239 * G_B4_3 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t11523773* G_B6_2 = NULL;
	ObjectU5BU5D_t11523773* G_B6_3 = NULL;
	UITextList_t736798239 * G_B6_4 = NULL;
	{
		UIInput_t289134998 * L_0 = Component_GetComponent_TisUIInput_t289134998_m1246790371(__this, /*hidden argument*/Component_GetComponent_TisUIInput_t289134998_m1246790371_MethodInfo_var);
		__this->set_mInput_4(L_0);
		UIInput_t289134998 * L_1 = __this->get_mInput_4();
		NullCheck(L_1);
		UILabel_t291504320 * L_2 = L_1->get_label_4();
		NullCheck(L_2);
		UILabel_set_maxLineCount_m3150459678(L_2, 1, /*hidden argument*/NULL);
		bool L_3 = __this->get_fillWithDummyData_3();
		if (!L_3)
		{
			goto IL_0095;
		}
	}
	{
		UITextList_t736798239 * L_4 = __this->get_textList_2();
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0095;
		}
	}
	{
		V_0 = 0;
		goto IL_008d;
	}

IL_0040:
	{
		UITextList_t736798239 * L_6 = __this->get_textList_2();
		ObjectU5BU5D_t11523773* L_7 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_8 = V_0;
		G_B4_0 = 0;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		G_B4_3 = L_6;
		if (((int32_t)((int32_t)L_8%(int32_t)2)))
		{
			G_B5_0 = 0;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			G_B5_3 = L_6;
			goto IL_0060;
		}
	}
	{
		G_B6_0 = _stringLiteral3748508002;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_0065;
	}

IL_0060:
	{
		G_B6_0 = _stringLiteral3458039618;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_0065:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (Il2CppObject *)G_B6_0);
		ObjectU5BU5D_t11523773* L_9 = G_B6_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, _stringLiteral1028334852);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral1028334852);
		ObjectU5BU5D_t11523773* L_10 = L_9;
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_13);
		ObjectU5BU5D_t11523773* L_14 = L_10;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, _stringLiteral88939);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral88939);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m3016520001(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		NullCheck(G_B6_4);
		UITextList_Add_m1668405703(G_B6_4, L_15, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_008d:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)30))))
		{
			goto IL_0040;
		}
	}

IL_0095:
	{
		return;
	}
}
// System.Void ChatInput::OnSubmit()
extern Il2CppClass* NGUIText_t3886970074_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t ChatInput_OnSubmit_m1097797570_MetadataUsageId;
extern "C"  void ChatInput_OnSubmit_m1097797570 (ChatInput_t3603581746 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ChatInput_OnSubmit_m1097797570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		UITextList_t736798239 * L_0 = __this->get_textList_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0055;
		}
	}
	{
		UIInput_t289134998 * L_2 = __this->get_mInput_4();
		NullCheck(L_2);
		String_t* L_3 = UIInput_get_value_m187471550(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUIText_t3886970074_il2cpp_TypeInfo_var);
		String_t* L_4 = NGUIText_StripSymbols_m1576203641(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		String_t* L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0055;
		}
	}
	{
		UITextList_t736798239 * L_7 = __this->get_textList_2();
		String_t* L_8 = V_0;
		NullCheck(L_7);
		UITextList_Add_m1668405703(L_7, L_8, /*hidden argument*/NULL);
		UIInput_t289134998 * L_9 = __this->get_mInput_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_9);
		UIInput_set_value_m753256763(L_9, L_10, /*hidden argument*/NULL);
		UIInput_t289134998 * L_11 = __this->get_mInput_4();
		NullCheck(L_11);
		UIInput_set_isSelected_m4090711944(L_11, (bool)0, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// System.Void Common::.ctor()
extern "C"  void Common__ctor_m453241584 (Common_t2024019467 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Common::.cctor()
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1954599866;
extern Il2CppCodeGenString* _stringLiteral2759596;
extern Il2CppCodeGenString* _stringLiteral65262;
extern const uint32_t Common__cctor_m683491005_MetadataUsageId;
extern "C"  void Common__cctor_m683491005 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Common__cctor_m683491005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_POS_SURFACE_EFFECT_Y_0((0.01f));
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_POS_SURFACE_Y_1((0.0f));
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_POS_MONSTER_Y_2((-0.01f));
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_POS_EFFECT_Y_3((-0.02f));
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_POS_ZONE_Y_4((-0.03f));
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_POS_WAVE_Y_5((-0.04f));
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_POS_WATER_Y_6((-0.05f));
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_POS_COIN_Y_7((-1.0f));
		Vector3_t3525329789  L_0 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_1 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_0, (0.7f), /*hidden argument*/NULL);
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_SCALE_COIN_8(L_1);
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_SIZE_OF_DIVER_9((2.0f));
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_GAP_OF_DIVER_10((1.0f));
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_TAG_MONSTER_11(_stringLiteral1954599866);
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_TAG_ZONE_12(_stringLiteral2759596);
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_TAG_AXE_13(_stringLiteral65262);
		return;
	}
}
// Common/DIRECTION Common::ConvertDirection(System.String)
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1958628151_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral69;
extern Il2CppCodeGenString* _stringLiteral2487;
extern Il2CppCodeGenString* _stringLiteral78;
extern Il2CppCodeGenString* _stringLiteral2505;
extern Il2CppCodeGenString* _stringLiteral87;
extern Il2CppCodeGenString* _stringLiteral2660;
extern Il2CppCodeGenString* _stringLiteral83;
extern Il2CppCodeGenString* _stringLiteral2642;
extern const uint32_t Common_ConvertDirection_m1352663584_MetadataUsageId;
extern "C"  int32_t Common_ConvertDirection_m1352663584 (Il2CppObject * __this /* static, unused */, String_t* ___direction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Common_ConvertDirection_m1352663584_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	Dictionary_2_t190145395 * V_1 = NULL;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___direction0;
		V_0 = L_0;
		String_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_00cc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_2 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map4_14();
		if (L_2)
		{
			goto IL_007f;
		}
	}
	{
		Dictionary_2_t190145395 * L_3 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1958628151(L_3, 8, /*hidden argument*/Dictionary_2__ctor_m1958628151_MethodInfo_var);
		V_1 = L_3;
		Dictionary_2_t190145395 * L_4 = V_1;
		NullCheck(L_4);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_4, _stringLiteral69, 0);
		Dictionary_2_t190145395 * L_5 = V_1;
		NullCheck(L_5);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_5, _stringLiteral2487, 1);
		Dictionary_2_t190145395 * L_6 = V_1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_6, _stringLiteral78, 2);
		Dictionary_2_t190145395 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_7, _stringLiteral2505, 3);
		Dictionary_2_t190145395 * L_8 = V_1;
		NullCheck(L_8);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_8, _stringLiteral87, 4);
		Dictionary_2_t190145395 * L_9 = V_1;
		NullCheck(L_9);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_9, _stringLiteral2660, 5);
		Dictionary_2_t190145395 * L_10 = V_1;
		NullCheck(L_10);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_10, _stringLiteral83, 6);
		Dictionary_2_t190145395 * L_11 = V_1;
		NullCheck(L_11);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_11, _stringLiteral2642, 7);
		Dictionary_2_t190145395 * L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__switchU24map4_14(L_12);
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		Dictionary_2_t190145395 * L_13 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__switchU24map4_14();
		String_t* L_14 = V_0;
		NullCheck(L_13);
		bool L_15 = VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_13, L_14, (&V_2));
		if (!L_15)
		{
			goto IL_00cc;
		}
	}
	{
		int32_t L_16 = V_2;
		if (L_16 == 0)
		{
			goto IL_00bc;
		}
		if (L_16 == 1)
		{
			goto IL_00be;
		}
		if (L_16 == 2)
		{
			goto IL_00c0;
		}
		if (L_16 == 3)
		{
			goto IL_00c2;
		}
		if (L_16 == 4)
		{
			goto IL_00c4;
		}
		if (L_16 == 5)
		{
			goto IL_00c6;
		}
		if (L_16 == 6)
		{
			goto IL_00c8;
		}
		if (L_16 == 7)
		{
			goto IL_00ca;
		}
	}
	{
		goto IL_00cc;
	}

IL_00bc:
	{
		return (int32_t)(0);
	}

IL_00be:
	{
		return (int32_t)(1);
	}

IL_00c0:
	{
		return (int32_t)(2);
	}

IL_00c2:
	{
		return (int32_t)(3);
	}

IL_00c4:
	{
		return (int32_t)(4);
	}

IL_00c6:
	{
		return (int32_t)(5);
	}

IL_00c8:
	{
		return (int32_t)(6);
	}

IL_00ca:
	{
		return (int32_t)(7);
	}

IL_00cc:
	{
		return (int32_t)(0);
	}
}
// System.Boolean Common::IsEast(Common/DIRECTION)
extern "C"  bool Common_IsEast_m3103083476 (Il2CppObject * __this /* static, unused */, int32_t ___direction0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___direction0;
		if ((((int32_t)2) >= ((int32_t)L_0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___direction0;
		G_B3_0 = ((((int32_t)L_1) < ((int32_t)6))? 1 : 0);
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
	}

IL_000e:
	{
		return (bool)((((int32_t)G_B3_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean Common::IsNorth(Common/DIRECTION)
extern "C"  bool Common_IsNorth_m1443366618 (Il2CppObject * __this /* static, unused */, int32_t ___direction0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___direction0;
		if ((((int32_t)0) >= ((int32_t)L_0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___direction0;
		G_B3_0 = ((((int32_t)L_1) < ((int32_t)4))? 1 : 0);
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 0;
	}

IL_000e:
	{
		return (bool)G_B3_0;
	}
}
// System.Void Console::.ctor()
extern Il2CppClass* List_1_t797035549_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GUIContent_t2432841515_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1263066603_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral65193517;
extern Il2CppCodeGenString* _stringLiteral3362518283;
extern Il2CppCodeGenString* _stringLiteral3727525837;
extern Il2CppCodeGenString* _stringLiteral2621264570;
extern const uint32_t Console__ctor_m1797103636_MetadataUsageId;
extern "C"  void Console__ctor_m1797103636 (Console_t2616163639 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Console__ctor_m1797103636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_toggleKey_3(((int32_t)96));
		List_1_t797035549 * L_0 = (List_1_t797035549 *)il2cpp_codegen_object_new(List_1_t797035549_il2cpp_TypeInfo_var);
		List_1__ctor_m1263066603(L_0, /*hidden argument*/List_1__ctor_m1263066603_MethodInfo_var);
		__this->set_logs_4(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_command_8(L_1);
		int32_t L_2 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_3 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Rect__ctor_m3291325233(&L_4, (20.0f), (20.0f), (((float)((float)((int32_t)((int32_t)L_2-(int32_t)((int32_t)40)))))), (((float)((float)((int32_t)((int32_t)L_3-(int32_t)((int32_t)40)))))), /*hidden argument*/NULL);
		__this->set_windowRect_10(L_4);
		Rect_t1525428817  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Rect__ctor_m3291325233(&L_5, (0.0f), (0.0f), (10000.0f), (20.0f), /*hidden argument*/NULL);
		__this->set_titleBarRect_11(L_5);
		GUIContent_t2432841515 * L_6 = (GUIContent_t2432841515 *)il2cpp_codegen_object_new(GUIContent_t2432841515_il2cpp_TypeInfo_var);
		GUIContent__ctor_m3415640175(L_6, _stringLiteral65193517, _stringLiteral3362518283, /*hidden argument*/NULL);
		__this->set_clearLabel_12(L_6);
		GUIContent_t2432841515 * L_7 = (GUIContent_t2432841515 *)il2cpp_codegen_object_new(GUIContent_t2432841515_il2cpp_TypeInfo_var);
		GUIContent__ctor_m3415640175(L_7, _stringLiteral3727525837, _stringLiteral2621264570, /*hidden argument*/NULL);
		__this->set_collapseLabel_13(L_7);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Console::.cctor()
extern Il2CppClass* Dictionary_2_t881716807_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t2616163639_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1420748223_MethodInfo_var;
extern const uint32_t Console__cctor_m3688508953_MetadataUsageId;
extern "C"  void Console__cctor_m3688508953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Console__cctor_m3688508953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Dictionary_2_t881716807 * V_0 = NULL;
	{
		Dictionary_2_t881716807 * L_0 = (Dictionary_2_t881716807 *)il2cpp_codegen_object_new(Dictionary_2_t881716807_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1420748223(L_0, /*hidden argument*/Dictionary_2__ctor_m1420748223_MethodInfo_var);
		V_0 = L_0;
		Dictionary_2_t881716807 * L_1 = V_0;
		Color_t1588175760  L_2 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		VirtActionInvoker2< int32_t, Color_t1588175760  >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Add(!0,!1) */, L_1, 1, L_2);
		Dictionary_2_t881716807 * L_3 = V_0;
		Color_t1588175760  L_4 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		VirtActionInvoker2< int32_t, Color_t1588175760  >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Add(!0,!1) */, L_3, 0, L_4);
		Dictionary_2_t881716807 * L_5 = V_0;
		Color_t1588175760  L_6 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		VirtActionInvoker2< int32_t, Color_t1588175760  >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Add(!0,!1) */, L_5, 4, L_6);
		Dictionary_2_t881716807 * L_7 = V_0;
		Color_t1588175760  L_8 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		VirtActionInvoker2< int32_t, Color_t1588175760  >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Add(!0,!1) */, L_7, 3, L_8);
		Dictionary_2_t881716807 * L_9 = V_0;
		Color_t1588175760  L_10 = Color_get_yellow_m599454500(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker2< int32_t, Color_t1588175760  >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Add(!0,!1) */, L_9, 2, L_10);
		Dictionary_2_t881716807 * L_11 = V_0;
		((Console_t2616163639_StaticFields*)Console_t2616163639_il2cpp_TypeInfo_var->static_fields)->set_logTypeColors_9(L_11);
		return;
	}
}
// System.Void Console::OnEnable()
extern "C"  void Console_OnEnable_m2818653938 (Console_t2616163639 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Console::OnDisable()
extern "C"  void Console_OnDisable_m1919863419 (Console_t2616163639 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Console::Update()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t Console_Update_m1602499961_MetadataUsageId;
extern "C"  void Console_Update_m1602499961 (Console_t2616163639 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Console_Update_m1602499961_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_toggleKey_3();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		bool L_2 = __this->get_show_6();
		__this->set_show_6((bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0));
	}

IL_001f:
	{
		return;
	}
}
// System.Void Console::OnGUI()
extern Il2CppClass* WindowFunction_t999919624_il2cpp_TypeInfo_var;
extern Il2CppClass* GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var;
extern const MethodInfo* Console_ConsoleWindow_m4108353130_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2616163639;
extern const uint32_t Console_OnGUI_m1292502286_MetadataUsageId;
extern "C"  void Console_OnGUI_m1292502286 (Console_t2616163639 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Console_OnGUI_m1292502286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get_show_6();
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		Rect_t1525428817  L_1 = __this->get_windowRect_10();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)Console_ConsoleWindow_m4108353130_MethodInfo_var);
		WindowFunction_t999919624 * L_3 = (WindowFunction_t999919624 *)il2cpp_codegen_object_new(WindowFunction_t999919624_il2cpp_TypeInfo_var);
		WindowFunction__ctor_m732638321(L_3, __this, L_2, /*hidden argument*/NULL);
		Rect_t1525428817  L_4 = GUILayout_Window_m3742951272(NULL /*static, unused*/, ((int32_t)123456), L_1, L_3, _stringLiteral2616163639, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		__this->set_windowRect_10(L_4);
		return;
	}
}
// System.Void Console::ConsoleWindow(System.Int32)
extern Il2CppClass* GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Console_t2616163639_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100358090;
extern Il2CppCodeGenString* _stringLiteral2280;
extern const uint32_t Console_ConsoleWindow_m4108353130_MetadataUsageId;
extern "C"  void Console_ConsoleWindow_m4108353130 (Console_t2616163639 * __this, int32_t ___windowID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Console_ConsoleWindow_m4108353130_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Log_t76580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	bool V_2 = false;
	String_t* V_3 = NULL;
	Event_t1590224583 * V_4 = NULL;
	Log_t76580  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t G_B5_0 = 0;
	{
		Vector2_t3525329788  L_0 = __this->get_scrollPosition_5();
		Vector2_t3525329788  L_1 = GUILayout_BeginScrollView_m247627737(NULL /*static, unused*/, L_0, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		__this->set_scrollPosition_5(L_1);
		V_0 = 0;
		goto IL_009b;
	}

IL_001e:
	{
		List_1_t797035549 * L_2 = __this->get_logs_4();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Log_t76580  L_4 = VirtFuncInvoker1< Log_t76580 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Console/Log>::get_Item(System.Int32) */, L_2, L_3);
		V_1 = L_4;
		bool L_5 = __this->get_collapse_7();
		if (!L_5)
		{
			goto IL_006f;
		}
	}
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		String_t* L_7 = (&V_1)->get_message_0();
		List_1_t797035549 * L_8 = __this->get_logs_4();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		Log_t76580  L_10 = VirtFuncInvoker1< Log_t76580 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Console/Log>::get_Item(System.Int32) */, L_8, ((int32_t)((int32_t)L_9-(int32_t)1)));
		V_5 = L_10;
		String_t* L_11 = (&V_5)->get_message_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_7, L_11, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_12));
		goto IL_0063;
	}

IL_0062:
	{
		G_B5_0 = 0;
	}

IL_0063:
	{
		V_2 = (bool)G_B5_0;
		bool L_13 = V_2;
		if (!L_13)
		{
			goto IL_006f;
		}
	}
	{
		goto IL_0097;
	}

IL_006f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Console_t2616163639_il2cpp_TypeInfo_var);
		Dictionary_2_t881716807 * L_14 = ((Console_t2616163639_StaticFields*)Console_t2616163639_il2cpp_TypeInfo_var->static_fields)->get_logTypeColors_9();
		int32_t L_15 = (&V_1)->get_type_2();
		NullCheck(L_14);
		Color_t1588175760  L_16 = VirtFuncInvoker1< Color_t1588175760 , int32_t >::Invoke(24 /* !1 System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::get_Item(!0) */, L_14, L_15);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_set_contentColor_m3144718585(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		String_t* L_17 = (&V_1)->get_message_0();
		GUILayout_Label_m925445731(NULL /*static, unused*/, L_17, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
	}

IL_0097:
	{
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_009b:
	{
		int32_t L_19 = V_0;
		List_1_t797035549 * L_20 = __this->get_logs_4();
		NullCheck(L_20);
		int32_t L_21 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<Console/Log>::get_Count() */, L_20);
		if ((((int32_t)L_19) < ((int32_t)L_21)))
		{
			goto IL_001e;
		}
	}
	{
		GUILayout_EndScrollView_m2116108639(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t1588175760  L_22 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_set_contentColor_m3144718585(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m722450062(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUIContent_t2432841515 * L_23 = __this->get_clearLabel_12();
		bool L_24 = GUILayout_Button_m1054745774(NULL /*static, unused*/, L_23, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_00e7;
		}
	}
	{
		List_1_t797035549 * L_25 = __this->get_logs_4();
		NullCheck(L_25);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<Console/Log>::Clear() */, L_25);
	}

IL_00e7:
	{
		bool L_26 = __this->get_collapse_7();
		GUIContent_t2432841515 * L_27 = __this->get_collapseLabel_13();
		GUILayoutOptionU5BU5D_t1890718142* L_28 = ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t3151226183 * L_29 = GUILayout_ExpandWidth_m1724891333(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 0);
		ArrayElementTypeCheck (L_28, L_29);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t3151226183 *)L_29);
		bool L_30 = GUILayout_Toggle_m2796543747(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		__this->set_collapse_7(L_30);
		GUILayout_EndHorizontal_m556624369(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t1525428817  L_31 = __this->get_titleBarRect_11();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		GUI_DragWindow_m4272720463(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
		GUILayout_BeginHorizontal_m722450062(NULL /*static, unused*/, ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		GUI_SetNextControlName_m1893157449(NULL /*static, unused*/, _stringLiteral100358090, /*hidden argument*/NULL);
		GUI_FocusControl_m1668337335(NULL /*static, unused*/, _stringLiteral100358090, /*hidden argument*/NULL);
		String_t* L_32 = __this->get_command_8();
		String_t* L_33 = GUILayout_TextField_m1362880318(NULL /*static, unused*/, L_32, ((int32_t)20), ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)0)), /*hidden argument*/NULL);
		__this->set_command_8(L_33);
		V_3 = _stringLiteral2280;
		String_t* L_34 = V_3;
		GUILayoutOptionU5BU5D_t1890718142* L_35 = ((GUILayoutOptionU5BU5D_t1890718142*)SZArrayNew(GUILayoutOptionU5BU5D_t1890718142_il2cpp_TypeInfo_var, (uint32_t)1));
		GUILayoutOption_t3151226183 * L_36 = GUILayout_ExpandWidth_m1724891333(NULL /*static, unused*/, (bool)0, /*hidden argument*/NULL);
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 0);
		ArrayElementTypeCheck (L_35, L_36);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t3151226183 *)L_36);
		bool L_37 = GUILayout_Button_m6468109(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_017b;
		}
	}
	{
		Console_ExcuteCommand_m145081287(__this, /*hidden argument*/NULL);
	}

IL_017b:
	{
		GUILayout_EndHorizontal_m556624369(NULL /*static, unused*/, /*hidden argument*/NULL);
		Event_t1590224583 * L_38 = Event_get_current_m238587645(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_38;
		Event_t1590224583 * L_39 = V_4;
		NullCheck(L_39);
		bool L_40 = Event_get_isKey_m645126607(L_39, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_01da;
		}
	}
	{
		Event_t1590224583 * L_41 = V_4;
		NullCheck(L_41);
		int32_t L_42 = Event_get_keyCode_m1820698462(L_41, /*hidden argument*/NULL);
		if ((((int32_t)L_42) == ((int32_t)((int32_t)271))))
		{
			goto IL_01b2;
		}
	}
	{
		Event_t1590224583 * L_43 = V_4;
		NullCheck(L_43);
		int32_t L_44 = Event_get_keyCode_m1820698462(L_43, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_01bd;
		}
	}

IL_01b2:
	{
		Console_ExcuteCommand_m145081287(__this, /*hidden argument*/NULL);
		goto IL_01da;
	}

IL_01bd:
	{
		Event_t1590224583 * L_45 = V_4;
		NullCheck(L_45);
		int32_t L_46 = Event_get_keyCode_m1820698462(L_45, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_46) == ((uint32_t)((int32_t)27)))))
		{
			goto IL_01da;
		}
	}
	{
		bool L_47 = __this->get_show_6();
		__this->set_show_6((bool)((((int32_t)L_47) == ((int32_t)0))? 1 : 0));
	}

IL_01da:
	{
		return;
	}
}
// System.Void Console::HandleLog(System.String,System.String,UnityEngine.LogType)
extern Il2CppClass* Log_t76580_il2cpp_TypeInfo_var;
extern const uint32_t Console_HandleLog_m3608954709_MetadataUsageId;
extern "C"  void Console_HandleLog_m3608954709 (Console_t2616163639 * __this, String_t* ___message0, String_t* ___stackTrace1, int32_t ___type2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Console_HandleLog_m3608954709_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Log_t76580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		List_1_t797035549 * L_0 = __this->get_logs_4();
		Initobj (Log_t76580_il2cpp_TypeInfo_var, (&V_0));
		String_t* L_1 = ___message0;
		(&V_0)->set_message_0(L_1);
		String_t* L_2 = ___stackTrace1;
		(&V_0)->set_stackTrace_1(L_2);
		int32_t L_3 = ___type2;
		(&V_0)->set_type_2(L_3);
		Log_t76580  L_4 = V_0;
		NullCheck(L_0);
		VirtActionInvoker1< Log_t76580  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Console/Log>::Add(!0) */, L_0, L_4);
		return;
	}
}
// System.Void Console::ExcuteCommand()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Console_ExcuteCommand_m145081287_MetadataUsageId;
extern "C"  void Console_ExcuteCommand_m145081287 (Console_t2616163639 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Console_ExcuteCommand_m145081287_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_command_8(L_0);
		return;
	}
}
// Conversion methods for marshalling of: Console/Log
extern "C" void Log_t76580_marshal_pinvoke(const Log_t76580& unmarshaled, Log_t76580_marshaled_pinvoke& marshaled)
{
	marshaled.___message_0 = il2cpp_codegen_marshal_string(unmarshaled.get_message_0());
	marshaled.___stackTrace_1 = il2cpp_codegen_marshal_string(unmarshaled.get_stackTrace_1());
	marshaled.___type_2 = unmarshaled.get_type_2();
}
extern "C" void Log_t76580_marshal_pinvoke_back(const Log_t76580_marshaled_pinvoke& marshaled, Log_t76580& unmarshaled)
{
	unmarshaled.set_message_0(il2cpp_codegen_marshal_string_result(marshaled.___message_0));
	unmarshaled.set_stackTrace_1(il2cpp_codegen_marshal_string_result(marshaled.___stackTrace_1));
	int32_t unmarshaled_type_temp = 0;
	unmarshaled_type_temp = marshaled.___type_2;
	unmarshaled.set_type_2(unmarshaled_type_temp);
}
// Conversion method for clean up from marshalling of: Console/Log
extern "C" void Log_t76580_marshal_pinvoke_cleanup(Log_t76580_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___message_0);
	marshaled.___message_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___stackTrace_1);
	marshaled.___stackTrace_1 = NULL;
}
// Conversion methods for marshalling of: Console/Log
extern "C" void Log_t76580_marshal_com(const Log_t76580& unmarshaled, Log_t76580_marshaled_com& marshaled)
{
	marshaled.___message_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_message_0());
	marshaled.___stackTrace_1 = il2cpp_codegen_marshal_bstring(unmarshaled.get_stackTrace_1());
	marshaled.___type_2 = unmarshaled.get_type_2();
}
extern "C" void Log_t76580_marshal_com_back(const Log_t76580_marshaled_com& marshaled, Log_t76580& unmarshaled)
{
	unmarshaled.set_message_0(il2cpp_codegen_marshal_bstring_result(marshaled.___message_0));
	unmarshaled.set_stackTrace_1(il2cpp_codegen_marshal_bstring_result(marshaled.___stackTrace_1));
	int32_t unmarshaled_type_temp = 0;
	unmarshaled_type_temp = marshaled.___type_2;
	unmarshaled.set_type_2(unmarshaled_type_temp);
}
// Conversion method for clean up from marshalling of: Console/Log
extern "C" void Log_t76580_marshal_com_cleanup(Log_t76580_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___message_0);
	marshaled.___message_0 = NULL;
	il2cpp_codegen_marshal_free_bstring(marshaled.___stackTrace_1);
	marshaled.___stackTrace_1 = NULL;
}
// System.Void Control::.ctor()
extern "C"  void Control__ctor_m3714434766 (Control_t2616196413 * __this, const MethodInfo* method)
{
	{
		__this->set_MAX_AXES_2(2);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Control::Awake()
extern "C"  void Control_Awake_m3952039985 (Control_t2616196413 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Control::OnDestroy()
extern "C"  void Control_OnDestroy_m607880391 (Control_t2616196413 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Control::Update()
extern "C"  void Control_Update_m910222847 (Control_t2616196413 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Control::StartTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Control_StartTouch_m106134380 (Control_t2616196413 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Control::MovedTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Control_MovedTouch_m2670271099 (Control_t2616196413 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Control::EndTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Control_EndTouch_m4134908051 (Control_t2616196413 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Control::CanceldTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Control_CanceldTouch_m44628068 (Control_t2616196413 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Data::.ctor()
extern "C"  void Data__ctor_m1242679825 (Data_t2122699 * __this, const MethodInfo* method)
{
	{
		__this->set_m_load_max_3(8);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// DataSetting Data::get_settings()
extern Il2CppClass* Data_t2122699_il2cpp_TypeInfo_var;
extern const uint32_t Data_get_settings_m2852704140_MetadataUsageId;
extern "C"  DataSetting_t2172603174 * Data_get_settings_m2852704140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_get_settings_m2852704140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Data_t2122699 * L_0 = ((Data_t2122699_StaticFields*)Data_t2122699_il2cpp_TypeInfo_var->static_fields)->get_instance_12();
		NullCheck(L_0);
		DataSetting_t2172603174 * L_1 = L_0->get_m_setting_4();
		return L_1;
	}
}
// DataChapters Data::get_chapters()
extern Il2CppClass* Data_t2122699_il2cpp_TypeInfo_var;
extern const uint32_t Data_get_chapters_m2935368655_MetadataUsageId;
extern "C"  DataChapters_t2926209968 * Data_get_chapters_m2935368655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_get_chapters_m2935368655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Data_t2122699 * L_0 = ((Data_t2122699_StaticFields*)Data_t2122699_il2cpp_TypeInfo_var->static_fields)->get_instance_12();
		NullCheck(L_0);
		DataChapters_t2926209968 * L_1 = L_0->get_m_chapters_5();
		return L_1;
	}
}
// DataStages Data::get_stages()
extern Il2CppClass* Data_t2122699_il2cpp_TypeInfo_var;
extern const uint32_t Data_get_stages_m2462436463_MetadataUsageId;
extern "C"  DataStages_t3269946783 * Data_get_stages_m2462436463 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_get_stages_m2462436463_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Data_t2122699 * L_0 = ((Data_t2122699_StaticFields*)Data_t2122699_il2cpp_TypeInfo_var->static_fields)->get_instance_12();
		NullCheck(L_0);
		DataStages_t3269946783 * L_1 = L_0->get_m_stages_6();
		return L_1;
	}
}
// DataAxes Data::get_axe()
extern Il2CppClass* Data_t2122699_il2cpp_TypeInfo_var;
extern const uint32_t Data_get_axe_m1914057446_MetadataUsageId;
extern "C"  DataAxes_t1853147663 * Data_get_axe_m1914057446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_get_axe_m1914057446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Data_t2122699 * L_0 = ((Data_t2122699_StaticFields*)Data_t2122699_il2cpp_TypeInfo_var->static_fields)->get_instance_12();
		NullCheck(L_0);
		DataAxes_t1853147663 * L_1 = L_0->get_m_axes_7();
		return L_1;
	}
}
// DataMonsters Data::get_monster()
extern Il2CppClass* Data_t2122699_il2cpp_TypeInfo_var;
extern const uint32_t Data_get_monster_m4060264806_MetadataUsageId;
extern "C"  DataMonsters_t1171984835 * Data_get_monster_m4060264806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_get_monster_m4060264806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Data_t2122699 * L_0 = ((Data_t2122699_StaticFields*)Data_t2122699_il2cpp_TypeInfo_var->static_fields)->get_instance_12();
		NullCheck(L_0);
		DataMonsters_t1171984835 * L_1 = L_0->get_m_monsters_8();
		return L_1;
	}
}
// DataZones Data::get_zone()
extern Il2CppClass* Data_t2122699_il2cpp_TypeInfo_var;
extern const uint32_t Data_get_zone_m1284072190_MetadataUsageId;
extern "C"  DataZones_t1635830941 * Data_get_zone_m1284072190 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_get_zone_m1284072190_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Data_t2122699 * L_0 = ((Data_t2122699_StaticFields*)Data_t2122699_il2cpp_TypeInfo_var->static_fields)->get_instance_12();
		NullCheck(L_0);
		DataZones_t1635830941 * L_1 = L_0->get_m_zones_9();
		return L_1;
	}
}
// DataPatterns Data::get_pattern()
extern Il2CppClass* Data_t2122699_il2cpp_TypeInfo_var;
extern const uint32_t Data_get_pattern_m1685008806_MetadataUsageId;
extern "C"  DataPatterns_t2737562829 * Data_get_pattern_m1685008806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_get_pattern_m1685008806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Data_t2122699 * L_0 = ((Data_t2122699_StaticFields*)Data_t2122699_il2cpp_TypeInfo_var->static_fields)->get_instance_12();
		NullCheck(L_0);
		DataPatterns_t2737562829 * L_1 = L_0->get_m_patterns_10();
		return L_1;
	}
}
// DataUser Data::get_user()
extern Il2CppClass* Data_t2122699_il2cpp_TypeInfo_var;
extern const uint32_t Data_get_user_m2747519663_MetadataUsageId;
extern "C"  DataUser_t1853738677 * Data_get_user_m2747519663 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_get_user_m2747519663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Data_t2122699 * L_0 = ((Data_t2122699_StaticFields*)Data_t2122699_il2cpp_TypeInfo_var->static_fields)->get_instance_12();
		NullCheck(L_0);
		DataUser_t1853738677 * L_1 = L_0->get_m_user_11();
		return L_1;
	}
}
// System.Void Data::Awake()
extern Il2CppClass* Data_t2122699_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1909762512;
extern const uint32_t Data_Awake_m1480285044_MetadataUsageId;
extern "C"  void Data_Awake_m1480285044 (Data_t2122699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_Awake_m1480285044_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Data_t2122699_StaticFields*)Data_t2122699_il2cpp_TypeInfo_var->static_fields)->set_instance_12(__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_DontDestroyOnLoad_m4064482788(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_m_load_index_2(0);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral1909762512, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Data::OnDestory()
extern "C"  void Data_OnDestory_m2293581092 (Data_t2122699 * __this, const MethodInfo* method)
{
	{
		DataUser_t1853738677 * L_0 = __this->get_m_user_11();
		NullCheck(L_0);
		DataUser_Clear_m3439093681(L_0, /*hidden argument*/NULL);
		__this->set_m_user_11((DataUser_t1853738677 *)NULL);
		return;
	}
}
// System.Collections.IEnumerator Data::LoadData()
extern Il2CppClass* U3CLoadDataU3Ec__Iterator4_t1112965621_il2cpp_TypeInfo_var;
extern const uint32_t Data_LoadData_m3661499403_MetadataUsageId;
extern "C"  Il2CppObject * Data_LoadData_m3661499403 (Data_t2122699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_LoadData_m3661499403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CLoadDataU3Ec__Iterator4_t1112965621 * V_0 = NULL;
	{
		U3CLoadDataU3Ec__Iterator4_t1112965621 * L_0 = (U3CLoadDataU3Ec__Iterator4_t1112965621 *)il2cpp_codegen_object_new(U3CLoadDataU3Ec__Iterator4_t1112965621_il2cpp_TypeInfo_var);
		U3CLoadDataU3Ec__Iterator4__ctor_m2258043601(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CLoadDataU3Ec__Iterator4_t1112965621 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CLoadDataU3Ec__Iterator4_t1112965621 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Data::LoadError(Data/PROGRESS)
extern Il2CppClass* PROGRESS_t4076515885_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3464030460;
extern Il2CppCodeGenString* _stringLiteral1909762512;
extern const uint32_t Data_LoadError_m1812381775_MetadataUsageId;
extern "C"  void Data_LoadError_m1812381775 (Data_t2122699 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_LoadError_m1812381775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(PROGRESS_t4076515885_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral3464030460, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		MonoBehaviour_StopCoroutine_m2790918991(__this, _stringLiteral1909762512, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Data::LoadComplete()
extern Il2CppCodeGenString* _stringLiteral2619220150;
extern const uint32_t Data_LoadComplete_m1919134610_MetadataUsageId;
extern "C"  void Data_LoadComplete_m1919134610 (Data_t2122699 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_LoadComplete_m1919134610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral2619220150, /*hidden argument*/NULL);
		return;
	}
}
// System.String Data::LoadJson(System.String)
extern Il2CppClass* TextAsset_t2461560304_il2cpp_TypeInfo_var;
extern const uint32_t Data_LoadJson_m3427881438_MetadataUsageId;
extern "C"  String_t* Data_LoadJson_m3427881438 (Data_t2122699 * __this, String_t* ___url0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Data_LoadJson_m3427881438_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	TextAsset_t2461560304 * V_0 = NULL;
	{
		String_t* L_0 = ___url0;
		Object_t3878351788 * L_1 = Resources_Load_m2187391845(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((TextAsset_t2461560304 *)IsInstClass(L_1, TextAsset_t2461560304_il2cpp_TypeInfo_var));
		TextAsset_t2461560304 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001a;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_001a:
	{
		TextAsset_t2461560304 * L_4 = V_0;
		NullCheck(L_4);
		String_t* L_5 = TextAsset_get_text_m655578209(L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void Data::Remove()
extern "C"  void Data_Remove_m4054779927 (Data_t2122699 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Data/<LoadData>c__Iterator4::.ctor()
extern "C"  void U3CLoadDataU3Ec__Iterator4__ctor_m2258043601 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Data/<LoadData>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CLoadDataU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m308608673 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Object Data/<LoadData>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CLoadDataU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m2709677109 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean Data/<LoadData>c__Iterator4::MoveNext()
extern Il2CppClass* DataUser_t1853738677_il2cpp_TypeInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisDataSetting_t2172603174_m496604577_MethodInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisDataChapters_t2926209968_m2045579131_MethodInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisDataStages_t3269946783_m3591454058_MethodInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisDataAxes_t1853147663_m2862425050_MethodInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisDataMonsters_t1171984835_m1408053134_MethodInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisDataZones_t1635830941_m53944088_MethodInfo_var;
extern const MethodInfo* JsonUtility_FromJson_TisDataPatterns_t2737562829_m970632856_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1090247915;
extern Il2CppCodeGenString* _stringLiteral4138289896;
extern Il2CppCodeGenString* _stringLiteral2209381465;
extern Il2CppCodeGenString* _stringLiteral1852559145;
extern Il2CppCodeGenString* _stringLiteral340924021;
extern Il2CppCodeGenString* _stringLiteral1595495153;
extern Il2CppCodeGenString* _stringLiteral2608183851;
extern Il2CppCodeGenString* _stringLiteral1909762512;
extern const uint32_t U3CLoadDataU3Ec__Iterator4_MoveNext_m577577859_MetadataUsageId;
extern "C"  bool U3CLoadDataU3Ec__Iterator4_MoveNext_m577577859 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadDataU3Ec__Iterator4_MoveNext_m577577859_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		int32_t L_0 = __this->get_U24PC_1();
		V_0 = L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_02ff;
		}
	}
	{
		goto IL_033c;
	}

IL_0021:
	{
		goto IL_02ff;
	}

IL_0026:
	{
		Data_t2122699 * L_2 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_m_load_index_2();
		V_1 = L_3;
		int32_t L_4 = V_1;
		if (L_4 == 0)
		{
			goto IL_0061;
		}
		if (L_4 == 1)
		{
			goto IL_00b3;
		}
		if (L_4 == 2)
		{
			goto IL_0105;
		}
		if (L_4 == 3)
		{
			goto IL_0157;
		}
		if (L_4 == 4)
		{
			goto IL_01b9;
		}
		if (L_4 == 5)
		{
			goto IL_020b;
		}
		if (L_4 == 6)
		{
			goto IL_025d;
		}
		if (L_4 == 7)
		{
			goto IL_02af;
		}
		if (L_4 == 8)
		{
			goto IL_02d4;
		}
	}
	{
		goto IL_02d9;
	}

IL_0061:
	{
		Data_t2122699 * L_5 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_5);
		String_t* L_6 = Data_LoadJson_m3427881438(L_5, _stringLiteral1090247915, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__0_0(L_6);
		String_t* L_7 = __this->get_U3CjsonU3E__0_0();
		if (L_7)
		{
			goto IL_0098;
		}
	}
	{
		Data_t2122699 * L_8 = __this->get_U3CU3Ef__this_3();
		Data_t2122699 * L_9 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_m_load_index_2();
		NullCheck(L_8);
		Data_LoadError_m1812381775(L_8, L_10, /*hidden argument*/NULL);
	}

IL_0098:
	{
		Data_t2122699 * L_11 = __this->get_U3CU3Ef__this_3();
		String_t* L_12 = __this->get_U3CjsonU3E__0_0();
		DataSetting_t2172603174 * L_13 = JsonUtility_FromJson_TisDataSetting_t2172603174_m496604577(NULL /*static, unused*/, L_12, /*hidden argument*/JsonUtility_FromJson_TisDataSetting_t2172603174_m496604577_MethodInfo_var);
		NullCheck(L_11);
		L_11->set_m_setting_4(L_13);
		goto IL_02d9;
	}

IL_00b3:
	{
		Data_t2122699 * L_14 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_14);
		String_t* L_15 = Data_LoadJson_m3427881438(L_14, _stringLiteral4138289896, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__0_0(L_15);
		String_t* L_16 = __this->get_U3CjsonU3E__0_0();
		if (L_16)
		{
			goto IL_00ea;
		}
	}
	{
		Data_t2122699 * L_17 = __this->get_U3CU3Ef__this_3();
		Data_t2122699 * L_18 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_m_load_index_2();
		NullCheck(L_17);
		Data_LoadError_m1812381775(L_17, L_19, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		Data_t2122699 * L_20 = __this->get_U3CU3Ef__this_3();
		String_t* L_21 = __this->get_U3CjsonU3E__0_0();
		DataChapters_t2926209968 * L_22 = JsonUtility_FromJson_TisDataChapters_t2926209968_m2045579131(NULL /*static, unused*/, L_21, /*hidden argument*/JsonUtility_FromJson_TisDataChapters_t2926209968_m2045579131_MethodInfo_var);
		NullCheck(L_20);
		L_20->set_m_chapters_5(L_22);
		goto IL_02d9;
	}

IL_0105:
	{
		Data_t2122699 * L_23 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_23);
		String_t* L_24 = Data_LoadJson_m3427881438(L_23, _stringLiteral2209381465, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__0_0(L_24);
		String_t* L_25 = __this->get_U3CjsonU3E__0_0();
		if (L_25)
		{
			goto IL_013c;
		}
	}
	{
		Data_t2122699 * L_26 = __this->get_U3CU3Ef__this_3();
		Data_t2122699 * L_27 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_27);
		int32_t L_28 = L_27->get_m_load_index_2();
		NullCheck(L_26);
		Data_LoadError_m1812381775(L_26, L_28, /*hidden argument*/NULL);
	}

IL_013c:
	{
		Data_t2122699 * L_29 = __this->get_U3CU3Ef__this_3();
		String_t* L_30 = __this->get_U3CjsonU3E__0_0();
		DataStages_t3269946783 * L_31 = JsonUtility_FromJson_TisDataStages_t3269946783_m3591454058(NULL /*static, unused*/, L_30, /*hidden argument*/JsonUtility_FromJson_TisDataStages_t3269946783_m3591454058_MethodInfo_var);
		NullCheck(L_29);
		L_29->set_m_stages_6(L_31);
		goto IL_02d9;
	}

IL_0157:
	{
		Data_t2122699 * L_32 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_32);
		String_t* L_33 = Data_LoadJson_m3427881438(L_32, _stringLiteral1852559145, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__0_0(L_33);
		String_t* L_34 = __this->get_U3CjsonU3E__0_0();
		if (L_34)
		{
			goto IL_018e;
		}
	}
	{
		Data_t2122699 * L_35 = __this->get_U3CU3Ef__this_3();
		Data_t2122699 * L_36 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_36);
		int32_t L_37 = L_36->get_m_load_index_2();
		NullCheck(L_35);
		Data_LoadError_m1812381775(L_35, L_37, /*hidden argument*/NULL);
	}

IL_018e:
	{
		Data_t2122699 * L_38 = __this->get_U3CU3Ef__this_3();
		String_t* L_39 = __this->get_U3CjsonU3E__0_0();
		DataAxes_t1853147663 * L_40 = JsonUtility_FromJson_TisDataAxes_t1853147663_m2862425050(NULL /*static, unused*/, L_39, /*hidden argument*/JsonUtility_FromJson_TisDataAxes_t1853147663_m2862425050_MethodInfo_var);
		NullCheck(L_38);
		L_38->set_m_axes_7(L_40);
		Data_t2122699 * L_41 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_41);
		DataAxes_t1853147663 * L_42 = L_41->get_m_axes_7();
		NullCheck(L_42);
		DataAxes_PreCalculate_m3246782107(L_42, /*hidden argument*/NULL);
		goto IL_02d9;
	}

IL_01b9:
	{
		Data_t2122699 * L_43 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_43);
		String_t* L_44 = Data_LoadJson_m3427881438(L_43, _stringLiteral340924021, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__0_0(L_44);
		String_t* L_45 = __this->get_U3CjsonU3E__0_0();
		if (L_45)
		{
			goto IL_01f0;
		}
	}
	{
		Data_t2122699 * L_46 = __this->get_U3CU3Ef__this_3();
		Data_t2122699 * L_47 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_47);
		int32_t L_48 = L_47->get_m_load_index_2();
		NullCheck(L_46);
		Data_LoadError_m1812381775(L_46, L_48, /*hidden argument*/NULL);
	}

IL_01f0:
	{
		Data_t2122699 * L_49 = __this->get_U3CU3Ef__this_3();
		String_t* L_50 = __this->get_U3CjsonU3E__0_0();
		DataMonsters_t1171984835 * L_51 = JsonUtility_FromJson_TisDataMonsters_t1171984835_m1408053134(NULL /*static, unused*/, L_50, /*hidden argument*/JsonUtility_FromJson_TisDataMonsters_t1171984835_m1408053134_MethodInfo_var);
		NullCheck(L_49);
		L_49->set_m_monsters_8(L_51);
		goto IL_02d9;
	}

IL_020b:
	{
		Data_t2122699 * L_52 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_52);
		String_t* L_53 = Data_LoadJson_m3427881438(L_52, _stringLiteral1595495153, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__0_0(L_53);
		String_t* L_54 = __this->get_U3CjsonU3E__0_0();
		if (L_54)
		{
			goto IL_0242;
		}
	}
	{
		Data_t2122699 * L_55 = __this->get_U3CU3Ef__this_3();
		Data_t2122699 * L_56 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_56);
		int32_t L_57 = L_56->get_m_load_index_2();
		NullCheck(L_55);
		Data_LoadError_m1812381775(L_55, L_57, /*hidden argument*/NULL);
	}

IL_0242:
	{
		Data_t2122699 * L_58 = __this->get_U3CU3Ef__this_3();
		String_t* L_59 = __this->get_U3CjsonU3E__0_0();
		DataZones_t1635830941 * L_60 = JsonUtility_FromJson_TisDataZones_t1635830941_m53944088(NULL /*static, unused*/, L_59, /*hidden argument*/JsonUtility_FromJson_TisDataZones_t1635830941_m53944088_MethodInfo_var);
		NullCheck(L_58);
		L_58->set_m_zones_9(L_60);
		goto IL_02d9;
	}

IL_025d:
	{
		Data_t2122699 * L_61 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_61);
		String_t* L_62 = Data_LoadJson_m3427881438(L_61, _stringLiteral2608183851, /*hidden argument*/NULL);
		__this->set_U3CjsonU3E__0_0(L_62);
		String_t* L_63 = __this->get_U3CjsonU3E__0_0();
		if (L_63)
		{
			goto IL_0294;
		}
	}
	{
		Data_t2122699 * L_64 = __this->get_U3CU3Ef__this_3();
		Data_t2122699 * L_65 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_65);
		int32_t L_66 = L_65->get_m_load_index_2();
		NullCheck(L_64);
		Data_LoadError_m1812381775(L_64, L_66, /*hidden argument*/NULL);
	}

IL_0294:
	{
		Data_t2122699 * L_67 = __this->get_U3CU3Ef__this_3();
		String_t* L_68 = __this->get_U3CjsonU3E__0_0();
		DataPatterns_t2737562829 * L_69 = JsonUtility_FromJson_TisDataPatterns_t2737562829_m970632856(NULL /*static, unused*/, L_68, /*hidden argument*/JsonUtility_FromJson_TisDataPatterns_t2737562829_m970632856_MethodInfo_var);
		NullCheck(L_67);
		L_67->set_m_patterns_10(L_69);
		goto IL_02d9;
	}

IL_02af:
	{
		Data_t2122699 * L_70 = __this->get_U3CU3Ef__this_3();
		DataUser_t1853738677 * L_71 = (DataUser_t1853738677 *)il2cpp_codegen_object_new(DataUser_t1853738677_il2cpp_TypeInfo_var);
		DataUser__ctor_m1737993094(L_71, /*hidden argument*/NULL);
		NullCheck(L_70);
		L_70->set_m_user_11(L_71);
		Data_t2122699 * L_72 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_72);
		DataUser_t1853738677 * L_73 = L_72->get_m_user_11();
		NullCheck(L_73);
		DataUser_LoadData_m2013187118(L_73, /*hidden argument*/NULL);
		goto IL_02d9;
	}

IL_02d4:
	{
		goto IL_02d9;
	}

IL_02d9:
	{
		Data_t2122699 * L_74 = __this->get_U3CU3Ef__this_3();
		Data_t2122699 * L_75 = L_74;
		NullCheck(L_75);
		int32_t L_76 = L_75->get_m_load_index_2();
		NullCheck(L_75);
		L_75->set_m_load_index_2(((int32_t)((int32_t)L_76+(int32_t)1)));
		__this->set_U24current_2(NULL);
		__this->set_U24PC_1(1);
		goto IL_033e;
	}

IL_02ff:
	{
		Data_t2122699 * L_77 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_77);
		int32_t L_78 = L_77->get_m_load_index_2();
		Data_t2122699 * L_79 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_79);
		int32_t L_80 = L_79->get_m_load_max_3();
		if ((((int32_t)L_78) < ((int32_t)L_80)))
		{
			goto IL_0026;
		}
	}
	{
		Data_t2122699 * L_81 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_81);
		Data_LoadComplete_m1919134610(L_81, /*hidden argument*/NULL);
		Data_t2122699 * L_82 = __this->get_U3CU3Ef__this_3();
		NullCheck(L_82);
		MonoBehaviour_StopCoroutine_m2790918991(L_82, _stringLiteral1909762512, /*hidden argument*/NULL);
		__this->set_U24PC_1((-1));
	}

IL_033c:
	{
		return (bool)0;
	}

IL_033e:
	{
		return (bool)1;
	}
	// Dead block : IL_0340: ldloc.2
}
// System.Void Data/<LoadData>c__Iterator4::Dispose()
extern "C"  void U3CLoadDataU3Ec__Iterator4_Dispose_m919699150 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void Data/<LoadData>c__Iterator4::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CLoadDataU3Ec__Iterator4_Reset_m4199443838_MetadataUsageId;
extern "C"  void U3CLoadDataU3Ec__Iterator4_Reset_m4199443838 (U3CLoadDataU3Ec__Iterator4_t1112965621 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CLoadDataU3Ec__Iterator4_Reset_m4199443838_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void DataAxe::.ctor()
extern "C"  void DataAxe__ctor_m2172165383 (DataAxe_t3107820260 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataAxe::PreCalculate()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t DataAxe_PreCalculate_m3808590496_MetadataUsageId;
extern "C"  void DataAxe_PreCalculate_m3808590496 (DataAxe_t3107820260 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataAxe_PreCalculate_m3808590496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_degree_12();
		__this->set_degree_rad_13(((float)((float)((float)((float)L_0*(float)(3.14159274f)))/(float)(180.0f))));
		float L_1 = __this->get_degree_rad_13();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_2 = sinf(L_1);
		__this->set_sin_14(L_2);
		float L_3 = __this->get_degree_rad_13();
		float L_4 = cosf(L_3);
		__this->set_cos_15(L_4);
		return;
	}
}
// System.Void DataAxe::CalculateVelocity(System.Single)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t DataAxe_CalculateVelocity_m3396167907_MetadataUsageId;
extern "C"  void DataAxe_CalculateVelocity_m3396167907 (DataAxe_t3107820260 * __this, float ___distance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataAxe_CalculateVelocity_m3396167907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = __this->get_gravity_16();
		float L_1 = ___distance0;
		DataSetting_t2172603174 * L_2 = Data_get_settings_m2852704140(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		float L_3 = L_2->get_MIN_DISTANCE_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_4 = powf(((float)((float)L_1*(float)L_3)), (2.0f));
		float L_5 = ___distance0;
		DataSetting_t2172603174 * L_6 = Data_get_settings_m2852704140(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		float L_7 = L_6->get_MIN_DISTANCE_4();
		float L_8 = __this->get_sin_14();
		float L_9 = __this->get_cos_15();
		float L_10 = __this->get_throw_height_5();
		float L_11 = __this->get_cos_15();
		float L_12 = powf(L_11, (2.0f));
		float L_13 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_4))/(float)(2.0f)))/(float)((float)((float)((float)((float)((float)((float)((float)((float)L_5*(float)L_7))*(float)L_8))*(float)L_9))+(float)((float)((float)L_10*(float)L_12)))))));
		V_0 = L_13;
		float L_14 = __this->get_gravity_16();
		float L_15 = ___distance0;
		DataSetting_t2172603174 * L_16 = Data_get_settings_m2852704140(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		float L_17 = L_16->get_MAX_DISTANCE_3();
		float L_18 = powf(((float)((float)L_15*(float)L_17)), (2.0f));
		float L_19 = ___distance0;
		DataSetting_t2172603174 * L_20 = Data_get_settings_m2852704140(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		float L_21 = L_20->get_MAX_DISTANCE_3();
		float L_22 = __this->get_sin_14();
		float L_23 = __this->get_cos_15();
		float L_24 = __this->get_throw_height_5();
		float L_25 = __this->get_cos_15();
		float L_26 = powf(L_25, (2.0f));
		float L_27 = sqrtf(((float)((float)((float)((float)((float)((float)L_14*(float)L_18))/(float)(2.0f)))/(float)((float)((float)((float)((float)((float)((float)((float)((float)L_19*(float)L_21))*(float)L_22))*(float)L_23))+(float)((float)((float)L_24*(float)L_26)))))));
		V_1 = L_27;
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_2));
		Vector3_t3525329789  L_28 = V_2;
		__this->set_min_velocity_19(L_28);
		Vector3_t3525329789 * L_29 = __this->get_address_of_min_velocity_19();
		L_29->set_x_1((0.0f));
		Vector3_t3525329789 * L_30 = __this->get_address_of_min_velocity_19();
		float L_31 = V_0;
		float L_32 = __this->get_sin_14();
		L_30->set_y_2(((float)((float)L_31*(float)L_32)));
		Vector3_t3525329789 * L_33 = __this->get_address_of_min_velocity_19();
		float L_34 = V_0;
		float L_35 = __this->get_cos_15();
		L_33->set_z_3(((float)((float)L_34*(float)L_35)));
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_3));
		Vector3_t3525329789  L_36 = V_3;
		__this->set_max_velocity_18(L_36);
		Vector3_t3525329789 * L_37 = __this->get_address_of_max_velocity_18();
		L_37->set_x_1((0.0f));
		Vector3_t3525329789 * L_38 = __this->get_address_of_max_velocity_18();
		float L_39 = V_1;
		float L_40 = __this->get_sin_14();
		L_38->set_y_2(((float)((float)L_39*(float)L_40)));
		Vector3_t3525329789 * L_41 = __this->get_address_of_max_velocity_18();
		float L_42 = V_1;
		float L_43 = __this->get_cos_15();
		L_41->set_z_3(((float)((float)L_42*(float)L_43)));
		return;
	}
}
// System.Void DataAxe::Finalize()
extern "C"  void DataAxe_Finalize_m3104150011 (DataAxe_t3107820260 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0xC, FINALLY_0005);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0005;
	}

FINALLY_0005:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(5)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(5)
	{
		IL2CPP_JUMP_TBL(0xC, IL_000c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_000c:
	{
		return;
	}
}
// System.Void DataAxes::.ctor()
extern "C"  void DataAxes__ctor_m3759986028 (DataAxes_t1853147663 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataAxes::PreCalculate()
extern Il2CppClass* Enumerator_t1990562221_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1141275782_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3574942278_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3230144266_MethodInfo_var;
extern const uint32_t DataAxes_PreCalculate_m3246782107_MetadataUsageId;
extern "C"  void DataAxes_PreCalculate_m3246782107 (DataAxes_t1853147663 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataAxes_PreCalculate_m3246782107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataAxe_t3107820260 * V_0 = NULL;
	Enumerator_t1990562221  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3904779229 * L_0 = __this->get_axes_0();
		NullCheck(L_0);
		Enumerator_t1990562221  L_1 = List_1_GetEnumerator_m1141275782(L_0, /*hidden argument*/List_1_GetEnumerator_m1141275782_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_001f;
		}

IL_0011:
		{
			DataAxe_t3107820260 * L_2 = Enumerator_get_Current_m3574942278((&V_1), /*hidden argument*/Enumerator_get_Current_m3574942278_MethodInfo_var);
			V_0 = L_2;
			DataAxe_t3107820260 * L_3 = V_0;
			NullCheck(L_3);
			DataAxe_PreCalculate_m3808590496(L_3, /*hidden argument*/NULL);
		}

IL_001f:
		{
			bool L_4 = Enumerator_MoveNext_m3230144266((&V_1), /*hidden argument*/Enumerator_MoveNext_m3230144266_MethodInfo_var);
			if (L_4)
			{
				goto IL_0011;
			}
		}

IL_002b:
		{
			IL2CPP_LEAVE(0x3C, FINALLY_0030);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0030;
	}

FINALLY_0030:
	{ // begin finally (depth: 1)
		Enumerator_t1990562221  L_5 = V_1;
		Enumerator_t1990562221  L_6 = L_5;
		Il2CppObject * L_7 = Box(Enumerator_t1990562221_il2cpp_TypeInfo_var, &L_6);
		NullCheck((Il2CppObject *)L_7);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_7);
		IL2CPP_END_FINALLY(48)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(48)
	{
		IL2CPP_JUMP_TBL(0x3C, IL_003c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_003c:
	{
		return;
	}
}
// DataAxe DataAxes::getData(System.UInt32)
extern Il2CppClass* Enumerator_t1990562221_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1141275782_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3574942278_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3230144266_MethodInfo_var;
extern const uint32_t DataAxes_getData_m1458414431_MetadataUsageId;
extern "C"  DataAxe_t3107820260 * DataAxes_getData_m1458414431 (DataAxes_t1853147663 * __this, uint32_t ___axe_id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataAxes_getData_m1458414431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataAxe_t3107820260 * V_0 = NULL;
	Enumerator_t1990562221  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DataAxe_t3107820260 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t3904779229 * L_0 = __this->get_axes_0();
		NullCheck(L_0);
		Enumerator_t1990562221  L_1 = List_1_GetEnumerator_m1141275782(L_0, /*hidden argument*/List_1_GetEnumerator_m1141275782_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			DataAxe_t3107820260 * L_2 = Enumerator_get_Current_m3574942278((&V_1), /*hidden argument*/Enumerator_get_Current_m3574942278_MethodInfo_var);
			V_0 = L_2;
			DataAxe_t3107820260 * L_3 = V_0;
			NullCheck(L_3);
			uint32_t L_4 = L_3->get_id_0();
			uint32_t L_5 = ___axe_id0;
			if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
			{
				goto IL_002c;
			}
		}

IL_0025:
		{
			DataAxe_t3107820260 * L_6 = V_0;
			V_2 = L_6;
			IL2CPP_LEAVE(0x4B, FINALLY_003d);
		}

IL_002c:
		{
			bool L_7 = Enumerator_MoveNext_m3230144266((&V_1), /*hidden argument*/Enumerator_MoveNext_m3230144266_MethodInfo_var);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Enumerator_t1990562221  L_8 = V_1;
		Enumerator_t1990562221  L_9 = L_8;
		Il2CppObject * L_10 = Box(Enumerator_t1990562221_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0049:
	{
		return (DataAxe_t3107820260 *)NULL;
	}

IL_004b:
	{
		DataAxe_t3107820260 * L_11 = V_2;
		return L_11;
	}
}
// System.Void DataBossMode::.ctor()
extern "C"  void DataBossMode__ctor_m1563572033 (DataBossMode_t3617900698 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataChapter::.ctor()
extern "C"  void DataChapter__ctor_m3863566760 (DataChapter_t925677859 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataChapters::.ctor()
extern "C"  void DataChapters__ctor_m358853867 (DataChapters_t2926209968 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// DataChapter DataChapters::GetChapterData(System.Int32)
extern Il2CppClass* Enumerator_t4103387116_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m1108510341_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2627363589_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3070894507_MethodInfo_var;
extern const uint32_t DataChapters_GetChapterData_m2981922363_MetadataUsageId;
extern "C"  DataChapter_t925677859 * DataChapters_GetChapterData_m2981922363 (DataChapters_t2926209968 * __this, int32_t ___chapter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataChapters_GetChapterData_m2981922363_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataChapter_t925677859 * V_0 = NULL;
	Enumerator_t4103387116  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DataChapter_t925677859 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t1722636828 * L_0 = __this->get_chapters_0();
		NullCheck(L_0);
		Enumerator_t4103387116  L_1 = List_1_GetEnumerator_m1108510341(L_0, /*hidden argument*/List_1_GetEnumerator_m1108510341_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			DataChapter_t925677859 * L_2 = Enumerator_get_Current_m2627363589((&V_1), /*hidden argument*/Enumerator_get_Current_m2627363589_MethodInfo_var);
			V_0 = L_2;
			DataChapter_t925677859 * L_3 = V_0;
			NullCheck(L_3);
			int32_t L_4 = L_3->get_chapter_0();
			int32_t L_5 = ___chapter0;
			if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
			{
				goto IL_002c;
			}
		}

IL_0025:
		{
			DataChapter_t925677859 * L_6 = V_0;
			V_2 = L_6;
			IL2CPP_LEAVE(0x4B, FINALLY_003d);
		}

IL_002c:
		{
			bool L_7 = Enumerator_MoveNext_m3070894507((&V_1), /*hidden argument*/Enumerator_MoveNext_m3070894507_MethodInfo_var);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Enumerator_t4103387116  L_8 = V_1;
		Enumerator_t4103387116  L_9 = L_8;
		Il2CppObject * L_10 = Box(Enumerator_t4103387116_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0049:
	{
		return (DataChapter_t925677859 *)NULL;
	}

IL_004b:
	{
		DataChapter_t925677859 * L_11 = V_2;
		return L_11;
	}
}
// System.Void DataFeverMode::.ctor()
extern "C"  void DataFeverMode__ctor_m2102503534 (DataFeverMode_t3892525469 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataMonster::.ctor()
extern "C"  void DataMonster__ctor_m1858616507 (DataMonster_t1423279280 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataMonsterAni::.ctor()
extern "C"  void DataMonsterAni__ctor_m3445160719 (DataMonsterAni_t995950348 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String DataMonsterAni::GetAnimation(MONSTER_STATE)
extern "C"  String_t* DataMonsterAni_GetAnimation_m2683680960 (DataMonsterAni_t995950348 * __this, int32_t ___state0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___state0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_001d;
		}
		if (L_1 == 1)
		{
			goto IL_003a;
		}
		if (L_1 == 2)
		{
			goto IL_0057;
		}
		if (L_1 == 3)
		{
			goto IL_0074;
		}
	}
	{
		goto IL_0091;
	}

IL_001d:
	{
		List_1_t1765447871 * L_2 = __this->get_idle_0();
		List_1_t1765447871 * L_3 = __this->get_idle_0();
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_3);
		int32_t L_5 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_6 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_2, L_5);
		return L_6;
	}

IL_003a:
	{
		List_1_t1765447871 * L_7 = __this->get_hit_1();
		List_1_t1765447871 * L_8 = __this->get_hit_1();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_8);
		int32_t L_10 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		String_t* L_11 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_7, L_10);
		return L_11;
	}

IL_0057:
	{
		List_1_t1765447871 * L_12 = __this->get_critical_hit_2();
		List_1_t1765447871 * L_13 = __this->get_critical_hit_2();
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_13);
		int32_t L_15 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_16 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_12, L_15);
		return L_16;
	}

IL_0074:
	{
		List_1_t1765447871 * L_17 = __this->get_die_3();
		List_1_t1765447871 * L_18 = __this->get_die_3();
		NullCheck(L_18);
		int32_t L_19 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_18);
		int32_t L_20 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		String_t* L_21 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_17, L_20);
		return L_21;
	}

IL_0091:
	{
		return (String_t*)NULL;
	}
}
// System.Void DataMonsters::.ctor()
extern "C"  void DataMonsters__ctor_m2629905464 (DataMonsters_t1171984835 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// DataMonster DataMonsters::getData(System.UInt32)
extern Il2CppClass* Enumerator_t306021241_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2820040914_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2329703570_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1349206846_MethodInfo_var;
extern const uint32_t DataMonsters_getData_m2242462663_MetadataUsageId;
extern "C"  DataMonster_t1423279280 * DataMonsters_getData_m2242462663 (DataMonsters_t1171984835 * __this, uint32_t ___monster_id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataMonsters_getData_m2242462663_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataMonster_t1423279280 * V_0 = NULL;
	Enumerator_t306021241  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DataMonster_t1423279280 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2220238249 * L_0 = __this->get_monsters_0();
		NullCheck(L_0);
		Enumerator_t306021241  L_1 = List_1_GetEnumerator_m2820040914(L_0, /*hidden argument*/List_1_GetEnumerator_m2820040914_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			DataMonster_t1423279280 * L_2 = Enumerator_get_Current_m2329703570((&V_1), /*hidden argument*/Enumerator_get_Current_m2329703570_MethodInfo_var);
			V_0 = L_2;
			DataMonster_t1423279280 * L_3 = V_0;
			NullCheck(L_3);
			uint32_t L_4 = L_3->get_id_0();
			uint32_t L_5 = ___monster_id0;
			if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
			{
				goto IL_002c;
			}
		}

IL_0025:
		{
			DataMonster_t1423279280 * L_6 = V_0;
			V_2 = L_6;
			IL2CPP_LEAVE(0x4B, FINALLY_003d);
		}

IL_002c:
		{
			bool L_7 = Enumerator_MoveNext_m1349206846((&V_1), /*hidden argument*/Enumerator_MoveNext_m1349206846_MethodInfo_var);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Enumerator_t306021241  L_8 = V_1;
		Enumerator_t306021241  L_9 = L_8;
		Il2CppObject * L_10 = Box(Enumerator_t306021241_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0049:
	{
		return (DataMonster_t1423279280 *)NULL;
	}

IL_004b:
	{
		DataMonster_t1423279280 * L_11 = V_2;
		return L_11;
	}
}
// System.Void DataNormalMode::.ctor()
extern "C"  void DataNormalMode__ctor_m1559120007 (DataNormalMode_t1907108500 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String DataNormalMode::GetType()
extern "C"  String_t* DataNormalMode_GetType_m910607250 (DataNormalMode_t1907108500 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_type_4();
		List_1_t1765447871 * L_1 = __this->get_type_4();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_1);
		int32_t L_3 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_4 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_0, L_3);
		return L_4;
	}
}
// System.Int32 DataNormalMode::GetRandomCount()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t DataNormalMode_GetRandomCount_m1036559937_MetadataUsageId;
extern "C"  int32_t DataNormalMode_GetRandomCount_m1036559937 (DataNormalMode_t1907108500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataNormalMode_GetRandomCount_m1036559937_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_count_min_5();
		int32_t L_1 = __this->get_count_max_6();
		int32_t L_2 = __this->get_count_min_5();
		float L_3 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)(((float)((float)((int32_t)((int32_t)L_1-(int32_t)L_2)))))*(float)L_3)), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)L_4));
	}
}
// System.Int32 DataNormalMode::GetCount(System.Single)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t DataNormalMode_GetCount_m1647762599_MetadataUsageId;
extern "C"  int32_t DataNormalMode_GetCount_m1647762599 (DataNormalMode_t1907108500 * __this, float ___current0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataNormalMode_GetCount_m1647762599_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_count_min_5();
		float L_1 = ___current0;
		float L_2 = __this->get_max_3();
		int32_t L_3 = __this->get_count_max_6();
		int32_t L_4 = __this->get_count_min_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)((float)((float)L_1/(float)L_2))*(float)(((float)((float)((int32_t)((int32_t)L_3-(int32_t)L_4))))))), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)L_5));
	}
}
// System.Single DataNormalMode::GetRandomDelay()
extern "C"  float DataNormalMode_GetRandomDelay_m665011315 (DataNormalMode_t1907108500 * __this, const MethodInfo* method)
{
	{
		List_1_t1755167990 * L_0 = __this->get_delay_7();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker1< float, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32) */, L_0, 0);
		List_1_t1755167990 * L_2 = __this->get_delay_7();
		NullCheck(L_2);
		float L_3 = VirtFuncInvoker1< float, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32) */, L_2, 1);
		List_1_t1755167990 * L_4 = __this->get_delay_7();
		NullCheck(L_4);
		float L_5 = VirtFuncInvoker1< float, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32) */, L_4, 0);
		float L_6 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((float)((float)L_1+(float)((float)((float)((float)((float)L_3-(float)L_5))*(float)L_6))));
	}
}
// System.Single DataNormalMode::GetDelay(System.Single)
extern "C"  float DataNormalMode_GetDelay_m2525530421 (DataNormalMode_t1907108500 * __this, float ___current0, const MethodInfo* method)
{
	{
		List_1_t1755167990 * L_0 = __this->get_delay_7();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker1< float, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32) */, L_0, 0);
		List_1_t1755167990 * L_2 = __this->get_delay_7();
		NullCheck(L_2);
		float L_3 = VirtFuncInvoker1< float, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32) */, L_2, 1);
		List_1_t1755167990 * L_4 = __this->get_delay_7();
		NullCheck(L_4);
		float L_5 = VirtFuncInvoker1< float, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Single>::get_Item(System.Int32) */, L_4, 0);
		float L_6 = ___current0;
		float L_7 = __this->get_max_3();
		return ((float)((float)L_1+(float)((float)((float)((float)((float)((float)((float)L_3-(float)L_5))*(float)L_6))/(float)L_7))));
	}
}
// System.Void DataObject::.ctor()
extern "C"  void DataObject__ctor_m4076306 (DataObject_t3139072937 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataPattern::.ctor()
extern "C"  void DataPattern__ctor_m2923587013 (DataPattern_t3690539110 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataPatterns::.ctor()
extern "C"  void DataPatterns__ctor_m1284252782 (DataPatterns_t2737562829 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// DataPattern DataPatterns::getData(System.UInt32)
extern Il2CppClass* Enumerator_t2573281071_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2144914184_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3978161224_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2234898248_MethodInfo_var;
extern const uint32_t DataPatterns_getData_m4186287835_MetadataUsageId;
extern "C"  DataPattern_t3690539110 * DataPatterns_getData_m4186287835 (DataPatterns_t2737562829 * __this, uint32_t ___pattern_id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataPatterns_getData_m4186287835_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataPattern_t3690539110 * V_0 = NULL;
	Enumerator_t2573281071  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DataPattern_t3690539110 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t192530783 * L_0 = __this->get_patterns_0();
		NullCheck(L_0);
		Enumerator_t2573281071  L_1 = List_1_GetEnumerator_m2144914184(L_0, /*hidden argument*/List_1_GetEnumerator_m2144914184_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			DataPattern_t3690539110 * L_2 = Enumerator_get_Current_m3978161224((&V_1), /*hidden argument*/Enumerator_get_Current_m3978161224_MethodInfo_var);
			V_0 = L_2;
			DataPattern_t3690539110 * L_3 = V_0;
			NullCheck(L_3);
			uint32_t L_4 = L_3->get_id_0();
			uint32_t L_5 = ___pattern_id0;
			if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
			{
				goto IL_002c;
			}
		}

IL_0025:
		{
			DataPattern_t3690539110 * L_6 = V_0;
			V_2 = L_6;
			IL2CPP_LEAVE(0x4B, FINALLY_003d);
		}

IL_002c:
		{
			bool L_7 = Enumerator_MoveNext_m2234898248((&V_1), /*hidden argument*/Enumerator_MoveNext_m2234898248_MethodInfo_var);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Enumerator_t2573281071  L_8 = V_1;
		Enumerator_t2573281071  L_9 = L_8;
		Il2CppObject * L_10 = Box(Enumerator_t2573281071_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0049:
	{
		return (DataPattern_t3690539110 *)NULL;
	}

IL_004b:
	{
		DataPattern_t3690539110 * L_11 = V_2;
		return L_11;
	}
}
// System.Void DataSetting::.ctor()
extern "C"  void DataSetting__ctor_m3617020677 (DataSetting_t2172603174 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataStage::.ctor()
extern "C"  void DataStage__ctor_m134780567 (DataStage_t1629502804 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataStageChapter::.ctor()
extern "C"  void DataStageChapter__ctor_m1999800930 (DataStageChapter_t1740173401 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// DataStage DataStageChapter::GetStageData(System.Int32)
extern Il2CppClass* Enumerator_t512244765_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2855169462_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m213173494_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m249099226_MethodInfo_var;
extern const uint32_t DataStageChapter_GetStageData_m1832735364_MetadataUsageId;
extern "C"  DataStage_t1629502804 * DataStageChapter_GetStageData_m1832735364 (DataStageChapter_t1740173401 * __this, int32_t ___stage0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataStageChapter_GetStageData_m1832735364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataStage_t1629502804 * V_0 = NULL;
	Enumerator_t512244765  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DataStage_t1629502804 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2426461773 * L_0 = __this->get_stages_1();
		NullCheck(L_0);
		Enumerator_t512244765  L_1 = List_1_GetEnumerator_m2855169462(L_0, /*hidden argument*/List_1_GetEnumerator_m2855169462_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			DataStage_t1629502804 * L_2 = Enumerator_get_Current_m213173494((&V_1), /*hidden argument*/Enumerator_get_Current_m213173494_MethodInfo_var);
			V_0 = L_2;
			DataStage_t1629502804 * L_3 = V_0;
			NullCheck(L_3);
			int32_t L_4 = L_3->get_stage_0();
			int32_t L_5 = ___stage0;
			if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
			{
				goto IL_002c;
			}
		}

IL_0025:
		{
			DataStage_t1629502804 * L_6 = V_0;
			V_2 = L_6;
			IL2CPP_LEAVE(0x4B, FINALLY_003d);
		}

IL_002c:
		{
			bool L_7 = Enumerator_MoveNext_m249099226((&V_1), /*hidden argument*/Enumerator_MoveNext_m249099226_MethodInfo_var);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Enumerator_t512244765  L_8 = V_1;
		Enumerator_t512244765  L_9 = L_8;
		Il2CppObject * L_10 = Box(Enumerator_t512244765_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0049:
	{
		return (DataStage_t1629502804 *)NULL;
	}

IL_004b:
	{
		DataStage_t1629502804 * L_11 = V_2;
		return L_11;
	}
}
// System.Void DataStages::.ctor()
extern "C"  void DataStages__ctor_m730598876 (DataStages_t3269946783 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// DataStage DataStages::GetStageData(System.Int32,System.Int32)
extern "C"  DataStage_t1629502804 * DataStages_GetStageData_m4270061837 (DataStages_t3269946783 * __this, int32_t ___chapter0, int32_t ___stage1, const MethodInfo* method)
{
	DataStageChapter_t1740173401 * V_0 = NULL;
	DataStage_t1629502804 * V_1 = NULL;
	{
		int32_t L_0 = ___chapter0;
		DataStageChapter_t1740173401 * L_1 = DataStages_GetChapterData_m76190110(__this, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		DataStageChapter_t1740173401 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		DataStageChapter_t1740173401 * L_3 = V_0;
		int32_t L_4 = ___stage1;
		NullCheck(L_3);
		DataStage_t1629502804 * L_5 = DataStageChapter_GetStageData_m1832735364(L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		DataStage_t1629502804 * L_6 = V_1;
		return L_6;
	}

IL_0018:
	{
		return (DataStage_t1629502804 *)NULL;
	}
}
// DataStageChapter DataStages::GetChapterData(System.Int32)
extern Il2CppClass* Enumerator_t622915362_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m2705933265_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3963148861_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3960828735_MethodInfo_var;
extern const uint32_t DataStages_GetChapterData_m76190110_MetadataUsageId;
extern "C"  DataStageChapter_t1740173401 * DataStages_GetChapterData_m76190110 (DataStages_t3269946783 * __this, int32_t ___chapter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataStages_GetChapterData_m76190110_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataStageChapter_t1740173401 * V_0 = NULL;
	Enumerator_t622915362  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DataStageChapter_t1740173401 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2537132370 * L_0 = __this->get_chapters_0();
		NullCheck(L_0);
		Enumerator_t622915362  L_1 = List_1_GetEnumerator_m2705933265(L_0, /*hidden argument*/List_1_GetEnumerator_m2705933265_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			DataStageChapter_t1740173401 * L_2 = Enumerator_get_Current_m3963148861((&V_1), /*hidden argument*/Enumerator_get_Current_m3963148861_MethodInfo_var);
			V_0 = L_2;
			DataStageChapter_t1740173401 * L_3 = V_0;
			NullCheck(L_3);
			int32_t L_4 = L_3->get_chapter_0();
			int32_t L_5 = ___chapter0;
			if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
			{
				goto IL_002c;
			}
		}

IL_0025:
		{
			DataStageChapter_t1740173401 * L_6 = V_0;
			V_2 = L_6;
			IL2CPP_LEAVE(0x4B, FINALLY_003d);
		}

IL_002c:
		{
			bool L_7 = Enumerator_MoveNext_m3960828735((&V_1), /*hidden argument*/Enumerator_MoveNext_m3960828735_MethodInfo_var);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Enumerator_t622915362  L_8 = V_1;
		Enumerator_t622915362  L_9 = L_8;
		Il2CppObject * L_10 = Box(Enumerator_t622915362_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0049:
	{
		return (DataStageChapter_t1740173401 *)NULL;
	}

IL_004b:
	{
		DataStageChapter_t1740173401 * L_11 = V_2;
		return L_11;
	}
}
// System.Void DataUser::.ctor()
extern "C"  void DataUser__ctor_m1737993094 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String DataUser::get_id()
extern "C"  String_t* DataUser_get_id_m1162814117 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_m_id_0();
		return L_0;
	}
}
// System.Void DataUser::set_id(System.String)
extern Il2CppCodeGenString* _stringLiteral3355;
extern const uint32_t DataUser_set_id_m147616012_MetadataUsageId;
extern "C"  void DataUser_set_id_m147616012 (DataUser_t1853738677 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataUser_set_id_m147616012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		__this->set_m_id_0(L_0);
		String_t* L_1 = __this->get_m_id_0();
		DataUser_SetString_m3018843111(__this, _stringLiteral3355, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 DataUser::get_coin()
extern "C"  int32_t DataUser_get_coin_m2506806470 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_coin_1();
		return L_0;
	}
}
// System.Void DataUser::set_coin(System.Int32)
extern Il2CppCodeGenString* _stringLiteral3059345;
extern const uint32_t DataUser_set_coin_m154068605_MetadataUsageId;
extern "C"  void DataUser_set_coin_m154068605 (DataUser_t1853738677 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataUser_set_coin_m154068605_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_m_coin_1(L_0);
		int32_t L_1 = __this->get_m_coin_1();
		DataUser_SetInt_m916432896(__this, _stringLiteral3059345, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 DataUser::get_diamond()
extern "C"  int32_t DataUser_get_diamond_m1529789281 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_diamond_2();
		return L_0;
	}
}
// System.Void DataUser::set_diamond(System.Int32)
extern Il2CppCodeGenString* _stringLiteral1655054676;
extern const uint32_t DataUser_set_diamond_m1515606028_MetadataUsageId;
extern "C"  void DataUser_set_diamond_m1515606028 (DataUser_t1853738677 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataUser_set_diamond_m1515606028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_m_diamond_2(L_0);
		int32_t L_1 = __this->get_m_diamond_2();
		DataUser_SetInt_m916432896(__this, _stringLiteral1655054676, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 DataUser::get_chapter()
extern "C"  int32_t DataUser_get_chapter_m1684683802 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_chapter_3();
		return L_0;
	}
}
// System.Void DataUser::set_chapter(System.Int32)
extern Il2CppCodeGenString* _stringLiteral739015757;
extern const uint32_t DataUser_set_chapter_m1124172613_MetadataUsageId;
extern "C"  void DataUser_set_chapter_m1124172613 (DataUser_t1853738677 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataUser_set_chapter_m1124172613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_m_chapter_3(L_0);
		int32_t L_1 = __this->get_m_chapter_3();
		DataUser_SetInt_m916432896(__this, _stringLiteral739015757, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 DataUser::get_stage()
extern "C"  int32_t DataUser_get_stage_m1852353931 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_stage_4();
		return L_0;
	}
}
// System.Void DataUser::set_stage(System.Int32)
extern Il2CppCodeGenString* _stringLiteral109757182;
extern const uint32_t DataUser_set_stage_m866959798_MetadataUsageId;
extern "C"  void DataUser_set_stage_m866959798 (DataUser_t1853738677 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataUser_set_stage_m866959798_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___value0;
		__this->set_m_stage_4(L_0);
		int32_t L_1 = __this->get_m_stage_4();
		DataUser_SetInt_m916432896(__this, _stringLiteral109757182, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataUser::Clear()
extern "C"  void DataUser_Clear_m3439093681 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void DataUser::Finalize()
extern "C"  void DataUser_Finalize_m824016668 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0xC, FINALLY_0005);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0005;
	}

FINALLY_0005:
	{ // begin finally (depth: 1)
		Object_Finalize_m3027285644(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(5)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(5)
	{
		IL2CPP_JUMP_TBL(0xC, IL_000c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_000c:
	{
		return;
	}
}
// System.Void DataUser::LoadData()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3355;
extern Il2CppCodeGenString* _stringLiteral3059345;
extern Il2CppCodeGenString* _stringLiteral1655054676;
extern Il2CppCodeGenString* _stringLiteral739015757;
extern Il2CppCodeGenString* _stringLiteral109757182;
extern const uint32_t DataUser_LoadData_m2013187118_MetadataUsageId;
extern "C"  void DataUser_LoadData_m2013187118 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataUser_LoadData_m2013187118_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = DataUser_GetString_m3856822746(__this, _stringLiteral3355, /*hidden argument*/NULL);
		__this->set_m_id_0(L_0);
		String_t* L_1 = __this->get_m_id_0();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002d;
		}
	}
	{
		DataUser_MakeNewData_m516225792(__this, /*hidden argument*/NULL);
		return;
	}

IL_002d:
	{
		int32_t L_4 = DataUser_GetInt_m3777241181(__this, _stringLiteral3059345, /*hidden argument*/NULL);
		__this->set_m_coin_1(L_4);
		int32_t L_5 = DataUser_GetInt_m3777241181(__this, _stringLiteral1655054676, /*hidden argument*/NULL);
		__this->set_m_diamond_2(L_5);
		int32_t L_6 = DataUser_GetInt_m3777241181(__this, _stringLiteral739015757, /*hidden argument*/NULL);
		__this->set_m_chapter_3(L_6);
		int32_t L_7 = DataUser_GetInt_m3777241181(__this, _stringLiteral109757182, /*hidden argument*/NULL);
		__this->set_m_stage_4(L_7);
		return;
	}
}
// System.Void DataUser::MakeNewData()
extern "C"  void DataUser_MakeNewData_m516225792 (DataUser_t1853738677 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = SystemInfo_get_deviceUniqueIdentifier_m983206480(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataUser_set_id_m147616012(__this, L_0, /*hidden argument*/NULL);
		DataUser_set_coin_m154068605(__this, 0, /*hidden argument*/NULL);
		DataUser_set_diamond_m1515606028(__this, 0, /*hidden argument*/NULL);
		DataUser_set_chapter_m1124172613(__this, 1, /*hidden argument*/NULL);
		DataUser_set_stage_m866959798(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataUser::SetInt(System.String,System.Int32)
extern "C"  void DataUser_SetInt_m916432896 (DataUser_t1853738677 * __this, String_t* ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		int32_t L_1 = ___value1;
		PlayerPrefs_SetInt_m3485171996(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataUser::SetFloat(System.String,System.Single)
extern "C"  void DataUser_SetFloat_m4108343375 (DataUser_t1853738677 * __this, String_t* ___key0, float ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		float L_1 = ___value1;
		PlayerPrefs_SetFloat_m1687591347(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataUser::SetString(System.String,System.String)
extern "C"  void DataUser_SetString_m3018843111 (DataUser_t1853738677 * __this, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = ___value1;
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 DataUser::GetInt(System.String)
extern "C"  int32_t DataUser_GetInt_m3777241181 (DataUser_t1853738677 * __this, String_t* ___key0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		int32_t L_1 = PlayerPrefs_GetInt_m1334009359(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single DataUser::GetFloat(System.String)
extern "C"  float DataUser_GetFloat_m1584966802 (DataUser_t1853738677 * __this, String_t* ___key0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		float L_1 = PlayerPrefs_GetFloat_m4179026766(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.String DataUser::GetString(System.String)
extern "C"  String_t* DataUser_GetString_m3856822746 (DataUser_t1853738677 * __this, String_t* ___key0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___key0;
		String_t* L_1 = PlayerPrefs_GetString_m378864272(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void DataZone::.ctor()
extern "C"  void DataZone__ctor_m3316629701 (DataZone_t1853884054 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DataZones::.ctor()
extern "C"  void DataZones__ctor_m583674222 (DataZones_t1635830941 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// DataZone DataZones::getData(System.UInt32)
extern Il2CppClass* Enumerator_t736626015_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m532983758_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3876460410_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m2151607586_MethodInfo_var;
extern const uint32_t DataZones_getData_m970677351_MetadataUsageId;
extern "C"  DataZone_t1853884054 * DataZones_getData_m970677351 (DataZones_t1635830941 * __this, uint32_t ___zone_id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DataZones_getData_m970677351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataZone_t1853884054 * V_0 = NULL;
	Enumerator_t736626015  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DataZone_t1853884054 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t2650843023 * L_0 = __this->get_zones_0();
		NullCheck(L_0);
		Enumerator_t736626015  L_1 = List_1_GetEnumerator_m532983758(L_0, /*hidden argument*/List_1_GetEnumerator_m532983758_MethodInfo_var);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002c;
		}

IL_0011:
		{
			DataZone_t1853884054 * L_2 = Enumerator_get_Current_m3876460410((&V_1), /*hidden argument*/Enumerator_get_Current_m3876460410_MethodInfo_var);
			V_0 = L_2;
			DataZone_t1853884054 * L_3 = V_0;
			NullCheck(L_3);
			uint32_t L_4 = L_3->get_id_0();
			uint32_t L_5 = ___zone_id0;
			if ((!(((uint32_t)L_4) == ((uint32_t)L_5))))
			{
				goto IL_002c;
			}
		}

IL_0025:
		{
			DataZone_t1853884054 * L_6 = V_0;
			V_2 = L_6;
			IL2CPP_LEAVE(0x4B, FINALLY_003d);
		}

IL_002c:
		{
			bool L_7 = Enumerator_MoveNext_m2151607586((&V_1), /*hidden argument*/Enumerator_MoveNext_m2151607586_MethodInfo_var);
			if (L_7)
			{
				goto IL_0011;
			}
		}

IL_0038:
		{
			IL2CPP_LEAVE(0x49, FINALLY_003d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_003d;
	}

FINALLY_003d:
	{ // begin finally (depth: 1)
		Enumerator_t736626015  L_8 = V_1;
		Enumerator_t736626015  L_9 = L_8;
		Il2CppObject * L_10 = Box(Enumerator_t736626015_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Il2CppObject *)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_10);
		IL2CPP_END_FINALLY(61)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(61)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_JUMP_TBL(0x49, IL_0049)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0049:
	{
		return (DataZone_t1853884054 *)NULL;
	}

IL_004b:
	{
		DataZone_t1853884054 * L_11 = V_2;
		return L_11;
	}
}
// System.Void Diver::.ctor()
extern "C"  void Diver__ctor_m225321549 (Diver_t66044126 * __this, const MethodInfo* method)
{
	{
		Character__ctor_m90739842(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Diver::get_IsMonster()
extern "C"  bool Diver_get_IsMonster_m1684747526 (Diver_t66044126 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_m_isMonster_7();
		return L_0;
	}
}
// System.UInt32 Diver::get_MonsterID()
extern "C"  uint32_t Diver_get_MonsterID_m3449010450 (Diver_t66044126 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_m_monster_id_8();
		return L_0;
	}
}
// Diver Diver::Alloc()
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisDiver_t66044126_m1188232782_MethodInfo_var;
extern const uint32_t Diver_Alloc_m547669845_MetadataUsageId;
extern "C"  Diver_t66044126 * Diver_Alloc_m547669845 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Diver_Alloc_m547669845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Diver_t66044126 * V_1 = NULL;
	{
		GameObject_t4012695102 * L_0 = (GameObject_t4012695102 *)il2cpp_codegen_object_new(GameObject_t4012695102_il2cpp_TypeInfo_var);
		GameObject__ctor_m845034556(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t4012695102 * L_1 = V_0;
		NullCheck(L_1);
		Diver_t66044126 * L_2 = GameObject_AddComponent_TisDiver_t66044126_m1188232782(L_1, /*hidden argument*/GameObject_AddComponent_TisDiver_t66044126_m1188232782_MethodInfo_var);
		V_1 = L_2;
		Diver_t66044126 * L_3 = V_1;
		return L_3;
	}
}
// System.Void Diver::Awake()
extern "C"  void Diver_Awake_m462926768 (Diver_t66044126 * __this, const MethodInfo* method)
{
	{
		__this->set_m_isMonster_7((bool)0);
		Character_Awake_m328345061(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Diver::Update()
extern "C"  void Diver_Update_m121895520 (Diver_t66044126 * __this, const MethodInfo* method)
{
	{
		Character_Update_m244829899(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Diver::Init(System.Single)
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisEffectWaveSystem_t2871388953_m1215556537_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral66044126;
extern const uint32_t Diver_Init_m156627620_MetadataUsageId;
extern "C"  void Diver_Init_m156627620 (Diver_t66044126 * __this, float ___size0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Diver_Init_m156627620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EffectWaveSystem_t2871388953 * V_0 = NULL;
	{
		Object_set_name_m1123518500(__this, _stringLiteral66044126, /*hidden argument*/NULL);
		float L_0 = ___size0;
		__this->set_m_size_6(L_0);
		GamePlay_t2590336614 * L_1 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_1);
		DataChapter_t925677859 * L_2 = GamePlay_get_dataChapter_m1400870973(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = L_2->get_diver_8();
		Object_t3878351788 * L_4 = Resources_Load_m2187391845(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		Object_t3878351788 * L_5 = Object_Instantiate_m3040600263(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->set_m_diver_5(((GameObject_t4012695102 *)IsInstSealed(L_5, GameObject_t4012695102_il2cpp_TypeInfo_var)));
		GameObject_t4012695102 * L_6 = __this->get_m_diver_5();
		NullCheck(L_6);
		Transform_t284553113 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_t284553113 * L_9 = GameObject_get_transform_m1278640159(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_set_parent_m3231272063(L_7, L_9, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_10 = __this->get_m_diver_5();
		NullCheck(L_10);
		Transform_t284553113 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		Vector3_t3525329789  L_12 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_13 = ___size0;
		Vector3_t3525329789  L_14 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m310756934(L_11, L_14, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_15 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		EffectWaveSystem_t2871388953 * L_16 = GameObject_AddComponent_TisEffectWaveSystem_t2871388953_m1215556537(L_15, /*hidden argument*/GameObject_AddComponent_TisEffectWaveSystem_t2871388953_m1215556537_MethodInfo_var);
		V_0 = L_16;
		EffectWaveSystem_t2871388953 * L_17 = V_0;
		float L_18 = ___size0;
		NullCheck(L_17);
		EffectWaveSystem_Init_m3117736862(L_17, (1.1f), L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Diver::SetMonster(System.UInt32)
extern "C"  void Diver_SetMonster_m2845158763 (Diver_t66044126 * __this, uint32_t ___monster_id0, const MethodInfo* method)
{
	{
		__this->set_m_isMonster_7((bool)1);
		uint32_t L_0 = ___monster_id0;
		__this->set_m_monster_id_8(L_0);
		return;
	}
}
// System.Void Diver::OnEndMove()
extern "C"  void Diver_OnEndMove_m4170788760 (Diver_t66044126 * __this, const MethodInfo* method)
{
	{
		Pattern_t873562992 * L_0 = ((Character_t3568163593 *)__this)->get_m_pattern_4();
		NullCheck(L_0);
		Pattern_set_success_m3994517526(L_0, (bool)0, /*hidden argument*/NULL);
		Pattern_t873562992 * L_1 = ((Character_t3568163593 *)__this)->get_m_pattern_4();
		NullCheck(L_1);
		Pattern_RemoveDiver_m455503873(L_1, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Diver::UpdateMove(UnityEngine.Vector3)
extern "C"  void Diver_UpdateMove_m2930291112 (Diver_t66044126 * __this, Vector3_t3525329789  ___delta0, const MethodInfo* method)
{
	{
		Vector3_t3525329789  L_0 = ___delta0;
		Character_UpdateMove_m3102545117(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Diver::Hit(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Diver_Hit_m1917533498 (Diver_t66044126 * __this, Vector3_t3525329789  ___point0, Vector3_t3525329789  ___collider1, const MethodInfo* method)
{
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t1525428817  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		float L_0 = (&___point0)->get_x_1();
		float L_1 = (&___collider1)->get_x_1();
		float L_2 = (&___point0)->get_z_3();
		float L_3 = (&___collider1)->get_z_3();
		float L_4 = (&___collider1)->get_x_1();
		float L_5 = (&___collider1)->get_z_3();
		Rect__ctor_m3291325233((&V_0), ((float)((float)L_0-(float)((float)((float)L_1/(float)(2.0f))))), ((float)((float)L_2-(float)((float)((float)L_3/(float)(2.0f))))), L_4, L_5, /*hidden argument*/NULL);
		Transform_t284553113 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t3525329789  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		float L_8 = (&V_2)->get_x_1();
		float L_9 = __this->get_m_size_6();
		Transform_t284553113 * L_10 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		float L_12 = (&V_3)->get_z_3();
		float L_13 = __this->get_m_size_6();
		float L_14 = __this->get_m_size_6();
		float L_15 = __this->get_m_size_6();
		Rect__ctor_m3291325233((&V_1), ((float)((float)L_8-(float)((float)((float)L_9/(float)(2.0f))))), ((float)((float)L_12-(float)((float)((float)L_13/(float)(2.0f))))), L_14, L_15, /*hidden argument*/NULL);
		Rect_t1525428817  L_16 = V_1;
		bool L_17 = Rect_Overlaps_m669681106((&V_0), L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00a1;
		}
	}
	{
		return (bool)1;
	}

IL_00a1:
	{
		return (bool)0;
	}
}
// System.Void DownloadTexture::.ctor()
extern Il2CppCodeGenString* _stringLiteral3620927203;
extern const uint32_t DownloadTexture__ctor_m1619239288_MetadataUsageId;
extern "C"  void DownloadTexture__ctor_m1619239288 (DownloadTexture_t2275226451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadTexture__ctor_m1619239288_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_url_2(_stringLiteral3620927203);
		__this->set_pixelPerfect_3((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator DownloadTexture::Start()
extern Il2CppClass* U3CStartU3Ec__Iterator0_t4138896567_il2cpp_TypeInfo_var;
extern const uint32_t DownloadTexture_Start_m4283064512_MetadataUsageId;
extern "C"  Il2CppObject * DownloadTexture_Start_m4283064512 (DownloadTexture_t2275226451 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DownloadTexture_Start_m4283064512_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__Iterator0_t4138896567 * V_0 = NULL;
	{
		U3CStartU3Ec__Iterator0_t4138896567 * L_0 = (U3CStartU3Ec__Iterator0_t4138896567 *)il2cpp_codegen_object_new(U3CStartU3Ec__Iterator0_t4138896567_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m285235664(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t4138896567 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_4(__this);
		U3CStartU3Ec__Iterator0_t4138896567 * L_2 = V_0;
		return L_2;
	}
}
// System.Void DownloadTexture::OnDestroy()
extern "C"  void DownloadTexture_OnDestroy_m3661101681 (DownloadTexture_t2275226451 * __this, const MethodInfo* method)
{
	{
		Texture2D_t2509538522 * L_0 = __this->get_mTex_4();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Texture2D_t2509538522 * L_2 = __this->get_mTex_4();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void DownloadTexture/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m285235664 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object DownloadTexture/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3620367874 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Object DownloadTexture/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3947672982 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_3();
		return L_0;
	}
}
// System.Boolean DownloadTexture/<Start>c__Iterator0::MoveNext()
extern Il2CppClass* WWW_t1522972100_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUITexture_t3903132647_m1338778738_MethodInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_MoveNext_m2313841700_MetadataUsageId;
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2313841700 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_MoveNext_m2313841700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_2();
		V_0 = L_0;
		__this->set_U24PC_2((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004f;
		}
	}
	{
		goto IL_00cf;
	}

IL_0021:
	{
		DownloadTexture_t2275226451 * L_2 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_url_2();
		WWW_t1522972100 * L_4 = (WWW_t1522972100 *)il2cpp_codegen_object_new(WWW_t1522972100_il2cpp_TypeInfo_var);
		WWW__ctor_m1985874080(L_4, L_3, /*hidden argument*/NULL);
		__this->set_U3CwwwU3E__0_0(L_4);
		WWW_t1522972100 * L_5 = __this->get_U3CwwwU3E__0_0();
		__this->set_U24current_3(L_5);
		__this->set_U24PC_2(1);
		goto IL_00d1;
	}

IL_004f:
	{
		DownloadTexture_t2275226451 * L_6 = __this->get_U3CU3Ef__this_4();
		WWW_t1522972100 * L_7 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_7);
		Texture2D_t2509538522 * L_8 = WWW_get_texture_m2854732303(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		L_6->set_mTex_4(L_8);
		DownloadTexture_t2275226451 * L_9 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_9);
		Texture2D_t2509538522 * L_10 = L_9->get_mTex_4();
		bool L_11 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_10, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00bd;
		}
	}
	{
		DownloadTexture_t2275226451 * L_12 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_12);
		UITexture_t3903132647 * L_13 = Component_GetComponent_TisUITexture_t3903132647_m1338778738(L_12, /*hidden argument*/Component_GetComponent_TisUITexture_t3903132647_m1338778738_MethodInfo_var);
		__this->set_U3CutU3E__1_1(L_13);
		UITexture_t3903132647 * L_14 = __this->get_U3CutU3E__1_1();
		DownloadTexture_t2275226451 * L_15 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_15);
		Texture2D_t2509538522 * L_16 = L_15->get_mTex_4();
		NullCheck(L_14);
		VirtActionInvoker1< Texture_t1769722184 * >::Invoke(28 /* System.Void UITexture::set_mainTexture(UnityEngine.Texture) */, L_14, L_16);
		DownloadTexture_t2275226451 * L_17 = __this->get_U3CU3Ef__this_4();
		NullCheck(L_17);
		bool L_18 = L_17->get_pixelPerfect_3();
		if (!L_18)
		{
			goto IL_00bd;
		}
	}
	{
		UITexture_t3903132647 * L_19 = __this->get_U3CutU3E__1_1();
		NullCheck(L_19);
		VirtActionInvoker0::Invoke(33 /* System.Void UITexture::MakePixelPerfect() */, L_19);
	}

IL_00bd:
	{
		WWW_t1522972100 * L_20 = __this->get_U3CwwwU3E__0_0();
		NullCheck(L_20);
		WWW_Dispose_m2446678367(L_20, /*hidden argument*/NULL);
		__this->set_U24PC_2((-1));
	}

IL_00cf:
	{
		return (bool)0;
	}

IL_00d1:
	{
		return (bool)1;
	}
	// Dead block : IL_00d3: ldloc.1
}
// System.Void DownloadTexture/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3426816525 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_2((-1));
		return;
	}
}
// System.Void DownloadTexture/<Start>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CStartU3Ec__Iterator0_Reset_m2226635901_MetadataUsageId;
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2226635901 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ec__Iterator0_Reset_m2226635901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EffectAnimation::.ctor()
extern "C"  void EffectAnimation__ctor_m1426528248 (EffectAnimation_t1201827027 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectAnimation::Awake()
extern const MethodInfo* Component_GetComponentsInChildren_TisAnimation_t350396337_m1284719643_MethodInfo_var;
extern const uint32_t EffectAnimation_Awake_m1664133467_MetadataUsageId;
extern "C"  void EffectAnimation_Awake_m1664133467 (EffectAnimation_t1201827027 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectAnimation_Awake_m1664133467_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AnimationU5BU5D_t2512821868* L_0 = Component_GetComponentsInChildren_TisAnimation_t350396337_m1284719643(__this, /*hidden argument*/Component_GetComponentsInChildren_TisAnimation_t350396337_m1284719643_MethodInfo_var);
		__this->set_m_animations_2(L_0);
		return;
	}
}
// System.Void EffectAnimation::Update()
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t EffectAnimation_Update_m2999564821_MetadataUsageId;
extern "C"  void EffectAnimation_Update_m2999564821 (EffectAnimation_t1201827027 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectAnimation_Update_m2999564821_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = EffectAnimation_get_isPlaying_m2743687717(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		GamePlay_t2590336614 * L_1 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_1);
		Stage_t80204510 * L_2 = GamePlay_get_stage_m3096959283(L_1, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Stage_RemoveEffect_m557265552(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Boolean EffectAnimation::get_isPlaying()
extern "C"  bool EffectAnimation_get_isPlaying_m2743687717 (EffectAnimation_t1201827027 * __this, const MethodInfo* method)
{
	Animation_t350396337 * V_0 = NULL;
	AnimationU5BU5D_t2512821868* V_1 = NULL;
	int32_t V_2 = 0;
	{
		AnimationU5BU5D_t2512821868* L_0 = __this->get_m_animations_2();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		AnimationU5BU5D_t2512821868* L_1 = __this->get_m_animations_2();
		V_1 = L_1;
		V_2 = 0;
		goto IL_0030;
	}

IL_001b:
	{
		AnimationU5BU5D_t2512821868* L_2 = V_1;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		V_0 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)));
		Animation_t350396337 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6 = Animation_get_isPlaying_m3295833780(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_002c;
		}
	}
	{
		return (bool)1;
	}

IL_002c:
	{
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_8 = V_2;
		AnimationU5BU5D_t2512821868* L_9 = V_1;
		NullCheck(L_9);
		if ((((int32_t)L_8) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void EffectCircle::.ctor()
extern "C"  void EffectCircle__ctor_m2828892634 (EffectCircle_t3606873569 * __this, const MethodInfo* method)
{
	{
		__this->set_range_2((0.03f));
		__this->set_speed1_3((2.1f));
		__this->set_speed2_4((-3.1f));
		__this->set_speed3_5((4.1f));
		__this->set_amplitude1_6((1.0f));
		__this->set_amplitude2_7((0.2f));
		__this->set_amplitude3_8((0.3f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectCircle::Awake()
extern const MethodInfo* Component_GetComponent_TisRenderer_t1092684080_m500377675_MethodInfo_var;
extern const uint32_t EffectCircle_Awake_m3066497853_MetadataUsageId;
extern "C"  void EffectCircle_Awake_m3066497853 (EffectCircle_t3606873569 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCircle_Awake_m3066497853_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_amplitude1_6((0.0f));
		__this->set_amplitude2_7((2.09439516f));
		__this->set_amplitude3_8((4.18879032f));
		Renderer_t1092684080 * L_0 = Component_GetComponent_TisRenderer_t1092684080_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t1092684080_m500377675_MethodInfo_var);
		__this->set_m_renderer_9(L_0);
		return;
	}
}
// System.Void EffectCircle::Update()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral558922319;
extern Il2CppCodeGenString* _stringLiteral146722755;
extern Il2CppCodeGenString* _stringLiteral146722756;
extern const uint32_t EffectCircle_Update_m3523187827_MetadataUsageId;
extern "C"  void EffectCircle_Update_m3523187827 (EffectCircle_t3606873569 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCircle_Update_m3523187827_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		Renderer_t1092684080 * L_0 = __this->get_m_renderer_9();
		NullCheck(L_0);
		Material_t1886596500 * L_1 = Renderer_get_material_m2720864603(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		bool L_2 = Material_HasProperty_m2077312757(L_1, _stringLiteral558922319, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_007c;
		}
	}
	{
		float L_3 = __this->get_amplitude1_6();
		float L_4 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_speed1_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_6 = sinf(((float)((float)L_3+(float)((float)((float)L_4*(float)L_5)))));
		float L_7 = __this->get_range_2();
		V_0 = ((float)((float)L_6*(float)L_7));
		float L_8 = __this->get_amplitude1_6();
		float L_9 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = __this->get_speed1_3();
		float L_11 = cosf(((float)((float)L_8+(float)((float)((float)L_9*(float)L_10)))));
		float L_12 = __this->get_range_2();
		V_1 = ((float)((float)((float)((float)L_11*(float)L_12))/(float)(2.0f)));
		Renderer_t1092684080 * L_13 = __this->get_m_renderer_9();
		NullCheck(L_13);
		Material_t1886596500 * L_14 = Renderer_get_material_m2720864603(L_13, /*hidden argument*/NULL);
		float L_15 = V_0;
		float L_16 = V_1;
		Vector2_t3525329788  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector2__ctor_m1517109030(&L_17, L_15, L_16, /*hidden argument*/NULL);
		NullCheck(L_14);
		Material_SetTextureOffset_m1301408396(L_14, _stringLiteral558922319, L_17, /*hidden argument*/NULL);
	}

IL_007c:
	{
		Renderer_t1092684080 * L_18 = __this->get_m_renderer_9();
		NullCheck(L_18);
		Material_t1886596500 * L_19 = Renderer_get_material_m2720864603(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		bool L_20 = Material_HasProperty_m2077312757(L_19, _stringLiteral146722755, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00f8;
		}
	}
	{
		float L_21 = __this->get_amplitude2_7();
		float L_22 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_23 = __this->get_speed2_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_24 = sinf(((float)((float)L_21+(float)((float)((float)L_22*(float)L_23)))));
		float L_25 = __this->get_range_2();
		V_0 = ((float)((float)L_24*(float)L_25));
		float L_26 = __this->get_amplitude2_7();
		float L_27 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_28 = __this->get_speed2_4();
		float L_29 = cosf(((float)((float)L_26+(float)((float)((float)L_27*(float)L_28)))));
		float L_30 = __this->get_range_2();
		V_1 = ((float)((float)((float)((float)L_29*(float)L_30))/(float)(2.0f)));
		Renderer_t1092684080 * L_31 = __this->get_m_renderer_9();
		NullCheck(L_31);
		Material_t1886596500 * L_32 = Renderer_get_material_m2720864603(L_31, /*hidden argument*/NULL);
		float L_33 = V_0;
		float L_34 = V_1;
		Vector2_t3525329788  L_35;
		memset(&L_35, 0, sizeof(L_35));
		Vector2__ctor_m1517109030(&L_35, L_33, L_34, /*hidden argument*/NULL);
		NullCheck(L_32);
		Material_SetTextureOffset_m1301408396(L_32, _stringLiteral146722755, L_35, /*hidden argument*/NULL);
	}

IL_00f8:
	{
		Renderer_t1092684080 * L_36 = __this->get_m_renderer_9();
		NullCheck(L_36);
		Material_t1886596500 * L_37 = Renderer_get_material_m2720864603(L_36, /*hidden argument*/NULL);
		NullCheck(L_37);
		bool L_38 = Material_HasProperty_m2077312757(L_37, _stringLiteral146722756, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_0174;
		}
	}
	{
		float L_39 = __this->get_amplitude3_8();
		float L_40 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_41 = __this->get_speed3_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_42 = sinf(((float)((float)L_39+(float)((float)((float)L_40*(float)L_41)))));
		float L_43 = __this->get_range_2();
		V_0 = ((float)((float)L_42*(float)L_43));
		float L_44 = __this->get_amplitude3_8();
		float L_45 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_46 = __this->get_speed3_5();
		float L_47 = cosf(((float)((float)L_44+(float)((float)((float)L_45*(float)L_46)))));
		float L_48 = __this->get_range_2();
		V_1 = ((float)((float)((float)((float)L_47*(float)L_48))/(float)(2.0f)));
		Renderer_t1092684080 * L_49 = __this->get_m_renderer_9();
		NullCheck(L_49);
		Material_t1886596500 * L_50 = Renderer_get_material_m2720864603(L_49, /*hidden argument*/NULL);
		float L_51 = V_0;
		float L_52 = V_1;
		Vector2_t3525329788  L_53;
		memset(&L_53, 0, sizeof(L_53));
		Vector2__ctor_m1517109030(&L_53, L_51, L_52, /*hidden argument*/NULL);
		NullCheck(L_50);
		Material_SetTextureOffset_m1301408396(L_50, _stringLiteral146722756, L_53, /*hidden argument*/NULL);
	}

IL_0174:
	{
		return;
	}
}
// System.Void EffectCoin::.ctor()
extern "C"  void EffectCoin__ctor_m1662635769 (EffectCoin_t3708782562 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectCoin::Awake()
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2106033;
extern Il2CppCodeGenString* _stringLiteral940838271;
extern const uint32_t EffectCoin_Awake_m1900240988_MetadataUsageId;
extern "C"  void EffectCoin_Awake_m1900240988 (EffectCoin_t3708782562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCoin_Awake_m1900240988_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Transform_t284553113 * V_1 = NULL;
	{
		Object_set_name_m1123518500(__this, _stringLiteral2106033, /*hidden argument*/NULL);
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set_m_tr_4(L_0);
		Object_t3878351788 * L_1 = Resources_Load_m2187391845(NULL /*static, unused*/, _stringLiteral940838271, /*hidden argument*/NULL);
		Object_t3878351788 * L_2 = Object_Instantiate_m3040600263(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)IsInstSealed(L_2, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t284553113 * L_4 = GameObject_GetComponent_TisTransform_t284553113_m3795369772(L_3, /*hidden argument*/GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var);
		V_1 = L_4;
		Transform_t284553113 * L_5 = V_1;
		Transform_t284553113 * L_6 = __this->get_m_tr_4();
		NullCheck(L_5);
		Transform_set_parent_m3231272063(L_5, L_6, /*hidden argument*/NULL);
		Transform_t284553113 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		Vector3_t3525329789  L_8 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_SCALE_COIN_8();
		NullCheck(L_7);
		Transform_set_localScale_m310756934(L_7, L_8, /*hidden argument*/NULL);
		Transform_t284553113 * L_9 = V_1;
		Transform_t284553113 * L_10 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_position_m2211398607(L_10, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_12 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_12);
		Camera_t3533968274 * L_13 = L_12->get_camera_3();
		NullCheck(L_13);
		Transform_t284553113 * L_14 = Component_get_transform_m4257140443(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		Quaternion_t1891715979  L_15 = Transform_get_rotation_m11483428(L_14, /*hidden argument*/NULL);
		Vector3_t3525329789  L_16 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_17 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_15, L_16, /*hidden argument*/NULL);
		Vector3_t3525329789  L_18 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_11, L_17, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_19 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_19);
		Camera_t3533968274 * L_20 = L_19->get_camera_3();
		NullCheck(L_20);
		Transform_t284553113 * L_21 = Component_get_transform_m4257140443(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		Quaternion_t1891715979  L_22 = Transform_get_rotation_m11483428(L_21, /*hidden argument*/NULL);
		Vector3_t3525329789  L_23 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_24 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_22, L_23, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_LookAt_m2054691043(L_9, L_18, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectCoin::Update()
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t EffectCoin_Update_m1728963380_MetadataUsageId;
extern "C"  void EffectCoin_Update_m1728963380 (EffectCoin_t3708782562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCoin_Update_m1728963380_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = __this->get_m_time_6();
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0027;
		}
	}
	{
		float L_1 = __this->get_m_time_6();
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_m_time_6(((float)((float)L_1-(float)L_2)));
		goto IL_0099;
	}

IL_0027:
	{
		Transform_t284553113 * L_3 = __this->get_m_tr_4();
		Vector3_t3525329789  L_4 = __this->get_m_velocity_2();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_Translate_m2849099360(L_3, L_6, /*hidden argument*/NULL);
		Transform_t284553113 * L_7 = __this->get_m_tr_4();
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = (&V_0)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_10 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_POS_COIN_Y_7();
		if ((!(((float)L_9) <= ((float)L_10))))
		{
			goto IL_007b;
		}
	}
	{
		EffectCoin_OnCollisionWater_m2532732911(__this, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_11 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_11);
		Stage_t80204510 * L_12 = GamePlay_get_stage_m3096959283(L_11, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_13 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Stage_RemoveEffect_m557265552(L_12, L_13, /*hidden argument*/NULL);
	}

IL_007b:
	{
		Vector3_t3525329789 * L_14 = __this->get_address_of_m_velocity_2();
		Vector3_t3525329789 * L_15 = L_14;
		float L_16 = L_15->get_y_2();
		float L_17 = __this->get_m_gravity_3();
		float L_18 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_15->set_y_2(((float)((float)L_16-(float)((float)((float)L_17*(float)L_18)))));
	}

IL_0099:
	{
		return;
	}
}
// System.Void EffectCoin::InitCoin(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern const uint32_t EffectCoin_InitCoin_m1372446272_MetadataUsageId;
extern "C"  void EffectCoin_InitCoin_m1372446272 (EffectCoin_t3708782562 * __this, int32_t ___coin0, float ___x1, float ___z2, float ___degree3, float ___power_h4, float ___power_v5, float ___gravity6, float ___delay7, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCoin_InitCoin_m1372446272_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_0));
		float L_0 = ___x1;
		(&V_0)->set_x_1(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_1 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_POS_COIN_Y_7();
		(&V_0)->set_y_2(L_1);
		float L_2 = ___z2;
		(&V_0)->set_z_3(L_2);
		int32_t L_3 = ___coin0;
		Vector3_t3525329789  L_4 = V_0;
		float L_5 = ___degree3;
		float L_6 = ___power_h4;
		float L_7 = ___power_v5;
		float L_8 = ___gravity6;
		float L_9 = ___delay7;
		EffectCoin_InitCoin_m613784841(__this, L_3, L_4, L_5, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectCoin::InitCoin(System.Int32,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t EffectCoin_InitCoin_m613784841_MetadataUsageId;
extern "C"  void EffectCoin_InitCoin_m613784841 (EffectCoin_t3708782562 * __this, int32_t ___coin0, Vector3_t3525329789  ___position1, float ___degree2, float ___power_h3, float ___power_v4, float ___gravity5, float ___delay6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCoin_InitCoin_m613784841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___delay6;
		__this->set_m_time_6(L_0);
		int32_t L_1 = ___coin0;
		__this->set_m_coin_5(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_2 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_POS_COIN_Y_7();
		(&___position1)->set_y_2(L_2);
		Transform_t284553113 * L_3 = __this->get_m_tr_4();
		Vector3_t3525329789  L_4 = ___position1;
		NullCheck(L_3);
		Transform_set_position_m3111394108(L_3, L_4, /*hidden argument*/NULL);
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t3525329789  L_5 = V_0;
		__this->set_m_velocity_2(L_5);
		Vector3_t3525329789 * L_6 = __this->get_address_of_m_velocity_2();
		float L_7 = ___power_h3;
		float L_8 = ___degree2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_9 = cosf(L_8);
		L_6->set_x_1(((float)((float)L_7*(float)L_9)));
		Vector3_t3525329789 * L_10 = __this->get_address_of_m_velocity_2();
		float L_11 = ___power_v4;
		L_10->set_y_2(L_11);
		Vector3_t3525329789 * L_12 = __this->get_address_of_m_velocity_2();
		float L_13 = ___power_h3;
		float L_14 = ___degree2;
		float L_15 = sinf(L_14);
		L_12->set_z_3(((float)((float)L_13*(float)L_15)));
		float L_16 = ___gravity5;
		__this->set_m_gravity_3(L_16);
		return;
	}
}
// System.Void EffectCoin::OnCollisionWater()
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139_MethodInfo_var;
extern const uint32_t EffectCoin_OnCollisionWater_m2532732911_MetadataUsageId;
extern "C"  void EffectCoin_OnCollisionWater_m2532732911 (EffectCoin_t3708782562 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCoin_OnCollisionWater_m2532732911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GameObject_t4012695102 * V_1 = NULL;
	EffectWave_t3709365322 * V_2 = NULL;
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Transform_t284553113 * L_0 = __this->get_m_tr_4();
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		V_3 = L_1;
		float L_2 = (&V_3)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_3 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_POS_SURFACE_EFFECT_Y_0();
		Transform_t284553113 * L_4 = __this->get_m_tr_4();
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		V_4 = L_5;
		float L_6 = (&V_4)->get_z_3();
		Vector3__ctor_m2926210380((&V_0), L_2, L_3, L_6, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_7 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_7);
		Stage_t80204510 * L_8 = GamePlay_get_stage_m3096959283(L_7, /*hidden argument*/NULL);
		Vector3_t3525329789  L_9 = V_0;
		GamePlay_t2590336614 * L_10 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_10);
		DataChapter_t925677859 * L_11 = GamePlay_get_dataChapter_m1400870973(L_10, /*hidden argument*/NULL);
		NullCheck(L_11);
		String_t* L_12 = L_11->get_effect_wave_6();
		NullCheck(L_8);
		GameObject_t4012695102 * L_13 = Stage_AddEffect_m3322959738(L_8, L_9, L_12, (bool)0, /*hidden argument*/NULL);
		V_1 = L_13;
		GameObject_t4012695102 * L_14 = V_1;
		NullCheck(L_14);
		EffectWave_t3709365322 * L_15 = GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139(L_14, /*hidden argument*/GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139_MethodInfo_var);
		V_2 = L_15;
		EffectWave_t3709365322 * L_16 = V_2;
		bool L_17 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_16, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0087;
		}
	}
	{
		EffectWave_t3709365322 * L_18 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_19 = (((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_address_of_SCALE_COIN_8())->get_x_1();
		float L_20 = (((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_address_of_SCALE_COIN_8())->get_x_1();
		NullCheck(L_18);
		EffectWave_Init_m328236749(L_18, L_19, ((float)((float)L_20*(float)(2.0f))), /*hidden argument*/NULL);
	}

IL_0087:
	{
		GamePlay_t2590336614 * L_21 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_21);
		Stage_t80204510 * L_22 = GamePlay_get_stage_m3096959283(L_21, /*hidden argument*/NULL);
		int32_t L_23 = __this->get_m_coin_5();
		Transform_t284553113 * L_24 = __this->get_m_tr_4();
		NullCheck(L_24);
		Vector3_t3525329789  L_25 = Transform_get_position_m2211398607(L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		Stage_AddCoinTextEffect_m2830881887(L_22, L_23, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectCoinText::.ctor()
extern "C"  void EffectCoinText__ctor_m2635403596 (EffectCoinText_t948731951 * __this, const MethodInfo* method)
{
	{
		__this->set_m_moveSpeed_8((5.0f));
		__this->set_m_fadeSpeed_9((8.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectCoinText::Awake()
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTextMesh_t583678247_m1543870284_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t1092684080_m4102086307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3509975388;
extern Il2CppCodeGenString* _stringLiteral3429560140;
extern const uint32_t EffectCoinText_Awake_m2873008815_MetadataUsageId;
extern "C"  void EffectCoinText_Awake_m2873008815 (EffectCoinText_t948731951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCoinText_Awake_m2873008815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	Transform_t284553113 * V_1 = NULL;
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set_m_tr_4(L_0);
		Object_set_name_m1123518500(__this, _stringLiteral3509975388, /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Resources_Load_m2187391845(NULL /*static, unused*/, _stringLiteral3429560140, /*hidden argument*/NULL);
		Object_t3878351788 * L_2 = Object_Instantiate_m3040600263(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = ((GameObject_t4012695102 *)IsInstSealed(L_2, GameObject_t4012695102_il2cpp_TypeInfo_var));
		GameObject_t4012695102 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t284553113 * L_4 = GameObject_GetComponent_TisTransform_t284553113_m3795369772(L_3, /*hidden argument*/GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var);
		V_1 = L_4;
		Transform_t284553113 * L_5 = V_1;
		Transform_t284553113 * L_6 = __this->get_m_tr_4();
		NullCheck(L_5);
		Transform_set_parent_m3231272063(L_5, L_6, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = V_0;
		NullCheck(L_7);
		TextMesh_t583678247 * L_8 = GameObject_GetComponent_TisTextMesh_t583678247_m1543870284(L_7, /*hidden argument*/GameObject_GetComponent_TisTextMesh_t583678247_m1543870284_MethodInfo_var);
		__this->set_m_textMesh_5(L_8);
		GameObject_t4012695102 * L_9 = V_0;
		NullCheck(L_9);
		Renderer_t1092684080 * L_10 = GameObject_GetComponent_TisRenderer_t1092684080_m4102086307(L_9, /*hidden argument*/GameObject_GetComponent_TisRenderer_t1092684080_m4102086307_MethodInfo_var);
		__this->set_m_textRenderer_6(L_10);
		Transform_t284553113 * L_11 = V_1;
		Transform_t284553113 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t3525329789  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_14 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_14);
		Camera_t3533968274 * L_15 = L_14->get_camera_3();
		NullCheck(L_15);
		Transform_t284553113 * L_16 = Component_get_transform_m4257140443(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		Quaternion_t1891715979  L_17 = Transform_get_rotation_m11483428(L_16, /*hidden argument*/NULL);
		Vector3_t3525329789  L_18 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_19 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		Vector3_t3525329789  L_20 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_13, L_19, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_21 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_21);
		Camera_t3533968274 * L_22 = L_21->get_camera_3();
		NullCheck(L_22);
		Transform_t284553113 * L_23 = Component_get_transform_m4257140443(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		Quaternion_t1891715979  L_24 = Transform_get_rotation_m11483428(L_23, /*hidden argument*/NULL);
		Vector3_t3525329789  L_25 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_26 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_LookAt_m2054691043(L_11, L_20, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectCoinText::InitCoin(System.Int32,UnityEngine.Vector3)
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern const uint32_t EffectCoinText_InitCoin_m2759943901_MetadataUsageId;
extern "C"  void EffectCoinText_InitCoin_m2759943901 (EffectCoinText_t948731951 * __this, int32_t ___coin0, Vector3_t3525329789  ___position1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCoinText_InitCoin_m2759943901_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextMesh_t583678247 * L_0 = __this->get_m_textMesh_5();
		String_t* L_1 = Int32_ToString_m1286526384((&___coin0), /*hidden argument*/NULL);
		NullCheck(L_0);
		TextMesh_set_text_m3628430759(L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_2 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_POS_COIN_Y_7();
		(&___position1)->set_y_2(L_2);
		Transform_t284553113 * L_3 = __this->get_m_tr_4();
		Vector3_t3525329789  L_4 = ___position1;
		NullCheck(L_3);
		Transform_set_position_m3111394108(L_3, L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_5 = ___position1;
		__this->set_m_targetPos_2(L_5);
		Vector3_t3525329789 * L_6 = __this->get_address_of_m_targetPos_2();
		L_6->set_y_2((1.0f));
		Renderer_t1092684080 * L_7 = __this->get_m_textRenderer_6();
		NullCheck(L_7);
		Material_t1886596500 * L_8 = Renderer_get_material_m2720864603(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		Color_t1588175760  L_9 = Material_get_color_m2268945527(L_8, /*hidden argument*/NULL);
		__this->set_m_targetColor_3(L_9);
		Color_t1588175760 * L_10 = __this->get_address_of_m_targetColor_3();
		L_10->set_a_3((0.0f));
		return;
	}
}
// System.Void EffectCoinText::Update()
extern "C"  void EffectCoinText_Update_m1819994945 (EffectCoinText_t948731951 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_m_phase_7();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		EffectCoinText_UpdateMove_m3587588850(__this, /*hidden argument*/NULL);
		goto IL_001c;
	}

IL_0016:
	{
		EffectCoinText_UpdateDisappear_m2856886660(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void EffectCoinText::UpdateMove()
extern "C"  void EffectCoinText_UpdateMove_m3587588850 (EffectCoinText_t948731951 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = __this->get_m_tr_4();
		Transform_t284553113 * L_1 = __this->get_m_tr_4();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		Vector3_t3525329789  L_3 = __this->get_m_targetPos_2();
		float L_4 = __this->get_m_moveSpeed_8();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_2, L_3, ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_6, /*hidden argument*/NULL);
		Transform_t284553113 * L_7 = __this->get_m_tr_4();
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		float L_9 = (&V_0)->get_y_2();
		Vector3_t3525329789 * L_10 = __this->get_address_of_m_targetPos_2();
		float L_11 = L_10->get_y_2();
		if ((!(((float)((float)((float)L_9+(float)(0.1f)))) >= ((float)L_11))))
		{
			goto IL_005d;
		}
	}
	{
		__this->set_m_phase_7(1);
	}

IL_005d:
	{
		return;
	}
}
// System.Void EffectCoinText::UpdateDisappear()
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t EffectCoinText_UpdateDisappear_m2856886660_MetadataUsageId;
extern "C"  void EffectCoinText_UpdateDisappear_m2856886660 (EffectCoinText_t948731951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectCoinText_UpdateDisappear_m2856886660_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Renderer_t1092684080 * L_0 = __this->get_m_textRenderer_6();
		NullCheck(L_0);
		Material_t1886596500 * L_1 = Renderer_get_material_m2720864603(L_0, /*hidden argument*/NULL);
		Renderer_t1092684080 * L_2 = __this->get_m_textRenderer_6();
		NullCheck(L_2);
		Material_t1886596500 * L_3 = Renderer_get_material_m2720864603(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Color_t1588175760  L_4 = Material_get_color_m2268945527(L_3, /*hidden argument*/NULL);
		Color_t1588175760  L_5 = __this->get_m_targetColor_3();
		float L_6 = __this->get_m_fadeSpeed_9();
		float L_7 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t1588175760  L_8 = Color_Lerp_m3494628845(NULL /*static, unused*/, L_4, L_5, ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		NullCheck(L_1);
		Material_set_color_m3296857020(L_1, L_8, /*hidden argument*/NULL);
		Renderer_t1092684080 * L_9 = __this->get_m_textRenderer_6();
		NullCheck(L_9);
		Material_t1886596500 * L_10 = Renderer_get_material_m2720864603(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Color_t1588175760  L_11 = Material_get_color_m2268945527(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		float L_12 = (&V_0)->get_a_3();
		if ((!(((float)L_12) < ((float)(0.01f)))))
		{
			goto IL_006f;
		}
	}
	{
		GamePlay_t2590336614 * L_13 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_13);
		Stage_t80204510 * L_14 = GamePlay_get_stage_m3096959283(L_13, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_15 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Stage_RemoveEffect_m557265552(L_14, L_15, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return;
	}
}
// System.Void EffectGod::.ctor()
extern "C"  void EffectGod__ctor_m2047690944 (EffectGod_t535283979 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectGod::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisAnimation_t350396337_m2209026056_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral489276651;
extern Il2CppCodeGenString* _stringLiteral489276652;
extern const uint32_t EffectGod_Awake_m2285296163_MetadataUsageId;
extern "C"  void EffectGod_Awake_m2285296163 (EffectGod_t535283979 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectGod_Awake_m2285296163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animation_t350396337 * L_0 = Component_GetComponentInChildren_TisAnimation_t350396337_m2209026056(__this, /*hidden argument*/Component_GetComponentInChildren_TisAnimation_t350396337_m2209026056_MethodInfo_var);
		__this->set_m_animation_2(L_0);
		int32_t L_1 = Random_Range_m75452833(NULL /*static, unused*/, 0, 2, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_002e;
		}
	}
	{
		Animation_t350396337 * L_2 = __this->get_m_animation_2();
		NullCheck(L_2);
		Animation_Play_m900498501(L_2, _stringLiteral489276651, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_002e:
	{
		Animation_t350396337 * L_3 = __this->get_m_animation_2();
		NullCheck(L_3);
		Animation_Play_m900498501(L_3, _stringLiteral489276652, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void EffectGod::Update()
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t EffectGod_Update_m780771917_MetadataUsageId;
extern "C"  void EffectGod_Update_m780771917 (EffectGod_t535283979 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectGod_Update_m780771917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Animation_t350396337 * L_0 = __this->get_m_animation_2();
		NullCheck(L_0);
		bool L_1 = Animation_get_isPlaying_m3295833780(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		GamePlay_t2590336614 * L_2 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_2);
		Stage_t80204510 * L_3 = GamePlay_get_stage_m3096959283(L_2, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Stage_RemoveEffect_m557265552(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void EffectParticle::.ctor()
extern "C"  void EffectParticle__ctor_m167438116 (EffectParticle_t2854106967 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectParticle::Awake()
extern const MethodInfo* Component_GetComponent_TisParticleSystem_t56787138_m423524409_MethodInfo_var;
extern const uint32_t EffectParticle_Awake_m405043335_MetadataUsageId;
extern "C"  void EffectParticle_Awake_m405043335 (EffectParticle_t2854106967 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectParticle_Awake_m405043335_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ParticleSystem_t56787138 * L_0 = Component_GetComponent_TisParticleSystem_t56787138_m423524409(__this, /*hidden argument*/Component_GetComponent_TisParticleSystem_t56787138_m423524409_MethodInfo_var);
		__this->set_m_system_2(L_0);
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		ParticleSystem_t56787138 * L_2 = __this->get_m_system_2();
		NullCheck(L_2);
		float L_3 = ParticleSystem_get_duration_m4044642069(L_2, /*hidden argument*/NULL);
		ParticleSystem_t56787138 * L_4 = __this->get_m_system_2();
		NullCheck(L_4);
		float L_5 = ParticleSystem_get_startLifetime_m4275191564(L_4, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_1, ((float)((float)L_3+(float)L_5)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectText::.ctor()
extern "C"  void EffectText__ctor_m1610344413 (EffectText_t3709279870 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectText::Start()
extern "C"  void EffectText_Start_m557482205 (EffectText_t3709279870 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void EffectText::Update()
extern "C"  void EffectText_Update_m107931344 (EffectText_t3709279870 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void EffectWave::.ctor()
extern "C"  void EffectWave__ctor_m2460442513 (EffectWave_t3709365322 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_7((2.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectWave::Awake()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisRenderer_t1092684080_m2983327791_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisTransform_t284553113_m3473594144_MethodInfo_var;
extern const uint32_t EffectWave_Awake_m2698047732_MetadataUsageId;
extern "C"  void EffectWave_Awake_m2698047732 (EffectWave_t3709365322 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectWave_Awake_m2698047732_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set_m_tr_2(L_0);
		Renderer_t1092684080 * L_1 = Component_GetComponentInChildren_TisRenderer_t1092684080_m2983327791(__this, /*hidden argument*/Component_GetComponentInChildren_TisRenderer_t1092684080_m2983327791_MethodInfo_var);
		__this->set_m_renderer_4(L_1);
		Transform_t284553113 * L_2 = Component_GetComponentInChildren_TisTransform_t284553113_m3473594144(__this, /*hidden argument*/Component_GetComponentInChildren_TisTransform_t284553113_m3473594144_MethodInfo_var);
		__this->set_m_wave_tr_3(L_2);
		return;
	}
}
// System.Void EffectWave::OnDestroy()
extern "C"  void EffectWave_OnDestroy_m3115123722 (EffectWave_t3709365322 * __this, const MethodInfo* method)
{
	{
		__this->set_m_tr_2((Transform_t284553113 *)NULL);
		__this->set_m_renderer_4((Renderer_t1092684080 *)NULL);
		return;
	}
}
// System.Void EffectWave::Init(System.Single,System.Single)
extern "C"  void EffectWave_Init_m328236749 (EffectWave_t3709365322 * __this, float ___scale0, float ___target_scale1, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = __this->get_m_tr_2();
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_localScale_m3886572677(L_0, /*hidden argument*/NULL);
		float L_2 = ___target_scale1;
		Vector3_t3525329789  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		__this->set_m_targetScale_6(L_3);
		Transform_t284553113 * L_4 = __this->get_m_tr_2();
		Transform_t284553113 * L_5 = __this->get_m_tr_2();
		NullCheck(L_5);
		Vector3_t3525329789  L_6 = Transform_get_localScale_m3886572677(L_5, /*hidden argument*/NULL);
		float L_7 = ___scale0;
		Vector3_t3525329789  L_8 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_set_localScale_m310756934(L_4, L_8, /*hidden argument*/NULL);
		Renderer_t1092684080 * L_9 = __this->get_m_renderer_4();
		NullCheck(L_9);
		Material_t1886596500 * L_10 = Renderer_get_material_m2720864603(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Color_t1588175760  L_11 = Material_get_color_m2268945527(L_10, /*hidden argument*/NULL);
		__this->set_m_targetColor_5(L_11);
		Color_t1588175760 * L_12 = __this->get_address_of_m_targetColor_5();
		L_12->set_a_3((0.0f));
		return;
	}
}
// System.Void EffectWave::Update()
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t EffectWave_Update_m691168668_MetadataUsageId;
extern "C"  void EffectWave_Update_m691168668 (EffectWave_t3709365322 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectWave_Update_m691168668_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = __this->get_m_tr_2();
		Transform_t284553113 * L_1 = __this->get_m_tr_2();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_localScale_m3886572677(L_1, /*hidden argument*/NULL);
		Vector3_t3525329789  L_3 = __this->get_m_targetScale_6();
		float L_4 = __this->get_speed_7();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_2, L_3, ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localScale_m310756934(L_0, L_6, /*hidden argument*/NULL);
		Renderer_t1092684080 * L_7 = __this->get_m_renderer_4();
		NullCheck(L_7);
		Material_t1886596500 * L_8 = Renderer_get_material_m2720864603(L_7, /*hidden argument*/NULL);
		Renderer_t1092684080 * L_9 = __this->get_m_renderer_4();
		NullCheck(L_9);
		Material_t1886596500 * L_10 = Renderer_get_material_m2720864603(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Color_t1588175760  L_11 = Material_get_color_m2268945527(L_10, /*hidden argument*/NULL);
		Color_t1588175760  L_12 = __this->get_m_targetColor_5();
		float L_13 = __this->get_speed_7();
		float L_14 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t1588175760  L_15 = Color_Lerp_m3494628845(NULL /*static, unused*/, L_11, L_12, ((float)((float)L_13*(float)L_14)), /*hidden argument*/NULL);
		NullCheck(L_8);
		Material_set_color_m3296857020(L_8, L_15, /*hidden argument*/NULL);
		Renderer_t1092684080 * L_16 = __this->get_m_renderer_4();
		NullCheck(L_16);
		Material_t1886596500 * L_17 = Renderer_get_material_m2720864603(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Color_t1588175760  L_18 = Material_get_color_m2268945527(L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		float L_19 = (&V_0)->get_a_3();
		if ((!(((float)L_19) < ((float)(0.01f)))))
		{
			goto IL_009c;
		}
	}
	{
		GamePlay_t2590336614 * L_20 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_20);
		Stage_t80204510 * L_21 = GamePlay_get_stage_m3096959283(L_20, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_22 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Stage_RemoveEffect_m557265552(L_21, L_22, /*hidden argument*/NULL);
	}

IL_009c:
	{
		return;
	}
}
// System.Void EffectWaveSystem::.ctor()
extern "C"  void EffectWaveSystem__ctor_m1020503970 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectWaveSystem::Awake()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const uint32_t EffectWaveSystem_Awake_m1258109189_MetadataUsageId;
extern "C"  void EffectWaveSystem_Awake_m1258109189 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectWaveSystem_Awake_m1258109189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set_m_tr_2(L_0);
		return;
	}
}
// System.Void EffectWaveSystem::OnDestroy()
extern "C"  void EffectWaveSystem_OnDestroy_m1701108635 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method)
{
	{
		__this->set_m_enabled_5((bool)0);
		return;
	}
}
// System.Void EffectWaveSystem::Init(System.Single,System.Single)
extern Il2CppCodeGenString* _stringLiteral516153114;
extern const uint32_t EffectWaveSystem_Init_m3117736862_MetadataUsageId;
extern "C"  void EffectWaveSystem_Init_m3117736862 (EffectWaveSystem_t2871388953 * __this, float ___time0, float ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectWaveSystem_Init_m3117736862_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___time0;
		__this->set_m_time_3(L_0);
		float L_1 = ___size1;
		__this->set_m_size_4(L_1);
		__this->set_m_enabled_5((bool)1);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral516153114, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EffectWaveSystem::SetSpeed(System.Single)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2191959;
extern const uint32_t EffectWaveSystem_SetSpeed_m3172009060_MetadataUsageId;
extern "C"  void EffectWaveSystem_SetSpeed_m3172009060 (EffectWaveSystem_t2871388953 * __this, float ___speed0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectWaveSystem_SetSpeed_m3172009060_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StopCoroutine_m2790918991(__this, _stringLiteral2191959, /*hidden argument*/NULL);
		float L_0 = ___speed0;
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(Single_t958209021_il2cpp_TypeInfo_var, &L_1);
		MonoBehaviour_StartCoroutine_m2964903975(__this, _stringLiteral2191959, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator EffectWaveSystem::AddWave()
extern Il2CppClass* U3CAddWaveU3Ec__Iterator5_t4079190500_il2cpp_TypeInfo_var;
extern const uint32_t EffectWaveSystem_AddWave_m775507570_MetadataUsageId;
extern "C"  Il2CppObject * EffectWaveSystem_AddWave_m775507570 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EffectWaveSystem_AddWave_m775507570_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CAddWaveU3Ec__Iterator5_t4079190500 * V_0 = NULL;
	{
		U3CAddWaveU3Ec__Iterator5_t4079190500 * L_0 = (U3CAddWaveU3Ec__Iterator5_t4079190500 *)il2cpp_codegen_object_new(U3CAddWaveU3Ec__Iterator5_t4079190500_il2cpp_TypeInfo_var);
		U3CAddWaveU3Ec__Iterator5__ctor_m604444909(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAddWaveU3Ec__Iterator5_t4079190500 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_5(__this);
		U3CAddWaveU3Ec__Iterator5_t4079190500 * L_2 = V_0;
		return L_2;
	}
}
// System.Void EffectWaveSystem::Update()
extern "C"  void EffectWaveSystem_Update_m3297714091 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void EffectWaveSystem/<AddWave>c__Iterator5::.ctor()
extern "C"  void U3CAddWaveU3Ec__Iterator5__ctor_m604444909 (U3CAddWaveU3Ec__Iterator5_t4079190500 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object EffectWaveSystem/<AddWave>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CAddWaveU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1476538191 (U3CAddWaveU3Ec__Iterator5_t4079190500 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Object EffectWaveSystem/<AddWave>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAddWaveU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m633516259 (U3CAddWaveU3Ec__Iterator5_t4079190500 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_4();
		return L_0;
	}
}
// System.Boolean EffectWaveSystem/<AddWave>c__Iterator5::MoveNext()
extern Il2CppClass* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139_MethodInfo_var;
extern const uint32_t U3CAddWaveU3Ec__Iterator5_MoveNext_m3553211087_MetadataUsageId;
extern "C"  bool U3CAddWaveU3Ec__Iterator5_MoveNext_m3553211087 (U3CAddWaveU3Ec__Iterator5_t4079190500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAddWaveU3Ec__Iterator5_MoveNext_m3553211087_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_3();
		V_0 = L_0;
		__this->set_U24PC_3((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_0041;
		}
		if (L_1 == 2)
		{
			goto IL_0102;
		}
	}
	{
		goto IL_0119;
	}

IL_0025:
	{
		WaitForSeconds_t1291133240 * L_2 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (0.1f), /*hidden argument*/NULL);
		__this->set_U24current_4(L_2);
		__this->set_U24PC_3(1);
		goto IL_011b;
	}

IL_0041:
	{
		goto IL_0102;
	}

IL_0046:
	{
		EffectWaveSystem_t2871388953 * L_3 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_3);
		Transform_t284553113 * L_4 = L_3->get_m_tr_2();
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		__this->set_U3CpositionU3E__0_0(L_5);
		Vector3_t3525329789 * L_6 = __this->get_address_of_U3CpositionU3E__0_0();
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_7 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_POS_SURFACE_EFFECT_Y_0();
		L_6->set_y_2(L_7);
		GamePlay_t2590336614 * L_8 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_8);
		Stage_t80204510 * L_9 = GamePlay_get_stage_m3096959283(L_8, /*hidden argument*/NULL);
		Vector3_t3525329789  L_10 = __this->get_U3CpositionU3E__0_0();
		GamePlay_t2590336614 * L_11 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_11);
		DataChapter_t925677859 * L_12 = GamePlay_get_dataChapter_m1400870973(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		String_t* L_13 = L_12->get_effect_wave_6();
		NullCheck(L_9);
		GameObject_t4012695102 * L_14 = Stage_AddEffect_m3322959738(L_9, L_10, L_13, (bool)0, /*hidden argument*/NULL);
		__this->set_U3CgameObjU3E__1_1(L_14);
		GameObject_t4012695102 * L_15 = __this->get_U3CgameObjU3E__1_1();
		NullCheck(L_15);
		EffectWave_t3709365322 * L_16 = GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139(L_15, /*hidden argument*/GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139_MethodInfo_var);
		__this->set_U3CeffectU3E__2_2(L_16);
		EffectWave_t3709365322 * L_17 = __this->get_U3CeffectU3E__2_2();
		bool L_18 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_17, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00e0;
		}
	}
	{
		EffectWave_t3709365322 * L_19 = __this->get_U3CeffectU3E__2_2();
		EffectWaveSystem_t2871388953 * L_20 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_20);
		float L_21 = L_20->get_m_size_4();
		EffectWaveSystem_t2871388953 * L_22 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_22);
		float L_23 = L_22->get_m_size_4();
		NullCheck(L_19);
		EffectWave_Init_m328236749(L_19, L_21, ((float)((float)L_23*(float)(2.0f))), /*hidden argument*/NULL);
	}

IL_00e0:
	{
		EffectWaveSystem_t2871388953 * L_24 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_24);
		float L_25 = L_24->get_m_time_3();
		WaitForSeconds_t1291133240 * L_26 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_26, L_25, /*hidden argument*/NULL);
		__this->set_U24current_4(L_26);
		__this->set_U24PC_3(2);
		goto IL_011b;
	}

IL_0102:
	{
		EffectWaveSystem_t2871388953 * L_27 = __this->get_U3CU3Ef__this_5();
		NullCheck(L_27);
		bool L_28 = L_27->get_m_enabled_5();
		if (L_28)
		{
			goto IL_0046;
		}
	}
	{
		__this->set_U24PC_3((-1));
	}

IL_0119:
	{
		return (bool)0;
	}

IL_011b:
	{
		return (bool)1;
	}
	// Dead block : IL_011d: ldloc.1
}
// System.Void EffectWaveSystem/<AddWave>c__Iterator5::Dispose()
extern "C"  void U3CAddWaveU3Ec__Iterator5_Dispose_m949255658 (U3CAddWaveU3Ec__Iterator5_t4079190500 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_3((-1));
		return;
	}
}
// System.Void EffectWaveSystem/<AddWave>c__Iterator5::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CAddWaveU3Ec__Iterator5_Reset_m2545845146_MetadataUsageId;
extern "C"  void U3CAddWaveU3Ec__Iterator5_Reset_m2545845146 (U3CAddWaveU3Ec__Iterator5_t4079190500 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CAddWaveU3Ec__Iterator5_Reset_m2545845146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void EnvelopContent::.ctor()
extern "C"  void EnvelopContent__ctor_m2186705271 (EnvelopContent_t239872932 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnvelopContent::Start()
extern "C"  void EnvelopContent_Start_m1133843063 (EnvelopContent_t239872932 * __this, const MethodInfo* method)
{
	{
		__this->set_mStarted_7((bool)1);
		EnvelopContent_Execute_m4036147146(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EnvelopContent::OnEnable()
extern "C"  void EnvelopContent_OnEnable_m144361135 (EnvelopContent_t239872932 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mStarted_7();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		EnvelopContent_Execute_m4036147146(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void EnvelopContent::Execute()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2863362005;
extern Il2CppCodeGenString* _stringLiteral2379005125;
extern Il2CppCodeGenString* _stringLiteral2735365941;
extern const uint32_t EnvelopContent_Execute_m4036147146_MetadataUsageId;
extern "C"  void EnvelopContent_Execute_m4036147146 (EnvelopContent_t239872932 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EnvelopContent_Execute_m4036147146_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Bounds_t3518514978  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	UIWidget_t769069560 * V_5 = NULL;
	Vector3_t3525329789  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3525329789  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3_t3525329789  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Vector3_t3525329789  V_9;
	memset(&V_9, 0, sizeof(V_9));
	{
		Transform_t284553113 * L_0 = __this->get_targetRoot_2();
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m214246398(NULL /*static, unused*/, _stringLiteral2863362005, __this, /*hidden argument*/NULL);
		goto IL_00ee;
	}

IL_0026:
	{
		Transform_t284553113 * L_3 = __this->get_targetRoot_2();
		Transform_t284553113 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_5 = NGUITools_IsChild_m3856680211(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m214246398(NULL /*static, unused*/, _stringLiteral2379005125, __this, /*hidden argument*/NULL);
		goto IL_00ee;
	}

IL_004c:
	{
		Transform_t284553113 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t284553113 * L_7 = Transform_get_parent_m2236876972(L_6, /*hidden argument*/NULL);
		Transform_t284553113 * L_8 = __this->get_targetRoot_2();
		Bounds_t3518514978  L_9 = NGUIMath_CalculateRelativeWidgetBounds_m638875862(NULL /*static, unused*/, L_7, L_8, (bool)0, (bool)1, /*hidden argument*/NULL);
		V_0 = L_9;
		Vector3_t3525329789  L_10 = Bounds_get_min_m2329472069((&V_0), /*hidden argument*/NULL);
		V_6 = L_10;
		float L_11 = (&V_6)->get_x_1();
		int32_t L_12 = __this->get_padLeft_3();
		V_1 = ((float)((float)L_11+(float)(((float)((float)L_12)))));
		Vector3_t3525329789  L_13 = Bounds_get_min_m2329472069((&V_0), /*hidden argument*/NULL);
		V_7 = L_13;
		float L_14 = (&V_7)->get_y_2();
		int32_t L_15 = __this->get_padBottom_5();
		V_2 = ((float)((float)L_14+(float)(((float)((float)L_15)))));
		Vector3_t3525329789  L_16 = Bounds_get_max_m2329243351((&V_0), /*hidden argument*/NULL);
		V_8 = L_16;
		float L_17 = (&V_8)->get_x_1();
		int32_t L_18 = __this->get_padRight_4();
		V_3 = ((float)((float)L_17+(float)(((float)((float)L_18)))));
		Vector3_t3525329789  L_19 = Bounds_get_max_m2329243351((&V_0), /*hidden argument*/NULL);
		V_9 = L_19;
		float L_20 = (&V_9)->get_y_2();
		int32_t L_21 = __this->get_padTop_6();
		V_4 = ((float)((float)L_20+(float)(((float)((float)L_21)))));
		UIWidget_t769069560 * L_22 = Component_GetComponent_TisUIWidget_t769069560_m2158946701(__this, /*hidden argument*/Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var);
		V_5 = L_22;
		UIWidget_t769069560 * L_23 = V_5;
		float L_24 = V_1;
		float L_25 = V_2;
		float L_26 = V_3;
		float L_27 = V_1;
		float L_28 = V_4;
		float L_29 = V_2;
		NullCheck(L_23);
		VirtActionInvoker4< float, float, float, float >::Invoke(19 /* System.Void UIWidget::SetRect(System.Single,System.Single,System.Single,System.Single) */, L_23, L_24, L_25, ((float)((float)L_26-(float)L_27)), ((float)((float)L_28-(float)L_29)));
		Component_BroadcastMessage_m1985948636(__this, _stringLiteral2735365941, 1, /*hidden argument*/NULL);
	}

IL_00ee:
	{
		return;
	}
}
// System.Void EquipItems::.ctor()
extern "C"  void EquipItems__ctor_m681791755 (EquipItems_t967969168 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EquipItems::Start()
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern Il2CppClass* InvGameItem_t1588794646_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisInvEquipment_t2103517373_m1342821800_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisInvEquipment_t2103517373_m2731734293_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral449152171;
extern const uint32_t EquipItems_Start_m3923896843_MetadataUsageId;
extern "C"  void EquipItems_Start_m3923896843 (EquipItems_t967969168 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EquipItems_Start_m3923896843_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	InvEquipment_t2103517373 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	InvBaseItem_t1636452469 * V_5 = NULL;
	InvGameItem_t1588794646 * V_6 = NULL;
	{
		Int32U5BU5D_t1809983122* L_0 = __this->get_itemIDs_2();
		if (!L_0)
		{
			goto IL_00c7;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_1 = __this->get_itemIDs_2();
		NullCheck(L_1);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_00c7;
		}
	}
	{
		InvEquipment_t2103517373 * L_2 = Component_GetComponent_TisInvEquipment_t2103517373_m1342821800(__this, /*hidden argument*/Component_GetComponent_TisInvEquipment_t2103517373_m1342821800_MethodInfo_var);
		V_0 = L_2;
		InvEquipment_t2103517373 * L_3 = V_0;
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		InvEquipment_t2103517373 * L_6 = GameObject_AddComponent_TisInvEquipment_t2103517373_m2731734293(L_5, /*hidden argument*/GameObject_AddComponent_TisInvEquipment_t2103517373_m2731734293_MethodInfo_var);
		V_0 = L_6;
	}

IL_0038:
	{
		V_1 = ((int32_t)12);
		V_2 = 0;
		Int32U5BU5D_t1809983122* L_7 = __this->get_itemIDs_2();
		NullCheck(L_7);
		V_3 = (((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length))));
		goto IL_00c0;
	}

IL_004b:
	{
		Int32U5BU5D_t1809983122* L_8 = __this->get_itemIDs_2();
		int32_t L_9 = V_2;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		V_4 = ((L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10)));
		int32_t L_11 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvBaseItem_t1636452469 * L_12 = InvDatabase_FindByID_m363273219(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_5 = L_12;
		InvBaseItem_t1636452469 * L_13 = V_5;
		if (!L_13)
		{
			goto IL_00a6;
		}
	}
	{
		int32_t L_14 = V_4;
		InvBaseItem_t1636452469 * L_15 = V_5;
		InvGameItem_t1588794646 * L_16 = (InvGameItem_t1588794646 *)il2cpp_codegen_object_new(InvGameItem_t1588794646_il2cpp_TypeInfo_var);
		InvGameItem__ctor_m2017209789(L_16, L_14, L_15, /*hidden argument*/NULL);
		V_6 = L_16;
		InvGameItem_t1588794646 * L_17 = V_6;
		int32_t L_18 = V_1;
		int32_t L_19 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_quality_1(L_19);
		InvGameItem_t1588794646 * L_20 = V_6;
		InvBaseItem_t1636452469 * L_21 = V_5;
		NullCheck(L_21);
		int32_t L_22 = L_21->get_minItemLevel_4();
		InvBaseItem_t1636452469 * L_23 = V_5;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_maxItemLevel_5();
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		int32_t L_25 = NGUITools_RandomRange_m4187782595(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		NullCheck(L_20);
		L_20->set_itemLevel_2(L_25);
		InvEquipment_t2103517373 * L_26 = V_0;
		InvGameItem_t1588794646 * L_27 = V_6;
		NullCheck(L_26);
		InvEquipment_Equip_m1078130573(L_26, L_27, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_00a6:
	{
		int32_t L_28 = V_4;
		int32_t L_29 = L_28;
		Il2CppObject * L_30 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_29);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral449152171, L_30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_31, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		int32_t L_32 = V_2;
		V_2 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_33 = V_2;
		int32_t L_34 = V_3;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_004b;
		}
	}

IL_00c7:
	{
		Object_Destroy_m176400816(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EquipRandomItem::.ctor()
extern "C"  void EquipRandomItem__ctor_m4292880261 (EquipRandomItem_t3062381222 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EquipRandomItem::OnClick()
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern Il2CppClass* InvGameItem_t1588794646_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t EquipRandomItem_OnClick_m4133531852_MetadataUsageId;
extern "C"  void EquipRandomItem_OnClick_m4133531852 (EquipRandomItem_t3062381222 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EquipRandomItem_OnClick_m4133531852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t2433411438 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	InvBaseItem_t1636452469 * V_3 = NULL;
	InvGameItem_t1588794646 * V_4 = NULL;
	{
		InvEquipment_t2103517373 * L_0 = __this->get_equipment_2();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}

IL_0012:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvDatabaseU5BU5D_t3324850277* L_2 = InvDatabase_get_list_m1583387015(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		int32_t L_3 = 0;
		NullCheck(((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3))));
		List_1_t2433411438 * L_4 = ((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_3)))->get_items_5();
		V_0 = L_4;
		List_1_t2433411438 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InvBaseItem>::get_Count() */, L_5);
		if (L_6)
		{
			goto IL_002b;
		}
	}
	{
		return;
	}

IL_002b:
	{
		V_1 = ((int32_t)12);
		List_1_t2433411438 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InvBaseItem>::get_Count() */, L_7);
		int32_t L_9 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		List_1_t2433411438 * L_10 = V_0;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		InvBaseItem_t1636452469 * L_12 = VirtFuncInvoker1< InvBaseItem_t1636452469 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<InvBaseItem>::get_Item(System.Int32) */, L_10, L_11);
		V_3 = L_12;
		int32_t L_13 = V_2;
		InvBaseItem_t1636452469 * L_14 = V_3;
		InvGameItem_t1588794646 * L_15 = (InvGameItem_t1588794646 *)il2cpp_codegen_object_new(InvGameItem_t1588794646_il2cpp_TypeInfo_var);
		InvGameItem__ctor_m2017209789(L_15, L_13, L_14, /*hidden argument*/NULL);
		V_4 = L_15;
		InvGameItem_t1588794646 * L_16 = V_4;
		int32_t L_17 = V_1;
		int32_t L_18 = Random_Range_m75452833(NULL /*static, unused*/, 0, L_17, /*hidden argument*/NULL);
		NullCheck(L_16);
		L_16->set_quality_1(L_18);
		InvGameItem_t1588794646 * L_19 = V_4;
		InvBaseItem_t1636452469 * L_20 = V_3;
		NullCheck(L_20);
		int32_t L_21 = L_20->get_minItemLevel_4();
		InvBaseItem_t1636452469 * L_22 = V_3;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_maxItemLevel_5();
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		int32_t L_24 = NGUITools_RandomRange_m4187782595(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		NullCheck(L_19);
		L_19->set_itemLevel_2(L_24);
		InvEquipment_t2103517373 * L_25 = __this->get_equipment_2();
		InvGameItem_t1588794646 * L_26 = V_4;
		NullCheck(L_25);
		InvEquipment_Equip_m1078130573(L_25, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventDelegate::.ctor()
extern "C"  void EventDelegate__ctor_m4124027436 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventDelegate::.ctor(EventDelegate/Callback)
extern "C"  void EventDelegate__ctor_m3576861239 (EventDelegate_t4004424223 * __this, Callback_t4187391077 * ___call0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Callback_t4187391077 * L_0 = ___call0;
		EventDelegate_Set_m4033556855(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventDelegate::.ctor(UnityEngine.MonoBehaviour,System.String)
extern "C"  void EventDelegate__ctor_m2088604063 (EventDelegate_t4004424223 * __this, MonoBehaviour_t3012272455 * ___target0, String_t* ___methodName1, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		MonoBehaviour_t3012272455 * L_0 = ___target0;
		String_t* L_1 = ___methodName1;
		EventDelegate_Set_m4163852895(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventDelegate::.cctor()
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4004424223;
extern const uint32_t EventDelegate__cctor_m2808702721_MetadataUsageId;
extern "C"  void EventDelegate__cctor_m2808702721 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate__cctor_m2808702721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck(_stringLiteral4004424223);
		int32_t L_0 = String_GetHashCode_m471729487(_stringLiteral4004424223, /*hidden argument*/NULL);
		((EventDelegate_t4004424223_StaticFields*)EventDelegate_t4004424223_il2cpp_TypeInfo_var->static_fields)->set_s_Hash_10(L_0);
		return;
	}
}
// UnityEngine.MonoBehaviour EventDelegate::get_target()
extern "C"  MonoBehaviour_t3012272455 * EventDelegate_get_target_m2810767074 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour_t3012272455 * L_0 = __this->get_mTarget_0();
		return L_0;
	}
}
// System.Void EventDelegate::set_target(UnityEngine.MonoBehaviour)
extern "C"  void EventDelegate_set_target_m1859480905 (EventDelegate_t4004424223 * __this, MonoBehaviour_t3012272455 * ___value0, const MethodInfo* method)
{
	{
		MonoBehaviour_t3012272455 * L_0 = ___value0;
		__this->set_mTarget_0(L_0);
		__this->set_mCachedCallback_4((Callback_t4187391077 *)NULL);
		__this->set_mRawDelegate_5((bool)0);
		__this->set_mCached_6((bool)0);
		__this->set_mMethod_7((MethodInfo_t *)NULL);
		__this->set_mParameterInfos_8((ParameterInfoU5BU5D_t1127461800*)NULL);
		__this->set_mParameters_2((ParameterU5BU5D_t3433801780*)NULL);
		return;
	}
}
// System.String EventDelegate::get_methodName()
extern "C"  String_t* EventDelegate_get_methodName_m3404252298 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mMethodName_1();
		return L_0;
	}
}
// System.Void EventDelegate::set_methodName(System.String)
extern "C"  void EventDelegate_set_methodName_m209940257 (EventDelegate_t4004424223 * __this, String_t* ___value0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_mMethodName_1(L_0);
		__this->set_mCachedCallback_4((Callback_t4187391077 *)NULL);
		__this->set_mRawDelegate_5((bool)0);
		__this->set_mCached_6((bool)0);
		__this->set_mMethod_7((MethodInfo_t *)NULL);
		__this->set_mParameterInfos_8((ParameterInfoU5BU5D_t1127461800*)NULL);
		__this->set_mParameters_2((ParameterU5BU5D_t3433801780*)NULL);
		return;
	}
}
// EventDelegate/Parameter[] EventDelegate::get_parameters()
extern "C"  ParameterU5BU5D_t3433801780* EventDelegate_get_parameters_m929749107 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mCached_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		EventDelegate_Cache_m1213589068(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		ParameterU5BU5D_t3433801780* L_1 = __this->get_mParameters_2();
		return L_1;
	}
}
// System.Boolean EventDelegate::get_isValid()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EventDelegate_get_isValid_m2331147303_MetadataUsageId;
extern "C"  bool EventDelegate_get_isValid_m2331147303 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_get_isValid_m2331147303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	{
		bool L_0 = __this->get_mCached_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		EventDelegate_Cache_m1213589068(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		bool L_1 = __this->get_mRawDelegate_5();
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Callback_t4187391077 * L_2 = __this->get_mCachedCallback_4();
		if (L_2)
		{
			goto IL_004b;
		}
	}

IL_0027:
	{
		MonoBehaviour_t3012272455 * L_3 = __this->get_mTarget_0();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_5 = __this->get_mMethodName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		G_B7_0 = ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		goto IL_0049;
	}

IL_0048:
	{
		G_B7_0 = 0;
	}

IL_0049:
	{
		G_B9_0 = G_B7_0;
		goto IL_004c;
	}

IL_004b:
	{
		G_B9_0 = 1;
	}

IL_004c:
	{
		return (bool)G_B9_0;
	}
}
// System.Boolean EventDelegate::get_isEnabled()
extern "C"  bool EventDelegate_get_isEnabled_m3034503084 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	MonoBehaviour_t3012272455 * V_0 = NULL;
	int32_t G_B10_0 = 0;
	{
		bool L_0 = __this->get_mCached_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		EventDelegate_Cache_m1213589068(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		bool L_1 = __this->get_mRawDelegate_5();
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		Callback_t4187391077 * L_2 = __this->get_mCachedCallback_4();
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		return (bool)1;
	}

IL_0029:
	{
		MonoBehaviour_t3012272455 * L_3 = __this->get_mTarget_0();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003c;
		}
	}
	{
		return (bool)0;
	}

IL_003c:
	{
		MonoBehaviour_t3012272455 * L_5 = __this->get_mTarget_0();
		V_0 = L_5;
		MonoBehaviour_t3012272455 * L_6 = V_0;
		bool L_7 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0057;
		}
	}
	{
		MonoBehaviour_t3012272455 * L_8 = V_0;
		NullCheck(L_8);
		bool L_9 = Behaviour_get_enabled_m1239363704(L_8, /*hidden argument*/NULL);
		G_B10_0 = ((int32_t)(L_9));
		goto IL_0058;
	}

IL_0057:
	{
		G_B10_0 = 1;
	}

IL_0058:
	{
		return (bool)G_B10_0;
	}
}
// System.String EventDelegate::GetMethodName(EventDelegate/Callback)
extern "C"  String_t* EventDelegate_GetMethodName_m1170951034 (Il2CppObject * __this /* static, unused */, Callback_t4187391077 * ___callback0, const MethodInfo* method)
{
	{
		Callback_t4187391077 * L_0 = ___callback0;
		NullCheck(L_0);
		MethodInfo_t * L_1 = Delegate_get_Method_m669548326(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_1);
		return L_2;
	}
}
// System.Boolean EventDelegate::IsValid(EventDelegate/Callback)
extern "C"  bool EventDelegate_IsValid_m102844731 (Il2CppObject * __this /* static, unused */, Callback_t4187391077 * ___callback0, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Callback_t4187391077 * L_0 = ___callback0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		Callback_t4187391077 * L_1 = ___callback0;
		NullCheck(L_1);
		MethodInfo_t * L_2 = Delegate_get_Method_m669548326(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((((int32_t)((((Il2CppObject*)(MethodInfo_t *)L_2) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0015;
	}

IL_0014:
	{
		G_B3_0 = 0;
	}

IL_0015:
	{
		return (bool)G_B3_0;
	}
}
// System.Boolean EventDelegate::Equals(System.Object)
extern Il2CppClass* Callback_t4187391077_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoBehaviour_t3012272455_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t EventDelegate_Equals_m670649297_MetadataUsageId;
extern "C"  bool EventDelegate_Equals_m670649297 (EventDelegate_t4004424223 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Equals_m670649297_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Callback_t4187391077 * V_0 = NULL;
	MonoBehaviour_t3012272455 * V_1 = NULL;
	EventDelegate_t4004424223 * V_2 = NULL;
	int32_t G_B8_0 = 0;
	int32_t G_B13_0 = 0;
	{
		Il2CppObject * L_0 = ___obj0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = EventDelegate_get_isValid_m2331147303(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___obj0;
		if (!((Callback_t4187391077 *)IsInstSealed(L_2, Callback_t4187391077_il2cpp_TypeInfo_var)))
		{
			goto IL_0067;
		}
	}
	{
		Il2CppObject * L_3 = ___obj0;
		V_0 = ((Callback_t4187391077 *)IsInstSealed(L_3, Callback_t4187391077_il2cpp_TypeInfo_var));
		Callback_t4187391077 * L_4 = V_0;
		Callback_t4187391077 * L_5 = __this->get_mCachedCallback_4();
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.MulticastDelegate::Equals(System.Object) */, L_4, L_5);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		return (bool)1;
	}

IL_0035:
	{
		Callback_t4187391077 * L_7 = V_0;
		NullCheck(L_7);
		Il2CppObject * L_8 = Delegate_get_Target_m2860483769(L_7, /*hidden argument*/NULL);
		V_1 = ((MonoBehaviour_t3012272455 *)IsInstClass(L_8, MonoBehaviour_t3012272455_il2cpp_TypeInfo_var));
		MonoBehaviour_t3012272455 * L_9 = __this->get_mTarget_0();
		MonoBehaviour_t3012272455 * L_10 = V_1;
		bool L_11 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0065;
		}
	}
	{
		String_t* L_12 = __this->get_mMethodName_1();
		Callback_t4187391077 * L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		String_t* L_14 = EventDelegate_GetMethodName_m1170951034(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_Equals_m1002918753(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		G_B8_0 = ((int32_t)(L_15));
		goto IL_0066;
	}

IL_0065:
	{
		G_B8_0 = 0;
	}

IL_0066:
	{
		return (bool)G_B8_0;
	}

IL_0067:
	{
		Il2CppObject * L_16 = ___obj0;
		if (!((EventDelegate_t4004424223 *)IsInstClass(L_16, EventDelegate_t4004424223_il2cpp_TypeInfo_var)))
		{
			goto IL_00a4;
		}
	}
	{
		Il2CppObject * L_17 = ___obj0;
		V_2 = ((EventDelegate_t4004424223 *)IsInstClass(L_17, EventDelegate_t4004424223_il2cpp_TypeInfo_var));
		MonoBehaviour_t3012272455 * L_18 = __this->get_mTarget_0();
		EventDelegate_t4004424223 * L_19 = V_2;
		NullCheck(L_19);
		MonoBehaviour_t3012272455 * L_20 = L_19->get_mTarget_0();
		bool L_21 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_18, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_00a2;
		}
	}
	{
		String_t* L_22 = __this->get_mMethodName_1();
		EventDelegate_t4004424223 * L_23 = V_2;
		NullCheck(L_23);
		String_t* L_24 = L_23->get_mMethodName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_25 = String_Equals_m1002918753(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		G_B13_0 = ((int32_t)(L_25));
		goto IL_00a3;
	}

IL_00a2:
	{
		G_B13_0 = 0;
	}

IL_00a3:
	{
		return (bool)G_B13_0;
	}

IL_00a4:
	{
		return (bool)0;
	}
}
// System.Int32 EventDelegate::GetHashCode()
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t EventDelegate_GetHashCode_m1013505769_MetadataUsageId;
extern "C"  int32_t EventDelegate_GetHashCode_m1013505769 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_GetHashCode_m1013505769_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		int32_t L_0 = ((EventDelegate_t4004424223_StaticFields*)EventDelegate_t4004424223_il2cpp_TypeInfo_var->static_fields)->get_s_Hash_10();
		return L_0;
	}
}
// System.Void EventDelegate::Set(EventDelegate/Callback)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppClass* MonoBehaviour_t3012272455_il2cpp_TypeInfo_var;
extern const uint32_t EventDelegate_Set_m4033556855_MetadataUsageId;
extern "C"  void EventDelegate_Set_m4033556855 (EventDelegate_t4004424223 * __this, Callback_t4187391077 * ___call0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Set_m4033556855_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		EventDelegate_Clear_m1530160727(__this, /*hidden argument*/NULL);
		Callback_t4187391077 * L_0 = ___call0;
		if (!L_0)
		{
			goto IL_0066;
		}
	}
	{
		Callback_t4187391077 * L_1 = ___call0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		bool L_2 = EventDelegate_IsValid_m102844731(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0066;
		}
	}
	{
		Callback_t4187391077 * L_3 = ___call0;
		NullCheck(L_3);
		Il2CppObject * L_4 = Delegate_get_Target_m2860483769(L_3, /*hidden argument*/NULL);
		__this->set_mTarget_0(((MonoBehaviour_t3012272455 *)IsInstClass(L_4, MonoBehaviour_t3012272455_il2cpp_TypeInfo_var)));
		MonoBehaviour_t3012272455 * L_5 = __this->get_mTarget_0();
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		__this->set_mRawDelegate_5((bool)1);
		Callback_t4187391077 * L_7 = ___call0;
		__this->set_mCachedCallback_4(L_7);
		__this->set_mMethodName_1((String_t*)NULL);
		goto IL_0066;
	}

IL_0053:
	{
		Callback_t4187391077 * L_8 = ___call0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		String_t* L_9 = EventDelegate_GetMethodName_m1170951034(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		__this->set_mMethodName_1(L_9);
		__this->set_mRawDelegate_5((bool)0);
	}

IL_0066:
	{
		return;
	}
}
// System.Void EventDelegate::Set(UnityEngine.MonoBehaviour,System.String)
extern "C"  void EventDelegate_Set_m4163852895 (EventDelegate_t4004424223 * __this, MonoBehaviour_t3012272455 * ___target0, String_t* ___methodName1, const MethodInfo* method)
{
	{
		EventDelegate_Clear_m1530160727(__this, /*hidden argument*/NULL);
		MonoBehaviour_t3012272455 * L_0 = ___target0;
		__this->set_mTarget_0(L_0);
		String_t* L_1 = ___methodName1;
		__this->set_mMethodName_1(L_1);
		return;
	}
}
// System.Void EventDelegate::Cache()
extern const Il2CppType* Void_t2779279689_0_0_0_var;
extern const Il2CppType* Callback_t4187391077_0_0_0_var;
extern Il2CppClass* MonoBehaviour_t3012272455_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Callback_t4187391077_il2cpp_TypeInfo_var;
extern Il2CppClass* ParameterU5BU5D_t3433801780_il2cpp_TypeInfo_var;
extern Il2CppClass* Parameter_t3958428553_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2440788707;
extern Il2CppCodeGenString* _stringLiteral37080744;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral2138086648;
extern const uint32_t EventDelegate_Cache_m1213589068_MetadataUsageId;
extern "C"  void EventDelegate_Cache_m1213589068 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Cache_m1213589068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		__this->set_mCached_6((bool)1);
		bool L_0 = __this->get_mRawDelegate_5();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Callback_t4187391077 * L_1 = __this->get_mCachedCallback_4();
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		Callback_t4187391077 * L_2 = __this->get_mCachedCallback_4();
		NullCheck(L_2);
		Il2CppObject * L_3 = Delegate_get_Target_m2860483769(L_2, /*hidden argument*/NULL);
		MonoBehaviour_t3012272455 * L_4 = __this->get_mTarget_0();
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, ((MonoBehaviour_t3012272455 *)IsInstClass(L_3, MonoBehaviour_t3012272455_il2cpp_TypeInfo_var)), L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0059;
		}
	}
	{
		Callback_t4187391077 * L_6 = __this->get_mCachedCallback_4();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		String_t* L_7 = EventDelegate_GetMethodName_m1170951034(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		String_t* L_8 = __this->get_mMethodName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_025b;
		}
	}

IL_0059:
	{
		MonoBehaviour_t3012272455 * L_10 = __this->get_mTarget_0();
		bool L_11 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_10, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_025b;
		}
	}
	{
		String_t* L_12 = __this->get_mMethodName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_025b;
		}
	}
	{
		MonoBehaviour_t3012272455 * L_14 = __this->get_mTarget_0();
		NullCheck(L_14);
		Type_t * L_15 = Object_GetType_m2022236990(L_14, /*hidden argument*/NULL);
		V_0 = L_15;
		__this->set_mMethod_7((MethodInfo_t *)NULL);
		goto IL_00c8;
	}

IL_0092:
	try
	{ // begin try (depth: 1)
		{
			Type_t * L_16 = V_0;
			String_t* L_17 = __this->get_mMethodName_1();
			NullCheck(L_16);
			MethodInfo_t * L_18 = VirtFuncInvoker2< MethodInfo_t *, String_t*, int32_t >::Invoke(48 /* System.Reflection.MethodInfo System.Type::GetMethod(System.String,System.Reflection.BindingFlags) */, L_16, L_17, ((int32_t)52));
			__this->set_mMethod_7(L_18);
			MethodInfo_t * L_19 = __this->get_mMethod_7();
			if (!L_19)
			{
				goto IL_00b6;
			}
		}

IL_00b1:
		{
			goto IL_00ce;
		}

IL_00b6:
		{
			goto IL_00c1;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00bb;
		throw e;
	}

CATCH_00bb:
	{ // begin catch(System.Exception)
		goto IL_00c1;
	} // end catch (depth: 1)

IL_00c1:
	{
		Type_t * L_20 = V_0;
		NullCheck(L_20);
		Type_t * L_21 = VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_20);
		V_0 = L_21;
	}

IL_00c8:
	{
		Type_t * L_22 = V_0;
		if (L_22)
		{
			goto IL_0092;
		}
	}

IL_00ce:
	{
		MethodInfo_t * L_23 = __this->get_mMethod_7();
		if (L_23)
		{
			goto IL_0117;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_24 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 0);
		ArrayElementTypeCheck (L_24, _stringLiteral2440788707);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral2440788707);
		ObjectU5BU5D_t11523773* L_25 = L_24;
		String_t* L_26 = __this->get_mMethodName_1();
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 1);
		ArrayElementTypeCheck (L_25, L_26);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_26);
		ObjectU5BU5D_t11523773* L_27 = L_25;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		ArrayElementTypeCheck (L_27, _stringLiteral37080744);
		(L_27)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral37080744);
		ObjectU5BU5D_t11523773* L_28 = L_27;
		MonoBehaviour_t3012272455 * L_29 = __this->get_mTarget_0();
		NullCheck(L_29);
		Type_t * L_30 = Object_GetType_m2022236990(L_29, /*hidden argument*/NULL);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 3);
		ArrayElementTypeCheck (L_28, L_30);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_30);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_31 = String_Concat_m3016520001(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		MonoBehaviour_t3012272455 * L_32 = __this->get_mTarget_0();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m214246398(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		return;
	}

IL_0117:
	{
		MethodInfo_t * L_33 = __this->get_mMethod_7();
		NullCheck(L_33);
		Type_t * L_34 = VirtFuncInvoker0< Type_t * >::Invoke(31 /* System.Type System.Reflection.MethodInfo::get_ReturnType() */, L_33);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_35 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Void_t2779279689_0_0_0_var), /*hidden argument*/NULL);
		if ((((Il2CppObject*)(Type_t *)L_34) == ((Il2CppObject*)(Type_t *)L_35)))
		{
			goto IL_016f;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_36 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
		MonoBehaviour_t3012272455 * L_37 = __this->get_mTarget_0();
		NullCheck(L_37);
		Type_t * L_38 = Object_GetType_m2022236990(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 0);
		ArrayElementTypeCheck (L_36, L_38);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_38);
		ObjectU5BU5D_t11523773* L_39 = L_36;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 1);
		ArrayElementTypeCheck (L_39, _stringLiteral46);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)_stringLiteral46);
		ObjectU5BU5D_t11523773* L_40 = L_39;
		String_t* L_41 = __this->get_mMethodName_1();
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 2);
		ArrayElementTypeCheck (L_40, L_41);
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)L_41);
		ObjectU5BU5D_t11523773* L_42 = L_40;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 3);
		ArrayElementTypeCheck (L_42, _stringLiteral2138086648);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)_stringLiteral2138086648);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_43 = String_Concat_m3016520001(NULL /*static, unused*/, L_42, /*hidden argument*/NULL);
		MonoBehaviour_t3012272455 * L_44 = __this->get_mTarget_0();
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m214246398(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		return;
	}

IL_016f:
	{
		MethodInfo_t * L_45 = __this->get_mMethod_7();
		NullCheck(L_45);
		ParameterInfoU5BU5D_t1127461800* L_46 = VirtFuncInvoker0< ParameterInfoU5BU5D_t1127461800* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_45);
		__this->set_mParameterInfos_8(L_46);
		ParameterInfoU5BU5D_t1127461800* L_47 = __this->get_mParameterInfos_8();
		NullCheck(L_47);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_47)->max_length)))))
		{
			goto IL_01c2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_48 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Callback_t4187391077_0_0_0_var), /*hidden argument*/NULL);
		MonoBehaviour_t3012272455 * L_49 = __this->get_mTarget_0();
		String_t* L_50 = __this->get_mMethodName_1();
		Delegate_t3660574010 * L_51 = Delegate_CreateDelegate_m1085193661(NULL /*static, unused*/, L_48, L_49, L_50, /*hidden argument*/NULL);
		__this->set_mCachedCallback_4(((Callback_t4187391077 *)CastclassSealed(L_51, Callback_t4187391077_il2cpp_TypeInfo_var)));
		__this->set_mArgs_9((ObjectU5BU5D_t11523773*)NULL);
		__this->set_mParameters_2((ParameterU5BU5D_t3433801780*)NULL);
		return;
	}

IL_01c2:
	{
		__this->set_mCachedCallback_4((Callback_t4187391077 *)NULL);
		ParameterU5BU5D_t3433801780* L_52 = __this->get_mParameters_2();
		if (!L_52)
		{
			goto IL_01e9;
		}
	}
	{
		ParameterU5BU5D_t3433801780* L_53 = __this->get_mParameters_2();
		NullCheck(L_53);
		ParameterInfoU5BU5D_t1127461800* L_54 = __this->get_mParameterInfos_8();
		NullCheck(L_54);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_53)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_54)->max_length)))))))
		{
			goto IL_0224;
		}
	}

IL_01e9:
	{
		ParameterInfoU5BU5D_t1127461800* L_55 = __this->get_mParameterInfos_8();
		NullCheck(L_55);
		__this->set_mParameters_2(((ParameterU5BU5D_t3433801780*)SZArrayNew(ParameterU5BU5D_t3433801780_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_55)->max_length)))))));
		V_1 = 0;
		ParameterU5BU5D_t3433801780* L_56 = __this->get_mParameters_2();
		NullCheck(L_56);
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_56)->max_length))));
		goto IL_021d;
	}

IL_020c:
	{
		ParameterU5BU5D_t3433801780* L_57 = __this->get_mParameters_2();
		int32_t L_58 = V_1;
		Parameter_t3958428553 * L_59 = (Parameter_t3958428553 *)il2cpp_codegen_object_new(Parameter_t3958428553_il2cpp_TypeInfo_var);
		Parameter__ctor_m3379983346(L_59, /*hidden argument*/NULL);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_58);
		ArrayElementTypeCheck (L_57, L_59);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(L_58), (Parameter_t3958428553 *)L_59);
		int32_t L_60 = V_1;
		V_1 = ((int32_t)((int32_t)L_60+(int32_t)1));
	}

IL_021d:
	{
		int32_t L_61 = V_1;
		int32_t L_62 = V_2;
		if ((((int32_t)L_61) < ((int32_t)L_62)))
		{
			goto IL_020c;
		}
	}

IL_0224:
	{
		V_3 = 0;
		ParameterU5BU5D_t3433801780* L_63 = __this->get_mParameters_2();
		NullCheck(L_63);
		V_4 = (((int32_t)((int32_t)(((Il2CppArray *)L_63)->max_length))));
		goto IL_0253;
	}

IL_0235:
	{
		ParameterU5BU5D_t3433801780* L_64 = __this->get_mParameters_2();
		int32_t L_65 = V_3;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		int32_t L_66 = L_65;
		ParameterInfoU5BU5D_t1127461800* L_67 = __this->get_mParameterInfos_8();
		int32_t L_68 = V_3;
		NullCheck(L_67);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_67, L_68);
		int32_t L_69 = L_68;
		NullCheck(((L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_69))));
		Type_t * L_70 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, ((L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_69))));
		NullCheck(((L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66))));
		((L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66)))->set_expectedType_3(L_70);
		int32_t L_71 = V_3;
		V_3 = ((int32_t)((int32_t)L_71+(int32_t)1));
	}

IL_0253:
	{
		int32_t L_72 = V_3;
		int32_t L_73 = V_4;
		if ((((int32_t)L_72) < ((int32_t)L_73)))
		{
			goto IL_0235;
		}
	}

IL_025b:
	{
		return;
	}
}
// System.Boolean EventDelegate::Execute()
extern Il2CppClass* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t124305799_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1516708628;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral1830;
extern Il2CppCodeGenString* _stringLiteral1017356552;
extern Il2CppCodeGenString* _stringLiteral3982044599;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral1731468785;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t EventDelegate_Execute_m213950963_MetadataUsageId;
extern "C"  bool EventDelegate_Execute_m213950963 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Execute_m213950963_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ArgumentException_t124305799 * V_3 = NULL;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	String_t* V_9 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	int32_t G_B8_0 = 0;
	{
		bool L_0 = __this->get_mCached_6();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		EventDelegate_Cache_m1213589068(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		Callback_t4187391077 * L_1 = __this->get_mCachedCallback_4();
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		Callback_t4187391077 * L_2 = __this->get_mCachedCallback_4();
		NullCheck(L_2);
		Callback_Invoke_m3974108630(L_2, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_0029:
	{
		MethodInfo_t * L_3 = __this->get_mMethod_7();
		if (!L_3)
		{
			goto IL_02e5;
		}
	}
	{
		ParameterU5BU5D_t3433801780* L_4 = __this->get_mParameters_2();
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		ParameterU5BU5D_t3433801780* L_5 = __this->get_mParameters_2();
		NullCheck(L_5);
		G_B8_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))));
		goto IL_004d;
	}

IL_004c:
	{
		G_B8_0 = 0;
	}

IL_004d:
	{
		V_0 = G_B8_0;
		int32_t L_6 = V_0;
		if (L_6)
		{
			goto IL_006c;
		}
	}
	{
		MethodInfo_t * L_7 = __this->get_mMethod_7();
		MonoBehaviour_t3012272455 * L_8 = __this->get_mTarget_0();
		NullCheck(L_7);
		VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(16 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[]) */, L_7, L_8, (ObjectU5BU5D_t11523773*)(ObjectU5BU5D_t11523773*)NULL);
		goto IL_02e3;
	}

IL_006c:
	{
		ObjectU5BU5D_t11523773* L_9 = __this->get_mArgs_9();
		if (!L_9)
		{
			goto IL_008c;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_10 = __this->get_mArgs_9();
		NullCheck(L_10);
		ParameterU5BU5D_t3433801780* L_11 = __this->get_mParameters_2();
		NullCheck(L_11);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))) == ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_009f;
		}
	}

IL_008c:
	{
		ParameterU5BU5D_t3433801780* L_12 = __this->get_mParameters_2();
		NullCheck(L_12);
		__this->set_mArgs_9(((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))));
	}

IL_009f:
	{
		V_1 = 0;
		ParameterU5BU5D_t3433801780* L_13 = __this->get_mParameters_2();
		NullCheck(L_13);
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length))));
		goto IL_00c8;
	}

IL_00af:
	{
		ObjectU5BU5D_t11523773* L_14 = __this->get_mArgs_9();
		int32_t L_15 = V_1;
		ParameterU5BU5D_t3433801780* L_16 = __this->get_mParameters_2();
		int32_t L_17 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		NullCheck(((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18))));
		Il2CppObject * L_19 = Parameter_get_value_m134508845(((L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18))), /*hidden argument*/NULL);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		ArrayElementTypeCheck (L_14, L_19);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_15), (Il2CppObject *)L_19);
		int32_t L_20 = V_1;
		V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_00c8:
	{
		int32_t L_21 = V_1;
		int32_t L_22 = V_2;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_00af;
		}
	}

IL_00cf:
	try
	{ // begin try (depth: 1)
		MethodInfo_t * L_23 = __this->get_mMethod_7();
		MonoBehaviour_t3012272455 * L_24 = __this->get_mTarget_0();
		ObjectU5BU5D_t11523773* L_25 = __this->get_mArgs_9();
		NullCheck(L_23);
		VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(16 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[]) */, L_23, L_24, L_25);
		goto IL_027b;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (ArgumentException_t124305799_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00ec;
		throw e;
	}

CATCH_00ec:
	{ // begin catch(System.ArgumentException)
		{
			V_3 = ((ArgumentException_t124305799 *)__exception_local);
			V_4 = _stringLiteral1516708628;
			MonoBehaviour_t3012272455 * L_26 = __this->get_mTarget_0();
			bool L_27 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_26, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
			if (!L_27)
			{
				goto IL_011e;
			}
		}

IL_0105:
		{
			String_t* L_28 = V_4;
			MethodInfo_t * L_29 = __this->get_mMethod_7();
			NullCheck(L_29);
			String_t* L_30 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_29);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_31 = String_Concat_m138640077(NULL /*static, unused*/, L_28, L_30, /*hidden argument*/NULL);
			V_4 = L_31;
			goto IL_0158;
		}

IL_011e:
		{
			String_t* L_32 = V_4;
			V_9 = L_32;
			ObjectU5BU5D_t11523773* L_33 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)4));
			String_t* L_34 = V_9;
			NullCheck(L_33);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 0);
			ArrayElementTypeCheck (L_33, L_34);
			(L_33)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)L_34);
			ObjectU5BU5D_t11523773* L_35 = L_33;
			MonoBehaviour_t3012272455 * L_36 = __this->get_mTarget_0();
			NullCheck(L_36);
			Type_t * L_37 = Object_GetType_m2022236990(L_36, /*hidden argument*/NULL);
			NullCheck(L_35);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_35, 1);
			ArrayElementTypeCheck (L_35, L_37);
			(L_35)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_37);
			ObjectU5BU5D_t11523773* L_38 = L_35;
			NullCheck(L_38);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 2);
			ArrayElementTypeCheck (L_38, _stringLiteral46);
			(L_38)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral46);
			ObjectU5BU5D_t11523773* L_39 = L_38;
			MethodInfo_t * L_40 = __this->get_mMethod_7();
			NullCheck(L_40);
			String_t* L_41 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_40);
			NullCheck(L_39);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 3);
			ArrayElementTypeCheck (L_39, L_41);
			(L_39)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_41);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_42 = String_Concat_m3016520001(NULL /*static, unused*/, L_39, /*hidden argument*/NULL);
			V_4 = L_42;
		}

IL_0158:
		{
			String_t* L_43 = V_4;
			ArgumentException_t124305799 * L_44 = V_3;
			NullCheck(L_44);
			String_t* L_45 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.ArgumentException::get_Message() */, L_44);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_46 = String_Concat_m1825781833(NULL /*static, unused*/, L_43, _stringLiteral1830, L_45, /*hidden argument*/NULL);
			V_4 = L_46;
			String_t* L_47 = V_4;
			String_t* L_48 = String_Concat_m138640077(NULL /*static, unused*/, L_47, _stringLiteral1017356552, /*hidden argument*/NULL);
			V_4 = L_48;
			ParameterInfoU5BU5D_t1127461800* L_49 = __this->get_mParameterInfos_8();
			NullCheck(L_49);
			if ((((int32_t)((int32_t)(((Il2CppArray *)L_49)->max_length)))))
			{
				goto IL_019a;
			}
		}

IL_0187:
		{
			String_t* L_50 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_51 = String_Concat_m138640077(NULL /*static, unused*/, L_50, _stringLiteral3982044599, /*hidden argument*/NULL);
			V_4 = L_51;
			goto IL_01e4;
		}

IL_019a:
		{
			String_t* L_52 = V_4;
			ParameterInfoU5BU5D_t1127461800* L_53 = __this->get_mParameterInfos_8();
			NullCheck(L_53);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 0);
			int32_t L_54 = 0;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_55 = String_Concat_m389863537(NULL /*static, unused*/, L_52, ((L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_54))), /*hidden argument*/NULL);
			V_4 = L_55;
			V_5 = 1;
			goto IL_01d5;
		}

IL_01b3:
		{
			String_t* L_56 = V_4;
			ParameterInfoU5BU5D_t1127461800* L_57 = __this->get_mParameterInfos_8();
			int32_t L_58 = V_5;
			NullCheck(L_57);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_58);
			int32_t L_59 = L_58;
			NullCheck(((L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59))));
			Type_t * L_60 = VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, ((L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59))));
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_61 = String_Concat_m2809334143(NULL /*static, unused*/, L_56, _stringLiteral1396, L_60, /*hidden argument*/NULL);
			V_4 = L_61;
			int32_t L_62 = V_5;
			V_5 = ((int32_t)((int32_t)L_62+(int32_t)1));
		}

IL_01d5:
		{
			int32_t L_63 = V_5;
			ParameterInfoU5BU5D_t1127461800* L_64 = __this->get_mParameterInfos_8();
			NullCheck(L_64);
			if ((((int32_t)L_63) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_64)->max_length)))))))
			{
				goto IL_01b3;
			}
		}

IL_01e4:
		{
			String_t* L_65 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_66 = String_Concat_m138640077(NULL /*static, unused*/, L_65, _stringLiteral1731468785, /*hidden argument*/NULL);
			V_4 = L_66;
			ParameterU5BU5D_t3433801780* L_67 = __this->get_mParameters_2();
			NullCheck(L_67);
			if ((((int32_t)((int32_t)(((Il2CppArray *)L_67)->max_length)))))
			{
				goto IL_0212;
			}
		}

IL_01ff:
		{
			String_t* L_68 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_69 = String_Concat_m138640077(NULL /*static, unused*/, L_68, _stringLiteral3982044599, /*hidden argument*/NULL);
			V_4 = L_69;
			goto IL_0261;
		}

IL_0212:
		{
			String_t* L_70 = V_4;
			ParameterU5BU5D_t3433801780* L_71 = __this->get_mParameters_2();
			NullCheck(L_71);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_71, 0);
			int32_t L_72 = 0;
			NullCheck(((L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_72))));
			Type_t * L_73 = Parameter_get_type_m2662781659(((L_71)->GetAt(static_cast<il2cpp_array_size_t>(L_72))), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_74 = String_Concat_m389863537(NULL /*static, unused*/, L_70, L_73, /*hidden argument*/NULL);
			V_4 = L_74;
			V_6 = 1;
			goto IL_0252;
		}

IL_0230:
		{
			String_t* L_75 = V_4;
			ParameterU5BU5D_t3433801780* L_76 = __this->get_mParameters_2();
			int32_t L_77 = V_6;
			NullCheck(L_76);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_76, L_77);
			int32_t L_78 = L_77;
			NullCheck(((L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_78))));
			Type_t * L_79 = Parameter_get_type_m2662781659(((L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_78))), /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_80 = String_Concat_m2809334143(NULL /*static, unused*/, L_75, _stringLiteral1396, L_79, /*hidden argument*/NULL);
			V_4 = L_80;
			int32_t L_81 = V_6;
			V_6 = ((int32_t)((int32_t)L_81+(int32_t)1));
		}

IL_0252:
		{
			int32_t L_82 = V_6;
			ParameterU5BU5D_t3433801780* L_83 = __this->get_mParameters_2();
			NullCheck(L_83);
			if ((((int32_t)L_82) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_83)->max_length)))))))
			{
				goto IL_0230;
			}
		}

IL_0261:
		{
			String_t* L_84 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_85 = String_Concat_m138640077(NULL /*static, unused*/, L_84, _stringLiteral10, /*hidden argument*/NULL);
			V_4 = L_85;
			String_t* L_86 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogError_m4127342994(NULL /*static, unused*/, L_86, /*hidden argument*/NULL);
			goto IL_027b;
		}
	} // end catch (depth: 1)

IL_027b:
	{
		V_7 = 0;
		ObjectU5BU5D_t11523773* L_87 = __this->get_mArgs_9();
		NullCheck(L_87);
		V_8 = (((int32_t)((int32_t)(((Il2CppArray *)L_87)->max_length))));
		goto IL_02da;
	}

IL_028d:
	{
		ParameterInfoU5BU5D_t1127461800* L_88 = __this->get_mParameterInfos_8();
		int32_t L_89 = V_7;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, L_89);
		int32_t L_90 = L_89;
		NullCheck(((L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90))));
		bool L_91 = ParameterInfo_get_IsIn_m462729209(((L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90))), /*hidden argument*/NULL);
		if (L_91)
		{
			goto IL_02b3;
		}
	}
	{
		ParameterInfoU5BU5D_t1127461800* L_92 = __this->get_mParameterInfos_8();
		int32_t L_93 = V_7;
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, L_93);
		int32_t L_94 = L_93;
		NullCheck(((L_92)->GetAt(static_cast<il2cpp_array_size_t>(L_94))));
		bool L_95 = ParameterInfo_get_IsOut_m1465526300(((L_92)->GetAt(static_cast<il2cpp_array_size_t>(L_94))), /*hidden argument*/NULL);
		if (!L_95)
		{
			goto IL_02ca;
		}
	}

IL_02b3:
	{
		ParameterU5BU5D_t3433801780* L_96 = __this->get_mParameters_2();
		int32_t L_97 = V_7;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, L_97);
		int32_t L_98 = L_97;
		ObjectU5BU5D_t11523773* L_99 = __this->get_mArgs_9();
		int32_t L_100 = V_7;
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, L_100);
		int32_t L_101 = L_100;
		NullCheck(((L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98))));
		Parameter_set_value_m1967520784(((L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98))), ((L_99)->GetAt(static_cast<il2cpp_array_size_t>(L_101))), /*hidden argument*/NULL);
	}

IL_02ca:
	{
		ObjectU5BU5D_t11523773* L_102 = __this->get_mArgs_9();
		int32_t L_103 = V_7;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, L_103);
		ArrayElementTypeCheck (L_102, NULL);
		(L_102)->SetAt(static_cast<il2cpp_array_size_t>(L_103), (Il2CppObject *)NULL);
		int32_t L_104 = V_7;
		V_7 = ((int32_t)((int32_t)L_104+(int32_t)1));
	}

IL_02da:
	{
		int32_t L_105 = V_7;
		int32_t L_106 = V_8;
		if ((((int32_t)L_105) < ((int32_t)L_106)))
		{
			goto IL_028d;
		}
	}

IL_02e3:
	{
		return (bool)1;
	}

IL_02e5:
	{
		return (bool)0;
	}
}
// System.Void EventDelegate::Clear()
extern "C"  void EventDelegate_Clear_m1530160727 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	{
		__this->set_mTarget_0((MonoBehaviour_t3012272455 *)NULL);
		__this->set_mMethodName_1((String_t*)NULL);
		__this->set_mRawDelegate_5((bool)0);
		__this->set_mCachedCallback_4((Callback_t4187391077 *)NULL);
		__this->set_mParameters_2((ParameterU5BU5D_t3433801780*)NULL);
		__this->set_mCached_6((bool)0);
		__this->set_mMethod_7((MethodInfo_t *)NULL);
		__this->set_mParameterInfos_8((ParameterInfoU5BU5D_t1127461800*)NULL);
		__this->set_mArgs_9((ObjectU5BU5D_t11523773*)NULL);
		return;
	}
}
// System.String EventDelegate::ToString()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral47;
extern Il2CppCodeGenString* _stringLiteral368958188;
extern Il2CppCodeGenString* _stringLiteral3221293437;
extern const uint32_t EventDelegate_ToString_m1851471041_MetadataUsageId;
extern "C"  String_t* EventDelegate_ToString_m1851471041 (EventDelegate_t4004424223 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_ToString_m1851471041_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* G_B9_0 = NULL;
	{
		MonoBehaviour_t3012272455 * L_0 = __this->get_mTarget_0();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006a;
		}
	}
	{
		MonoBehaviour_t3012272455 * L_2 = __this->get_mTarget_0();
		NullCheck(L_2);
		Type_t * L_3 = Object_GetType_m2022236990(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Type::ToString() */, L_3);
		V_0 = L_4;
		String_t* L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = String_LastIndexOf_m3245805612(L_5, ((int32_t)46), /*hidden argument*/NULL);
		V_1 = L_6;
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		String_t* L_8 = V_0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		String_t* L_10 = String_Substring_m2809233063(L_8, ((int32_t)((int32_t)L_9+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_10;
	}

IL_003c:
	{
		String_t* L_11 = EventDelegate_get_methodName_m3404252298(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_005e;
		}
	}
	{
		String_t* L_13 = V_0;
		String_t* L_14 = EventDelegate_get_methodName_m3404252298(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m1825781833(NULL /*static, unused*/, L_13, _stringLiteral47, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_005e:
	{
		String_t* L_16 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m138640077(NULL /*static, unused*/, L_16, _stringLiteral368958188, /*hidden argument*/NULL);
		return L_17;
	}

IL_006a:
	{
		bool L_18 = __this->get_mRawDelegate_5();
		if (!L_18)
		{
			goto IL_007f;
		}
	}
	{
		G_B9_0 = _stringLiteral3221293437;
		goto IL_0080;
	}

IL_007f:
	{
		G_B9_0 = ((String_t*)(NULL));
	}

IL_0080:
	{
		return G_B9_0;
	}
}
// System.Void EventDelegate::Execute(System.Collections.Generic.List`1<EventDelegate>)
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const uint32_t EventDelegate_Execute_m895247138_MetadataUsageId;
extern "C"  void EventDelegate_Execute_m895247138 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Execute_m895247138_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	EventDelegate_t4004424223 * V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		List_1_t506415896 * L_0 = ___list0;
		if (!L_0)
		{
			goto IL_00a2;
		}
	}
	{
		V_0 = 0;
		goto IL_0096;
	}

IL_000d:
	{
		List_1_t506415896 * L_1 = ___list0;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		EventDelegate_t4004424223 * L_3 = VirtFuncInvoker1< EventDelegate_t4004424223 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<EventDelegate>::get_Item(System.Int32) */, L_1, L_2);
		V_1 = L_3;
		EventDelegate_t4004424223 * L_4 = V_1;
		if (!L_4)
		{
			goto IL_0092;
		}
	}

IL_001b:
	try
	{ // begin try (depth: 1)
		EventDelegate_t4004424223 * L_5 = V_1;
		NullCheck(L_5);
		EventDelegate_Execute_m213950963(L_5, /*hidden argument*/NULL);
		goto IL_0058;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0027;
		throw e;
	}

CATCH_0027:
	{ // begin catch(System.Exception)
		{
			V_2 = ((Exception_t1967233988 *)__exception_local);
			Exception_t1967233988 * L_6 = V_2;
			NullCheck(L_6);
			Exception_t1967233988 * L_7 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_6);
			if (!L_7)
			{
				goto IL_0048;
			}
		}

IL_0033:
		{
			Exception_t1967233988 * L_8 = V_2;
			NullCheck(L_8);
			Exception_t1967233988 * L_9 = VirtFuncInvoker0< Exception_t1967233988 * >::Invoke(5 /* System.Exception System.Exception::get_InnerException() */, L_8);
			NullCheck(L_9);
			String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_9);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogError_m4127342994(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
			goto IL_0053;
		}

IL_0048:
		{
			Exception_t1967233988 * L_11 = V_2;
			NullCheck(L_11);
			String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_11);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
			Debug_LogError_m4127342994(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		}

IL_0053:
		{
			goto IL_0058;
		}
	} // end catch (depth: 1)

IL_0058:
	{
		int32_t L_13 = V_0;
		List_1_t506415896 * L_14 = ___list0;
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<EventDelegate>::get_Count() */, L_14);
		if ((((int32_t)L_13) < ((int32_t)L_15)))
		{
			goto IL_0069;
		}
	}
	{
		goto IL_00a2;
	}

IL_0069:
	{
		List_1_t506415896 * L_16 = ___list0;
		int32_t L_17 = V_0;
		NullCheck(L_16);
		EventDelegate_t4004424223 * L_18 = VirtFuncInvoker1< EventDelegate_t4004424223 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<EventDelegate>::get_Item(System.Int32) */, L_16, L_17);
		EventDelegate_t4004424223 * L_19 = V_1;
		if ((((Il2CppObject*)(EventDelegate_t4004424223 *)L_18) == ((Il2CppObject*)(EventDelegate_t4004424223 *)L_19)))
		{
			goto IL_007b;
		}
	}
	{
		goto IL_0096;
	}

IL_007b:
	{
		EventDelegate_t4004424223 * L_20 = V_1;
		NullCheck(L_20);
		bool L_21 = L_20->get_oneShot_3();
		if (!L_21)
		{
			goto IL_0092;
		}
	}
	{
		List_1_t506415896 * L_22 = ___list0;
		int32_t L_23 = V_0;
		NullCheck(L_22);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<EventDelegate>::RemoveAt(System.Int32) */, L_22, L_23);
		goto IL_0096;
	}

IL_0092:
	{
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_25 = V_0;
		List_1_t506415896 * L_26 = ___list0;
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<EventDelegate>::get_Count() */, L_26);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_000d;
		}
	}

IL_00a2:
	{
		return;
	}
}
// System.Boolean EventDelegate::IsValid(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  bool EventDelegate_IsValid_m979192339 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	EventDelegate_t4004424223 * V_2 = NULL;
	{
		List_1_t506415896 * L_0 = ___list0;
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		V_0 = 0;
		List_1_t506415896 * L_1 = ___list0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<EventDelegate>::get_Count() */, L_1);
		V_1 = L_2;
		goto IL_0033;
	}

IL_0014:
	{
		List_1_t506415896 * L_3 = ___list0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		EventDelegate_t4004424223 * L_5 = VirtFuncInvoker1< EventDelegate_t4004424223 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<EventDelegate>::get_Item(System.Int32) */, L_3, L_4);
		V_2 = L_5;
		EventDelegate_t4004424223 * L_6 = V_2;
		if (!L_6)
		{
			goto IL_002f;
		}
	}
	{
		EventDelegate_t4004424223 * L_7 = V_2;
		NullCheck(L_7);
		bool L_8 = EventDelegate_get_isValid_m2331147303(L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		return (bool)1;
	}

IL_002f:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0014;
		}
	}

IL_003a:
	{
		return (bool)0;
	}
}
// EventDelegate EventDelegate::Set(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t EventDelegate_Set_m3964967374_MetadataUsageId;
extern "C"  EventDelegate_t4004424223 * EventDelegate_Set_m3964967374 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, Callback_t4187391077 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Set_m3964967374_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EventDelegate_t4004424223 * V_0 = NULL;
	{
		List_1_t506415896 * L_0 = ___list0;
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		Callback_t4187391077 * L_1 = ___callback1;
		EventDelegate_t4004424223 * L_2 = (EventDelegate_t4004424223 *)il2cpp_codegen_object_new(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate__ctor_m3576861239(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		List_1_t506415896 * L_3 = ___list0;
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<EventDelegate>::Clear() */, L_3);
		List_1_t506415896 * L_4 = ___list0;
		EventDelegate_t4004424223 * L_5 = V_0;
		NullCheck(L_4);
		VirtActionInvoker1< EventDelegate_t4004424223 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<EventDelegate>::Add(!0) */, L_4, L_5);
		EventDelegate_t4004424223 * L_6 = V_0;
		return L_6;
	}

IL_001c:
	{
		return (EventDelegate_t4004424223 *)NULL;
	}
}
// System.Void EventDelegate::Set(System.Collections.Generic.List`1<EventDelegate>,EventDelegate)
extern "C"  void EventDelegate_Set_m2854322620 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, EventDelegate_t4004424223 * ___del1, const MethodInfo* method)
{
	{
		List_1_t506415896 * L_0 = ___list0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		List_1_t506415896 * L_1 = ___list0;
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<EventDelegate>::Clear() */, L_1);
		List_1_t506415896 * L_2 = ___list0;
		EventDelegate_t4004424223 * L_3 = ___del1;
		NullCheck(L_2);
		VirtActionInvoker1< EventDelegate_t4004424223 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<EventDelegate>::Add(!0) */, L_2, L_3);
	}

IL_0013:
	{
		return;
	}
}
// EventDelegate EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t EventDelegate_Add_m1811262575_MetadataUsageId;
extern "C"  EventDelegate_t4004424223 * EventDelegate_Add_m1811262575 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, Callback_t4187391077 * ___callback1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Add_m1811262575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t506415896 * L_0 = ___list0;
		Callback_t4187391077 * L_1 = ___callback1;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_t4004424223 * L_2 = EventDelegate_Add_m1711037230(NULL /*static, unused*/, L_0, L_1, (bool)0, /*hidden argument*/NULL);
		return L_2;
	}
}
// EventDelegate EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback,System.Boolean)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2476773555;
extern const uint32_t EventDelegate_Add_m1711037230_MetadataUsageId;
extern "C"  EventDelegate_t4004424223 * EventDelegate_Add_m1711037230 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, Callback_t4187391077 * ___callback1, bool ___oneShot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Add_m1711037230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	EventDelegate_t4004424223 * V_2 = NULL;
	EventDelegate_t4004424223 * V_3 = NULL;
	{
		List_1_t506415896 * L_0 = ___list0;
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		V_0 = 0;
		List_1_t506415896 * L_1 = ___list0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<EventDelegate>::get_Count() */, L_1);
		V_1 = L_2;
		goto IL_0034;
	}

IL_0014:
	{
		List_1_t506415896 * L_3 = ___list0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		EventDelegate_t4004424223 * L_5 = VirtFuncInvoker1< EventDelegate_t4004424223 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<EventDelegate>::get_Item(System.Int32) */, L_3, L_4);
		V_2 = L_5;
		EventDelegate_t4004424223 * L_6 = V_2;
		if (!L_6)
		{
			goto IL_0030;
		}
	}
	{
		EventDelegate_t4004424223 * L_7 = V_2;
		Callback_t4187391077 * L_8 = ___callback1;
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean EventDelegate::Equals(System.Object) */, L_7, L_8);
		if (!L_9)
		{
			goto IL_0030;
		}
	}
	{
		EventDelegate_t4004424223 * L_10 = V_2;
		return L_10;
	}

IL_0030:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_12 = V_0;
		int32_t L_13 = V_1;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_0014;
		}
	}
	{
		Callback_t4187391077 * L_14 = ___callback1;
		EventDelegate_t4004424223 * L_15 = (EventDelegate_t4004424223 *)il2cpp_codegen_object_new(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate__ctor_m3576861239(L_15, L_14, /*hidden argument*/NULL);
		V_3 = L_15;
		EventDelegate_t4004424223 * L_16 = V_3;
		bool L_17 = ___oneShot2;
		NullCheck(L_16);
		L_16->set_oneShot_3(L_17);
		List_1_t506415896 * L_18 = ___list0;
		EventDelegate_t4004424223 * L_19 = V_3;
		NullCheck(L_18);
		VirtActionInvoker1< EventDelegate_t4004424223 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<EventDelegate>::Add(!0) */, L_18, L_19);
		EventDelegate_t4004424223 * L_20 = V_3;
		return L_20;
	}

IL_0052:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral2476773555, /*hidden argument*/NULL);
		return (EventDelegate_t4004424223 *)NULL;
	}
}
// System.Void EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t EventDelegate_Add_m4032808443_MetadataUsageId;
extern "C"  void EventDelegate_Add_m4032808443 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, EventDelegate_t4004424223 * ___ev1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Add_m4032808443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t506415896 * L_0 = ___list0;
		EventDelegate_t4004424223 * L_1 = ___ev1;
		EventDelegate_t4004424223 * L_2 = ___ev1;
		NullCheck(L_2);
		bool L_3 = L_2->get_oneShot_3();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m3670177826(NULL /*static, unused*/, L_0, L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventDelegate::Add(System.Collections.Generic.List`1<EventDelegate>,EventDelegate,System.Boolean)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppClass* ParameterU5BU5D_t3433801780_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2476773555;
extern const uint32_t EventDelegate_Add_m3670177826_MetadataUsageId;
extern "C"  void EventDelegate_Add_m3670177826 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, EventDelegate_t4004424223 * ___ev1, bool ___oneShot2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (EventDelegate_Add_m3670177826_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	EventDelegate_t4004424223 * V_2 = NULL;
	EventDelegate_t4004424223 * V_3 = NULL;
	int32_t V_4 = 0;
	{
		EventDelegate_t4004424223 * L_0 = ___ev1;
		NullCheck(L_0);
		bool L_1 = L_0->get_mRawDelegate_5();
		if (L_1)
		{
			goto IL_002c;
		}
	}
	{
		EventDelegate_t4004424223 * L_2 = ___ev1;
		NullCheck(L_2);
		MonoBehaviour_t3012272455 * L_3 = EventDelegate_get_target_m2810767074(L_2, /*hidden argument*/NULL);
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002c;
		}
	}
	{
		EventDelegate_t4004424223 * L_5 = ___ev1;
		NullCheck(L_5);
		String_t* L_6 = EventDelegate_get_methodName_m3404252298(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003f;
		}
	}

IL_002c:
	{
		List_1_t506415896 * L_8 = ___list0;
		EventDelegate_t4004424223 * L_9 = ___ev1;
		NullCheck(L_9);
		Callback_t4187391077 * L_10 = L_9->get_mCachedCallback_4();
		bool L_11 = ___oneShot2;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1711037230(NULL /*static, unused*/, L_8, L_10, L_11, /*hidden argument*/NULL);
		goto IL_0103;
	}

IL_003f:
	{
		List_1_t506415896 * L_12 = ___list0;
		if (!L_12)
		{
			goto IL_00f9;
		}
	}
	{
		V_0 = 0;
		List_1_t506415896 * L_13 = ___list0;
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<EventDelegate>::get_Count() */, L_13);
		V_1 = L_14;
		goto IL_0072;
	}

IL_0053:
	{
		List_1_t506415896 * L_15 = ___list0;
		int32_t L_16 = V_0;
		NullCheck(L_15);
		EventDelegate_t4004424223 * L_17 = VirtFuncInvoker1< EventDelegate_t4004424223 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<EventDelegate>::get_Item(System.Int32) */, L_15, L_16);
		V_2 = L_17;
		EventDelegate_t4004424223 * L_18 = V_2;
		if (!L_18)
		{
			goto IL_006e;
		}
	}
	{
		EventDelegate_t4004424223 * L_19 = V_2;
		EventDelegate_t4004424223 * L_20 = ___ev1;
		NullCheck(L_19);
		bool L_21 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean EventDelegate::Equals(System.Object) */, L_19, L_20);
		if (!L_21)
		{
			goto IL_006e;
		}
	}
	{
		return;
	}

IL_006e:
	{
		int32_t L_22 = V_0;
		V_0 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_0072:
	{
		int32_t L_23 = V_0;
		int32_t L_24 = V_1;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_0053;
		}
	}
	{
		EventDelegate_t4004424223 * L_25 = ___ev1;
		NullCheck(L_25);
		MonoBehaviour_t3012272455 * L_26 = EventDelegate_get_target_m2810767074(L_25, /*hidden argument*/NULL);
		EventDelegate_t4004424223 * L_27 = ___ev1;
		NullCheck(L_27);
		String_t* L_28 = EventDelegate_get_methodName_m3404252298(L_27, /*hidden argument*/NULL);
		EventDelegate_t4004424223 * L_29 = (EventDelegate_t4004424223 *)il2cpp_codegen_object_new(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate__ctor_m2088604063(L_29, L_26, L_28, /*hidden argument*/NULL);
		V_3 = L_29;
		EventDelegate_t4004424223 * L_30 = V_3;
		bool L_31 = ___oneShot2;
		NullCheck(L_30);
		L_30->set_oneShot_3(L_31);
		EventDelegate_t4004424223 * L_32 = ___ev1;
		NullCheck(L_32);
		ParameterU5BU5D_t3433801780* L_33 = L_32->get_mParameters_2();
		if (!L_33)
		{
			goto IL_00ed;
		}
	}
	{
		EventDelegate_t4004424223 * L_34 = ___ev1;
		NullCheck(L_34);
		ParameterU5BU5D_t3433801780* L_35 = L_34->get_mParameters_2();
		NullCheck(L_35);
		if ((((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_35)->max_length))))) <= ((int32_t)0)))
		{
			goto IL_00ed;
		}
	}
	{
		EventDelegate_t4004424223 * L_36 = V_3;
		EventDelegate_t4004424223 * L_37 = ___ev1;
		NullCheck(L_37);
		ParameterU5BU5D_t3433801780* L_38 = L_37->get_mParameters_2();
		NullCheck(L_38);
		NullCheck(L_36);
		L_36->set_mParameters_2(((ParameterU5BU5D_t3433801780*)SZArrayNew(ParameterU5BU5D_t3433801780_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_38)->max_length)))))));
		V_4 = 0;
		goto IL_00de;
	}

IL_00c6:
	{
		EventDelegate_t4004424223 * L_39 = V_3;
		NullCheck(L_39);
		ParameterU5BU5D_t3433801780* L_40 = L_39->get_mParameters_2();
		int32_t L_41 = V_4;
		EventDelegate_t4004424223 * L_42 = ___ev1;
		NullCheck(L_42);
		ParameterU5BU5D_t3433801780* L_43 = L_42->get_mParameters_2();
		int32_t L_44 = V_4;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		int32_t L_45 = L_44;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		ArrayElementTypeCheck (L_40, ((L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45))));
		(L_40)->SetAt(static_cast<il2cpp_array_size_t>(L_41), (Parameter_t3958428553 *)((L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45))));
		int32_t L_46 = V_4;
		V_4 = ((int32_t)((int32_t)L_46+(int32_t)1));
	}

IL_00de:
	{
		int32_t L_47 = V_4;
		EventDelegate_t4004424223 * L_48 = ___ev1;
		NullCheck(L_48);
		ParameterU5BU5D_t3433801780* L_49 = L_48->get_mParameters_2();
		NullCheck(L_49);
		if ((((int32_t)L_47) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_49)->max_length)))))))
		{
			goto IL_00c6;
		}
	}

IL_00ed:
	{
		List_1_t506415896 * L_50 = ___list0;
		EventDelegate_t4004424223 * L_51 = V_3;
		NullCheck(L_50);
		VirtActionInvoker1< EventDelegate_t4004424223 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<EventDelegate>::Add(!0) */, L_50, L_51);
		goto IL_0103;
	}

IL_00f9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, _stringLiteral2476773555, /*hidden argument*/NULL);
	}

IL_0103:
	{
		return;
	}
}
// System.Boolean EventDelegate::Remove(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  bool EventDelegate_Remove_m188644518 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, Callback_t4187391077 * ___callback1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	EventDelegate_t4004424223 * V_2 = NULL;
	{
		List_1_t506415896 * L_0 = ___list0;
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		V_0 = 0;
		List_1_t506415896 * L_1 = ___list0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<EventDelegate>::get_Count() */, L_1);
		V_1 = L_2;
		goto IL_003b;
	}

IL_0014:
	{
		List_1_t506415896 * L_3 = ___list0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		EventDelegate_t4004424223 * L_5 = VirtFuncInvoker1< EventDelegate_t4004424223 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<EventDelegate>::get_Item(System.Int32) */, L_3, L_4);
		V_2 = L_5;
		EventDelegate_t4004424223 * L_6 = V_2;
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		EventDelegate_t4004424223 * L_7 = V_2;
		Callback_t4187391077 * L_8 = ___callback1;
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean EventDelegate::Equals(System.Object) */, L_7, L_8);
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		List_1_t506415896 * L_10 = ___list0;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<EventDelegate>::RemoveAt(System.Int32) */, L_10, L_11);
		return (bool)1;
	}

IL_0037:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0014;
		}
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Boolean EventDelegate::Remove(System.Collections.Generic.List`1<EventDelegate>,EventDelegate)
extern "C"  bool EventDelegate_Remove_m2048219512 (Il2CppObject * __this /* static, unused */, List_1_t506415896 * ___list0, EventDelegate_t4004424223 * ___ev1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	EventDelegate_t4004424223 * V_2 = NULL;
	{
		List_1_t506415896 * L_0 = ___list0;
		if (!L_0)
		{
			goto IL_0042;
		}
	}
	{
		V_0 = 0;
		List_1_t506415896 * L_1 = ___list0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<EventDelegate>::get_Count() */, L_1);
		V_1 = L_2;
		goto IL_003b;
	}

IL_0014:
	{
		List_1_t506415896 * L_3 = ___list0;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		EventDelegate_t4004424223 * L_5 = VirtFuncInvoker1< EventDelegate_t4004424223 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<EventDelegate>::get_Item(System.Int32) */, L_3, L_4);
		V_2 = L_5;
		EventDelegate_t4004424223 * L_6 = V_2;
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		EventDelegate_t4004424223 * L_7 = V_2;
		EventDelegate_t4004424223 * L_8 = ___ev1;
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean EventDelegate::Equals(System.Object) */, L_7, L_8);
		if (!L_9)
		{
			goto IL_0037;
		}
	}
	{
		List_1_t506415896 * L_10 = ___list0;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<EventDelegate>::RemoveAt(System.Int32) */, L_10, L_11);
		return (bool)1;
	}

IL_0037:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_0014;
		}
	}

IL_0042:
	{
		return (bool)0;
	}
}
// System.Void EventDelegate/Callback::.ctor(System.Object,System.IntPtr)
extern "C"  void Callback__ctor_m3018475132 (Callback_t4187391077 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void EventDelegate/Callback::Invoke()
extern "C"  void Callback_Invoke_m3974108630 (Callback_t4187391077 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Callback_Invoke_m3974108630((Callback_t4187391077 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_Callback_t4187391077(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult EventDelegate/Callback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Callback_BeginInvoke_m1093023085 (Callback_t4187391077 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void EventDelegate/Callback::EndInvoke(System.IAsyncResult)
extern "C"  void Callback_EndInvoke_m1551652492 (Callback_t4187391077 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void EventDelegate/Parameter::.ctor()
extern const Il2CppType* Void_t2779279689_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Parameter__ctor_m3379983346_MetadataUsageId;
extern "C"  void Parameter__ctor_m3379983346 (Parameter_t3958428553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parameter__ctor_m3379983346_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Void_t2779279689_0_0_0_var), /*hidden argument*/NULL);
		__this->set_expectedType_3(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventDelegate/Parameter::.ctor(UnityEngine.Object,System.String)
extern const Il2CppType* Void_t2779279689_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Parameter__ctor_m981369464_MetadataUsageId;
extern "C"  void Parameter__ctor_m981369464 (Parameter_t3958428553 * __this, Object_t3878351788 * ___obj0, String_t* ___field1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parameter__ctor_m981369464_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Void_t2779279689_0_0_0_var), /*hidden argument*/NULL);
		__this->set_expectedType_3(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = ___obj0;
		__this->set_obj_0(L_1);
		String_t* L_2 = ___field1;
		__this->set_field_1(L_2);
		return;
	}
}
// System.Void EventDelegate/Parameter::.ctor(System.Object)
extern const Il2CppType* Void_t2779279689_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Parameter__ctor_m3475228386_MetadataUsageId;
extern "C"  void Parameter__ctor_m3475228386 (Parameter_t3958428553 * __this, Il2CppObject * ___val0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parameter__ctor_m3475228386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Void_t2779279689_0_0_0_var), /*hidden argument*/NULL);
		__this->set_expectedType_3(L_0);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = ___val0;
		__this->set_mValue_2(L_1);
		return;
	}
}
// System.Object EventDelegate/Parameter::get_value()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Convert_t1097883944_il2cpp_TypeInfo_var;
extern const uint32_t Parameter_get_value_m134508845_MetadataUsageId;
extern "C"  Il2CppObject * Parameter_get_value_m134508845 (Parameter_t3958428553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parameter_get_value_m134508845_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = NULL;
	{
		Il2CppObject * L_0 = __this->get_mValue_2();
		if (!L_0)
		{
			goto IL_0012;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mValue_2();
		return L_1;
	}

IL_0012:
	{
		bool L_2 = __this->get_cached_4();
		if (L_2)
		{
			goto IL_008e;
		}
	}
	{
		__this->set_cached_4((bool)1);
		__this->set_fieldInfo_6((FieldInfo_t *)NULL);
		__this->set_propInfo_5((PropertyInfo_t *)NULL);
		Object_t3878351788 * L_3 = __this->get_obj_0();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_008e;
		}
	}
	{
		String_t* L_5 = __this->get_field_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_6 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_008e;
		}
	}
	{
		Object_t3878351788 * L_7 = __this->get_obj_0();
		NullCheck(L_7);
		Type_t * L_8 = Object_GetType_m2022236990(L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		Type_t * L_9 = V_0;
		String_t* L_10 = __this->get_field_1();
		NullCheck(L_9);
		PropertyInfo_t * L_11 = VirtFuncInvoker1< PropertyInfo_t *, String_t* >::Invoke(53 /* System.Reflection.PropertyInfo System.Type::GetProperty(System.String) */, L_9, L_10);
		__this->set_propInfo_5(L_11);
		PropertyInfo_t * L_12 = __this->get_propInfo_5();
		if (L_12)
		{
			goto IL_008e;
		}
	}
	{
		Type_t * L_13 = V_0;
		String_t* L_14 = __this->get_field_1();
		NullCheck(L_13);
		FieldInfo_t * L_15 = VirtFuncInvoker1< FieldInfo_t *, String_t* >::Invoke(44 /* System.Reflection.FieldInfo System.Type::GetField(System.String) */, L_13, L_14);
		__this->set_fieldInfo_6(L_15);
	}

IL_008e:
	{
		PropertyInfo_t * L_16 = __this->get_propInfo_5();
		if (!L_16)
		{
			goto IL_00ac;
		}
	}
	{
		PropertyInfo_t * L_17 = __this->get_propInfo_5();
		Object_t3878351788 * L_18 = __this->get_obj_0();
		NullCheck(L_17);
		Il2CppObject * L_19 = VirtFuncInvoker2< Il2CppObject *, Il2CppObject *, ObjectU5BU5D_t11523773* >::Invoke(22 /* System.Object System.Reflection.PropertyInfo::GetValue(System.Object,System.Object[]) */, L_17, L_18, (ObjectU5BU5D_t11523773*)(ObjectU5BU5D_t11523773*)NULL);
		return L_19;
	}

IL_00ac:
	{
		FieldInfo_t * L_20 = __this->get_fieldInfo_6();
		if (!L_20)
		{
			goto IL_00c9;
		}
	}
	{
		FieldInfo_t * L_21 = __this->get_fieldInfo_6();
		Object_t3878351788 * L_22 = __this->get_obj_0();
		NullCheck(L_21);
		Il2CppObject * L_23 = VirtFuncInvoker1< Il2CppObject *, Il2CppObject * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_21, L_22);
		return L_23;
	}

IL_00c9:
	{
		Object_t3878351788 * L_24 = __this->get_obj_0();
		bool L_25 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_24, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00e1;
		}
	}
	{
		Object_t3878351788 * L_26 = __this->get_obj_0();
		return L_26;
	}

IL_00e1:
	{
		Type_t * L_27 = __this->get_expectedType_3();
		if (!L_27)
		{
			goto IL_00fe;
		}
	}
	{
		Type_t * L_28 = __this->get_expectedType_3();
		NullCheck(L_28);
		bool L_29 = VirtFuncInvoker0< bool >::Invoke(33 /* System.Boolean System.Type::get_IsValueType() */, L_28);
		if (!L_29)
		{
			goto IL_00fe;
		}
	}
	{
		return NULL;
	}

IL_00fe:
	{
		Type_t * L_30 = __this->get_expectedType_3();
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t1097883944_il2cpp_TypeInfo_var);
		Il2CppObject * L_31 = Convert_ChangeType_m2922880930(NULL /*static, unused*/, NULL, L_30, /*hidden argument*/NULL);
		return L_31;
	}
}
// System.Void EventDelegate/Parameter::set_value(System.Object)
extern "C"  void Parameter_set_value_m1967520784 (Parameter_t3958428553 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_mValue_2(L_0);
		return;
	}
}
// System.Type EventDelegate/Parameter::get_type()
extern const Il2CppType* Void_t2779279689_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern const uint32_t Parameter_get_type_m2662781659_MetadataUsageId;
extern "C"  Type_t * Parameter_get_type_m2662781659 (Parameter_t3958428553 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Parameter_get_type_m2662781659_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_mValue_2();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_mValue_2();
		NullCheck(L_1);
		Type_t * L_2 = Object_GetType_m2022236990(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0017:
	{
		Object_t3878351788 * L_3 = __this->get_obj_0();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_5 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Void_t2779279689_0_0_0_var), /*hidden argument*/NULL);
		return L_5;
	}

IL_0033:
	{
		Object_t3878351788 * L_6 = __this->get_obj_0();
		NullCheck(L_6);
		Type_t * L_7 = Object_GetType_m2022236990(L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void EventManager::.ctor()
extern "C"  void EventManager__ctor_m3403204648 (EventManager_t1907836883 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void EventManager::Clear()
extern "C"  void EventManager_Clear_m809337939 (EventManager_t1907836883 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void EventManager::Update()
extern "C"  void EventManager_Update_m4146991077 (EventManager_t1907836883 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ExampleDragDropItem::.ctor()
extern Il2CppClass* UIDragDropItem_t2087865514_il2cpp_TypeInfo_var;
extern const uint32_t ExampleDragDropItem__ctor_m1883911435_MetadataUsageId;
extern "C"  void ExampleDragDropItem__ctor_m1883911435 (ExampleDragDropItem_t1551469152 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExampleDragDropItem__ctor_m1883911435_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIDragDropItem_t2087865514_il2cpp_TypeInfo_var);
		UIDragDropItem__ctor_m3493732145(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExampleDragDropItem::OnDragDropRelease(UnityEngine.GameObject)
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisExampleDragDropSurface_t2000059968_m366110237_MethodInfo_var;
extern const uint32_t ExampleDragDropItem_OnDragDropRelease_m34599494_MetadataUsageId;
extern "C"  void ExampleDragDropItem_OnDragDropRelease_m34599494 (ExampleDragDropItem_t1551469152 * __this, GameObject_t4012695102 * ___surface0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ExampleDragDropItem_OnDragDropRelease_m34599494_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ExampleDragDropSurface_t2000059968 * V_0 = NULL;
	GameObject_t4012695102 * V_1 = NULL;
	Transform_t284553113 * V_2 = NULL;
	{
		GameObject_t4012695102 * L_0 = ___surface0;
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_009e;
		}
	}
	{
		GameObject_t4012695102 * L_2 = ___surface0;
		NullCheck(L_2);
		ExampleDragDropSurface_t2000059968 * L_3 = GameObject_GetComponent_TisExampleDragDropSurface_t2000059968_m366110237(L_2, /*hidden argument*/GameObject_GetComponent_TisExampleDragDropSurface_t2000059968_m366110237_MethodInfo_var);
		V_0 = L_3;
		ExampleDragDropSurface_t2000059968 * L_4 = V_0;
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_009e;
		}
	}
	{
		ExampleDragDropSurface_t2000059968 * L_6 = V_0;
		NullCheck(L_6);
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(L_6, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_8 = __this->get_prefab_20();
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		GameObject_t4012695102 * L_9 = NGUITools_AddChild_m4238455353(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		GameObject_t4012695102 * L_10 = V_1;
		NullCheck(L_10);
		Transform_t284553113 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		ExampleDragDropSurface_t2000059968 * L_12 = V_0;
		NullCheck(L_12);
		Transform_t284553113 * L_13 = Component_get_transform_m4257140443(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t3525329789  L_14 = Transform_get_localScale_m3886572677(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_localScale_m310756934(L_11, L_14, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_15 = V_1;
		NullCheck(L_15);
		Transform_t284553113 * L_16 = GameObject_get_transform_m1278640159(L_15, /*hidden argument*/NULL);
		V_2 = L_16;
		Transform_t284553113 * L_17 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		Vector3_t3525329789  L_18 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_lastWorldPosition_43();
		NullCheck(L_17);
		Transform_set_position_m3111394108(L_17, L_18, /*hidden argument*/NULL);
		ExampleDragDropSurface_t2000059968 * L_19 = V_0;
		NullCheck(L_19);
		bool L_20 = L_19->get_rotatePlacedObject_2();
		if (!L_20)
		{
			goto IL_0092;
		}
	}
	{
		Transform_t284553113 * L_21 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		Vector3_t3525329789  L_22 = RaycastHit_get_normal_m1346998891((((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_address_of_lastHit_44()), /*hidden argument*/NULL);
		Quaternion_t1891715979  L_23 = Quaternion_LookRotation_m1257501645(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_24 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t1891715979  L_25 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		Transform_set_rotation_m1525803229(L_21, L_25, /*hidden argument*/NULL);
	}

IL_0092:
	{
		GameObject_t4012695102 * L_26 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_Destroy_m2062477823(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
		return;
	}

IL_009e:
	{
		GameObject_t4012695102 * L_27 = ___surface0;
		UIDragDropItem_OnDragDropRelease_m146133740(__this, L_27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ExampleDragDropSurface::.ctor()
extern "C"  void ExampleDragDropSurface__ctor_m479182427 (ExampleDragDropSurface_t2000059968 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FeverLights::.ctor()
extern "C"  void FeverLights__ctor_m1266229994 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FeverLights::Awake()
extern "C"  void FeverLights_Awake_m1503835213 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FeverLights::OnDestroy()
extern "C"  void FeverLights_OnDestroy_m1657500387 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FeverLights::Init()
extern Il2CppCodeGenString* _stringLiteral4290488102;
extern const uint32_t FeverLights_Init_m269477418_MetadataUsageId;
extern "C"  void FeverLights_Init_m269477418 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FeverLights_Init_m269477418_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_enable_4((bool)1);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral4290488102, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FeverLights::Clear()
extern Il2CppCodeGenString* _stringLiteral4290488102;
extern const uint32_t FeverLights_Clear_m2967330581_MetadataUsageId;
extern "C"  void FeverLights_Clear_m2967330581 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FeverLights_Clear_m2967330581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_enable_4((bool)0);
		MonoBehaviour_StopCoroutine_m2790918991(__this, _stringLiteral4290488102, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator FeverLights::ChangeLight()
extern Il2CppClass* U3CChangeLightU3Ec__Iterator6_t2129571673_il2cpp_TypeInfo_var;
extern const uint32_t FeverLights_ChangeLight_m2739986646_MetadataUsageId;
extern "C"  Il2CppObject * FeverLights_ChangeLight_m2739986646 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FeverLights_ChangeLight_m2739986646_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CChangeLightU3Ec__Iterator6_t2129571673 * V_0 = NULL;
	{
		U3CChangeLightU3Ec__Iterator6_t2129571673 * L_0 = (U3CChangeLightU3Ec__Iterator6_t2129571673 *)il2cpp_codegen_object_new(U3CChangeLightU3Ec__Iterator6_t2129571673_il2cpp_TypeInfo_var);
		U3CChangeLightU3Ec__Iterator6__ctor_m4196669792(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CChangeLightU3Ec__Iterator6_t2129571673 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CChangeLightU3Ec__Iterator6_t2129571673 * L_2 = V_0;
		return L_2;
	}
}
// System.Void FeverLights::SetIdle()
extern Il2CppClass* UpdateFunction_t1249117153_il2cpp_TypeInfo_var;
extern const MethodInfo* FeverLights_UpdateIdle_m2390725463_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4290488102;
extern const uint32_t FeverLights_SetIdle_m1306842142_MetadataUsageId;
extern "C"  void FeverLights_SetIdle_m1306842142 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FeverLights_SetIdle_m1306842142_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_seq_5(0);
		__this->set_m_max_seq_6(((int32_t)2147483647LL));
		__this->set_m_time_3((0.5f));
		IntPtr_t L_0;
		L_0.set_m_value_0((void*)(void*)FeverLights_UpdateIdle_m2390725463_MethodInfo_var);
		UpdateFunction_t1249117153 * L_1 = (UpdateFunction_t1249117153 *)il2cpp_codegen_object_new(UpdateFunction_t1249117153_il2cpp_TypeInfo_var);
		UpdateFunction__ctor_m3574849846(L_1, __this, L_0, /*hidden argument*/NULL);
		__this->set_m_updateFunc_7(L_1);
		MonoBehaviour_StopCoroutine_m2790918991(__this, _stringLiteral4290488102, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral4290488102, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FeverLights::SetFlicker(System.Int32)
extern Il2CppClass* UpdateFunction_t1249117153_il2cpp_TypeInfo_var;
extern const MethodInfo* FeverLights_UpdateFlicker_m2483547703_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4290488102;
extern const uint32_t FeverLights_SetFlicker_m2686271841_MetadataUsageId;
extern "C"  void FeverLights_SetFlicker_m2686271841 (FeverLights_t2165513633 * __this, int32_t ___times0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FeverLights_SetFlicker_m2686271841_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_m_seq_5(0);
		int32_t L_0 = ___times0;
		__this->set_m_max_seq_6(((int32_t)((int32_t)L_0*(int32_t)2)));
		__this->set_m_time_3((0.1f));
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)FeverLights_UpdateFlicker_m2483547703_MethodInfo_var);
		UpdateFunction_t1249117153 * L_2 = (UpdateFunction_t1249117153 *)il2cpp_codegen_object_new(UpdateFunction_t1249117153_il2cpp_TypeInfo_var);
		UpdateFunction__ctor_m3574849846(L_2, __this, L_1, /*hidden argument*/NULL);
		__this->set_m_updateFunc_7(L_2);
		MonoBehaviour_StopCoroutine_m2790918991(__this, _stringLiteral4290488102, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2272783641(__this, _stringLiteral4290488102, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FeverLights::UpdateFlicker()
extern "C"  void FeverLights_UpdateFlicker_m2483547703 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_m_seq_5();
		V_0 = (bool)((((int32_t)((int32_t)((int32_t)L_0%(int32_t)2))) == ((int32_t)0))? 1 : 0);
		V_1 = 0;
		goto IL_0029;
	}

IL_0013:
	{
		List_1_t1458396018 * L_1 = __this->get_lights_2();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		UISprite_t661437049 * L_3 = VirtFuncInvoker1< UISprite_t661437049 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UISprite>::get_Item(System.Int32) */, L_1, L_2);
		bool L_4 = V_0;
		NullCheck(L_3);
		Behaviour_set_enabled_m2046806933(L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_6 = V_1;
		List_1_t1458396018 * L_7 = __this->get_lights_2();
		NullCheck(L_7);
		int32_t L_8 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UISprite>::get_Count() */, L_7);
		if ((((int32_t)L_6) < ((int32_t)L_8)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void FeverLights::UpdateIdle()
extern "C"  void FeverLights_UpdateIdle_m2390725463 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_m_seq_5();
		V_0 = (bool)((((int32_t)((int32_t)((int32_t)L_0%(int32_t)2))) == ((int32_t)0))? 1 : 0);
		V_1 = 0;
		goto IL_004b;
	}

IL_0013:
	{
		int32_t L_1 = V_1;
		if (((int32_t)((int32_t)L_1%(int32_t)2)))
		{
			goto IL_0032;
		}
	}
	{
		List_1_t1458396018 * L_2 = __this->get_lights_2();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		UISprite_t661437049 * L_4 = VirtFuncInvoker1< UISprite_t661437049 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UISprite>::get_Item(System.Int32) */, L_2, L_3);
		bool L_5 = V_0;
		NullCheck(L_4);
		Behaviour_set_enabled_m2046806933(L_4, L_5, /*hidden argument*/NULL);
		goto IL_0047;
	}

IL_0032:
	{
		List_1_t1458396018 * L_6 = __this->get_lights_2();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		UISprite_t661437049 * L_8 = VirtFuncInvoker1< UISprite_t661437049 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UISprite>::get_Item(System.Int32) */, L_6, L_7);
		bool L_9 = V_0;
		NullCheck(L_8);
		Behaviour_set_enabled_m2046806933(L_8, (bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0047:
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_004b:
	{
		int32_t L_11 = V_1;
		List_1_t1458396018 * L_12 = __this->get_lights_2();
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UISprite>::get_Count() */, L_12);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void FeverLights::UpdateRotate()
extern "C"  void FeverLights_UpdateRotate_m3880325790 (FeverLights_t2165513633 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_m_seq_5();
		V_0 = ((int32_t)((int32_t)L_0%(int32_t)4));
		V_1 = 0;
		goto IL_0051;
	}

IL_0010:
	{
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_1%(int32_t)4))) == ((int32_t)L_2)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_3 = V_1;
		int32_t L_4 = V_0;
		if ((!(((uint32_t)((int32_t)((int32_t)L_3%(int32_t)4))) == ((uint32_t)((int32_t)((int32_t)L_4+(int32_t)1))))))
		{
			goto IL_003b;
		}
	}

IL_0024:
	{
		List_1_t1458396018 * L_5 = __this->get_lights_2();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		UISprite_t661437049 * L_7 = VirtFuncInvoker1< UISprite_t661437049 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UISprite>::get_Item(System.Int32) */, L_5, L_6);
		NullCheck(L_7);
		Behaviour_set_enabled_m2046806933(L_7, (bool)1, /*hidden argument*/NULL);
		goto IL_004d;
	}

IL_003b:
	{
		List_1_t1458396018 * L_8 = __this->get_lights_2();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		UISprite_t661437049 * L_10 = VirtFuncInvoker1< UISprite_t661437049 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UISprite>::get_Item(System.Int32) */, L_8, L_9);
		NullCheck(L_10);
		Behaviour_set_enabled_m2046806933(L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_004d:
	{
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0051:
	{
		int32_t L_12 = V_1;
		List_1_t1458396018 * L_13 = __this->get_lights_2();
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UISprite>::get_Count() */, L_13);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// System.Void FeverLights/<ChangeLight>c__Iterator6::.ctor()
extern "C"  void U3CChangeLightU3Ec__Iterator6__ctor_m4196669792 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object FeverLights/<ChangeLight>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CChangeLightU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4109466034 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object FeverLights/<ChangeLight>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CChangeLightU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1004609862 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean FeverLights/<ChangeLight>c__Iterator6::MoveNext()
extern Il2CppClass* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern const uint32_t U3CChangeLightU3Ec__Iterator6_MoveNext_m2415804308_MetadataUsageId;
extern "C"  bool U3CChangeLightU3Ec__Iterator6_MoveNext_m2415804308 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeLightU3Ec__Iterator6_MoveNext_m2415804308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_00a5;
		}
		if (L_1 == 2)
		{
			goto IL_00c8;
		}
	}
	{
		goto IL_00cf;
	}

IL_0025:
	{
		goto IL_00a5;
	}

IL_002a:
	{
		FeverLights_t2165513633 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		int32_t L_3 = L_2->get_m_seq_5();
		FeverLights_t2165513633 * L_4 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_m_max_seq_6();
		if ((((int32_t)L_3) <= ((int32_t)L_5)))
		{
			goto IL_0050;
		}
	}
	{
		FeverLights_t2165513633 * L_6 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_6);
		FeverLights_SetIdle_m1306842142(L_6, /*hidden argument*/NULL);
	}

IL_0050:
	{
		FeverLights_t2165513633 * L_7 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_7);
		UpdateFunction_t1249117153 * L_8 = L_7->get_m_updateFunc_7();
		if (!L_8)
		{
			goto IL_0070;
		}
	}
	{
		FeverLights_t2165513633 * L_9 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_9);
		UpdateFunction_t1249117153 * L_10 = L_9->get_m_updateFunc_7();
		NullCheck(L_10);
		UpdateFunction_Invoke_m1968219408(L_10, /*hidden argument*/NULL);
	}

IL_0070:
	{
		FeverLights_t2165513633 * L_11 = __this->get_U3CU3Ef__this_2();
		FeverLights_t2165513633 * L_12 = L_11;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_m_seq_5();
		NullCheck(L_12);
		L_12->set_m_seq_5(((int32_t)((int32_t)L_13+(int32_t)1)));
		FeverLights_t2165513633 * L_14 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_14);
		float L_15 = L_14->get_m_time_3();
		WaitForSeconds_t1291133240 * L_16 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_16, L_15, /*hidden argument*/NULL);
		__this->set_U24current_1(L_16);
		__this->set_U24PC_0(1);
		goto IL_00d1;
	}

IL_00a5:
	{
		FeverLights_t2165513633 * L_17 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_17);
		bool L_18 = L_17->get_m_enable_4();
		if (L_18)
		{
			goto IL_002a;
		}
	}
	{
		__this->set_U24current_1(NULL);
		__this->set_U24PC_0(2);
		goto IL_00d1;
	}

IL_00c8:
	{
		__this->set_U24PC_0((-1));
	}

IL_00cf:
	{
		return (bool)0;
	}

IL_00d1:
	{
		return (bool)1;
	}
	// Dead block : IL_00d3: ldloc.1
}
// System.Void FeverLights/<ChangeLight>c__Iterator6::Dispose()
extern "C"  void U3CChangeLightU3Ec__Iterator6_Dispose_m4218629533 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void FeverLights/<ChangeLight>c__Iterator6::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CChangeLightU3Ec__Iterator6_Reset_m1843102733_MetadataUsageId;
extern "C"  void U3CChangeLightU3Ec__Iterator6_Reset_m1843102733 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CChangeLightU3Ec__Iterator6_Reset_m1843102733_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void FeverLights/UpdateFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateFunction__ctor_m3574849846 (UpdateFunction_t1249117153 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void FeverLights/UpdateFunction::Invoke()
extern "C"  void UpdateFunction_Invoke_m1968219408 (UpdateFunction_t1249117153 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UpdateFunction_Invoke_m1968219408((UpdateFunction_t1249117153 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_UpdateFunction_t1249117153(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult FeverLights/UpdateFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * UpdateFunction_BeginInvoke_m3437820659 (UpdateFunction_t1249117153 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void FeverLights/UpdateFunction::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateFunction_EndInvoke_m1959026246 (UpdateFunction_t1249117153 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void GameEvent::.ctor(System.Single)
extern "C"  void GameEvent__ctor_m1175921096 (GameEvent_t2981166504 * __this, float ___prob0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		float L_0 = ___prob0;
		__this->set_m_prob_0(L_0);
		float L_1 = __this->get_m_prob_0();
		__this->set_m_event_1(L_1);
		return;
	}
}
// System.Boolean GameEvent::IsTrigger()
extern "C"  bool GameEvent_IsTrigger_m1587815235 (GameEvent_t2981166504 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_m_event_1();
		float L_1 = __this->get_m_prob_0();
		__this->set_m_event_1(((float)((float)L_0+(float)L_1)));
		float L_2 = __this->get_m_event_1();
		if ((!(((float)L_2) >= ((float)(1.0f)))))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_3 = Random_Range_m75452833(NULL /*static, unused*/, 0, 1, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003c;
		}
	}
	{
		__this->set_m_event_1((0.0f));
		return (bool)1;
	}

IL_003c:
	{
		return (bool)0;
	}
}
// System.Void GamePlay::.ctor()
extern "C"  void GamePlay__ctor_m3893439221 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// Stage GamePlay::get_stage()
extern "C"  Stage_t80204510 * GamePlay_get_stage_m3096959283 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		Stage_t80204510 * L_0 = __this->get_m_stage_4();
		return L_0;
	}
}
// System.UInt32 GamePlay::get_axe_id()
extern "C"  uint32_t GamePlay_get_axe_id_m4284588703 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		AxeSlot_t1089550860 * L_0 = __this->get_m_axeSlot_5();
		NullCheck(L_0);
		uint32_t L_1 = AxeSlot_get_id_m1552903246(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// DataAxe GamePlay::get_dataAxe()
extern "C"  DataAxe_t3107820260 * GamePlay_get_dataAxe_m2446895999 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		AxeSlot_t1089550860 * L_0 = __this->get_m_axeSlot_5();
		NullCheck(L_0);
		DataAxe_t3107820260 * L_1 = AxeSlot_get_data_m3596026761(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// DataChapter GamePlay::get_dataChapter()
extern "C"  DataChapter_t925677859 * GamePlay_get_dataChapter_m1400870973 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		Stage_t80204510 * L_0 = __this->get_m_stage_4();
		NullCheck(L_0);
		DataChapter_t925677859 * L_1 = Stage_get_dataChapter_m2108362549(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// DataStage GamePlay::get_dataStage()
extern "C"  DataStage_t1629502804 * GamePlay_get_dataStage_m2883595423 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		Stage_t80204510 * L_0 = __this->get_m_stage_4();
		NullCheck(L_0);
		DataStage_t1629502804 * L_1 = Stage_get_dataStage_m3617598165(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// GameUI GamePlay::get_ui()
extern "C"  GameUI_t2125598246 * GamePlay_get_ui_m982624707 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		GameUI_t2125598246 * L_0 = __this->get_m_ui_8();
		return L_0;
	}
}
// PatternManager GamePlay::get_patternManager()
extern "C"  PatternManager_t2286577181 * GamePlay_get_patternManager_m3261677077 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		PatternManager_t2286577181 * L_0 = __this->get_m_patternManager_6();
		return L_0;
	}
}
// Mode GamePlay::get_mode()
extern "C"  Mode_t2403781 * GamePlay_get_mode_m3347757909 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		Mode_t2403781 * L_0 = __this->get_m_currentMode_9();
		return L_0;
	}
}
// GameEvent GamePlay::get_eventFrog()
extern "C"  GameEvent_t2981166504 * GamePlay_get_eventFrog_m3172212061 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		GameEvent_t2981166504 * L_0 = __this->get_m_frog_event_12();
		return L_0;
	}
}
// System.Void GamePlay::Awake()
extern Il2CppCodeGenString* _stringLiteral2122698;
extern Il2CppCodeGenString* _stringLiteral2647333352;
extern const uint32_t GamePlay_Awake_m4131044440_MetadataUsageId;
extern "C"  void GamePlay_Awake_m4131044440 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GamePlay_Awake_m4131044440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2122698, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t4012695102 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, _stringLiteral2647333352, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		GamePlay_Init_m631320767(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GamePlay::OnDestroy()
extern "C"  void GamePlay_OnDestroy_m1689943406 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		AxeSlot_t1089550860 * L_0 = __this->get_m_axeSlot_5();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		AxeSlot_t1089550860 * L_1 = __this->get_m_axeSlot_5();
		NullCheck(L_1);
		AxeSlot_Clear_m1825521418(L_1, /*hidden argument*/NULL);
		__this->set_m_axeSlot_5((AxeSlot_t1089550860 *)NULL);
	}

IL_001d:
	{
		PatternManager_t2286577181 * L_2 = __this->get_m_patternManager_6();
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		PatternManager_t2286577181 * L_3 = __this->get_m_patternManager_6();
		NullCheck(L_3);
		PatternManager_Clear_m3855921993(L_3, /*hidden argument*/NULL);
		__this->set_m_patternManager_6((PatternManager_t2286577181 *)NULL);
	}

IL_003a:
	{
		EventManager_t1907836883 * L_4 = __this->get_m_eventManager_7();
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		EventManager_t1907836883 * L_5 = __this->get_m_eventManager_7();
		NullCheck(L_5);
		EventManager_Clear_m809337939(L_5, /*hidden argument*/NULL);
		__this->set_m_eventManager_7((EventManager_t1907836883 *)NULL);
	}

IL_0057:
	{
		__this->set_m_frog_event_12((GameEvent_t2981166504 *)NULL);
		return;
	}
}
// System.Void GamePlay::Init()
extern const Il2CppType* GameUI_t2125598246_0_0_0_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GameUI_t2125598246_il2cpp_TypeInfo_var;
extern Il2CppClass* AxeSlot_t1089550860_il2cpp_TypeInfo_var;
extern Il2CppClass* PatternManager_t2286577181_il2cpp_TypeInfo_var;
extern Il2CppClass* EventManager_t1907836883_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t799362748_il2cpp_TypeInfo_var;
extern Il2CppClass* GameEvent_t2981166504_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisStage_t80204510_m2133437518_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisGameTouch_t2994825805_m3778956287_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m4022751406_MethodInfo_var;
extern const uint32_t GamePlay_Init_m631320767_MetadataUsageId;
extern "C"  void GamePlay_Init_m631320767 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GamePlay_Init_m631320767_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Mode_t2403781 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->set_instance_2(__this);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Stage_t80204510 * L_1 = GameObject_AddComponent_TisStage_t80204510_m2133437518(L_0, /*hidden argument*/GameObject_AddComponent_TisStage_t80204510_m2133437518_MethodInfo_var);
		__this->set_m_stage_4(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(GameUI_t2125598246_0_0_0_var), /*hidden argument*/NULL);
		Component_t2126946602 * L_3 = Component_GetComponentInParent_m1953645192(__this, L_2, /*hidden argument*/NULL);
		__this->set_m_ui_8(((GameUI_t2125598246 *)IsInstClass(L_3, GameUI_t2125598246_il2cpp_TypeInfo_var)));
		AxeSlot_t1089550860 * L_4 = (AxeSlot_t1089550860 *)il2cpp_codegen_object_new(AxeSlot_t1089550860_il2cpp_TypeInfo_var);
		AxeSlot__ctor_m124420831(L_4, /*hidden argument*/NULL);
		__this->set_m_axeSlot_5(L_4);
		PatternManager_t2286577181 * L_5 = (PatternManager_t2286577181 *)il2cpp_codegen_object_new(PatternManager_t2286577181_il2cpp_TypeInfo_var);
		PatternManager__ctor_m2154821406(L_5, /*hidden argument*/NULL);
		__this->set_m_patternManager_6(L_5);
		EventManager_t1907836883 * L_6 = (EventManager_t1907836883 *)il2cpp_codegen_object_new(EventManager_t1907836883_il2cpp_TypeInfo_var);
		EventManager__ctor_m3403204648(L_6, /*hidden argument*/NULL);
		__this->set_m_eventManager_7(L_6);
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameTouch_t2994825805 * L_8 = GameObject_AddComponent_TisGameTouch_t2994825805_m3778956287(L_7, /*hidden argument*/GameObject_AddComponent_TisGameTouch_t2994825805_m3778956287_MethodInfo_var);
		__this->set_m_touch_11(L_8);
		List_1_t799362748 * L_9 = (List_1_t799362748 *)il2cpp_codegen_object_new(List_1_t799362748_il2cpp_TypeInfo_var);
		List_1__ctor_m4022751406(L_9, /*hidden argument*/List_1__ctor_m4022751406_MethodInfo_var);
		__this->set_m_modes_10(L_9);
		V_1 = 0;
		goto IL_008d;
	}

IL_0076:
	{
		int32_t L_10 = V_1;
		Mode_t2403781 * L_11 = Mode_Alloc_m965895771(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		List_1_t799362748 * L_12 = __this->get_m_modes_10();
		Mode_t2403781 * L_13 = V_0;
		NullCheck(L_12);
		VirtActionInvoker1< Mode_t2403781 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<Mode>::Add(!0) */, L_12, L_13);
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_008d:
	{
		int32_t L_15 = V_1;
		if ((((int32_t)L_15) < ((int32_t)3)))
		{
			goto IL_0076;
		}
	}
	{
		DataUser_t1853738677 * L_16 = Data_get_user_m2747519663(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = DataUser_get_chapter_m1684683802(L_16, /*hidden argument*/NULL);
		GamePlay_SetChapter_m1737945675(__this, L_17, /*hidden argument*/NULL);
		DataUser_t1853738677 * L_18 = Data_get_user_m2747519663(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = DataUser_get_stage_m1852353931(L_18, /*hidden argument*/NULL);
		GamePlay_SetStage_m3602790972(__this, L_19, /*hidden argument*/NULL);
		GamePlay_SetAxe_m2967815263(__this, 0, /*hidden argument*/NULL);
		GamePlay_SetMode_m2394895988(__this, 0, /*hidden argument*/NULL);
		GameEvent_t2981166504 * L_20 = (GameEvent_t2981166504 *)il2cpp_codegen_object_new(GameEvent_t2981166504_il2cpp_TypeInfo_var);
		GameEvent__ctor_m1175921096(L_20, (0.05f), /*hidden argument*/NULL);
		__this->set_m_frog_event_12(L_20);
		Stage_t80204510 * L_21 = GamePlay_get_stage_m3096959283(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Stage_InitCamera_m2386906604(L_21, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_22 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_22);
		GameUI_t2125598246 * L_23 = GamePlay_get_ui_m982624707(L_22, /*hidden argument*/NULL);
		DataUser_t1853738677 * L_24 = Data_get_user_m2747519663(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		int32_t L_25 = DataUser_get_coin_m2506806470(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		GameUI_SetCoin_m1898449783(L_23, L_25, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GamePlay::SetChapter(System.Int32)
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t GamePlay_SetChapter_m1737945675_MetadataUsageId;
extern "C"  void GamePlay_SetChapter_m1737945675 (GamePlay_t2590336614 * __this, int32_t ___chapter0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GamePlay_SetChapter_m1737945675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stage_t80204510 * L_0 = __this->get_m_stage_4();
		int32_t L_1 = ___chapter0;
		NullCheck(L_0);
		Stage_SetChapter_m714039999(L_0, L_1, /*hidden argument*/NULL);
		AxeSlot_t1089550860 * L_2 = __this->get_m_axeSlot_5();
		DataChapter_t925677859 * L_3 = GamePlay_get_dataChapter_m1400870973(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Rect_t1525428817 * L_4 = L_3->get_address_of_rect_2();
		float L_5 = Rect_get_height_m2154960823(L_4, /*hidden argument*/NULL);
		NullCheck(L_2);
		AxeSlot_CalculateVelocity_m475350539(L_2, L_5, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_6 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_6);
		GameUI_t2125598246 * L_7 = GamePlay_get_ui_m982624707(L_6, /*hidden argument*/NULL);
		int32_t L_8 = ___chapter0;
		DataUser_t1853738677 * L_9 = Data_get_user_m2747519663(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = DataUser_get_stage_m1852353931(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameUI_SetStage_m4145363547(L_7, L_8, L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GamePlay::SetStage(System.Int32)
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t GamePlay_SetStage_m3602790972_MetadataUsageId;
extern "C"  void GamePlay_SetStage_m3602790972 (GamePlay_t2590336614 * __this, int32_t ___stage0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GamePlay_SetStage_m3602790972_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stage_t80204510 * L_0 = __this->get_m_stage_4();
		int32_t L_1 = ___stage0;
		NullCheck(L_0);
		Stage_SetStage_m1916811184(L_0, L_1, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_2 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_2);
		GameUI_t2125598246 * L_3 = GamePlay_get_ui_m982624707(L_2, /*hidden argument*/NULL);
		DataUser_t1853738677 * L_4 = Data_get_user_m2747519663(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = DataUser_get_chapter_m1684683802(L_4, /*hidden argument*/NULL);
		int32_t L_6 = ___stage0;
		NullCheck(L_3);
		GameUI_SetStage_m4145363547(L_3, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GamePlay::SetAxe(System.UInt32)
extern "C"  void GamePlay_SetAxe_m2967815263 (GamePlay_t2590336614 * __this, uint32_t ___axe_id0, const MethodInfo* method)
{
	{
		AxeSlot_t1089550860 * L_0 = __this->get_m_axeSlot_5();
		uint32_t L_1 = ___axe_id0;
		NullCheck(L_0);
		AxeSlot_SetAxe_m301184637(L_0, L_1, /*hidden argument*/NULL);
		GameTouch_t2994825805 * L_2 = __this->get_m_touch_11();
		uint32_t L_3 = ___axe_id0;
		NullCheck(L_2);
		VirtActionInvoker1< uint32_t >::Invoke(7 /* System.Void GameTouch::SetControl(System.UInt32) */, L_2, L_3);
		return;
	}
}
// System.Void GamePlay::SetMode(Mode/TYPE)
extern "C"  void GamePlay_SetMode_m2394895988 (GamePlay_t2590336614 * __this, int32_t ___type0, const MethodInfo* method)
{
	{
		Mode_t2403781 * L_0 = __this->get_m_currentMode_9();
		if (!L_0)
		{
			goto IL_0028;
		}
	}
	{
		Mode_t2403781 * L_1 = __this->get_m_currentMode_9();
		NullCheck(L_1);
		int32_t L_2 = Mode_get_type_m1292918588(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___type0;
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_001d;
		}
	}
	{
		return;
	}

IL_001d:
	{
		Mode_t2403781 * L_4 = __this->get_m_currentMode_9();
		NullCheck(L_4);
		VirtActionInvoker0::Invoke(5 /* System.Void Mode::Clear() */, L_4);
	}

IL_0028:
	{
		List_1_t799362748 * L_5 = __this->get_m_modes_10();
		int32_t L_6 = ___type0;
		NullCheck(L_5);
		Mode_t2403781 * L_7 = VirtFuncInvoker1< Mode_t2403781 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<Mode>::get_Item(System.Int32) */, L_5, L_6);
		__this->set_m_currentMode_9(L_7);
		Mode_t2403781 * L_8 = __this->get_m_currentMode_9();
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(4 /* System.Void Mode::Init() */, L_8);
		return;
	}
}
// System.Void GamePlay::AddCoin(System.Int32)
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t GamePlay_AddCoin_m1214255030_MetadataUsageId;
extern "C"  void GamePlay_AddCoin_m1214255030 (GamePlay_t2590336614 * __this, int32_t ___coin0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GamePlay_AddCoin_m1214255030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		DataUser_t1853738677 * L_0 = Data_get_user_m2747519663(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataUser_t1853738677 * L_1 = L_0;
		NullCheck(L_1);
		int32_t L_2 = DataUser_get_coin_m2506806470(L_1, /*hidden argument*/NULL);
		int32_t L_3 = ___coin0;
		NullCheck(L_1);
		DataUser_set_coin_m154068605(L_1, ((int32_t)((int32_t)L_2+(int32_t)L_3)), /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_4 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_4);
		GameUI_t2125598246 * L_5 = GamePlay_get_ui_m982624707(L_4, /*hidden argument*/NULL);
		DataUser_t1853738677 * L_6 = Data_get_user_m2747519663(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = DataUser_get_coin_m2506806470(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameUI_SetCoin_m1898449783(L_5, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GamePlay::Update()
extern "C"  void GamePlay_Update_m2164393656 (GamePlay_t2590336614 * __this, const MethodInfo* method)
{
	{
		EventManager_t1907836883 * L_0 = __this->get_m_eventManager_7();
		NullCheck(L_0);
		EventManager_Update_m4146991077(L_0, /*hidden argument*/NULL);
		Mode_t2403781 * L_1 = __this->get_m_currentMode_9();
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		Mode_t2403781 * L_2 = __this->get_m_currentMode_9();
		float L_3 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< float >::Invoke(6 /* System.Void Mode::Update(System.Single) */, L_2, L_3);
	}

IL_0026:
	{
		return;
	}
}
// System.Void GamePlay::HitWater(Axe)
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t GamePlay_HitWater_m3876954609_MetadataUsageId;
extern "C"  void GamePlay_HitWater_m3876954609 (GamePlay_t2590336614 * __this, Axe_t66286 * ___axe0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GamePlay_HitWater_m3876954609_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Mode_t2403781 * L_0 = __this->get_m_currentMode_9();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(7 /* System.Void Mode::HitWater() */, L_0);
		Axe_t66286 * L_1 = ___axe0;
		NullCheck(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = (&V_1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_5 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_POS_EFFECT_Y_3();
		Axe_t66286 * L_6 = ___axe0;
		NullCheck(L_6);
		Transform_t284553113 * L_7 = Component_get_transform_m4257140443(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = (&V_2)->get_z_3();
		Vector3__ctor_m2926210380((&V_0), L_4, L_5, L_9, /*hidden argument*/NULL);
		Stage_t80204510 * L_10 = GamePlay_get_stage_m3096959283(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_11 = V_0;
		GamePlay_t2590336614 * L_12 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_12);
		Stage_t80204510 * L_13 = GamePlay_get_stage_m3096959283(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		DataChapter_t925677859 * L_14 = Stage_get_dataChapter_m2108362549(L_13, /*hidden argument*/NULL);
		NullCheck(L_14);
		String_t* L_15 = L_14->get_effect_drop_5();
		NullCheck(L_10);
		Stage_AddEffect_m3322959738(L_10, L_11, L_15, (bool)1, /*hidden argument*/NULL);
		Stage_t80204510 * L_16 = GamePlay_get_stage_m3096959283(__this, /*hidden argument*/NULL);
		Axe_t66286 * L_17 = ___axe0;
		NullCheck(L_17);
		Transform_t284553113 * L_18 = Component_get_transform_m4257140443(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t3525329789  L_19 = Transform_get_position_m2211398607(L_18, /*hidden argument*/NULL);
		float L_20 = Random_Range_m3362417303(NULL /*static, unused*/, (0.0f), (3.14159274f), /*hidden argument*/NULL);
		NullCheck(L_16);
		Stage_AddCoinEffect_m2181335947(L_16, 1, L_19, L_20, (2.0f), (12.0f), (40.0f), (0.0f), /*hidden argument*/NULL);
		GamePlay_AddCoin_m1214255030(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GamePlay::HitDiver(Axe,Diver)
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139_MethodInfo_var;
extern const uint32_t GamePlay_HitDiver_m148186040_MetadataUsageId;
extern "C"  void GamePlay_HitDiver_m148186040 (GamePlay_t2590336614 * __this, Axe_t66286 * ___axe0, Diver_t66044126 * ___diver1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GamePlay_HitDiver_m148186040_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	GameObject_t4012695102 * V_1 = NULL;
	EffectWave_t3709365322 * V_2 = NULL;
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Mode_t2403781 * L_0 = __this->get_m_currentMode_9();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(8 /* System.Void Mode::HitDiver() */, L_0);
		Axe_t66286 * L_1 = ___axe0;
		NullCheck(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		V_4 = L_3;
		float L_4 = (&V_4)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_5 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_POS_EFFECT_Y_3();
		Axe_t66286 * L_6 = ___axe0;
		NullCheck(L_6);
		Transform_t284553113 * L_7 = Component_get_transform_m4257140443(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		V_5 = L_8;
		float L_9 = (&V_5)->get_z_3();
		Vector3__ctor_m2926210380((&V_0), L_4, L_5, L_9, /*hidden argument*/NULL);
		Diver_t66044126 * L_10 = ___diver1;
		NullCheck(L_10);
		bool L_11 = Diver_get_IsMonster_m1684747526(L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_006c;
		}
	}
	{
		Diver_t66044126 * L_12 = ___diver1;
		NullCheck(L_12);
		Pattern_t873562992 * L_13 = Character_get_pattern_m1274480394(L_12, /*hidden argument*/NULL);
		Diver_t66044126 * L_14 = ___diver1;
		NullCheck(L_14);
		uint32_t L_15 = Diver_get_MonsterID_m3449010450(L_14, /*hidden argument*/NULL);
		Diver_t66044126 * L_16 = ___diver1;
		NullCheck(L_16);
		Transform_t284553113 * L_17 = Component_get_transform_m4257140443(L_16, /*hidden argument*/NULL);
		NullCheck(L_17);
		Vector3_t3525329789  L_18 = Transform_get_position_m2211398607(L_17, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtFuncInvoker2< Monster_t2901270458 *, uint32_t, Vector3_t3525329789  >::Invoke(7 /* Monster Pattern::AddMonster(System.UInt32,UnityEngine.Vector3) */, L_13, L_15, L_18);
		goto IL_016d;
	}

IL_006c:
	{
		Stage_t80204510 * L_19 = GamePlay_get_stage_m3096959283(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_20 = V_0;
		GamePlay_t2590336614 * L_21 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_21);
		Stage_t80204510 * L_22 = GamePlay_get_stage_m3096959283(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		DataChapter_t925677859 * L_23 = Stage_get_dataChapter_m2108362549(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24 = L_23->get_effect_drop_5();
		NullCheck(L_19);
		Stage_AddEffect_m3322959738(L_19, L_20, L_24, (bool)1, /*hidden argument*/NULL);
		Stage_t80204510 * L_25 = GamePlay_get_stage_m3096959283(__this, /*hidden argument*/NULL);
		Diver_t66044126 * L_26 = ___diver1;
		NullCheck(L_26);
		Transform_t284553113 * L_27 = Component_get_transform_m4257140443(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t3525329789  L_28 = Transform_get_position_m2211398607(L_27, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_29 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_29);
		DataChapter_t925677859 * L_30 = GamePlay_get_dataChapter_m1400870973(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		String_t* L_31 = L_30->get_effect_god_7();
		NullCheck(L_25);
		Stage_AddEffect_m3322959738(L_25, L_28, L_31, (bool)1, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_32 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_32);
		Stage_t80204510 * L_33 = GamePlay_get_stage_m3096959283(L_32, /*hidden argument*/NULL);
		Diver_t66044126 * L_34 = ___diver1;
		NullCheck(L_34);
		Transform_t284553113 * L_35 = Component_get_transform_m4257140443(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t3525329789  L_36 = Transform_get_position_m2211398607(L_35, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_37 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_37);
		DataChapter_t925677859 * L_38 = GamePlay_get_dataChapter_m1400870973(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		String_t* L_39 = L_38->get_effect_wave_6();
		NullCheck(L_33);
		GameObject_t4012695102 * L_40 = Stage_AddEffect_m3322959738(L_33, L_36, L_39, (bool)0, /*hidden argument*/NULL);
		V_1 = L_40;
		GameObject_t4012695102 * L_41 = V_1;
		NullCheck(L_41);
		EffectWave_t3709365322 * L_42 = GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139(L_41, /*hidden argument*/GameObject_GetComponent_TisEffectWave_t3709365322_m1324925139_MethodInfo_var);
		V_2 = L_42;
		EffectWave_t3709365322 * L_43 = V_2;
		bool L_44 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_43, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0103;
		}
	}
	{
		EffectWave_t3709365322 * L_45 = V_2;
		NullCheck(L_45);
		EffectWave_Init_m328236749(L_45, (2.0f), (4.0f), /*hidden argument*/NULL);
	}

IL_0103:
	{
		Diver_t66044126 * L_46 = ___diver1;
		NullCheck(L_46);
		Transform_t284553113 * L_47 = Component_get_transform_m4257140443(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		Vector3_t3525329789  L_48 = Transform_get_position_m2211398607(L_47, /*hidden argument*/NULL);
		V_3 = L_48;
		Vector3_t3525329789 * L_49 = (&V_3);
		float L_50 = L_49->get_z_3();
		L_49->set_z_3(((float)((float)L_50-(float)(0.2f))));
		(&V_3)->set_y_2((0.3f));
		GamePlay_t2590336614 * L_51 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_51);
		Stage_t80204510 * L_52 = GamePlay_get_stage_m3096959283(L_51, /*hidden argument*/NULL);
		Vector3_t3525329789  L_53 = V_3;
		float L_54 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_52);
		Stage_AddCoinEffect_m2181335947(L_52, ((int32_t)10), L_53, ((float)((float)((float)((float)L_54*(float)(2.0f)))*(float)(3.14159274f))), (2.0f), (16.0f), (40.0f), (0.15f), /*hidden argument*/NULL);
		GamePlay_AddCoin_m1214255030(__this, ((int32_t)10), /*hidden argument*/NULL);
	}

IL_016d:
	{
		return;
	}
}
// System.Void GamePlay::HitMonster(Monster,Axe)
extern "C"  void GamePlay_HitMonster_m4135026560 (GamePlay_t2590336614 * __this, Monster_t2901270458 * ___monster0, Axe_t66286 * ___axe1, const MethodInfo* method)
{
	{
		Mode_t2403781 * L_0 = __this->get_m_currentMode_9();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(9 /* System.Void Mode::HitMonster() */, L_0);
		return;
	}
}
// System.Void GamePlay::KillMonster(Monster,Axe)
extern "C"  void GamePlay_KillMonster_m2354964455 (GamePlay_t2590336614 * __this, Monster_t2901270458 * ___monster0, Axe_t66286 * ___axe1, const MethodInfo* method)
{
	{
		Mode_t2403781 * L_0 = __this->get_m_currentMode_9();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(10 /* System.Void Mode::KillMonster() */, L_0);
		return;
	}
}
// System.Void GameTouch::.ctor()
extern "C"  void GameTouch__ctor_m4003983806 (GameTouch_t2994825805 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameTouch::Awake()
extern "C"  void GameTouch_Awake_m4241589025 (GameTouch_t2994825805 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameTouch::OnDestroy()
extern "C"  void GameTouch_OnDestroy_m563001271 (GameTouch_t2994825805 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(6 /* System.Void GameTouch::RemoveControl() */, __this);
		return;
	}
}
// System.Void GameTouch::RemoveControl()
extern "C"  void GameTouch_RemoveControl_m3656731093 (GameTouch_t2994825805 * __this, const MethodInfo* method)
{
	{
		Control_t2616196413 * L_0 = __this->get_m_control_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		Control_t2616196413 * L_2 = __this->get_m_control_2();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001c:
	{
		__this->set_m_control_2((Control_t2616196413 *)NULL);
		return;
	}
}
// System.Void GameTouch::SetControl(System.UInt32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisTimeControl_t3323199920_m4267318908_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisTouchControl_t3773144382_m2573704436_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisAutoControl_t1228330126_m1876096734_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2575053;
extern Il2CppCodeGenString* _stringLiteral80013087;
extern const uint32_t GameTouch_SetControl_m2085859001_MetadataUsageId;
extern "C"  void GameTouch_SetControl_m2085859001 (GameTouch_t2994825805 * __this, uint32_t ___axe_id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameTouch_SetControl_m2085859001_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DataAxe_t3107820260 * V_0 = NULL;
	{
		VirtActionInvoker0::Invoke(6 /* System.Void GameTouch::RemoveControl() */, __this);
		DataAxes_t1853147663 * L_0 = Data_get_axe_m1914057446(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint32_t L_1 = ___axe_id0;
		NullCheck(L_0);
		DataAxe_t3107820260 * L_2 = DataAxes_getData_m1458414431(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		DataAxe_t3107820260 * L_3 = V_0;
		NullCheck(L_3);
		String_t* L_4 = L_3->get_control_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_4, _stringLiteral2575053, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003d;
		}
	}
	{
		GameObject_t4012695102 * L_6 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		TimeControl_t3323199920 * L_7 = GameObject_AddComponent_TisTimeControl_t3323199920_m4267318908(L_6, /*hidden argument*/GameObject_AddComponent_TisTimeControl_t3323199920_m4267318908_MethodInfo_var);
		__this->set_m_control_2(L_7);
		goto IL_0079;
	}

IL_003d:
	{
		DataAxe_t3107820260 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = L_8->get_control_6();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_9, _stringLiteral80013087, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0068;
		}
	}
	{
		GameObject_t4012695102 * L_11 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_11);
		TouchControl_t3773144382 * L_12 = GameObject_AddComponent_TisTouchControl_t3773144382_m2573704436(L_11, /*hidden argument*/GameObject_AddComponent_TisTouchControl_t3773144382_m2573704436_MethodInfo_var);
		__this->set_m_control_2(L_12);
		goto IL_0079;
	}

IL_0068:
	{
		GameObject_t4012695102 * L_13 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		AutoControl_t1228330126 * L_14 = GameObject_AddComponent_TisAutoControl_t1228330126_m1876096734(L_13, /*hidden argument*/GameObject_AddComponent_TisAutoControl_t1228330126_m1876096734_MethodInfo_var);
		__this->set_m_control_2(L_14);
	}

IL_0079:
	{
		return;
	}
}
// System.Void GameTouch::Update()
extern Il2CppClass* Input_t1593691127_il2cpp_TypeInfo_var;
extern const uint32_t GameTouch_Update_m1296308495_MetadataUsageId;
extern "C"  void GameTouch_Update_m1296308495 (GameTouch_t2994825805 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameTouch_Update_m1296308495_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Touch_t1603883884  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		V_0 = 0;
		goto IL_00cd;
	}

IL_0007:
	{
		int32_t L_0 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		Touch_t1603883884  L_1 = Input_GetTouch_m2282421092(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		int32_t L_2 = Touch_get_phase_m3314549414((&V_1), /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_3 = Touch_get_fingerId_m1427167959((&V_1), /*hidden argument*/NULL);
		Vector2_t3525329788  L_4 = Touch_get_position_m1943849441((&V_1), /*hidden argument*/NULL);
		Vector2_t3525329788  L_5 = Touch_get_deltaPosition_m3983677995((&V_1), /*hidden argument*/NULL);
		VirtActionInvoker3< int32_t, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(9 /* System.Void GameTouch::OnStartTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2) */, __this, L_3, L_4, L_5);
		goto IL_00c9;
	}

IL_003a:
	{
		int32_t L_6 = Touch_get_phase_m3314549414((&V_1), /*hidden argument*/NULL);
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_7 = Touch_get_phase_m3314549414((&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)2))))
		{
			goto IL_0074;
		}
	}

IL_0054:
	{
		int32_t L_8 = Touch_get_fingerId_m1427167959((&V_1), /*hidden argument*/NULL);
		Vector2_t3525329788  L_9 = Touch_get_position_m1943849441((&V_1), /*hidden argument*/NULL);
		Vector2_t3525329788  L_10 = Touch_get_deltaPosition_m3983677995((&V_1), /*hidden argument*/NULL);
		VirtActionInvoker3< int32_t, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(10 /* System.Void GameTouch::OnMovedTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2) */, __this, L_8, L_9, L_10);
		goto IL_00c9;
	}

IL_0074:
	{
		int32_t L_11 = Touch_get_phase_m3314549414((&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)3))))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_12 = Touch_get_fingerId_m1427167959((&V_1), /*hidden argument*/NULL);
		Vector2_t3525329788  L_13 = Touch_get_position_m1943849441((&V_1), /*hidden argument*/NULL);
		Vector2_t3525329788  L_14 = Touch_get_deltaPosition_m3983677995((&V_1), /*hidden argument*/NULL);
		VirtActionInvoker3< int32_t, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(11 /* System.Void GameTouch::OnEndTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2) */, __this, L_12, L_13, L_14);
		goto IL_00c9;
	}

IL_00a1:
	{
		int32_t L_15 = Touch_get_phase_m3314549414((&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_15) == ((uint32_t)4))))
		{
			goto IL_00c9;
		}
	}
	{
		int32_t L_16 = Touch_get_fingerId_m1427167959((&V_1), /*hidden argument*/NULL);
		Vector2_t3525329788  L_17 = Touch_get_position_m1943849441((&V_1), /*hidden argument*/NULL);
		Vector2_t3525329788  L_18 = Touch_get_deltaPosition_m3983677995((&V_1), /*hidden argument*/NULL);
		VirtActionInvoker3< int32_t, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(12 /* System.Void GameTouch::OnCanceldTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2) */, __this, L_16, L_17, L_18);
	}

IL_00c9:
	{
		int32_t L_19 = V_0;
		V_0 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_00cd:
	{
		int32_t L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1593691127_il2cpp_TypeInfo_var);
		int32_t L_21 = Input_get_touchCount_m1430909390(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GameTouch::OnStartTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void GameTouch_OnStartTouch_m911723323 (GameTouch_t2994825805 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		DataSetting_t2172603174 * L_1 = Data_get_settings_m2852704140(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = L_1->get_TOUCH_COUNT_0();
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Control_t2616196413 * L_3 = __this->get_m_control_2();
		int32_t L_4 = ___id0;
		Vector2_t3525329788  L_5 = ___position1;
		Vector2_t3525329788  L_6 = ___deltaPosition2;
		NullCheck(L_3);
		VirtActionInvoker3< int32_t, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(7 /* System.Void Control::StartTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2) */, L_3, L_4, L_5, L_6);
		return;
	}
}
// System.Void GameTouch::OnMovedTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void GameTouch_OnMovedTouch_m3475860042 (GameTouch_t2994825805 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		DataSetting_t2172603174 * L_1 = Data_get_settings_m2852704140(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = L_1->get_TOUCH_COUNT_0();
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Control_t2616196413 * L_3 = __this->get_m_control_2();
		int32_t L_4 = ___id0;
		Vector2_t3525329788  L_5 = ___position1;
		Vector2_t3525329788  L_6 = ___deltaPosition2;
		NullCheck(L_3);
		VirtActionInvoker3< int32_t, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(8 /* System.Void Control::MovedTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2) */, L_3, L_4, L_5, L_6);
		return;
	}
}
// System.Void GameTouch::OnEndTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void GameTouch_OnEndTouch_m3751389218 (GameTouch_t2994825805 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		DataSetting_t2172603174 * L_1 = Data_get_settings_m2852704140(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = L_1->get_TOUCH_COUNT_0();
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Control_t2616196413 * L_3 = __this->get_m_control_2();
		int32_t L_4 = ___id0;
		Vector2_t3525329788  L_5 = ___position1;
		Vector2_t3525329788  L_6 = ___deltaPosition2;
		NullCheck(L_3);
		VirtActionInvoker3< int32_t, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(9 /* System.Void Control::EndTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2) */, L_3, L_4, L_5, L_6);
		return;
	}
}
// System.Void GameTouch::OnCanceldTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void GameTouch_OnCanceldTouch_m1121489011 (GameTouch_t2994825805 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method)
{
	{
		int32_t L_0 = ___id0;
		DataSetting_t2172603174 * L_1 = Data_get_settings_m2852704140(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = L_1->get_TOUCH_COUNT_0();
		if ((((int32_t)L_0) < ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		return;
	}

IL_0011:
	{
		Control_t2616196413 * L_3 = __this->get_m_control_2();
		int32_t L_4 = ___id0;
		Vector2_t3525329788  L_5 = ___position1;
		Vector2_t3525329788  L_6 = ___deltaPosition2;
		NullCheck(L_3);
		VirtActionInvoker3< int32_t, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(10 /* System.Void Control::CanceldTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2) */, L_3, L_4, L_5, L_6);
		return;
	}
}
// System.Void GameUI::.ctor()
extern "C"  void GameUI__ctor_m2095833397 (GameUI_t2125598246 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameUI::Awake()
extern "C"  void GameUI_Awake_m2333438616 (GameUI_t2125598246 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameUI::SetNormal()
extern "C"  void GameUI_SetNormal_m1645742844 (GameUI_t2125598246 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = __this->get_feverNode_5();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)0, /*hidden argument*/NULL);
		FeverLights_t2165513633 * L_1 = __this->get_feverLights_4();
		NullCheck(L_1);
		FeverLights_Clear_m2967330581(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameUI::SetFever()
extern "C"  void GameUI_SetFever_m705982769 (GameUI_t2125598246 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = __this->get_feverNode_5();
		NullCheck(L_0);
		GameObject_SetActive_m3538205401(L_0, (bool)1, /*hidden argument*/NULL);
		FeverLights_t2165513633 * L_1 = __this->get_feverLights_4();
		NullCheck(L_1);
		FeverLights_Init_m269477418(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameUI::SetEvent()
extern "C"  void GameUI_SetEvent_m289744839 (GameUI_t2125598246 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameUI::SetBoss()
extern "C"  void GameUI_SetBoss_m3796739330 (GameUI_t2125598246 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameUI::SetCoin(System.Int32)
extern "C"  void GameUI_SetCoin_m1898449783 (GameUI_t2125598246 * __this, int32_t ___coin0, const MethodInfo* method)
{
	{
		UILabel_t291504320 * L_0 = __this->get_labelCoin_6();
		String_t* L_1 = Int32_ToString_m1286526384((&___coin0), /*hidden argument*/NULL);
		NullCheck(L_0);
		UILabel_set_text_m4037075551(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameUI::SetStage(System.Int32,System.Int32)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral45;
extern const uint32_t GameUI_SetStage_m4145363547_MetadataUsageId;
extern "C"  void GameUI_SetStage_m4145363547 (GameUI_t2125598246 * __this, int32_t ___chapter0, int32_t ___stage1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameUI_SetStage_m4145363547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UILabel_t291504320 * L_0 = __this->get_labelStage_7();
		String_t* L_1 = Int32_ToString_m1286526384((&___chapter0), /*hidden argument*/NULL);
		String_t* L_2 = Int32_ToString_m1286526384((&___stage1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m1825781833(NULL /*static, unused*/, L_1, _stringLiteral45, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		UILabel_set_text_m4037075551(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameUI::SetScore(System.Int32)
extern "C"  void GameUI_SetScore_m3619087504 (GameUI_t2125598246 * __this, int32_t ___score0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameUI::SetPower(System.Single)
extern "C"  void GameUI_SetPower_m2496519033 (GameUI_t2125598246 * __this, float ___power0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameUI::SetDistance(System.Single)
extern "C"  void GameUI_SetDistance_m919038049 (GameUI_t2125598246 * __this, float ___dist0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameUI::Update()
extern "C"  void GameUI_Update_m2273187960 (GameUI_t2125598246 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GoldMonster::.ctor()
extern "C"  void GoldMonster__ctor_m846045553 (GoldMonster_t84400954 * __this, const MethodInfo* method)
{
	{
		Monster__ctor_m3066192113(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoldMonster::Awake()
extern "C"  void GoldMonster_Awake_m1083650772 (GoldMonster_t84400954 * __this, const MethodInfo* method)
{
	{
		Monster_Awake_m3303797332(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoldMonster::OnDestroy()
extern "C"  void GoldMonster_OnDestroy_m2797557226 (GoldMonster_t84400954 * __this, const MethodInfo* method)
{
	{
		Monster_OnDestroy_m1806194026(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoldMonster::Update()
extern "C"  void GoldMonster_Update_m2184470460 (GoldMonster_t84400954 * __this, const MethodInfo* method)
{
	{
		Monster_Update_m2289537084(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoldMonster::Init(DataMonster)
extern "C"  void GoldMonster_Init_m1664766623 (GoldMonster_t84400954 * __this, DataMonster_t1423279280 * ___data0, const MethodInfo* method)
{
	{
		DataMonster_t1423279280 * L_0 = ___data0;
		VirtActionInvoker1< DataMonster_t1423279280 * >::Invoke(10 /* System.Void Monster::InitMonster(DataMonster) */, __this, L_0);
		DataMonster_t1423279280 * L_1 = ___data0;
		VirtActionInvoker1< DataMonster_t1423279280 * >::Invoke(11 /* System.Void Monster::InitPosition(DataMonster) */, __this, L_1);
		return;
	}
}
// System.Void GoldMonster::OnEndMove()
extern Il2CppClass* MoveFloat_t3049115883_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisBoxCollider_t131631884_m3222626457_MethodInfo_var;
extern const uint32_t GoldMonster_OnEndMove_m2243722428_MetadataUsageId;
extern "C"  void GoldMonster_OnEndMove_m2243722428 (GoldMonster_t84400954 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GoldMonster_OnEndMove_m2243722428_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BoxCollider_t131631884 * V_0 = NULL;
	{
		Move_t2404337 * L_0 = ((Character_t3568163593 *)__this)->get_m_move_3();
		NullCheck(L_0);
		int32_t L_1 = Move_get_type_m3871643672(L_0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_004a;
		}
	}
	{
		GameObject_t4012695102 * L_2 = ((Monster_t2901270458 *)__this)->get_m_monster_5();
		NullCheck(L_2);
		BoxCollider_t131631884 * L_3 = GameObject_GetComponent_TisBoxCollider_t131631884_m3222626457(L_2, /*hidden argument*/GameObject_GetComponent_TisBoxCollider_t131631884_m3222626457_MethodInfo_var);
		V_0 = L_3;
		BoxCollider_t131631884 * L_4 = V_0;
		NullCheck(L_4);
		Collider_set_enabled_m2575670866(L_4, (bool)1, /*hidden argument*/NULL);
		DataMonster_t1423279280 * L_5 = ((Monster_t2901270458 *)__this)->get_m_data_12();
		NullCheck(L_5);
		float L_6 = L_5->get_size_5();
		Transform_t284553113 * L_7 = ((Character_t3568163593 *)__this)->get_m_tr_2();
		NullCheck(L_7);
		Vector3_t3525329789  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		MoveFloat_t3049115883 * L_9 = (MoveFloat_t3049115883 *)il2cpp_codegen_object_new(MoveFloat_t3049115883_il2cpp_TypeInfo_var);
		MoveFloat__ctor_m389997278(L_9, L_6, L_8, /*hidden argument*/NULL);
		Character_SetMove_m4254943650(__this, L_9, /*hidden argument*/NULL);
		goto IL_0067;
	}

IL_004a:
	{
		Move_t2404337 * L_10 = ((Character_t3568163593 *)__this)->get_m_move_3();
		NullCheck(L_10);
		int32_t L_11 = Move_get_type_m3871643672(L_10, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_11) == ((uint32_t)2))))
		{
			goto IL_0067;
		}
	}
	{
		Pattern_t873562992 * L_12 = ((Character_t3568163593 *)__this)->get_m_pattern_4();
		NullCheck(L_12);
		Pattern_RemoveMonster_m2098574537(L_12, __this, /*hidden argument*/NULL);
	}

IL_0067:
	{
		return;
	}
}
// System.Void GoldMonster::Hit(System.Int32)
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern const uint32_t GoldMonster_Hit_m948076339_MetadataUsageId;
extern "C"  void GoldMonster_Hit_m948076339 (GoldMonster_t84400954 * __this, int32_t ___damage0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GoldMonster_Hit_m948076339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Monster_SetState_m3519019012(__this, 1, /*hidden argument*/NULL);
		Transform_t284553113 * L_0 = ((Character_t3568163593 *)__this)->get_m_tr_2();
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		GamePlay_t2590336614 * L_2 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_2);
		Stage_t80204510 * L_3 = GamePlay_get_stage_m3096959283(L_2, /*hidden argument*/NULL);
		Vector3_t3525329789  L_4 = V_0;
		float L_5 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		Stage_AddCoinEffect_m2181335947(L_3, ((int32_t)10), L_4, ((float)((float)((float)((float)L_5*(float)(2.0f)))*(float)(3.14159274f))), (2.0f), (16.0f), (40.0f), (0.0f), /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_6 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_6);
		Stage_t80204510 * L_7 = GamePlay_get_stage_m3096959283(L_6, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8 = V_0;
		float L_9 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_7);
		Stage_AddCoinEffect_m2181335947(L_7, ((int32_t)10), L_8, ((float)((float)((float)((float)L_9*(float)(2.0f)))*(float)(3.14159274f))), (2.0f), (16.0f), (40.0f), (0.0f), /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_10 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_10);
		GamePlay_AddCoin_m1214255030(L_10, ((int32_t)20), /*hidden argument*/NULL);
		return;
	}
}
// System.Void InitShader::.ctor()
extern Il2CppCodeGenString* _stringLiteral3139364321;
extern const uint32_t InitShader__ctor_m3490417414_MetadataUsageId;
extern "C"  void InitShader__ctor_m3490417414 (InitShader_t2240846133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InitShader__ctor_m3490417414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_shader_name_2(_stringLiteral3139364321);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InitShader::Awake()
extern const MethodInfo* Component_GetComponent_TisRenderer_t1092684080_m500377675_MethodInfo_var;
extern const uint32_t InitShader_Awake_m3728022633_MetadataUsageId;
extern "C"  void InitShader_Awake_m3728022633 (InitShader_t2240846133 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InitShader_Awake_m3728022633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t1092684080 * L_0 = Component_GetComponent_TisRenderer_t1092684080_m500377675(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t1092684080_m500377675_MethodInfo_var);
		NullCheck(L_0);
		Material_t1886596500 * L_1 = Renderer_get_material_m2720864603(L_0, /*hidden argument*/NULL);
		String_t* L_2 = __this->get_shader_name_2();
		Shader_t3998140498 * L_3 = Shader_Find_m4048047578(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		Material_set_shader_m3742529604(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InitShader::Update()
extern "C"  void InitShader_Update_m2555619527 (InitShader_t2240846133 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void InvAttachmentPoint::.ctor()
extern "C"  void InvAttachmentPoint__ctor_m1558575615 (InvAttachmentPoint_t1265837596 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject InvAttachmentPoint::Attach(UnityEngine.GameObject)
extern Il2CppClass* GameObject_t4012695102_il2cpp_TypeInfo_var;
extern const uint32_t InvAttachmentPoint_Attach_m3085706967_MetadataUsageId;
extern "C"  GameObject_t4012695102 * InvAttachmentPoint_Attach_m3085706967 (InvAttachmentPoint_t1265837596 * __this, GameObject_t4012695102 * ___prefab0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvAttachmentPoint_Attach_m3085706967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t284553113 * V_0 = NULL;
	Transform_t284553113 * V_1 = NULL;
	{
		GameObject_t4012695102 * L_0 = __this->get_mPrefab_3();
		GameObject_t4012695102 * L_1 = ___prefab0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00a2;
		}
	}
	{
		GameObject_t4012695102 * L_3 = ___prefab0;
		__this->set_mPrefab_3(L_3);
		GameObject_t4012695102 * L_4 = __this->get_mChild_4();
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		GameObject_t4012695102 * L_6 = __this->get_mChild_4();
		Object_Destroy_m176400816(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0034:
	{
		GameObject_t4012695102 * L_7 = __this->get_mPrefab_3();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00a2;
		}
	}
	{
		Transform_t284553113 * L_9 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		V_0 = L_9;
		GameObject_t4012695102 * L_10 = __this->get_mPrefab_3();
		Transform_t284553113 * L_11 = V_0;
		NullCheck(L_11);
		Vector3_t3525329789  L_12 = Transform_get_position_m2211398607(L_11, /*hidden argument*/NULL);
		Transform_t284553113 * L_13 = V_0;
		NullCheck(L_13);
		Quaternion_t1891715979  L_14 = Transform_get_rotation_m11483428(L_13, /*hidden argument*/NULL);
		Object_t3878351788 * L_15 = Object_Instantiate_m2255090103(NULL /*static, unused*/, L_10, L_12, L_14, /*hidden argument*/NULL);
		__this->set_mChild_4(((GameObject_t4012695102 *)IsInstSealed(L_15, GameObject_t4012695102_il2cpp_TypeInfo_var)));
		GameObject_t4012695102 * L_16 = __this->get_mChild_4();
		NullCheck(L_16);
		Transform_t284553113 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		Transform_t284553113 * L_18 = V_1;
		Transform_t284553113 * L_19 = V_0;
		NullCheck(L_18);
		Transform_set_parent_m3231272063(L_18, L_19, /*hidden argument*/NULL);
		Transform_t284553113 * L_20 = V_1;
		Vector3_t3525329789  L_21 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		Transform_set_localPosition_m3504330903(L_20, L_21, /*hidden argument*/NULL);
		Transform_t284553113 * L_22 = V_1;
		Quaternion_t1891715979  L_23 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_set_localRotation_m3719981474(L_22, L_23, /*hidden argument*/NULL);
		Transform_t284553113 * L_24 = V_1;
		Vector3_t3525329789  L_25 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localScale_m310756934(L_24, L_25, /*hidden argument*/NULL);
	}

IL_00a2:
	{
		GameObject_t4012695102 * L_26 = __this->get_mChild_4();
		return L_26;
	}
}
// System.Void InvBaseItem::.ctor()
extern Il2CppClass* List_1_t126020286_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3542527780_MethodInfo_var;
extern const uint32_t InvBaseItem__ctor_m1514333846_MetadataUsageId;
extern "C"  void InvBaseItem__ctor_m1514333846 (InvBaseItem_t1636452469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvBaseItem__ctor_m1514333846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_minItemLevel_4(1);
		__this->set_maxItemLevel_5(((int32_t)50));
		List_1_t126020286 * L_0 = (List_1_t126020286 *)il2cpp_codegen_object_new(List_1_t126020286_il2cpp_TypeInfo_var);
		List_1__ctor_m3542527780(L_0, /*hidden argument*/List_1__ctor_m3542527780_MethodInfo_var);
		__this->set_stats_6(L_0);
		Color_t1588175760  L_1 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_color_8(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_iconName_10(L_2);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InvDatabase::.ctor()
extern Il2CppClass* List_1_t2433411438_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2747380948_MethodInfo_var;
extern const uint32_t InvDatabase__ctor_m147685503_MetadataUsageId;
extern "C"  void InvDatabase__ctor_m147685503 (InvDatabase_t852767852 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvDatabase__ctor_m147685503_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2433411438 * L_0 = (List_1_t2433411438 *)il2cpp_codegen_object_new(List_1_t2433411438_il2cpp_TypeInfo_var);
		List_1__ctor_m2747380948(L_0, /*hidden argument*/List_1__ctor_m2747380948_MethodInfo_var);
		__this->set_items_5(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void InvDatabase::.cctor()
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern const uint32_t InvDatabase__cctor_m4096154382_MetadataUsageId;
extern "C"  void InvDatabase__cctor_m4096154382 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvDatabase__cctor_m4096154382_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((InvDatabase_t852767852_StaticFields*)InvDatabase_t852767852_il2cpp_TypeInfo_var->static_fields)->set_mIsDirty_3((bool)1);
		return;
	}
}
// InvDatabase[] InvDatabase::get_list()
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* NGUITools_FindActive_TisInvDatabase_t852767852_m1589005867_MethodInfo_var;
extern const uint32_t InvDatabase_get_list_m1583387015_MetadataUsageId;
extern "C"  InvDatabaseU5BU5D_t3324850277* InvDatabase_get_list_m1583387015 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvDatabase_get_list_m1583387015_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		bool L_0 = ((InvDatabase_t852767852_StaticFields*)InvDatabase_t852767852_il2cpp_TypeInfo_var->static_fields)->get_mIsDirty_3();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		((InvDatabase_t852767852_StaticFields*)InvDatabase_t852767852_il2cpp_TypeInfo_var->static_fields)->set_mIsDirty_3((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		InvDatabaseU5BU5D_t3324850277* L_1 = NGUITools_FindActive_TisInvDatabase_t852767852_m1589005867(NULL /*static, unused*/, /*hidden argument*/NGUITools_FindActive_TisInvDatabase_t852767852_m1589005867_MethodInfo_var);
		((InvDatabase_t852767852_StaticFields*)InvDatabase_t852767852_il2cpp_TypeInfo_var->static_fields)->set_mList_2(L_1);
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvDatabaseU5BU5D_t3324850277* L_2 = ((InvDatabase_t852767852_StaticFields*)InvDatabase_t852767852_il2cpp_TypeInfo_var->static_fields)->get_mList_2();
		return L_2;
	}
}
// System.Void InvDatabase::OnEnable()
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern const uint32_t InvDatabase_OnEnable_m3723887271_MetadataUsageId;
extern "C"  void InvDatabase_OnEnable_m3723887271 (InvDatabase_t852767852 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvDatabase_OnEnable_m3723887271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		((InvDatabase_t852767852_StaticFields*)InvDatabase_t852767852_il2cpp_TypeInfo_var->static_fields)->set_mIsDirty_3((bool)1);
		return;
	}
}
// System.Void InvDatabase::OnDisable()
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern const uint32_t InvDatabase_OnDisable_m4212292966_MetadataUsageId;
extern "C"  void InvDatabase_OnDisable_m4212292966 (InvDatabase_t852767852 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvDatabase_OnDisable_m4212292966_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		((InvDatabase_t852767852_StaticFields*)InvDatabase_t852767852_il2cpp_TypeInfo_var->static_fields)->set_mIsDirty_3((bool)1);
		return;
	}
}
// InvBaseItem InvDatabase::GetItem(System.Int32)
extern "C"  InvBaseItem_t1636452469 * InvDatabase_GetItem_m340865653 (InvDatabase_t852767852 * __this, int32_t ___id160, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	InvBaseItem_t1636452469 * V_2 = NULL;
	{
		V_0 = 0;
		List_1_t2433411438 * L_0 = __this->get_items_5();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InvBaseItem>::get_Count() */, L_0);
		V_1 = L_1;
		goto IL_0032;
	}

IL_0013:
	{
		List_1_t2433411438 * L_2 = __this->get_items_5();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		InvBaseItem_t1636452469 * L_4 = VirtFuncInvoker1< InvBaseItem_t1636452469 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<InvBaseItem>::get_Item(System.Int32) */, L_2, L_3);
		V_2 = L_4;
		InvBaseItem_t1636452469 * L_5 = V_2;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_id16_0();
		int32_t L_7 = ___id160;
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_002e;
		}
	}
	{
		InvBaseItem_t1636452469 * L_8 = V_2;
		return L_8;
	}

IL_002e:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0013;
		}
	}
	{
		return (InvBaseItem_t1636452469 *)NULL;
	}
}
// InvDatabase InvDatabase::GetDatabase(System.Int32)
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern const uint32_t InvDatabase_GetDatabase_m2035689222_MetadataUsageId;
extern "C"  InvDatabase_t852767852 * InvDatabase_GetDatabase_m2035689222 (Il2CppObject * __this /* static, unused */, int32_t ___dbID0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvDatabase_GetDatabase_m2035689222_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	InvDatabase_t852767852 * V_2 = NULL;
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvDatabaseU5BU5D_t3324850277* L_0 = InvDatabase_get_list_m1583387015(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		goto IL_0029;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvDatabaseU5BU5D_t3324850277* L_1 = InvDatabase_get_list_m1583387015(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3)));
		InvDatabase_t852767852 * L_4 = V_2;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_databaseID_4();
		int32_t L_6 = ___dbID0;
		if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
		{
			goto IL_0025;
		}
	}
	{
		InvDatabase_t852767852 * L_7 = V_2;
		return L_7;
	}

IL_0025:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0029:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_000f;
		}
	}
	{
		return (InvDatabase_t852767852 *)NULL;
	}
}
// InvBaseItem InvDatabase::FindByID(System.Int32)
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern const uint32_t InvDatabase_FindByID_m363273219_MetadataUsageId;
extern "C"  InvBaseItem_t1636452469 * InvDatabase_FindByID_m363273219 (Il2CppObject * __this /* static, unused */, int32_t ___id320, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvDatabase_FindByID_m363273219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	InvDatabase_t852767852 * V_0 = NULL;
	InvBaseItem_t1636452469 * G_B3_0 = NULL;
	{
		int32_t L_0 = ___id320;
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvDatabase_t852767852 * L_1 = InvDatabase_GetDatabase_m2035689222(NULL /*static, unused*/, ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)16))), /*hidden argument*/NULL);
		V_0 = L_1;
		InvDatabase_t852767852 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0028;
		}
	}
	{
		InvDatabase_t852767852 * L_4 = V_0;
		int32_t L_5 = ___id320;
		NullCheck(L_4);
		InvBaseItem_t1636452469 * L_6 = InvDatabase_GetItem_m340865653(L_4, ((int32_t)((int32_t)L_5&(int32_t)((int32_t)65535))), /*hidden argument*/NULL);
		G_B3_0 = L_6;
		goto IL_0029;
	}

IL_0028:
	{
		G_B3_0 = ((InvBaseItem_t1636452469 *)(NULL));
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// InvBaseItem InvDatabase::FindByName(System.String)
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t InvDatabase_FindByName_m519611520_MetadataUsageId;
extern "C"  InvBaseItem_t1636452469 * InvDatabase_FindByName_m519611520 (Il2CppObject * __this /* static, unused */, String_t* ___exact0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvDatabase_FindByName_m519611520_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	InvDatabase_t852767852 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	InvBaseItem_t1636452469 * V_5 = NULL;
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvDatabaseU5BU5D_t3324850277* L_0 = InvDatabase_get_list_m1583387015(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		goto IL_005e;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvDatabaseU5BU5D_t3324850277* L_1 = InvDatabase_get_list_m1583387015(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3)));
		V_3 = 0;
		InvDatabase_t852767852 * L_4 = V_2;
		NullCheck(L_4);
		List_1_t2433411438 * L_5 = L_4->get_items_5();
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InvBaseItem>::get_Count() */, L_5);
		V_4 = L_6;
		goto IL_0052;
	}

IL_002b:
	{
		InvDatabase_t852767852 * L_7 = V_2;
		NullCheck(L_7);
		List_1_t2433411438 * L_8 = L_7->get_items_5();
		int32_t L_9 = V_3;
		NullCheck(L_8);
		InvBaseItem_t1636452469 * L_10 = VirtFuncInvoker1< InvBaseItem_t1636452469 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<InvBaseItem>::get_Item(System.Int32) */, L_8, L_9);
		V_5 = L_10;
		InvBaseItem_t1636452469 * L_11 = V_5;
		NullCheck(L_11);
		String_t* L_12 = L_11->get_name_1();
		String_t* L_13 = ___exact0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_14 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_004e;
		}
	}
	{
		InvBaseItem_t1636452469 * L_15 = V_5;
		return L_15;
	}

IL_004e:
	{
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_17 = V_3;
		int32_t L_18 = V_4;
		if ((((int32_t)L_17) < ((int32_t)L_18)))
		{
			goto IL_002b;
		}
	}
	{
		int32_t L_19 = V_0;
		V_0 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_005e:
	{
		int32_t L_20 = V_0;
		int32_t L_21 = V_1;
		if ((((int32_t)L_20) < ((int32_t)L_21)))
		{
			goto IL_000f;
		}
	}
	{
		return (InvBaseItem_t1636452469 *)NULL;
	}
}
// System.Int32 InvDatabase::FindItemID(InvBaseItem)
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern const uint32_t InvDatabase_FindItemID_m3871128319_MetadataUsageId;
extern "C"  int32_t InvDatabase_FindItemID_m3871128319 (Il2CppObject * __this /* static, unused */, InvBaseItem_t1636452469 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvDatabase_FindItemID_m3871128319_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	InvDatabase_t852767852 * V_2 = NULL;
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvDatabaseU5BU5D_t3324850277* L_0 = InvDatabase_get_list_m1583387015(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		goto IL_003d;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvDatabaseU5BU5D_t3324850277* L_1 = InvDatabase_get_list_m1583387015(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3)));
		InvDatabase_t852767852 * L_4 = V_2;
		NullCheck(L_4);
		List_1_t2433411438 * L_5 = L_4->get_items_5();
		InvBaseItem_t1636452469 * L_6 = ___item0;
		NullCheck(L_5);
		bool L_7 = VirtFuncInvoker1< bool, InvBaseItem_t1636452469 * >::Invoke(24 /* System.Boolean System.Collections.Generic.List`1<InvBaseItem>::Contains(!0) */, L_5, L_6);
		if (!L_7)
		{
			goto IL_0039;
		}
	}
	{
		InvDatabase_t852767852 * L_8 = V_2;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_databaseID_4();
		InvBaseItem_t1636452469 * L_10 = ___item0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_id16_0();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_9<<(int32_t)((int32_t)16)))|(int32_t)L_11));
	}

IL_0039:
	{
		int32_t L_12 = V_0;
		V_0 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_003d:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = V_1;
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_000f;
		}
	}
	{
		return (-1);
	}
}
// System.Void InvEquipment::.ctor()
extern "C"  void InvEquipment__ctor_m266557054 (InvEquipment_t2103517373 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// InvGameItem[] InvEquipment::get_equippedItems()
extern "C"  InvGameItemU5BU5D_t1443296723* InvEquipment_get_equippedItems_m988541625 (InvEquipment_t2103517373 * __this, const MethodInfo* method)
{
	{
		InvGameItemU5BU5D_t1443296723* L_0 = __this->get_mItems_2();
		return L_0;
	}
}
// InvGameItem InvEquipment::Replace(InvBaseItem/Slot,InvGameItem)
extern Il2CppClass* InvGameItemU5BU5D_t1443296723_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisInvAttachmentPoint_t1265837596_m2807949594_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t1092684080_m4102086307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1501720559;
extern Il2CppCodeGenString* _stringLiteral2043661874;
extern const uint32_t InvEquipment_Replace_m2259815697_MetadataUsageId;
extern "C"  InvGameItem_t1588794646 * InvEquipment_Replace_m2259815697 (InvEquipment_t2103517373 * __this, int32_t ___slot0, InvGameItem_t1588794646 * ___item1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvEquipment_Replace_m2259815697_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	InvBaseItem_t1636452469 * V_0 = NULL;
	int32_t V_1 = 0;
	InvGameItem_t1588794646 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	InvAttachmentPoint_t1265837596 * V_5 = NULL;
	GameObject_t4012695102 * V_6 = NULL;
	Renderer_t1092684080 * V_7 = NULL;
	InvBaseItem_t1636452469 * G_B3_0 = NULL;
	InvAttachmentPoint_t1265837596 * G_B15_0 = NULL;
	InvAttachmentPoint_t1265837596 * G_B14_0 = NULL;
	GameObject_t4012695102 * G_B16_0 = NULL;
	InvAttachmentPoint_t1265837596 * G_B16_1 = NULL;
	{
		InvGameItem_t1588794646 * L_0 = ___item1;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		InvGameItem_t1588794646 * L_1 = ___item1;
		NullCheck(L_1);
		InvBaseItem_t1636452469 * L_2 = InvGameItem_get_baseItem_m862510590(L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0012;
	}

IL_0011:
	{
		G_B3_0 = ((InvBaseItem_t1636452469 *)(NULL));
	}

IL_0012:
	{
		V_0 = G_B3_0;
		int32_t L_3 = ___slot0;
		if (!L_3)
		{
			goto IL_00ff;
		}
	}
	{
		InvBaseItem_t1636452469 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		InvBaseItem_t1636452469 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_slot_3();
		int32_t L_7 = ___slot0;
		if ((((int32_t)L_6) == ((int32_t)L_7)))
		{
			goto IL_002d;
		}
	}
	{
		InvGameItem_t1588794646 * L_8 = ___item1;
		return L_8;
	}

IL_002d:
	{
		InvGameItemU5BU5D_t1443296723* L_9 = __this->get_mItems_2();
		if (L_9)
		{
			goto IL_0046;
		}
	}
	{
		V_1 = 8;
		int32_t L_10 = V_1;
		__this->set_mItems_2(((InvGameItemU5BU5D_t1443296723*)SZArrayNew(InvGameItemU5BU5D_t1443296723_il2cpp_TypeInfo_var, (uint32_t)L_10)));
	}

IL_0046:
	{
		InvGameItemU5BU5D_t1443296723* L_11 = __this->get_mItems_2();
		int32_t L_12 = ___slot0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12-(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12-(int32_t)1));
		V_2 = ((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13)));
		InvGameItemU5BU5D_t1443296723* L_14 = __this->get_mItems_2();
		int32_t L_15 = ___slot0;
		InvGameItem_t1588794646 * L_16 = ___item1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)L_15-(int32_t)1)));
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_15-(int32_t)1))), (InvGameItem_t1588794646 *)L_16);
		InvAttachmentPointU5BU5D_t2968998389* L_17 = __this->get_mAttachments_3();
		if (L_17)
		{
			goto IL_0073;
		}
	}
	{
		InvAttachmentPointU5BU5D_t2968998389* L_18 = Component_GetComponentsInChildren_TisInvAttachmentPoint_t1265837596_m2807949594(__this, /*hidden argument*/Component_GetComponentsInChildren_TisInvAttachmentPoint_t1265837596_m2807949594_MethodInfo_var);
		__this->set_mAttachments_3(L_18);
	}

IL_0073:
	{
		V_3 = 0;
		InvAttachmentPointU5BU5D_t2968998389* L_19 = __this->get_mAttachments_3();
		NullCheck(L_19);
		V_4 = (((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length))));
		goto IL_00f5;
	}

IL_0084:
	{
		InvAttachmentPointU5BU5D_t2968998389* L_20 = __this->get_mAttachments_3();
		int32_t L_21 = V_3;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		V_5 = ((L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22)));
		InvAttachmentPoint_t1265837596 * L_23 = V_5;
		NullCheck(L_23);
		int32_t L_24 = L_23->get_slot_2();
		int32_t L_25 = ___slot0;
		if ((!(((uint32_t)L_24) == ((uint32_t)L_25))))
		{
			goto IL_00f1;
		}
	}
	{
		InvAttachmentPoint_t1265837596 * L_26 = V_5;
		InvBaseItem_t1636452469 * L_27 = V_0;
		G_B14_0 = L_26;
		if (!L_27)
		{
			G_B15_0 = L_26;
			goto IL_00ae;
		}
	}
	{
		InvBaseItem_t1636452469 * L_28 = V_0;
		NullCheck(L_28);
		GameObject_t4012695102 * L_29 = L_28->get_attachment_7();
		G_B16_0 = L_29;
		G_B16_1 = G_B14_0;
		goto IL_00af;
	}

IL_00ae:
	{
		G_B16_0 = ((GameObject_t4012695102 *)(NULL));
		G_B16_1 = G_B15_0;
	}

IL_00af:
	{
		NullCheck(G_B16_1);
		GameObject_t4012695102 * L_30 = InvAttachmentPoint_Attach_m3085706967(G_B16_1, G_B16_0, /*hidden argument*/NULL);
		V_6 = L_30;
		InvBaseItem_t1636452469 * L_31 = V_0;
		if (!L_31)
		{
			goto IL_00f1;
		}
	}
	{
		GameObject_t4012695102 * L_32 = V_6;
		bool L_33 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_32, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_00f1;
		}
	}
	{
		GameObject_t4012695102 * L_34 = V_6;
		NullCheck(L_34);
		Renderer_t1092684080 * L_35 = GameObject_GetComponent_TisRenderer_t1092684080_m4102086307(L_34, /*hidden argument*/GameObject_GetComponent_TisRenderer_t1092684080_m4102086307_MethodInfo_var);
		V_7 = L_35;
		Renderer_t1092684080 * L_36 = V_7;
		bool L_37 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_36, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_00f1;
		}
	}
	{
		Renderer_t1092684080 * L_38 = V_7;
		NullCheck(L_38);
		Material_t1886596500 * L_39 = Renderer_get_material_m2720864603(L_38, /*hidden argument*/NULL);
		InvBaseItem_t1636452469 * L_40 = V_0;
		NullCheck(L_40);
		Color_t1588175760  L_41 = L_40->get_color_8();
		NullCheck(L_39);
		Material_set_color_m3296857020(L_39, L_41, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		int32_t L_42 = V_3;
		V_3 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_00f5:
	{
		int32_t L_43 = V_3;
		int32_t L_44 = V_4;
		if ((((int32_t)L_43) < ((int32_t)L_44)))
		{
			goto IL_0084;
		}
	}
	{
		InvGameItem_t1588794646 * L_45 = V_2;
		return L_45;
	}

IL_00ff:
	{
		InvGameItem_t1588794646 * L_46 = ___item1;
		if (!L_46)
		{
			goto IL_011f;
		}
	}
	{
		InvGameItem_t1588794646 * L_47 = ___item1;
		NullCheck(L_47);
		String_t* L_48 = InvGameItem_get_name_m428644288(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_49 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1501720559, L_48, _stringLiteral2043661874, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
	}

IL_011f:
	{
		InvGameItem_t1588794646 * L_50 = ___item1;
		return L_50;
	}
}
// InvGameItem InvEquipment::Equip(InvGameItem)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral449152171;
extern const uint32_t InvEquipment_Equip_m1078130573_MetadataUsageId;
extern "C"  InvGameItem_t1588794646 * InvEquipment_Equip_m1078130573 (InvEquipment_t2103517373 * __this, InvGameItem_t1588794646 * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvEquipment_Equip_m1078130573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	InvBaseItem_t1636452469 * V_0 = NULL;
	{
		InvGameItem_t1588794646 * L_0 = ___item0;
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		InvGameItem_t1588794646 * L_1 = ___item0;
		NullCheck(L_1);
		InvBaseItem_t1636452469 * L_2 = InvGameItem_get_baseItem_m862510590(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		InvBaseItem_t1636452469 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		InvBaseItem_t1636452469 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_slot_3();
		InvGameItem_t1588794646 * L_6 = ___item0;
		InvGameItem_t1588794646 * L_7 = InvEquipment_Replace_m2259815697(__this, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_0021:
	{
		InvGameItem_t1588794646 * L_8 = ___item0;
		NullCheck(L_8);
		int32_t L_9 = InvGameItem_get_baseItemID_m34079433(L_8, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Il2CppObject * L_11 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral449152171, L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_003b:
	{
		InvGameItem_t1588794646 * L_13 = ___item0;
		return L_13;
	}
}
// InvGameItem InvEquipment::Unequip(InvGameItem)
extern "C"  InvGameItem_t1588794646 * InvEquipment_Unequip_m74385766 (InvEquipment_t2103517373 * __this, InvGameItem_t1588794646 * ___item0, const MethodInfo* method)
{
	InvBaseItem_t1636452469 * V_0 = NULL;
	{
		InvGameItem_t1588794646 * L_0 = ___item0;
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		InvGameItem_t1588794646 * L_1 = ___item0;
		NullCheck(L_1);
		InvBaseItem_t1636452469 * L_2 = InvGameItem_get_baseItem_m862510590(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		InvBaseItem_t1636452469 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0021;
		}
	}
	{
		InvBaseItem_t1636452469 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_slot_3();
		InvGameItem_t1588794646 * L_6 = InvEquipment_Replace_m2259815697(__this, L_5, (InvGameItem_t1588794646 *)NULL, /*hidden argument*/NULL);
		return L_6;
	}

IL_0021:
	{
		InvGameItem_t1588794646 * L_7 = ___item0;
		return L_7;
	}
}
// InvGameItem InvEquipment::Unequip(InvBaseItem/Slot)
extern "C"  InvGameItem_t1588794646 * InvEquipment_Unequip_m1181962238 (InvEquipment_t2103517373 * __this, int32_t ___slot0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___slot0;
		InvGameItem_t1588794646 * L_1 = InvEquipment_Replace_m2259815697(__this, L_0, (InvGameItem_t1588794646 *)NULL, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean InvEquipment::HasEquipped(InvGameItem)
extern "C"  bool InvEquipment_HasEquipped_m2004473211 (InvEquipment_t2103517373 * __this, InvGameItem_t1588794646 * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		InvGameItemU5BU5D_t1443296723* L_0 = __this->get_mItems_2();
		if (!L_0)
		{
			goto IL_0036;
		}
	}
	{
		V_0 = 0;
		InvGameItemU5BU5D_t1443296723* L_1 = __this->get_mItems_2();
		NullCheck(L_1);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))));
		goto IL_002f;
	}

IL_001b:
	{
		InvGameItemU5BU5D_t1443296723* L_2 = __this->get_mItems_2();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		InvGameItem_t1588794646 * L_5 = ___item0;
		if ((!(((Il2CppObject*)(InvGameItem_t1588794646 *)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4)))) == ((Il2CppObject*)(InvGameItem_t1588794646 *)L_5))))
		{
			goto IL_002b;
		}
	}
	{
		return (bool)1;
	}

IL_002b:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_002f:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_001b;
		}
	}

IL_0036:
	{
		return (bool)0;
	}
}
// System.Boolean InvEquipment::HasEquipped(InvBaseItem/Slot)
extern "C"  bool InvEquipment_HasEquipped_m1780336905 (InvEquipment_t2103517373 * __this, int32_t ___slot0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	InvBaseItem_t1636452469 * V_2 = NULL;
	{
		InvGameItemU5BU5D_t1443296723* L_0 = __this->get_mItems_2();
		if (!L_0)
		{
			goto IL_0048;
		}
	}
	{
		V_0 = 0;
		InvGameItemU5BU5D_t1443296723* L_1 = __this->get_mItems_2();
		NullCheck(L_1);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))));
		goto IL_0041;
	}

IL_001b:
	{
		InvGameItemU5BU5D_t1443296723* L_2 = __this->get_mItems_2();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck(((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))));
		InvBaseItem_t1636452469 * L_5 = InvGameItem_get_baseItem_m862510590(((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4))), /*hidden argument*/NULL);
		V_2 = L_5;
		InvBaseItem_t1636452469 * L_6 = V_2;
		if (!L_6)
		{
			goto IL_003d;
		}
	}
	{
		InvBaseItem_t1636452469 * L_7 = V_2;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_slot_3();
		int32_t L_9 = ___slot0;
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_003d;
		}
	}
	{
		return (bool)1;
	}

IL_003d:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0041:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_001b;
		}
	}

IL_0048:
	{
		return (bool)0;
	}
}
// InvGameItem InvEquipment::GetItem(InvBaseItem/Slot)
extern "C"  InvGameItem_t1588794646 * InvEquipment_GetItem_m1873048016 (InvEquipment_t2103517373 * __this, int32_t ___slot0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___slot0;
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_1 = ___slot0;
		V_0 = ((int32_t)((int32_t)L_1-(int32_t)1));
		InvGameItemU5BU5D_t1443296723* L_2 = __this->get_mItems_2();
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		int32_t L_3 = V_0;
		InvGameItemU5BU5D_t1443296723* L_4 = __this->get_mItems_2();
		NullCheck(L_4);
		if ((((int32_t)L_3) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))
		{
			goto IL_002c;
		}
	}
	{
		InvGameItemU5BU5D_t1443296723* L_5 = __this->get_mItems_2();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		return ((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7)));
	}

IL_002c:
	{
		return (InvGameItem_t1588794646 *)NULL;
	}
}
// System.Void InvGameItem::.ctor(System.Int32)
extern "C"  void InvGameItem__ctor_m219577574 (InvGameItem_t1588794646 * __this, int32_t ___id0, const MethodInfo* method)
{
	{
		__this->set_quality_1(4);
		__this->set_itemLevel_2(1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___id0;
		__this->set_mBaseItemID_0(L_0);
		return;
	}
}
// System.Void InvGameItem::.ctor(System.Int32,InvBaseItem)
extern "C"  void InvGameItem__ctor_m2017209789 (InvGameItem_t1588794646 * __this, int32_t ___id0, InvBaseItem_t1636452469 * ___bi1, const MethodInfo* method)
{
	{
		__this->set_quality_1(4);
		__this->set_itemLevel_2(1);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___id0;
		__this->set_mBaseItemID_0(L_0);
		InvBaseItem_t1636452469 * L_1 = ___bi1;
		__this->set_mBaseItem_3(L_1);
		return;
	}
}
// System.Int32 InvGameItem::get_baseItemID()
extern "C"  int32_t InvGameItem_get_baseItemID_m34079433 (InvGameItem_t1588794646 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mBaseItemID_0();
		return L_0;
	}
}
// InvBaseItem InvGameItem::get_baseItem()
extern Il2CppClass* InvDatabase_t852767852_il2cpp_TypeInfo_var;
extern const uint32_t InvGameItem_get_baseItem_m862510590_MetadataUsageId;
extern "C"  InvBaseItem_t1636452469 * InvGameItem_get_baseItem_m862510590 (InvGameItem_t1588794646 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvGameItem_get_baseItem_m862510590_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InvBaseItem_t1636452469 * L_0 = __this->get_mBaseItem_3();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = InvGameItem_get_baseItemID_m34079433(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(InvDatabase_t852767852_il2cpp_TypeInfo_var);
		InvBaseItem_t1636452469 * L_2 = InvDatabase_FindByID_m363273219(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_mBaseItem_3(L_2);
	}

IL_001c:
	{
		InvBaseItem_t1636452469 * L_3 = __this->get_mBaseItem_3();
		return L_3;
	}
}
// System.String InvGameItem::get_name()
extern Il2CppClass* Quality_t2315868383_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t InvGameItem_get_name_m428644288_MetadataUsageId;
extern "C"  String_t* InvGameItem_get_name_m428644288 (InvGameItem_t1588794646 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvGameItem_get_name_m428644288_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InvBaseItem_t1636452469 * L_0 = InvGameItem_get_baseItem_m862510590(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000d:
	{
		int32_t L_1 = __this->get_quality_1();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Quality_t2315868383_il2cpp_TypeInfo_var, &L_2);
		NullCheck((Enum_t2778772662 *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2778772662 *)L_3);
		InvBaseItem_t1636452469 * L_5 = InvGameItem_get_baseItem_m862510590(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = L_5->get_name_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = String_Concat_m1825781833(NULL /*static, unused*/, L_4, _stringLiteral32, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Single InvGameItem::get_statMultiplier()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t InvGameItem_get_statMultiplier_m2835306337_MetadataUsageId;
extern "C"  float InvGameItem_get_statMultiplier_m2835306337 (InvGameItem_t1588794646 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvGameItem_get_statMultiplier_m2835306337_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		V_0 = (0.0f);
		int32_t L_0 = __this->get_quality_1();
		V_2 = L_0;
		int32_t L_1 = V_2;
		if (L_1 == 0)
		{
			goto IL_0053;
		}
		if (L_1 == 1)
		{
			goto IL_0048;
		}
		if (L_1 == 2)
		{
			goto IL_005e;
		}
		if (L_1 == 3)
		{
			goto IL_0069;
		}
		if (L_1 == 4)
		{
			goto IL_0074;
		}
		if (L_1 == 5)
		{
			goto IL_007f;
		}
		if (L_1 == 6)
		{
			goto IL_008a;
		}
		if (L_1 == 7)
		{
			goto IL_0095;
		}
		if (L_1 == 8)
		{
			goto IL_00a0;
		}
		if (L_1 == 9)
		{
			goto IL_00ab;
		}
		if (L_1 == 10)
		{
			goto IL_00b6;
		}
		if (L_1 == 11)
		{
			goto IL_00c1;
		}
	}
	{
		goto IL_00cc;
	}

IL_0048:
	{
		V_0 = (-1.0f);
		goto IL_00cc;
	}

IL_0053:
	{
		V_0 = (0.0f);
		goto IL_00cc;
	}

IL_005e:
	{
		V_0 = (0.25f);
		goto IL_00cc;
	}

IL_0069:
	{
		V_0 = (0.9f);
		goto IL_00cc;
	}

IL_0074:
	{
		V_0 = (1.0f);
		goto IL_00cc;
	}

IL_007f:
	{
		V_0 = (1.1f);
		goto IL_00cc;
	}

IL_008a:
	{
		V_0 = (1.25f);
		goto IL_00cc;
	}

IL_0095:
	{
		V_0 = (1.5f);
		goto IL_00cc;
	}

IL_00a0:
	{
		V_0 = (1.75f);
		goto IL_00cc;
	}

IL_00ab:
	{
		V_0 = (2.0f);
		goto IL_00cc;
	}

IL_00b6:
	{
		V_0 = (2.5f);
		goto IL_00cc;
	}

IL_00c1:
	{
		V_0 = (3.0f);
		goto IL_00cc;
	}

IL_00cc:
	{
		int32_t L_2 = __this->get_itemLevel_2();
		V_1 = ((float)((float)(((float)((float)L_2)))/(float)(50.0f)));
		float L_3 = V_0;
		float L_4 = V_1;
		float L_5 = V_1;
		float L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_4, ((float)((float)L_5*(float)L_6)), (0.5f), /*hidden argument*/NULL);
		V_0 = ((float)((float)L_3*(float)L_7));
		float L_8 = V_0;
		return L_8;
	}
}
// UnityEngine.Color InvGameItem::get_color()
extern "C"  Color_t1588175760  InvGameItem_get_color_m2355338580 (InvGameItem_t1588794646 * __this, const MethodInfo* method)
{
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	{
		Color_t1588175760  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = __this->get_quality_1();
		V_1 = L_1;
		int32_t L_2 = V_1;
		if (L_2 == 0)
		{
			goto IL_0053;
		}
		if (L_2 == 1)
		{
			goto IL_0048;
		}
		if (L_2 == 2)
		{
			goto IL_006e;
		}
		if (L_2 == 3)
		{
			goto IL_0089;
		}
		if (L_2 == 4)
		{
			goto IL_00a4;
		}
		if (L_2 == 5)
		{
			goto IL_00bf;
		}
		if (L_2 == 6)
		{
			goto IL_00cf;
		}
		if (L_2 == 7)
		{
			goto IL_00df;
		}
		if (L_2 == 8)
		{
			goto IL_00ef;
		}
		if (L_2 == 9)
		{
			goto IL_00ff;
		}
		if (L_2 == 10)
		{
			goto IL_010f;
		}
		if (L_2 == 11)
		{
			goto IL_011f;
		}
	}
	{
		goto IL_012f;
	}

IL_0048:
	{
		Color_t1588175760  L_3 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_012f;
	}

IL_0053:
	{
		Color__ctor_m103496991((&V_0), (0.4f), (0.2f), (0.2f), /*hidden argument*/NULL);
		goto IL_012f;
	}

IL_006e:
	{
		Color__ctor_m103496991((&V_0), (0.4f), (0.4f), (0.4f), /*hidden argument*/NULL);
		goto IL_012f;
	}

IL_0089:
	{
		Color__ctor_m103496991((&V_0), (0.7f), (0.7f), (0.7f), /*hidden argument*/NULL);
		goto IL_012f;
	}

IL_00a4:
	{
		Color__ctor_m103496991((&V_0), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		goto IL_012f;
	}

IL_00bf:
	{
		Color_t1588175760  L_4 = NGUIMath_HexToColor_m375679784(NULL /*static, unused*/, ((int32_t)-520110337), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_012f;
	}

IL_00cf:
	{
		Color_t1588175760  L_5 = NGUIMath_HexToColor_m375679784(NULL /*static, unused*/, ((int32_t)-1814607361), /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_012f;
	}

IL_00df:
	{
		Color_t1588175760  L_6 = NGUIMath_HexToColor_m375679784(NULL /*static, unused*/, ((int32_t)1325334783), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_012f;
	}

IL_00ef:
	{
		Color_t1588175760  L_7 = NGUIMath_HexToColor_m375679784(NULL /*static, unused*/, ((int32_t)12255231), /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_012f;
	}

IL_00ff:
	{
		Color_t1588175760  L_8 = NGUIMath_HexToColor_m375679784(NULL /*static, unused*/, ((int32_t)1937178111), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_012f;
	}

IL_010f:
	{
		Color_t1588175760  L_9 = NGUIMath_HexToColor_m375679784(NULL /*static, unused*/, ((int32_t)-1778319361), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_012f;
	}

IL_011f:
	{
		Color_t1588175760  L_10 = NGUIMath_HexToColor_m375679784(NULL /*static, unused*/, ((int32_t)-7339777), /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_012f;
	}

IL_012f:
	{
		Color_t1588175760  L_11 = V_0;
		return L_11;
	}
}
// System.Collections.Generic.List`1<InvStat> InvGameItem::CalculateStats()
extern Il2CppClass* List_1_t126020286_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* InvStat_t3624028613_il2cpp_TypeInfo_var;
extern Il2CppClass* Comparison_1_t2032736193_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3542527780_MethodInfo_var;
extern const MethodInfo* InvStat_CompareArmor_m1514641866_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m4235594531_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m3986012104_MethodInfo_var;
extern const uint32_t InvGameItem_CalculateStats_m417290082_MetadataUsageId;
extern "C"  List_1_t126020286 * InvGameItem_CalculateStats_m417290082 (InvGameItem_t1588794646 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvGameItem_CalculateStats_m417290082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t126020286 * V_0 = NULL;
	float V_1 = 0.0f;
	List_1_t126020286 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	InvStat_t3624028613 * V_5 = NULL;
	int32_t V_6 = 0;
	bool V_7 = false;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	InvStat_t3624028613 * V_10 = NULL;
	InvStat_t3624028613 * V_11 = NULL;
	{
		List_1_t126020286 * L_0 = (List_1_t126020286 *)il2cpp_codegen_object_new(List_1_t126020286_il2cpp_TypeInfo_var);
		List_1__ctor_m3542527780(L_0, /*hidden argument*/List_1__ctor_m3542527780_MethodInfo_var);
		V_0 = L_0;
		InvBaseItem_t1636452469 * L_1 = InvGameItem_get_baseItem_m862510590(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_011c;
		}
	}
	{
		float L_2 = InvGameItem_get_statMultiplier_m2835306337(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		InvBaseItem_t1636452469 * L_3 = InvGameItem_get_baseItem_m862510590(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		List_1_t126020286 * L_4 = L_3->get_stats_6();
		V_2 = L_4;
		V_3 = 0;
		List_1_t126020286 * L_5 = V_2;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InvStat>::get_Count() */, L_5);
		V_4 = L_6;
		goto IL_0102;
	}

IL_0033:
	{
		List_1_t126020286 * L_7 = V_2;
		int32_t L_8 = V_3;
		NullCheck(L_7);
		InvStat_t3624028613 * L_9 = VirtFuncInvoker1< InvStat_t3624028613 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<InvStat>::get_Item(System.Int32) */, L_7, L_8);
		V_5 = L_9;
		float L_10 = V_1;
		InvStat_t3624028613 * L_11 = V_5;
		NullCheck(L_11);
		int32_t L_12 = L_11->get_amount_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_13 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_10*(float)(((float)((float)L_12))))), /*hidden argument*/NULL);
		V_6 = L_13;
		int32_t L_14 = V_6;
		if (L_14)
		{
			goto IL_0059;
		}
	}
	{
		goto IL_00fe;
	}

IL_0059:
	{
		V_7 = (bool)0;
		V_8 = 0;
		List_1_t126020286 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<InvStat>::get_Count() */, L_15);
		V_9 = L_16;
		goto IL_00ba;
	}

IL_006c:
	{
		List_1_t126020286 * L_17 = V_0;
		int32_t L_18 = V_8;
		NullCheck(L_17);
		InvStat_t3624028613 * L_19 = VirtFuncInvoker1< InvStat_t3624028613 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<InvStat>::get_Item(System.Int32) */, L_17, L_18);
		V_10 = L_19;
		InvStat_t3624028613 * L_20 = V_10;
		NullCheck(L_20);
		int32_t L_21 = L_20->get_id_0();
		InvStat_t3624028613 * L_22 = V_5;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_id_0();
		if ((!(((uint32_t)L_21) == ((uint32_t)L_23))))
		{
			goto IL_00b4;
		}
	}
	{
		InvStat_t3624028613 * L_24 = V_10;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_modifier_1();
		InvStat_t3624028613 * L_26 = V_5;
		NullCheck(L_26);
		int32_t L_27 = L_26->get_modifier_1();
		if ((!(((uint32_t)L_25) == ((uint32_t)L_27))))
		{
			goto IL_00b4;
		}
	}
	{
		InvStat_t3624028613 * L_28 = V_10;
		InvStat_t3624028613 * L_29 = L_28;
		NullCheck(L_29);
		int32_t L_30 = L_29->get_amount_2();
		int32_t L_31 = V_6;
		NullCheck(L_29);
		L_29->set_amount_2(((int32_t)((int32_t)L_30+(int32_t)L_31)));
		V_7 = (bool)1;
		goto IL_00c3;
	}

IL_00b4:
	{
		int32_t L_32 = V_8;
		V_8 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00ba:
	{
		int32_t L_33 = V_8;
		int32_t L_34 = V_9;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_006c;
		}
	}

IL_00c3:
	{
		bool L_35 = V_7;
		if (L_35)
		{
			goto IL_00fe;
		}
	}
	{
		InvStat_t3624028613 * L_36 = (InvStat_t3624028613 *)il2cpp_codegen_object_new(InvStat_t3624028613_il2cpp_TypeInfo_var);
		InvStat__ctor_m2855080774(L_36, /*hidden argument*/NULL);
		V_11 = L_36;
		InvStat_t3624028613 * L_37 = V_11;
		InvStat_t3624028613 * L_38 = V_5;
		NullCheck(L_38);
		int32_t L_39 = L_38->get_id_0();
		NullCheck(L_37);
		L_37->set_id_0(L_39);
		InvStat_t3624028613 * L_40 = V_11;
		int32_t L_41 = V_6;
		NullCheck(L_40);
		L_40->set_amount_2(L_41);
		InvStat_t3624028613 * L_42 = V_11;
		InvStat_t3624028613 * L_43 = V_5;
		NullCheck(L_43);
		int32_t L_44 = L_43->get_modifier_1();
		NullCheck(L_42);
		L_42->set_modifier_1(L_44);
		List_1_t126020286 * L_45 = V_0;
		InvStat_t3624028613 * L_46 = V_11;
		NullCheck(L_45);
		VirtActionInvoker1< InvStat_t3624028613 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<InvStat>::Add(!0) */, L_45, L_46);
	}

IL_00fe:
	{
		int32_t L_47 = V_3;
		V_3 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0102:
	{
		int32_t L_48 = V_3;
		int32_t L_49 = V_4;
		if ((((int32_t)L_48) < ((int32_t)L_49)))
		{
			goto IL_0033;
		}
	}
	{
		List_1_t126020286 * L_50 = V_0;
		IntPtr_t L_51;
		L_51.set_m_value_0((void*)(void*)InvStat_CompareArmor_m1514641866_MethodInfo_var);
		Comparison_1_t2032736193 * L_52 = (Comparison_1_t2032736193 *)il2cpp_codegen_object_new(Comparison_1_t2032736193_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m4235594531(L_52, NULL, L_51, /*hidden argument*/Comparison_1__ctor_m4235594531_MethodInfo_var);
		NullCheck(L_50);
		List_1_Sort_m3986012104(L_50, L_52, /*hidden argument*/List_1_Sort_m3986012104_MethodInfo_var);
	}

IL_011c:
	{
		List_1_t126020286 * L_53 = V_0;
		return L_53;
	}
}
// System.Void InvStat::.ctor()
extern "C"  void InvStat__ctor_m2855080774 (InvStat_t3624028613 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String InvStat::GetName(InvStat/Identifier)
extern Il2CppClass* Identifier_t375032009_il2cpp_TypeInfo_var;
extern const uint32_t InvStat_GetName_m1849497365_MetadataUsageId;
extern "C"  String_t* InvStat_GetName_m1849497365 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvStat_GetName_m1849497365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___i0;
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(Identifier_t375032009_il2cpp_TypeInfo_var, &L_1);
		NullCheck((Enum_t2778772662 *)L_2);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, (Enum_t2778772662 *)L_2);
		return L_3;
	}
}
// System.String InvStat::GetDescription(InvStat/Identifier)
extern Il2CppCodeGenString* _stringLiteral846460809;
extern Il2CppCodeGenString* _stringLiteral3652381264;
extern Il2CppCodeGenString* _stringLiteral255115477;
extern Il2CppCodeGenString* _stringLiteral749340151;
extern Il2CppCodeGenString* _stringLiteral2354128714;
extern Il2CppCodeGenString* _stringLiteral2545726634;
extern Il2CppCodeGenString* _stringLiteral482003562;
extern Il2CppCodeGenString* _stringLiteral527957966;
extern Il2CppCodeGenString* _stringLiteral2677084351;
extern const uint32_t InvStat_GetDescription_m2897270414_MetadataUsageId;
extern "C"  String_t* InvStat_GetDescription_m2897270414 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InvStat_GetDescription_m2897270414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___i0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0031;
		}
		if (L_1 == 1)
		{
			goto IL_0037;
		}
		if (L_1 == 2)
		{
			goto IL_003d;
		}
		if (L_1 == 3)
		{
			goto IL_0043;
		}
		if (L_1 == 4)
		{
			goto IL_0049;
		}
		if (L_1 == 5)
		{
			goto IL_004f;
		}
		if (L_1 == 6)
		{
			goto IL_0055;
		}
		if (L_1 == 7)
		{
			goto IL_005b;
		}
		if (L_1 == 8)
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0067;
	}

IL_0031:
	{
		return _stringLiteral846460809;
	}

IL_0037:
	{
		return _stringLiteral3652381264;
	}

IL_003d:
	{
		return _stringLiteral255115477;
	}

IL_0043:
	{
		return _stringLiteral749340151;
	}

IL_0049:
	{
		return _stringLiteral2354128714;
	}

IL_004f:
	{
		return _stringLiteral2545726634;
	}

IL_0055:
	{
		return _stringLiteral482003562;
	}

IL_005b:
	{
		return _stringLiteral527957966;
	}

IL_0061:
	{
		return _stringLiteral2677084351;
	}

IL_0067:
	{
		return (String_t*)NULL;
	}
}
// System.Int32 InvStat::CompareArmor(InvStat,InvStat)
extern "C"  int32_t InvStat_CompareArmor_m1514641866 (Il2CppObject * __this /* static, unused */, InvStat_t3624028613 * ___a0, InvStat_t3624028613 * ___b1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		InvStat_t3624028613 * L_0 = ___a0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_id_0();
		V_0 = L_1;
		InvStat_t3624028613 * L_2 = ___b1;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_id_0();
		V_1 = L_3;
		InvStat_t3624028613 * L_4 = ___a0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_id_0();
		if ((!(((uint32_t)L_5) == ((uint32_t)6))))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6-(int32_t)((int32_t)10000)));
		goto IL_003b;
	}

IL_0027:
	{
		InvStat_t3624028613 * L_7 = ___a0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_id_0();
		if ((!(((uint32_t)L_8) == ((uint32_t)4))))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9-(int32_t)((int32_t)5000)));
	}

IL_003b:
	{
		InvStat_t3624028613 * L_10 = ___b1;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_id_0();
		if ((!(((uint32_t)L_11) == ((uint32_t)6))))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12-(int32_t)((int32_t)10000)));
		goto IL_0068;
	}

IL_0054:
	{
		InvStat_t3624028613 * L_13 = ___b1;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_id_0();
		if ((!(((uint32_t)L_14) == ((uint32_t)4))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15-(int32_t)((int32_t)5000)));
	}

IL_0068:
	{
		InvStat_t3624028613 * L_16 = ___a0;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_amount_2();
		if ((((int32_t)L_17) >= ((int32_t)0)))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)((int32_t)1000)));
	}

IL_007c:
	{
		InvStat_t3624028613 * L_19 = ___b1;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_amount_2();
		if ((((int32_t)L_20) >= ((int32_t)0)))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_21 = V_1;
		V_1 = ((int32_t)((int32_t)L_21+(int32_t)((int32_t)1000)));
	}

IL_0090:
	{
		InvStat_t3624028613 * L_22 = ___a0;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_modifier_1();
		if ((!(((uint32_t)L_23) == ((uint32_t)1))))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)((int32_t)100)));
	}

IL_00a1:
	{
		InvStat_t3624028613 * L_25 = ___b1;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_modifier_1();
		if ((!(((uint32_t)L_26) == ((uint32_t)1))))
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)((int32_t)100)));
	}

IL_00b2:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) >= ((int32_t)L_29)))
		{
			goto IL_00bb;
		}
	}
	{
		return (-1);
	}

IL_00bb:
	{
		int32_t L_30 = V_0;
		int32_t L_31 = V_1;
		if ((((int32_t)L_30) <= ((int32_t)L_31)))
		{
			goto IL_00c4;
		}
	}
	{
		return 1;
	}

IL_00c4:
	{
		return 0;
	}
}
// System.Int32 InvStat::CompareWeapon(InvStat,InvStat)
extern "C"  int32_t InvStat_CompareWeapon_m1919480161 (Il2CppObject * __this /* static, unused */, InvStat_t3624028613 * ___a0, InvStat_t3624028613 * ___b1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		InvStat_t3624028613 * L_0 = ___a0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_id_0();
		V_0 = L_1;
		InvStat_t3624028613 * L_2 = ___b1;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_id_0();
		V_1 = L_3;
		InvStat_t3624028613 * L_4 = ___a0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_id_0();
		if ((!(((uint32_t)L_5) == ((uint32_t)4))))
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6-(int32_t)((int32_t)10000)));
		goto IL_003b;
	}

IL_0027:
	{
		InvStat_t3624028613 * L_7 = ___a0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_id_0();
		if ((!(((uint32_t)L_8) == ((uint32_t)6))))
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9-(int32_t)((int32_t)5000)));
	}

IL_003b:
	{
		InvStat_t3624028613 * L_10 = ___b1;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_id_0();
		if ((!(((uint32_t)L_11) == ((uint32_t)4))))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12-(int32_t)((int32_t)10000)));
		goto IL_0068;
	}

IL_0054:
	{
		InvStat_t3624028613 * L_13 = ___b1;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_id_0();
		if ((!(((uint32_t)L_14) == ((uint32_t)6))))
		{
			goto IL_0068;
		}
	}
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15-(int32_t)((int32_t)5000)));
	}

IL_0068:
	{
		InvStat_t3624028613 * L_16 = ___a0;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_amount_2();
		if ((((int32_t)L_17) >= ((int32_t)0)))
		{
			goto IL_007c;
		}
	}
	{
		int32_t L_18 = V_0;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)((int32_t)1000)));
	}

IL_007c:
	{
		InvStat_t3624028613 * L_19 = ___b1;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_amount_2();
		if ((((int32_t)L_20) >= ((int32_t)0)))
		{
			goto IL_0090;
		}
	}
	{
		int32_t L_21 = V_1;
		V_1 = ((int32_t)((int32_t)L_21+(int32_t)((int32_t)1000)));
	}

IL_0090:
	{
		InvStat_t3624028613 * L_22 = ___a0;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_modifier_1();
		if ((!(((uint32_t)L_23) == ((uint32_t)1))))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)((int32_t)100)));
	}

IL_00a1:
	{
		InvStat_t3624028613 * L_25 = ___b1;
		NullCheck(L_25);
		int32_t L_26 = L_25->get_modifier_1();
		if ((!(((uint32_t)L_26) == ((uint32_t)1))))
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_27+(int32_t)((int32_t)100)));
	}

IL_00b2:
	{
		int32_t L_28 = V_0;
		int32_t L_29 = V_1;
		if ((((int32_t)L_28) >= ((int32_t)L_29)))
		{
			goto IL_00bb;
		}
	}
	{
		return (-1);
	}

IL_00bb:
	{
		int32_t L_30 = V_0;
		int32_t L_31 = V_1;
		if ((((int32_t)L_30) <= ((int32_t)L_31)))
		{
			goto IL_00c4;
		}
	}
	{
		return 1;
	}

IL_00c4:
	{
		return 0;
	}
}
// System.Void LagPosition::.ctor()
extern "C"  void LagPosition__ctor_m3796437264 (LagPosition_t2611909819 * __this, const MethodInfo* method)
{
	{
		Vector3_t3525329789  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (10.0f), (10.0f), (10.0f), /*hidden argument*/NULL);
		__this->set_speed_2(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LagPosition::OnRepositionEnd()
extern "C"  void LagPosition_OnRepositionEnd_m72961326 (LagPosition_t2611909819 * __this, const MethodInfo* method)
{
	{
		LagPosition_Interpolate_m3387977948(__this, (1000.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void LagPosition::Interpolate(System.Single)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t LagPosition_Interpolate_m3387977948_MetadataUsageId;
extern "C"  void LagPosition_Interpolate_m3387977948 (LagPosition_t2611909819 * __this, float ___delta0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LagPosition_Interpolate_m3387977948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t284553113 * V_0 = NULL;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t284553113 * L_0 = __this->get_mTrans_4();
		NullCheck(L_0);
		Transform_t284553113 * L_1 = Transform_get_parent_m2236876972(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Transform_t284553113 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00e2;
		}
	}
	{
		Transform_t284553113 * L_4 = V_0;
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Transform_t284553113 * L_6 = V_0;
		NullCheck(L_6);
		Quaternion_t1891715979  L_7 = Transform_get_rotation_m11483428(L_6, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8 = __this->get_mRelative_5();
		Vector3_t3525329789  L_9 = Quaternion_op_Multiply_m3771288979(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		Vector3_t3525329789  L_10 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_5, L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		Vector3_t3525329789 * L_11 = __this->get_address_of_mAbsolute_6();
		Vector3_t3525329789 * L_12 = __this->get_address_of_mAbsolute_6();
		float L_13 = L_12->get_x_1();
		float L_14 = (&V_1)->get_x_1();
		float L_15 = ___delta0;
		Vector3_t3525329789 * L_16 = __this->get_address_of_speed_2();
		float L_17 = L_16->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_18 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, ((float)((float)L_15*(float)L_17)), /*hidden argument*/NULL);
		float L_19 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_13, L_14, L_18, /*hidden argument*/NULL);
		L_11->set_x_1(L_19);
		Vector3_t3525329789 * L_20 = __this->get_address_of_mAbsolute_6();
		Vector3_t3525329789 * L_21 = __this->get_address_of_mAbsolute_6();
		float L_22 = L_21->get_y_2();
		float L_23 = (&V_1)->get_y_2();
		float L_24 = ___delta0;
		Vector3_t3525329789 * L_25 = __this->get_address_of_speed_2();
		float L_26 = L_25->get_y_2();
		float L_27 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, ((float)((float)L_24*(float)L_26)), /*hidden argument*/NULL);
		float L_28 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_22, L_23, L_27, /*hidden argument*/NULL);
		L_20->set_y_2(L_28);
		Vector3_t3525329789 * L_29 = __this->get_address_of_mAbsolute_6();
		Vector3_t3525329789 * L_30 = __this->get_address_of_mAbsolute_6();
		float L_31 = L_30->get_z_3();
		float L_32 = (&V_1)->get_z_3();
		float L_33 = ___delta0;
		Vector3_t3525329789 * L_34 = __this->get_address_of_speed_2();
		float L_35 = L_34->get_z_3();
		float L_36 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, ((float)((float)L_33*(float)L_35)), /*hidden argument*/NULL);
		float L_37 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_31, L_32, L_36, /*hidden argument*/NULL);
		L_29->set_z_3(L_37);
		Transform_t284553113 * L_38 = __this->get_mTrans_4();
		Vector3_t3525329789  L_39 = __this->get_mAbsolute_6();
		NullCheck(L_38);
		Transform_set_position_m3111394108(L_38, L_39, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		return;
	}
}
// System.Void LagPosition::Awake()
extern "C"  void LagPosition_Awake_m4034042483 (LagPosition_t2611909819 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_mTrans_4(L_0);
		return;
	}
}
// System.Void LagPosition::OnEnable()
extern "C"  void LagPosition_OnEnable_m2360304758 (LagPosition_t2611909819 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mStarted_7();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		LagPosition_ResetPosition_m1763100358(__this, /*hidden argument*/NULL);
	}

IL_0011:
	{
		return;
	}
}
// System.Void LagPosition::Start()
extern "C"  void LagPosition_Start_m2743575056 (LagPosition_t2611909819 * __this, const MethodInfo* method)
{
	{
		__this->set_mStarted_7((bool)1);
		LagPosition_ResetPosition_m1763100358(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LagPosition::ResetPosition()
extern "C"  void LagPosition_ResetPosition_m1763100358 (LagPosition_t2611909819 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = __this->get_mTrans_4();
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		__this->set_mAbsolute_6(L_1);
		Transform_t284553113 * L_2 = __this->get_mTrans_4();
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_localPosition_m668140784(L_2, /*hidden argument*/NULL);
		__this->set_mRelative_5(L_3);
		return;
	}
}
// System.Void LagPosition::Update()
extern "C"  void LagPosition_Update_m3452300285 (LagPosition_t2611909819 * __this, const MethodInfo* method)
{
	LagPosition_t2611909819 * G_B2_0 = NULL;
	LagPosition_t2611909819 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	LagPosition_t2611909819 * G_B3_1 = NULL;
	{
		bool L_0 = __this->get_ignoreTimeScale_3();
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0016;
		}
	}
	{
		float L_1 = RealTime_get_deltaTime_m2274453566(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_001b;
	}

IL_0016:
	{
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_001b:
	{
		NullCheck(G_B3_1);
		LagPosition_Interpolate_m3387977948(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LagRotation::.ctor()
extern "C"  void LagRotation__ctor_m3465142875 (LagRotation_t1823804176 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_2((10.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LagRotation::OnRepositionEnd()
extern "C"  void LagRotation_OnRepositionEnd_m455034041 (LagRotation_t1823804176 * __this, const MethodInfo* method)
{
	{
		LagRotation_Interpolate_m13682097(__this, (1000.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void LagRotation::Interpolate(System.Single)
extern "C"  void LagRotation_Interpolate_m13682097 (LagRotation_t1823804176 * __this, float ___delta0, const MethodInfo* method)
{
	Transform_t284553113 * V_0 = NULL;
	{
		Transform_t284553113 * L_0 = __this->get_mTrans_4();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0064;
		}
	}
	{
		Transform_t284553113 * L_2 = __this->get_mTrans_4();
		NullCheck(L_2);
		Transform_t284553113 * L_3 = Transform_get_parent_m2236876972(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t284553113 * L_4 = V_0;
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0064;
		}
	}
	{
		Quaternion_t1891715979  L_6 = __this->get_mAbsolute_6();
		Transform_t284553113 * L_7 = V_0;
		NullCheck(L_7);
		Quaternion_t1891715979  L_8 = Transform_get_rotation_m11483428(L_7, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_9 = __this->get_mRelative_5();
		Quaternion_t1891715979  L_10 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		float L_11 = ___delta0;
		float L_12 = __this->get_speed_2();
		Quaternion_t1891715979  L_13 = Quaternion_Slerp_m844700366(NULL /*static, unused*/, L_6, L_10, ((float)((float)L_11*(float)L_12)), /*hidden argument*/NULL);
		__this->set_mAbsolute_6(L_13);
		Transform_t284553113 * L_14 = __this->get_mTrans_4();
		Quaternion_t1891715979  L_15 = __this->get_mAbsolute_6();
		NullCheck(L_14);
		Transform_set_rotation_m1525803229(L_14, L_15, /*hidden argument*/NULL);
	}

IL_0064:
	{
		return;
	}
}
// System.Void LagRotation::Start()
extern "C"  void LagRotation_Start_m2412280667 (LagRotation_t1823804176 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_mTrans_4(L_0);
		Transform_t284553113 * L_1 = __this->get_mTrans_4();
		NullCheck(L_1);
		Quaternion_t1891715979  L_2 = Transform_get_localRotation_m3343229381(L_1, /*hidden argument*/NULL);
		__this->set_mRelative_5(L_2);
		Transform_t284553113 * L_3 = __this->get_mTrans_4();
		NullCheck(L_3);
		Quaternion_t1891715979  L_4 = Transform_get_rotation_m11483428(L_3, /*hidden argument*/NULL);
		__this->set_mAbsolute_6(L_4);
		return;
	}
}
// System.Void LagRotation::Update()
extern "C"  void LagRotation_Update_m1772108818 (LagRotation_t1823804176 * __this, const MethodInfo* method)
{
	LagRotation_t1823804176 * G_B2_0 = NULL;
	LagRotation_t1823804176 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	LagRotation_t1823804176 * G_B3_1 = NULL;
	{
		bool L_0 = __this->get_ignoreTimeScale_3();
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0016;
		}
	}
	{
		float L_1 = RealTime_get_deltaTime_m2274453566(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_001b;
	}

IL_0016:
	{
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_001b:
	{
		NullCheck(G_B3_1);
		LagRotation_Interpolate_m13682097(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LanguageSelection::.ctor()
extern "C"  void LanguageSelection__ctor_m986906359 (LanguageSelection_t764444916 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LanguageSelection::Awake()
extern const MethodInfo* Component_GetComponent_TisUIPopupList_t1804933942_m1506054211_MethodInfo_var;
extern const uint32_t LanguageSelection_Awake_m1224511578_MetadataUsageId;
extern "C"  void LanguageSelection_Awake_m1224511578 (LanguageSelection_t764444916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LanguageSelection_Awake_m1224511578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIPopupList_t1804933942 * L_0 = Component_GetComponent_TisUIPopupList_t1804933942_m1506054211(__this, /*hidden argument*/Component_GetComponent_TisUIPopupList_t1804933942_m1506054211_MethodInfo_var);
		__this->set_mList_2(L_0);
		LanguageSelection_Refresh_m2607036208(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LanguageSelection::Start()
extern Il2CppClass* LanguageSelection_t764444916_il2cpp_TypeInfo_var;
extern Il2CppClass* Callback_t4187391077_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const MethodInfo* LanguageSelection_U3CStartU3Em__0_m4016999088_MethodInfo_var;
extern const uint32_t LanguageSelection_Start_m4229011447_MetadataUsageId;
extern "C"  void LanguageSelection_Start_m4229011447 (LanguageSelection_t764444916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LanguageSelection_Start_m4229011447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t506415896 * G_B2_0 = NULL;
	List_1_t506415896 * G_B1_0 = NULL;
	{
		UIPopupList_t1804933942 * L_0 = __this->get_mList_2();
		NullCheck(L_0);
		List_1_t506415896 * L_1 = L_0->get_onChange_27();
		Callback_t4187391077 * L_2 = ((LanguageSelection_t764444916_StaticFields*)LanguageSelection_t764444916_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		G_B1_0 = L_1;
		if (L_2)
		{
			G_B2_0 = L_1;
			goto IL_0023;
		}
	}
	{
		IntPtr_t L_3;
		L_3.set_m_value_0((void*)(void*)LanguageSelection_U3CStartU3Em__0_m4016999088_MethodInfo_var);
		Callback_t4187391077 * L_4 = (Callback_t4187391077 *)il2cpp_codegen_object_new(Callback_t4187391077_il2cpp_TypeInfo_var);
		Callback__ctor_m3018475132(L_4, NULL, L_3, /*hidden argument*/NULL);
		((LanguageSelection_t764444916_StaticFields*)LanguageSelection_t764444916_il2cpp_TypeInfo_var->static_fields)->set_U3CU3Ef__amU24cache1_3(L_4);
		G_B2_0 = G_B1_0;
	}

IL_0023:
	{
		Callback_t4187391077 * L_5 = ((LanguageSelection_t764444916_StaticFields*)LanguageSelection_t764444916_il2cpp_TypeInfo_var->static_fields)->get_U3CU3Ef__amU24cache1_3();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1811262575(NULL /*static, unused*/, G_B2_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LanguageSelection::Refresh()
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t LanguageSelection_Refresh_m2607036208_MetadataUsageId;
extern "C"  void LanguageSelection_Refresh_m2607036208 (LanguageSelection_t764444916 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LanguageSelection_Refresh_m2607036208_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		UIPopupList_t1804933942 * L_0 = __this->get_mList_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0067;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_2 = Localization_get_knownLanguages_m431207172(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0067;
		}
	}
	{
		UIPopupList_t1804933942 * L_3 = __this->get_mList_2();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(7 /* System.Void UIPopupList::Clear() */, L_3);
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_4 = Localization_get_knownLanguages_m431207172(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))));
		goto IL_0050;
	}

IL_0035:
	{
		UIPopupList_t1804933942 * L_5 = __this->get_mList_2();
		NullCheck(L_5);
		List_1_t1765447871 * L_6 = L_5->get_items_17();
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_7 = Localization_get_knownLanguages_m431207172(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_6, ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))));
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0050:
	{
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0035;
		}
	}
	{
		UIPopupList_t1804933942 * L_13 = __this->get_mList_2();
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		String_t* L_14 = Localization_get_language_m4046813542(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void UIPopupList::set_value(System.String) */, L_13, L_14);
	}

IL_0067:
	{
		return;
	}
}
// System.Void LanguageSelection::<Start>m__0()
extern Il2CppClass* UIPopupList_t1804933942_il2cpp_TypeInfo_var;
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t LanguageSelection_U3CStartU3Em__0_m4016999088_MetadataUsageId;
extern "C"  void LanguageSelection_U3CStartU3Em__0_m4016999088 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LanguageSelection_U3CStartU3Em__0_m4016999088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIPopupList_t1804933942_il2cpp_TypeInfo_var);
		UIPopupList_t1804933942 * L_0 = ((UIPopupList_t1804933942_StaticFields*)UIPopupList_t1804933942_il2cpp_TypeInfo_var->static_fields)->get_current_3();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(4 /* System.String UIPopupList::get_value() */, L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Localization_set_language_m411704107(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadLevelOnClick::.ctor()
extern "C"  void LoadLevelOnClick__ctor_m1644310544 (LoadLevelOnClick_t3005058795 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadLevelOnClick::OnClick()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t LoadLevelOnClick_OnClick_m1478673047_MetadataUsageId;
extern "C"  void LoadLevelOnClick_OnClick_m1478673047 (LoadLevelOnClick_t3005058795 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoadLevelOnClick_OnClick_m1478673047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_levelName_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_2 = __this->get_levelName_2();
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}
}
// System.Void Localization::.cctor()
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t2606186806_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t299600851_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1597524044_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1120242542_MethodInfo_var;
extern const uint32_t Localization__cctor_m1191965675_MetadataUsageId;
extern "C"  void Localization__cctor_m1191965675 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization__cctor_m1191965675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_localizationHasBeenSet_2((bool)0);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguages_3((StringU5BU5D_t2956870243*)NULL);
		Dictionary_2_t2606186806 * L_0 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1597524044(L_0, /*hidden argument*/Dictionary_2__ctor_m1597524044_MethodInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mOldDictionary_4(L_0);
		Dictionary_2_t299600851 * L_1 = (Dictionary_2_t299600851 *)il2cpp_codegen_object_new(Dictionary_2_t299600851_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1120242542(L_1, /*hidden argument*/Dictionary_2__ctor_m1120242542_MethodInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mDictionary_5(L_1);
		Dictionary_2_t2606186806 * L_2 = (Dictionary_2_t2606186806 *)il2cpp_codegen_object_new(Dictionary_2_t2606186806_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1597524044(L_2, /*hidden argument*/Dictionary_2__ctor_m1597524044_MethodInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mReplacement_6(L_2);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguageIndex_7((-1));
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mMerging_9((bool)0);
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.String[]> Localization::get_dictionary()
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746021752;
extern Il2CppCodeGenString* _stringLiteral60895824;
extern const uint32_t Localization_get_dictionary_m2295583953_MetadataUsageId;
extern "C"  Dictionary_2_t299600851 * Localization_get_dictionary_m2295583953 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_get_dictionary_m2295583953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_localizationHasBeenSet_2();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_1 = PlayerPrefs_GetString_m3230559948(NULL /*static, unused*/, _stringLiteral2746021752, _stringLiteral60895824, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Localization_LoadDictionary_m3777629616(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_2 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		return L_2;
	}
}
// System.Void Localization::set_dictionary(System.Collections.Generic.Dictionary`2<System.String,System.String[]>)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t Localization_set_dictionary_m2122357832_MetadataUsageId;
extern "C"  void Localization_set_dictionary_m2122357832 (Il2CppObject * __this /* static, unused */, Dictionary_2_t299600851 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_set_dictionary_m2122357832_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t299600851 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_localizationHasBeenSet_2((bool)((((int32_t)((((Il2CppObject*)(Dictionary_2_t299600851 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		Dictionary_2_t299600851 * L_1 = ___value0;
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mDictionary_5(L_1);
		return;
	}
}
// System.String[] Localization::get_knownLanguages()
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746021752;
extern Il2CppCodeGenString* _stringLiteral60895824;
extern const uint32_t Localization_get_knownLanguages_m431207172_MetadataUsageId;
extern "C"  StringU5BU5D_t2956870243* Localization_get_knownLanguages_m431207172 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_get_knownLanguages_m431207172_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_localizationHasBeenSet_2();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		String_t* L_1 = PlayerPrefs_GetString_m3230559948(NULL /*static, unused*/, _stringLiteral2746021752, _stringLiteral60895824, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Localization_LoadDictionary_m3777629616(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_001f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_2 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		return L_2;
	}
}
// System.String Localization::get_language()
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746021752;
extern Il2CppCodeGenString* _stringLiteral60895824;
extern const uint32_t Localization_get_language_m4046813542_MetadataUsageId;
extern "C"  String_t* Localization_get_language_m4046813542 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_get_language_m4046813542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguage_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		String_t* L_2 = PlayerPrefs_GetString_m3230559948(NULL /*static, unused*/, _stringLiteral2746021752, _stringLiteral60895824, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguage_8(L_2);
		String_t* L_3 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguage_8();
		Localization_LoadAndSelect_m1822437257(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		String_t* L_4 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguage_8();
		return L_4;
	}
}
// System.Void Localization::set_language(System.String)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Localization_set_language_m411704107_MetadataUsageId;
extern "C"  void Localization_set_language_m411704107 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_set_language_m411704107_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		String_t* L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguage_8();
		String_t* L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		String_t* L_3 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguage_8(L_3);
		String_t* L_4 = ___value0;
		Localization_LoadAndSelect_m1822437257(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Localization::LoadDictionary(System.String)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3647281849;
extern const uint32_t Localization_LoadDictionary_m3777629616_MetadataUsageId;
extern "C"  bool Localization_LoadDictionary_m3777629616 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_LoadDictionary_m3777629616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t58506160* V_0 = NULL;
	TextAsset_t2461560304 * V_1 = NULL;
	TextAsset_t2461560304 * V_2 = NULL;
	{
		V_0 = (ByteU5BU5D_t58506160*)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_localizationHasBeenSet_2();
		if (L_0)
		{
			goto IL_004f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		LoadFunction_t2829036286 * L_1 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_loadFunction_0();
		if (L_1)
		{
			goto IL_0039;
		}
	}
	{
		TextAsset_t2461560304 * L_2 = Resources_Load_TisTextAsset_t2461560304_m585700060(NULL /*static, unused*/, _stringLiteral3647281849, /*hidden argument*/Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var);
		V_1 = L_2;
		TextAsset_t2461560304 * L_3 = V_1;
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0034;
		}
	}
	{
		TextAsset_t2461560304 * L_5 = V_1;
		NullCheck(L_5);
		ByteU5BU5D_t58506160* L_6 = TextAsset_get_bytes_m2395427488(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
	}

IL_0034:
	{
		goto IL_0049;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		LoadFunction_t2829036286 * L_7 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_loadFunction_0();
		NullCheck(L_7);
		ByteU5BU5D_t58506160* L_8 = LoadFunction_Invoke_m2234063239(L_7, _stringLiteral3647281849, /*hidden argument*/NULL);
		V_0 = L_8;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_localizationHasBeenSet_2((bool)1);
	}

IL_004f:
	{
		ByteU5BU5D_t58506160* L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_10 = Localization_LoadCSV_m1504216576(NULL /*static, unused*/, L_9, (bool)0, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_005d;
		}
	}
	{
		return (bool)1;
	}

IL_005d:
	{
		String_t* L_11 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_12 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_006f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		String_t* L_13 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguage_8();
		___value0 = L_13;
	}

IL_006f:
	{
		String_t* L_14 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_15 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007c;
		}
	}
	{
		return (bool)0;
	}

IL_007c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		LoadFunction_t2829036286 * L_16 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_loadFunction_0();
		if (L_16)
		{
			goto IL_00a5;
		}
	}
	{
		String_t* L_17 = ___value0;
		TextAsset_t2461560304 * L_18 = Resources_Load_TisTextAsset_t2461560304_m585700060(NULL /*static, unused*/, L_17, /*hidden argument*/Resources_Load_TisTextAsset_t2461560304_m585700060_MethodInfo_var);
		V_2 = L_18;
		TextAsset_t2461560304 * L_19 = V_2;
		bool L_20 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_19, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00a0;
		}
	}
	{
		TextAsset_t2461560304 * L_21 = V_2;
		NullCheck(L_21);
		ByteU5BU5D_t58506160* L_22 = TextAsset_get_bytes_m2395427488(L_21, /*hidden argument*/NULL);
		V_0 = L_22;
	}

IL_00a0:
	{
		goto IL_00b1;
	}

IL_00a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		LoadFunction_t2829036286 * L_23 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_loadFunction_0();
		String_t* L_24 = ___value0;
		NullCheck(L_23);
		ByteU5BU5D_t58506160* L_25 = LoadFunction_Invoke_m2234063239(L_23, L_24, /*hidden argument*/NULL);
		V_0 = L_25;
	}

IL_00b1:
	{
		ByteU5BU5D_t58506160* L_26 = V_0;
		if (!L_26)
		{
			goto IL_00c0;
		}
	}
	{
		String_t* L_27 = ___value0;
		ByteU5BU5D_t58506160* L_28 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Localization_Set_m3157847683(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_00c0:
	{
		return (bool)0;
	}
}
// System.Boolean Localization::LoadAndSelect(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746021752;
extern const uint32_t Localization_LoadAndSelect_m1822437257_MetadataUsageId;
extern "C"  bool Localization_LoadAndSelect_m1822437257 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_LoadAndSelect_m1822437257_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0034;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_2 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String[]>::get_Count() */, L_2);
		if (L_3)
		{
			goto IL_0027;
		}
	}
	{
		String_t* L_4 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_5 = Localization_LoadDictionary_m3777629616(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0027;
		}
	}
	{
		return (bool)0;
	}

IL_0027:
	{
		String_t* L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_7 = Localization_SelectLanguage_m3438398552(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0034;
		}
	}
	{
		return (bool)1;
	}

IL_0034:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_8 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String>::get_Count() */, L_8);
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0046;
		}
	}
	{
		return (bool)1;
	}

IL_0046:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_10 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		NullCheck(L_10);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Clear() */, L_10);
		Dictionary_2_t299600851 * L_11 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		NullCheck(L_11);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String[]>::Clear() */, L_11);
		String_t* L_12 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_13 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_006f;
		}
	}
	{
		PlayerPrefs_DeleteKey_m1547199302(NULL /*static, unused*/, _stringLiteral2746021752, /*hidden argument*/NULL);
	}

IL_006f:
	{
		return (bool)0;
	}
}
// System.Void Localization::Load(UnityEngine.TextAsset)
extern Il2CppClass* ByteReader_t2446302219_il2cpp_TypeInfo_var;
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t Localization_Load_m4202658910_MetadataUsageId;
extern "C"  void Localization_Load_m4202658910 (Il2CppObject * __this /* static, unused */, TextAsset_t2461560304 * ___asset0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_Load_m4202658910_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteReader_t2446302219 * V_0 = NULL;
	{
		TextAsset_t2461560304 * L_0 = ___asset0;
		ByteReader_t2446302219 * L_1 = (ByteReader_t2446302219 *)il2cpp_codegen_object_new(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		ByteReader__ctor_m3209705686(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		TextAsset_t2461560304 * L_2 = ___asset0;
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m3709440845(L_2, /*hidden argument*/NULL);
		ByteReader_t2446302219 * L_4 = V_0;
		NullCheck(L_4);
		Dictionary_2_t2606186806 * L_5 = ByteReader_ReadDictionary_m2502625102(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Localization_Set_m3679996727(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Localization::Set(System.String,System.Byte[])
extern Il2CppClass* ByteReader_t2446302219_il2cpp_TypeInfo_var;
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t Localization_Set_m3157847683_MetadataUsageId;
extern "C"  void Localization_Set_m3157847683 (Il2CppObject * __this /* static, unused */, String_t* ___languageName0, ByteU5BU5D_t58506160* ___bytes1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_Set_m3157847683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteReader_t2446302219 * V_0 = NULL;
	{
		ByteU5BU5D_t58506160* L_0 = ___bytes1;
		ByteReader_t2446302219 * L_1 = (ByteReader_t2446302219 *)il2cpp_codegen_object_new(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		ByteReader__ctor_m417508569(L_1, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ___languageName0;
		ByteReader_t2446302219 * L_3 = V_0;
		NullCheck(L_3);
		Dictionary_2_t2606186806 * L_4 = ByteReader_ReadDictionary_m2502625102(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Localization_Set_m3679996727(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Localization::ReplaceKey(System.String,System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t Localization_ReplaceKey_m3861312273_MetadataUsageId;
extern "C"  void Localization_ReplaceKey_m3861312273 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___val1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_ReplaceKey_m3861312273_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___val1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_2 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mReplacement_6();
		String_t* L_3 = ___key0;
		String_t* L_4 = ___val1;
		NullCheck(L_2);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(25 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_2, L_3, L_4);
		goto IL_0028;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_5 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mReplacement_6();
		String_t* L_6 = ___key0;
		NullCheck(L_5);
		VirtFuncInvoker1< bool, String_t* >::Invoke(30 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::Remove(!0) */, L_5, L_6);
	}

IL_0028:
	{
		return;
	}
}
// System.Void Localization::ClearReplacements()
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t Localization_ClearReplacements_m2220150446_MetadataUsageId;
extern "C"  void Localization_ClearReplacements_m2220150446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_ClearReplacements_m2220150446_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mReplacement_6();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Clear() */, L_0);
		return;
	}
}
// System.Boolean Localization::LoadCSV(UnityEngine.TextAsset,System.Boolean)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t Localization_LoadCSV_m727032931_MetadataUsageId;
extern "C"  bool Localization_LoadCSV_m727032931 (Il2CppObject * __this /* static, unused */, TextAsset_t2461560304 * ___asset0, bool ___merge1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_LoadCSV_m727032931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		TextAsset_t2461560304 * L_0 = ___asset0;
		NullCheck(L_0);
		ByteU5BU5D_t58506160* L_1 = TextAsset_get_bytes_m2395427488(L_0, /*hidden argument*/NULL);
		TextAsset_t2461560304 * L_2 = ___asset0;
		bool L_3 = ___merge1;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_4 = Localization_LoadCSV_m2021377728(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Boolean Localization::LoadCSV(System.Byte[],System.Boolean)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t Localization_LoadCSV_m1504216576_MetadataUsageId;
extern "C"  bool Localization_LoadCSV_m1504216576 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___bytes0, bool ___merge1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_LoadCSV_m1504216576_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t58506160* L_0 = ___bytes0;
		bool L_1 = ___merge1;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_2 = Localization_LoadCSV_m2021377728(NULL /*static, unused*/, L_0, (TextAsset_t2461560304 *)NULL, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean Localization::HasLanguage(System.String)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Localization_HasLanguage_m484554116_MetadataUsageId;
extern "C"  bool Localization_HasLanguage_m484554116 (Il2CppObject * __this /* static, unused */, String_t* ___languageName0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_HasLanguage_m484554116_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_0);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		goto IL_0027;
	}

IL_000f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_1 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		String_t* L_4 = ___languageName0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1260523650(NULL /*static, unused*/, ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3))), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0023;
		}
	}
	{
		return (bool)1;
	}

IL_0023:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean Localization::LoadCSV(System.Byte[],UnityEngine.TextAsset,System.Boolean)
extern Il2CppClass* ByteReader_t2446302219_il2cpp_TypeInfo_var;
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t299600851_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t66628792_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t190145395_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1_RemoveAt_m1844819628_MethodInfo_var;
extern const MethodInfo* BetterList_1_get_Item_m4060667538_MethodInfo_var;
extern const MethodInfo* Array_Resize_TisString_t_m939990198_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1120242542_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1889285220_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3797208814_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2149107613_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2631615050_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1312278737_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m642355421_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746021752;
extern const uint32_t Localization_LoadCSV_m2021377728_MetadataUsageId;
extern "C"  bool Localization_LoadCSV_m2021377728 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t58506160* ___bytes0, TextAsset_t2461560304 * ___asset1, bool ___merge2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_LoadCSV_m2021377728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteReader_t2446302219 * V_0 = NULL;
	BetterList_1_t2465456914 * V_1 = NULL;
	StringU5BU5D_t2956870243* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Dictionary_2_t299600851 * V_7 = NULL;
	KeyValuePair_2_t4083099445  V_8;
	memset(&V_8, 0, sizeof(V_8));
	Enumerator_t66628792  V_9;
	memset(&V_9, 0, sizeof(V_9));
	StringU5BU5D_t2956870243* V_10 = NULL;
	Dictionary_2_t190145395 * V_11 = NULL;
	int32_t V_12 = 0;
	BetterList_1_t2465456914 * V_13 = NULL;
	OnLocalizeNotification_t1642290739 * V_14 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		ByteU5BU5D_t58506160* L_0 = ___bytes0;
		if (L_0)
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}

IL_0008:
	{
		ByteU5BU5D_t58506160* L_1 = ___bytes0;
		ByteReader_t2446302219 * L_2 = (ByteReader_t2446302219 *)il2cpp_codegen_object_new(ByteReader_t2446302219_il2cpp_TypeInfo_var);
		ByteReader__ctor_m417508569(L_2, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ByteReader_t2446302219 * L_3 = V_0;
		NullCheck(L_3);
		BetterList_1_t2465456914 * L_4 = ByteReader_ReadCSV_m2254485814(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		BetterList_1_t2465456914 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_size_1();
		if ((((int32_t)L_6) >= ((int32_t)2)))
		{
			goto IL_0024;
		}
	}
	{
		return (bool)0;
	}

IL_0024:
	{
		BetterList_1_t2465456914 * L_7 = V_1;
		NullCheck(L_7);
		BetterList_1_RemoveAt_m1844819628(L_7, 0, /*hidden argument*/BetterList_1_RemoveAt_m1844819628_MethodInfo_var);
		V_2 = (StringU5BU5D_t2956870243*)NULL;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		String_t* L_8 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguage_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_localizationHasBeenSet_2((bool)0);
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_10 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_localizationHasBeenSet_2();
		if (!L_10)
		{
			goto IL_0072;
		}
	}
	{
		bool L_11 = ___merge2;
		if (L_11)
		{
			goto IL_005c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_12 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mMerging_9();
		if (!L_12)
		{
			goto IL_0072;
		}
	}

IL_005c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_13 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		if (!L_13)
		{
			goto IL_0072;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_14 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_14);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))
		{
			goto IL_00f8;
		}
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_15 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		NullCheck(L_15);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String[]>::Clear() */, L_15);
		BetterList_1_t2465456914 * L_16 = V_1;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_size_1();
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguages_3(((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_17)));
		bool L_18 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_localizationHasBeenSet_2();
		if (L_18)
		{
			goto IL_00b2;
		}
	}
	{
		BetterList_1_t2465456914 * L_19 = V_1;
		NullCheck(L_19);
		String_t* L_20 = BetterList_1_get_Item_m4060667538(L_19, 0, /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		String_t* L_21 = PlayerPrefs_GetString_m3230559948(NULL /*static, unused*/, _stringLiteral2746021752, L_20, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguage_8(L_21);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_localizationHasBeenSet_2((bool)1);
	}

IL_00b2:
	{
		V_3 = 0;
		goto IL_00e7;
	}

IL_00b9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_22 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		int32_t L_23 = V_3;
		BetterList_1_t2465456914 * L_24 = V_1;
		int32_t L_25 = V_3;
		NullCheck(L_24);
		String_t* L_26 = BetterList_1_get_Item_m4060667538(L_24, L_25, /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		ArrayElementTypeCheck (L_22, L_26);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(L_23), (String_t*)L_26);
		StringU5BU5D_t2956870243* L_27 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		int32_t L_28 = V_3;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		int32_t L_29 = L_28;
		String_t* L_30 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguage_8();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_31 = String_op_Equality_m1260523650(NULL /*static, unused*/, ((L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29))), L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_00e3;
		}
	}
	{
		int32_t L_32 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguageIndex_7(L_32);
	}

IL_00e3:
	{
		int32_t L_33 = V_3;
		V_3 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00e7:
	{
		int32_t L_34 = V_3;
		BetterList_1_t2465456914 * L_35 = V_1;
		NullCheck(L_35);
		int32_t L_36 = L_35->get_size_1();
		if ((((int32_t)L_34) < ((int32_t)L_36)))
		{
			goto IL_00b9;
		}
	}
	{
		goto IL_01f4;
	}

IL_00f8:
	{
		BetterList_1_t2465456914 * L_37 = V_1;
		NullCheck(L_37);
		int32_t L_38 = L_37->get_size_1();
		V_2 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)L_38));
		V_4 = 0;
		goto IL_011e;
	}

IL_010c:
	{
		StringU5BU5D_t2956870243* L_39 = V_2;
		int32_t L_40 = V_4;
		BetterList_1_t2465456914 * L_41 = V_1;
		int32_t L_42 = V_4;
		NullCheck(L_41);
		String_t* L_43 = BetterList_1_get_Item_m4060667538(L_41, L_42, /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		ArrayElementTypeCheck (L_39, L_43);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(L_40), (String_t*)L_43);
		int32_t L_44 = V_4;
		V_4 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_011e:
	{
		int32_t L_45 = V_4;
		BetterList_1_t2465456914 * L_46 = V_1;
		NullCheck(L_46);
		int32_t L_47 = L_46->get_size_1();
		if ((((int32_t)L_45) < ((int32_t)L_47)))
		{
			goto IL_010c;
		}
	}
	{
		V_5 = 0;
		goto IL_01e7;
	}

IL_0133:
	{
		BetterList_1_t2465456914 * L_48 = V_1;
		int32_t L_49 = V_5;
		NullCheck(L_48);
		String_t* L_50 = BetterList_1_get_Item_m4060667538(L_48, L_49, /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_51 = Localization_HasLanguage_m484554116(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		if (L_51)
		{
			goto IL_01e1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_52 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_52);
		V_6 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_52)->max_length))))+(int32_t)1));
		int32_t L_53 = V_6;
		Array_Resize_TisString_t_m939990198(NULL /*static, unused*/, (((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_address_of_mLanguages_3()), L_53, /*hidden argument*/Array_Resize_TisString_t_m939990198_MethodInfo_var);
		StringU5BU5D_t2956870243* L_54 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		int32_t L_55 = V_6;
		BetterList_1_t2465456914 * L_56 = V_1;
		int32_t L_57 = V_5;
		NullCheck(L_56);
		String_t* L_58 = BetterList_1_get_Item_m4060667538(L_56, L_57, /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)((int32_t)L_55-(int32_t)1)));
		ArrayElementTypeCheck (L_54, L_58);
		(L_54)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_55-(int32_t)1))), (String_t*)L_58);
		Dictionary_2_t299600851 * L_59 = (Dictionary_2_t299600851 *)il2cpp_codegen_object_new(Dictionary_2_t299600851_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1120242542(L_59, /*hidden argument*/Dictionary_2__ctor_m1120242542_MethodInfo_var);
		V_7 = L_59;
		Dictionary_2_t299600851 * L_60 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		NullCheck(L_60);
		Enumerator_t66628792  L_61 = Dictionary_2_GetEnumerator_m1889285220(L_60, /*hidden argument*/Dictionary_2_GetEnumerator_m1889285220_MethodInfo_var);
		V_9 = L_61;
	}

IL_0181:
	try
	{ // begin try (depth: 1)
		{
			goto IL_01bc;
		}

IL_0186:
		{
			KeyValuePair_2_t4083099445  L_62 = Enumerator_get_Current_m3797208814((&V_9), /*hidden argument*/Enumerator_get_Current_m3797208814_MethodInfo_var);
			V_8 = L_62;
			StringU5BU5D_t2956870243* L_63 = KeyValuePair_2_get_Value_m2149107613((&V_8), /*hidden argument*/KeyValuePair_2_get_Value_m2149107613_MethodInfo_var);
			V_10 = L_63;
			int32_t L_64 = V_6;
			Array_Resize_TisString_t_m939990198(NULL /*static, unused*/, (&V_10), L_64, /*hidden argument*/Array_Resize_TisString_t_m939990198_MethodInfo_var);
			StringU5BU5D_t2956870243* L_65 = V_10;
			int32_t L_66 = V_6;
			StringU5BU5D_t2956870243* L_67 = V_10;
			NullCheck(L_67);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_67, 0);
			int32_t L_68 = 0;
			NullCheck(L_65);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_65, ((int32_t)((int32_t)L_66-(int32_t)1)));
			ArrayElementTypeCheck (L_65, ((L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_68))));
			(L_65)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_66-(int32_t)1))), (String_t*)((L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_68))));
			Dictionary_2_t299600851 * L_69 = V_7;
			String_t* L_70 = KeyValuePair_2_get_Key_m2631615050((&V_8), /*hidden argument*/KeyValuePair_2_get_Key_m2631615050_MethodInfo_var);
			StringU5BU5D_t2956870243* L_71 = V_10;
			NullCheck(L_69);
			VirtActionInvoker2< String_t*, StringU5BU5D_t2956870243* >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String[]>::Add(!0,!1) */, L_69, L_70, L_71);
		}

IL_01bc:
		{
			bool L_72 = Enumerator_MoveNext_m1312278737((&V_9), /*hidden argument*/Enumerator_MoveNext_m1312278737_MethodInfo_var);
			if (L_72)
			{
				goto IL_0186;
			}
		}

IL_01c8:
		{
			IL2CPP_LEAVE(0x1DA, FINALLY_01cd);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_01cd;
	}

FINALLY_01cd:
	{ // begin finally (depth: 1)
		Enumerator_t66628792  L_73 = V_9;
		Enumerator_t66628792  L_74 = L_73;
		Il2CppObject * L_75 = Box(Enumerator_t66628792_il2cpp_TypeInfo_var, &L_74);
		NullCheck((Il2CppObject *)L_75);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_75);
		IL2CPP_END_FINALLY(461)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(461)
	{
		IL2CPP_JUMP_TBL(0x1DA, IL_01da)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_01da:
	{
		Dictionary_2_t299600851 * L_76 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mDictionary_5(L_76);
	}

IL_01e1:
	{
		int32_t L_77 = V_5;
		V_5 = ((int32_t)((int32_t)L_77+(int32_t)1));
	}

IL_01e7:
	{
		int32_t L_78 = V_5;
		BetterList_1_t2465456914 * L_79 = V_1;
		NullCheck(L_79);
		int32_t L_80 = L_79->get_size_1();
		if ((((int32_t)L_78) < ((int32_t)L_80)))
		{
			goto IL_0133;
		}
	}

IL_01f4:
	{
		Dictionary_2_t190145395 * L_81 = (Dictionary_2_t190145395 *)il2cpp_codegen_object_new(Dictionary_2_t190145395_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m642355421(L_81, /*hidden argument*/Dictionary_2__ctor_m642355421_MethodInfo_var);
		V_11 = L_81;
		V_12 = 0;
		goto IL_021a;
	}

IL_0203:
	{
		Dictionary_2_t190145395 * L_82 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_83 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		int32_t L_84 = V_12;
		NullCheck(L_83);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_83, L_84);
		int32_t L_85 = L_84;
		int32_t L_86 = V_12;
		NullCheck(L_82);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_82, ((L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_85))), L_86);
		int32_t L_87 = V_12;
		V_12 = ((int32_t)((int32_t)L_87+(int32_t)1));
	}

IL_021a:
	{
		int32_t L_88 = V_12;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_89 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_89);
		if ((((int32_t)L_88) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_89)->max_length)))))))
		{
			goto IL_0203;
		}
	}
	{
		goto IL_026e;
	}

IL_022d:
	{
		ByteReader_t2446302219 * L_90 = V_0;
		NullCheck(L_90);
		BetterList_1_t2465456914 * L_91 = ByteReader_ReadCSV_m2254485814(L_90, /*hidden argument*/NULL);
		V_13 = L_91;
		BetterList_1_t2465456914 * L_92 = V_13;
		if (!L_92)
		{
			goto IL_0248;
		}
	}
	{
		BetterList_1_t2465456914 * L_93 = V_13;
		NullCheck(L_93);
		int32_t L_94 = L_93->get_size_1();
		if (L_94)
		{
			goto IL_024d;
		}
	}

IL_0248:
	{
		goto IL_0273;
	}

IL_024d:
	{
		BetterList_1_t2465456914 * L_95 = V_13;
		NullCheck(L_95);
		String_t* L_96 = BetterList_1_get_Item_m4060667538(L_95, 0, /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_97 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_96, /*hidden argument*/NULL);
		if (!L_97)
		{
			goto IL_0264;
		}
	}
	{
		goto IL_026e;
	}

IL_0264:
	{
		BetterList_1_t2465456914 * L_98 = V_13;
		StringU5BU5D_t2956870243* L_99 = V_2;
		Dictionary_2_t190145395 * L_100 = V_11;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Localization_AddCSV_m3834968952(NULL /*static, unused*/, L_98, L_99, L_100, /*hidden argument*/NULL);
	}

IL_026e:
	{
		goto IL_022d;
	}

IL_0273:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_101 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mMerging_9();
		if (L_101)
		{
			goto IL_02ae;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		OnLocalizeNotification_t1642290739 * L_102 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_onLocalize_1();
		if (!L_102)
		{
			goto IL_02ae;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mMerging_9((bool)1);
		OnLocalizeNotification_t1642290739 * L_103 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_onLocalize_1();
		V_14 = L_103;
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_onLocalize_1((OnLocalizeNotification_t1642290739 *)NULL);
		OnLocalizeNotification_t1642290739 * L_104 = V_14;
		NullCheck(L_104);
		OnLocalizeNotification_Invoke_m449099770(L_104, /*hidden argument*/NULL);
		OnLocalizeNotification_t1642290739 * L_105 = V_14;
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_onLocalize_1(L_105);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mMerging_9((bool)0);
	}

IL_02ae:
	{
		return (bool)1;
	}
}
// System.Void Localization::AddCSV(BetterList`1<System.String>,System.String[],System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t1967233988_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1_get_Item_m4060667538_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1387857535;
extern Il2CppCodeGenString* _stringLiteral2002059926;
extern Il2CppCodeGenString* _stringLiteral751668336;
extern Il2CppCodeGenString* _stringLiteral912029118;
extern const uint32_t Localization_AddCSV_m3834968952_MetadataUsageId;
extern "C"  void Localization_AddCSV_m3834968952 (Il2CppObject * __this /* static, unused */, BetterList_1_t2465456914 * ___newValues0, StringU5BU5D_t2956870243* ___newLanguages1, Dictionary_2_t190145395 * ___languageIndices2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_AddCSV_m3834968952_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	StringU5BU5D_t2956870243* V_1 = NULL;
	Exception_t1967233988 * V_2 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		BetterList_1_t2465456914 * L_0 = ___newValues0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_size_1();
		if ((((int32_t)L_1) >= ((int32_t)2)))
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		BetterList_1_t2465456914 * L_2 = ___newValues0;
		NullCheck(L_2);
		String_t* L_3 = BetterList_1_get_Item_m4060667538(L_2, 0, /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		V_0 = L_3;
		String_t* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0021;
		}
	}
	{
		return;
	}

IL_0021:
	{
		BetterList_1_t2465456914 * L_6 = ___newValues0;
		StringU5BU5D_t2956870243* L_7 = ___newLanguages1;
		Dictionary_2_t190145395 * L_8 = ___languageIndices2;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_9 = Localization_ExtractStrings_m218665019(NULL /*static, unused*/, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Dictionary_2_t299600851 * L_10 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_11 = V_0;
		NullCheck(L_10);
		bool L_12 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String[]>::ContainsKey(!0) */, L_10, L_11);
		if (!L_12)
		{
			goto IL_0066;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_13 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_14 = V_0;
		StringU5BU5D_t2956870243* L_15 = V_1;
		NullCheck(L_13);
		VirtActionInvoker2< String_t*, StringU5BU5D_t2956870243* >::Invoke(25 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String[]>::set_Item(!0,!1) */, L_13, L_14, L_15);
		StringU5BU5D_t2956870243* L_16 = ___newLanguages1;
		if (L_16)
		{
			goto IL_0061;
		}
	}
	{
		String_t* L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_18 = String_Concat_m1825781833(NULL /*static, unused*/, _stringLiteral1387857535, L_17, _stringLiteral2002059926, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_0061:
	{
		goto IL_0098;
	}

IL_0066:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_19 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_20 = V_0;
		StringU5BU5D_t2956870243* L_21 = V_1;
		NullCheck(L_19);
		VirtActionInvoker2< String_t*, StringU5BU5D_t2956870243* >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String[]>::Add(!0,!1) */, L_19, L_20, L_21);
		goto IL_0098;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t1967233988 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t1967233988_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0077;
		throw e;
	}

CATCH_0077:
	{ // begin catch(System.Exception)
		V_2 = ((Exception_t1967233988 *)__exception_local);
		String_t* L_22 = V_0;
		Exception_t1967233988 * L_23 = V_2;
		NullCheck(L_23);
		String_t* L_24 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_23);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m2933632197(NULL /*static, unused*/, _stringLiteral751668336, L_22, _stringLiteral912029118, L_24, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		goto IL_0098;
	} // end catch (depth: 1)

IL_0098:
	{
		return;
	}
}
// System.String[] Localization::ExtractStrings(BetterList`1<System.String>,System.String[],System.Collections.Generic.Dictionary`2<System.String,System.Int32>)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1_get_Item_m4060667538_MethodInfo_var;
extern const uint32_t Localization_ExtractStrings_m218665019_MetadataUsageId;
extern "C"  StringU5BU5D_t2956870243* Localization_ExtractStrings_m218665019 (Il2CppObject * __this /* static, unused */, BetterList_1_t2465456914 * ___added0, StringU5BU5D_t2956870243* ___newLanguages1, Dictionary_2_t190145395 * ___languageIndices2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_ExtractStrings_m218665019_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	StringU5BU5D_t2956870243* V_3 = NULL;
	String_t* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	int32_t V_8 = 0;
	{
		StringU5BU5D_t2956870243* L_0 = ___newLanguages1;
		if (L_0)
		{
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_1 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_1);
		V_0 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))));
		V_1 = 1;
		BetterList_1_t2465456914 * L_2 = ___added0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_size_1();
		StringU5BU5D_t2956870243* L_4 = V_0;
		NullCheck(L_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Min_m2413438171(NULL /*static, unused*/, L_3, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length))))+(int32_t)1)), /*hidden argument*/NULL);
		V_2 = L_5;
		goto IL_003b;
	}

IL_002b:
	{
		StringU5BU5D_t2956870243* L_6 = V_0;
		int32_t L_7 = V_1;
		BetterList_1_t2465456914 * L_8 = ___added0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		String_t* L_10 = BetterList_1_get_Item_m4060667538(L_8, L_9, /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7-(int32_t)1)));
		ArrayElementTypeCheck (L_6, L_10);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_7-(int32_t)1))), (String_t*)L_10);
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003b:
	{
		int32_t L_12 = V_1;
		int32_t L_13 = V_2;
		if ((((int32_t)L_12) < ((int32_t)L_13)))
		{
			goto IL_002b;
		}
	}
	{
		StringU5BU5D_t2956870243* L_14 = V_0;
		return L_14;
	}

IL_0044:
	{
		BetterList_1_t2465456914 * L_15 = ___added0;
		NullCheck(L_15);
		String_t* L_16 = BetterList_1_get_Item_m4060667538(L_15, 0, /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		V_4 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_17 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_18 = V_4;
		NullCheck(L_17);
		bool L_19 = VirtFuncInvoker2< bool, String_t*, StringU5BU5D_t2956870243** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String[]>::TryGetValue(!0,!1&) */, L_17, L_18, (&V_3));
		if (L_19)
		{
			goto IL_006d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_20 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_20);
		V_3 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_20)->max_length))))));
	}

IL_006d:
	{
		V_5 = 0;
		StringU5BU5D_t2956870243* L_21 = ___newLanguages1;
		NullCheck(L_21);
		V_6 = (((int32_t)((int32_t)(((Il2CppArray *)L_21)->max_length))));
		goto IL_009e;
	}

IL_007a:
	{
		StringU5BU5D_t2956870243* L_22 = ___newLanguages1;
		int32_t L_23 = V_5;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		V_7 = ((L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24)));
		Dictionary_2_t190145395 * L_25 = ___languageIndices2;
		String_t* L_26 = V_7;
		NullCheck(L_25);
		int32_t L_27 = VirtFuncInvoker1< int32_t, String_t* >::Invoke(24 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Int32>::get_Item(!0) */, L_25, L_26);
		V_8 = L_27;
		StringU5BU5D_t2956870243* L_28 = V_3;
		int32_t L_29 = V_8;
		BetterList_1_t2465456914 * L_30 = ___added0;
		int32_t L_31 = V_5;
		NullCheck(L_30);
		String_t* L_32 = BetterList_1_get_Item_m4060667538(L_30, ((int32_t)((int32_t)L_31+(int32_t)1)), /*hidden argument*/BetterList_1_get_Item_m4060667538_MethodInfo_var);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		ArrayElementTypeCheck (L_28, L_32);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(L_29), (String_t*)L_32);
		int32_t L_33 = V_5;
		V_5 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_009e:
	{
		int32_t L_34 = V_5;
		int32_t L_35 = V_6;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_007a;
		}
	}
	{
		StringU5BU5D_t2956870243* L_36 = V_3;
		return L_36;
	}
}
// System.Boolean Localization::SelectLanguage(System.String)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UIRoot_t2503447958_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746021752;
extern Il2CppCodeGenString* _stringLiteral2369256744;
extern const uint32_t Localization_SelectLanguage_m3438398552_MetadataUsageId;
extern "C"  bool Localization_SelectLanguage_m3438398552 (Il2CppObject * __this /* static, unused */, String_t* ___language0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_SelectLanguage_m3438398552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguageIndex_7((-1));
		Dictionary_2_t299600851 * L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.String[]>::get_Count() */, L_0);
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_2 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_2);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))));
		goto IL_0081;
	}

IL_0026:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_3 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		String_t* L_6 = ___language0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m1260523650(NULL /*static, unused*/, ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_8 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		NullCheck(L_8);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Clear() */, L_8);
		int32_t L_9 = V_0;
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguageIndex_7(L_9);
		String_t* L_10 = ___language0;
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguage_8(L_10);
		String_t* L_11 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguage_8();
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral2746021752, L_11, /*hidden argument*/NULL);
		OnLocalizeNotification_t1642290739 * L_12 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_onLocalize_1();
		if (!L_12)
		{
			goto IL_0071;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		OnLocalizeNotification_t1642290739 * L_13 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_onLocalize_1();
		NullCheck(L_13);
		OnLocalizeNotification_Invoke_m449099770(L_13, /*hidden argument*/NULL);
	}

IL_0071:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIRoot_t2503447958_il2cpp_TypeInfo_var);
		UIRoot_Broadcast_m2431666462(NULL /*static, unused*/, _stringLiteral2369256744, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_007d:
	{
		int32_t L_14 = V_0;
		V_0 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0081:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = V_1;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0026;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void Localization::Set(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* UIRoot_t2503447958_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746021752;
extern Il2CppCodeGenString* _stringLiteral2369256744;
extern const uint32_t Localization_Set_m3679996727_MetadataUsageId;
extern "C"  void Localization_Set_m3679996727 (Il2CppObject * __this /* static, unused */, String_t* ___languageName0, Dictionary_2_t2606186806 * ___dictionary1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_Set_m3679996727_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___languageName0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguage_8(L_0);
		String_t* L_1 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguage_8();
		PlayerPrefs_SetString_m989974275(NULL /*static, unused*/, _stringLiteral2746021752, L_1, /*hidden argument*/NULL);
		Dictionary_2_t2606186806 * L_2 = ___dictionary1;
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mOldDictionary_4(L_2);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_localizationHasBeenSet_2((bool)1);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguageIndex_7((-1));
		StringU5BU5D_t2956870243* L_3 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_4 = ___languageName0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		ArrayElementTypeCheck (L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_4);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguages_3(L_3);
		OnLocalizeNotification_t1642290739 * L_5 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_onLocalize_1();
		if (!L_5)
		{
			goto IL_004a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		OnLocalizeNotification_t1642290739 * L_6 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_onLocalize_1();
		NullCheck(L_6);
		OnLocalizeNotification_Invoke_m449099770(L_6, /*hidden argument*/NULL);
	}

IL_004a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIRoot_t2503447958_il2cpp_TypeInfo_var);
		UIRoot_Broadcast_m2431666462(NULL /*static, unused*/, _stringLiteral2369256744, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Localization::Set(System.String,System.String)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t Localization_Set_m920686012_MetadataUsageId;
extern "C"  void Localization_Set_m920686012 (Il2CppObject * __this /* static, unused */, String_t* ___key0, String_t* ___value1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_Set_m920686012_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		String_t* L_1 = ___key0;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_3 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		String_t* L_4 = ___key0;
		String_t* L_5 = ___value1;
		NullCheck(L_3);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(25 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_3, L_4, L_5);
		goto IL_002d;
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_6 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		String_t* L_7 = ___key0;
		String_t* L_8 = ___value1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::Add(!0,!1) */, L_6, L_7, L_8);
	}

IL_002d:
	{
		return;
	}
}
// System.String Localization::Get(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746021752;
extern Il2CppCodeGenString* _stringLiteral60895824;
extern Il2CppCodeGenString* _stringLiteral307620589;
extern Il2CppCodeGenString* _stringLiteral2577447219;
extern Il2CppCodeGenString* _stringLiteral645326050;
extern Il2CppCodeGenString* _stringLiteral963024860;
extern const uint32_t Localization_Get_m2079644207_MetadataUsageId;
extern "C"  String_t* Localization_Get_m2079644207 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_Get_m2079644207_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	StringU5BU5D_t2956870243* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	{
		String_t* L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (String_t*)NULL;
	}

IL_000d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_2 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_localizationHasBeenSet_2();
		if (L_2)
		{
			goto IL_002c;
		}
	}
	{
		String_t* L_3 = PlayerPrefs_GetString_m3230559948(NULL /*static, unused*/, _stringLiteral2746021752, _stringLiteral60895824, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Localization_LoadDictionary_m3777629616(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_002c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_4 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		if (L_4)
		{
			goto IL_0042;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, _stringLiteral307620589, /*hidden argument*/NULL);
		return (String_t*)NULL;
	}

IL_0042:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		String_t* L_5 = Localization_get_language_m4046813542(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		if ((!(((uint32_t)L_6) == ((uint32_t)(-1)))))
		{
			goto IL_0088;
		}
	}
	{
		V_1 = 0;
		goto IL_007b;
	}

IL_005a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_7 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		String_t* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m1260523650(NULL /*static, unused*/, ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9))), L_10, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_12 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguageIndex_7(L_12);
		goto IL_0088;
	}

IL_0077:
	{
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_007b:
	{
		int32_t L_14 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_15 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_15);
		if ((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length)))))))
		{
			goto IL_005a;
		}
	}

IL_0088:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_16 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		if ((!(((uint32_t)L_16) == ((uint32_t)(-1)))))
		{
			goto IL_00b5;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguageIndex_7(0);
		StringU5BU5D_t2956870243* L_17 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 0);
		int32_t L_18 = 0;
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguage_8(((L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_18))));
		String_t* L_19 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m138640077(NULL /*static, unused*/, _stringLiteral2577447219, L_19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m3123317694(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
	}

IL_00b5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		int32_t L_21 = UICamera_get_currentScheme_m1314774372(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_21;
		int32_t L_22 = V_4;
		if ((!(((uint32_t)L_22) == ((uint32_t)1))))
		{
			goto IL_0133;
		}
	}
	{
		String_t* L_23 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m138640077(NULL /*static, unused*/, L_23, _stringLiteral645326050, /*hidden argument*/NULL);
		V_5 = L_24;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_25 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mReplacement_6();
		String_t* L_26 = V_5;
		NullCheck(L_25);
		bool L_27 = VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_25, L_26, (&V_2));
		if (!L_27)
		{
			goto IL_00e6;
		}
	}
	{
		String_t* L_28 = V_2;
		return L_28;
	}

IL_00e6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_29 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		if ((((int32_t)L_29) == ((int32_t)(-1))))
		{
			goto IL_0119;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_30 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_31 = V_5;
		NullCheck(L_30);
		bool L_32 = VirtFuncInvoker2< bool, String_t*, StringU5BU5D_t2956870243** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String[]>::TryGetValue(!0,!1&) */, L_30, L_31, (&V_3));
		if (!L_32)
		{
			goto IL_0119;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_33 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		StringU5BU5D_t2956870243* L_34 = V_3;
		NullCheck(L_34);
		if ((((int32_t)L_33) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_34)->max_length)))))))
		{
			goto IL_0119;
		}
	}
	{
		StringU5BU5D_t2956870243* L_35 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_36 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		int32_t L_37 = L_36;
		return ((L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37)));
	}

IL_0119:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_38 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		String_t* L_39 = V_5;
		NullCheck(L_38);
		bool L_40 = VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_38, L_39, (&V_2));
		if (!L_40)
		{
			goto IL_012e;
		}
	}
	{
		String_t* L_41 = V_2;
		return L_41;
	}

IL_012e:
	{
		goto IL_01a5;
	}

IL_0133:
	{
		int32_t L_42 = V_4;
		if ((!(((uint32_t)L_42) == ((uint32_t)2))))
		{
			goto IL_01a5;
		}
	}
	{
		String_t* L_43 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m138640077(NULL /*static, unused*/, L_43, _stringLiteral963024860, /*hidden argument*/NULL);
		V_6 = L_44;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_45 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mReplacement_6();
		String_t* L_46 = V_6;
		NullCheck(L_45);
		bool L_47 = VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_45, L_46, (&V_2));
		if (!L_47)
		{
			goto IL_015d;
		}
	}
	{
		String_t* L_48 = V_2;
		return L_48;
	}

IL_015d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_49 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		if ((((int32_t)L_49) == ((int32_t)(-1))))
		{
			goto IL_0190;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_50 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_51 = V_6;
		NullCheck(L_50);
		bool L_52 = VirtFuncInvoker2< bool, String_t*, StringU5BU5D_t2956870243** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String[]>::TryGetValue(!0,!1&) */, L_50, L_51, (&V_3));
		if (!L_52)
		{
			goto IL_0190;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_53 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		StringU5BU5D_t2956870243* L_54 = V_3;
		NullCheck(L_54);
		if ((((int32_t)L_53) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_54)->max_length)))))))
		{
			goto IL_0190;
		}
	}
	{
		StringU5BU5D_t2956870243* L_55 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_56 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_56);
		int32_t L_57 = L_56;
		return ((L_55)->GetAt(static_cast<il2cpp_array_size_t>(L_57)));
	}

IL_0190:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_58 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		String_t* L_59 = V_6;
		NullCheck(L_58);
		bool L_60 = VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_58, L_59, (&V_2));
		if (!L_60)
		{
			goto IL_01a5;
		}
	}
	{
		String_t* L_61 = V_2;
		return L_61;
	}

IL_01a5:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_62 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mReplacement_6();
		String_t* L_63 = ___key0;
		NullCheck(L_62);
		bool L_64 = VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_62, L_63, (&V_2));
		if (!L_64)
		{
			goto IL_01b9;
		}
	}
	{
		String_t* L_65 = V_2;
		return L_65;
	}

IL_01b9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_66 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		if ((((int32_t)L_66) == ((int32_t)(-1))))
		{
			goto IL_0204;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_67 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_68 = ___key0;
		NullCheck(L_67);
		bool L_69 = VirtFuncInvoker2< bool, String_t*, StringU5BU5D_t2956870243** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String[]>::TryGetValue(!0,!1&) */, L_67, L_68, (&V_3));
		if (!L_69)
		{
			goto IL_0204;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_70 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		StringU5BU5D_t2956870243* L_71 = V_3;
		NullCheck(L_71);
		if ((((int32_t)L_70) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_71)->max_length)))))))
		{
			goto IL_0200;
		}
	}
	{
		StringU5BU5D_t2956870243* L_72 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		int32_t L_73 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguageIndex_7();
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, L_73);
		int32_t L_74 = L_73;
		V_7 = ((L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_74)));
		String_t* L_75 = V_7;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_76 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_75, /*hidden argument*/NULL);
		if (!L_76)
		{
			goto IL_01fd;
		}
	}
	{
		StringU5BU5D_t2956870243* L_77 = V_3;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, 0);
		int32_t L_78 = 0;
		V_7 = ((L_77)->GetAt(static_cast<il2cpp_array_size_t>(L_78)));
	}

IL_01fd:
	{
		String_t* L_79 = V_7;
		return L_79;
	}

IL_0200:
	{
		StringU5BU5D_t2956870243* L_80 = V_3;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, 0);
		int32_t L_81 = 0;
		return ((L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_81)));
	}

IL_0204:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_82 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		String_t* L_83 = ___key0;
		NullCheck(L_82);
		bool L_84 = VirtFuncInvoker2< bool, String_t*, String_t** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::TryGetValue(!0,!1&) */, L_82, L_83, (&V_2));
		if (!L_84)
		{
			goto IL_0218;
		}
	}
	{
		String_t* L_85 = V_2;
		return L_85;
	}

IL_0218:
	{
		String_t* L_86 = ___key0;
		return L_86;
	}
}
// System.String Localization::Format(System.String,System.Object[])
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Localization_Format_m3071416850_MetadataUsageId;
extern "C"  String_t* Localization_Format_m3071416850 (Il2CppObject * __this /* static, unused */, String_t* ___key0, ObjectU5BU5D_t11523773* ___parameters1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_Format_m3071416850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		String_t* L_1 = Localization_Get_m2079644207(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_2 = ___parameters1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Format_m4050103162(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Localization::get_isActive()
extern "C"  bool Localization_get_isActive_m1111783919 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.String Localization::Localize(System.String)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern const uint32_t Localization_Localize_m976395348_MetadataUsageId;
extern "C"  String_t* Localization_Localize_m976395348 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_Localize_m976395348_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		String_t* L_1 = Localization_Get_m2079644207(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean Localization::Exists(System.String)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2746021752;
extern Il2CppCodeGenString* _stringLiteral60895824;
extern Il2CppCodeGenString* _stringLiteral645326050;
extern const uint32_t Localization_Exists_m739235440_MetadataUsageId;
extern "C"  bool Localization_Exists_m739235440 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_Exists_m739235440_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	int32_t G_B9_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		bool L_0 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_localizationHasBeenSet_2();
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_1 = PlayerPrefs_GetString_m3230559948(NULL /*static, unused*/, _stringLiteral2746021752, _stringLiteral60895824, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Localization_set_language_m411704107(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_001e:
	{
		String_t* L_2 = ___key0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m138640077(NULL /*static, unused*/, L_2, _stringLiteral645326050, /*hidden argument*/NULL);
		V_0 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_4 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_5 = V_0;
		NullCheck(L_4);
		bool L_6 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String[]>::ContainsKey(!0) */, L_4, L_5);
		if (!L_6)
		{
			goto IL_003c;
		}
	}
	{
		return (bool)1;
	}

IL_003c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_7 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		String_t* L_8 = V_0;
		NullCheck(L_7);
		bool L_9 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_7, L_8);
		if (!L_9)
		{
			goto IL_004e;
		}
	}
	{
		return (bool)1;
	}

IL_004e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_10 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_11 = ___key0;
		NullCheck(L_10);
		bool L_12 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String[]>::ContainsKey(!0) */, L_10, L_11);
		if (L_12)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t2606186806 * L_13 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mOldDictionary_4();
		String_t* L_14 = ___key0;
		NullCheck(L_13);
		bool L_15 = VirtFuncInvoker1< bool, String_t* >::Invoke(27 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String>::ContainsKey(!0) */, L_13, L_14);
		G_B9_0 = ((int32_t)(L_15));
		goto IL_006c;
	}

IL_006b:
	{
		G_B9_0 = 1;
	}

IL_006c:
	{
		return (bool)G_B9_0;
	}
}
// System.Void Localization::Set(System.String,System.String,System.String)
extern Il2CppClass* Localization_t3647281849_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t2956870243_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Dictionary_2_t299600851_il2cpp_TypeInfo_var;
extern Il2CppClass* Enumerator_t66628792_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Array_Resize_TisString_t_m939990198_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m1120242542_MethodInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m1889285220_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3797208814_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m2149107613_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m2631615050_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1312278737_MethodInfo_var;
extern const uint32_t Localization_Set_m2516647160_MetadataUsageId;
extern "C"  void Localization_Set_m2516647160 (Il2CppObject * __this /* static, unused */, String_t* ___language0, String_t* ___key1, String_t* ___text2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Localization_Set_m2516647160_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	StringU5BU5D_t2956870243* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	StringU5BU5D_t2956870243* V_3 = NULL;
	int32_t V_4 = 0;
	Dictionary_2_t299600851 * V_5 = NULL;
	KeyValuePair_2_t4083099445  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Enumerator_t66628792  V_7;
	memset(&V_7, 0, sizeof(V_7));
	StringU5BU5D_t2956870243* V_8 = NULL;
	StringU5BU5D_t2956870243* V_9 = NULL;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_0 = Localization_get_knownLanguages_m431207172(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		StringU5BU5D_t2956870243* L_1 = V_0;
		if (L_1)
		{
			goto IL_0021;
		}
	}
	{
		StringU5BU5D_t2956870243* L_2 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)1));
		String_t* L_3 = ___language0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mLanguages_3(L_2);
		StringU5BU5D_t2956870243* L_4 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		V_0 = L_4;
	}

IL_0021:
	{
		V_1 = 0;
		StringU5BU5D_t2956870243* L_5 = V_0;
		NullCheck(L_5);
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))));
		goto IL_006e;
	}

IL_002c:
	{
		StringU5BU5D_t2956870243* L_6 = V_0;
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		String_t* L_9 = ___language0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_10 = String_op_Equality_m1260523650(NULL /*static, unused*/, ((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_11 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_12 = ___key1;
		NullCheck(L_11);
		bool L_13 = VirtFuncInvoker2< bool, String_t*, StringU5BU5D_t2956870243** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String[]>::TryGetValue(!0,!1&) */, L_11, L_12, (&V_3));
		if (L_13)
		{
			goto IL_0065;
		}
	}
	{
		StringU5BU5D_t2956870243* L_14 = V_0;
		NullCheck(L_14);
		V_3 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))));
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_15 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_16 = ___key1;
		StringU5BU5D_t2956870243* L_17 = V_3;
		NullCheck(L_15);
		VirtActionInvoker2< String_t*, StringU5BU5D_t2956870243* >::Invoke(25 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String[]>::set_Item(!0,!1) */, L_15, L_16, L_17);
		StringU5BU5D_t2956870243* L_18 = V_3;
		String_t* L_19 = ___text2;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		ArrayElementTypeCheck (L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_19);
	}

IL_0065:
	{
		StringU5BU5D_t2956870243* L_20 = V_3;
		int32_t L_21 = V_1;
		String_t* L_22 = ___text2;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		ArrayElementTypeCheck (L_20, L_22);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_21), (String_t*)L_22);
		return;
	}

IL_006a:
	{
		int32_t L_23 = V_1;
		V_1 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_006e:
	{
		int32_t L_24 = V_1;
		int32_t L_25 = V_2;
		if ((((int32_t)L_24) < ((int32_t)L_25)))
		{
			goto IL_002c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		StringU5BU5D_t2956870243* L_26 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		NullCheck(L_26);
		V_4 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length))))+(int32_t)1));
		int32_t L_27 = V_4;
		Array_Resize_TisString_t_m939990198(NULL /*static, unused*/, (((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_address_of_mLanguages_3()), L_27, /*hidden argument*/Array_Resize_TisString_t_m939990198_MethodInfo_var);
		StringU5BU5D_t2956870243* L_28 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mLanguages_3();
		int32_t L_29 = V_4;
		String_t* L_30 = ___language0;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, ((int32_t)((int32_t)L_29-(int32_t)1)));
		ArrayElementTypeCheck (L_28, L_30);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_29-(int32_t)1))), (String_t*)L_30);
		Dictionary_2_t299600851 * L_31 = (Dictionary_2_t299600851 *)il2cpp_codegen_object_new(Dictionary_2_t299600851_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m1120242542(L_31, /*hidden argument*/Dictionary_2__ctor_m1120242542_MethodInfo_var);
		V_5 = L_31;
		Dictionary_2_t299600851 * L_32 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		NullCheck(L_32);
		Enumerator_t66628792  L_33 = Dictionary_2_GetEnumerator_m1889285220(L_32, /*hidden argument*/Dictionary_2_GetEnumerator_m1889285220_MethodInfo_var);
		V_7 = L_33;
	}

IL_00aa:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00e5;
		}

IL_00af:
		{
			KeyValuePair_2_t4083099445  L_34 = Enumerator_get_Current_m3797208814((&V_7), /*hidden argument*/Enumerator_get_Current_m3797208814_MethodInfo_var);
			V_6 = L_34;
			StringU5BU5D_t2956870243* L_35 = KeyValuePair_2_get_Value_m2149107613((&V_6), /*hidden argument*/KeyValuePair_2_get_Value_m2149107613_MethodInfo_var);
			V_8 = L_35;
			int32_t L_36 = V_4;
			Array_Resize_TisString_t_m939990198(NULL /*static, unused*/, (&V_8), L_36, /*hidden argument*/Array_Resize_TisString_t_m939990198_MethodInfo_var);
			StringU5BU5D_t2956870243* L_37 = V_8;
			int32_t L_38 = V_4;
			StringU5BU5D_t2956870243* L_39 = V_8;
			NullCheck(L_39);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 0);
			int32_t L_40 = 0;
			NullCheck(L_37);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_37, ((int32_t)((int32_t)L_38-(int32_t)1)));
			ArrayElementTypeCheck (L_37, ((L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40))));
			(L_37)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_38-(int32_t)1))), (String_t*)((L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_40))));
			Dictionary_2_t299600851 * L_41 = V_5;
			String_t* L_42 = KeyValuePair_2_get_Key_m2631615050((&V_6), /*hidden argument*/KeyValuePair_2_get_Key_m2631615050_MethodInfo_var);
			StringU5BU5D_t2956870243* L_43 = V_8;
			NullCheck(L_41);
			VirtActionInvoker2< String_t*, StringU5BU5D_t2956870243* >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String[]>::Add(!0,!1) */, L_41, L_42, L_43);
		}

IL_00e5:
		{
			bool L_44 = Enumerator_MoveNext_m1312278737((&V_7), /*hidden argument*/Enumerator_MoveNext_m1312278737_MethodInfo_var);
			if (L_44)
			{
				goto IL_00af;
			}
		}

IL_00f1:
		{
			IL2CPP_LEAVE(0x103, FINALLY_00f6);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_00f6;
	}

FINALLY_00f6:
	{ // begin finally (depth: 1)
		Enumerator_t66628792  L_45 = V_7;
		Enumerator_t66628792  L_46 = L_45;
		Il2CppObject * L_47 = Box(Enumerator_t66628792_il2cpp_TypeInfo_var, &L_46);
		NullCheck((Il2CppObject *)L_47);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Il2CppObject *)L_47);
		IL2CPP_END_FINALLY(246)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(246)
	{
		IL2CPP_JUMP_TBL(0x103, IL_0103)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0103:
	{
		Dictionary_2_t299600851 * L_48 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->set_mDictionary_5(L_48);
		Dictionary_2_t299600851 * L_49 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_50 = ___key1;
		NullCheck(L_49);
		bool L_51 = VirtFuncInvoker2< bool, String_t*, StringU5BU5D_t2956870243** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.String[]>::TryGetValue(!0,!1&) */, L_49, L_50, (&V_9));
		if (L_51)
		{
			goto IL_0138;
		}
	}
	{
		StringU5BU5D_t2956870243* L_52 = V_0;
		NullCheck(L_52);
		V_9 = ((StringU5BU5D_t2956870243*)SZArrayNew(StringU5BU5D_t2956870243_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_52)->max_length))))));
		IL2CPP_RUNTIME_CLASS_INIT(Localization_t3647281849_il2cpp_TypeInfo_var);
		Dictionary_2_t299600851 * L_53 = ((Localization_t3647281849_StaticFields*)Localization_t3647281849_il2cpp_TypeInfo_var->static_fields)->get_mDictionary_5();
		String_t* L_54 = ___key1;
		StringU5BU5D_t2956870243* L_55 = V_9;
		NullCheck(L_53);
		VirtActionInvoker2< String_t*, StringU5BU5D_t2956870243* >::Invoke(25 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String[]>::set_Item(!0,!1) */, L_53, L_54, L_55);
		StringU5BU5D_t2956870243* L_56 = V_9;
		String_t* L_57 = ___text2;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 0);
		ArrayElementTypeCheck (L_56, L_57);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)L_57);
	}

IL_0138:
	{
		StringU5BU5D_t2956870243* L_58 = V_9;
		int32_t L_59 = V_4;
		String_t* L_60 = ___text2;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)((int32_t)L_59-(int32_t)1)));
		ArrayElementTypeCheck (L_58, L_60);
		(L_58)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_59-(int32_t)1))), (String_t*)L_60);
		return;
	}
}
// System.Void Localization/LoadFunction::.ctor(System.Object,System.IntPtr)
extern "C"  void LoadFunction__ctor_m3587491947 (LoadFunction_t2829036286 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Byte[] Localization/LoadFunction::Invoke(System.String)
extern "C"  ByteU5BU5D_t58506160* LoadFunction_Invoke_m2234063239 (LoadFunction_t2829036286 * __this, String_t* ___path0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		LoadFunction_Invoke_m2234063239((LoadFunction_t2829036286 *)__this->get_prev_9(),___path0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef ByteU5BU5D_t58506160* (*FunctionPointerType) (Il2CppObject *, void* __this, String_t* ___path0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___path0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef ByteU5BU5D_t58506160* (*FunctionPointerType) (void* __this, String_t* ___path0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___path0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef ByteU5BU5D_t58506160* (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___path0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern Il2CppClass* Byte_t2778693821_il2cpp_TypeInfo_var;
extern const uint32_t LoadFunction_Invoke_m2234063239DelegateFromManagedToNative_MetadataUsageId;
extern "C" ByteU5BU5D_t58506160* pinvoke_delegate_wrapper_LoadFunction_t2829036286(Il2CppObject* delegate, String_t* ___path0)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoadFunction_Invoke_m2234063239DelegateFromManagedToNative_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	typedef uint8_t* (STDCALL *native_function_ptr_type)(char*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___path0' to native representation
	char* ____path0_marshaled = NULL;
	____path0_marshaled = il2cpp_codegen_marshal_string(___path0);

	// Native function invocation and marshaling of return value back from native representation
	uint8_t* _return_value = _il2cpp_pinvoke_func(____path0_marshaled);
	ByteU5BU5D_t58506160* __return_value_unmarshaled = NULL;
	__return_value_unmarshaled = (ByteU5BU5D_t58506160*)il2cpp_codegen_marshal_array_result(Byte_t2778693821_il2cpp_TypeInfo_var, _return_value, 1);

	// Marshaling cleanup of parameter '___path0' native representation
	il2cpp_codegen_marshal_free(____path0_marshaled);
	____path0_marshaled = NULL;

	return __return_value_unmarshaled;
}
// System.IAsyncResult Localization/LoadFunction::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LoadFunction_BeginInvoke_m3903429034 (LoadFunction_t2829036286 * __this, String_t* ___path0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___path0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Byte[] Localization/LoadFunction::EndInvoke(System.IAsyncResult)
extern "C"  ByteU5BU5D_t58506160* LoadFunction_EndInvoke_m733835345 (LoadFunction_t2829036286 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (ByteU5BU5D_t58506160*)__result;
}
// System.Void Localization/OnLocalizeNotification::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLocalizeNotification__ctor_m3905864608 (OnLocalizeNotification_t1642290739 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Localization/OnLocalizeNotification::Invoke()
extern "C"  void OnLocalizeNotification_Invoke_m449099770 (OnLocalizeNotification_t1642290739 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnLocalizeNotification_Invoke_m449099770((OnLocalizeNotification_t1642290739 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnLocalizeNotification_t1642290739(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult Localization/OnLocalizeNotification::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnLocalizeNotification_BeginInvoke_m3533136977 (OnLocalizeNotification_t1642290739 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void Localization/OnLocalizeNotification::EndInvoke(System.IAsyncResult)
extern "C"  void OnLocalizeNotification_EndInvoke_m425002416 (OnLocalizeNotification_t1642290739 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void LookAtTarget::.ctor()
extern "C"  void LookAtTarget__ctor_m2328694392 (LookAtTarget_t2847647619 * __this, const MethodInfo* method)
{
	{
		__this->set_speed_4((8.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LookAtTarget::Start()
extern "C"  void LookAtTarget_Start_m1275832184 (LookAtTarget_t2847647619 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_mTrans_5(L_0);
		return;
	}
}
// System.Void LookAtTarget::LateUpdate()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t LookAtTarget_LateUpdate_m1455851227_MetadataUsageId;
extern "C"  void LookAtTarget_LateUpdate_m1455851227 (LookAtTarget_t2847647619 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LookAtTarget_LateUpdate_m1455851227_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Quaternion_t1891715979  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Transform_t284553113 * L_0 = __this->get_target_3();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0074;
		}
	}
	{
		Transform_t284553113 * L_2 = __this->get_target_3();
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Transform_t284553113 * L_4 = __this->get_mTrans_5();
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Vector3_t3525329789  L_6 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = Vector3_get_magnitude_m989985786((&V_0), /*hidden argument*/NULL);
		V_1 = L_7;
		float L_8 = V_1;
		if ((!(((float)L_8) > ((float)(0.001f)))))
		{
			goto IL_0074;
		}
	}
	{
		Vector3_t3525329789  L_9 = V_0;
		Quaternion_t1891715979  L_10 = Quaternion_LookRotation_m1257501645(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		Transform_t284553113 * L_11 = __this->get_mTrans_5();
		Transform_t284553113 * L_12 = __this->get_mTrans_5();
		NullCheck(L_12);
		Quaternion_t1891715979  L_13 = Transform_get_rotation_m11483428(L_12, /*hidden argument*/NULL);
		Quaternion_t1891715979  L_14 = V_2;
		float L_15 = __this->get_speed_4();
		float L_16 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_17 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, ((float)((float)L_15*(float)L_16)), /*hidden argument*/NULL);
		Quaternion_t1891715979  L_18 = Quaternion_Slerp_m844700366(NULL /*static, unused*/, L_13, L_14, L_17, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_rotation_m1525803229(L_11, L_18, /*hidden argument*/NULL);
	}

IL_0074:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
