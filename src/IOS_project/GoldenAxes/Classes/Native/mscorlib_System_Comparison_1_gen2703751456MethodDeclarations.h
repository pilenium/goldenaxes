﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<Console/Log>
struct Comparison_1_t2703751456;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Comparison`1<Console/Log>::.ctor(System.Object,System.IntPtr)
extern "C"  void Comparison_1__ctor_m3817489541_gshared (Comparison_1_t2703751456 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Comparison_1__ctor_m3817489541(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t2703751456 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m3817489541_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<Console/Log>::Invoke(T,T)
extern "C"  int32_t Comparison_1_Invoke_m3914762075_gshared (Comparison_1_t2703751456 * __this, Log_t76580  ___x0, Log_t76580  ___y1, const MethodInfo* method);
#define Comparison_1_Invoke_m3914762075(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t2703751456 *, Log_t76580 , Log_t76580 , const MethodInfo*))Comparison_1_Invoke_m3914762075_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<Console/Log>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Comparison_1_BeginInvoke_m549720020_gshared (Comparison_1_t2703751456 * __this, Log_t76580  ___x0, Log_t76580  ___y1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m549720020(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t2703751456 *, Log_t76580 , Log_t76580 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m549720020_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<Console/Log>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t Comparison_1_EndInvoke_m2577694001_gshared (Comparison_1_t2703751456 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Comparison_1_EndInvoke_m2577694001(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t2703751456 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m2577694001_gshared)(__this, ___result0, method)
