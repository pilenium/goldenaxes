﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataZones
struct DataZones_t1635830941;
// DataZone
struct DataZone_t1853884054;

#include "codegen/il2cpp-codegen.h"

// System.Void DataZones::.ctor()
extern "C"  void DataZones__ctor_m583674222 (DataZones_t1635830941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataZone DataZones::getData(System.UInt32)
extern "C"  DataZone_t1853884054 * DataZones_getData_m970677351 (DataZones_t1635830941 * __this, uint32_t ___zone_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
