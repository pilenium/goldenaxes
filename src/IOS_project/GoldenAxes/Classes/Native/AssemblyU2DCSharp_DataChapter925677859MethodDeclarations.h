﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataChapter
struct DataChapter_t925677859;

#include "codegen/il2cpp-codegen.h"

// System.Void DataChapter::.ctor()
extern "C"  void DataChapter__ctor_m3863566760 (DataChapter_t925677859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
