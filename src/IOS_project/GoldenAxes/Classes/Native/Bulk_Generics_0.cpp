﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>
struct U3CGetEnumeratorU3Ec__Iterator3_t1930958490;
// System.Object
struct Il2CppObject;
// BetterList`1/<GetEnumerator>c__Iterator3<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator3_t4215617419;
// BetterList`1/<GetEnumerator>c__Iterator3<System.Single>
struct U3CGetEnumeratorU3Ec__Iterator3_t41752724;
// BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>
struct U3CGetEnumeratorU3Ec__Iterator3_t179375053;
// BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>
struct U3CGetEnumeratorU3Ec__Iterator3_t1278240806;
// BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>
struct U3CGetEnumeratorU3Ec__Iterator3_t671719463;
// BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>
struct U3CGetEnumeratorU3Ec__Iterator3_t2608873491;
// BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>
struct U3CGetEnumeratorU3Ec__Iterator3_t2608873492;
// BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>
struct U3CGetEnumeratorU3Ec__Iterator3_t2608873493;
// BetterList`1/CompareFunc<System.Int32>
struct CompareFunc_t694073545;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// BetterList`1/CompareFunc<System.Object>
struct CompareFunc_t2978732474;
// BetterList`1/CompareFunc<System.Single>
struct CompareFunc_t3099835075;
// BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>
struct CompareFunc_t3237457404;
// BetterList`1/CompareFunc<UICamera/DepthEntry>
struct CompareFunc_t41355861;
// BetterList`1/CompareFunc<UnityEngine.Color>
struct CompareFunc_t3729801814;
// BetterList`1/CompareFunc<UnityEngine.Vector2>
struct CompareFunc_t1371988546;
// BetterList`1/CompareFunc<UnityEngine.Vector3>
struct CompareFunc_t1371988547;
// BetterList`1/CompareFunc<UnityEngine.Vector4>
struct CompareFunc_t1371988548;
// BetterList`1<System.Int32>
struct BetterList_1_t49415503;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;
// System.Int32[]
struct Int32U5BU5D_t1809983122;
// BetterList`1<System.Object>
struct BetterList_1_t2334074432;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2320212868;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// BetterList`1<System.Single>
struct BetterList_1_t2455177033;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t2441315469;
// System.Single[]
struct SingleU5BU5D_t1219431280;
// BetterList`1<TypewriterEffect/FadeEntry>
struct BetterList_1_t2592799362;
// System.Collections.Generic.IEnumerator`1<TypewriterEffect/FadeEntry>
struct IEnumerator_1_t2578937798;
// TypewriterEffect/FadeEntry[]
struct FadeEntryU5BU5D_t4004785971;
// BetterList`1<UICamera/DepthEntry>
struct BetterList_1_t3691665115;
// System.Collections.Generic.IEnumerator`1<UICamera/DepthEntry>
struct IEnumerator_1_t3677803551;
// UICamera/DepthEntry[]
struct DepthEntryU5BU5D_t2046984534;
// BetterList`1<UnityEngine.Color>
struct BetterList_1_t3085143772;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color>
struct IEnumerator_1_t3071282208;
// UnityEngine.Color[]
struct ColorU5BU5D_t3477081137;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t727330504;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector2>
struct IEnumerator_1_t713468940;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t2741383957;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t727330505;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t713468941;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t3227571696;
// BetterList`1<UnityEngine.Vector4>
struct BetterList_1_t727330506;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t713468942;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t3713759435;
// NGUITools/OnInitFunc`1<System.Object>
struct OnInitFunc_1_t3269005821;
// System.Action`1<System.Boolean>
struct Action_1_t359458046;
// System.Action`1<System.Object>
struct Action_1_t985559125;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>
struct U3CGetEnumeratorU3Ec__Iterator0_t3510862208;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2992490917;
// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t3234171350;
// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t398457203;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Exception
struct Exception_t1967233988;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>
struct ArrayReadOnlyList_1_t4175053208;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t3019176036;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t1801841577;
// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t121766345;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t3123668047;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t2043522010;
// System.Array
struct Il2CppArray;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1930958490.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1930958490MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Boolean211005341.h"
#include "mscorlib_System_UInt32985925326.h"
#include "AssemblyU2DCSharp_BetterList_1_gen49415503.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E4215617419.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E4215617419MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2334074432.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3Ec_41752724.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3Ec_41752724MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2455177033.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3Ec179375053.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3Ec179375053MethodDeclarations.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry1095831350.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2592799362.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1278240806.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E1278240806MethodDeclarations.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry2194697103.h"
#include "AssemblyU2DCSharp_BetterList_1_gen3691665115.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3Ec671719463.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3Ec671719463MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharp_BetterList_1_gen3085143772.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E2608873491.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E2608873491MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharp_BetterList_1_gen727330504.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E2608873492.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E2608873492MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharp_BetterList_1_gen727330505.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E2608873493.h"
#include "AssemblyU2DCSharp_BetterList_1_U3CGetEnumeratorU3E2608873493MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "AssemblyU2DCSharp_BetterList_1_gen727330506.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen694073545.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen694073545MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen2978732474.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen2978732474MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen3099835075.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen3099835075MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen3237457404.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen3237457404MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen41355861.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen41355861MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen3729801814.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen3729801814MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1371988546.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1371988546MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1371988547.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1371988547MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1371988548.h"
#include "AssemblyU2DCSharp_BetterList_1_CompareFunc_gen1371988548MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen49415503MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "mscorlib_System_Array2840145358MethodDeclarations.h"
#include "mscorlib_System_Int322847414787MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2281805437.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2281805437MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2334074432MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare271497070MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2455177033MethodDeclarations.h"
#include "mscorlib_System_Single958209021MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare392599671.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare392599671MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2592799362MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare530222000.h"
#include "mscorlib_System_Collections_Generic_EqualityCompare530222000MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen3691665115MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1629087753.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1629087753MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen3085143772MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1022566410.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar1022566410MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen727330504MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2959720438.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2959720438MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen727330505MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2959720439.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2959720439MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen727330506MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector43525329790MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2959720440.h"
#include "mscorlib_System_Collections_Generic_EqualityCompar2959720440MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUITools_OnInitFunc_1_gen3269005821.h"
#include "AssemblyU2DCSharp_NGUITools_OnInitFunc_1_gen3269005821MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen359458046.h"
#include "mscorlib_System_Action_1_gen359458046MethodDeclarations.h"
#include "mscorlib_System_Action_1_gen985559125.h"
#include "mscorlib_System_Action_1_gen985559125MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3510862208.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3510862208MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen398457203.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2992490917.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn2992490917MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgu318735129.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4175053208.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3234171350.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_U3CGetEn3234171350MethodDeclarations.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgu560415562.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen121766345.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen398457203MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_ArgumentOutOfRangeException3479058991.h"
#include "mscorlib_System_Exception1967233988.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen4175053208MethodDeclarations.h"
#include "mscorlib_System_Array_ArrayReadOnlyList_1_gen121766345MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4202534379.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4202534379MethodDeclarations.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"
#include "mscorlib_System_InvalidOperationException2420574324MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException2420574324.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen383943926.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen383943926MethodDeclarations.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexe476453423.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2632522680.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2632522680MethodDeclarations.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake2725032177.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen118495844.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen118495844MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2686184324.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2686184324MethodDeclarations.h"
#include "mscorlib_System_Byte2778693821.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2686197202.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2686197202MethodDeclarations.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen37517749.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen37517749MethodDeclarations.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen723939004.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen723939004MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2594345872.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2594345872MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22686855369.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen935788022.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen935788022MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21028297519.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3220446951.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3220446951MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23312956448.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1517861163.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1517861163MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21610370660.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen277738608.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen277738608MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_370248105.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2404181862.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2404181862MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Link2496691359.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4205037797.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4205037797MethodDeclarations.h"
#include "mscorlib_System_Collections_Hashtable_Slot2579998.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"
#include "mscorlib_System_Collections_SortedList_Slot2579998.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246524439.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen246524439MethodDeclarations.h"
#include "mscorlib_System_DateTime339033936.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1596047757.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1596047757MethodDeclarations.h"
#include "mscorlib_System_Decimal1688557254.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen442007117.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen442007117MethodDeclarations.h"
#include "mscorlib_System_Double534516614.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905232.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905232MethodDeclarations.h"
#include "mscorlib_System_Int162847414729.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905290.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905290MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905385.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2754905385MethodDeclarations.h"
#include "mscorlib_System_Int642847414882.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen584182523.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen584182523MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen744596923.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen744596923MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen226225632.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen226225632MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen467906065.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen467906065MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1303237477.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1303237477MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelD1395746974.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen228063683.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen228063683MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFi320573180.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3630765784.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3630765784MethodDeclarations.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo3723275281.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen407693973.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen407693973MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterModifier500203470.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3607348206.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3607348206MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceC3699857703.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3982075075.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3982075075MethodDeclarations.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceI4074584572.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1645779784.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1645779784MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_B1738289281.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2762836567.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2762836567MethodDeclarations.h"
#include "mscorlib_System_SByte2855346064.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1029642187.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1029642187MethodDeclarations.h"
#include "System_System_Security_Cryptography_X509Certificat1122151684.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen865699524.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen865699524MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3633423279.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3633423279MethodDeclarations.h"
#include "System_System_Text_RegularExpressions_Mark3725932776.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen671353395.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen671353395MethodDeclarations.h"
#include "mscorlib_System_TimeSpan763862892.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415771.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415771MethodDeclarations.h"
#include "mscorlib_System_UInt16985925268.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415829.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415829MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415924.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen893415924MethodDeclarations.h"
#include "mscorlib_System_UInt64985925421.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3174019288.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3174019288MethodDeclarations.h"
#include "System_System_Uri_UriScheme3266528785.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1003321853.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1003321853MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2102187606.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2102187606MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3426005481.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3426005481MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1495666263.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1495666263MethodDeclarations.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4044574710.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4044574710MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color324137084207.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2858612868.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2858612868MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2951122365.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3871236822.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3871236822MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ContactPoint2D3963746319.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen867389192.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen867389192MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResu959898689.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2279071712.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2279071712MethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2002543010.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2002543010MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe2095052507.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3436759954.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3436759954MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4248679326.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4248679326MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990273904.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3990273904MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit2D4082783401.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2498719112.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2498719112MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo2591228609.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1224502599.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1224502599MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter1317012096.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2131168810.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2131168810MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter2223678307.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3336978431.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3336978431MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp3429487928.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1186227706.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen1186227706MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentTy1278737203.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen311311084.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen311311084MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo403820581.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen64411786.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen64411786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo156921283.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2167552108.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2167552108MethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertex2260061605.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3432820291.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3432820291MethodDeclarations.h"

// System.Int32 System.Array::IndexOf<System.Object>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisIl2CppObject_m2661005505_gshared (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t11523773* p0, Il2CppObject * p1, const MethodInfo* method);
#define Array_IndexOf_TisIl2CppObject_m2661005505(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, const MethodInfo*))Array_IndexOf_TisIl2CppObject_m2661005505_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeNamedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeNamedArgument_t318735129_m3345236030_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036* p0, CustomAttributeNamedArgument_t318735129  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeNamedArgument_t318735129_m3345236030(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, CustomAttributeNamedArgument_t318735129 , const MethodInfo*))Array_IndexOf_TisCustomAttributeNamedArgument_t318735129_m3345236030_gshared)(__this /* static, unused */, p0, p1, method)
// System.Int32 System.Array::IndexOf<System.Reflection.CustomAttributeTypedArgument>(!!0[],!!0)
extern "C"  int32_t Array_IndexOf_TisCustomAttributeTypedArgument_t560415562_m858079087_gshared (Il2CppObject * __this /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047* p0, CustomAttributeTypedArgument_t560415562  p1, const MethodInfo* method);
#define Array_IndexOf_TisCustomAttributeTypedArgument_t560415562_m858079087(__this /* static, unused */, p0, p1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, CustomAttributeTypedArgument_t560415562 , const MethodInfo*))Array_IndexOf_TisCustomAttributeTypedArgument_t560415562_m858079087_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 System.Array::InternalArray__get_Item<Console/Log>(System.Int32)
extern "C"  Log_t76580  Array_InternalArray__get_Item_TisLog_t76580_m2834108274_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLog_t76580_m2834108274(__this, p0, method) ((  Log_t76580  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLog_t76580_m2834108274_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Globalization.Unicode.CodePointIndexer/TableRange>(System.Int32)
extern "C"  TableRange_t476453423  Array_InternalArray__get_Item_TisTableRange_t476453423_m3354422249_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTableRange_t476453423_m3354422249(__this, p0, method) ((  TableRange_t476453423  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTableRange_t476453423_m3354422249_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisClientCertificateType_t2725032177_m115939321_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisClientCertificateType_t2725032177_m115939321(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisClientCertificateType_t2725032177_m115939321_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Boolean>(System.Int32)
extern "C"  bool Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257(__this, p0, method) ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBoolean_t211005341_m1243823257_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Byte>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisByte_t2778693821_m3484475127_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Char>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisChar_t2778706699_m125306601_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisChar_t2778706699_m125306601(__this, p0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisChar_t2778706699_m125306601_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.DictionaryEntry>(System.Int32)
extern "C"  DictionaryEntry_t130027246  Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038(__this, p0, method) ((  DictionaryEntry_t130027246  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDictionaryEntry_t130027246_m297283038_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t816448501  Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488(__this, p0, method) ((  KeyValuePair_2_t816448501  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t816448501_m203509488_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>(System.Int32)
extern "C"  KeyValuePair_2_t2686855369  Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102(__this, p0, method) ((  KeyValuePair_2_t2686855369  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t2686855369_m2380168102_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>(System.Int32)
extern "C"  KeyValuePair_2_t1028297519  Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332(__this, p0, method) ((  KeyValuePair_2_t1028297519  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1028297519_m1457368332_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>(System.Int32)
extern "C"  KeyValuePair_2_t3312956448  Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653(__this, p0, method) ((  KeyValuePair_2_t3312956448  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t3312956448_m1021495653_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>(System.Int32)
extern "C"  KeyValuePair_2_t1610370660  Array_InternalArray__get_Item_TisKeyValuePair_2_t1610370660_m1001108893_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t1610370660_m1001108893(__this, p0, method) ((  KeyValuePair_2_t1610370660  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t1610370660_m1001108893_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>(System.Int32)
extern "C"  KeyValuePair_2_t370248105  Array_InternalArray__get_Item_TisKeyValuePair_2_t370248105_m2277208764_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyValuePair_2_t370248105_m2277208764(__this, p0, method) ((  KeyValuePair_2_t370248105  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyValuePair_2_t370248105_m2277208764_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Generic.Link>(System.Int32)
extern "C"  Link_t2496691359  Array_InternalArray__get_Item_TisLink_t2496691359_m818406549_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLink_t2496691359_m818406549(__this, p0, method) ((  Link_t2496691359  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLink_t2496691359_m818406549_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.Hashtable/Slot>(System.Int32)
extern "C"  Slot_t2579998  Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401(__this, p0, method) ((  Slot_t2579998  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2579998_m2684325401_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Collections.SortedList/Slot>(System.Int32)
extern "C"  Slot_t2579999  Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440(__this, p0, method) ((  Slot_t2579999  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSlot_t2579999_m2216062440_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.DateTime>(System.Int32)
extern "C"  DateTime_t339033936  Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548(__this, p0, method) ((  DateTime_t339033936  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDateTime_t339033936_m185788548_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Decimal>(System.Int32)
extern "C"  Decimal_t1688557254  Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984(__this, p0, method) ((  Decimal_t1688557254  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDecimal_t1688557254_m4127931984_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Double>(System.Int32)
extern "C"  double Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990(__this, p0, method) ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDouble_t534516614_m3142342990_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int16>(System.Int32)
extern "C"  int16_t Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053(__this, p0, method) ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt16_t2847414729_m2614347053_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int32>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt32_t2847414787_m3068135859_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Int64>(System.Int32)
extern "C"  int64_t Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300(__this, p0, method) ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisInt64_t2847414882_m1812029300_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.IntPtr>(System.Int32)
extern "C"  IntPtr_t Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIntPtr_t_m1819425504(__this, p0, method) ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIntPtr_t_m1819425504_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Object>(System.Int32)
extern "C"  Il2CppObject * Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisIl2CppObject_m1537058848(__this, p0, method) ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisIl2CppObject_m1537058848_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeNamedArgument>(System.Int32)
extern "C"  CustomAttributeNamedArgument_t318735129  Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667(__this, p0, method) ((  CustomAttributeNamedArgument_t318735129  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeNamedArgument_t318735129_m25283667_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.CustomAttributeTypedArgument>(System.Int32)
extern "C"  CustomAttributeTypedArgument_t560415562  Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746(__this, p0, method) ((  CustomAttributeTypedArgument_t560415562  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisCustomAttributeTypedArgument_t560415562_m193823746_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelData>(System.Int32)
extern "C"  LabelData_t1395746974  Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434(__this, p0, method) ((  LabelData_t1395746974  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelData_t1395746974_m64093434_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILGenerator/LabelFixup>(System.Int32)
extern "C"  LabelFixup_t320573180  Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500(__this, p0, method) ((  LabelFixup_t320573180  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLabelFixup_t320573180_m2255271500_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.Emit.ILTokenInfo>(System.Int32)
extern "C"  ILTokenInfo_t3723275281  Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117(__this, p0, method) ((  ILTokenInfo_t3723275281  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisILTokenInfo_t3723275281_m1012704117_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Reflection.ParameterModifier>(System.Int32)
extern "C"  ParameterModifier_t500203470  Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410(__this, p0, method) ((  ParameterModifier_t500203470  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisParameterModifier_t500203470_m2808893410_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceCacheItem>(System.Int32)
extern "C"  ResourceCacheItem_t3699857703  Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445(__this, p0, method) ((  ResourceCacheItem_t3699857703  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceCacheItem_t3699857703_m4118445_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Resources.ResourceReader/ResourceInfo>(System.Int32)
extern "C"  ResourceInfo_t4074584572  Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776(__this, p0, method) ((  ResourceInfo_t4074584572  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisResourceInfo_t4074584572_m3024657776_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Runtime.Serialization.Formatters.Binary.TypeTag>(System.Int32)
extern "C"  uint8_t Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397(__this, p0, method) ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTypeTag_t1738289281_m673122397_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.SByte>(System.Int32)
extern "C"  int8_t Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230(__this, p0, method) ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSByte_t2855346064_m2884868230_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Security.Cryptography.X509Certificates.X509ChainStatus>(System.Int32)
extern "C"  X509ChainStatus_t1122151684  Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218(__this, p0, method) ((  X509ChainStatus_t1122151684  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisX509ChainStatus_t1122151684_m1414334218_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Single>(System.Int32)
extern "C"  float Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775(__this, p0, method) ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisSingle_t958209021_m1101558775_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Text.RegularExpressions.Mark>(System.Int32)
extern "C"  Mark_t3725932776  Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisMark_t3725932776_m160824484(__this, p0, method) ((  Mark_t3725932776  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisMark_t3725932776_m160824484_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.TimeSpan>(System.Int32)
extern "C"  TimeSpan_t763862892  Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760(__this, p0, method) ((  TimeSpan_t763862892  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTimeSpan_t763862892_m1118358760_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt16>(System.Int32)
extern "C"  uint16_t Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256(__this, p0, method) ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt16_t985925268_m1629104256_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt32>(System.Int32)
extern "C"  uint32_t Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062(__this, p0, method) ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt32_t985925326_m2082893062_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.UInt64>(System.Int32)
extern "C"  uint64_t Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503(__this, p0, method) ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUInt64_t985925421_m826786503_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<System.Uri/UriScheme>(System.Int32)
extern "C"  UriScheme_t3266528785  Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123(__this, p0, method) ((  UriScheme_t3266528785  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUriScheme_t3266528785_m2328943123_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<TypewriterEffect/FadeEntry>(System.Int32)
extern "C"  FadeEntry_t1095831350  Array_InternalArray__get_Item_TisFadeEntry_t1095831350_m2179658461_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisFadeEntry_t1095831350_m2179658461(__this, p0, method) ((  FadeEntry_t1095831350  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisFadeEntry_t1095831350_m2179658461_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UICamera/DepthEntry>(System.Int32)
extern "C"  DepthEntry_t2194697103  Array_InternalArray__get_Item_TisDepthEntry_t2194697103_m1694698009_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisDepthEntry_t2194697103_m1694698009(__this, p0, method) ((  DepthEntry_t2194697103  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisDepthEntry_t2194697103_m1694698009_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Bounds>(System.Int32)
extern "C"  Bounds_t3518514978  Array_InternalArray__get_Item_TisBounds_t3518514978_m3868888566_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisBounds_t3518514978_m3868888566(__this, p0, method) ((  Bounds_t3518514978  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisBounds_t3518514978_m3868888566_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color>(System.Int32)
extern "C"  Color_t1588175760  Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850(__this, p0, method) ((  Color_t1588175760  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor_t1588175760_m3851996850_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Color32>(System.Int32)
extern "C"  Color32_t4137084207  Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403(__this, p0, method) ((  Color32_t4137084207  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisColor32_t4137084207_m2218876403_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint>(System.Int32)
extern "C"  ContactPoint_t2951122365  Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859(__this, p0, method) ((  ContactPoint_t2951122365  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint_t2951122365_m451644859_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.ContactPoint2D>(System.Int32)
extern "C"  ContactPoint2D_t3963746319  Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017(__this, p0, method) ((  ContactPoint2D_t3963746319  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContactPoint2D_t3963746319_m997735017_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.EventSystems.RaycastResult>(System.Int32)
extern "C"  RaycastResult_t959898689  Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905(__this, p0, method) ((  RaycastResult_t959898689  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastResult_t959898689_m1513632905_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.KeyCode>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisKeyCode_t2371581209_m3074538697_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyCode_t2371581209_m3074538697(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyCode_t2371581209_m3074538697_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Keyframe>(System.Int32)
extern "C"  Keyframe_t2095052507  Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013(__this, p0, method) ((  Keyframe_t2095052507  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisKeyframe_t2095052507_m1061522013_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.LogType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisLogType_t3529269451_m2421798615_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisLogType_t3529269451_m2421798615(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisLogType_t3529269451_m2421798615_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RaycastHit>(System.Int32)
extern "C"  RaycastHit_t46221527  Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793(__this, p0, method) ((  RaycastHit_t46221527  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit_t46221527_m959669793_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.RaycastHit2D>(System.Int32)
extern "C"  RaycastHit2D_t4082783401  Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143(__this, p0, method) ((  RaycastHit2D_t4082783401  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisRaycastHit2D_t4082783401_m3878392143_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SendMouseEvents/HitInfo>(System.Int32)
extern "C"  HitInfo_t2591228609  Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997(__this, p0, method) ((  HitInfo_t2591228609  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisHitInfo_t2591228609_m3354825997_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>(System.Int32)
extern "C"  GcAchievementData_t1317012096  Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226(__this, p0, method) ((  GcAchievementData_t1317012096  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcAchievementData_t1317012096_m625859226_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>(System.Int32)
extern "C"  GcScoreData_t2223678307  Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207(__this, p0, method) ((  GcScoreData_t2223678307  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisGcScoreData_t2223678307_m3611437207_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.TextEditor/TextEditOp>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisTextEditOp_t3429487928_m4291319144_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisTextEditOp_t3429487928_m4291319144(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisTextEditOp_t3429487928_m4291319144_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UI.InputField/ContentType>(System.Int32)
extern "C"  int32_t Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015(__this, p0, method) ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisContentType_t1278737203_m696167015_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UICharInfo>(System.Int32)
extern "C"  UICharInfo_t403820581  Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395(__this, p0, method) ((  UICharInfo_t403820581  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUICharInfo_t403820581_m2153093395_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UILineInfo>(System.Int32)
extern "C"  UILineInfo_t156921283  Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045(__this, p0, method) ((  UILineInfo_t156921283  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUILineInfo_t156921283_m2200325045_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.UIVertex>(System.Int32)
extern "C"  UIVertex_t2260061605  Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059(__this, p0, method) ((  UIVertex_t2260061605  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisUIVertex_t2260061605_m2640447059_gshared)(__this, p0, method)
// !!0 System.Array::InternalArray__get_Item<UnityEngine.Vector2>(System.Int32)
extern "C"  Vector2_t3525329788  Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared (Il2CppArray * __this, int32_t p0, const MethodInfo* method);
#define Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166(__this, p0, method) ((  Vector2_t3525329788  (*) (Il2CppArray *, int32_t, const MethodInfo*))Array_InternalArray__get_Item_TisVector2_t3525329788_m1844444166_gshared)(__this, p0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m2857828086_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  int32_t U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m163175455_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1728905850_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_U24current_2();
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m1867136038_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t49415503 * L_2 = (BetterList_1_t49415503 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Int32U5BU5D_t1809983122* L_3 = (Int32U5BU5D_t1809983122*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t49415503 * L_4 = (BetterList_1_t49415503 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_U24current_2(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t49415503 * L_10 = (BetterList_1_t49415503 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m1786971571_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m504261027_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m504261027_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m504261027_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m4146347777_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2593626376_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1784268677_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m2157268819_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t2334074432 * L_2 = (BetterList_1_t2334074432 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t2334074432 * L_4 = (BetterList_1_t2334074432 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_U24current_2(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t2334074432 * L_10 = (BetterList_1_t2334074432 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3103813374_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m1792780718_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1792780718_gshared (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m1792780718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m3804309706_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  float U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1047030993_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1789359964_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method)
{
	{
		float L_0 = (float)__this->get_U24current_2();
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m163521770_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t2455177033 * L_2 = (BetterList_1_t2455177033 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		SingleU5BU5D_t1219431280* L_3 = (SingleU5BU5D_t1219431280*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t2455177033 * L_4 = (BetterList_1_t2455177033 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		SingleU5BU5D_t1219431280* L_5 = (SingleU5BU5D_t1219431280*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_U24current_2(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t2455177033 * L_10 = (BetterList_1_t2455177033 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m822741639_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Single>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m1450742647_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1450742647_gshared (U3CGetEnumeratorU3Ec__Iterator3_t41752724 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m1450742647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m276727948_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  FadeEntry_t1095831350  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m150729717_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method)
{
	{
		FadeEntry_t1095831350  L_0 = (FadeEntry_t1095831350 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1575443940_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method)
{
	{
		FadeEntry_t1095831350  L_0 = (FadeEntry_t1095831350 )__this->get_U24current_2();
		FadeEntry_t1095831350  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m649803728_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t2592799362 * L_2 = (BetterList_1_t2592799362 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		FadeEntryU5BU5D_t4004785971* L_3 = (FadeEntryU5BU5D_t4004785971*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t2592799362 * L_4 = (BetterList_1_t2592799362 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		FadeEntryU5BU5D_t4004785971* L_5 = (FadeEntryU5BU5D_t4004785971*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_U24current_2(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t2592799362 * L_10 = (BetterList_1_t2592799362 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3840836041_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m2218128185_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m2218128185_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m2218128185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m3718098024_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  DepthEntry_t2194697103  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1321247023_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method)
{
	{
		DepthEntry_t2194697103  L_0 = (DepthEntry_t2194697103 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m195183166_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method)
{
	{
		DepthEntry_t2194697103  L_0 = (DepthEntry_t2194697103 )__this->get_U24current_2();
		DepthEntry_t2194697103  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m1863043980_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t3691665115 * L_2 = (BetterList_1_t3691665115 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		DepthEntryU5BU5D_t2046984534* L_3 = (DepthEntryU5BU5D_t2046984534*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t3691665115 * L_4 = (BetterList_1_t3691665115 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		DepthEntryU5BU5D_t2046984534* L_5 = (DepthEntryU5BU5D_t2046984534*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_U24current_2(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t3691665115 * L_10 = (BetterList_1_t3691665115 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3872661157_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m1364530965_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1364530965_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m1364530965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m3409289007_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Color_t1588175760  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m283889078_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = (Color_t1588175760 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1809604247_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = (Color_t1588175760 )__this->get_U24current_2();
		Color_t1588175760  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m4124414565_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t3085143772 * L_2 = (BetterList_1_t3085143772 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ColorU5BU5D_t3477081137* L_3 = (ColorU5BU5D_t3477081137*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t3085143772 * L_4 = (BetterList_1_t3085143772 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		ColorU5BU5D_t3477081137* L_5 = (ColorU5BU5D_t3477081137*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_U24current_2(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t3085143772 * L_10 = (BetterList_1_t3085143772 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3459939244_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Color>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m1055721948_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1055721948_gshared (U3CGetEnumeratorU3Ec__Iterator3_t671719463 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m1055721948_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m4115823195_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector2_t3525329788  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1852087586_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method)
{
	{
		Vector2_t3525329788  L_0 = (Vector2_t3525329788 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m4249108523_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method)
{
	{
		Vector2_t3525329788  L_0 = (Vector2_t3525329788 )__this->get_U24current_2();
		Vector2_t3525329788  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m678843577_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t727330504 * L_2 = (BetterList_1_t727330504 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Vector2U5BU5D_t2741383957* L_3 = (Vector2U5BU5D_t2741383957*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t727330504 * L_4 = (BetterList_1_t727330504 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		Vector2U5BU5D_t2741383957* L_5 = (Vector2U5BU5D_t2741383957*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_U24current_2(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t727330504 * L_10 = (BetterList_1_t727330504 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3834461144_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m1762256136_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1762256136_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m1762256136_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m2318871836_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector3_t3525329789  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2132437475_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method)
{
	{
		Vector3_t3525329789  L_0 = (Vector3_t3525329789 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m923722250_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method)
{
	{
		Vector3_t3525329789  L_0 = (Vector3_t3525329789 )__this->get_U24current_2();
		Vector3_t3525329789  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m173284952_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t727330505 * L_2 = (BetterList_1_t727330505 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Vector3U5BU5D_t3227571696* L_3 = (Vector3U5BU5D_t3227571696*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t727330505 * L_4 = (BetterList_1_t727330505 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		Vector3U5BU5D_t3227571696* L_5 = (Vector3U5BU5D_t3227571696*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_U24current_2(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t727330505 * L_10 = (BetterList_1_t727330505 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3541058137_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m4260272073_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m4260272073_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m4260272073_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m521920477_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector4_t3525329790  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2412787364_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method)
{
	{
		Vector4_t3525329790  L_0 = (Vector4_t3525329790 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1893303273_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method)
{
	{
		Vector4_t3525329790  L_0 = (Vector4_t3525329790 )__this->get_U24current_2();
		Vector4_t3525329790  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m3962693623_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0065;
		}
	}
	{
		goto IL_0090;
	}

IL_0021:
	{
		BetterList_1_t727330506 * L_2 = (BetterList_1_t727330506 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		Vector4U5BU5D_t3713759435* L_3 = (Vector4U5BU5D_t3713759435*)L_2->get_buffer_0();
		if (!L_3)
		{
			goto IL_0089;
		}
	}
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0073;
	}

IL_003d:
	{
		BetterList_1_t727330506 * L_4 = (BetterList_1_t727330506 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_4);
		Vector4U5BU5D_t3713759435* L_5 = (Vector4U5BU5D_t3713759435*)L_4->get_buffer_0();
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		__this->set_U24current_2(((L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7))));
		__this->set_U24PC_1(1);
		goto IL_0092;
	}

IL_0065:
	{
		int32_t L_8 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_8+(int32_t)1)));
	}

IL_0073:
	{
		int32_t L_9 = (int32_t)__this->get_U3CiU3E__0_0();
		BetterList_1_t727330506 * L_10 = (BetterList_1_t727330506 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_10);
		int32_t L_11 = (int32_t)L_10->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_003d;
		}
	}

IL_0089:
	{
		__this->set_U24PC_1((-1));
	}

IL_0090:
	{
		return (bool)0;
	}

IL_0092:
	{
		return (bool)1;
	}
	// Dead block : IL_0094: ldloc.1
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3247655130_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator3_Reset_m2463320714_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m2463320714_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator3_Reset_m2463320714_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void BetterList`1/CompareFunc<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m2813669491_gshared (CompareFunc_t694073545 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<System.Int32>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m2755697915_gshared (CompareFunc_t694073545 * __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m2755697915((CompareFunc_t694073545 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, int32_t ___left0, int32_t ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<System.Int32>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m1350408102_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m1350408102_gshared (CompareFunc_t694073545 * __this, int32_t ___left0, int32_t ___right1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m1350408102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2654356369_gshared (CompareFunc_t694073545 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m3334039642_gshared (CompareFunc_t2978732474 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<System.Object>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m529220816_gshared (CompareFunc_t2978732474 * __this, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m529220816((CompareFunc_t2978732474 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___left0, Il2CppObject * ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Il2CppObject * ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<System.Object>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m2181926439_gshared (CompareFunc_t2978732474 * __this, Il2CppObject * ___left0, Il2CppObject * ___right1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___left0;
	__d_args[1] = ___right1;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2267781532_gshared (CompareFunc_t2978732474 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m3446778097_gshared (CompareFunc_t3099835075 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<System.Single>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m3147571737_gshared (CompareFunc_t3099835075 * __this, float ___left0, float ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m3147571737((CompareFunc_t3099835075 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, float ___left0, float ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, float ___left0, float ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<System.Single>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m753512048_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m753512048_gshared (CompareFunc_t3099835075 * __this, float ___left0, float ___right1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m753512048_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Single_t958209021_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2488384947_gshared (CompareFunc_t3099835075 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m2987098333_gshared (CompareFunc_t3237457404 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m2695003217_gshared (CompareFunc_t3237457404 * __this, FadeEntry_t1095831350  ___left0, FadeEntry_t1095831350  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m2695003217((CompareFunc_t3237457404 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, FadeEntry_t1095831350  ___left0, FadeEntry_t1095831350  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, FadeEntry_t1095831350  ___left0, FadeEntry_t1095831350  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* FadeEntry_t1095831350_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m2795945852_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m2795945852_gshared (CompareFunc_t3237457404 * __this, FadeEntry_t1095831350  ___left0, FadeEntry_t1095831350  ___right1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m2795945852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(FadeEntry_t1095831350_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(FadeEntry_t1095831350_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<TypewriterEffect/FadeEntry>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m642779259_gshared (CompareFunc_t3237457404 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UICamera/DepthEntry>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m1393544147_gshared (CompareFunc_t41355861 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UICamera/DepthEntry>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m3104379639_gshared (CompareFunc_t41355861 * __this, DepthEntry_t2194697103  ___left0, DepthEntry_t2194697103  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m3104379639((CompareFunc_t41355861 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, DepthEntry_t2194697103  ___left0, DepthEntry_t2194697103  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, DepthEntry_t2194697103  ___left0, DepthEntry_t2194697103  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UICamera/DepthEntry>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* DepthEntry_t2194697103_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m3444521166_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m3444521166_gshared (CompareFunc_t41355861 * __this, DepthEntry_t2194697103  ___left0, DepthEntry_t2194697103  ___right1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m3444521166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(DepthEntry_t2194697103_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(DepthEntry_t2194697103_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UICamera/DepthEntry>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2125902869_gshared (CompareFunc_t41355861 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UnityEngine.Color>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m1713620716_gshared (CompareFunc_t3729801814 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Color>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m283192190_gshared (CompareFunc_t3729801814 * __this, Color_t1588175760  ___left0, Color_t1588175760  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m283192190((CompareFunc_t3729801814 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Color_t1588175760  ___left0, Color_t1588175760  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Color_t1588175760  ___left0, Color_t1588175760  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UnityEngine.Color>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m1887011797_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m1887011797_gshared (CompareFunc_t3729801814 * __this, Color_t1588175760  ___left0, Color_t1588175760  ___right1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m1887011797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Color_t1588175760_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Color_t1588175760_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Color>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m4199524398_gshared (CompareFunc_t3729801814 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m70049280_gshared (CompareFunc_t1371988546 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector2>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m753905514_gshared (CompareFunc_t1371988546 * __this, Vector2_t3525329788  ___left0, Vector2_t3525329788  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m753905514((CompareFunc_t1371988546 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t3525329788  ___left0, Vector2_t3525329788  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Vector2_t3525329788  ___left0, Vector2_t3525329788  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UnityEngine.Vector2>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m2841454145_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m2841454145_gshared (CompareFunc_t1371988546 * __this, Vector2_t3525329788  ___left0, Vector2_t3525329788  ___right1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m2841454145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector2_t3525329788_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Vector2_t3525329788_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m1902503106_gshared (CompareFunc_t1371988546 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m1702853279_gshared (CompareFunc_t1371988547 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector3>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m2261457323_gshared (CompareFunc_t1371988547 * __this, Vector3_t3525329789  ___left0, Vector3_t3525329789  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m2261457323((CompareFunc_t1371988547 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t3525329789  ___left0, Vector3_t3525329789  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Vector3_t3525329789  ___left0, Vector3_t3525329789  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UnityEngine.Vector3>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m3534846850_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m3534846850_gshared (CompareFunc_t1371988547 * __this, Vector3_t3525329789  ___left0, Vector3_t3525329789  ___right1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m3534846850_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector3_t3525329789_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Vector3_t3525329789_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m2927995105_gshared (CompareFunc_t1371988547 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1/CompareFunc<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void CompareFunc__ctor_m3335657278_gshared (CompareFunc_t1371988548 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector4>::Invoke(T,T)
extern "C"  int32_t CompareFunc_Invoke_m3769009132_gshared (CompareFunc_t1371988548 * __this, Vector4_t3525329790  ___left0, Vector4_t3525329790  ___right1, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		CompareFunc_Invoke_m3769009132((CompareFunc_t1371988548 *)__this->get_prev_9(),___left0, ___right1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef int32_t (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t3525329790  ___left0, Vector4_t3525329790  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef int32_t (*FunctionPointerType) (void* __this, Vector4_t3525329790  ___left0, Vector4_t3525329790  ___right1, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___left0, ___right1,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult BetterList`1/CompareFunc<UnityEngine.Vector4>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t CompareFunc_BeginInvoke_m4228239555_MetadataUsageId;
extern "C"  Il2CppObject * CompareFunc_BeginInvoke_m4228239555_gshared (CompareFunc_t1371988548 * __this, Vector4_t3525329790  ___left0, Vector4_t3525329790  ___right1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CompareFunc_BeginInvoke_m4228239555_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[3] = {0};
	__d_args[0] = Box(Vector4_t3525329790_il2cpp_TypeInfo_var, &___left0);
	__d_args[1] = Box(Vector4_t3525329790_il2cpp_TypeInfo_var, &___right1);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (Il2CppObject*)___object3);
}
// System.Int32 BetterList`1/CompareFunc<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CompareFunc_EndInvoke_m3953487104_gshared (CompareFunc_t1371988548 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(int32_t*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void BetterList`1<System.Int32>::.ctor()
extern "C"  void BetterList_1__ctor_m1752750761_gshared (BetterList_1_t49415503 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<System.Int32>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m2458670693_gshared (BetterList_1_t49415503 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<System.Int32>::get_Item(System.Int32)
extern "C"  int32_t BetterList_1_get_Item_m2264406119_gshared (BetterList_1_t49415503 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void BetterList`1<System.Int32>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m4189497524_gshared (BetterList_1_t49415503 * __this, int32_t ___i0, int32_t ___value1, const MethodInfo* method)
{
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		int32_t L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int32_t)L_2);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::AllocateMore()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m1734071085_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m1734071085_gshared (BetterList_1_t49415503 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m1734071085_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t1809983122* V_0 = NULL;
	Int32U5BU5D_t1809983122* G_B3_0 = NULL;
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_1 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (Int32U5BU5D_t1809983122*)G_B3_0;
		Int32U5BU5D_t1809983122* L_3 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		Int32U5BU5D_t1809983122* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0);
	}

IL_0052:
	{
		Int32U5BU5D_t1809983122* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Trim()
extern "C"  void BetterList_1_Trim_m3513273629_gshared (BetterList_1_t49415503 * __this, const MethodInfo* method)
{
	Int32U5BU5D_t1809983122* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Int32U5BU5D_t1809983122* L_2 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (Int32U5BU5D_t1809983122*)((Int32U5BU5D_t1809983122*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		Int32U5BU5D_t1809983122* L_4 = V_0;
		int32_t L_5 = V_1;
		Int32U5BU5D_t1809983122* L_6 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int32_t)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_12 = V_0;
		__this->set_buffer_0(L_12);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((Int32U5BU5D_t1809983122*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Clear()
extern "C"  void BetterList_1_Clear_m3453851348_gshared (BetterList_1_t49415503 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Release()
extern "C"  void BetterList_1_Release_m503310478_gshared (BetterList_1_t49415503 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((Int32U5BU5D_t1809983122*)NULL);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Add(T)
extern "C"  void BetterList_1_Add_m2956176566_gshared (BetterList_1_t49415503 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Int32U5BU5D_t1809983122* L_2 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t49415503 *)__this);
		((  void (*) (BetterList_1_t49415503 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t49415503 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		Int32U5BU5D_t1809983122* L_3 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		int32_t L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int32_t)L_7);
		return;
	}
}
// System.Void BetterList`1<System.Int32>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m2719791901_gshared (BetterList_1_t49415503 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Int32U5BU5D_t1809983122* L_2 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t49415503 *)__this);
		((  void (*) (BetterList_1_t49415503 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t49415503 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		Int32U5BU5D_t1809983122* L_7 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		Int32U5BU5D_t1809983122* L_9 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (int32_t)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___index0;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0043;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_15 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_16 = ___index0;
		int32_t L_17 = ___item1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (int32_t)L_17);
		int32_t L_18 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		int32_t L_19 = ___item1;
		NullCheck((BetterList_1_t49415503 *)__this);
		((  void (*) (BetterList_1_t49415503 *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((BetterList_1_t49415503 *)__this, (int32_t)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<System.Int32>::Contains(T)
extern "C"  bool BetterList_1_Contains_m1402378384_gshared (BetterList_1_t49415503 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Int32U5BU5D_t1809983122* L_1 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = ___item0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Int32_Equals_m4061110258((int32_t*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<System.Int32>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m2360506524_gshared (BetterList_1_t49415503 * __this, int32_t ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Int32U5BU5D_t1809983122* L_1 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = ___item0;
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Int32_Equals_m4061110258((int32_t*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<System.Int32>::Remove(T)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m1069038731_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m1069038731_gshared (BetterList_1_t49415503 * __this, int32_t ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m1069038731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t2281805437 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t2281805437 * L_1 = ((  EqualityComparer_1_t2281805437 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t2281805437 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t2281805437 * L_2 = V_0;
		Int32U5BU5D_t1809983122* L_3 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int32_t L_6 = ___item0;
		NullCheck((EqualityComparer_1_t2281805437 *)L_2);
		bool L_7 = VirtFuncInvoker2< bool, int32_t, int32_t >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Int32>::Equals(!0,!0) */, (EqualityComparer_1_t2281805437 *)L_2, (int32_t)((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (int32_t)L_6);
		if (!L_7)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_8-(int32_t)1)));
		Int32U5BU5D_t1809983122* L_9 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_10 = V_1;
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_3));
		int32_t L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (int32_t)L_11);
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		Int32U5BU5D_t1809983122* L_13 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_14 = V_2;
		Int32U5BU5D_t1809983122* L_15 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (int32_t)((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_21 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_22 = (int32_t)__this->get_size_1();
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_4));
		int32_t L_23 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (int32_t)L_23);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<System.Int32>::RemoveAt(System.Int32)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m593644771_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m593644771_gshared (BetterList_1_t49415503 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m593644771_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		Int32U5BU5D_t1809983122* L_5 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_1));
		int32_t L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (int32_t)L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		Int32U5BU5D_t1809983122* L_9 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		Int32U5BU5D_t1809983122* L_11 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (int32_t)((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0048;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_17 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_18 = (int32_t)__this->get_size_1();
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_19 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (int32_t)L_19);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<System.Int32>::Pop()
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m3036224441_MetadataUsageId;
extern "C"  int32_t BetterList_1_Pop_m3036224441_gshared (BetterList_1_t49415503 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m3036224441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_2 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		V_0 = (int32_t)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		Int32U5BU5D_t1809983122* L_7 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_8 = (int32_t)__this->get_size_1();
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_2));
		int32_t L_9 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (int32_t)L_9);
		int32_t L_10 = V_0;
		return L_10;
	}

IL_004f:
	{
		Initobj (Int32_t2847414787_il2cpp_TypeInfo_var, (&V_3));
		int32_t L_11 = V_3;
		return L_11;
	}
}
// T[] BetterList`1<System.Int32>::ToArray()
extern "C"  Int32U5BU5D_t1809983122* BetterList_1_ToArray_m3197892328_gshared (BetterList_1_t49415503 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t49415503 *)__this);
		((  void (*) (BetterList_1_t49415503 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((BetterList_1_t49415503 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Int32U5BU5D_t1809983122* L_0 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<System.Int32>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m89075084_gshared (BetterList_1_t49415503 * __this, CompareFunc_t694073545 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t694073545 * L_2 = ___comparer0;
		Int32U5BU5D_t1809983122* L_3 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Int32U5BU5D_t1809983122* L_6 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		NullCheck((CompareFunc_t694073545 *)L_2);
		int32_t L_9 = ((  int32_t (*) (CompareFunc_t694073545 *, int32_t, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CompareFunc_t694073545 *)L_2, (int32_t)((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (int32_t)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		Int32U5BU5D_t1809983122* L_10 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = (int32_t)((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		Int32U5BU5D_t1809983122* L_13 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_14 = V_3;
		Int32U5BU5D_t1809983122* L_15 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (int32_t)((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		Int32U5BU5D_t1809983122* L_18 = (Int32U5BU5D_t1809983122*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		int32_t L_20 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)1))), (int32_t)L_20);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_3;
		if (L_22)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_23 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<System.Object>::.ctor()
extern "C"  void BetterList_1__ctor_m4248689070_gshared (BetterList_1_t2334074432 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m3505227298_gshared (BetterList_1_t2334074432 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator3_t4215617419 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t4215617419 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * BetterList_1_get_Item_m638315300_gshared (BetterList_1_t2334074432 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void BetterList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m747172943_gshared (BetterList_1_t2334074432 * __this, int32_t ___i0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Il2CppObject * L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Il2CppObject *)L_2);
		return;
	}
}
// System.Void BetterList`1<System.Object>::AllocateMore()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m19911816_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m19911816_gshared (BetterList_1_t2334074432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m19911816_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t11523773* V_0 = NULL;
	ObjectU5BU5D_t11523773* G_B3_0 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (ObjectU5BU5D_t11523773*)G_B3_0;
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		ObjectU5BU5D_t11523773* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0);
	}

IL_0052:
	{
		ObjectU5BU5D_t11523773* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<System.Object>::Trim()
extern "C"  void BetterList_1_Trim_m3593787768_gshared (BetterList_1_t2334074432 * __this, const MethodInfo* method)
{
	ObjectU5BU5D_t11523773* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (ObjectU5BU5D_t11523773*)((ObjectU5BU5D_t11523773*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		ObjectU5BU5D_t11523773* L_4 = V_0;
		int32_t L_5 = V_1;
		ObjectU5BU5D_t11523773* L_6 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Il2CppObject *)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_12 = V_0;
		__this->set_buffer_0(L_12);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((ObjectU5BU5D_t11523773*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<System.Object>::Clear()
extern "C"  void BetterList_1_Clear_m1654822361_gshared (BetterList_1_t2334074432 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<System.Object>::Release()
extern "C"  void BetterList_1_Release_m2508274259_gshared (BetterList_1_t2334074432 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((ObjectU5BU5D_t11523773*)NULL);
		return;
	}
}
// System.Void BetterList`1<System.Object>::Add(T)
extern "C"  void BetterList_1_Add_m3036690705_gshared (BetterList_1_t2334074432 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2334074432 *)__this);
		((  void (*) (BetterList_1_t2334074432 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t2334074432 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Il2CppObject * L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_7);
		return;
	}
}
// System.Void BetterList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m548614520_gshared (BetterList_1_t2334074432 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2334074432 *)__this);
		((  void (*) (BetterList_1_t2334074432 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t2334074432 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Il2CppObject *)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___index0;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0043;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_16 = ___index0;
		Il2CppObject * L_17 = ___item1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (Il2CppObject *)L_17);
		int32_t L_18 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Il2CppObject * L_19 = ___item1;
		NullCheck((BetterList_1_t2334074432 *)__this);
		((  void (*) (BetterList_1_t2334074432 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((BetterList_1_t2334074432 *)__this, (Il2CppObject *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<System.Object>::Contains(T)
extern "C"  bool BetterList_1_Contains_m1227426045_gshared (BetterList_1_t2334074432 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Il2CppObject * L_3 = ___item0;
		NullCheck((Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_5 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<System.Object>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m3505521179_gshared (BetterList_1_t2334074432 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Il2CppObject * L_3 = ___item0;
		NullCheck((Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2)))), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_5 = V_0;
		return L_5;
	}

IL_0038:
	{
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_7 = V_0;
		int32_t L_8 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_7) < ((int32_t)L_8)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<System.Object>::Remove(T)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m1444275256_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m1444275256_gshared (BetterList_1_t2334074432 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m1444275256_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t271497070 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Il2CppObject * V_3 = NULL;
	Il2CppObject * V_4 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t271497070 * L_1 = ((  EqualityComparer_1_t271497070 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t271497070 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t271497070 * L_2 = V_0;
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = ___item0;
		NullCheck((EqualityComparer_1_t271497070 *)L_2);
		bool L_7 = VirtFuncInvoker2< bool, Il2CppObject *, Il2CppObject * >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Object>::Equals(!0,!0) */, (EqualityComparer_1_t271497070 *)L_2, (Il2CppObject *)((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Il2CppObject *)L_6);
		if (!L_7)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_8-(int32_t)1)));
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_10 = V_1;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Il2CppObject *)L_11);
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_14 = V_2;
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_21 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_22 = (int32_t)__this->get_size_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_4));
		Il2CppObject * L_23 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (Il2CppObject *)L_23);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<System.Object>::RemoveAt(System.Int32)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m2717434686_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m2717434686_gshared (BetterList_1_t2334074432 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m2717434686_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		ObjectU5BU5D_t11523773* L_5 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_1));
		Il2CppObject * L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Il2CppObject *)L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		ObjectU5BU5D_t11523773* L_11 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Il2CppObject *)((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0048;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_17 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_18 = (int32_t)__this->get_size_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_19 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_19);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<System.Object>::Pop()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m1337402204_MetadataUsageId;
extern "C"  Il2CppObject * BetterList_1_Pop_m1337402204_gshared (BetterList_1_t2334074432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m1337402204_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	Il2CppObject * V_2 = NULL;
	Il2CppObject * V_3 = NULL;
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_2 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		V_0 = (Il2CppObject *)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		ObjectU5BU5D_t11523773* L_7 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_8 = (int32_t)__this->get_size_1();
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_2));
		Il2CppObject * L_9 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Il2CppObject *)L_9);
		Il2CppObject * L_10 = V_0;
		return L_10;
	}

IL_004f:
	{
		Initobj (Il2CppObject_il2cpp_TypeInfo_var, (&V_3));
		Il2CppObject * L_11 = V_3;
		return L_11;
	}
}
// T[] BetterList`1<System.Object>::ToArray()
extern "C"  ObjectU5BU5D_t11523773* BetterList_1_ToArray_m1744488967_gshared (BetterList_1_t2334074432 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t2334074432 *)__this);
		((  void (*) (BetterList_1_t2334074432 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((BetterList_1_t2334074432 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<System.Object>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1264107601_gshared (BetterList_1_t2334074432 * __this, CompareFunc_t2978732474 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Il2CppObject * V_4 = NULL;
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t2978732474 * L_2 = ___comparer0;
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		ObjectU5BU5D_t11523773* L_6 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		NullCheck((CompareFunc_t2978732474 *)L_2);
		int32_t L_9 = ((  int32_t (*) (CompareFunc_t2978732474 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CompareFunc_t2978732474 *)L_2, (Il2CppObject *)((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Il2CppObject *)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		ObjectU5BU5D_t11523773* L_10 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = (Il2CppObject *)((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		ObjectU5BU5D_t11523773* L_13 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_14 = V_3;
		ObjectU5BU5D_t11523773* L_15 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Il2CppObject *)((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		ObjectU5BU5D_t11523773* L_18 = (ObjectU5BU5D_t11523773*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		Il2CppObject * L_20 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)1))), (Il2CppObject *)L_20);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_3;
		if (L_22)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_23 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<System.Single>::.ctor()
extern "C"  void BetterList_1__ctor_m3906650999_gshared (BetterList_1_t2455177033 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<System.Single>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m323786475_gshared (BetterList_1_t2455177033 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator3_t41752724 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t41752724 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t41752724 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t41752724 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator3_t41752724 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t41752724 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t41752724 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<System.Single>::get_Item(System.Int32)
extern "C"  float BetterList_1_get_Item_m3982956091_gshared (BetterList_1_t2455177033 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void BetterList`1<System.Single>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m2311435686_gshared (BetterList_1_t2455177033 * __this, int32_t ___i0, float ___value1, const MethodInfo* method)
{
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		float L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (float)L_2);
		return;
	}
}
// System.Void BetterList`1<System.Single>::AllocateMore()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m2411136671_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m2411136671_gshared (BetterList_1_t2455177033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m2411136671_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t1219431280* V_0 = NULL;
	SingleU5BU5D_t1219431280* G_B3_0 = NULL;
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		SingleU5BU5D_t1219431280* L_1 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((SingleU5BU5D_t1219431280*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((SingleU5BU5D_t1219431280*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (SingleU5BU5D_t1219431280*)G_B3_0;
		SingleU5BU5D_t1219431280* L_3 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		SingleU5BU5D_t1219431280* L_5 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		SingleU5BU5D_t1219431280* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0);
	}

IL_0052:
	{
		SingleU5BU5D_t1219431280* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<System.Single>::Trim()
extern "C"  void BetterList_1_Trim_m534712975_gshared (BetterList_1_t2455177033 * __this, const MethodInfo* method)
{
	SingleU5BU5D_t1219431280* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		SingleU5BU5D_t1219431280* L_2 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (SingleU5BU5D_t1219431280*)((SingleU5BU5D_t1219431280*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		SingleU5BU5D_t1219431280* L_4 = V_0;
		int32_t L_5 = V_1;
		SingleU5BU5D_t1219431280* L_6 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (float)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		SingleU5BU5D_t1219431280* L_12 = V_0;
		__this->set_buffer_0(L_12);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((SingleU5BU5D_t1219431280*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<System.Single>::Clear()
extern "C"  void BetterList_1_Clear_m1312784290_gshared (BetterList_1_t2455177033 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<System.Single>::Release()
extern "C"  void BetterList_1_Release_m227202524_gshared (BetterList_1_t2455177033 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((SingleU5BU5D_t1219431280*)NULL);
		return;
	}
}
// System.Void BetterList`1<System.Single>::Add(T)
extern "C"  void BetterList_1_Add_m4272583208_gshared (BetterList_1_t2455177033 * __this, float ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		SingleU5BU5D_t1219431280* L_2 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2455177033 *)__this);
		((  void (*) (BetterList_1_t2455177033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t2455177033 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		SingleU5BU5D_t1219431280* L_3 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		float L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (float)L_7);
		return;
	}
}
// System.Void BetterList`1<System.Single>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m3893255311_gshared (BetterList_1_t2455177033 * __this, int32_t ___index0, float ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		SingleU5BU5D_t1219431280* L_2 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2455177033 *)__this);
		((  void (*) (BetterList_1_t2455177033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t2455177033 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		SingleU5BU5D_t1219431280* L_7 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		SingleU5BU5D_t1219431280* L_9 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (float)((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___index0;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0043;
		}
	}
	{
		SingleU5BU5D_t1219431280* L_15 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_16 = ___index0;
		float L_17 = ___item1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (float)L_17);
		int32_t L_18 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		float L_19 = ___item1;
		NullCheck((BetterList_1_t2455177033 *)__this);
		((  void (*) (BetterList_1_t2455177033 *, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((BetterList_1_t2455177033 *)__this, (float)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<System.Single>::Contains(T)
extern "C"  bool BetterList_1_Contains_m3845776966_gshared (BetterList_1_t2455177033 * __this, float ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		SingleU5BU5D_t1219431280* L_1 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		float L_3 = ___item0;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Single_Equals_m2650902624((float*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<System.Single>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m1511774130_gshared (BetterList_1_t2455177033 * __this, float ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		SingleU5BU5D_t1219431280* L_1 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		float L_3 = ___item0;
		float L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Single_Equals_m2650902624((float*)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<System.Single>::Remove(T)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m3458170817_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m3458170817_gshared (BetterList_1_t2455177033 * __this, float ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m3458170817_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t392599671 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t392599671 * L_1 = ((  EqualityComparer_1_t392599671 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t392599671 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t392599671 * L_2 = V_0;
		SingleU5BU5D_t1219431280* L_3 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		float L_6 = ___item0;
		NullCheck((EqualityComparer_1_t392599671 *)L_2);
		bool L_7 = VirtFuncInvoker2< bool, float, float >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<System.Single>::Equals(!0,!0) */, (EqualityComparer_1_t392599671 *)L_2, (float)((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (float)L_6);
		if (!L_7)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_8-(int32_t)1)));
		SingleU5BU5D_t1219431280* L_9 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_10 = V_1;
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_3));
		float L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (float)L_11);
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		SingleU5BU5D_t1219431280* L_13 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_14 = V_2;
		SingleU5BU5D_t1219431280* L_15 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (float)((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		SingleU5BU5D_t1219431280* L_21 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_22 = (int32_t)__this->get_size_1();
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_4));
		float L_23 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (float)L_23);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<System.Single>::RemoveAt(System.Int32)
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m1767108181_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m1767108181_gshared (BetterList_1_t2455177033 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m1767108181_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		SingleU5BU5D_t1219431280* L_5 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_1));
		float L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (float)L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		SingleU5BU5D_t1219431280* L_9 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		SingleU5BU5D_t1219431280* L_11 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (float)((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0048;
		}
	}
	{
		SingleU5BU5D_t1219431280* L_17 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_18 = (int32_t)__this->get_size_1();
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_2));
		float L_19 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (float)L_19);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<System.Single>::Pop()
extern Il2CppClass* Single_t958209021_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m2347101029_MetadataUsageId;
extern "C"  float BetterList_1_Pop_m2347101029_gshared (BetterList_1_t2455177033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m2347101029_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		SingleU5BU5D_t1219431280* L_2 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		V_0 = (float)((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		SingleU5BU5D_t1219431280* L_7 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_8 = (int32_t)__this->get_size_1();
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_2));
		float L_9 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (float)L_9);
		float L_10 = V_0;
		return L_10;
	}

IL_004f:
	{
		Initobj (Single_t958209021_il2cpp_TypeInfo_var, (&V_3));
		float L_11 = V_3;
		return L_11;
	}
}
// T[] BetterList`1<System.Single>::ToArray()
extern "C"  SingleU5BU5D_t1219431280* BetterList_1_ToArray_m3758384528_gshared (BetterList_1_t2455177033 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t2455177033 *)__this);
		((  void (*) (BetterList_1_t2455177033 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((BetterList_1_t2455177033 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		SingleU5BU5D_t1219431280* L_0 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<System.Single>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1960480986_gshared (BetterList_1_t2455177033 * __this, CompareFunc_t3099835075 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	float V_4 = 0.0f;
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t3099835075 * L_2 = ___comparer0;
		SingleU5BU5D_t1219431280* L_3 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		SingleU5BU5D_t1219431280* L_6 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		NullCheck((CompareFunc_t3099835075 *)L_2);
		int32_t L_9 = ((  int32_t (*) (CompareFunc_t3099835075 *, float, float, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CompareFunc_t3099835075 *)L_2, (float)((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (float)((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		SingleU5BU5D_t1219431280* L_10 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = (float)((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		SingleU5BU5D_t1219431280* L_13 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_14 = V_3;
		SingleU5BU5D_t1219431280* L_15 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (float)((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		SingleU5BU5D_t1219431280* L_18 = (SingleU5BU5D_t1219431280*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		float L_20 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)1))), (float)L_20);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_3;
		if (L_22)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_23 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::.ctor()
extern "C"  void BetterList_1__ctor_m2301652735_gshared (BetterList_1_t2592799362 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<TypewriterEffect/FadeEntry>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m3698161083_gshared (BetterList_1_t2592799362 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator3_t179375053 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t179375053 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t179375053 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t179375053 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator3_t179375053 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t179375053 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t179375053 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<TypewriterEffect/FadeEntry>::get_Item(System.Int32)
extern "C"  FadeEntry_t1095831350  BetterList_1_get_Item_m1644696337_gshared (BetterList_1_t2592799362 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m3717958430_gshared (BetterList_1_t2592799362 * __this, int32_t ___i0, FadeEntry_t1095831350  ___value1, const MethodInfo* method)
{
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		FadeEntry_t1095831350  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (FadeEntry_t1095831350 )L_2);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::AllocateMore()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m1851445783_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m1851445783_gshared (BetterList_1_t2592799362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m1851445783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FadeEntryU5BU5D_t4004785971* V_0 = NULL;
	FadeEntryU5BU5D_t4004785971* G_B3_0 = NULL;
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		FadeEntryU5BU5D_t4004785971* L_1 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((FadeEntryU5BU5D_t4004785971*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((FadeEntryU5BU5D_t4004785971*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (FadeEntryU5BU5D_t4004785971*)G_B3_0;
		FadeEntryU5BU5D_t4004785971* L_3 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		FadeEntryU5BU5D_t4004785971* L_5 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		FadeEntryU5BU5D_t4004785971* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0);
	}

IL_0052:
	{
		FadeEntryU5BU5D_t4004785971* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Trim()
extern "C"  void BetterList_1_Trim_m2145506823_gshared (BetterList_1_t2592799362 * __this, const MethodInfo* method)
{
	FadeEntryU5BU5D_t4004785971* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		FadeEntryU5BU5D_t4004785971* L_2 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (FadeEntryU5BU5D_t4004785971*)((FadeEntryU5BU5D_t4004785971*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		FadeEntryU5BU5D_t4004785971* L_4 = V_0;
		int32_t L_5 = V_1;
		FadeEntryU5BU5D_t4004785971* L_6 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (FadeEntry_t1095831350 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		FadeEntryU5BU5D_t4004785971* L_12 = V_0;
		__this->set_buffer_0(L_12);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((FadeEntryU5BU5D_t4004785971*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Clear()
extern "C"  void BetterList_1_Clear_m4002753322_gshared (BetterList_1_t2592799362 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Release()
extern "C"  void BetterList_1_Release_m4012097380_gshared (BetterList_1_t2592799362 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((FadeEntryU5BU5D_t4004785971*)NULL);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Add(T)
extern "C"  void BetterList_1_Add_m1588409760_gshared (BetterList_1_t2592799362 * __this, FadeEntry_t1095831350  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		FadeEntryU5BU5D_t4004785971* L_2 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2592799362 *)__this);
		((  void (*) (BetterList_1_t2592799362 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t2592799362 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		FadeEntryU5BU5D_t4004785971* L_3 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		FadeEntry_t1095831350  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (FadeEntry_t1095831350 )L_7);
		return;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m936062983_gshared (BetterList_1_t2592799362 * __this, int32_t ___index0, FadeEntry_t1095831350  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		FadeEntryU5BU5D_t4004785971* L_2 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t2592799362 *)__this);
		((  void (*) (BetterList_1_t2592799362 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t2592799362 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		FadeEntryU5BU5D_t4004785971* L_7 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		FadeEntryU5BU5D_t4004785971* L_9 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (FadeEntry_t1095831350 )((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___index0;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0043;
		}
	}
	{
		FadeEntryU5BU5D_t4004785971* L_15 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_16 = ___index0;
		FadeEntry_t1095831350  L_17 = ___item1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (FadeEntry_t1095831350 )L_17);
		int32_t L_18 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		FadeEntry_t1095831350  L_19 = ___item1;
		NullCheck((BetterList_1_t2592799362 *)__this);
		((  void (*) (BetterList_1_t2592799362 *, FadeEntry_t1095831350 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((BetterList_1_t2592799362 *)__this, (FadeEntry_t1095831350 )L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<TypewriterEffect/FadeEntry>::Contains(T)
extern "C"  bool BetterList_1_Contains_m2163227878_gshared (BetterList_1_t2592799362 * __this, FadeEntry_t1095831350  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		FadeEntryU5BU5D_t4004785971* L_1 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		FadeEntry_t1095831350  L_3 = ___item0;
		FadeEntry_t1095831350  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<TypewriterEffect/FadeEntry>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m1957571590_gshared (BetterList_1_t2592799362 * __this, FadeEntry_t1095831350  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		FadeEntryU5BU5D_t4004785971* L_1 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		FadeEntry_t1095831350  L_3 = ___item0;
		FadeEntry_t1095831350  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_8 = V_0;
		return L_8;
	}

IL_0038:
	{
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<TypewriterEffect/FadeEntry>::Remove(T)
extern Il2CppClass* FadeEntry_t1095831350_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m582680161_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m582680161_gshared (BetterList_1_t2592799362 * __this, FadeEntry_t1095831350  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m582680161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t530222000 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	FadeEntry_t1095831350  V_3;
	memset(&V_3, 0, sizeof(V_3));
	FadeEntry_t1095831350  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t530222000 * L_1 = ((  EqualityComparer_1_t530222000 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t530222000 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t530222000 * L_2 = V_0;
		FadeEntryU5BU5D_t4004785971* L_3 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		FadeEntry_t1095831350  L_6 = ___item0;
		NullCheck((EqualityComparer_1_t530222000 *)L_2);
		bool L_7 = VirtFuncInvoker2< bool, FadeEntry_t1095831350 , FadeEntry_t1095831350  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<TypewriterEffect/FadeEntry>::Equals(!0,!0) */, (EqualityComparer_1_t530222000 *)L_2, (FadeEntry_t1095831350 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (FadeEntry_t1095831350 )L_6);
		if (!L_7)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_8-(int32_t)1)));
		FadeEntryU5BU5D_t4004785971* L_9 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_10 = V_1;
		Initobj (FadeEntry_t1095831350_il2cpp_TypeInfo_var, (&V_3));
		FadeEntry_t1095831350  L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (FadeEntry_t1095831350 )L_11);
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		FadeEntryU5BU5D_t4004785971* L_13 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_14 = V_2;
		FadeEntryU5BU5D_t4004785971* L_15 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (FadeEntry_t1095831350 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		FadeEntryU5BU5D_t4004785971* L_21 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_22 = (int32_t)__this->get_size_1();
		Initobj (FadeEntry_t1095831350_il2cpp_TypeInfo_var, (&V_4));
		FadeEntry_t1095831350  L_23 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (FadeEntry_t1095831350 )L_23);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::RemoveAt(System.Int32)
extern Il2CppClass* FadeEntry_t1095831350_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m3104883149_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m3104883149_gshared (BetterList_1_t2592799362 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m3104883149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	FadeEntry_t1095831350  V_1;
	memset(&V_1, 0, sizeof(V_1));
	FadeEntry_t1095831350  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		FadeEntryU5BU5D_t4004785971* L_5 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (FadeEntry_t1095831350_il2cpp_TypeInfo_var, (&V_1));
		FadeEntry_t1095831350  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (FadeEntry_t1095831350 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		FadeEntryU5BU5D_t4004785971* L_9 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		FadeEntryU5BU5D_t4004785971* L_11 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (FadeEntry_t1095831350 )((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0048;
		}
	}
	{
		FadeEntryU5BU5D_t4004785971* L_17 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_18 = (int32_t)__this->get_size_1();
		Initobj (FadeEntry_t1095831350_il2cpp_TypeInfo_var, (&V_2));
		FadeEntry_t1095831350  L_19 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (FadeEntry_t1095831350 )L_19);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<TypewriterEffect/FadeEntry>::Pop()
extern Il2CppClass* FadeEntry_t1095831350_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m2212040655_MetadataUsageId;
extern "C"  FadeEntry_t1095831350  BetterList_1_Pop_m2212040655_gshared (BetterList_1_t2592799362 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m2212040655_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FadeEntry_t1095831350  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	FadeEntry_t1095831350  V_2;
	memset(&V_2, 0, sizeof(V_2));
	FadeEntry_t1095831350  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		FadeEntryU5BU5D_t4004785971* L_2 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		V_0 = (FadeEntry_t1095831350 )((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		FadeEntryU5BU5D_t4004785971* L_7 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_8 = (int32_t)__this->get_size_1();
		Initobj (FadeEntry_t1095831350_il2cpp_TypeInfo_var, (&V_2));
		FadeEntry_t1095831350  L_9 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (FadeEntry_t1095831350 )L_9);
		FadeEntry_t1095831350  L_10 = V_0;
		return L_10;
	}

IL_004f:
	{
		Initobj (FadeEntry_t1095831350_il2cpp_TypeInfo_var, (&V_3));
		FadeEntry_t1095831350  L_11 = V_3;
		return L_11;
	}
}
// T[] BetterList`1<TypewriterEffect/FadeEntry>::ToArray()
extern "C"  FadeEntryU5BU5D_t4004785971* BetterList_1_ToArray_m3002788990_gshared (BetterList_1_t2592799362 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t2592799362 *)__this);
		((  void (*) (BetterList_1_t2592799362 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((BetterList_1_t2592799362 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		FadeEntryU5BU5D_t4004785971* L_0 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<TypewriterEffect/FadeEntry>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1043423842_gshared (BetterList_1_t2592799362 * __this, CompareFunc_t3237457404 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	FadeEntry_t1095831350  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t3237457404 * L_2 = ___comparer0;
		FadeEntryU5BU5D_t4004785971* L_3 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		FadeEntryU5BU5D_t4004785971* L_6 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		NullCheck((CompareFunc_t3237457404 *)L_2);
		int32_t L_9 = ((  int32_t (*) (CompareFunc_t3237457404 *, FadeEntry_t1095831350 , FadeEntry_t1095831350 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CompareFunc_t3237457404 *)L_2, (FadeEntry_t1095831350 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (FadeEntry_t1095831350 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		FadeEntryU5BU5D_t4004785971* L_10 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = (FadeEntry_t1095831350 )((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		FadeEntryU5BU5D_t4004785971* L_13 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_14 = V_3;
		FadeEntryU5BU5D_t4004785971* L_15 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (FadeEntry_t1095831350 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		FadeEntryU5BU5D_t4004785971* L_18 = (FadeEntryU5BU5D_t4004785971*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		FadeEntry_t1095831350  L_20 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)1))), (FadeEntry_t1095831350 )L_20);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_3;
		if (L_22)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_23 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::.ctor()
extern "C"  void BetterList_1__ctor_m1815204181_gshared (BetterList_1_t3691665115 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UICamera/DepthEntry>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m3336479177_gshared (BetterList_1_t3691665115 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UICamera/DepthEntry>::get_Item(System.Int32)
extern "C"  DepthEntry_t2194697103  BetterList_1_get_Item_m1053881309_gshared (BetterList_1_t3691665115 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m508321672_gshared (BetterList_1_t3691665115 * __this, int32_t ___i0, DepthEntry_t2194697103  ___value1, const MethodInfo* method)
{
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		DepthEntry_t2194697103  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (DepthEntry_t2194697103 )L_2);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::AllocateMore()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m1896628993_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m1896628993_gshared (BetterList_1_t3691665115 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m1896628993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DepthEntryU5BU5D_t2046984534* V_0 = NULL;
	DepthEntryU5BU5D_t2046984534* G_B3_0 = NULL;
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		DepthEntryU5BU5D_t2046984534* L_1 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((DepthEntryU5BU5D_t2046984534*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((DepthEntryU5BU5D_t2046984534*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (DepthEntryU5BU5D_t2046984534*)G_B3_0;
		DepthEntryU5BU5D_t2046984534* L_3 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		DepthEntryU5BU5D_t2046984534* L_5 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		DepthEntryU5BU5D_t2046984534* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0);
	}

IL_0052:
	{
		DepthEntryU5BU5D_t2046984534* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Trim()
extern "C"  void BetterList_1_Trim_m882888945_gshared (BetterList_1_t3691665115 * __this, const MethodInfo* method)
{
	DepthEntryU5BU5D_t2046984534* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		DepthEntryU5BU5D_t2046984534* L_2 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (DepthEntryU5BU5D_t2046984534*)((DepthEntryU5BU5D_t2046984534*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		DepthEntryU5BU5D_t2046984534* L_4 = V_0;
		int32_t L_5 = V_1;
		DepthEntryU5BU5D_t2046984534* L_6 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (DepthEntry_t2194697103 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		DepthEntryU5BU5D_t2046984534* L_12 = V_0;
		__this->set_buffer_0(L_12);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((DepthEntryU5BU5D_t2046984534*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Clear()
extern "C"  void BetterList_1_Clear_m3516304768_gshared (BetterList_1_t3691665115 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Release()
extern "C"  void BetterList_1_Release_m391504954_gshared (BetterList_1_t3691665115 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((DepthEntryU5BU5D_t2046984534*)NULL);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Add(T)
extern "C"  void BetterList_1_Add_m325791882_gshared (BetterList_1_t3691665115 * __this, DepthEntry_t2194697103  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		DepthEntryU5BU5D_t2046984534* L_2 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t3691665115 *)__this);
		((  void (*) (BetterList_1_t3691665115 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t3691665115 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		DepthEntryU5BU5D_t2046984534* L_3 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		DepthEntry_t2194697103  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (DepthEntry_t2194697103 )L_7);
		return;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m1889146609_gshared (BetterList_1_t3691665115 * __this, int32_t ___index0, DepthEntry_t2194697103  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		DepthEntryU5BU5D_t2046984534* L_2 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t3691665115 *)__this);
		((  void (*) (BetterList_1_t3691665115 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t3691665115 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		DepthEntryU5BU5D_t2046984534* L_7 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		DepthEntryU5BU5D_t2046984534* L_9 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (DepthEntry_t2194697103 )((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___index0;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0043;
		}
	}
	{
		DepthEntryU5BU5D_t2046984534* L_15 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_16 = ___index0;
		DepthEntry_t2194697103  L_17 = ___item1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (DepthEntry_t2194697103 )L_17);
		int32_t L_18 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		DepthEntry_t2194697103  L_19 = ___item1;
		NullCheck((BetterList_1_t3691665115 *)__this);
		((  void (*) (BetterList_1_t3691665115 *, DepthEntry_t2194697103 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((BetterList_1_t3691665115 *)__this, (DepthEntry_t2194697103 )L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UICamera/DepthEntry>::Contains(T)
extern "C"  bool BetterList_1_Contains_m2157248036_gshared (BetterList_1_t3691665115 * __this, DepthEntry_t2194697103  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		DepthEntryU5BU5D_t2046984534* L_1 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		DepthEntry_t2194697103  L_3 = ___item0;
		DepthEntry_t2194697103  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UICamera/DepthEntry>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m2812979092_gshared (BetterList_1_t3691665115 * __this, DepthEntry_t2194697103  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		DepthEntryU5BU5D_t2046984534* L_1 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		DepthEntry_t2194697103  L_3 = ___item0;
		DepthEntry_t2194697103  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_8 = V_0;
		return L_8;
	}

IL_0038:
	{
		int32_t L_9 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UICamera/DepthEntry>::Remove(T)
extern Il2CppClass* DepthEntry_t2194697103_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m310048543_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m310048543_gshared (BetterList_1_t3691665115 * __this, DepthEntry_t2194697103  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m310048543_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t1629087753 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	DepthEntry_t2194697103  V_3;
	memset(&V_3, 0, sizeof(V_3));
	DepthEntry_t2194697103  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t1629087753 * L_1 = ((  EqualityComparer_1_t1629087753 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t1629087753 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t1629087753 * L_2 = V_0;
		DepthEntryU5BU5D_t2046984534* L_3 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		DepthEntry_t2194697103  L_6 = ___item0;
		NullCheck((EqualityComparer_1_t1629087753 *)L_2);
		bool L_7 = VirtFuncInvoker2< bool, DepthEntry_t2194697103 , DepthEntry_t2194697103  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UICamera/DepthEntry>::Equals(!0,!0) */, (EqualityComparer_1_t1629087753 *)L_2, (DepthEntry_t2194697103 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (DepthEntry_t2194697103 )L_6);
		if (!L_7)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_8-(int32_t)1)));
		DepthEntryU5BU5D_t2046984534* L_9 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_10 = V_1;
		Initobj (DepthEntry_t2194697103_il2cpp_TypeInfo_var, (&V_3));
		DepthEntry_t2194697103  L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (DepthEntry_t2194697103 )L_11);
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		DepthEntryU5BU5D_t2046984534* L_13 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_14 = V_2;
		DepthEntryU5BU5D_t2046984534* L_15 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (DepthEntry_t2194697103 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		DepthEntryU5BU5D_t2046984534* L_21 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_22 = (int32_t)__this->get_size_1();
		Initobj (DepthEntry_t2194697103_il2cpp_TypeInfo_var, (&V_4));
		DepthEntry_t2194697103  L_23 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (DepthEntry_t2194697103 )L_23);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::RemoveAt(System.Int32)
extern Il2CppClass* DepthEntry_t2194697103_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m4057966775_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m4057966775_gshared (BetterList_1_t3691665115 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m4057966775_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	DepthEntry_t2194697103  V_1;
	memset(&V_1, 0, sizeof(V_1));
	DepthEntry_t2194697103  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		DepthEntryU5BU5D_t2046984534* L_5 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (DepthEntry_t2194697103_il2cpp_TypeInfo_var, (&V_1));
		DepthEntry_t2194697103  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (DepthEntry_t2194697103 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		DepthEntryU5BU5D_t2046984534* L_9 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		DepthEntryU5BU5D_t2046984534* L_11 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (DepthEntry_t2194697103 )((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0048;
		}
	}
	{
		DepthEntryU5BU5D_t2046984534* L_17 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_18 = (int32_t)__this->get_size_1();
		Initobj (DepthEntry_t2194697103_il2cpp_TypeInfo_var, (&V_2));
		DepthEntry_t2194697103  L_19 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (DepthEntry_t2194697103 )L_19);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UICamera/DepthEntry>::Pop()
extern Il2CppClass* DepthEntry_t2194697103_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m1257557379_MetadataUsageId;
extern "C"  DepthEntry_t2194697103  BetterList_1_Pop_m1257557379_gshared (BetterList_1_t3691665115 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m1257557379_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	DepthEntry_t2194697103  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	DepthEntry_t2194697103  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DepthEntry_t2194697103  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		DepthEntryU5BU5D_t2046984534* L_2 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		V_0 = (DepthEntry_t2194697103 )((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		DepthEntryU5BU5D_t2046984534* L_7 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_8 = (int32_t)__this->get_size_1();
		Initobj (DepthEntry_t2194697103_il2cpp_TypeInfo_var, (&V_2));
		DepthEntry_t2194697103  L_9 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (DepthEntry_t2194697103 )L_9);
		DepthEntry_t2194697103  L_10 = V_0;
		return L_10;
	}

IL_004f:
	{
		Initobj (DepthEntry_t2194697103_il2cpp_TypeInfo_var, (&V_3));
		DepthEntry_t2194697103  L_11 = V_3;
		return L_11;
	}
}
// T[] BetterList`1<UICamera/DepthEntry>::ToArray()
extern "C"  DepthEntryU5BU5D_t2046984534* BetterList_1_ToArray_m2114361902_gshared (BetterList_1_t3691665115 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t3691665115 *)__this);
		((  void (*) (BetterList_1_t3691665115 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((BetterList_1_t3691665115 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		DepthEntryU5BU5D_t2046984534* L_0 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UICamera/DepthEntry>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m3147294008_gshared (BetterList_1_t3691665115 * __this, CompareFunc_t41355861 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	DepthEntry_t2194697103  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t41355861 * L_2 = ___comparer0;
		DepthEntryU5BU5D_t2046984534* L_3 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		DepthEntryU5BU5D_t2046984534* L_6 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		NullCheck((CompareFunc_t41355861 *)L_2);
		int32_t L_9 = ((  int32_t (*) (CompareFunc_t41355861 *, DepthEntry_t2194697103 , DepthEntry_t2194697103 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CompareFunc_t41355861 *)L_2, (DepthEntry_t2194697103 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (DepthEntry_t2194697103 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		DepthEntryU5BU5D_t2046984534* L_10 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = (DepthEntry_t2194697103 )((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		DepthEntryU5BU5D_t2046984534* L_13 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_14 = V_3;
		DepthEntryU5BU5D_t2046984534* L_15 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (DepthEntry_t2194697103 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		DepthEntryU5BU5D_t2046984534* L_18 = (DepthEntryU5BU5D_t2046984534*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		DepthEntry_t2194697103  L_20 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)1))), (DepthEntry_t2194697103 )L_20);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_3;
		if (L_22)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_23 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::.ctor()
extern "C"  void BetterList_1__ctor_m2692225884_gshared (BetterList_1_t3085143772 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Color>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m1994410960_gshared (BetterList_1_t3085143772 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator3_t671719463 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t671719463 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t671719463 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t671719463 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator3_t671719463 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t671719463 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t671719463 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UnityEngine.Color>::get_Item(System.Int32)
extern "C"  Color_t1588175760  BetterList_1_get_Item_m872963894_gshared (BetterList_1_t3085143772 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void BetterList`1<UnityEngine.Color>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m3391128673_gshared (BetterList_1_t3085143772 * __this, int32_t ___i0, Color_t1588175760  ___value1, const MethodInfo* method)
{
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Color_t1588175760  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Color_t1588175760 )L_2);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::AllocateMore()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m4029084442_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m4029084442_gshared (BetterList_1_t3085143772 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m4029084442_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ColorU5BU5D_t3477081137* V_0 = NULL;
	ColorU5BU5D_t3477081137* G_B3_0 = NULL;
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_1 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((ColorU5BU5D_t3477081137*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((ColorU5BU5D_t3477081137*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (ColorU5BU5D_t3477081137*)G_B3_0;
		ColorU5BU5D_t3477081137* L_3 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_5 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		ColorU5BU5D_t3477081137* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0);
	}

IL_0052:
	{
		ColorU5BU5D_t3477081137* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Trim()
extern "C"  void BetterList_1_Trim_m3405031946_gshared (BetterList_1_t3085143772 * __this, const MethodInfo* method)
{
	ColorU5BU5D_t3477081137* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ColorU5BU5D_t3477081137* L_2 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (ColorU5BU5D_t3477081137*)((ColorU5BU5D_t3477081137*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		ColorU5BU5D_t3477081137* L_4 = V_0;
		int32_t L_5 = V_1;
		ColorU5BU5D_t3477081137* L_6 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Color_t1588175760 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_12 = V_0;
		__this->set_buffer_0(L_12);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((ColorU5BU5D_t3477081137*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Clear()
extern "C"  void BetterList_1_Clear_m98359175_gshared (BetterList_1_t3085143772 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Release()
extern "C"  void BetterList_1_Release_m1395771521_gshared (BetterList_1_t3085143772 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((ColorU5BU5D_t3477081137*)NULL);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Add(T)
extern "C"  void BetterList_1_Add_m2847934883_gshared (BetterList_1_t3085143772 * __this, Color_t1588175760  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ColorU5BU5D_t3477081137* L_2 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t3085143772 *)__this);
		((  void (*) (BetterList_1_t3085143772 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t3085143772 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		ColorU5BU5D_t3477081137* L_3 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Color_t1588175760  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Color_t1588175760 )L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m3165888010_gshared (BetterList_1_t3085143772 * __this, int32_t ___index0, Color_t1588175760  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		ColorU5BU5D_t3477081137* L_2 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t3085143772 *)__this);
		((  void (*) (BetterList_1_t3085143772 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t3085143772 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		ColorU5BU5D_t3477081137* L_7 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		ColorU5BU5D_t3477081137* L_9 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Color_t1588175760 )((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___index0;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0043;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_15 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_16 = ___index0;
		Color_t1588175760  L_17 = ___item1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (Color_t1588175760 )L_17);
		int32_t L_18 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Color_t1588175760  L_19 = ___item1;
		NullCheck((BetterList_1_t3085143772 *)__this);
		((  void (*) (BetterList_1_t3085143772 *, Color_t1588175760 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((BetterList_1_t3085143772 *)__this, (Color_t1588175760 )L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UnityEngine.Color>::Contains(T)
extern "C"  bool BetterList_1_Contains_m28461483_gshared (BetterList_1_t3085143772 * __this, Color_t1588175760  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		ColorU5BU5D_t3477081137* L_1 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Color_t1588175760  L_3 = ___item0;
		Color_t1588175760  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Color_Equals_m3016668205((Color_t1588175760 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UnityEngine.Color>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m2216612013_gshared (BetterList_1_t3085143772 * __this, Color_t1588175760  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		ColorU5BU5D_t3477081137* L_1 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Color_t1588175760  L_3 = ___item0;
		Color_t1588175760  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Color_Equals_m3016668205((Color_t1588175760 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UnityEngine.Color>::Remove(T)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m4289951846_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m4289951846_gshared (BetterList_1_t3085143772 * __this, Color_t1588175760  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m4289951846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t1022566410 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Color_t1588175760  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Color_t1588175760  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t1022566410 * L_1 = ((  EqualityComparer_1_t1022566410 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t1022566410 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t1022566410 * L_2 = V_0;
		ColorU5BU5D_t3477081137* L_3 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Color_t1588175760  L_6 = ___item0;
		NullCheck((EqualityComparer_1_t1022566410 *)L_2);
		bool L_7 = VirtFuncInvoker2< bool, Color_t1588175760 , Color_t1588175760  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Color>::Equals(!0,!0) */, (EqualityComparer_1_t1022566410 *)L_2, (Color_t1588175760 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Color_t1588175760 )L_6);
		if (!L_7)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_8-(int32_t)1)));
		ColorU5BU5D_t3477081137* L_9 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_10 = V_1;
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_3));
		Color_t1588175760  L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Color_t1588175760 )L_11);
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		ColorU5BU5D_t3477081137* L_13 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_14 = V_2;
		ColorU5BU5D_t3477081137* L_15 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Color_t1588175760 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_21 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_22 = (int32_t)__this->get_size_1();
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_4));
		Color_t1588175760  L_23 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (Color_t1588175760 )L_23);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::RemoveAt(System.Int32)
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m1039740880_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m1039740880_gshared (BetterList_1_t3085143772 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m1039740880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Color_t1588175760  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Color_t1588175760  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		ColorU5BU5D_t3477081137* L_5 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_1));
		Color_t1588175760  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Color_t1588175760 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		ColorU5BU5D_t3477081137* L_9 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		ColorU5BU5D_t3477081137* L_11 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Color_t1588175760 )((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0048;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_17 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_18 = (int32_t)__this->get_size_1();
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_2));
		Color_t1588175760  L_19 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Color_t1588175760 )L_19);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UnityEngine.Color>::Pop()
extern Il2CppClass* Color_t1588175760_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m3105358090_MetadataUsageId;
extern "C"  Color_t1588175760  BetterList_1_Pop_m3105358090_gshared (BetterList_1_t3085143772 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m3105358090_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t1588175760  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Color_t1588175760  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Color_t1588175760  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_2 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		V_0 = (Color_t1588175760 )((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		ColorU5BU5D_t3477081137* L_7 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_8 = (int32_t)__this->get_size_1();
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_2));
		Color_t1588175760  L_9 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Color_t1588175760 )L_9);
		Color_t1588175760  L_10 = V_0;
		return L_10;
	}

IL_004f:
	{
		Initobj (Color_t1588175760_il2cpp_TypeInfo_var, (&V_3));
		Color_t1588175760  L_11 = V_3;
		return L_11;
	}
}
// T[] BetterList`1<UnityEngine.Color>::ToArray()
extern "C"  ColorU5BU5D_t3477081137* BetterList_1_ToArray_m1509532085_gshared (BetterList_1_t3085143772 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t3085143772 *)__this);
		((  void (*) (BetterList_1_t3085143772 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((BetterList_1_t3085143772 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		ColorU5BU5D_t3477081137* L_0 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UnityEngine.Color>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m2221745279_gshared (BetterList_1_t3085143772 * __this, CompareFunc_t3729801814 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Color_t1588175760  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t3729801814 * L_2 = ___comparer0;
		ColorU5BU5D_t3477081137* L_3 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		ColorU5BU5D_t3477081137* L_6 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		NullCheck((CompareFunc_t3729801814 *)L_2);
		int32_t L_9 = ((  int32_t (*) (CompareFunc_t3729801814 *, Color_t1588175760 , Color_t1588175760 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CompareFunc_t3729801814 *)L_2, (Color_t1588175760 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Color_t1588175760 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_10 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = (Color_t1588175760 )((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		ColorU5BU5D_t3477081137* L_13 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_14 = V_3;
		ColorU5BU5D_t3477081137* L_15 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Color_t1588175760 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		ColorU5BU5D_t3477081137* L_18 = (ColorU5BU5D_t3477081137*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		Color_t1588175760  L_20 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)1))), (Color_t1588175760 )L_20);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_3;
		if (L_22)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_23 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::.ctor()
extern "C"  void BetterList_1__ctor_m2212929352_gshared (BetterList_1_t727330504 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Vector2>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m1953221820_gshared (BetterList_1_t727330504 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UnityEngine.Vector2>::get_Item(System.Int32)
extern "C"  Vector2_t3525329788  BetterList_1_get_Item_m884875658_gshared (BetterList_1_t727330504 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m1302648309_gshared (BetterList_1_t727330504 * __this, int32_t ___i0, Vector2_t3525329788  ___value1, const MethodInfo* method)
{
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Vector2_t3525329788  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Vector2_t3525329788 )L_2);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::AllocateMore()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m4068765102_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m4068765102_gshared (BetterList_1_t727330504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m4068765102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2U5BU5D_t2741383957* V_0 = NULL;
	Vector2U5BU5D_t2741383957* G_B3_0 = NULL;
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Vector2U5BU5D_t2741383957* L_1 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((Vector2U5BU5D_t2741383957*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((Vector2U5BU5D_t2741383957*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (Vector2U5BU5D_t2741383957*)G_B3_0;
		Vector2U5BU5D_t2741383957* L_3 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Vector2U5BU5D_t2741383957* L_5 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		Vector2U5BU5D_t2741383957* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0);
	}

IL_0052:
	{
		Vector2U5BU5D_t2741383957* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Trim()
extern "C"  void BetterList_1_Trim_m1727002782_gshared (BetterList_1_t727330504 * __this, const MethodInfo* method)
{
	Vector2U5BU5D_t2741383957* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector2U5BU5D_t2741383957* L_2 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (Vector2U5BU5D_t2741383957*)((Vector2U5BU5D_t2741383957*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		Vector2U5BU5D_t2741383957* L_4 = V_0;
		int32_t L_5 = V_1;
		Vector2U5BU5D_t2741383957* L_6 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Vector2_t3525329788 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		Vector2U5BU5D_t2741383957* L_12 = V_0;
		__this->set_buffer_0(L_12);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((Vector2U5BU5D_t2741383957*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Clear()
extern "C"  void BetterList_1_Clear_m3914029939_gshared (BetterList_1_t727330504 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Release()
extern "C"  void BetterList_1_Release_m353304941_gshared (BetterList_1_t727330504 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((Vector2U5BU5D_t2741383957*)NULL);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Add(T)
extern "C"  void BetterList_1_Add_m1169905719_gshared (BetterList_1_t727330504 * __this, Vector2_t3525329788  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector2U5BU5D_t2741383957* L_2 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t727330504 *)__this);
		((  void (*) (BetterList_1_t727330504 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t727330504 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		Vector2U5BU5D_t2741383957* L_3 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Vector2_t3525329788  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector2_t3525329788 )L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m1720140958_gshared (BetterList_1_t727330504 * __this, int32_t ___index0, Vector2_t3525329788  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector2U5BU5D_t2741383957* L_2 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t727330504 *)__this);
		((  void (*) (BetterList_1_t727330504 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t727330504 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		Vector2U5BU5D_t2741383957* L_7 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		Vector2U5BU5D_t2741383957* L_9 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Vector2_t3525329788 )((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___index0;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0043;
		}
	}
	{
		Vector2U5BU5D_t2741383957* L_15 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_16 = ___index0;
		Vector2_t3525329788  L_17 = ___item1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (Vector2_t3525329788 )L_17);
		int32_t L_18 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Vector2_t3525329788  L_19 = ___item1;
		NullCheck((BetterList_1_t727330504 *)__this);
		((  void (*) (BetterList_1_t727330504 *, Vector2_t3525329788 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((BetterList_1_t727330504 *)__this, (Vector2_t3525329788 )L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector2>::Contains(T)
extern "C"  bool BetterList_1_Contains_m4101741207_gshared (BetterList_1_t727330504 * __this, Vector2_t3525329788  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector2U5BU5D_t2741383957* L_1 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector2_t3525329788  L_3 = ___item0;
		Vector2_t3525329788  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Vector2_Equals_m3404198849((Vector2_t3525329788 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UnityEngine.Vector2>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m1628778689_gshared (BetterList_1_t727330504 * __this, Vector2_t3525329788  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector2U5BU5D_t2741383957* L_1 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector2_t3525329788  L_3 = ___item0;
		Vector2_t3525329788  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Vector2_Equals_m3404198849((Vector2_t3525329788 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector2>::Remove(T)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m271848530_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m271848530_gshared (BetterList_1_t727330504 * __this, Vector2_t3525329788  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m271848530_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t2959720438 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector2_t3525329788  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector2_t3525329788  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t2959720438 * L_1 = ((  EqualityComparer_1_t2959720438 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t2959720438 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t2959720438 * L_2 = V_0;
		Vector2U5BU5D_t2741383957* L_3 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector2_t3525329788  L_6 = ___item0;
		NullCheck((EqualityComparer_1_t2959720438 *)L_2);
		bool L_7 = VirtFuncInvoker2< bool, Vector2_t3525329788 , Vector2_t3525329788  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector2>::Equals(!0,!0) */, (EqualityComparer_1_t2959720438 *)L_2, (Vector2_t3525329788 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Vector2_t3525329788 )L_6);
		if (!L_7)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_8-(int32_t)1)));
		Vector2U5BU5D_t2741383957* L_9 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_10 = V_1;
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_3));
		Vector2_t3525329788  L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector2_t3525329788 )L_11);
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		Vector2U5BU5D_t2741383957* L_13 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_14 = V_2;
		Vector2U5BU5D_t2741383957* L_15 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Vector2_t3525329788 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		Vector2U5BU5D_t2741383957* L_21 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_22 = (int32_t)__this->get_size_1();
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_4));
		Vector2_t3525329788  L_23 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (Vector2_t3525329788 )L_23);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::RemoveAt(System.Int32)
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m3888961124_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m3888961124_gshared (BetterList_1_t727330504 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m3888961124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector2_t3525329788  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector2_t3525329788  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		Vector2U5BU5D_t2741383957* L_5 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_1));
		Vector2_t3525329788  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector2_t3525329788 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		Vector2U5BU5D_t2741383957* L_9 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		Vector2U5BU5D_t2741383957* L_11 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector2_t3525329788 )((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0048;
		}
	}
	{
		Vector2U5BU5D_t2741383957* L_17 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_18 = (int32_t)__this->get_size_1();
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_2));
		Vector2_t3525329788  L_19 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Vector2_t3525329788 )L_19);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UnityEngine.Vector2>::Pop()
extern Il2CppClass* Vector2_t3525329788_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m1700428854_MetadataUsageId;
extern "C"  Vector2_t3525329788  BetterList_1_Pop_m1700428854_gshared (BetterList_1_t727330504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m1700428854_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector2_t3525329788  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t3525329788  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Vector2U5BU5D_t2741383957* L_2 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		V_0 = (Vector2_t3525329788 )((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		Vector2U5BU5D_t2741383957* L_7 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_8 = (int32_t)__this->get_size_1();
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_2));
		Vector2_t3525329788  L_9 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Vector2_t3525329788 )L_9);
		Vector2_t3525329788  L_10 = V_0;
		return L_10;
	}

IL_004f:
	{
		Initobj (Vector2_t3525329788_il2cpp_TypeInfo_var, (&V_3));
		Vector2_t3525329788  L_11 = V_3;
		return L_11;
	}
}
// T[] BetterList`1<UnityEngine.Vector2>::ToArray()
extern "C"  Vector2U5BU5D_t2741383957* BetterList_1_ToArray_m2076161889_gshared (BetterList_1_t727330504 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t727330504 *)__this);
		((  void (*) (BetterList_1_t727330504 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((BetterList_1_t727330504 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Vector2U5BU5D_t2741383957* L_0 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector2>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m749295979_gshared (BetterList_1_t727330504 * __this, CompareFunc_t1371988546 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Vector2_t3525329788  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t1371988546 * L_2 = ___comparer0;
		Vector2U5BU5D_t2741383957* L_3 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector2U5BU5D_t2741383957* L_6 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		NullCheck((CompareFunc_t1371988546 *)L_2);
		int32_t L_9 = ((  int32_t (*) (CompareFunc_t1371988546 *, Vector2_t3525329788 , Vector2_t3525329788 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CompareFunc_t1371988546 *)L_2, (Vector2_t3525329788 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Vector2_t3525329788 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		Vector2U5BU5D_t2741383957* L_10 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = (Vector2_t3525329788 )((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		Vector2U5BU5D_t2741383957* L_13 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_14 = V_3;
		Vector2U5BU5D_t2741383957* L_15 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Vector2_t3525329788 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		Vector2U5BU5D_t2741383957* L_18 = (Vector2U5BU5D_t2741383957*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		Vector2_t3525329788  L_20 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)1))), (Vector2_t3525329788 )L_20);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_3;
		if (L_22)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_23 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::.ctor()
extern "C"  void BetterList_1__ctor_m415977993_gshared (BetterList_1_t727330505 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Vector3>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m1108749949_gshared (BetterList_1_t727330505 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C"  Vector3_t3525329789  BetterList_1_get_Item_m915895465_gshared (BetterList_1_t727330505 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m1047911764_gshared (BetterList_1_t727330505 * __this, int32_t ___i0, Vector3_t3525329789  ___value1, const MethodInfo* method)
{
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Vector3_t3525329789  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Vector3_t3525329789 )L_2);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::AllocateMore()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m3071692749_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m3071692749_gshared (BetterList_1_t727330505 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m3071692749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3U5BU5D_t3227571696* V_0 = NULL;
	Vector3U5BU5D_t3227571696* G_B3_0 = NULL;
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_1 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((Vector3U5BU5D_t3227571696*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((Vector3U5BU5D_t3227571696*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (Vector3U5BU5D_t3227571696*)G_B3_0;
		Vector3U5BU5D_t3227571696* L_3 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_5 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		Vector3U5BU5D_t3227571696* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0);
	}

IL_0052:
	{
		Vector3U5BU5D_t3227571696* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Trim()
extern "C"  void BetterList_1_Trim_m1530489277_gshared (BetterList_1_t727330505 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t3227571696* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector3U5BU5D_t3227571696* L_2 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (Vector3U5BU5D_t3227571696*)((Vector3U5BU5D_t3227571696*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		Vector3U5BU5D_t3227571696* L_4 = V_0;
		int32_t L_5 = V_1;
		Vector3U5BU5D_t3227571696* L_6 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Vector3_t3525329789 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_12 = V_0;
		__this->set_buffer_0(L_12);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((Vector3U5BU5D_t3227571696*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Clear()
extern "C"  void BetterList_1_Clear_m2117078580_gshared (BetterList_1_t727330505 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Release()
extern "C"  void BetterList_1_Release_m59901934_gshared (BetterList_1_t727330505 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((Vector3U5BU5D_t3227571696*)NULL);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Add(T)
extern "C"  void BetterList_1_Add_m973392214_gshared (BetterList_1_t727330505 * __this, Vector3_t3525329789  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector3U5BU5D_t3227571696* L_2 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t727330505 *)__this);
		((  void (*) (BetterList_1_t727330505 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t727330505 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		Vector3U5BU5D_t3227571696* L_3 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Vector3_t3525329789  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector3_t3525329789 )L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m1751160765_gshared (BetterList_1_t727330505 * __this, int32_t ___index0, Vector3_t3525329789  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector3U5BU5D_t3227571696* L_2 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t727330505 *)__this);
		((  void (*) (BetterList_1_t727330505 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t727330505 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		Vector3U5BU5D_t3227571696* L_7 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		Vector3U5BU5D_t3227571696* L_9 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Vector3_t3525329789 )((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___index0;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0043;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_15 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_16 = ___index0;
		Vector3_t3525329789  L_17 = ___item1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (Vector3_t3525329789 )L_17);
		int32_t L_18 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Vector3_t3525329789  L_19 = ___item1;
		NullCheck((BetterList_1_t727330505 *)__this);
		((  void (*) (BetterList_1_t727330505 *, Vector3_t3525329789 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((BetterList_1_t727330505 *)__this, (Vector3_t3525329789 )L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector3>::Contains(T)
extern "C"  bool BetterList_1_Contains_m1314325720_gshared (BetterList_1_t727330505 * __this, Vector3_t3525329789  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector3U5BU5D_t3227571696* L_1 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector3_t3525329789  L_3 = ___item0;
		Vector3_t3525329789  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Vector3_Equals_m3337192096((Vector3_t3525329789 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UnityEngine.Vector3>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m1123220064_gshared (BetterList_1_t727330505 * __this, Vector3_t3525329789  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector3U5BU5D_t3227571696* L_1 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector3_t3525329789  L_3 = ___item0;
		Vector3_t3525329789  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Vector3_Equals_m3337192096((Vector3_t3525329789 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector3>::Remove(T)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m4273412819_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m4273412819_gshared (BetterList_1_t727330505 * __this, Vector3_t3525329789  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m4273412819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t2959720439 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t2959720439 * L_1 = ((  EqualityComparer_1_t2959720439 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t2959720439 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t2959720439 * L_2 = V_0;
		Vector3U5BU5D_t3227571696* L_3 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector3_t3525329789  L_6 = ___item0;
		NullCheck((EqualityComparer_1_t2959720439 *)L_2);
		bool L_7 = VirtFuncInvoker2< bool, Vector3_t3525329789 , Vector3_t3525329789  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector3>::Equals(!0,!0) */, (EqualityComparer_1_t2959720439 *)L_2, (Vector3_t3525329789 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Vector3_t3525329789 )L_6);
		if (!L_7)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_8-(int32_t)1)));
		Vector3U5BU5D_t3227571696* L_9 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_10 = V_1;
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_3));
		Vector3_t3525329789  L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector3_t3525329789 )L_11);
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		Vector3U5BU5D_t3227571696* L_13 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_14 = V_2;
		Vector3U5BU5D_t3227571696* L_15 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Vector3_t3525329789 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_21 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_22 = (int32_t)__this->get_size_1();
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_4));
		Vector3_t3525329789  L_23 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (Vector3_t3525329789 )L_23);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m3919980931_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m3919980931_gshared (BetterList_1_t727330505 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m3919980931_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		Vector3U5BU5D_t3227571696* L_5 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_1));
		Vector3_t3525329789  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector3_t3525329789 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		Vector3U5BU5D_t3227571696* L_9 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		Vector3U5BU5D_t3227571696* L_11 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector3_t3525329789 )((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0048;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_17 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_18 = (int32_t)__this->get_size_1();
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_2));
		Vector3_t3525329789  L_19 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Vector3_t3525329789 )L_19);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UnityEngine.Vector3>::Pop()
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m4187941687_MetadataUsageId;
extern "C"  Vector3_t3525329789  BetterList_1_Pop_m4187941687_gshared (BetterList_1_t727330505 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m4187941687_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_2 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		V_0 = (Vector3_t3525329789 )((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		Vector3U5BU5D_t3227571696* L_7 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_8 = (int32_t)__this->get_size_1();
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_2));
		Vector3_t3525329789  L_9 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Vector3_t3525329789 )L_9);
		Vector3_t3525329789  L_10 = V_0;
		return L_10;
	}

IL_004f:
	{
		Initobj (Vector3_t3525329789_il2cpp_TypeInfo_var, (&V_3));
		Vector3_t3525329789  L_11 = V_3;
		return L_11;
	}
}
// T[] BetterList`1<UnityEngine.Vector3>::ToArray()
extern "C"  Vector3U5BU5D_t3227571696* BetterList_1_ToArray_m1782758882_gshared (BetterList_1_t727330505 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t727330505 *)__this);
		((  void (*) (BetterList_1_t727330505 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((BetterList_1_t727330505 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Vector3U5BU5D_t3227571696* L_0 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector3>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1079061740_gshared (BetterList_1_t727330505 * __this, CompareFunc_t1371988547 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t1371988547 * L_2 = ___comparer0;
		Vector3U5BU5D_t3227571696* L_3 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector3U5BU5D_t3227571696* L_6 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		NullCheck((CompareFunc_t1371988547 *)L_2);
		int32_t L_9 = ((  int32_t (*) (CompareFunc_t1371988547 *, Vector3_t3525329789 , Vector3_t3525329789 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CompareFunc_t1371988547 *)L_2, (Vector3_t3525329789 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Vector3_t3525329789 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_10 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = (Vector3_t3525329789 )((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		Vector3U5BU5D_t3227571696* L_13 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_14 = V_3;
		Vector3U5BU5D_t3227571696* L_15 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Vector3_t3525329789 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		Vector3U5BU5D_t3227571696* L_18 = (Vector3U5BU5D_t3227571696*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		Vector3_t3525329789  L_20 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)1))), (Vector3_t3525329789 )L_20);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_3;
		if (L_22)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_23 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::.ctor()
extern "C"  void BetterList_1__ctor_m2913993930_gshared (BetterList_1_t727330506 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> BetterList`1<UnityEngine.Vector4>::GetEnumerator()
extern "C"  Il2CppObject* BetterList_1_GetEnumerator_m264278078_gshared (BetterList_1_t727330506 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * L_0 = (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * L_2 = V_0;
		return L_2;
	}
}
// T BetterList`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C"  Vector4_t3525329790  BetterList_1_get_Item_m946915272_gshared (BetterList_1_t727330506 * __this, int32_t ___i0, const MethodInfo* method)
{
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		return ((L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2)));
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C"  void BetterList_1_set_Item_m793175219_gshared (BetterList_1_t727330506 * __this, int32_t ___i0, Vector4_t3525329790  ___value1, const MethodInfo* method)
{
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_1 = ___i0;
		Vector4_t3525329790  L_2 = ___value1;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (Vector4_t3525329790 )L_2);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::AllocateMore()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_AllocateMore_m2074620396_MetadataUsageId;
extern "C"  void BetterList_1_AllocateMore_m2074620396_gshared (BetterList_1_t727330506 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_AllocateMore_m2074620396_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4U5BU5D_t3713759435* V_0 = NULL;
	Vector4U5BU5D_t3713759435* G_B3_0 = NULL;
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Vector4U5BU5D_t3713759435* L_1 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		NullCheck(L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_2 = Mathf_Max_m2911193737(NULL /*static, unused*/, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))<<(int32_t)1)), (int32_t)((int32_t)32), /*hidden argument*/NULL);
		G_B3_0 = ((Vector4U5BU5D_t3713759435*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_2));
		goto IL_002d;
	}

IL_0026:
	{
		G_B3_0 = ((Vector4U5BU5D_t3713759435*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)((int32_t)32)));
	}

IL_002d:
	{
		V_0 = (Vector4U5BU5D_t3713759435*)G_B3_0;
		Vector4U5BU5D_t3713759435* L_3 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		if (!L_3)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		Vector4U5BU5D_t3713759435* L_5 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		Vector4U5BU5D_t3713759435* L_6 = V_0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_5);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_5, (Il2CppArray *)(Il2CppArray *)L_6, (int32_t)0);
	}

IL_0052:
	{
		Vector4U5BU5D_t3713759435* L_7 = V_0;
		__this->set_buffer_0(L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Trim()
extern "C"  void BetterList_1_Trim_m1333975772_gshared (BetterList_1_t727330506 * __this, const MethodInfo* method)
{
	Vector4U5BU5D_t3713759435* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector4U5BU5D_t3713759435* L_2 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((((int32_t)L_1) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_3 = (int32_t)__this->get_size_1();
		V_0 = (Vector4U5BU5D_t3713759435*)((Vector4U5BU5D_t3713759435*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), (uint32_t)L_3));
		V_1 = (int32_t)0;
		goto IL_0049;
	}

IL_0032:
	{
		Vector4U5BU5D_t3713759435* L_4 = V_0;
		int32_t L_5 = V_1;
		Vector4U5BU5D_t3713759435* L_6 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (Vector4_t3525329790 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))));
		int32_t L_9 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0032;
		}
	}
	{
		Vector4U5BU5D_t3713759435* L_12 = V_0;
		__this->set_buffer_0(L_12);
	}

IL_005c:
	{
		goto IL_0068;
	}

IL_0061:
	{
		__this->set_buffer_0((Vector4U5BU5D_t3713759435*)NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Clear()
extern "C"  void BetterList_1_Clear_m320127221_gshared (BetterList_1_t727330506 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Release()
extern "C"  void BetterList_1_Release_m4061466223_gshared (BetterList_1_t727330506 * __this, const MethodInfo* method)
{
	{
		__this->set_size_1(0);
		__this->set_buffer_0((Vector4U5BU5D_t3713759435*)NULL);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Add(T)
extern "C"  void BetterList_1_Add_m776878709_gshared (BetterList_1_t727330506 * __this, Vector4_t3525329790  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector4U5BU5D_t3713759435* L_2 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t727330506 *)__this);
		((  void (*) (BetterList_1_t727330506 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t727330506 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		Vector4U5BU5D_t3713759435* L_3 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_4 = (int32_t)__this->get_size_1();
		int32_t L_5 = (int32_t)L_4;
		V_0 = (int32_t)L_5;
		__this->set_size_1(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_0;
		Vector4_t3525329790  L_7 = ___item0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector4_t3525329790 )L_7);
		return;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C"  void BetterList_1_Insert_m1782180572_gshared (BetterList_1_t727330506 * __this, int32_t ___index0, Vector4_t3525329790  ___item1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		Vector4U5BU5D_t3713759435* L_2 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		NullCheck(L_2);
		if ((!(((uint32_t)L_1) == ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))))
		{
			goto IL_0024;
		}
	}

IL_001e:
	{
		NullCheck((BetterList_1_t727330506 *)__this);
		((  void (*) (BetterList_1_t727330506 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)((BetterList_1_t727330506 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
	}

IL_0024:
	{
		int32_t L_3 = ___index0;
		if ((((int32_t)L_3) <= ((int32_t)(-1))))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_4 = ___index0;
		int32_t L_5 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_4) >= ((int32_t)L_5)))
		{
			goto IL_0088;
		}
	}
	{
		int32_t L_6 = (int32_t)__this->get_size_1();
		V_0 = (int32_t)L_6;
		goto IL_0061;
	}

IL_0043:
	{
		Vector4U5BU5D_t3713759435* L_7 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_8 = V_0;
		Vector4U5BU5D_t3713759435* L_9 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Vector4_t3525329790 )((L_9)->GetAt(static_cast<il2cpp_array_size_t>(L_11))));
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_12-(int32_t)1));
	}

IL_0061:
	{
		int32_t L_13 = V_0;
		int32_t L_14 = ___index0;
		if ((((int32_t)L_13) > ((int32_t)L_14)))
		{
			goto IL_0043;
		}
	}
	{
		Vector4U5BU5D_t3713759435* L_15 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_16 = ___index0;
		Vector4_t3525329790  L_17 = ___item1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (Vector4_t3525329790 )L_17);
		int32_t L_18 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_18+(int32_t)1)));
		goto IL_008f;
	}

IL_0088:
	{
		Vector4_t3525329790  L_19 = ___item1;
		NullCheck((BetterList_1_t727330506 *)__this);
		((  void (*) (BetterList_1_t727330506 *, Vector4_t3525329790 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->method)((BetterList_1_t727330506 *)__this, (Vector4_t3525329790 )L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
	}

IL_008f:
	{
		return;
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector4>::Contains(T)
extern "C"  bool BetterList_1_Contains_m2821877529_gshared (BetterList_1_t727330506 * __this, Vector4_t3525329790  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector4U5BU5D_t3713759435* L_1 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector4_t3525329790  L_3 = ___item0;
		Vector4_t3525329790  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Vector4_Equals_m3270185343((Vector4_t3525329790 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		return (bool)1;
	}

IL_0038:
	{
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_8 = V_0;
		int32_t L_9 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}
}
// System.Int32 BetterList`1<UnityEngine.Vector4>::IndexOf(T)
extern "C"  int32_t BetterList_1_IndexOf_m617661439_gshared (BetterList_1_t727330506 * __this, Vector4_t3525329790  ___item0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return (-1);
	}

IL_000d:
	{
		V_0 = (int32_t)0;
		goto IL_003c;
	}

IL_0014:
	{
		Vector4U5BU5D_t3713759435* L_1 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		Vector4_t3525329790  L_3 = ___item0;
		Vector4_t3525329790  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), &L_4);
		bool L_6 = Vector4_Equals_m3270185343((Vector4_t3525329790 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_2))), (Il2CppObject *)L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0038;
		}
	}
	{
		int32_t L_7 = V_0;
		return L_7;
	}

IL_0038:
	{
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_9 = V_0;
		int32_t L_10 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_0014;
		}
	}
	{
		return (-1);
	}
}
// System.Boolean BetterList`1<UnityEngine.Vector4>::Remove(T)
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Remove_m3980009812_MetadataUsageId;
extern "C"  bool BetterList_1_Remove_m3980009812_gshared (BetterList_1_t727330506 * __this, Vector4_t3525329790  ___item0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Remove_m3980009812_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	EqualityComparer_1_t2959720440 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector4_t3525329790  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector4_t3525329790  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_00b1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 7));
		EqualityComparer_1_t2959720440 * L_1 = ((  EqualityComparer_1_t2959720440 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 6));
		V_0 = (EqualityComparer_1_t2959720440 *)L_1;
		V_1 = (int32_t)0;
		goto IL_00a5;
	}

IL_0018:
	{
		EqualityComparer_1_t2959720440 * L_2 = V_0;
		Vector4U5BU5D_t3713759435* L_3 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector4_t3525329790  L_6 = ___item0;
		NullCheck((EqualityComparer_1_t2959720440 *)L_2);
		bool L_7 = VirtFuncInvoker2< bool, Vector4_t3525329790 , Vector4_t3525329790  >::Invoke(9 /* System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.Vector4>::Equals(!0,!0) */, (EqualityComparer_1_t2959720440 *)L_2, (Vector4_t3525329790 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Vector4_t3525329790 )L_6);
		if (!L_7)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_8 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_8-(int32_t)1)));
		Vector4U5BU5D_t3713759435* L_9 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_10 = V_1;
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_3));
		Vector4_t3525329790  L_11 = V_3;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector4_t3525329790 )L_11);
		int32_t L_12 = V_1;
		V_2 = (int32_t)L_12;
		goto IL_0078;
	}

IL_005a:
	{
		Vector4U5BU5D_t3713759435* L_13 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_14 = V_2;
		Vector4U5BU5D_t3713759435* L_15 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Vector4_t3525329790 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		int32_t L_18 = V_2;
		V_2 = (int32_t)((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0078:
	{
		int32_t L_19 = V_2;
		int32_t L_20 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_005a;
		}
	}
	{
		Vector4U5BU5D_t3713759435* L_21 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_22 = (int32_t)__this->get_size_1();
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_4));
		Vector4_t3525329790  L_23 = V_4;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_22), (Vector4_t3525329790 )L_23);
		return (bool)1;
	}

IL_00a1:
	{
		int32_t L_24 = V_1;
		V_1 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00a5:
	{
		int32_t L_25 = V_1;
		int32_t L_26 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_0018;
		}
	}

IL_00b1:
	{
		return (bool)0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_RemoveAt_m3951000738_MetadataUsageId;
extern "C"  void BetterList_1_RemoveAt_m3951000738_gshared (BetterList_1_t727330506 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_RemoveAt_m3951000738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector4_t3525329790  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector4_t3525329790  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) <= ((int32_t)(-1))))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_2 = ___index0;
		int32_t L_3 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008c;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_size_1();
		__this->set_size_1(((int32_t)((int32_t)L_4-(int32_t)1)));
		Vector4U5BU5D_t3713759435* L_5 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_6 = ___index0;
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_1));
		Vector4_t3525329790  L_7 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Vector4_t3525329790 )L_7);
		int32_t L_8 = ___index0;
		V_0 = (int32_t)L_8;
		goto IL_0066;
	}

IL_0048:
	{
		Vector4U5BU5D_t3713759435* L_9 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_10 = V_0;
		Vector4U5BU5D_t3713759435* L_11 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Vector4_t3525329790 )((L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13))));
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_15 = V_0;
		int32_t L_16 = (int32_t)__this->get_size_1();
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0048;
		}
	}
	{
		Vector4U5BU5D_t3713759435* L_17 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_18 = (int32_t)__this->get_size_1();
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_2));
		Vector4_t3525329790  L_19 = V_2;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Vector4_t3525329790 )L_19);
	}

IL_008c:
	{
		return;
	}
}
// T BetterList`1<UnityEngine.Vector4>::Pop()
extern Il2CppClass* Vector4_t3525329790_il2cpp_TypeInfo_var;
extern const uint32_t BetterList_1_Pop_m2380487224_MetadataUsageId;
extern "C"  Vector4_t3525329790  BetterList_1_Pop_m2380487224_gshared (BetterList_1_t727330506 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (BetterList_1_Pop_m2380487224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector4_t3525329790  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector4_t3525329790  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t3525329790  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		if (!L_0)
		{
			goto IL_004f;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_size_1();
		if (!L_1)
		{
			goto IL_004f;
		}
	}
	{
		Vector4U5BU5D_t3713759435* L_2 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_3 = (int32_t)__this->get_size_1();
		int32_t L_4 = (int32_t)((int32_t)((int32_t)L_3-(int32_t)1));
		V_1 = (int32_t)L_4;
		__this->set_size_1(L_4);
		int32_t L_5 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		int32_t L_6 = L_5;
		V_0 = (Vector4_t3525329790 )((L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_6)));
		Vector4U5BU5D_t3713759435* L_7 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_8 = (int32_t)__this->get_size_1();
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_2));
		Vector4_t3525329790  L_9 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (Vector4_t3525329790 )L_9);
		Vector4_t3525329790  L_10 = V_0;
		return L_10;
	}

IL_004f:
	{
		Initobj (Vector4_t3525329790_il2cpp_TypeInfo_var, (&V_3));
		Vector4_t3525329790  L_11 = V_3;
		return L_11;
	}
}
// T[] BetterList`1<UnityEngine.Vector4>::ToArray()
extern "C"  Vector4U5BU5D_t3713759435* BetterList_1_ToArray_m1489355875_gshared (BetterList_1_t727330506 * __this, const MethodInfo* method)
{
	{
		NullCheck((BetterList_1_t727330506 *)__this);
		((  void (*) (BetterList_1_t727330506 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9)->method)((BetterList_1_t727330506 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 9));
		Vector4U5BU5D_t3713759435* L_0 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		return L_0;
	}
}
// System.Void BetterList`1<UnityEngine.Vector4>::Sort(BetterList`1/CompareFunc<T>)
extern "C"  void BetterList_1_Sort_m1408827501_gshared (BetterList_1_t727330506 * __this, CompareFunc_t1371988548 * ___comparer0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	int32_t V_3 = 0;
	Vector4_t3525329790  V_4;
	memset(&V_4, 0, sizeof(V_4));
	int32_t G_B8_0 = 0;
	{
		V_0 = (int32_t)0;
		int32_t L_0 = (int32_t)__this->get_size_1();
		V_1 = (int32_t)((int32_t)((int32_t)L_0-(int32_t)1));
		V_2 = (bool)1;
		goto IL_00a1;
	}

IL_0012:
	{
		V_2 = (bool)0;
		int32_t L_1 = V_0;
		V_3 = (int32_t)L_1;
		goto IL_009a;
	}

IL_001b:
	{
		CompareFunc_t1371988548 * L_2 = ___comparer0;
		Vector4U5BU5D_t3713759435* L_3 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		Vector4U5BU5D_t3713759435* L_6 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, ((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = ((int32_t)((int32_t)L_7+(int32_t)1));
		NullCheck((CompareFunc_t1371988548 *)L_2);
		int32_t L_9 = ((  int32_t (*) (CompareFunc_t1371988548 *, Vector4_t3525329790 , Vector4_t3525329790 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10)->method)((CompareFunc_t1371988548 *)L_2, (Vector4_t3525329790 )((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))), (Vector4_t3525329790 )((L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 10));
		if ((((int32_t)L_9) <= ((int32_t)0)))
		{
			goto IL_0080;
		}
	}
	{
		Vector4U5BU5D_t3713759435* L_10 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_11 = V_3;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		V_4 = (Vector4_t3525329790 )((L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12)));
		Vector4U5BU5D_t3713759435* L_13 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_14 = V_3;
		Vector4U5BU5D_t3713759435* L_15 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, ((int32_t)((int32_t)L_16+(int32_t)1)));
		int32_t L_17 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (Vector4_t3525329790 )((L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17))));
		Vector4U5BU5D_t3713759435* L_18 = (Vector4U5BU5D_t3713759435*)__this->get_buffer_0();
		int32_t L_19 = V_3;
		Vector4_t3525329790  L_20 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)1)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)1))), (Vector4_t3525329790 )L_20);
		V_2 = (bool)1;
		goto IL_0096;
	}

IL_0080:
	{
		bool L_21 = V_2;
		if (L_21)
		{
			goto IL_0096;
		}
	}
	{
		int32_t L_22 = V_3;
		if (L_22)
		{
			goto IL_0092;
		}
	}
	{
		G_B8_0 = 0;
		goto IL_0095;
	}

IL_0092:
	{
		int32_t L_23 = V_3;
		G_B8_0 = ((int32_t)((int32_t)L_23-(int32_t)1));
	}

IL_0095:
	{
		V_0 = (int32_t)G_B8_0;
	}

IL_0096:
	{
		int32_t L_24 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_009a:
	{
		int32_t L_25 = V_3;
		int32_t L_26 = V_1;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00a1:
	{
		bool L_27 = V_2;
		if (L_27)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void NGUITools/OnInitFunc`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInitFunc_1__ctor_m2232820302_gshared (OnInitFunc_1_t3269005821 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void NGUITools/OnInitFunc`1<System.Object>::Invoke(T)
extern "C"  void OnInitFunc_1_Invoke_m4077680182_gshared (OnInitFunc_1_t3269005821 * __this, Il2CppObject * ___w0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnInitFunc_1_Invoke_m4077680182((OnInitFunc_1_t3269005821 *)__this->get_prev_9(),___w0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___w0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___w0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___w0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___w0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___w0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult NGUITools/OnInitFunc`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnInitFunc_1_BeginInvoke_m2482199179_gshared (OnInitFunc_1_t3269005821 * __this, Il2CppObject * ___w0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___w0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void NGUITools/OnInitFunc`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void OnInitFunc_1_EndInvoke_m4018334558_gshared (OnInitFunc_1_t3269005821 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Boolean>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m88247757_gshared (Action_1_t359458046 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Boolean>::Invoke(T)
extern "C"  void Action_1_Invoke_m3594021162_gshared (Action_1_t359458046 * __this, bool ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m3594021162((Action_1_t359458046 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, bool ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Action_1_BeginInvoke_m647183148_MetadataUsageId;
extern "C"  Il2CppObject * Action_1_BeginInvoke_m647183148_gshared (Action_1_t359458046 * __this, bool ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Action_1_BeginInvoke_m647183148_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Boolean>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m1601629789_gshared (Action_1_t359458046 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m881151526_gshared (Action_1_t985559125 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void System.Action`1<System.Object>::Invoke(T)
extern "C"  void Action_1_Invoke_m663971678_gshared (Action_1_t985559125 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Action_1_Invoke_m663971678((Action_1_t985559125 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Action`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Action_1_BeginInvoke_m917692971_gshared (Action_1_t985559125 * __this, Il2CppObject * ___obj0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Void System.Action`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  void Action_1_EndInvoke_m3562128182_gshared (Action_1_t985559125 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m4262077373_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2204526468_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4054268681_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_2();
		return L_0;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4201941769_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t398457203 * L_2 = (ArrayReadOnlyList_1_t398457203 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		__this->set_U24current_2(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t398457203 * L_8 = (ArrayReadOnlyList_1_t398457203 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_8);
		ObjectU5BU5D_t11523773* L_9 = (ObjectU5BU5D_t11523773*)L_8->get_array_0();
		NullCheck(L_9);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m2650805434_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Object>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m1908510314_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m2799549978_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeNamedArgument_t318735129  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2027929923_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t318735129  L_0 = (CustomAttributeNamedArgument_t318735129 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m1186685398_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t318735129  L_0 = (CustomAttributeNamedArgument_t318735129 )__this->get_U24current_2();
		CustomAttributeNamedArgument_t318735129  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m4084754896_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t4175053208 * L_2 = (ArrayReadOnlyList_1_t4175053208 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		__this->set_U24current_2(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t4175053208 * L_8 = (ArrayReadOnlyList_1_t4175053208 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_8);
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_9 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_8->get_array_0();
		NullCheck(L_9);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m1616284631_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m445982919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0__ctor_m643544331_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  CustomAttributeTypedArgument_t560415562  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m4108562996_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t560415562  L_0 = (CustomAttributeTypedArgument_t560415562 )__this->get_U24current_2();
		return L_0;
	}
}
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m2728697221_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t560415562  L_0 = (CustomAttributeTypedArgument_t560415562 )__this->get_U24current_2();
		CustomAttributeTypedArgument_t560415562  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0), &L_1);
		return L_2;
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m1461469503_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_1();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_1((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0055;
		}
	}
	{
		goto IL_0082;
	}

IL_0021:
	{
		__this->set_U3CiU3E__0_0(0);
		goto IL_0063;
	}

IL_002d:
	{
		ArrayReadOnlyList_1_t121766345 * L_2 = (ArrayReadOnlyList_1_t121766345 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_2);
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_3 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_2->get_array_0();
		int32_t L_4 = (int32_t)__this->get_U3CiU3E__0_0();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		__this->set_U24current_2(((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5))));
		__this->set_U24PC_1(1);
		goto IL_0084;
	}

IL_0055:
	{
		int32_t L_6 = (int32_t)__this->get_U3CiU3E__0_0();
		__this->set_U3CiU3E__0_0(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_0063:
	{
		int32_t L_7 = (int32_t)__this->get_U3CiU3E__0_0();
		ArrayReadOnlyList_1_t121766345 * L_8 = (ArrayReadOnlyList_1_t121766345 *)__this->get_U3CU3Ef__this_3();
		NullCheck(L_8);
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_9 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_8->get_array_0();
		NullCheck(L_9);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length)))))))
		{
			goto IL_002d;
		}
	}
	{
		__this->set_U24PC_1((-1));
	}

IL_0082:
	{
		return (bool)0;
	}

IL_0084:
	{
		return (bool)1;
	}
	// Dead block : IL_0086: ldloc.1
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m4164061832_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_1((-1));
		return;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeTypedArgument>::Reset()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId;
extern "C"  void U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_gshared (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetEnumeratorU3Ec__Iterator0_Reset_m2584944568_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m240689135_gshared (ArrayReadOnlyList_1_t398457203 * __this, ObjectU5BU5D_t11523773* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		ObjectU5BU5D_t11523773* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m2499499206_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t398457203 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(17 /* System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator() */, (ArrayReadOnlyList_1_t398457203 *)__this);
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId;
extern "C"  Il2CppObject * ArrayReadOnlyList_1_get_Item_m535939909_gshared (ArrayReadOnlyList_1_t398457203 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m535939909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		ObjectU5BU5D_t11523773* L_1 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		ObjectU5BU5D_t11523773* L_3 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m2625374704_gshared (ArrayReadOnlyList_1_t398457203 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m3813449101_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m1486920810_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m976758002_gshared (ArrayReadOnlyList_1_t398457203 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m2221418008_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m158553866_gshared (ArrayReadOnlyList_1_t398457203 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1244492898_gshared (ArrayReadOnlyList_1_t398457203 * __this, ObjectU5BU5D_t11523773* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		ObjectU5BU5D_t11523773* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m1770413409_gshared (ArrayReadOnlyList_1_t398457203 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3510862208 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3510862208 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m1098739118_gshared (ArrayReadOnlyList_1_t398457203 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		ObjectU5BU5D_t11523773* L_0 = (ObjectU5BU5D_t11523773*)__this->get_array_0();
		Il2CppObject * L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, ObjectU5BU5D_t11523773*, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (ObjectU5BU5D_t11523773*)L_0, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m738278233_gshared (ArrayReadOnlyList_1_t398457203 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m12996997_gshared (ArrayReadOnlyList_1_t398457203 * __this, Il2CppObject * ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2907098399_gshared (ArrayReadOnlyList_1_t398457203 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId;
extern "C"  Exception_t1967233988 * ArrayReadOnlyList_1_ReadOnlyError_m740547916_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m740547916_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m3527945938_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgumentU5BU5D_t3019176036* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m932360725_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t4175053208 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(17 /* System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator() */, (ArrayReadOnlyList_1_t4175053208 *)__this);
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t318735129  ArrayReadOnlyList_1_get_Item_m3165498630_gshared (ArrayReadOnlyList_1_t4175053208 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m3165498630_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_3 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3031060371_gshared (ArrayReadOnlyList_1_t4175053208 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___value1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2401585746_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m2438902353_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3652230485_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m3556686357_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2530929859_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		CustomAttributeNamedArgument_t318735129  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, CustomAttributeNamedArgument_t318735129 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1650178565_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgumentU5BU5D_t3019176036* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2319686054_gshared (ArrayReadOnlyList_1_t4175053208 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t2992490917 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t2992490917 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m3408499785_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgumentU5BU5D_t3019176036* L_0 = (CustomAttributeNamedArgumentU5BU5D_t3019176036*)__this->get_array_0();
		CustomAttributeNamedArgument_t318735129  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeNamedArgumentU5BU5D_t3019176036*, CustomAttributeNamedArgument_t318735129 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (CustomAttributeNamedArgumentU5BU5D_t3019176036*)L_0, (CustomAttributeNamedArgument_t318735129 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m2906295740_gshared (ArrayReadOnlyList_1_t4175053208 * __this, int32_t ___index0, CustomAttributeNamedArgument_t318735129  ___item1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m609878398_gshared (ArrayReadOnlyList_1_t4175053208 * __this, CustomAttributeNamedArgument_t318735129  ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m780148610_gshared (ArrayReadOnlyList_1_t4175053208 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeNamedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId;
extern "C"  Exception_t1967233988 * ArrayReadOnlyList_1_ReadOnlyError_m467076531_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m467076531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C"  void ArrayReadOnlyList_1__ctor_m904660545_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgumentU5BU5D_t3123668047* ___array0, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = ___array0;
		__this->set_array_0(L_0);
		return;
	}
}
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m1042005508_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	{
		NullCheck((ArrayReadOnlyList_1_t121766345 *)__this);
		Il2CppObject* L_0 = VirtFuncInvoker0< Il2CppObject* >::Invoke(17 /* System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator() */, (ArrayReadOnlyList_1_t121766345 *)__this);
		return L_0;
	}
}
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern const uint32_t ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t560415562  ArrayReadOnlyList_1_get_Item_m957797877_gshared (ArrayReadOnlyList_1_t121766345 * __this, int32_t ___index0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_get_Item_m957797877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___index0;
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		NullCheck(L_1);
		if ((!(((uint32_t)L_0) >= ((uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))))))
		{
			goto IL_0019;
		}
	}
	{
		ArgumentOutOfRangeException_t3479058991 * L_2 = (ArgumentOutOfRangeException_t3479058991 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3479058991_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0019:
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_3 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		int32_t L_4 = ___index0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		return ((L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5)));
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_set_Item_m3144480962_gshared (ArrayReadOnlyList_1_t121766345 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___value1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C"  int32_t ArrayReadOnlyList_1_get_Count_m2684117187_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		NullCheck(L_0);
		return (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C"  bool ArrayReadOnlyList_1_get_IsReadOnly_m3126393472_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C"  void ArrayReadOnlyList_1_Add_m3859776580_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C"  void ArrayReadOnlyList_1_Clear_m1400680710_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C"  bool ArrayReadOnlyList_1_Contains_m2813461300_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		CustomAttributeTypedArgument_t560415562  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, CustomAttributeTypedArgument_t560415562 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return (bool)((((int32_t)((((int32_t)L_2) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C"  void ArrayReadOnlyList_1_CopyTo_m1763599156_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgumentU5BU5D_t3123668047* ___array0, int32_t ___index1, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_1 = ___array0;
		int32_t L_2 = ___index1;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_0);
		VirtActionInvoker2< Il2CppArray *, int32_t >::Invoke(9 /* System.Void System.Array::CopyTo(System.Array,System.Int32) */, (Il2CppArray *)(Il2CppArray *)L_0, (Il2CppArray *)(Il2CppArray *)L_1, (int32_t)L_2);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C"  Il2CppObject* ArrayReadOnlyList_1_GetEnumerator_m2480410519_gshared (ArrayReadOnlyList_1_t121766345 * __this, const MethodInfo* method)
{
	U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * V_0 = NULL;
	{
		U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * L_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		((  void (*) (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5)->method)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5));
		V_0 = (U3CGetEnumeratorU3Ec__Iterator0_t3234171350 *)L_0;
		U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_3(__this);
		U3CGetEnumeratorU3Ec__Iterator0_t3234171350 * L_2 = V_0;
		return L_2;
	}
}
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C"  int32_t ArrayReadOnlyList_1_IndexOf_m785214392_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgumentU5BU5D_t3123668047* L_0 = (CustomAttributeTypedArgumentU5BU5D_t3123668047*)__this->get_array_0();
		CustomAttributeTypedArgument_t560415562  L_1 = ___item0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject * /* static, unused */, CustomAttributeTypedArgumentU5BU5D_t3123668047*, CustomAttributeTypedArgument_t560415562 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3)->method)(NULL /*static, unused*/, (CustomAttributeTypedArgumentU5BU5D_t3123668047*)L_0, (CustomAttributeTypedArgument_t560415562 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3));
		return L_2;
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C"  void ArrayReadOnlyList_1_Insert_m698594987_gshared (ArrayReadOnlyList_1_t121766345 * __this, int32_t ___index0, CustomAttributeTypedArgument_t560415562  ___item1, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C"  bool ArrayReadOnlyList_1_Remove_m3157655599_gshared (ArrayReadOnlyList_1_t121766345 * __this, CustomAttributeTypedArgument_t560415562  ___item0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C"  void ArrayReadOnlyList_1_RemoveAt_m2867415153_gshared (ArrayReadOnlyList_1_t121766345 * __this, int32_t ___index0, const MethodInfo* method)
{
	{
		Exception_t1967233988 * L_0 = ((  Exception_t1967233988 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->method)(NULL /*static, unused*/, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern Il2CppClass* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral823020769;
extern const uint32_t ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId;
extern "C"  Exception_t1967233988 * ArrayReadOnlyList_1_ReadOnlyError_m627800996_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ArrayReadOnlyList_1_ReadOnlyError_m627800996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, (String_t*)_stringLiteral823020769, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<Console/Log>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m947749713_gshared (InternalEnumerator_1_t4202534379 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Console/Log>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3147343535_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Console/Log>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1615871077_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method)
{
	{
		Log_t76580  L_0 = ((  Log_t76580  (*) (InternalEnumerator_1_t4202534379 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t4202534379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Log_t76580  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Console/Log>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3633761896_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Console/Log>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1622233311_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Console/Log>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1121726266_MetadataUsageId;
extern "C"  Log_t76580  InternalEnumerator_1_get_Current_m1121726266_gshared (InternalEnumerator_1_t4202534379 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1121726266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Log_t76580  L_8 = ((  Log_t76580  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2510835690_gshared (InternalEnumerator_1_t383943926 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3501947254_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4292553954_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	{
		TableRange_t476453423  L_0 = ((  TableRange_t476453423  (*) (InternalEnumerator_1_t383943926 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t383943926 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TableRange_t476453423  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2516768321_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1609192930_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Globalization.Unicode.CodePointIndexer/TableRange>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId;
extern "C"  TableRange_t476453423  InternalEnumerator_1_get_Current_m2306301105_gshared (InternalEnumerator_1_t383943926 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2306301105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TableRange_t476453423  L_8 = ((  TableRange_t476453423  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4155127258_gshared (InternalEnumerator_1_t2632522680 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4053167494_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3385911154_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t2632522680 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2632522680 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4263405617_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m569750258_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<Mono.Security.Protocol.Tls.Handshake.ClientCertificateType>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m3784081185_gshared (InternalEnumerator_1_t2632522680 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3784081185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3904876858_gshared (InternalEnumerator_1_t118495844 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2500949542_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1200057490_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((  bool (*) (InternalEnumerator_1_t118495844 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t118495844 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2505350033_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2858864786_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m94889217_MetadataUsageId;
extern "C"  bool InternalEnumerator_1_get_Current_m94889217_gshared (InternalEnumerator_1_t118495844 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m94889217_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		bool L_8 = ((  bool (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2342462508_gshared (InternalEnumerator_1_t2686184324 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2017815028_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2368518122_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((  uint8_t (*) (InternalEnumerator_1_t2686184324 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2686184324 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1621603587_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3669835172_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4103229525_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m4103229525_gshared (InternalEnumerator_1_t2686184324 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4103229525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3601500154_gshared (InternalEnumerator_1_t2686197202 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2105255782_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Char>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3163002844_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = ((  uint16_t (*) (InternalEnumerator_1_t2686197202 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2686197202 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Char>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2748899921_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Char>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4256283158_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Char>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3048220323_MetadataUsageId;
extern "C"  uint16_t InternalEnumerator_1_get_Current_m3048220323_gshared (InternalEnumerator_1_t2686197202 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3048220323_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m263261269_gshared (InternalEnumerator_1_t37517749 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1942345515_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2828440151_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	{
		DictionaryEntry_t130027246  L_0 = ((  DictionaryEntry_t130027246  (*) (InternalEnumerator_1_t37517749 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t37517749 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DictionaryEntry_t130027246  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1282215020_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4030197783_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.DictionaryEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2915343068_MetadataUsageId;
extern "C"  DictionaryEntry_t130027246  InternalEnumerator_1_get_Current_m2915343068_gshared (InternalEnumerator_1_t37517749 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2915343068_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DictionaryEntry_t130027246  L_8 = ((  DictionaryEntry_t130027246  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m664150035_gshared (InternalEnumerator_1_t723939004 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898722477_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4211610019_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t816448501  L_0 = ((  KeyValuePair_2_t816448501  (*) (InternalEnumerator_1_t723939004 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t723939004 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t816448501  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m233182634_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m44493661_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m818645564_MetadataUsageId;
extern "C"  KeyValuePair_2_t816448501  InternalEnumerator_1_get_Current_m818645564_gshared (InternalEnumerator_1_t723939004 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m818645564_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t816448501  L_8 = ((  KeyValuePair_2_t816448501  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2662086813_gshared (InternalEnumerator_1_t2594345872 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1262974435_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m946892569_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t2686855369  L_0 = ((  KeyValuePair_2_t2686855369  (*) (InternalEnumerator_1_t2594345872 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2594345872 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t2686855369  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1742566068_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3398874899_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1242840582_MetadataUsageId;
extern "C"  KeyValuePair_2_t2686855369  InternalEnumerator_1_get_Current_m1242840582_gshared (InternalEnumerator_1_t2594345872 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1242840582_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t2686855369  L_8 = ((  KeyValuePair_2_t2686855369  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2624907895_gshared (InternalEnumerator_1_t935788022 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3448222153_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1348796351_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1028297519  L_0 = ((  KeyValuePair_2_t1028297519  (*) (InternalEnumerator_1_t935788022 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t935788022 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1028297519  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1793576206_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1172054137_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1033564064_MetadataUsageId;
extern "C"  KeyValuePair_2_t1028297519  InternalEnumerator_1_get_Current_m1033564064_gshared (InternalEnumerator_1_t935788022 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1033564064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1028297519  L_8 = ((  KeyValuePair_2_t1028297519  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2957909742_gshared (InternalEnumerator_1_t3220446951 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m483697650_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1713001566_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t3312956448  L_0 = ((  KeyValuePair_2_t3312956448  (*) (InternalEnumerator_1_t3220446951 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3220446951 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t3312956448  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m72014405_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m535991902_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4216610997_MetadataUsageId;
extern "C"  KeyValuePair_2_t3312956448  InternalEnumerator_1_get_Current_m4216610997_gshared (InternalEnumerator_1_t3220446951 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4216610997_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t3312956448  L_8 = ((  KeyValuePair_2_t3312956448  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2318391222_gshared (InternalEnumerator_1_t1517861163 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1440179754_gshared (InternalEnumerator_1_t1517861163 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m445212950_gshared (InternalEnumerator_1_t1517861163 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t1610370660  L_0 = ((  KeyValuePair_2_t1610370660  (*) (InternalEnumerator_1_t1517861163 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1517861163 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t1610370660  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m579824909_gshared (InternalEnumerator_1_t1517861163 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1060688278_gshared (InternalEnumerator_1_t1517861163 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,UnityEngine.TextEditor/TextEditOp>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2754542077_MetadataUsageId;
extern "C"  KeyValuePair_2_t1610370660  InternalEnumerator_1_get_Current_m2754542077_gshared (InternalEnumerator_1_t1517861163 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2754542077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t1610370660  L_8 = ((  KeyValuePair_2_t1610370660  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2868838327_gshared (InternalEnumerator_1_t277738608 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3184071817_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3517943285_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method)
{
	{
		KeyValuePair_2_t370248105  L_0 = ((  KeyValuePair_2_t370248105  (*) (InternalEnumerator_1_t277738608 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t277738608 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		KeyValuePair_2_t370248105  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m231699022_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2775239157_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3903580542_MetadataUsageId;
extern "C"  KeyValuePair_2_t370248105  InternalEnumerator_1_get_Current_m3903580542_gshared (InternalEnumerator_1_t277738608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3903580542_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		KeyValuePair_2_t370248105  L_8 = ((  KeyValuePair_2_t370248105  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1865178318_gshared (InternalEnumerator_1_t2404181862 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3269909010_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2101218568_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	{
		Link_t2496691359  L_0 = ((  Link_t2496691359  (*) (InternalEnumerator_1_t2404181862 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2404181862 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Link_t2496691359  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1986195493_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m608833986_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Generic.Link>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1839009783_MetadataUsageId;
extern "C"  Link_t2496691359  InternalEnumerator_1_get_Current_m1839009783_gshared (InternalEnumerator_1_t2404181862 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1839009783_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Link_t2496691359  L_8 = ((  Link_t2496691359  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1735201994_gshared (InternalEnumerator_1_t4205037797 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m372492438_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m434290508_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	{
		Slot_t2579998  L_0 = ((  Slot_t2579998  (*) (InternalEnumerator_1_t4205037797 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t4205037797 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2579998  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2838377249_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2675725766_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.Hashtable/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3036426419_MetadataUsageId;
extern "C"  Slot_t2579998  InternalEnumerator_1_get_Current_m3036426419_gshared (InternalEnumerator_1_t4205037797 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3036426419_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2579998  L_8 = ((  Slot_t2579998  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m831950091_gshared (InternalEnumerator_1_t4205037798 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m234798773_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4234291809_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	{
		Slot_t2579999  L_0 = ((  Slot_t2579999  (*) (InternalEnumerator_1_t4205037798 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t4205037798 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Slot_t2579999  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m857987234_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3764038305_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Collections.SortedList/Slot>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2267962386_MetadataUsageId;
extern "C"  Slot_t2579999  InternalEnumerator_1_get_Current_m2267962386_gshared (InternalEnumerator_1_t4205037798 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2267962386_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Slot_t2579999  L_8 = ((  Slot_t2579999  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3756518911_gshared (InternalEnumerator_1_t246524439 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3937513793_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m200412919_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	{
		DateTime_t339033936  L_0 = ((  DateTime_t339033936  (*) (InternalEnumerator_1_t246524439 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t246524439 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DateTime_t339033936  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1192762006_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m556104049_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4103732200_MetadataUsageId;
extern "C"  DateTime_t339033936  InternalEnumerator_1_get_Current_m4103732200_gshared (InternalEnumerator_1_t246524439 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4103732200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DateTime_t339033936  L_8 = ((  DateTime_t339033936  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2868618147_gshared (InternalEnumerator_1_t1596047757 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4177342749_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Decimal>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m543347273_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	{
		Decimal_t1688557254  L_0 = ((  Decimal_t1688557254  (*) (InternalEnumerator_1_t1596047757 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1596047757 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Decimal_t1688557254  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Decimal>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m136301882_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Decimal>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2432816137_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Decimal>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3602913834_MetadataUsageId;
extern "C"  Decimal_t1688557254  InternalEnumerator_1_get_Current_m3602913834_gshared (InternalEnumerator_1_t1596047757 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3602913834_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Decimal_t1688557254  L_8 = ((  Decimal_t1688557254  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m923076597_gshared (InternalEnumerator_1_t442007117 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3756521355_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m79236865_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	{
		double L_0 = ((  double (*) (InternalEnumerator_1_t442007117 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t442007117 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		double L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3854110732_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3253414715_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m403053214_MetadataUsageId;
extern "C"  double InternalEnumerator_1_get_Current_m403053214_gshared (InternalEnumerator_1_t442007117 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m403053214_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		double L_8 = ((  double (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1585494054_gshared (InternalEnumerator_1_t2754905232 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4058533818_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638134246_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	{
		int16_t L_0 = ((  int16_t (*) (InternalEnumerator_1_t2754905232 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2754905232 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2583239037_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3253274534_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m296300717_MetadataUsageId;
extern "C"  int16_t InternalEnumerator_1_get_Current_m296300717_gshared (InternalEnumerator_1_t2754905232 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m296300717_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int16_t L_8 = ((  int16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m767389920_gshared (InternalEnumerator_1_t2754905290 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2664847552_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4039258732_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t2754905290 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2754905290 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2745733815_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3995645356_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Int32>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1478851815_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1478851815_gshared (InternalEnumerator_1_t2754905290 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1478851815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3055898623_gshared (InternalEnumerator_1_t2754905385 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1566904129_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1660175405_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = ((  int64_t (*) (InternalEnumerator_1_t2754905385 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2754905385 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Int64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m642251926_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Int64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3212216237_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Int64>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1194254150_MetadataUsageId;
extern "C"  int64_t InternalEnumerator_1_get_Current_m1194254150_gshared (InternalEnumerator_1_t2754905385 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1194254150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int64_t L_8 = ((  int64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m152804899_gshared (InternalEnumerator_1_t584182523 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2898405021_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.IntPtr>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1126443155_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = ((  IntPtr_t (*) (InternalEnumerator_1_t584182523 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t584182523 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IntPtr_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.IntPtr>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1686038458_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.IntPtr>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m467683661_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.IntPtr>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2342284108_MetadataUsageId;
extern "C"  IntPtr_t InternalEnumerator_1_get_Current_m2342284108_gshared (InternalEnumerator_1_t584182523 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2342284108_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		IntPtr_t L_8 = ((  IntPtr_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2616641763_gshared (InternalEnumerator_1_t744596923 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (InternalEnumerator_1_t744596923 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t744596923 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Object>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2760671866_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Object>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3716548237_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Object>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2178852364_MetadataUsageId;
extern "C"  Il2CppObject * InternalEnumerator_1_get_Current_m2178852364_gshared (InternalEnumerator_1_t744596923 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2178852364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2025782336_gshared (InternalEnumerator_1_t226225632 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m105599328_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m125569100_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t318735129  L_0 = ((  CustomAttributeNamedArgument_t318735129  (*) (InternalEnumerator_1_t226225632 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t226225632 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeNamedArgument_t318735129  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3114194455_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1042509516_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2477961351_MetadataUsageId;
extern "C"  CustomAttributeNamedArgument_t318735129  InternalEnumerator_1_get_Current_m2477961351_gshared (InternalEnumerator_1_t226225632 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2477961351_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeNamedArgument_t318735129  L_8 = ((  CustomAttributeNamedArgument_t318735129  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m374673841_gshared (InternalEnumerator_1_t467906065 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1778039887_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667580923_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t560415562  L_0 = ((  CustomAttributeTypedArgument_t560415562  (*) (InternalEnumerator_1_t467906065 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t467906065 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		CustomAttributeTypedArgument_t560415562  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1367004360_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2714191419_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3407736504_MetadataUsageId;
extern "C"  CustomAttributeTypedArgument_t560415562  InternalEnumerator_1_get_Current_m3407736504_gshared (InternalEnumerator_1_t467906065 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3407736504_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		CustomAttributeTypedArgument_t560415562  L_8 = ((  CustomAttributeTypedArgument_t560415562  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m238137273_gshared (InternalEnumerator_1_t1303237477 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1811681607_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4263361971_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	{
		LabelData_t1395746974  L_0 = ((  LabelData_t1395746974  (*) (InternalEnumerator_1_t1303237477 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1303237477 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelData_t1395746974  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3252411600_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4284495539_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2308068992_MetadataUsageId;
extern "C"  LabelData_t1395746974  InternalEnumerator_1_get_Current_m2308068992_gshared (InternalEnumerator_1_t1303237477 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2308068992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelData_t1395746974  L_8 = ((  LabelData_t1395746974  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4080636215_gshared (InternalEnumerator_1_t228063683 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2191921929_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1633943551_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	{
		LabelFixup_t320573180  L_0 = ((  LabelFixup_t320573180  (*) (InternalEnumerator_1_t228063683 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t228063683 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		LabelFixup_t320573180  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2169103310_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1336166329_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILGenerator/LabelFixup>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3571284320_MetadataUsageId;
extern "C"  LabelFixup_t320573180  InternalEnumerator_1_get_Current_m3571284320_gshared (InternalEnumerator_1_t228063683 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3571284320_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		LabelFixup_t320573180  L_8 = ((  LabelFixup_t320573180  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2480959198_gshared (InternalEnumerator_1_t3630765784 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3034770946_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m375843822_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	{
		ILTokenInfo_t3723275281  L_0 = ((  ILTokenInfo_t3723275281  (*) (InternalEnumerator_1_t3630765784 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3630765784 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ILTokenInfo_t3723275281  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1771263541_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2010832750_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.Emit.ILTokenInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3618560037_MetadataUsageId;
extern "C"  ILTokenInfo_t3723275281  InternalEnumerator_1_get_Current_m3618560037_gshared (InternalEnumerator_1_t3630765784 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3618560037_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ILTokenInfo_t3723275281  L_8 = ((  ILTokenInfo_t3723275281  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2078231777_gshared (InternalEnumerator_1_t407693973 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m787534623_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3940484565_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	{
		ParameterModifier_t500203470  L_0 = ((  ParameterModifier_t500203470  (*) (InternalEnumerator_1_t407693973 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t407693973 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ParameterModifier_t500203470  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2387364856_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3792346959_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Reflection.ParameterModifier>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m147444170_MetadataUsageId;
extern "C"  ParameterModifier_t500203470  InternalEnumerator_1_get_Current_m147444170_gshared (InternalEnumerator_1_t407693973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m147444170_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ParameterModifier_t500203470  L_8 = ((  ParameterModifier_t500203470  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2772733110_gshared (InternalEnumerator_1_t3607348206 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2262077738_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1326759648_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	{
		ResourceCacheItem_t3699857703  L_0 = ((  ResourceCacheItem_t3699857703  (*) (InternalEnumerator_1_t3607348206 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3607348206 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceCacheItem_t3699857703  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m420635149_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4160471130_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceCacheItem>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4042729375_MetadataUsageId;
extern "C"  ResourceCacheItem_t3699857703  InternalEnumerator_1_get_Current_m4042729375_gshared (InternalEnumerator_1_t3607348206 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4042729375_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceCacheItem_t3699857703  L_8 = ((  ResourceCacheItem_t3699857703  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2865872515_gshared (InternalEnumerator_1_t3982075075 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3939002941_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m763692585_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	{
		ResourceInfo_t4074584572  L_0 = ((  ResourceInfo_t4074584572  (*) (InternalEnumerator_1_t3982075075 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3982075075 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ResourceInfo_t4074584572  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3639607322_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3402661033_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Resources.ResourceReader/ResourceInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3891250378_MetadataUsageId;
extern "C"  ResourceInfo_t4074584572  InternalEnumerator_1_get_Current_m3891250378_gshared (InternalEnumerator_1_t3982075075 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3891250378_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ResourceInfo_t4074584572  L_8 = ((  ResourceInfo_t4074584572  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2400880886_gshared (InternalEnumerator_1_t1645779784 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2455416042_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2757425494_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	{
		uint8_t L_0 = ((  uint8_t (*) (InternalEnumerator_1_t1645779784 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1645779784 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m44740173_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2285731670_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m790384317_MetadataUsageId;
extern "C"  uint8_t InternalEnumerator_1_get_Current_m790384317_gshared (InternalEnumerator_1_t1645779784 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m790384317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint8_t L_8 = ((  uint8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1925586605_gshared (InternalEnumerator_1_t2762836567 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3343201747_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.SByte>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1022211391_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	{
		int8_t L_0 = ((  int8_t (*) (InternalEnumerator_1_t2762836567 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2762836567 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int8_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.SByte>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2750196932_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.SByte>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4134001983_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.SByte>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m92522612_MetadataUsageId;
extern "C"  int8_t InternalEnumerator_1_get_Current_m92522612_gshared (InternalEnumerator_1_t2762836567 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m92522612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int8_t L_8 = ((  int8_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3415441081_gshared (InternalEnumerator_1_t1029642187 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2773606983_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1625743037_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	{
		X509ChainStatus_t1122151684  L_0 = ((  X509ChainStatus_t1122151684  (*) (InternalEnumerator_1_t1029642187 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1029642187 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		X509ChainStatus_t1122151684  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2305059792_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1811839991_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2419281506_MetadataUsageId;
extern "C"  X509ChainStatus_t1122151684  InternalEnumerator_1_get_Current_m2419281506_gshared (InternalEnumerator_1_t1029642187 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2419281506_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		X509ChainStatus_t1122151684  L_8 = ((  X509ChainStatus_t1122151684  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2203995436_gshared (InternalEnumerator_1_t865699524 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4252737780_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m395855274_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	{
		float L_0 = ((  float (*) (InternalEnumerator_1_t865699524 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t865699524 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m479600131_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1722801188_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1563251989_MetadataUsageId;
extern "C"  float InternalEnumerator_1_get_Current_m1563251989_gshared (InternalEnumerator_1_t865699524 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1563251989_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		float L_8 = ((  float (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m207626719_gshared (InternalEnumerator_1_t3633423279 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2794074465_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3100977815_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	{
		Mark_t3725932776  L_0 = ((  Mark_t3725932776  (*) (InternalEnumerator_1_t3633423279 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3633423279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Mark_t3725932776  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1252370038_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2967245969_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3956653384_MetadataUsageId;
extern "C"  Mark_t3725932776  InternalEnumerator_1_get_Current_m3956653384_gshared (InternalEnumerator_1_t3633423279 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3956653384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Mark_t3725932776  L_8 = ((  Mark_t3725932776  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m111087899_gshared (InternalEnumerator_1_t671353395 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3097305253_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.TimeSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4215104347_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	{
		TimeSpan_t763862892  L_0 = ((  TimeSpan_t763862892  (*) (InternalEnumerator_1_t671353395 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t671353395 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		TimeSpan_t763862892  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.TimeSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m774844594_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.TimeSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m485566165_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.TimeSpan>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2948637700_MetadataUsageId;
extern "C"  TimeSpan_t763862892  InternalEnumerator_1_get_Current_m2948637700_gshared (InternalEnumerator_1_t671353395 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2948637700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		TimeSpan_t763862892  L_8 = ((  TimeSpan_t763862892  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1037769859_gshared (InternalEnumerator_1_t893415771 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1842760765_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1001231923_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = ((  uint16_t (*) (InternalEnumerator_1_t893415771 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t893415771 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1215749658_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3068600045_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m737292716_MetadataUsageId;
extern "C"  uint16_t InternalEnumerator_1_get_Current_m737292716_gshared (InternalEnumerator_1_t893415771 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m737292716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint16_t L_8 = ((  uint16_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m219665725_gshared (InternalEnumerator_1_t893415829 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m449074499_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.UInt32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1402356409_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = ((  uint32_t (*) (InternalEnumerator_1_t893415829 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t893415829 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1378244436_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3810970867_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.UInt32>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1919843814_MetadataUsageId;
extern "C"  uint32_t InternalEnumerator_1_get_Current_m1919843814_gshared (InternalEnumerator_1_t893415829 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1919843814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint32_t L_8 = ((  uint32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2508174428_gshared (InternalEnumerator_1_t893415924 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3646098372_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.UInt64>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3318240378_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = ((  uint64_t (*) (InternalEnumerator_1_t893415924 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t893415924 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint64_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.UInt64>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3569729843_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.UInt64>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3027541748_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.UInt64>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1635246149_MetadataUsageId;
extern "C"  uint64_t InternalEnumerator_1_get_Current_m1635246149_gshared (InternalEnumerator_1_t893415924 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1635246149_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		uint64_t L_8 = ((  uint64_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2879257728_gshared (InternalEnumerator_1_t3174019288 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m964815136_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<System.Uri/UriScheme>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m994385868_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	{
		UriScheme_t3266528785  L_0 = ((  UriScheme_t3266528785  (*) (InternalEnumerator_1_t3174019288 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3174019288 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UriScheme_t3266528785  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<System.Uri/UriScheme>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1861559895_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<System.Uri/UriScheme>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m91355148_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<System.Uri/UriScheme>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3881062791_MetadataUsageId;
extern "C"  UriScheme_t3266528785  InternalEnumerator_1_get_Current_m3881062791_gshared (InternalEnumerator_1_t3174019288 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3881062791_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UriScheme_t3266528785  L_8 = ((  UriScheme_t3266528785  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1560867446_gshared (InternalEnumerator_1_t1003321853 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m595907434_gshared (InternalEnumerator_1_t1003321853 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2160639574_gshared (InternalEnumerator_1_t1003321853 * __this, const MethodInfo* method)
{
	{
		FadeEntry_t1095831350  L_0 = ((  FadeEntry_t1095831350  (*) (InternalEnumerator_1_t1003321853 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1003321853 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		FadeEntry_t1095831350  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4198954957_gshared (InternalEnumerator_1_t1003321853 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m120295126_gshared (InternalEnumerator_1_t1003321853 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<TypewriterEffect/FadeEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3911490749_MetadataUsageId;
extern "C"  FadeEntry_t1095831350  InternalEnumerator_1_get_Current_m3911490749_gshared (InternalEnumerator_1_t1003321853 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3911490749_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		FadeEntry_t1095831350  L_8 = ((  FadeEntry_t1095831350  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UICamera/DepthEntry>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1378027210_gshared (InternalEnumerator_1_t2102187606 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UICamera/DepthEntry>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2047121046_gshared (InternalEnumerator_1_t2102187606 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UICamera/DepthEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2791742988_gshared (InternalEnumerator_1_t2102187606 * __this, const MethodInfo* method)
{
	{
		DepthEntry_t2194697103  L_0 = ((  DepthEntry_t2194697103  (*) (InternalEnumerator_1_t2102187606 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2102187606 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		DepthEntry_t2194697103  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UICamera/DepthEntry>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2940904737_gshared (InternalEnumerator_1_t2102187606 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UICamera/DepthEntry>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3900747590_gshared (InternalEnumerator_1_t2102187606 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UICamera/DepthEntry>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m283693171_MetadataUsageId;
extern "C"  DepthEntry_t2194697103  InternalEnumerator_1_get_Current_m283693171_gshared (InternalEnumerator_1_t2102187606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m283693171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		DepthEntry_t2194697103  L_8 = ((  DepthEntry_t2194697103  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Bounds>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m4055748925_gshared (InternalEnumerator_1_t3426005481 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Bounds>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m822401347_gshared (InternalEnumerator_1_t3426005481 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Bounds>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4284209263_gshared (InternalEnumerator_1_t3426005481 * __this, const MethodInfo* method)
{
	{
		Bounds_t3518514978  L_0 = ((  Bounds_t3518514978  (*) (InternalEnumerator_1_t3426005481 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3426005481 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Bounds_t3518514978  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Bounds>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m852972372_gshared (InternalEnumerator_1_t3426005481 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Bounds>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1395966511_gshared (InternalEnumerator_1_t3426005481 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Bounds>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m867797956_MetadataUsageId;
extern "C"  Bounds_t3518514978  InternalEnumerator_1_get_Current_m867797956_gshared (InternalEnumerator_1_t3426005481 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m867797956_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Bounds_t3518514978  L_8 = ((  Bounds_t3518514978  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1839379985_gshared (InternalEnumerator_1_t1495666263 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3079863279_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2080462309_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ((  Color_t1588175760  (*) (InternalEnumerator_1_t1495666263 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1495666263 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color_t1588175760  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m643330344_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1891900575_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3730699578_MetadataUsageId;
extern "C"  Color_t1588175760  InternalEnumerator_1_get_Current_m3730699578_gshared (InternalEnumerator_1_t1495666263 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3730699578_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color_t1588175760  L_8 = ((  Color_t1588175760  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2826013104_gshared (InternalEnumerator_1_t4044574710 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m4281953776_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1766787558_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	{
		Color32_t4137084207  L_0 = ((  Color32_t4137084207  (*) (InternalEnumerator_1_t4044574710 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t4044574710 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Color32_t4137084207  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Color32>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4247678855_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Color32>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1461072288_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Color32>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3648321497_MetadataUsageId;
extern "C"  Color32_t4137084207  InternalEnumerator_1_get_Current_m3648321497_gshared (InternalEnumerator_1_t4044574710 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3648321497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Color32_t4137084207  L_8 = ((  Color32_t4137084207  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2847645656_gshared (InternalEnumerator_1_t2858612868 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1868895944_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3774095860_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	{
		ContactPoint_t2951122365  L_0 = ((  ContactPoint_t2951122365  (*) (InternalEnumerator_1_t2858612868 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2858612868 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint_t2951122365  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2261686191_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2398593716_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3523444575_MetadataUsageId;
extern "C"  ContactPoint_t2951122365  InternalEnumerator_1_get_Current_m3523444575_gshared (InternalEnumerator_1_t2858612868 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3523444575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint_t2951122365  L_8 = ((  ContactPoint_t2951122365  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2104644970_gshared (InternalEnumerator_1_t3871236822 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2865529846_gshared (InternalEnumerator_1_t3871236822 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1836373474_gshared (InternalEnumerator_1_t3871236822 * __this, const MethodInfo* method)
{
	{
		ContactPoint2D_t3963746319  L_0 = ((  ContactPoint2D_t3963746319  (*) (InternalEnumerator_1_t3871236822 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3871236822 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ContactPoint2D_t3963746319  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4228758465_gshared (InternalEnumerator_1_t3871236822 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1044203874_gshared (InternalEnumerator_1_t3871236822 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.ContactPoint2D>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1631602353_MetadataUsageId;
extern "C"  ContactPoint2D_t3963746319  InternalEnumerator_1_get_Current_m1631602353_gshared (InternalEnumerator_1_t3871236822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1631602353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		ContactPoint2D_t3963746319  L_8 = ((  ContactPoint2D_t3963746319  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3551691594_gshared (InternalEnumerator_1_t867389192 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1289144854_gshared (InternalEnumerator_1_t867389192 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1671609218_gshared (InternalEnumerator_1_t867389192 * __this, const MethodInfo* method)
{
	{
		RaycastResult_t959898689  L_0 = ((  RaycastResult_t959898689  (*) (InternalEnumerator_1_t867389192 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t867389192 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastResult_t959898689  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2318647713_gshared (InternalEnumerator_1_t867389192 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m209654402_gshared (InternalEnumerator_1_t867389192 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3858822673_MetadataUsageId;
extern "C"  RaycastResult_t959898689  InternalEnumerator_1_get_Current_m3858822673_gshared (InternalEnumerator_1_t867389192 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3858822673_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastResult_t959898689  L_8 = ((  RaycastResult_t959898689  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2586242586_gshared (InternalEnumerator_1_t2279071712 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m539249990_gshared (InternalEnumerator_1_t2279071712 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m95326396_gshared (InternalEnumerator_1_t2279071712 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t2279071712 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2279071712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3260575857_gshared (InternalEnumerator_1_t2279071712 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m925650422_gshared (InternalEnumerator_1_t2279071712 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.KeyCode>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m109081539_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m109081539_gshared (InternalEnumerator_1_t2279071712 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m109081539_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2728019190_gshared (InternalEnumerator_1_t2002543010 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3521736938_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3512084502_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	{
		Keyframe_t2095052507  L_0 = ((  Keyframe_t2095052507  (*) (InternalEnumerator_1_t2002543010 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2002543010 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Keyframe_t2095052507  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1130719821_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3205116630_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m3245714045_MetadataUsageId;
extern "C"  Keyframe_t2095052507  InternalEnumerator_1_get_Current_m3245714045_gshared (InternalEnumerator_1_t2002543010 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m3245714045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Keyframe_t2095052507  L_8 = ((  Keyframe_t2095052507  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.LogType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1028794444_gshared (InternalEnumerator_1_t3436759954 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.LogType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2269557204_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.LogType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1378538186_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t3436759954 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3436759954 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.LogType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2775277859_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.LogType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3061281668_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.LogType>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1348975477_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1348975477_gshared (InternalEnumerator_1_t3436759954 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1348975477_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3454518962_gshared (InternalEnumerator_1_t4248679326 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3915103662_gshared (InternalEnumerator_1_t4248679326 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2395460378_gshared (InternalEnumerator_1_t4248679326 * __this, const MethodInfo* method)
{
	{
		RaycastHit_t46221527  L_0 = ((  RaycastHit_t46221527  (*) (InternalEnumerator_1_t4248679326 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t4248679326 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit_t46221527  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3675957001_gshared (InternalEnumerator_1_t4248679326 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1662326298_gshared (InternalEnumerator_1_t4248679326 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2996847993_MetadataUsageId;
extern "C"  RaycastHit_t46221527  InternalEnumerator_1_get_Current_m2996847993_gshared (InternalEnumerator_1_t4248679326 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2996847993_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit_t46221527  L_8 = ((  RaycastHit_t46221527  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1194339780_gshared (InternalEnumerator_1_t3990273904 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2176125276_gshared (InternalEnumerator_1_t3990273904 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4112569736_gshared (InternalEnumerator_1_t3990273904 * __this, const MethodInfo* method)
{
	{
		RaycastHit2D_t4082783401  L_0 = ((  RaycastHit2D_t4082783401  (*) (InternalEnumerator_1_t3990273904 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3990273904 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		RaycastHit2D_t4082783401  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1838374043_gshared (InternalEnumerator_1_t3990273904 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2160819016_gshared (InternalEnumerator_1_t3990273904 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit2D>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2378427979_MetadataUsageId;
extern "C"  RaycastHit2D_t4082783401  InternalEnumerator_1_get_Current_m2378427979_gshared (InternalEnumerator_1_t3990273904 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2378427979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		RaycastHit2D_t4082783401  L_8 = ((  RaycastHit2D_t4082783401  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2952786262_gshared (InternalEnumerator_1_t2498719112 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3508316298_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2891046656_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	{
		HitInfo_t2591228609  L_0 = ((  HitInfo_t2591228609  (*) (InternalEnumerator_1_t2498719112 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2498719112 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		HitInfo_t2591228609  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m54857389_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m204092218_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4186452479_MetadataUsageId;
extern "C"  HitInfo_t2591228609  InternalEnumerator_1_get_Current_m4186452479_gshared (InternalEnumerator_1_t2498719112 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4186452479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		HitInfo_t2591228609  L_8 = ((  HitInfo_t2591228609  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3202091545_gshared (InternalEnumerator_1_t1224502599 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3642743527_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3542460115_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	{
		GcAchievementData_t1317012096  L_0 = ((  GcAchievementData_t1317012096  (*) (InternalEnumerator_1_t1224502599 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1224502599 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcAchievementData_t1317012096  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2757865264_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2931622739_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcAchievementData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m4069864032_MetadataUsageId;
extern "C"  GcAchievementData_t1317012096  InternalEnumerator_1_get_Current_m4069864032_gshared (InternalEnumerator_1_t1224502599 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m4069864032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcAchievementData_t1317012096  L_8 = ((  GcAchievementData_t1317012096  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m909397884_gshared (InternalEnumerator_1_t2131168810 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3844431012_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3502383888_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	{
		GcScoreData_t2223678307  L_0 = ((  GcScoreData_t2223678307  (*) (InternalEnumerator_1_t2131168810 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2131168810 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		GcScoreData_t2223678307  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3662398547_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3456760080_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.GameCenter.GcScoreData>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2404034883_MetadataUsageId;
extern "C"  GcScoreData_t2223678307  InternalEnumerator_1_get_Current_m2404034883_gshared (InternalEnumerator_1_t2131168810 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2404034883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		GcScoreData_t2223678307  L_8 = ((  GcScoreData_t2223678307  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1325614747_gshared (InternalEnumerator_1_t3336978431 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2147875621_gshared (InternalEnumerator_1_t3336978431 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3798108827_gshared (InternalEnumerator_1_t3336978431 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t3336978431 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3336978431 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3603973682_gshared (InternalEnumerator_1_t3336978431 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m639411413_gshared (InternalEnumerator_1_t3336978431 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.TextEditor/TextEditOp>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1313624900_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1313624900_gshared (InternalEnumerator_1_t3336978431 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1313624900_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m101605564_gshared (InternalEnumerator_1_t1186227706 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3938272100_gshared (InternalEnumerator_1_t1186227706 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m200787226_gshared (InternalEnumerator_1_t1186227706 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((  int32_t (*) (InternalEnumerator_1_t1186227706 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t1186227706 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m655254931_gshared (InternalEnumerator_1_t1186227706 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2420187284_gshared (InternalEnumerator_1_t1186227706 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UI.InputField/ContentType>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1455522213_MetadataUsageId;
extern "C"  int32_t InternalEnumerator_1_get_Current_m1455522213_gshared (InternalEnumerator_1_t1186227706 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1455522213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		int32_t L_8 = ((  int32_t (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3391471488_gshared (InternalEnumerator_1_t311311084 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2917484064_gshared (InternalEnumerator_1_t311311084 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3868311052_gshared (InternalEnumerator_1_t311311084 * __this, const MethodInfo* method)
{
	{
		UICharInfo_t403820581  L_0 = ((  UICharInfo_t403820581  (*) (InternalEnumerator_1_t311311084 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t311311084 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UICharInfo_t403820581  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1019521367_gshared (InternalEnumerator_1_t311311084 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m917200268_gshared (InternalEnumerator_1_t311311084 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m1338273991_MetadataUsageId;
extern "C"  UICharInfo_t403820581  InternalEnumerator_1_get_Current_m1338273991_gshared (InternalEnumerator_1_t311311084 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m1338273991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UICharInfo_t403820581  L_8 = ((  UICharInfo_t403820581  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1258762910_gshared (InternalEnumerator_1_t64411786 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1873285698_gshared (InternalEnumerator_1_t64411786 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1285515438_gshared (InternalEnumerator_1_t64411786 * __this, const MethodInfo* method)
{
	{
		UILineInfo_t156921283  L_0 = ((  UILineInfo_t156921283  (*) (InternalEnumerator_1_t64411786 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t64411786 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UILineInfo_t156921283  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m300403189_gshared (InternalEnumerator_1_t64411786 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m99373230_gshared (InternalEnumerator_1_t64411786 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UILineInfo>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m2802455141_MetadataUsageId;
extern "C"  UILineInfo_t156921283  InternalEnumerator_1_get_Current_m2802455141_gshared (InternalEnumerator_1_t64411786 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m2802455141_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UILineInfo_t156921283  L_8 = ((  UILineInfo_t156921283  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1877903424_gshared (InternalEnumerator_1_t2167552108 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2348303712_gshared (InternalEnumerator_1_t2167552108 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1410681868_gshared (InternalEnumerator_1_t2167552108 * __this, const MethodInfo* method)
{
	{
		UIVertex_t2260061605  L_0 = ((  UIVertex_t2260061605  (*) (InternalEnumerator_1_t2167552108 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t2167552108 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		UIVertex_t2260061605  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4035797527_gshared (InternalEnumerator_1_t2167552108 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3068212300_gshared (InternalEnumerator_1_t2167552108 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.UIVertex>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m652782919_MetadataUsageId;
extern "C"  UIVertex_t2260061605  InternalEnumerator_1_get_Current_m652782919_gshared (InternalEnumerator_1_t2167552108 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m652782919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		UIVertex_t2260061605  L_8 = ((  UIVertex_t2260061605  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3463151677_gshared (InternalEnumerator_1_t3432820291 * __this, Il2CppArray * ___array0, const MethodInfo* method)
{
	{
		Il2CppArray * L_0 = ___array0;
		__this->set_array_0(L_0);
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1422051907_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2550701049_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	{
		Vector2_t3525329788  L_0 = ((  Vector2_t3525329788  (*) (InternalEnumerator_1_t3432820291 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->method)((InternalEnumerator_1_t3432820291 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Vector2_t3525329788  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2902704724_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	{
		__this->set_idx_1(((int32_t)-2));
		return;
	}
}
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2716547187_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_001e;
		}
	}
	{
		Il2CppArray * L_1 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_1);
		int32_t L_2 = Array_get_Length_m1203127607((Il2CppArray *)L_1, /*hidden argument*/NULL);
		__this->set_idx_1(L_2);
	}

IL_001e:
	{
		int32_t L_3 = (int32_t)__this->get_idx_1();
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_4 = (int32_t)__this->get_idx_1();
		int32_t L_5 = (int32_t)((int32_t)((int32_t)L_4-(int32_t)1));
		V_0 = (int32_t)L_5;
		__this->set_idx_1(L_5);
		int32_t L_6 = V_0;
		G_B5_0 = ((((int32_t)((((int32_t)L_6) == ((int32_t)(-1)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0044;
	}

IL_0043:
	{
		G_B5_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B5_0;
	}
}
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern Il2CppClass* InvalidOperationException_t2420574324_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3621260575;
extern Il2CppCodeGenString* _stringLiteral2197328915;
extern const uint32_t InternalEnumerator_1_get_Current_m630856742_MetadataUsageId;
extern "C"  Vector2_t3525329788  InternalEnumerator_1_get_Current_m630856742_gshared (InternalEnumerator_1_t3432820291 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InternalEnumerator_1_get_Current_m630856742_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_1 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, (String_t*)_stringLiteral3621260575, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_idx_1();
		if ((!(((uint32_t)L_2) == ((uint32_t)(-1)))))
		{
			goto IL_002f;
		}
	}
	{
		InvalidOperationException_t2420574324 * L_3 = (InvalidOperationException_t2420574324 *)il2cpp_codegen_object_new(InvalidOperationException_t2420574324_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, (String_t*)_stringLiteral2197328915, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_002f:
	{
		Il2CppArray * L_4 = (Il2CppArray *)__this->get_array_0();
		Il2CppArray * L_5 = (Il2CppArray *)__this->get_array_0();
		NullCheck((Il2CppArray *)L_5);
		int32_t L_6 = Array_get_Length_m1203127607((Il2CppArray *)L_5, /*hidden argument*/NULL);
		int32_t L_7 = (int32_t)__this->get_idx_1();
		NullCheck((Il2CppArray *)L_4);
		Vector2_t3525329788  L_8 = ((  Vector2_t3525329788  (*) (Il2CppArray *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->method)((Il2CppArray *)L_4, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_6-(int32_t)1))-(int32_t)L_7)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_8;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
