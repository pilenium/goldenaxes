﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AxeSlot
struct AxeSlot_t1089550860;
// DataAxe
struct DataAxe_t3107820260;

#include "codegen/il2cpp-codegen.h"

// System.Void AxeSlot::.ctor()
extern "C"  void AxeSlot__ctor_m124420831 (AxeSlot_t1089550860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 AxeSlot::get_id()
extern "C"  uint32_t AxeSlot_get_id_m1552903246 (AxeSlot_t1089550860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataAxe AxeSlot::get_data()
extern "C"  DataAxe_t3107820260 * AxeSlot_get_data_m3596026761 (AxeSlot_t1089550860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AxeSlot::Clear()
extern "C"  void AxeSlot_Clear_m1825521418 (AxeSlot_t1089550860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AxeSlot::SetAxe(System.UInt32)
extern "C"  bool AxeSlot_SetAxe_m301184637 (AxeSlot_t1089550860 * __this, uint32_t ___axe_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AxeSlot::CalculateVelocity(System.Single)
extern "C"  void AxeSlot_CalculateVelocity_m475350539 (AxeSlot_t1089550860 * __this, float ___height0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
