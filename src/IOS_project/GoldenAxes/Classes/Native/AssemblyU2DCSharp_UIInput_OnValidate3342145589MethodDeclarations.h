﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIInput/OnValidate
struct OnValidate_t3342145589;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UIInput/OnValidate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnValidate__ctor_m3323288053 (OnValidate_t3342145589 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UIInput/OnValidate::Invoke(System.String,System.Int32,System.Char)
extern "C"  uint16_t OnValidate_Invoke_m1628410489 (OnValidate_t3342145589 * __this, String_t* ___text0, int32_t ___charIndex1, uint16_t ___addedChar2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" uint16_t pinvoke_delegate_wrapper_OnValidate_t3342145589(Il2CppObject* delegate, String_t* ___text0, int32_t ___charIndex1, uint16_t ___addedChar2);
// System.IAsyncResult UIInput/OnValidate::BeginInvoke(System.String,System.Int32,System.Char,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnValidate_BeginInvoke_m759250206 (OnValidate_t3342145589 * __this, String_t* ___text0, int32_t ___charIndex1, uint16_t ___addedChar2, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char UIInput/OnValidate::EndInvoke(System.IAsyncResult)
extern "C"  uint16_t OnValidate_EndInvoke_m3668322691 (OnValidate_t3342145589 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
