﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Console/Log>
struct List_1_t797035549;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3177785837.h"
#include "AssemblyU2DCSharp_Console_Log76580.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Console/Log>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m332140211_gshared (Enumerator_t3177785837 * __this, List_1_t797035549 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m332140211(__this, ___l0, method) ((  void (*) (Enumerator_t3177785837 *, List_1_t797035549 *, const MethodInfo*))Enumerator__ctor_m332140211_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Console/Log>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1724604863_gshared (Enumerator_t3177785837 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1724604863(__this, method) ((  void (*) (Enumerator_t3177785837 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1724604863_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Console/Log>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4264978027_gshared (Enumerator_t3177785837 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4264978027(__this, method) ((  Il2CppObject * (*) (Enumerator_t3177785837 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4264978027_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Console/Log>::Dispose()
extern "C"  void Enumerator_Dispose_m2266315096_gshared (Enumerator_t3177785837 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2266315096(__this, method) ((  void (*) (Enumerator_t3177785837 *, const MethodInfo*))Enumerator_Dispose_m2266315096_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Console/Log>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2245823633_gshared (Enumerator_t3177785837 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2245823633(__this, method) ((  void (*) (Enumerator_t3177785837 *, const MethodInfo*))Enumerator_VerifyState_m2245823633_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Console/Log>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m913100203_gshared (Enumerator_t3177785837 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m913100203(__this, method) ((  bool (*) (Enumerator_t3177785837 *, const MethodInfo*))Enumerator_MoveNext_m913100203_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Console/Log>::get_Current()
extern "C"  Log_t76580  Enumerator_get_Current_m3747289160_gshared (Enumerator_t3177785837 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3747289160(__this, method) ((  Log_t76580  (*) (Enumerator_t3177785837 *, const MethodInfo*))Enumerator_get_Current_m3747289160_gshared)(__this, method)
