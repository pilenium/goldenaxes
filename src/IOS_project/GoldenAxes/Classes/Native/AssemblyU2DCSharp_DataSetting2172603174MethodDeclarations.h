﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataSetting
struct DataSetting_t2172603174;

#include "codegen/il2cpp-codegen.h"

// System.Void DataSetting::.ctor()
extern "C"  void DataSetting__ctor_m3617020677 (DataSetting_t2172603174 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
