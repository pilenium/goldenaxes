﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataAxe
struct DataAxe_t3107820260;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AxeSlot
struct  AxeSlot_t1089550860  : public Il2CppObject
{
public:
	// System.UInt32 AxeSlot::m_id
	uint32_t ___m_id_0;
	// DataAxe AxeSlot::m_data
	DataAxe_t3107820260 * ___m_data_1;

public:
	inline static int32_t get_offset_of_m_id_0() { return static_cast<int32_t>(offsetof(AxeSlot_t1089550860, ___m_id_0)); }
	inline uint32_t get_m_id_0() const { return ___m_id_0; }
	inline uint32_t* get_address_of_m_id_0() { return &___m_id_0; }
	inline void set_m_id_0(uint32_t value)
	{
		___m_id_0 = value;
	}

	inline static int32_t get_offset_of_m_data_1() { return static_cast<int32_t>(offsetof(AxeSlot_t1089550860, ___m_data_1)); }
	inline DataAxe_t3107820260 * get_m_data_1() const { return ___m_data_1; }
	inline DataAxe_t3107820260 ** get_address_of_m_data_1() { return &___m_data_1; }
	inline void set_m_data_1(DataAxe_t3107820260 * value)
	{
		___m_data_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_data_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
