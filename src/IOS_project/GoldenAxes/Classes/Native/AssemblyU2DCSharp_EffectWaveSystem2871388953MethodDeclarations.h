﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectWaveSystem
struct EffectWaveSystem_t2871388953;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectWaveSystem::.ctor()
extern "C"  void EffectWaveSystem__ctor_m1020503970 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectWaveSystem::Awake()
extern "C"  void EffectWaveSystem_Awake_m1258109189 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectWaveSystem::OnDestroy()
extern "C"  void EffectWaveSystem_OnDestroy_m1701108635 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectWaveSystem::Init(System.Single,System.Single)
extern "C"  void EffectWaveSystem_Init_m3117736862 (EffectWaveSystem_t2871388953 * __this, float ___time0, float ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectWaveSystem::SetSpeed(System.Single)
extern "C"  void EffectWaveSystem_SetSpeed_m3172009060 (EffectWaveSystem_t2871388953 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator EffectWaveSystem::AddWave()
extern "C"  Il2CppObject * EffectWaveSystem_AddWave_m775507570 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectWaveSystem::Update()
extern "C"  void EffectWaveSystem_Update_m3297714091 (EffectWaveSystem_t2871388953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
