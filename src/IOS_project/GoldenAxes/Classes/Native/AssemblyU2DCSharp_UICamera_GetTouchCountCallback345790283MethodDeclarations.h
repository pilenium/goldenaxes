﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/GetTouchCountCallback
struct GetTouchCountCallback_t345790283;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UICamera/GetTouchCountCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void GetTouchCountCallback__ctor_m3944418492 (GetTouchCountCallback_t345790283 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera/GetTouchCountCallback::Invoke()
extern "C"  int32_t GetTouchCountCallback_Invoke_m316123172 (GetTouchCountCallback_t345790283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" int32_t pinvoke_delegate_wrapper_GetTouchCountCallback_t345790283(Il2CppObject* delegate);
// System.IAsyncResult UICamera/GetTouchCountCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GetTouchCountCallback_BeginInvoke_m1702091565 (GetTouchCountCallback_t345790283 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera/GetTouchCountCallback::EndInvoke(System.IAsyncResult)
extern "C"  int32_t GetTouchCountCallback_EndInvoke_m2209815130 (GetTouchCountCallback_t345790283 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
