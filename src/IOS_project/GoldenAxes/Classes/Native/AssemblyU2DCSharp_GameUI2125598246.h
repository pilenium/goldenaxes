﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISlider
struct UISlider_t657469589;
// FeverLights
struct FeverLights_t2165513633;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UILabel
struct UILabel_t291504320;
// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameUI
struct  GameUI_t2125598246  : public MonoBehaviour_t3012272455
{
public:
	// UISlider GameUI::normalSlide
	UISlider_t657469589 * ___normalSlide_2;
	// UISlider GameUI::feverSlide
	UISlider_t657469589 * ___feverSlide_3;
	// FeverLights GameUI::feverLights
	FeverLights_t2165513633 * ___feverLights_4;
	// UnityEngine.GameObject GameUI::feverNode
	GameObject_t4012695102 * ___feverNode_5;
	// UILabel GameUI::labelCoin
	UILabel_t291504320 * ___labelCoin_6;
	// UILabel GameUI::labelStage
	UILabel_t291504320 * ___labelStage_7;
	// UnityEngine.UI.Image GameUI::m_powText
	Image_t3354615620 * ___m_powText_8;
	// UnityEngine.UI.Image GameUI::m_guage
	Image_t3354615620 * ___m_guage_9;
	// UnityEngine.UI.Text GameUI::m_score
	Text_t3286458198 * ___m_score_10;
	// UnityEngine.UI.Text GameUI::m_distance
	Text_t3286458198 * ___m_distance_11;

public:
	inline static int32_t get_offset_of_normalSlide_2() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___normalSlide_2)); }
	inline UISlider_t657469589 * get_normalSlide_2() const { return ___normalSlide_2; }
	inline UISlider_t657469589 ** get_address_of_normalSlide_2() { return &___normalSlide_2; }
	inline void set_normalSlide_2(UISlider_t657469589 * value)
	{
		___normalSlide_2 = value;
		Il2CppCodeGenWriteBarrier(&___normalSlide_2, value);
	}

	inline static int32_t get_offset_of_feverSlide_3() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___feverSlide_3)); }
	inline UISlider_t657469589 * get_feverSlide_3() const { return ___feverSlide_3; }
	inline UISlider_t657469589 ** get_address_of_feverSlide_3() { return &___feverSlide_3; }
	inline void set_feverSlide_3(UISlider_t657469589 * value)
	{
		___feverSlide_3 = value;
		Il2CppCodeGenWriteBarrier(&___feverSlide_3, value);
	}

	inline static int32_t get_offset_of_feverLights_4() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___feverLights_4)); }
	inline FeverLights_t2165513633 * get_feverLights_4() const { return ___feverLights_4; }
	inline FeverLights_t2165513633 ** get_address_of_feverLights_4() { return &___feverLights_4; }
	inline void set_feverLights_4(FeverLights_t2165513633 * value)
	{
		___feverLights_4 = value;
		Il2CppCodeGenWriteBarrier(&___feverLights_4, value);
	}

	inline static int32_t get_offset_of_feverNode_5() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___feverNode_5)); }
	inline GameObject_t4012695102 * get_feverNode_5() const { return ___feverNode_5; }
	inline GameObject_t4012695102 ** get_address_of_feverNode_5() { return &___feverNode_5; }
	inline void set_feverNode_5(GameObject_t4012695102 * value)
	{
		___feverNode_5 = value;
		Il2CppCodeGenWriteBarrier(&___feverNode_5, value);
	}

	inline static int32_t get_offset_of_labelCoin_6() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___labelCoin_6)); }
	inline UILabel_t291504320 * get_labelCoin_6() const { return ___labelCoin_6; }
	inline UILabel_t291504320 ** get_address_of_labelCoin_6() { return &___labelCoin_6; }
	inline void set_labelCoin_6(UILabel_t291504320 * value)
	{
		___labelCoin_6 = value;
		Il2CppCodeGenWriteBarrier(&___labelCoin_6, value);
	}

	inline static int32_t get_offset_of_labelStage_7() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___labelStage_7)); }
	inline UILabel_t291504320 * get_labelStage_7() const { return ___labelStage_7; }
	inline UILabel_t291504320 ** get_address_of_labelStage_7() { return &___labelStage_7; }
	inline void set_labelStage_7(UILabel_t291504320 * value)
	{
		___labelStage_7 = value;
		Il2CppCodeGenWriteBarrier(&___labelStage_7, value);
	}

	inline static int32_t get_offset_of_m_powText_8() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___m_powText_8)); }
	inline Image_t3354615620 * get_m_powText_8() const { return ___m_powText_8; }
	inline Image_t3354615620 ** get_address_of_m_powText_8() { return &___m_powText_8; }
	inline void set_m_powText_8(Image_t3354615620 * value)
	{
		___m_powText_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_powText_8, value);
	}

	inline static int32_t get_offset_of_m_guage_9() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___m_guage_9)); }
	inline Image_t3354615620 * get_m_guage_9() const { return ___m_guage_9; }
	inline Image_t3354615620 ** get_address_of_m_guage_9() { return &___m_guage_9; }
	inline void set_m_guage_9(Image_t3354615620 * value)
	{
		___m_guage_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_guage_9, value);
	}

	inline static int32_t get_offset_of_m_score_10() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___m_score_10)); }
	inline Text_t3286458198 * get_m_score_10() const { return ___m_score_10; }
	inline Text_t3286458198 ** get_address_of_m_score_10() { return &___m_score_10; }
	inline void set_m_score_10(Text_t3286458198 * value)
	{
		___m_score_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_score_10, value);
	}

	inline static int32_t get_offset_of_m_distance_11() { return static_cast<int32_t>(offsetof(GameUI_t2125598246, ___m_distance_11)); }
	inline Text_t3286458198 * get_m_distance_11() const { return ___m_distance_11; }
	inline Text_t3286458198 ** get_address_of_m_distance_11() { return &___m_distance_11; }
	inline void set_m_distance_11(Text_t3286458198 * value)
	{
		___m_distance_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_distance_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
