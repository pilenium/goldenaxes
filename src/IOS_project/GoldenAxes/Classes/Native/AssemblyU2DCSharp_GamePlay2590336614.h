﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// GamePlay
struct GamePlay_t2590336614;
// UnityEngine.Camera
struct Camera_t3533968274;
// Stage
struct Stage_t80204510;
// AxeSlot
struct AxeSlot_t1089550860;
// PatternManager
struct PatternManager_t2286577181;
// EventManager
struct EventManager_t1907836883;
// GameUI
struct GameUI_t2125598246;
// Mode
struct Mode_t2403781;
// System.Collections.Generic.List`1<Mode>
struct List_1_t799362748;
// GameTouch
struct GameTouch_t2994825805;
// GameEvent
struct GameEvent_t2981166504;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GamePlay
struct  GamePlay_t2590336614  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Camera GamePlay::camera
	Camera_t3533968274 * ___camera_3;
	// Stage GamePlay::m_stage
	Stage_t80204510 * ___m_stage_4;
	// AxeSlot GamePlay::m_axeSlot
	AxeSlot_t1089550860 * ___m_axeSlot_5;
	// PatternManager GamePlay::m_patternManager
	PatternManager_t2286577181 * ___m_patternManager_6;
	// EventManager GamePlay::m_eventManager
	EventManager_t1907836883 * ___m_eventManager_7;
	// GameUI GamePlay::m_ui
	GameUI_t2125598246 * ___m_ui_8;
	// Mode GamePlay::m_currentMode
	Mode_t2403781 * ___m_currentMode_9;
	// System.Collections.Generic.List`1<Mode> GamePlay::m_modes
	List_1_t799362748 * ___m_modes_10;
	// GameTouch GamePlay::m_touch
	GameTouch_t2994825805 * ___m_touch_11;
	// GameEvent GamePlay::m_frog_event
	GameEvent_t2981166504 * ___m_frog_event_12;

public:
	inline static int32_t get_offset_of_camera_3() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___camera_3)); }
	inline Camera_t3533968274 * get_camera_3() const { return ___camera_3; }
	inline Camera_t3533968274 ** get_address_of_camera_3() { return &___camera_3; }
	inline void set_camera_3(Camera_t3533968274 * value)
	{
		___camera_3 = value;
		Il2CppCodeGenWriteBarrier(&___camera_3, value);
	}

	inline static int32_t get_offset_of_m_stage_4() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___m_stage_4)); }
	inline Stage_t80204510 * get_m_stage_4() const { return ___m_stage_4; }
	inline Stage_t80204510 ** get_address_of_m_stage_4() { return &___m_stage_4; }
	inline void set_m_stage_4(Stage_t80204510 * value)
	{
		___m_stage_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_stage_4, value);
	}

	inline static int32_t get_offset_of_m_axeSlot_5() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___m_axeSlot_5)); }
	inline AxeSlot_t1089550860 * get_m_axeSlot_5() const { return ___m_axeSlot_5; }
	inline AxeSlot_t1089550860 ** get_address_of_m_axeSlot_5() { return &___m_axeSlot_5; }
	inline void set_m_axeSlot_5(AxeSlot_t1089550860 * value)
	{
		___m_axeSlot_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_axeSlot_5, value);
	}

	inline static int32_t get_offset_of_m_patternManager_6() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___m_patternManager_6)); }
	inline PatternManager_t2286577181 * get_m_patternManager_6() const { return ___m_patternManager_6; }
	inline PatternManager_t2286577181 ** get_address_of_m_patternManager_6() { return &___m_patternManager_6; }
	inline void set_m_patternManager_6(PatternManager_t2286577181 * value)
	{
		___m_patternManager_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_patternManager_6, value);
	}

	inline static int32_t get_offset_of_m_eventManager_7() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___m_eventManager_7)); }
	inline EventManager_t1907836883 * get_m_eventManager_7() const { return ___m_eventManager_7; }
	inline EventManager_t1907836883 ** get_address_of_m_eventManager_7() { return &___m_eventManager_7; }
	inline void set_m_eventManager_7(EventManager_t1907836883 * value)
	{
		___m_eventManager_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_eventManager_7, value);
	}

	inline static int32_t get_offset_of_m_ui_8() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___m_ui_8)); }
	inline GameUI_t2125598246 * get_m_ui_8() const { return ___m_ui_8; }
	inline GameUI_t2125598246 ** get_address_of_m_ui_8() { return &___m_ui_8; }
	inline void set_m_ui_8(GameUI_t2125598246 * value)
	{
		___m_ui_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_ui_8, value);
	}

	inline static int32_t get_offset_of_m_currentMode_9() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___m_currentMode_9)); }
	inline Mode_t2403781 * get_m_currentMode_9() const { return ___m_currentMode_9; }
	inline Mode_t2403781 ** get_address_of_m_currentMode_9() { return &___m_currentMode_9; }
	inline void set_m_currentMode_9(Mode_t2403781 * value)
	{
		___m_currentMode_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_currentMode_9, value);
	}

	inline static int32_t get_offset_of_m_modes_10() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___m_modes_10)); }
	inline List_1_t799362748 * get_m_modes_10() const { return ___m_modes_10; }
	inline List_1_t799362748 ** get_address_of_m_modes_10() { return &___m_modes_10; }
	inline void set_m_modes_10(List_1_t799362748 * value)
	{
		___m_modes_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_modes_10, value);
	}

	inline static int32_t get_offset_of_m_touch_11() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___m_touch_11)); }
	inline GameTouch_t2994825805 * get_m_touch_11() const { return ___m_touch_11; }
	inline GameTouch_t2994825805 ** get_address_of_m_touch_11() { return &___m_touch_11; }
	inline void set_m_touch_11(GameTouch_t2994825805 * value)
	{
		___m_touch_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_touch_11, value);
	}

	inline static int32_t get_offset_of_m_frog_event_12() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614, ___m_frog_event_12)); }
	inline GameEvent_t2981166504 * get_m_frog_event_12() const { return ___m_frog_event_12; }
	inline GameEvent_t2981166504 ** get_address_of_m_frog_event_12() { return &___m_frog_event_12; }
	inline void set_m_frog_event_12(GameEvent_t2981166504 * value)
	{
		___m_frog_event_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_frog_event_12, value);
	}
};

struct GamePlay_t2590336614_StaticFields
{
public:
	// GamePlay GamePlay::instance
	GamePlay_t2590336614 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(GamePlay_t2590336614_StaticFields, ___instance_2)); }
	inline GamePlay_t2590336614 * get_instance_2() const { return ___instance_2; }
	inline GamePlay_t2590336614 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(GamePlay_t2590336614 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
