﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Axe
struct Axe_t66286;
// DataAxe
struct DataAxe_t3107820260;
// UnityEngine.Collider
struct Collider_t955670625;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DataAxe3107820260.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Axe::.ctor()
extern "C"  void Axe__ctor_m476662845 (Axe_t66286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Axe Axe::Alloc(DataAxe)
extern "C"  Axe_t66286 * Axe_Alloc_m2968844089 (Il2CppObject * __this /* static, unused */, DataAxe_t3107820260 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Axe::get_IsEnabled()
extern "C"  bool Axe_get_IsEnabled_m3084702109 (Axe_t66286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::Awake()
extern "C"  void Axe_Awake_m714268064 (Axe_t66286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::OnDestory()
extern "C"  void Axe_OnDestory_m2179453264 (Axe_t66286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::Init(DataAxe,System.Single,System.Single)
extern "C"  void Axe_Init_m633038913 (Axe_t66286 * __this, DataAxe_t3107820260 * ___data0, float ___width1, float ___power2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::Init(DataAxe,UnityEngine.Vector3)
extern "C"  void Axe_Init_m3101774794 (Axe_t66286 * __this, DataAxe_t3107820260 * ___data0, Vector3_t3525329789  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::InitData(DataAxe)
extern "C"  void Axe_InitData_m801913389 (Axe_t66286 * __this, DataAxe_t3107820260 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::InitPositionByWidth(System.Single)
extern "C"  void Axe_InitPositionByWidth_m2638844890 (Axe_t66286 * __this, float ___width0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::InitPositionByPosition(System.Single)
extern "C"  void Axe_InitPositionByPosition_m3253655083 (Axe_t66286 * __this, float ___position_x0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::InitVelocityByPower(System.Single)
extern "C"  void Axe_InitVelocityByPower_m3963918191 (Axe_t66286 * __this, float ___power0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::InitVelocityByDistance(System.Single)
extern "C"  void Axe_InitVelocityByDistance_m2030215339 (Axe_t66286 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::InitRotate(System.Single)
extern "C"  void Axe_InitRotate_m3977709113 (Axe_t66286 * __this, float ___velocity_z0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::Update()
extern "C"  void Axe_Update_m3618508400 (Axe_t66286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::UpdateSimplePhysics()
extern "C"  void Axe_UpdateSimplePhysics_m2236081863 (Axe_t66286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::UpdateNormalPhysics()
extern "C"  void Axe_UpdateNormalPhysics_m3906610994 (Axe_t66286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::OnCollisionWater()
extern "C"  void Axe_OnCollisionWater_m255246379 (Axe_t66286 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Axe_OnTriggerEnter_m1508574299 (Axe_t66286 * __this, Collider_t955670625 * ___coll0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Axe::Hit(UnityEngine.GameObject)
extern "C"  void Axe_Hit_m2761954214 (Axe_t66286 * __this, GameObject_t4012695102 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
