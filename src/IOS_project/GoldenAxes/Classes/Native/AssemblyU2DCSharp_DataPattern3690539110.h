﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<DataObject>
struct List_1_t3936031906;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataPattern
struct  DataPattern_t3690539110  : public Il2CppObject
{
public:
	// System.UInt32 DataPattern::id
	uint32_t ___id_0;
	// System.String DataPattern::move
	String_t* ___move_1;
	// System.Single DataPattern::idle
	float ___idle_2;
	// System.Single DataPattern::speed
	float ___speed_3;
	// System.Collections.Generic.List`1<DataObject> DataPattern::objects
	List_1_t3936031906 * ___objects_4;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(DataPattern_t3690539110, ___id_0)); }
	inline uint32_t get_id_0() const { return ___id_0; }
	inline uint32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_move_1() { return static_cast<int32_t>(offsetof(DataPattern_t3690539110, ___move_1)); }
	inline String_t* get_move_1() const { return ___move_1; }
	inline String_t** get_address_of_move_1() { return &___move_1; }
	inline void set_move_1(String_t* value)
	{
		___move_1 = value;
		Il2CppCodeGenWriteBarrier(&___move_1, value);
	}

	inline static int32_t get_offset_of_idle_2() { return static_cast<int32_t>(offsetof(DataPattern_t3690539110, ___idle_2)); }
	inline float get_idle_2() const { return ___idle_2; }
	inline float* get_address_of_idle_2() { return &___idle_2; }
	inline void set_idle_2(float value)
	{
		___idle_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(DataPattern_t3690539110, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_objects_4() { return static_cast<int32_t>(offsetof(DataPattern_t3690539110, ___objects_4)); }
	inline List_1_t3936031906 * get_objects_4() const { return ___objects_4; }
	inline List_1_t3936031906 ** get_address_of_objects_4() { return &___objects_4; }
	inline void set_objects_4(List_1_t3936031906 * value)
	{
		___objects_4 = value;
		Il2CppCodeGenWriteBarrier(&___objects_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
