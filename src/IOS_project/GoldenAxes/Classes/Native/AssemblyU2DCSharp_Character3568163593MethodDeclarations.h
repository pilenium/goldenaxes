﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Character
struct Character_t3568163593;
// Pattern
struct Pattern_t873562992;
// Move
struct Move_t2404337;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Move2404337.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharp_Pattern873562992.h"

// System.Void Character::.ctor()
extern "C"  void Character__ctor_m90739842 (Character_t3568163593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pattern Character::get_pattern()
extern "C"  Pattern_t873562992 * Character_get_pattern_m1274480394 (Character_t3568163593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Character::Awake()
extern "C"  void Character_Awake_m328345061 (Character_t3568163593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Character::OnDestroy()
extern "C"  void Character_OnDestroy_m3455604859 (Character_t3568163593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Character::Update()
extern "C"  void Character_Update_m244829899 (Character_t3568163593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Character::SetMove(Move)
extern "C"  void Character_SetMove_m4254943650 (Character_t3568163593 * __this, Move_t2404337 * ___move0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Character::OnEndMove()
extern "C"  void Character_OnEndMove_m2901770061 (Character_t3568163593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Character::UpdateMove(UnityEngine.Vector3)
extern "C"  void Character_UpdateMove_m3102545117 (Character_t3568163593 * __this, Vector3_t3525329789  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Character::SetPattern(Pattern)
extern "C"  void Character_SetPattern_m3083031538 (Character_t3568163593 * __this, Pattern_t873562992 * ___pattern0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
