﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModeFever
struct ModeFever_t4253717121;

#include "codegen/il2cpp-codegen.h"

// System.Void ModeFever::.ctor()
extern "C"  void ModeFever__ctor_m374785034 (ModeFever_t4253717121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeFever::Init()
extern "C"  void ModeFever_Init_m1349099786 (ModeFever_t4253717121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeFever::Clear()
extern "C"  void ModeFever_Clear_m2075885621 (ModeFever_t4253717121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeFever::Update(System.Single)
extern "C"  void ModeFever_Update_m3536401992 (ModeFever_t4253717121 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeFever::HitWater()
extern "C"  void ModeFever_HitWater_m3586875326 (ModeFever_t4253717121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeFever::HitDiver()
extern "C"  void ModeFever_HitDiver_m4135054821 (ModeFever_t4253717121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeFever::PatternSuccess()
extern "C"  void ModeFever_PatternSuccess_m2278891085 (ModeFever_t4253717121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeFever::PatternFail()
extern "C"  void ModeFever_PatternFail_m1984668470 (ModeFever_t4253717121 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
