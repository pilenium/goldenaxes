﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.Animation
struct Animation_t350396337;
// UnityEngine.BoxCollider
struct BoxCollider_t131631884;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;
// EffectWaveSystem
struct EffectWaveSystem_t2871388953;
// MonsterProgress
struct MonsterProgress_t2391190535;
// DataMonster
struct DataMonster_t1423279280;

#include "AssemblyU2DCSharp_Character3568163593.h"
#include "AssemblyU2DCSharp_MONSTER_STATE3983724460.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Monster
struct  Monster_t2901270458  : public Character_t3568163593
{
public:
	// UnityEngine.GameObject Monster::m_monster
	GameObject_t4012695102 * ___m_monster_5;
	// UnityEngine.Transform Monster::m_monster_tr
	Transform_t284553113 * ___m_monster_tr_6;
	// UnityEngine.Animation Monster::m_monster_ani
	Animation_t350396337 * ___m_monster_ani_7;
	// UnityEngine.BoxCollider Monster::m_monster_collider
	BoxCollider_t131631884 * ___m_monster_collider_8;
	// UnityEngine.Rigidbody Monster::m_rigidBody
	Rigidbody_t1972007546 * ___m_rigidBody_9;
	// EffectWaveSystem Monster::m_waveSystem
	EffectWaveSystem_t2871388953 * ___m_waveSystem_10;
	// MonsterProgress Monster::m_progress
	MonsterProgress_t2391190535 * ___m_progress_11;
	// DataMonster Monster::m_data
	DataMonster_t1423279280 * ___m_data_12;
	// MONSTER_STATE Monster::m_state
	int32_t ___m_state_13;
	// System.Int32 Monster::m_health
	int32_t ___m_health_14;

public:
	inline static int32_t get_offset_of_m_monster_5() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_monster_5)); }
	inline GameObject_t4012695102 * get_m_monster_5() const { return ___m_monster_5; }
	inline GameObject_t4012695102 ** get_address_of_m_monster_5() { return &___m_monster_5; }
	inline void set_m_monster_5(GameObject_t4012695102 * value)
	{
		___m_monster_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_monster_5, value);
	}

	inline static int32_t get_offset_of_m_monster_tr_6() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_monster_tr_6)); }
	inline Transform_t284553113 * get_m_monster_tr_6() const { return ___m_monster_tr_6; }
	inline Transform_t284553113 ** get_address_of_m_monster_tr_6() { return &___m_monster_tr_6; }
	inline void set_m_monster_tr_6(Transform_t284553113 * value)
	{
		___m_monster_tr_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_monster_tr_6, value);
	}

	inline static int32_t get_offset_of_m_monster_ani_7() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_monster_ani_7)); }
	inline Animation_t350396337 * get_m_monster_ani_7() const { return ___m_monster_ani_7; }
	inline Animation_t350396337 ** get_address_of_m_monster_ani_7() { return &___m_monster_ani_7; }
	inline void set_m_monster_ani_7(Animation_t350396337 * value)
	{
		___m_monster_ani_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_monster_ani_7, value);
	}

	inline static int32_t get_offset_of_m_monster_collider_8() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_monster_collider_8)); }
	inline BoxCollider_t131631884 * get_m_monster_collider_8() const { return ___m_monster_collider_8; }
	inline BoxCollider_t131631884 ** get_address_of_m_monster_collider_8() { return &___m_monster_collider_8; }
	inline void set_m_monster_collider_8(BoxCollider_t131631884 * value)
	{
		___m_monster_collider_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_monster_collider_8, value);
	}

	inline static int32_t get_offset_of_m_rigidBody_9() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_rigidBody_9)); }
	inline Rigidbody_t1972007546 * get_m_rigidBody_9() const { return ___m_rigidBody_9; }
	inline Rigidbody_t1972007546 ** get_address_of_m_rigidBody_9() { return &___m_rigidBody_9; }
	inline void set_m_rigidBody_9(Rigidbody_t1972007546 * value)
	{
		___m_rigidBody_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_rigidBody_9, value);
	}

	inline static int32_t get_offset_of_m_waveSystem_10() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_waveSystem_10)); }
	inline EffectWaveSystem_t2871388953 * get_m_waveSystem_10() const { return ___m_waveSystem_10; }
	inline EffectWaveSystem_t2871388953 ** get_address_of_m_waveSystem_10() { return &___m_waveSystem_10; }
	inline void set_m_waveSystem_10(EffectWaveSystem_t2871388953 * value)
	{
		___m_waveSystem_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_waveSystem_10, value);
	}

	inline static int32_t get_offset_of_m_progress_11() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_progress_11)); }
	inline MonsterProgress_t2391190535 * get_m_progress_11() const { return ___m_progress_11; }
	inline MonsterProgress_t2391190535 ** get_address_of_m_progress_11() { return &___m_progress_11; }
	inline void set_m_progress_11(MonsterProgress_t2391190535 * value)
	{
		___m_progress_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_progress_11, value);
	}

	inline static int32_t get_offset_of_m_data_12() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_data_12)); }
	inline DataMonster_t1423279280 * get_m_data_12() const { return ___m_data_12; }
	inline DataMonster_t1423279280 ** get_address_of_m_data_12() { return &___m_data_12; }
	inline void set_m_data_12(DataMonster_t1423279280 * value)
	{
		___m_data_12 = value;
		Il2CppCodeGenWriteBarrier(&___m_data_12, value);
	}

	inline static int32_t get_offset_of_m_state_13() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_state_13)); }
	inline int32_t get_m_state_13() const { return ___m_state_13; }
	inline int32_t* get_address_of_m_state_13() { return &___m_state_13; }
	inline void set_m_state_13(int32_t value)
	{
		___m_state_13 = value;
	}

	inline static int32_t get_offset_of_m_health_14() { return static_cast<int32_t>(offsetof(Monster_t2901270458, ___m_health_14)); }
	inline int32_t get_m_health_14() const { return ___m_health_14; }
	inline int32_t* get_address_of_m_health_14() { return &___m_health_14; }
	inline void set_m_health_14(int32_t value)
	{
		___m_health_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
