﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectCoin
struct EffectCoin_t3708782562;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void EffectCoin::.ctor()
extern "C"  void EffectCoin__ctor_m1662635769 (EffectCoin_t3708782562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoin::Awake()
extern "C"  void EffectCoin_Awake_m1900240988 (EffectCoin_t3708782562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoin::Update()
extern "C"  void EffectCoin_Update_m1728963380 (EffectCoin_t3708782562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoin::InitCoin(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void EffectCoin_InitCoin_m1372446272 (EffectCoin_t3708782562 * __this, int32_t ___coin0, float ___x1, float ___z2, float ___degree3, float ___power_h4, float ___power_v5, float ___gravity6, float ___delay7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoin::InitCoin(System.Int32,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void EffectCoin_InitCoin_m613784841 (EffectCoin_t3708782562 * __this, int32_t ___coin0, Vector3_t3525329789  ___position1, float ___degree2, float ___power_h3, float ___power_v4, float ___gravity5, float ___delay6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoin::OnCollisionWater()
extern "C"  void EffectCoin_OnCollisionWater_m2532732911 (EffectCoin_t3708782562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
