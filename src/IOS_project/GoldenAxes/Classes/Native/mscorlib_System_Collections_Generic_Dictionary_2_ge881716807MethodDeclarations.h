﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t881716807;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.LogType>
struct IEqualityComparer_1_t1558568806;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2995724695;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>[]
struct KeyValuePair_2U5BU5D_t3844584852;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>
struct IEnumerator_1_t1853354553;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1541724277;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.LogType,UnityEngine.Color>
struct ValueCollection_t2803853901;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2995724695.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCont986364934.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_370248105.h"
#include "mscorlib_System_Array2840145358.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En648744748.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::.ctor()
extern "C"  void Dictionary_2__ctor_m1420748223_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1420748223(__this, method) ((  void (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2__ctor_m1420748223_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3828555661_gshared (Dictionary_2_t881716807 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3828555661(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t881716807 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3828555661_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3962375271_gshared (Dictionary_2_t881716807 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3962375271(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t881716807 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3962375271_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2890824791_gshared (Dictionary_2_t881716807 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2890824791(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t881716807 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2__ctor_m2890824791_gshared)(__this, ___info0, ___context1, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m597681522_gshared (Dictionary_2_t881716807 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m597681522(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t881716807 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m597681522_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2501486807_gshared (Dictionary_2_t881716807 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2501486807(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t881716807 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2501486807_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3384683290_gshared (Dictionary_2_t881716807 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3384683290(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t881716807 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3384683290_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m911126805_gshared (Dictionary_2_t881716807 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m911126805(__this, ___key0, method) ((  void (*) (Dictionary_2_t881716807 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m911126805_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4248028856_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4248028856(__this, method) ((  bool (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4248028856_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4243151332_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4243151332(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4243151332_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3982452988_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3982452988(__this, method) ((  bool (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3982452988_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2289766187_gshared (Dictionary_2_t881716807 * __this, KeyValuePair_2_t370248105  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2289766187(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t881716807 *, KeyValuePair_2_t370248105 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2289766187_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1224606071_gshared (Dictionary_2_t881716807 * __this, KeyValuePair_2_t370248105  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1224606071(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t881716807 *, KeyValuePair_2_t370248105 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1224606071_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3559959119_gshared (Dictionary_2_t881716807 * __this, KeyValuePair_2U5BU5D_t3844584852* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3559959119(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t881716807 *, KeyValuePair_2U5BU5D_t3844584852*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3559959119_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2956728924_gshared (Dictionary_2_t881716807 * __this, KeyValuePair_2_t370248105  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2956728924(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t881716807 *, KeyValuePair_2_t370248105 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m2956728924_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2641711598_gshared (Dictionary_2_t881716807 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2641711598(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t881716807 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2641711598_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1663908841_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1663908841(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1663908841_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m7193446_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m7193446(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m7193446_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3188730625_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3188730625(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3188730625_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m266741630_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m266741630(__this, method) ((  int32_t (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_get_Count_m266741630_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::get_Item(TKey)
extern "C"  Color_t1588175760  Dictionary_2_get_Item_m2469066157_gshared (Dictionary_2_t881716807 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2469066157(__this, ___key0, method) ((  Color_t1588175760  (*) (Dictionary_2_t881716807 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2469066157_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3407486806_gshared (Dictionary_2_t881716807 * __this, int32_t ___key0, Color_t1588175760  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3407486806(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t881716807 *, int32_t, Color_t1588175760 , const MethodInfo*))Dictionary_2_set_Item_m3407486806_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m229765006_gshared (Dictionary_2_t881716807 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m229765006(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t881716807 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m229765006_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m4150510057_gshared (Dictionary_2_t881716807 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m4150510057(__this, ___size0, method) ((  void (*) (Dictionary_2_t881716807 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m4150510057_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3446968869_gshared (Dictionary_2_t881716807 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3446968869(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t881716807 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3446968869_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t370248105  Dictionary_2_make_pair_m1773729905_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Color_t1588175760  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1773729905(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t370248105  (*) (Il2CppObject * /* static, unused */, int32_t, Color_t1588175760 , const MethodInfo*))Dictionary_2_make_pair_m1773729905_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::pick_value(TKey,TValue)
extern "C"  Color_t1588175760  Dictionary_2_pick_value_m3536848229_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Color_t1588175760  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3536848229(__this /* static, unused */, ___key0, ___value1, method) ((  Color_t1588175760  (*) (Il2CppObject * /* static, unused */, int32_t, Color_t1588175760 , const MethodInfo*))Dictionary_2_pick_value_m3536848229_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1553519114_gshared (Dictionary_2_t881716807 * __this, KeyValuePair_2U5BU5D_t3844584852* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1553519114(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t881716807 *, KeyValuePair_2U5BU5D_t3844584852*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1553519114_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Resize()
extern "C"  void Dictionary_2_Resize_m121774818_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m121774818(__this, method) ((  void (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_Resize_m121774818_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m2523866463_gshared (Dictionary_2_t881716807 * __this, int32_t ___key0, Color_t1588175760  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m2523866463(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t881716807 *, int32_t, Color_t1588175760 , const MethodInfo*))Dictionary_2_Add_m2523866463_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Clear()
extern "C"  void Dictionary_2_Clear_m179143745_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m179143745(__this, method) ((  void (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_Clear_m179143745_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3913959015_gshared (Dictionary_2_t881716807 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3913959015(__this, ___key0, method) ((  bool (*) (Dictionary_2_t881716807 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m3913959015_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m146673383_gshared (Dictionary_2_t881716807 * __this, Color_t1588175760  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m146673383(__this, ___value0, method) ((  bool (*) (Dictionary_2_t881716807 *, Color_t1588175760 , const MethodInfo*))Dictionary_2_ContainsValue_m146673383_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2274720948_gshared (Dictionary_2_t881716807 * __this, SerializationInfo_t2995724695 * ___info0, StreamingContext_t986364934  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2274720948(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t881716807 *, SerializationInfo_t2995724695 *, StreamingContext_t986364934 , const MethodInfo*))Dictionary_2_GetObjectData_m2274720948_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3914939952_gshared (Dictionary_2_t881716807 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3914939952(__this, ___sender0, method) ((  void (*) (Dictionary_2_t881716807 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3914939952_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2206158377_gshared (Dictionary_2_t881716807 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2206158377(__this, ___key0, method) ((  bool (*) (Dictionary_2_t881716807 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2206158377_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1824081856_gshared (Dictionary_2_t881716807 * __this, int32_t ___key0, Color_t1588175760 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1824081856(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t881716807 *, int32_t, Color_t1588175760 *, const MethodInfo*))Dictionary_2_TryGetValue_m1824081856_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::get_Values()
extern "C"  ValueCollection_t2803853901 * Dictionary_2_get_Values_m3457783423_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3457783423(__this, method) ((  ValueCollection_t2803853901 * (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_get_Values_m3457783423_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m663128512_gshared (Dictionary_2_t881716807 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m663128512(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t881716807 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m663128512_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ToTValue(System.Object)
extern "C"  Color_t1588175760  Dictionary_2_ToTValue_m1849162368_gshared (Dictionary_2_t881716807 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1849162368(__this, ___value0, method) ((  Color_t1588175760  (*) (Dictionary_2_t881716807 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1849162368_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m400208908_gshared (Dictionary_2_t881716807 * __this, KeyValuePair_2_t370248105  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m400208908(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t881716807 *, KeyValuePair_2_t370248105 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m400208908_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::GetEnumerator()
extern "C"  Enumerator_t648744749  Dictionary_2_GetEnumerator_m916379099_gshared (Dictionary_2_t881716807 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m916379099(__this, method) ((  Enumerator_t648744749  (*) (Dictionary_2_t881716807 *, const MethodInfo*))Dictionary_2_GetEnumerator_m916379099_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t130027246  Dictionary_2_U3CCopyToU3Em__0_m3304845674_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Color_t1588175760  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3304845674(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t130027246  (*) (Il2CppObject * /* static, unused */, int32_t, Color_t1588175760 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3304845674_gshared)(__this /* static, unused */, ___key0, ___value1, method)
