﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stage/<RemoveWaveEffect>c__AnonStoreyB
struct U3CRemoveWaveEffectU3Ec__AnonStoreyB_t1841059887;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Stage/<RemoveWaveEffect>c__AnonStoreyB::.ctor()
extern "C"  void U3CRemoveWaveEffectU3Ec__AnonStoreyB__ctor_m1600622427 (U3CRemoveWaveEffectU3Ec__AnonStoreyB_t1841059887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stage/<RemoveWaveEffect>c__AnonStoreyB::<>m__C(UnityEngine.GameObject)
extern "C"  bool U3CRemoveWaveEffectU3Ec__AnonStoreyB_U3CU3Em__C_m2255572525 (U3CRemoveWaveEffectU3Ec__AnonStoreyB_t1841059887 * __this, GameObject_t4012695102 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
