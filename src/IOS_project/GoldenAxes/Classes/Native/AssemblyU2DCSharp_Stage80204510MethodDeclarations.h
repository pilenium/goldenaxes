﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stage
struct Stage_t80204510;
// DataChapter
struct DataChapter_t925677859;
// DataStage
struct DataStage_t1629502804;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t514686775;
// Axe
struct Axe_t66286;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharp_Axe66286.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Stage::.ctor()
extern "C"  void Stage__ctor_m2844414029 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Stage::get_stage()
extern "C"  int32_t Stage_get_stage_m4042735278 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataChapter Stage::get_dataChapter()
extern "C"  DataChapter_t925677859 * Stage_get_dataChapter_m2108362549 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataStage Stage::get_dataStage()
extern "C"  DataStage_t1629502804 * Stage_get_dataStage_m3617598165 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Stage::get_speed()
extern "C"  float Stage_get_speed_m3260634357 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Stage::get_distance()
extern "C"  float Stage_get_distance_m35557161 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Stage::get_deltaDistance()
extern "C"  float Stage_get_deltaDistance_m210846203 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Stage::get_axeCount()
extern "C"  int32_t Stage_get_axeCount_m3701595859 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::Awake()
extern "C"  void Stage_Awake_m3082019248 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::OnDestroy()
extern "C"  void Stage_OnDestroy_m3488691910 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::ClearGameObjectList(System.Collections.Generic.List`1<UnityEngine.GameObject>,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern "C"  void Stage_ClearGameObjectList_m1900825639 (Stage_t80204510 * __this, List_1_t514686775 * ___list0, List_1_t514686775 * ___secon_list1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::InitCamera()
extern "C"  void Stage_InitCamera_m2386906604 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stage::SetChapter(System.Int32)
extern "C"  bool Stage_SetChapter_m714039999 (Stage_t80204510 * __this, int32_t ___chapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stage::SetStage(System.Int32)
extern "C"  bool Stage_SetStage_m1916811184 (Stage_t80204510 * __this, int32_t ___stage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::SetSpeed(System.Single)
extern "C"  void Stage_SetSpeed_m1492055823 (Stage_t80204510 * __this, float ___speed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stage::CheckTile()
extern "C"  bool Stage_CheckTile_m3999392885 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::AddAxe(System.Single,System.Single)
extern "C"  void Stage_AddAxe_m3412358540 (Stage_t80204510 * __this, float ___width0, float ___power1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::AddAxe(UnityEngine.Vector3)
extern "C"  void Stage_AddAxe_m2005261333 (Stage_t80204510 * __this, Vector3_t3525329789  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stage::RemoveAxe(Axe)
extern "C"  bool Stage_RemoveAxe_m300927899 (Stage_t80204510 * __this, Axe_t66286 * ___axe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Stage::AddEffect(UnityEngine.Vector3,System.String,System.Boolean)
extern "C"  GameObject_t4012695102 * Stage_AddEffect_m3322959738 (Stage_t80204510 * __this, Vector3_t3525329789  ___position0, String_t* ___url1, bool ___scroll2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stage::RemoveEffect(UnityEngine.GameObject)
extern "C"  bool Stage_RemoveEffect_m557265552 (Stage_t80204510 * __this, GameObject_t4012695102 * ___effect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::AddWaveEffect(UnityEngine.Vector3)
extern "C"  void Stage_AddWaveEffect_m3403269635 (Stage_t80204510 * __this, Vector3_t3525329789  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stage::RemoveWaveEffect(UnityEngine.GameObject)
extern "C"  bool Stage_RemoveWaveEffect_m1788609769 (Stage_t80204510 * __this, GameObject_t4012695102 * ___effect0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::AddCoinEffect(System.Int32,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Stage_AddCoinEffect_m1261382338 (Stage_t80204510 * __this, int32_t ___coin0, float ___x1, float ___z2, float ___degree3, float ___power_h4, float ___power_v5, float ___gravity6, float ___delay7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::AddCoinEffect(System.Int32,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  void Stage_AddCoinEffect_m2181335947 (Stage_t80204510 * __this, int32_t ___coin0, Vector3_t3525329789  ___position1, float ___degree2, float ___power_h3, float ___power_v4, float ___gravity5, float ___delay6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::AddCoinTextEffect(System.Int32,UnityEngine.Vector3)
extern "C"  void Stage_AddCoinTextEffect_m2830881887 (Stage_t80204510 * __this, int32_t ___coin0, Vector3_t3525329789  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::Update()
extern "C"  void Stage_Update_m4004351072 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::UpdateMove()
extern "C"  void Stage_UpdateMove_m3448061073 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Stage::CheckDelete()
extern "C"  void Stage_CheckDelete_m3002516990 (Stage_t80204510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
