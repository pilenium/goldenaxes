﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>
struct U3CGetEnumeratorU3Ec__Iterator3_t1278240806;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry2194697103.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m3718098024_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3__ctor_m3718098024(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3__ctor_m3718098024_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  DepthEntry_t2194697103  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1321247023_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1321247023(__this, method) ((  DepthEntry_t2194697103  (*) (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1321247023_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m195183166_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m195183166(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m195183166_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m1863043980_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m1863043980(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m1863043980_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3872661157_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3872661157(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3872661157_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UICamera/DepthEntry>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1364530965_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Reset_m1364530965(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t1278240806 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Reset_m1364530965_gshared)(__this, method)
