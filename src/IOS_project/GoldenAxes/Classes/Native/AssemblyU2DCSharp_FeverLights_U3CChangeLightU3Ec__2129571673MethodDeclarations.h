﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FeverLights/<ChangeLight>c__Iterator6
struct U3CChangeLightU3Ec__Iterator6_t2129571673;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void FeverLights/<ChangeLight>c__Iterator6::.ctor()
extern "C"  void U3CChangeLightU3Ec__Iterator6__ctor_m4196669792 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FeverLights/<ChangeLight>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CChangeLightU3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4109466034 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object FeverLights/<ChangeLight>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CChangeLightU3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m1004609862 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FeverLights/<ChangeLight>c__Iterator6::MoveNext()
extern "C"  bool U3CChangeLightU3Ec__Iterator6_MoveNext_m2415804308 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights/<ChangeLight>c__Iterator6::Dispose()
extern "C"  void U3CChangeLightU3Ec__Iterator6_Dispose_m4218629533 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights/<ChangeLight>c__Iterator6::Reset()
extern "C"  void U3CChangeLightU3Ec__Iterator6_Reset_m1843102733 (U3CChangeLightU3Ec__Iterator6_t2129571673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
