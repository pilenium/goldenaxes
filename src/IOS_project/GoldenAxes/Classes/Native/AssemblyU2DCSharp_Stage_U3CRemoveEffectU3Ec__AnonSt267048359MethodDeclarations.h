﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Stage/<RemoveEffect>c__AnonStoreyA
struct U3CRemoveEffectU3Ec__AnonStoreyA_t267048359;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void Stage/<RemoveEffect>c__AnonStoreyA::.ctor()
extern "C"  void U3CRemoveEffectU3Ec__AnonStoreyA__ctor_m266069091 (U3CRemoveEffectU3Ec__AnonStoreyA_t267048359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Stage/<RemoveEffect>c__AnonStoreyA::<>m__B(UnityEngine.GameObject)
extern "C"  bool U3CRemoveEffectU3Ec__AnonStoreyA_U3CU3Em__B_m2650897700 (U3CRemoveEffectU3Ec__AnonStoreyA_t267048359 * __this, GameObject_t4012695102 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
