﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>
struct U3CGetEnumeratorU3Ec__Iterator3_t1930958490;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m2857828086_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3__ctor_m2857828086(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3__ctor_m2857828086_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  int32_t U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m163175455_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m163175455(__this, method) ((  int32_t (*) (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m163175455_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1728905850_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1728905850(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1728905850_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m1867136038_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m1867136038(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m1867136038_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m1786971571_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Dispose_m1786971571(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Dispose_m1786971571_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<System.Int32>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m504261027_gshared (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Reset_m504261027(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t1930958490 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Reset_m504261027_gshared)(__this, method)
