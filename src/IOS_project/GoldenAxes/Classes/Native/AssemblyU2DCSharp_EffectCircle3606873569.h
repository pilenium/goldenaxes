﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Renderer
struct Renderer_t1092684080;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectCircle
struct  EffectCircle_t3606873569  : public MonoBehaviour_t3012272455
{
public:
	// System.Single EffectCircle::range
	float ___range_2;
	// System.Single EffectCircle::speed1
	float ___speed1_3;
	// System.Single EffectCircle::speed2
	float ___speed2_4;
	// System.Single EffectCircle::speed3
	float ___speed3_5;
	// System.Single EffectCircle::amplitude1
	float ___amplitude1_6;
	// System.Single EffectCircle::amplitude2
	float ___amplitude2_7;
	// System.Single EffectCircle::amplitude3
	float ___amplitude3_8;
	// UnityEngine.Renderer EffectCircle::m_renderer
	Renderer_t1092684080 * ___m_renderer_9;

public:
	inline static int32_t get_offset_of_range_2() { return static_cast<int32_t>(offsetof(EffectCircle_t3606873569, ___range_2)); }
	inline float get_range_2() const { return ___range_2; }
	inline float* get_address_of_range_2() { return &___range_2; }
	inline void set_range_2(float value)
	{
		___range_2 = value;
	}

	inline static int32_t get_offset_of_speed1_3() { return static_cast<int32_t>(offsetof(EffectCircle_t3606873569, ___speed1_3)); }
	inline float get_speed1_3() const { return ___speed1_3; }
	inline float* get_address_of_speed1_3() { return &___speed1_3; }
	inline void set_speed1_3(float value)
	{
		___speed1_3 = value;
	}

	inline static int32_t get_offset_of_speed2_4() { return static_cast<int32_t>(offsetof(EffectCircle_t3606873569, ___speed2_4)); }
	inline float get_speed2_4() const { return ___speed2_4; }
	inline float* get_address_of_speed2_4() { return &___speed2_4; }
	inline void set_speed2_4(float value)
	{
		___speed2_4 = value;
	}

	inline static int32_t get_offset_of_speed3_5() { return static_cast<int32_t>(offsetof(EffectCircle_t3606873569, ___speed3_5)); }
	inline float get_speed3_5() const { return ___speed3_5; }
	inline float* get_address_of_speed3_5() { return &___speed3_5; }
	inline void set_speed3_5(float value)
	{
		___speed3_5 = value;
	}

	inline static int32_t get_offset_of_amplitude1_6() { return static_cast<int32_t>(offsetof(EffectCircle_t3606873569, ___amplitude1_6)); }
	inline float get_amplitude1_6() const { return ___amplitude1_6; }
	inline float* get_address_of_amplitude1_6() { return &___amplitude1_6; }
	inline void set_amplitude1_6(float value)
	{
		___amplitude1_6 = value;
	}

	inline static int32_t get_offset_of_amplitude2_7() { return static_cast<int32_t>(offsetof(EffectCircle_t3606873569, ___amplitude2_7)); }
	inline float get_amplitude2_7() const { return ___amplitude2_7; }
	inline float* get_address_of_amplitude2_7() { return &___amplitude2_7; }
	inline void set_amplitude2_7(float value)
	{
		___amplitude2_7 = value;
	}

	inline static int32_t get_offset_of_amplitude3_8() { return static_cast<int32_t>(offsetof(EffectCircle_t3606873569, ___amplitude3_8)); }
	inline float get_amplitude3_8() const { return ___amplitude3_8; }
	inline float* get_address_of_amplitude3_8() { return &___amplitude3_8; }
	inline void set_amplitude3_8(float value)
	{
		___amplitude3_8 = value;
	}

	inline static int32_t get_offset_of_m_renderer_9() { return static_cast<int32_t>(offsetof(EffectCircle_t3606873569, ___m_renderer_9)); }
	inline Renderer_t1092684080 * get_m_renderer_9() const { return ___m_renderer_9; }
	inline Renderer_t1092684080 ** get_address_of_m_renderer_9() { return &___m_renderer_9; }
	inline void set_m_renderer_9(Renderer_t1092684080 * value)
	{
		___m_renderer_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_renderer_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
