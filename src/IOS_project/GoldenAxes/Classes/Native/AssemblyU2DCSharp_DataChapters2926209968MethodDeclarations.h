﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataChapters
struct DataChapters_t2926209968;
// DataChapter
struct DataChapter_t925677859;

#include "codegen/il2cpp-codegen.h"

// System.Void DataChapters::.ctor()
extern "C"  void DataChapters__ctor_m358853867 (DataChapters_t2926209968 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataChapter DataChapters::GetChapterData(System.Int32)
extern "C"  DataChapter_t925677859 * DataChapters_GetChapterData_m2981922363 (DataChapters_t2926209968 * __this, int32_t ___chapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
