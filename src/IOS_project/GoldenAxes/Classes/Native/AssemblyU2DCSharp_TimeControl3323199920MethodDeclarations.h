﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TimeControl
struct TimeControl_t3323199920;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void TimeControl::.ctor()
extern "C"  void TimeControl__ctor_m1602641851 (TimeControl_t3323199920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeControl::Awake()
extern "C"  void TimeControl_Awake_m1840247070 (TimeControl_t3323199920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeControl::OnDestroy()
extern "C"  void TimeControl_OnDestroy_m22798132 (TimeControl_t3323199920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeControl::Update()
extern "C"  void TimeControl_Update_m4164119218 (TimeControl_t3323199920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeControl::StartTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void TimeControl_StartTouch_m3849079583 (TimeControl_t3323199920 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeControl::MovedTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void TimeControl_MovedTouch_m2118249006 (TimeControl_t3323199920 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeControl::EndTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void TimeControl_EndTouch_m4040478982 (TimeControl_t3323199920 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TimeControl::CanceldTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void TimeControl_CanceldTouch_m2127341399 (TimeControl_t3323199920 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
