﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TouchControl
struct TouchControl_t3773144382;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void TouchControl::.ctor()
extern "C"  void TouchControl__ctor_m212622109 (TouchControl_t3773144382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchControl::Awake()
extern "C"  void TouchControl_Awake_m450227328 (TouchControl_t3773144382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchControl::OnDestroy()
extern "C"  void TouchControl_OnDestroy_m1785813398 (TouchControl_t3773144382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchControl::Update()
extern "C"  void TouchControl_Update_m4023180176 (TouchControl_t3773144382 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchControl::StartTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void TouchControl_StartTouch_m2555742717 (TouchControl_t3773144382 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchControl::MovedTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void TouchControl_MovedTouch_m824912140 (TouchControl_t3773144382 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchControl::EndTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void TouchControl_EndTouch_m1478242148 (TouchControl_t3773144382 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TouchControl::CanceldTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void TouchControl_CanceldTouch_m476161717 (TouchControl_t3773144382 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
