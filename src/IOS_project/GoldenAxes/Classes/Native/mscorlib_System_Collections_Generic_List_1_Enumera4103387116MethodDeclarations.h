﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<DataChapter>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1387648330(__this, ___l0, method) ((  void (*) (Enumerator_t4103387116 *, List_1_t1722636828 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataChapter>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4282882248(__this, method) ((  void (*) (Enumerator_t4103387116 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<DataChapter>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2625991348(__this, method) ((  Il2CppObject * (*) (Enumerator_t4103387116 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataChapter>::Dispose()
#define Enumerator_Dispose_m2268911023(__this, method) ((  void (*) (Enumerator_t4103387116 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<DataChapter>::VerifyState()
#define Enumerator_VerifyState_m3047171432(__this, method) ((  void (*) (Enumerator_t4103387116 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<DataChapter>::MoveNext()
#define Enumerator_MoveNext_m3070894507(__this, method) ((  bool (*) (Enumerator_t4103387116 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<DataChapter>::get_Current()
#define Enumerator_get_Current_m2627363589(__this, method) ((  DataChapter_t925677859 * (*) (Enumerator_t4103387116 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
