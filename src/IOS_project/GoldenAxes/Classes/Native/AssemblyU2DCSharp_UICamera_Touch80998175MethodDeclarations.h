﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/Touch
struct Touch_t80998175;

#include "codegen/il2cpp-codegen.h"

// System.Void UICamera/Touch::.ctor()
extern "C"  void Touch__ctor_m1432635986 (Touch_t80998175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
