﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataNormalMode
struct DataNormalMode_t1907108500;

#include "AssemblyU2DCSharp_Mode2403779.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModeNormal
struct  ModeNormal_t3254368394  : public Mode_t2403781
{
public:
	// System.Single ModeNormal::m_stack
	float ___m_stack_1;
	// DataNormalMode ModeNormal::m_data
	DataNormalMode_t1907108500 * ___m_data_2;
	// System.Single ModeNormal::m_distance
	float ___m_distance_3;

public:
	inline static int32_t get_offset_of_m_stack_1() { return static_cast<int32_t>(offsetof(ModeNormal_t3254368394, ___m_stack_1)); }
	inline float get_m_stack_1() const { return ___m_stack_1; }
	inline float* get_address_of_m_stack_1() { return &___m_stack_1; }
	inline void set_m_stack_1(float value)
	{
		___m_stack_1 = value;
	}

	inline static int32_t get_offset_of_m_data_2() { return static_cast<int32_t>(offsetof(ModeNormal_t3254368394, ___m_data_2)); }
	inline DataNormalMode_t1907108500 * get_m_data_2() const { return ___m_data_2; }
	inline DataNormalMode_t1907108500 ** get_address_of_m_data_2() { return &___m_data_2; }
	inline void set_m_data_2(DataNormalMode_t1907108500 * value)
	{
		___m_data_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_data_2, value);
	}

	inline static int32_t get_offset_of_m_distance_3() { return static_cast<int32_t>(offsetof(ModeNormal_t3254368394, ___m_distance_3)); }
	inline float get_m_distance_3() const { return ___m_distance_3; }
	inline float* get_address_of_m_distance_3() { return &___m_distance_3; }
	inline void set_m_distance_3(float value)
	{
		___m_distance_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
