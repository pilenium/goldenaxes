﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BetterList`1<UICamera>
struct BetterList_1_t1686332965;
// UICamera/GetKeyStateFunc
struct GetKeyStateFunc_t2362323372;
// UICamera/GetAxisFunc
struct GetAxisFunc_t3240405851;
// UICamera/GetAnyKeyFunc
struct GetAnyKeyFunc_t2938188301;
// UICamera/OnScreenResize
struct OnScreenResize_t3539115999;
// System.String
struct String_t;
// UICamera/OnCustomInput
struct OnCustomInput_t3775009626;
// UICamera
struct UICamera_t189364953;
// UnityEngine.Camera
struct Camera_t3533968274;
// UICamera/OnSchemeChange
struct OnSchemeChange_t2442067284;
// UICamera/MouseOrTouch
struct MouseOrTouch_t908473047;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UICamera/VoidDelegate
struct VoidDelegate_t1986138970;
// UICamera/BoolDelegate
struct BoolDelegate_t945637040;
// UICamera/FloatDelegate
struct FloatDelegate_t4185225186;
// UICamera/VectorDelegate
struct VectorDelegate_t2458450953;
// UICamera/ObjectDelegate
struct ObjectDelegate_t3413901829;
// UICamera/KeyCodeDelegate
struct KeyCodeDelegate_t3737044594;
// UICamera/MoveDelegate
struct MoveDelegate_t757231766;
// UICamera/MouseOrTouch[]
struct MouseOrTouchU5BU5D_t2832174062;
// System.Collections.Generic.List`1<UICamera/MouseOrTouch>
struct List_1_t1705432016;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3644373756;
// BetterList`1<UICamera/DepthEntry>
struct BetterList_1_t3691665115;
// UICamera/GetTouchCountCallback
struct GetTouchCountCallback_t345790283;
// UICamera/GetTouchCallback
struct GetTouchCallback_t2933503246;
// BetterList`1/CompareFunc<UICamera/DepthEntry>
struct CompareFunc_t41355861;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "AssemblyU2DCSharp_UICamera_EventType2035398868.h"
#include "UnityEngine_UnityEngine_LayerMask1862190090.h"
#include "AssemblyU2DCSharp_UICamera_ProcessEventsIn943481741.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_RaycastHit46221527.h"
#include "AssemblyU2DCSharp_UICamera_ControlScheme1667267906.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry2194697103.h"
#include "UnityEngine_UnityEngine_Plane1600081545.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UICamera
struct  UICamera_t189364953  : public MonoBehaviour_t3012272455
{
public:
	// UICamera/EventType UICamera::eventType
	int32_t ___eventType_9;
	// System.Boolean UICamera::eventsGoToColliders
	bool ___eventsGoToColliders_10;
	// UnityEngine.LayerMask UICamera::eventReceiverMask
	LayerMask_t1862190090  ___eventReceiverMask_11;
	// UICamera/ProcessEventsIn UICamera::processEventsIn
	int32_t ___processEventsIn_12;
	// System.Boolean UICamera::debug
	bool ___debug_13;
	// System.Boolean UICamera::useMouse
	bool ___useMouse_14;
	// System.Boolean UICamera::useTouch
	bool ___useTouch_15;
	// System.Boolean UICamera::allowMultiTouch
	bool ___allowMultiTouch_16;
	// System.Boolean UICamera::useKeyboard
	bool ___useKeyboard_17;
	// System.Boolean UICamera::useController
	bool ___useController_18;
	// System.Boolean UICamera::stickyTooltip
	bool ___stickyTooltip_19;
	// System.Single UICamera::tooltipDelay
	float ___tooltipDelay_20;
	// System.Boolean UICamera::longPressTooltip
	bool ___longPressTooltip_21;
	// System.Single UICamera::mouseDragThreshold
	float ___mouseDragThreshold_22;
	// System.Single UICamera::mouseClickThreshold
	float ___mouseClickThreshold_23;
	// System.Single UICamera::touchDragThreshold
	float ___touchDragThreshold_24;
	// System.Single UICamera::touchClickThreshold
	float ___touchClickThreshold_25;
	// System.Single UICamera::rangeDistance
	float ___rangeDistance_26;
	// System.String UICamera::horizontalAxisName
	String_t* ___horizontalAxisName_27;
	// System.String UICamera::verticalAxisName
	String_t* ___verticalAxisName_28;
	// System.String UICamera::horizontalPanAxisName
	String_t* ___horizontalPanAxisName_29;
	// System.String UICamera::verticalPanAxisName
	String_t* ___verticalPanAxisName_30;
	// System.String UICamera::scrollAxisName
	String_t* ___scrollAxisName_31;
	// System.Boolean UICamera::commandClick
	bool ___commandClick_32;
	// UnityEngine.KeyCode UICamera::submitKey0
	int32_t ___submitKey0_33;
	// UnityEngine.KeyCode UICamera::submitKey1
	int32_t ___submitKey1_34;
	// UnityEngine.KeyCode UICamera::cancelKey0
	int32_t ___cancelKey0_35;
	// UnityEngine.KeyCode UICamera::cancelKey1
	int32_t ___cancelKey1_36;
	// System.Boolean UICamera::autoHideCursor
	bool ___autoHideCursor_37;
	// UnityEngine.Camera UICamera::mCam
	Camera_t3533968274 * ___mCam_79;
	// System.Single UICamera::mNextRaycast
	float ___mNextRaycast_81;

public:
	inline static int32_t get_offset_of_eventType_9() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___eventType_9)); }
	inline int32_t get_eventType_9() const { return ___eventType_9; }
	inline int32_t* get_address_of_eventType_9() { return &___eventType_9; }
	inline void set_eventType_9(int32_t value)
	{
		___eventType_9 = value;
	}

	inline static int32_t get_offset_of_eventsGoToColliders_10() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___eventsGoToColliders_10)); }
	inline bool get_eventsGoToColliders_10() const { return ___eventsGoToColliders_10; }
	inline bool* get_address_of_eventsGoToColliders_10() { return &___eventsGoToColliders_10; }
	inline void set_eventsGoToColliders_10(bool value)
	{
		___eventsGoToColliders_10 = value;
	}

	inline static int32_t get_offset_of_eventReceiverMask_11() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___eventReceiverMask_11)); }
	inline LayerMask_t1862190090  get_eventReceiverMask_11() const { return ___eventReceiverMask_11; }
	inline LayerMask_t1862190090 * get_address_of_eventReceiverMask_11() { return &___eventReceiverMask_11; }
	inline void set_eventReceiverMask_11(LayerMask_t1862190090  value)
	{
		___eventReceiverMask_11 = value;
	}

	inline static int32_t get_offset_of_processEventsIn_12() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___processEventsIn_12)); }
	inline int32_t get_processEventsIn_12() const { return ___processEventsIn_12; }
	inline int32_t* get_address_of_processEventsIn_12() { return &___processEventsIn_12; }
	inline void set_processEventsIn_12(int32_t value)
	{
		___processEventsIn_12 = value;
	}

	inline static int32_t get_offset_of_debug_13() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___debug_13)); }
	inline bool get_debug_13() const { return ___debug_13; }
	inline bool* get_address_of_debug_13() { return &___debug_13; }
	inline void set_debug_13(bool value)
	{
		___debug_13 = value;
	}

	inline static int32_t get_offset_of_useMouse_14() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useMouse_14)); }
	inline bool get_useMouse_14() const { return ___useMouse_14; }
	inline bool* get_address_of_useMouse_14() { return &___useMouse_14; }
	inline void set_useMouse_14(bool value)
	{
		___useMouse_14 = value;
	}

	inline static int32_t get_offset_of_useTouch_15() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useTouch_15)); }
	inline bool get_useTouch_15() const { return ___useTouch_15; }
	inline bool* get_address_of_useTouch_15() { return &___useTouch_15; }
	inline void set_useTouch_15(bool value)
	{
		___useTouch_15 = value;
	}

	inline static int32_t get_offset_of_allowMultiTouch_16() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___allowMultiTouch_16)); }
	inline bool get_allowMultiTouch_16() const { return ___allowMultiTouch_16; }
	inline bool* get_address_of_allowMultiTouch_16() { return &___allowMultiTouch_16; }
	inline void set_allowMultiTouch_16(bool value)
	{
		___allowMultiTouch_16 = value;
	}

	inline static int32_t get_offset_of_useKeyboard_17() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useKeyboard_17)); }
	inline bool get_useKeyboard_17() const { return ___useKeyboard_17; }
	inline bool* get_address_of_useKeyboard_17() { return &___useKeyboard_17; }
	inline void set_useKeyboard_17(bool value)
	{
		___useKeyboard_17 = value;
	}

	inline static int32_t get_offset_of_useController_18() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___useController_18)); }
	inline bool get_useController_18() const { return ___useController_18; }
	inline bool* get_address_of_useController_18() { return &___useController_18; }
	inline void set_useController_18(bool value)
	{
		___useController_18 = value;
	}

	inline static int32_t get_offset_of_stickyTooltip_19() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___stickyTooltip_19)); }
	inline bool get_stickyTooltip_19() const { return ___stickyTooltip_19; }
	inline bool* get_address_of_stickyTooltip_19() { return &___stickyTooltip_19; }
	inline void set_stickyTooltip_19(bool value)
	{
		___stickyTooltip_19 = value;
	}

	inline static int32_t get_offset_of_tooltipDelay_20() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___tooltipDelay_20)); }
	inline float get_tooltipDelay_20() const { return ___tooltipDelay_20; }
	inline float* get_address_of_tooltipDelay_20() { return &___tooltipDelay_20; }
	inline void set_tooltipDelay_20(float value)
	{
		___tooltipDelay_20 = value;
	}

	inline static int32_t get_offset_of_longPressTooltip_21() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___longPressTooltip_21)); }
	inline bool get_longPressTooltip_21() const { return ___longPressTooltip_21; }
	inline bool* get_address_of_longPressTooltip_21() { return &___longPressTooltip_21; }
	inline void set_longPressTooltip_21(bool value)
	{
		___longPressTooltip_21 = value;
	}

	inline static int32_t get_offset_of_mouseDragThreshold_22() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mouseDragThreshold_22)); }
	inline float get_mouseDragThreshold_22() const { return ___mouseDragThreshold_22; }
	inline float* get_address_of_mouseDragThreshold_22() { return &___mouseDragThreshold_22; }
	inline void set_mouseDragThreshold_22(float value)
	{
		___mouseDragThreshold_22 = value;
	}

	inline static int32_t get_offset_of_mouseClickThreshold_23() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mouseClickThreshold_23)); }
	inline float get_mouseClickThreshold_23() const { return ___mouseClickThreshold_23; }
	inline float* get_address_of_mouseClickThreshold_23() { return &___mouseClickThreshold_23; }
	inline void set_mouseClickThreshold_23(float value)
	{
		___mouseClickThreshold_23 = value;
	}

	inline static int32_t get_offset_of_touchDragThreshold_24() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___touchDragThreshold_24)); }
	inline float get_touchDragThreshold_24() const { return ___touchDragThreshold_24; }
	inline float* get_address_of_touchDragThreshold_24() { return &___touchDragThreshold_24; }
	inline void set_touchDragThreshold_24(float value)
	{
		___touchDragThreshold_24 = value;
	}

	inline static int32_t get_offset_of_touchClickThreshold_25() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___touchClickThreshold_25)); }
	inline float get_touchClickThreshold_25() const { return ___touchClickThreshold_25; }
	inline float* get_address_of_touchClickThreshold_25() { return &___touchClickThreshold_25; }
	inline void set_touchClickThreshold_25(float value)
	{
		___touchClickThreshold_25 = value;
	}

	inline static int32_t get_offset_of_rangeDistance_26() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___rangeDistance_26)); }
	inline float get_rangeDistance_26() const { return ___rangeDistance_26; }
	inline float* get_address_of_rangeDistance_26() { return &___rangeDistance_26; }
	inline void set_rangeDistance_26(float value)
	{
		___rangeDistance_26 = value;
	}

	inline static int32_t get_offset_of_horizontalAxisName_27() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___horizontalAxisName_27)); }
	inline String_t* get_horizontalAxisName_27() const { return ___horizontalAxisName_27; }
	inline String_t** get_address_of_horizontalAxisName_27() { return &___horizontalAxisName_27; }
	inline void set_horizontalAxisName_27(String_t* value)
	{
		___horizontalAxisName_27 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalAxisName_27, value);
	}

	inline static int32_t get_offset_of_verticalAxisName_28() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___verticalAxisName_28)); }
	inline String_t* get_verticalAxisName_28() const { return ___verticalAxisName_28; }
	inline String_t** get_address_of_verticalAxisName_28() { return &___verticalAxisName_28; }
	inline void set_verticalAxisName_28(String_t* value)
	{
		___verticalAxisName_28 = value;
		Il2CppCodeGenWriteBarrier(&___verticalAxisName_28, value);
	}

	inline static int32_t get_offset_of_horizontalPanAxisName_29() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___horizontalPanAxisName_29)); }
	inline String_t* get_horizontalPanAxisName_29() const { return ___horizontalPanAxisName_29; }
	inline String_t** get_address_of_horizontalPanAxisName_29() { return &___horizontalPanAxisName_29; }
	inline void set_horizontalPanAxisName_29(String_t* value)
	{
		___horizontalPanAxisName_29 = value;
		Il2CppCodeGenWriteBarrier(&___horizontalPanAxisName_29, value);
	}

	inline static int32_t get_offset_of_verticalPanAxisName_30() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___verticalPanAxisName_30)); }
	inline String_t* get_verticalPanAxisName_30() const { return ___verticalPanAxisName_30; }
	inline String_t** get_address_of_verticalPanAxisName_30() { return &___verticalPanAxisName_30; }
	inline void set_verticalPanAxisName_30(String_t* value)
	{
		___verticalPanAxisName_30 = value;
		Il2CppCodeGenWriteBarrier(&___verticalPanAxisName_30, value);
	}

	inline static int32_t get_offset_of_scrollAxisName_31() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___scrollAxisName_31)); }
	inline String_t* get_scrollAxisName_31() const { return ___scrollAxisName_31; }
	inline String_t** get_address_of_scrollAxisName_31() { return &___scrollAxisName_31; }
	inline void set_scrollAxisName_31(String_t* value)
	{
		___scrollAxisName_31 = value;
		Il2CppCodeGenWriteBarrier(&___scrollAxisName_31, value);
	}

	inline static int32_t get_offset_of_commandClick_32() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___commandClick_32)); }
	inline bool get_commandClick_32() const { return ___commandClick_32; }
	inline bool* get_address_of_commandClick_32() { return &___commandClick_32; }
	inline void set_commandClick_32(bool value)
	{
		___commandClick_32 = value;
	}

	inline static int32_t get_offset_of_submitKey0_33() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___submitKey0_33)); }
	inline int32_t get_submitKey0_33() const { return ___submitKey0_33; }
	inline int32_t* get_address_of_submitKey0_33() { return &___submitKey0_33; }
	inline void set_submitKey0_33(int32_t value)
	{
		___submitKey0_33 = value;
	}

	inline static int32_t get_offset_of_submitKey1_34() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___submitKey1_34)); }
	inline int32_t get_submitKey1_34() const { return ___submitKey1_34; }
	inline int32_t* get_address_of_submitKey1_34() { return &___submitKey1_34; }
	inline void set_submitKey1_34(int32_t value)
	{
		___submitKey1_34 = value;
	}

	inline static int32_t get_offset_of_cancelKey0_35() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___cancelKey0_35)); }
	inline int32_t get_cancelKey0_35() const { return ___cancelKey0_35; }
	inline int32_t* get_address_of_cancelKey0_35() { return &___cancelKey0_35; }
	inline void set_cancelKey0_35(int32_t value)
	{
		___cancelKey0_35 = value;
	}

	inline static int32_t get_offset_of_cancelKey1_36() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___cancelKey1_36)); }
	inline int32_t get_cancelKey1_36() const { return ___cancelKey1_36; }
	inline int32_t* get_address_of_cancelKey1_36() { return &___cancelKey1_36; }
	inline void set_cancelKey1_36(int32_t value)
	{
		___cancelKey1_36 = value;
	}

	inline static int32_t get_offset_of_autoHideCursor_37() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___autoHideCursor_37)); }
	inline bool get_autoHideCursor_37() const { return ___autoHideCursor_37; }
	inline bool* get_address_of_autoHideCursor_37() { return &___autoHideCursor_37; }
	inline void set_autoHideCursor_37(bool value)
	{
		___autoHideCursor_37 = value;
	}

	inline static int32_t get_offset_of_mCam_79() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mCam_79)); }
	inline Camera_t3533968274 * get_mCam_79() const { return ___mCam_79; }
	inline Camera_t3533968274 ** get_address_of_mCam_79() { return &___mCam_79; }
	inline void set_mCam_79(Camera_t3533968274 * value)
	{
		___mCam_79 = value;
		Il2CppCodeGenWriteBarrier(&___mCam_79, value);
	}

	inline static int32_t get_offset_of_mNextRaycast_81() { return static_cast<int32_t>(offsetof(UICamera_t189364953, ___mNextRaycast_81)); }
	inline float get_mNextRaycast_81() const { return ___mNextRaycast_81; }
	inline float* get_address_of_mNextRaycast_81() { return &___mNextRaycast_81; }
	inline void set_mNextRaycast_81(float value)
	{
		___mNextRaycast_81 = value;
	}
};

struct UICamera_t189364953_StaticFields
{
public:
	// BetterList`1<UICamera> UICamera::list
	BetterList_1_t1686332965 * ___list_2;
	// UICamera/GetKeyStateFunc UICamera::GetKeyDown
	GetKeyStateFunc_t2362323372 * ___GetKeyDown_3;
	// UICamera/GetKeyStateFunc UICamera::GetKeyUp
	GetKeyStateFunc_t2362323372 * ___GetKeyUp_4;
	// UICamera/GetKeyStateFunc UICamera::GetKey
	GetKeyStateFunc_t2362323372 * ___GetKey_5;
	// UICamera/GetAxisFunc UICamera::GetAxis
	GetAxisFunc_t3240405851 * ___GetAxis_6;
	// UICamera/GetAnyKeyFunc UICamera::GetAnyKeyDown
	GetAnyKeyFunc_t2938188301 * ___GetAnyKeyDown_7;
	// UICamera/OnScreenResize UICamera::onScreenResize
	OnScreenResize_t3539115999 * ___onScreenResize_8;
	// UICamera/OnCustomInput UICamera::onCustomInput
	OnCustomInput_t3775009626 * ___onCustomInput_38;
	// System.Boolean UICamera::showTooltips
	bool ___showTooltips_39;
	// System.Boolean UICamera::ignoreControllerInput
	bool ___ignoreControllerInput_40;
	// System.Boolean UICamera::mDisableController
	bool ___mDisableController_41;
	// UnityEngine.Vector2 UICamera::mLastPos
	Vector2_t3525329788  ___mLastPos_42;
	// UnityEngine.Vector3 UICamera::lastWorldPosition
	Vector3_t3525329789  ___lastWorldPosition_43;
	// UnityEngine.RaycastHit UICamera::lastHit
	RaycastHit_t46221527  ___lastHit_44;
	// UICamera UICamera::current
	UICamera_t189364953 * ___current_45;
	// UnityEngine.Camera UICamera::currentCamera
	Camera_t3533968274 * ___currentCamera_46;
	// UICamera/OnSchemeChange UICamera::onSchemeChange
	OnSchemeChange_t2442067284 * ___onSchemeChange_47;
	// UICamera/ControlScheme UICamera::mLastScheme
	int32_t ___mLastScheme_48;
	// System.Int32 UICamera::currentTouchID
	int32_t ___currentTouchID_49;
	// UnityEngine.KeyCode UICamera::mCurrentKey
	int32_t ___mCurrentKey_50;
	// UICamera/MouseOrTouch UICamera::currentTouch
	MouseOrTouch_t908473047 * ___currentTouch_51;
	// System.Boolean UICamera::mInputFocus
	bool ___mInputFocus_52;
	// UnityEngine.GameObject UICamera::mGenericHandler
	GameObject_t4012695102 * ___mGenericHandler_53;
	// UnityEngine.GameObject UICamera::fallThrough
	GameObject_t4012695102 * ___fallThrough_54;
	// UICamera/VoidDelegate UICamera::onClick
	VoidDelegate_t1986138970 * ___onClick_55;
	// UICamera/VoidDelegate UICamera::onDoubleClick
	VoidDelegate_t1986138970 * ___onDoubleClick_56;
	// UICamera/BoolDelegate UICamera::onHover
	BoolDelegate_t945637040 * ___onHover_57;
	// UICamera/BoolDelegate UICamera::onPress
	BoolDelegate_t945637040 * ___onPress_58;
	// UICamera/BoolDelegate UICamera::onSelect
	BoolDelegate_t945637040 * ___onSelect_59;
	// UICamera/FloatDelegate UICamera::onScroll
	FloatDelegate_t4185225186 * ___onScroll_60;
	// UICamera/VectorDelegate UICamera::onDrag
	VectorDelegate_t2458450953 * ___onDrag_61;
	// UICamera/VoidDelegate UICamera::onDragStart
	VoidDelegate_t1986138970 * ___onDragStart_62;
	// UICamera/ObjectDelegate UICamera::onDragOver
	ObjectDelegate_t3413901829 * ___onDragOver_63;
	// UICamera/ObjectDelegate UICamera::onDragOut
	ObjectDelegate_t3413901829 * ___onDragOut_64;
	// UICamera/VoidDelegate UICamera::onDragEnd
	VoidDelegate_t1986138970 * ___onDragEnd_65;
	// UICamera/ObjectDelegate UICamera::onDrop
	ObjectDelegate_t3413901829 * ___onDrop_66;
	// UICamera/KeyCodeDelegate UICamera::onKey
	KeyCodeDelegate_t3737044594 * ___onKey_67;
	// UICamera/KeyCodeDelegate UICamera::onNavigate
	KeyCodeDelegate_t3737044594 * ___onNavigate_68;
	// UICamera/VectorDelegate UICamera::onPan
	VectorDelegate_t2458450953 * ___onPan_69;
	// UICamera/BoolDelegate UICamera::onTooltip
	BoolDelegate_t945637040 * ___onTooltip_70;
	// UICamera/MoveDelegate UICamera::onMouseMove
	MoveDelegate_t757231766 * ___onMouseMove_71;
	// UICamera/MouseOrTouch[] UICamera::mMouse
	MouseOrTouchU5BU5D_t2832174062* ___mMouse_72;
	// UICamera/MouseOrTouch UICamera::controller
	MouseOrTouch_t908473047 * ___controller_73;
	// System.Collections.Generic.List`1<UICamera/MouseOrTouch> UICamera::activeTouches
	List_1_t1705432016 * ___activeTouches_74;
	// System.Collections.Generic.List`1<System.Int32> UICamera::mTouchIDs
	List_1_t3644373756 * ___mTouchIDs_75;
	// System.Int32 UICamera::mWidth
	int32_t ___mWidth_76;
	// System.Int32 UICamera::mHeight
	int32_t ___mHeight_77;
	// UnityEngine.GameObject UICamera::mTooltip
	GameObject_t4012695102 * ___mTooltip_78;
	// System.Single UICamera::mTooltipTime
	float ___mTooltipTime_80;
	// System.Boolean UICamera::isDragging
	bool ___isDragging_82;
	// UnityEngine.GameObject UICamera::mRayHitObject
	GameObject_t4012695102 * ___mRayHitObject_83;
	// UnityEngine.GameObject UICamera::mHover
	GameObject_t4012695102 * ___mHover_84;
	// UnityEngine.GameObject UICamera::mSelected
	GameObject_t4012695102 * ___mSelected_85;
	// UICamera/DepthEntry UICamera::mHit
	DepthEntry_t2194697103  ___mHit_86;
	// BetterList`1<UICamera/DepthEntry> UICamera::mHits
	BetterList_1_t3691665115 * ___mHits_87;
	// UnityEngine.Plane UICamera::m2DPlane
	Plane_t1600081545  ___m2DPlane_88;
	// System.Single UICamera::mNextEvent
	float ___mNextEvent_89;
	// System.Int32 UICamera::mNotifying
	int32_t ___mNotifying_90;
	// System.Boolean UICamera::mUsingTouchEvents
	bool ___mUsingTouchEvents_91;
	// UICamera/GetTouchCountCallback UICamera::GetInputTouchCount
	GetTouchCountCallback_t345790283 * ___GetInputTouchCount_92;
	// UICamera/GetTouchCallback UICamera::GetInputTouch
	GetTouchCallback_t2933503246 * ___GetInputTouch_93;
	// UICamera/GetKeyStateFunc UICamera::<>f__am$cache5C
	GetKeyStateFunc_t2362323372 * ___U3CU3Ef__amU24cache5C_94;
	// UICamera/GetKeyStateFunc UICamera::<>f__am$cache5D
	GetKeyStateFunc_t2362323372 * ___U3CU3Ef__amU24cache5D_95;
	// UICamera/GetKeyStateFunc UICamera::<>f__am$cache5E
	GetKeyStateFunc_t2362323372 * ___U3CU3Ef__amU24cache5E_96;
	// UICamera/GetAxisFunc UICamera::<>f__am$cache5F
	GetAxisFunc_t3240405851 * ___U3CU3Ef__amU24cache5F_97;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache60
	CompareFunc_t41355861 * ___U3CU3Ef__amU24cache60_98;
	// BetterList`1/CompareFunc<UICamera/DepthEntry> UICamera::<>f__am$cache61
	CompareFunc_t41355861 * ___U3CU3Ef__amU24cache61_99;

public:
	inline static int32_t get_offset_of_list_2() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___list_2)); }
	inline BetterList_1_t1686332965 * get_list_2() const { return ___list_2; }
	inline BetterList_1_t1686332965 ** get_address_of_list_2() { return &___list_2; }
	inline void set_list_2(BetterList_1_t1686332965 * value)
	{
		___list_2 = value;
		Il2CppCodeGenWriteBarrier(&___list_2, value);
	}

	inline static int32_t get_offset_of_GetKeyDown_3() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetKeyDown_3)); }
	inline GetKeyStateFunc_t2362323372 * get_GetKeyDown_3() const { return ___GetKeyDown_3; }
	inline GetKeyStateFunc_t2362323372 ** get_address_of_GetKeyDown_3() { return &___GetKeyDown_3; }
	inline void set_GetKeyDown_3(GetKeyStateFunc_t2362323372 * value)
	{
		___GetKeyDown_3 = value;
		Il2CppCodeGenWriteBarrier(&___GetKeyDown_3, value);
	}

	inline static int32_t get_offset_of_GetKeyUp_4() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetKeyUp_4)); }
	inline GetKeyStateFunc_t2362323372 * get_GetKeyUp_4() const { return ___GetKeyUp_4; }
	inline GetKeyStateFunc_t2362323372 ** get_address_of_GetKeyUp_4() { return &___GetKeyUp_4; }
	inline void set_GetKeyUp_4(GetKeyStateFunc_t2362323372 * value)
	{
		___GetKeyUp_4 = value;
		Il2CppCodeGenWriteBarrier(&___GetKeyUp_4, value);
	}

	inline static int32_t get_offset_of_GetKey_5() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetKey_5)); }
	inline GetKeyStateFunc_t2362323372 * get_GetKey_5() const { return ___GetKey_5; }
	inline GetKeyStateFunc_t2362323372 ** get_address_of_GetKey_5() { return &___GetKey_5; }
	inline void set_GetKey_5(GetKeyStateFunc_t2362323372 * value)
	{
		___GetKey_5 = value;
		Il2CppCodeGenWriteBarrier(&___GetKey_5, value);
	}

	inline static int32_t get_offset_of_GetAxis_6() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetAxis_6)); }
	inline GetAxisFunc_t3240405851 * get_GetAxis_6() const { return ___GetAxis_6; }
	inline GetAxisFunc_t3240405851 ** get_address_of_GetAxis_6() { return &___GetAxis_6; }
	inline void set_GetAxis_6(GetAxisFunc_t3240405851 * value)
	{
		___GetAxis_6 = value;
		Il2CppCodeGenWriteBarrier(&___GetAxis_6, value);
	}

	inline static int32_t get_offset_of_GetAnyKeyDown_7() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetAnyKeyDown_7)); }
	inline GetAnyKeyFunc_t2938188301 * get_GetAnyKeyDown_7() const { return ___GetAnyKeyDown_7; }
	inline GetAnyKeyFunc_t2938188301 ** get_address_of_GetAnyKeyDown_7() { return &___GetAnyKeyDown_7; }
	inline void set_GetAnyKeyDown_7(GetAnyKeyFunc_t2938188301 * value)
	{
		___GetAnyKeyDown_7 = value;
		Il2CppCodeGenWriteBarrier(&___GetAnyKeyDown_7, value);
	}

	inline static int32_t get_offset_of_onScreenResize_8() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onScreenResize_8)); }
	inline OnScreenResize_t3539115999 * get_onScreenResize_8() const { return ___onScreenResize_8; }
	inline OnScreenResize_t3539115999 ** get_address_of_onScreenResize_8() { return &___onScreenResize_8; }
	inline void set_onScreenResize_8(OnScreenResize_t3539115999 * value)
	{
		___onScreenResize_8 = value;
		Il2CppCodeGenWriteBarrier(&___onScreenResize_8, value);
	}

	inline static int32_t get_offset_of_onCustomInput_38() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onCustomInput_38)); }
	inline OnCustomInput_t3775009626 * get_onCustomInput_38() const { return ___onCustomInput_38; }
	inline OnCustomInput_t3775009626 ** get_address_of_onCustomInput_38() { return &___onCustomInput_38; }
	inline void set_onCustomInput_38(OnCustomInput_t3775009626 * value)
	{
		___onCustomInput_38 = value;
		Il2CppCodeGenWriteBarrier(&___onCustomInput_38, value);
	}

	inline static int32_t get_offset_of_showTooltips_39() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___showTooltips_39)); }
	inline bool get_showTooltips_39() const { return ___showTooltips_39; }
	inline bool* get_address_of_showTooltips_39() { return &___showTooltips_39; }
	inline void set_showTooltips_39(bool value)
	{
		___showTooltips_39 = value;
	}

	inline static int32_t get_offset_of_ignoreControllerInput_40() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___ignoreControllerInput_40)); }
	inline bool get_ignoreControllerInput_40() const { return ___ignoreControllerInput_40; }
	inline bool* get_address_of_ignoreControllerInput_40() { return &___ignoreControllerInput_40; }
	inline void set_ignoreControllerInput_40(bool value)
	{
		___ignoreControllerInput_40 = value;
	}

	inline static int32_t get_offset_of_mDisableController_41() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mDisableController_41)); }
	inline bool get_mDisableController_41() const { return ___mDisableController_41; }
	inline bool* get_address_of_mDisableController_41() { return &___mDisableController_41; }
	inline void set_mDisableController_41(bool value)
	{
		___mDisableController_41 = value;
	}

	inline static int32_t get_offset_of_mLastPos_42() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mLastPos_42)); }
	inline Vector2_t3525329788  get_mLastPos_42() const { return ___mLastPos_42; }
	inline Vector2_t3525329788 * get_address_of_mLastPos_42() { return &___mLastPos_42; }
	inline void set_mLastPos_42(Vector2_t3525329788  value)
	{
		___mLastPos_42 = value;
	}

	inline static int32_t get_offset_of_lastWorldPosition_43() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___lastWorldPosition_43)); }
	inline Vector3_t3525329789  get_lastWorldPosition_43() const { return ___lastWorldPosition_43; }
	inline Vector3_t3525329789 * get_address_of_lastWorldPosition_43() { return &___lastWorldPosition_43; }
	inline void set_lastWorldPosition_43(Vector3_t3525329789  value)
	{
		___lastWorldPosition_43 = value;
	}

	inline static int32_t get_offset_of_lastHit_44() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___lastHit_44)); }
	inline RaycastHit_t46221527  get_lastHit_44() const { return ___lastHit_44; }
	inline RaycastHit_t46221527 * get_address_of_lastHit_44() { return &___lastHit_44; }
	inline void set_lastHit_44(RaycastHit_t46221527  value)
	{
		___lastHit_44 = value;
	}

	inline static int32_t get_offset_of_current_45() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___current_45)); }
	inline UICamera_t189364953 * get_current_45() const { return ___current_45; }
	inline UICamera_t189364953 ** get_address_of_current_45() { return &___current_45; }
	inline void set_current_45(UICamera_t189364953 * value)
	{
		___current_45 = value;
		Il2CppCodeGenWriteBarrier(&___current_45, value);
	}

	inline static int32_t get_offset_of_currentCamera_46() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentCamera_46)); }
	inline Camera_t3533968274 * get_currentCamera_46() const { return ___currentCamera_46; }
	inline Camera_t3533968274 ** get_address_of_currentCamera_46() { return &___currentCamera_46; }
	inline void set_currentCamera_46(Camera_t3533968274 * value)
	{
		___currentCamera_46 = value;
		Il2CppCodeGenWriteBarrier(&___currentCamera_46, value);
	}

	inline static int32_t get_offset_of_onSchemeChange_47() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onSchemeChange_47)); }
	inline OnSchemeChange_t2442067284 * get_onSchemeChange_47() const { return ___onSchemeChange_47; }
	inline OnSchemeChange_t2442067284 ** get_address_of_onSchemeChange_47() { return &___onSchemeChange_47; }
	inline void set_onSchemeChange_47(OnSchemeChange_t2442067284 * value)
	{
		___onSchemeChange_47 = value;
		Il2CppCodeGenWriteBarrier(&___onSchemeChange_47, value);
	}

	inline static int32_t get_offset_of_mLastScheme_48() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mLastScheme_48)); }
	inline int32_t get_mLastScheme_48() const { return ___mLastScheme_48; }
	inline int32_t* get_address_of_mLastScheme_48() { return &___mLastScheme_48; }
	inline void set_mLastScheme_48(int32_t value)
	{
		___mLastScheme_48 = value;
	}

	inline static int32_t get_offset_of_currentTouchID_49() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentTouchID_49)); }
	inline int32_t get_currentTouchID_49() const { return ___currentTouchID_49; }
	inline int32_t* get_address_of_currentTouchID_49() { return &___currentTouchID_49; }
	inline void set_currentTouchID_49(int32_t value)
	{
		___currentTouchID_49 = value;
	}

	inline static int32_t get_offset_of_mCurrentKey_50() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mCurrentKey_50)); }
	inline int32_t get_mCurrentKey_50() const { return ___mCurrentKey_50; }
	inline int32_t* get_address_of_mCurrentKey_50() { return &___mCurrentKey_50; }
	inline void set_mCurrentKey_50(int32_t value)
	{
		___mCurrentKey_50 = value;
	}

	inline static int32_t get_offset_of_currentTouch_51() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___currentTouch_51)); }
	inline MouseOrTouch_t908473047 * get_currentTouch_51() const { return ___currentTouch_51; }
	inline MouseOrTouch_t908473047 ** get_address_of_currentTouch_51() { return &___currentTouch_51; }
	inline void set_currentTouch_51(MouseOrTouch_t908473047 * value)
	{
		___currentTouch_51 = value;
		Il2CppCodeGenWriteBarrier(&___currentTouch_51, value);
	}

	inline static int32_t get_offset_of_mInputFocus_52() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mInputFocus_52)); }
	inline bool get_mInputFocus_52() const { return ___mInputFocus_52; }
	inline bool* get_address_of_mInputFocus_52() { return &___mInputFocus_52; }
	inline void set_mInputFocus_52(bool value)
	{
		___mInputFocus_52 = value;
	}

	inline static int32_t get_offset_of_mGenericHandler_53() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mGenericHandler_53)); }
	inline GameObject_t4012695102 * get_mGenericHandler_53() const { return ___mGenericHandler_53; }
	inline GameObject_t4012695102 ** get_address_of_mGenericHandler_53() { return &___mGenericHandler_53; }
	inline void set_mGenericHandler_53(GameObject_t4012695102 * value)
	{
		___mGenericHandler_53 = value;
		Il2CppCodeGenWriteBarrier(&___mGenericHandler_53, value);
	}

	inline static int32_t get_offset_of_fallThrough_54() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___fallThrough_54)); }
	inline GameObject_t4012695102 * get_fallThrough_54() const { return ___fallThrough_54; }
	inline GameObject_t4012695102 ** get_address_of_fallThrough_54() { return &___fallThrough_54; }
	inline void set_fallThrough_54(GameObject_t4012695102 * value)
	{
		___fallThrough_54 = value;
		Il2CppCodeGenWriteBarrier(&___fallThrough_54, value);
	}

	inline static int32_t get_offset_of_onClick_55() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onClick_55)); }
	inline VoidDelegate_t1986138970 * get_onClick_55() const { return ___onClick_55; }
	inline VoidDelegate_t1986138970 ** get_address_of_onClick_55() { return &___onClick_55; }
	inline void set_onClick_55(VoidDelegate_t1986138970 * value)
	{
		___onClick_55 = value;
		Il2CppCodeGenWriteBarrier(&___onClick_55, value);
	}

	inline static int32_t get_offset_of_onDoubleClick_56() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDoubleClick_56)); }
	inline VoidDelegate_t1986138970 * get_onDoubleClick_56() const { return ___onDoubleClick_56; }
	inline VoidDelegate_t1986138970 ** get_address_of_onDoubleClick_56() { return &___onDoubleClick_56; }
	inline void set_onDoubleClick_56(VoidDelegate_t1986138970 * value)
	{
		___onDoubleClick_56 = value;
		Il2CppCodeGenWriteBarrier(&___onDoubleClick_56, value);
	}

	inline static int32_t get_offset_of_onHover_57() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onHover_57)); }
	inline BoolDelegate_t945637040 * get_onHover_57() const { return ___onHover_57; }
	inline BoolDelegate_t945637040 ** get_address_of_onHover_57() { return &___onHover_57; }
	inline void set_onHover_57(BoolDelegate_t945637040 * value)
	{
		___onHover_57 = value;
		Il2CppCodeGenWriteBarrier(&___onHover_57, value);
	}

	inline static int32_t get_offset_of_onPress_58() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onPress_58)); }
	inline BoolDelegate_t945637040 * get_onPress_58() const { return ___onPress_58; }
	inline BoolDelegate_t945637040 ** get_address_of_onPress_58() { return &___onPress_58; }
	inline void set_onPress_58(BoolDelegate_t945637040 * value)
	{
		___onPress_58 = value;
		Il2CppCodeGenWriteBarrier(&___onPress_58, value);
	}

	inline static int32_t get_offset_of_onSelect_59() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onSelect_59)); }
	inline BoolDelegate_t945637040 * get_onSelect_59() const { return ___onSelect_59; }
	inline BoolDelegate_t945637040 ** get_address_of_onSelect_59() { return &___onSelect_59; }
	inline void set_onSelect_59(BoolDelegate_t945637040 * value)
	{
		___onSelect_59 = value;
		Il2CppCodeGenWriteBarrier(&___onSelect_59, value);
	}

	inline static int32_t get_offset_of_onScroll_60() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onScroll_60)); }
	inline FloatDelegate_t4185225186 * get_onScroll_60() const { return ___onScroll_60; }
	inline FloatDelegate_t4185225186 ** get_address_of_onScroll_60() { return &___onScroll_60; }
	inline void set_onScroll_60(FloatDelegate_t4185225186 * value)
	{
		___onScroll_60 = value;
		Il2CppCodeGenWriteBarrier(&___onScroll_60, value);
	}

	inline static int32_t get_offset_of_onDrag_61() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDrag_61)); }
	inline VectorDelegate_t2458450953 * get_onDrag_61() const { return ___onDrag_61; }
	inline VectorDelegate_t2458450953 ** get_address_of_onDrag_61() { return &___onDrag_61; }
	inline void set_onDrag_61(VectorDelegate_t2458450953 * value)
	{
		___onDrag_61 = value;
		Il2CppCodeGenWriteBarrier(&___onDrag_61, value);
	}

	inline static int32_t get_offset_of_onDragStart_62() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragStart_62)); }
	inline VoidDelegate_t1986138970 * get_onDragStart_62() const { return ___onDragStart_62; }
	inline VoidDelegate_t1986138970 ** get_address_of_onDragStart_62() { return &___onDragStart_62; }
	inline void set_onDragStart_62(VoidDelegate_t1986138970 * value)
	{
		___onDragStart_62 = value;
		Il2CppCodeGenWriteBarrier(&___onDragStart_62, value);
	}

	inline static int32_t get_offset_of_onDragOver_63() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragOver_63)); }
	inline ObjectDelegate_t3413901829 * get_onDragOver_63() const { return ___onDragOver_63; }
	inline ObjectDelegate_t3413901829 ** get_address_of_onDragOver_63() { return &___onDragOver_63; }
	inline void set_onDragOver_63(ObjectDelegate_t3413901829 * value)
	{
		___onDragOver_63 = value;
		Il2CppCodeGenWriteBarrier(&___onDragOver_63, value);
	}

	inline static int32_t get_offset_of_onDragOut_64() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragOut_64)); }
	inline ObjectDelegate_t3413901829 * get_onDragOut_64() const { return ___onDragOut_64; }
	inline ObjectDelegate_t3413901829 ** get_address_of_onDragOut_64() { return &___onDragOut_64; }
	inline void set_onDragOut_64(ObjectDelegate_t3413901829 * value)
	{
		___onDragOut_64 = value;
		Il2CppCodeGenWriteBarrier(&___onDragOut_64, value);
	}

	inline static int32_t get_offset_of_onDragEnd_65() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDragEnd_65)); }
	inline VoidDelegate_t1986138970 * get_onDragEnd_65() const { return ___onDragEnd_65; }
	inline VoidDelegate_t1986138970 ** get_address_of_onDragEnd_65() { return &___onDragEnd_65; }
	inline void set_onDragEnd_65(VoidDelegate_t1986138970 * value)
	{
		___onDragEnd_65 = value;
		Il2CppCodeGenWriteBarrier(&___onDragEnd_65, value);
	}

	inline static int32_t get_offset_of_onDrop_66() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onDrop_66)); }
	inline ObjectDelegate_t3413901829 * get_onDrop_66() const { return ___onDrop_66; }
	inline ObjectDelegate_t3413901829 ** get_address_of_onDrop_66() { return &___onDrop_66; }
	inline void set_onDrop_66(ObjectDelegate_t3413901829 * value)
	{
		___onDrop_66 = value;
		Il2CppCodeGenWriteBarrier(&___onDrop_66, value);
	}

	inline static int32_t get_offset_of_onKey_67() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onKey_67)); }
	inline KeyCodeDelegate_t3737044594 * get_onKey_67() const { return ___onKey_67; }
	inline KeyCodeDelegate_t3737044594 ** get_address_of_onKey_67() { return &___onKey_67; }
	inline void set_onKey_67(KeyCodeDelegate_t3737044594 * value)
	{
		___onKey_67 = value;
		Il2CppCodeGenWriteBarrier(&___onKey_67, value);
	}

	inline static int32_t get_offset_of_onNavigate_68() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onNavigate_68)); }
	inline KeyCodeDelegate_t3737044594 * get_onNavigate_68() const { return ___onNavigate_68; }
	inline KeyCodeDelegate_t3737044594 ** get_address_of_onNavigate_68() { return &___onNavigate_68; }
	inline void set_onNavigate_68(KeyCodeDelegate_t3737044594 * value)
	{
		___onNavigate_68 = value;
		Il2CppCodeGenWriteBarrier(&___onNavigate_68, value);
	}

	inline static int32_t get_offset_of_onPan_69() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onPan_69)); }
	inline VectorDelegate_t2458450953 * get_onPan_69() const { return ___onPan_69; }
	inline VectorDelegate_t2458450953 ** get_address_of_onPan_69() { return &___onPan_69; }
	inline void set_onPan_69(VectorDelegate_t2458450953 * value)
	{
		___onPan_69 = value;
		Il2CppCodeGenWriteBarrier(&___onPan_69, value);
	}

	inline static int32_t get_offset_of_onTooltip_70() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onTooltip_70)); }
	inline BoolDelegate_t945637040 * get_onTooltip_70() const { return ___onTooltip_70; }
	inline BoolDelegate_t945637040 ** get_address_of_onTooltip_70() { return &___onTooltip_70; }
	inline void set_onTooltip_70(BoolDelegate_t945637040 * value)
	{
		___onTooltip_70 = value;
		Il2CppCodeGenWriteBarrier(&___onTooltip_70, value);
	}

	inline static int32_t get_offset_of_onMouseMove_71() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___onMouseMove_71)); }
	inline MoveDelegate_t757231766 * get_onMouseMove_71() const { return ___onMouseMove_71; }
	inline MoveDelegate_t757231766 ** get_address_of_onMouseMove_71() { return &___onMouseMove_71; }
	inline void set_onMouseMove_71(MoveDelegate_t757231766 * value)
	{
		___onMouseMove_71 = value;
		Il2CppCodeGenWriteBarrier(&___onMouseMove_71, value);
	}

	inline static int32_t get_offset_of_mMouse_72() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mMouse_72)); }
	inline MouseOrTouchU5BU5D_t2832174062* get_mMouse_72() const { return ___mMouse_72; }
	inline MouseOrTouchU5BU5D_t2832174062** get_address_of_mMouse_72() { return &___mMouse_72; }
	inline void set_mMouse_72(MouseOrTouchU5BU5D_t2832174062* value)
	{
		___mMouse_72 = value;
		Il2CppCodeGenWriteBarrier(&___mMouse_72, value);
	}

	inline static int32_t get_offset_of_controller_73() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___controller_73)); }
	inline MouseOrTouch_t908473047 * get_controller_73() const { return ___controller_73; }
	inline MouseOrTouch_t908473047 ** get_address_of_controller_73() { return &___controller_73; }
	inline void set_controller_73(MouseOrTouch_t908473047 * value)
	{
		___controller_73 = value;
		Il2CppCodeGenWriteBarrier(&___controller_73, value);
	}

	inline static int32_t get_offset_of_activeTouches_74() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___activeTouches_74)); }
	inline List_1_t1705432016 * get_activeTouches_74() const { return ___activeTouches_74; }
	inline List_1_t1705432016 ** get_address_of_activeTouches_74() { return &___activeTouches_74; }
	inline void set_activeTouches_74(List_1_t1705432016 * value)
	{
		___activeTouches_74 = value;
		Il2CppCodeGenWriteBarrier(&___activeTouches_74, value);
	}

	inline static int32_t get_offset_of_mTouchIDs_75() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mTouchIDs_75)); }
	inline List_1_t3644373756 * get_mTouchIDs_75() const { return ___mTouchIDs_75; }
	inline List_1_t3644373756 ** get_address_of_mTouchIDs_75() { return &___mTouchIDs_75; }
	inline void set_mTouchIDs_75(List_1_t3644373756 * value)
	{
		___mTouchIDs_75 = value;
		Il2CppCodeGenWriteBarrier(&___mTouchIDs_75, value);
	}

	inline static int32_t get_offset_of_mWidth_76() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mWidth_76)); }
	inline int32_t get_mWidth_76() const { return ___mWidth_76; }
	inline int32_t* get_address_of_mWidth_76() { return &___mWidth_76; }
	inline void set_mWidth_76(int32_t value)
	{
		___mWidth_76 = value;
	}

	inline static int32_t get_offset_of_mHeight_77() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHeight_77)); }
	inline int32_t get_mHeight_77() const { return ___mHeight_77; }
	inline int32_t* get_address_of_mHeight_77() { return &___mHeight_77; }
	inline void set_mHeight_77(int32_t value)
	{
		___mHeight_77 = value;
	}

	inline static int32_t get_offset_of_mTooltip_78() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mTooltip_78)); }
	inline GameObject_t4012695102 * get_mTooltip_78() const { return ___mTooltip_78; }
	inline GameObject_t4012695102 ** get_address_of_mTooltip_78() { return &___mTooltip_78; }
	inline void set_mTooltip_78(GameObject_t4012695102 * value)
	{
		___mTooltip_78 = value;
		Il2CppCodeGenWriteBarrier(&___mTooltip_78, value);
	}

	inline static int32_t get_offset_of_mTooltipTime_80() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mTooltipTime_80)); }
	inline float get_mTooltipTime_80() const { return ___mTooltipTime_80; }
	inline float* get_address_of_mTooltipTime_80() { return &___mTooltipTime_80; }
	inline void set_mTooltipTime_80(float value)
	{
		___mTooltipTime_80 = value;
	}

	inline static int32_t get_offset_of_isDragging_82() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___isDragging_82)); }
	inline bool get_isDragging_82() const { return ___isDragging_82; }
	inline bool* get_address_of_isDragging_82() { return &___isDragging_82; }
	inline void set_isDragging_82(bool value)
	{
		___isDragging_82 = value;
	}

	inline static int32_t get_offset_of_mRayHitObject_83() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mRayHitObject_83)); }
	inline GameObject_t4012695102 * get_mRayHitObject_83() const { return ___mRayHitObject_83; }
	inline GameObject_t4012695102 ** get_address_of_mRayHitObject_83() { return &___mRayHitObject_83; }
	inline void set_mRayHitObject_83(GameObject_t4012695102 * value)
	{
		___mRayHitObject_83 = value;
		Il2CppCodeGenWriteBarrier(&___mRayHitObject_83, value);
	}

	inline static int32_t get_offset_of_mHover_84() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHover_84)); }
	inline GameObject_t4012695102 * get_mHover_84() const { return ___mHover_84; }
	inline GameObject_t4012695102 ** get_address_of_mHover_84() { return &___mHover_84; }
	inline void set_mHover_84(GameObject_t4012695102 * value)
	{
		___mHover_84 = value;
		Il2CppCodeGenWriteBarrier(&___mHover_84, value);
	}

	inline static int32_t get_offset_of_mSelected_85() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mSelected_85)); }
	inline GameObject_t4012695102 * get_mSelected_85() const { return ___mSelected_85; }
	inline GameObject_t4012695102 ** get_address_of_mSelected_85() { return &___mSelected_85; }
	inline void set_mSelected_85(GameObject_t4012695102 * value)
	{
		___mSelected_85 = value;
		Il2CppCodeGenWriteBarrier(&___mSelected_85, value);
	}

	inline static int32_t get_offset_of_mHit_86() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHit_86)); }
	inline DepthEntry_t2194697103  get_mHit_86() const { return ___mHit_86; }
	inline DepthEntry_t2194697103 * get_address_of_mHit_86() { return &___mHit_86; }
	inline void set_mHit_86(DepthEntry_t2194697103  value)
	{
		___mHit_86 = value;
	}

	inline static int32_t get_offset_of_mHits_87() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mHits_87)); }
	inline BetterList_1_t3691665115 * get_mHits_87() const { return ___mHits_87; }
	inline BetterList_1_t3691665115 ** get_address_of_mHits_87() { return &___mHits_87; }
	inline void set_mHits_87(BetterList_1_t3691665115 * value)
	{
		___mHits_87 = value;
		Il2CppCodeGenWriteBarrier(&___mHits_87, value);
	}

	inline static int32_t get_offset_of_m2DPlane_88() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___m2DPlane_88)); }
	inline Plane_t1600081545  get_m2DPlane_88() const { return ___m2DPlane_88; }
	inline Plane_t1600081545 * get_address_of_m2DPlane_88() { return &___m2DPlane_88; }
	inline void set_m2DPlane_88(Plane_t1600081545  value)
	{
		___m2DPlane_88 = value;
	}

	inline static int32_t get_offset_of_mNextEvent_89() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mNextEvent_89)); }
	inline float get_mNextEvent_89() const { return ___mNextEvent_89; }
	inline float* get_address_of_mNextEvent_89() { return &___mNextEvent_89; }
	inline void set_mNextEvent_89(float value)
	{
		___mNextEvent_89 = value;
	}

	inline static int32_t get_offset_of_mNotifying_90() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mNotifying_90)); }
	inline int32_t get_mNotifying_90() const { return ___mNotifying_90; }
	inline int32_t* get_address_of_mNotifying_90() { return &___mNotifying_90; }
	inline void set_mNotifying_90(int32_t value)
	{
		___mNotifying_90 = value;
	}

	inline static int32_t get_offset_of_mUsingTouchEvents_91() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___mUsingTouchEvents_91)); }
	inline bool get_mUsingTouchEvents_91() const { return ___mUsingTouchEvents_91; }
	inline bool* get_address_of_mUsingTouchEvents_91() { return &___mUsingTouchEvents_91; }
	inline void set_mUsingTouchEvents_91(bool value)
	{
		___mUsingTouchEvents_91 = value;
	}

	inline static int32_t get_offset_of_GetInputTouchCount_92() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetInputTouchCount_92)); }
	inline GetTouchCountCallback_t345790283 * get_GetInputTouchCount_92() const { return ___GetInputTouchCount_92; }
	inline GetTouchCountCallback_t345790283 ** get_address_of_GetInputTouchCount_92() { return &___GetInputTouchCount_92; }
	inline void set_GetInputTouchCount_92(GetTouchCountCallback_t345790283 * value)
	{
		___GetInputTouchCount_92 = value;
		Il2CppCodeGenWriteBarrier(&___GetInputTouchCount_92, value);
	}

	inline static int32_t get_offset_of_GetInputTouch_93() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___GetInputTouch_93)); }
	inline GetTouchCallback_t2933503246 * get_GetInputTouch_93() const { return ___GetInputTouch_93; }
	inline GetTouchCallback_t2933503246 ** get_address_of_GetInputTouch_93() { return &___GetInputTouch_93; }
	inline void set_GetInputTouch_93(GetTouchCallback_t2933503246 * value)
	{
		___GetInputTouch_93 = value;
		Il2CppCodeGenWriteBarrier(&___GetInputTouch_93, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5C_94() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache5C_94)); }
	inline GetKeyStateFunc_t2362323372 * get_U3CU3Ef__amU24cache5C_94() const { return ___U3CU3Ef__amU24cache5C_94; }
	inline GetKeyStateFunc_t2362323372 ** get_address_of_U3CU3Ef__amU24cache5C_94() { return &___U3CU3Ef__amU24cache5C_94; }
	inline void set_U3CU3Ef__amU24cache5C_94(GetKeyStateFunc_t2362323372 * value)
	{
		___U3CU3Ef__amU24cache5C_94 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5C_94, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5D_95() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache5D_95)); }
	inline GetKeyStateFunc_t2362323372 * get_U3CU3Ef__amU24cache5D_95() const { return ___U3CU3Ef__amU24cache5D_95; }
	inline GetKeyStateFunc_t2362323372 ** get_address_of_U3CU3Ef__amU24cache5D_95() { return &___U3CU3Ef__amU24cache5D_95; }
	inline void set_U3CU3Ef__amU24cache5D_95(GetKeyStateFunc_t2362323372 * value)
	{
		___U3CU3Ef__amU24cache5D_95 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5D_95, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5E_96() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache5E_96)); }
	inline GetKeyStateFunc_t2362323372 * get_U3CU3Ef__amU24cache5E_96() const { return ___U3CU3Ef__amU24cache5E_96; }
	inline GetKeyStateFunc_t2362323372 ** get_address_of_U3CU3Ef__amU24cache5E_96() { return &___U3CU3Ef__amU24cache5E_96; }
	inline void set_U3CU3Ef__amU24cache5E_96(GetKeyStateFunc_t2362323372 * value)
	{
		___U3CU3Ef__amU24cache5E_96 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5E_96, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5F_97() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache5F_97)); }
	inline GetAxisFunc_t3240405851 * get_U3CU3Ef__amU24cache5F_97() const { return ___U3CU3Ef__amU24cache5F_97; }
	inline GetAxisFunc_t3240405851 ** get_address_of_U3CU3Ef__amU24cache5F_97() { return &___U3CU3Ef__amU24cache5F_97; }
	inline void set_U3CU3Ef__amU24cache5F_97(GetAxisFunc_t3240405851 * value)
	{
		___U3CU3Ef__amU24cache5F_97 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5F_97, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache60_98() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache60_98)); }
	inline CompareFunc_t41355861 * get_U3CU3Ef__amU24cache60_98() const { return ___U3CU3Ef__amU24cache60_98; }
	inline CompareFunc_t41355861 ** get_address_of_U3CU3Ef__amU24cache60_98() { return &___U3CU3Ef__amU24cache60_98; }
	inline void set_U3CU3Ef__amU24cache60_98(CompareFunc_t41355861 * value)
	{
		___U3CU3Ef__amU24cache60_98 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache60_98, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache61_99() { return static_cast<int32_t>(offsetof(UICamera_t189364953_StaticFields, ___U3CU3Ef__amU24cache61_99)); }
	inline CompareFunc_t41355861 * get_U3CU3Ef__amU24cache61_99() const { return ___U3CU3Ef__amU24cache61_99; }
	inline CompareFunc_t41355861 ** get_address_of_U3CU3Ef__amU24cache61_99() { return &___U3CU3Ef__amU24cache61_99; }
	inline void set_U3CU3Ef__amU24cache61_99(CompareFunc_t41355861 * value)
	{
		___U3CU3Ef__amU24cache61_99 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache61_99, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
