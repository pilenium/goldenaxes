﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MoveWaitCenter
struct MoveWaitCenter_t3813388539;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"

// System.Void MoveWaitCenter::.ctor(System.String,System.Single,System.Single,System.Single)
extern "C"  void MoveWaitCenter__ctor_m1045268721 (MoveWaitCenter_t3813388539 * __this, String_t* ___direction0, float ___speed1, float ___idle2, float ___size3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveWaitCenter::Start()
extern "C"  void MoveWaitCenter_Start_m3392184064 (MoveWaitCenter_t3813388539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveWaitCenter::Update()
extern "C"  void MoveWaitCenter_Update_m2084343053 (MoveWaitCenter_t3813388539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveWaitCenter::InitAppear()
extern "C"  void MoveWaitCenter_InitAppear_m346840713 (MoveWaitCenter_t3813388539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveWaitCenter::InitWait()
extern "C"  void MoveWaitCenter_InitWait_m2251905833 (MoveWaitCenter_t3813388539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveWaitCenter::InitDisappear()
extern "C"  void MoveWaitCenter_InitDisappear_m239880657 (MoveWaitCenter_t3813388539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveWaitCenter::UpdateAppear()
extern "C"  void MoveWaitCenter_UpdateAppear_m1933800578 (MoveWaitCenter_t3813388539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveWaitCenter::UpdateWait()
extern "C"  void MoveWaitCenter_UpdateWait_m358587234 (MoveWaitCenter_t3813388539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MoveWaitCenter::UpdateDissapear()
extern "C"  void MoveWaitCenter_UpdateDissapear_m3534353431 (MoveWaitCenter_t3813388539 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
