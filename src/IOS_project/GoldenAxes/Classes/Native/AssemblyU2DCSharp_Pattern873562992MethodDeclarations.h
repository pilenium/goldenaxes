﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pattern
struct Pattern_t873562992;
// System.String
struct String_t;
// Diver
struct Diver_t66044126;
// Monster
struct Monster_t2901270458;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t1755167990;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String968488902.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharp_Diver66044126.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"

// System.Void Pattern::.ctor()
extern "C"  void Pattern__ctor_m4131162619 (Pattern_t873562992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pattern::get_height()
extern "C"  float Pattern_get_height_m3476543149 (Pattern_t873562992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pattern::get_success()
extern "C"  bool Pattern_get_success_m1489968775 (Pattern_t873562992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pattern::set_success(System.Boolean)
extern "C"  void Pattern_set_success_m3994517526 (Pattern_t873562992 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pattern::Reset()
extern "C"  void Pattern_Reset_m1777595560 (Pattern_t873562992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pattern::Clear()
extern "C"  void Pattern_Clear_m1537295910 (Pattern_t873562992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pattern::Destory()
extern "C"  void Pattern_Destory_m3128977709 (Pattern_t873562992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pattern::Init(System.Int32,System.Boolean)
extern "C"  void Pattern_Init_m4038559091 (Pattern_t873562992 * __this, int32_t ___count0, bool ___isMonster1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pattern::SetType(System.String)
extern "C"  void Pattern_SetType_m1488296013 (Pattern_t873562992 * __this, String_t* ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Diver Pattern::AddDiver()
extern "C"  Diver_t66044126 * Pattern_AddDiver_m521494705 (Pattern_t873562992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster Pattern::AddMonster(System.UInt32,UnityEngine.Vector3)
extern "C"  Monster_t2901270458 * Pattern_AddMonster_m2512113156 (Pattern_t873562992 * __this, uint32_t ___monster_id0, Vector3_t3525329789  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pattern::RemoveDiver(Diver)
extern "C"  void Pattern_RemoveDiver_m455503873 (Pattern_t873562992 * __this, Diver_t66044126 * ___diver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pattern::RemoveMonster(Monster)
extern "C"  void Pattern_RemoveMonster_m2098574537 (Pattern_t873562992 * __this, Monster_t2901270458 * ___monster0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pattern::CheckEnd()
extern "C"  void Pattern_CheckEnd_m589599196 (Pattern_t873562992 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Diver Pattern::HitDiver(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Diver_t66044126 * Pattern_HitDiver_m1095507085 (Pattern_t873562992 * __this, Vector3_t3525329789  ___point0, Vector3_t3525329789  ___collider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pattern::getRandomX(System.Single)
extern "C"  float Pattern_getRandomX_m2923937455 (Il2CppObject * __this /* static, unused */, float ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pattern::getStartZ(System.Single)
extern "C"  float Pattern_getStartZ_m3915090648 (Il2CppObject * __this /* static, unused */, float ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pattern::getShuffledX(System.Int32,System.Single,System.Collections.Generic.List`1<System.Single>&)
extern "C"  bool Pattern_getShuffledX_m3790888189 (Il2CppObject * __this /* static, unused */, int32_t ___count0, float ___size1, List_1_t1755167990 ** ___list2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
