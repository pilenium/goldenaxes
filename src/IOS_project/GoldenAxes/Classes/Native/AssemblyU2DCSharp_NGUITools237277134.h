﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioListener
struct AudioListener_t1735598807;
// UnityEngine.AudioClip
struct AudioClip_t3714538611;
// System.Collections.Generic.Dictionary`2<System.Type,System.String>
struct Dictionary_2_t3081036617;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t3227571696;
// UnityEngine.KeyCode[]
struct KeyCodeU5BU5D_t3821076068;
// System.Collections.Generic.Dictionary`2<System.String,UIWidget>
struct Dictionary_2_t2406767464;
// UIPanel
struct UIPanel_t295209936;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_ColorSpace499857360.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NGUITools
struct  NGUITools_t237277134  : public Il2CppObject
{
public:

public:
};

struct NGUITools_t237277134_StaticFields
{
public:
	// UnityEngine.AudioListener NGUITools::mListener
	AudioListener_t1735598807 * ___mListener_0;
	// System.Boolean NGUITools::mLoaded
	bool ___mLoaded_1;
	// System.Single NGUITools::mGlobalVolume
	float ___mGlobalVolume_2;
	// System.Single NGUITools::mLastTimestamp
	float ___mLastTimestamp_3;
	// UnityEngine.AudioClip NGUITools::mLastClip
	AudioClip_t3714538611 * ___mLastClip_4;
	// System.Collections.Generic.Dictionary`2<System.Type,System.String> NGUITools::mTypeNames
	Dictionary_2_t3081036617 * ___mTypeNames_5;
	// UnityEngine.Vector3[] NGUITools::mSides
	Vector3U5BU5D_t3227571696* ___mSides_6;
	// UnityEngine.KeyCode[] NGUITools::keys
	KeyCodeU5BU5D_t3821076068* ___keys_7;
	// System.Collections.Generic.Dictionary`2<System.String,UIWidget> NGUITools::mWidgets
	Dictionary_2_t2406767464 * ___mWidgets_8;
	// UIPanel NGUITools::mRoot
	UIPanel_t295209936 * ___mRoot_9;
	// UnityEngine.GameObject NGUITools::mGo
	GameObject_t4012695102 * ___mGo_10;
	// UnityEngine.ColorSpace NGUITools::mColorSpace
	int32_t ___mColorSpace_11;

public:
	inline static int32_t get_offset_of_mListener_0() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mListener_0)); }
	inline AudioListener_t1735598807 * get_mListener_0() const { return ___mListener_0; }
	inline AudioListener_t1735598807 ** get_address_of_mListener_0() { return &___mListener_0; }
	inline void set_mListener_0(AudioListener_t1735598807 * value)
	{
		___mListener_0 = value;
		Il2CppCodeGenWriteBarrier(&___mListener_0, value);
	}

	inline static int32_t get_offset_of_mLoaded_1() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mLoaded_1)); }
	inline bool get_mLoaded_1() const { return ___mLoaded_1; }
	inline bool* get_address_of_mLoaded_1() { return &___mLoaded_1; }
	inline void set_mLoaded_1(bool value)
	{
		___mLoaded_1 = value;
	}

	inline static int32_t get_offset_of_mGlobalVolume_2() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mGlobalVolume_2)); }
	inline float get_mGlobalVolume_2() const { return ___mGlobalVolume_2; }
	inline float* get_address_of_mGlobalVolume_2() { return &___mGlobalVolume_2; }
	inline void set_mGlobalVolume_2(float value)
	{
		___mGlobalVolume_2 = value;
	}

	inline static int32_t get_offset_of_mLastTimestamp_3() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mLastTimestamp_3)); }
	inline float get_mLastTimestamp_3() const { return ___mLastTimestamp_3; }
	inline float* get_address_of_mLastTimestamp_3() { return &___mLastTimestamp_3; }
	inline void set_mLastTimestamp_3(float value)
	{
		___mLastTimestamp_3 = value;
	}

	inline static int32_t get_offset_of_mLastClip_4() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mLastClip_4)); }
	inline AudioClip_t3714538611 * get_mLastClip_4() const { return ___mLastClip_4; }
	inline AudioClip_t3714538611 ** get_address_of_mLastClip_4() { return &___mLastClip_4; }
	inline void set_mLastClip_4(AudioClip_t3714538611 * value)
	{
		___mLastClip_4 = value;
		Il2CppCodeGenWriteBarrier(&___mLastClip_4, value);
	}

	inline static int32_t get_offset_of_mTypeNames_5() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mTypeNames_5)); }
	inline Dictionary_2_t3081036617 * get_mTypeNames_5() const { return ___mTypeNames_5; }
	inline Dictionary_2_t3081036617 ** get_address_of_mTypeNames_5() { return &___mTypeNames_5; }
	inline void set_mTypeNames_5(Dictionary_2_t3081036617 * value)
	{
		___mTypeNames_5 = value;
		Il2CppCodeGenWriteBarrier(&___mTypeNames_5, value);
	}

	inline static int32_t get_offset_of_mSides_6() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mSides_6)); }
	inline Vector3U5BU5D_t3227571696* get_mSides_6() const { return ___mSides_6; }
	inline Vector3U5BU5D_t3227571696** get_address_of_mSides_6() { return &___mSides_6; }
	inline void set_mSides_6(Vector3U5BU5D_t3227571696* value)
	{
		___mSides_6 = value;
		Il2CppCodeGenWriteBarrier(&___mSides_6, value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___keys_7)); }
	inline KeyCodeU5BU5D_t3821076068* get_keys_7() const { return ___keys_7; }
	inline KeyCodeU5BU5D_t3821076068** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCodeU5BU5D_t3821076068* value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier(&___keys_7, value);
	}

	inline static int32_t get_offset_of_mWidgets_8() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mWidgets_8)); }
	inline Dictionary_2_t2406767464 * get_mWidgets_8() const { return ___mWidgets_8; }
	inline Dictionary_2_t2406767464 ** get_address_of_mWidgets_8() { return &___mWidgets_8; }
	inline void set_mWidgets_8(Dictionary_2_t2406767464 * value)
	{
		___mWidgets_8 = value;
		Il2CppCodeGenWriteBarrier(&___mWidgets_8, value);
	}

	inline static int32_t get_offset_of_mRoot_9() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mRoot_9)); }
	inline UIPanel_t295209936 * get_mRoot_9() const { return ___mRoot_9; }
	inline UIPanel_t295209936 ** get_address_of_mRoot_9() { return &___mRoot_9; }
	inline void set_mRoot_9(UIPanel_t295209936 * value)
	{
		___mRoot_9 = value;
		Il2CppCodeGenWriteBarrier(&___mRoot_9, value);
	}

	inline static int32_t get_offset_of_mGo_10() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mGo_10)); }
	inline GameObject_t4012695102 * get_mGo_10() const { return ___mGo_10; }
	inline GameObject_t4012695102 ** get_address_of_mGo_10() { return &___mGo_10; }
	inline void set_mGo_10(GameObject_t4012695102 * value)
	{
		___mGo_10 = value;
		Il2CppCodeGenWriteBarrier(&___mGo_10, value);
	}

	inline static int32_t get_offset_of_mColorSpace_11() { return static_cast<int32_t>(offsetof(NGUITools_t237277134_StaticFields, ___mColorSpace_11)); }
	inline int32_t get_mColorSpace_11() const { return ___mColorSpace_11; }
	inline int32_t* get_address_of_mColorSpace_11() { return &___mColorSpace_11; }
	inline void set_mColorSpace_11(int32_t value)
	{
		___mColorSpace_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
