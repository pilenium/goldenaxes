﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataAxes
struct DataAxes_t1853147663;
// DataAxe
struct DataAxe_t3107820260;

#include "codegen/il2cpp-codegen.h"

// System.Void DataAxes::.ctor()
extern "C"  void DataAxes__ctor_m3759986028 (DataAxes_t1853147663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataAxes::PreCalculate()
extern "C"  void DataAxes_PreCalculate_m3246782107 (DataAxes_t1853147663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataAxe DataAxes::getData(System.UInt32)
extern "C"  DataAxe_t3107820260 * DataAxes_getData_m1458414431 (DataAxes_t1853147663 * __this, uint32_t ___axe_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
