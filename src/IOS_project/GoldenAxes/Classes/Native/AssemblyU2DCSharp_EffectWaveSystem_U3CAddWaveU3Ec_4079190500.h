﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// EffectWave
struct EffectWave_t3709365322;
// System.Object
struct Il2CppObject;
// EffectWaveSystem
struct EffectWaveSystem_t2871388953;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectWaveSystem/<AddWave>c__Iterator5
struct  U3CAddWaveU3Ec__Iterator5_t4079190500  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 EffectWaveSystem/<AddWave>c__Iterator5::<position>__0
	Vector3_t3525329789  ___U3CpositionU3E__0_0;
	// UnityEngine.GameObject EffectWaveSystem/<AddWave>c__Iterator5::<gameObj>__1
	GameObject_t4012695102 * ___U3CgameObjU3E__1_1;
	// EffectWave EffectWaveSystem/<AddWave>c__Iterator5::<effect>__2
	EffectWave_t3709365322 * ___U3CeffectU3E__2_2;
	// System.Int32 EffectWaveSystem/<AddWave>c__Iterator5::$PC
	int32_t ___U24PC_3;
	// System.Object EffectWaveSystem/<AddWave>c__Iterator5::$current
	Il2CppObject * ___U24current_4;
	// EffectWaveSystem EffectWaveSystem/<AddWave>c__Iterator5::<>f__this
	EffectWaveSystem_t2871388953 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CpositionU3E__0_0() { return static_cast<int32_t>(offsetof(U3CAddWaveU3Ec__Iterator5_t4079190500, ___U3CpositionU3E__0_0)); }
	inline Vector3_t3525329789  get_U3CpositionU3E__0_0() const { return ___U3CpositionU3E__0_0; }
	inline Vector3_t3525329789 * get_address_of_U3CpositionU3E__0_0() { return &___U3CpositionU3E__0_0; }
	inline void set_U3CpositionU3E__0_0(Vector3_t3525329789  value)
	{
		___U3CpositionU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CgameObjU3E__1_1() { return static_cast<int32_t>(offsetof(U3CAddWaveU3Ec__Iterator5_t4079190500, ___U3CgameObjU3E__1_1)); }
	inline GameObject_t4012695102 * get_U3CgameObjU3E__1_1() const { return ___U3CgameObjU3E__1_1; }
	inline GameObject_t4012695102 ** get_address_of_U3CgameObjU3E__1_1() { return &___U3CgameObjU3E__1_1; }
	inline void set_U3CgameObjU3E__1_1(GameObject_t4012695102 * value)
	{
		___U3CgameObjU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgameObjU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CeffectU3E__2_2() { return static_cast<int32_t>(offsetof(U3CAddWaveU3Ec__Iterator5_t4079190500, ___U3CeffectU3E__2_2)); }
	inline EffectWave_t3709365322 * get_U3CeffectU3E__2_2() const { return ___U3CeffectU3E__2_2; }
	inline EffectWave_t3709365322 ** get_address_of_U3CeffectU3E__2_2() { return &___U3CeffectU3E__2_2; }
	inline void set_U3CeffectU3E__2_2(EffectWave_t3709365322 * value)
	{
		___U3CeffectU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CeffectU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CAddWaveU3Ec__Iterator5_t4079190500, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CAddWaveU3Ec__Iterator5_t4079190500, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CAddWaveU3Ec__Iterator5_t4079190500, ___U3CU3Ef__this_5)); }
	inline EffectWaveSystem_t2871388953 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline EffectWaveSystem_t2871388953 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(EffectWaveSystem_t2871388953 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
