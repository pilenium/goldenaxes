﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModeEvent
struct ModeEvent_t4253283991;

#include "codegen/il2cpp-codegen.h"

// System.Void ModeEvent::.ctor()
extern "C"  void ModeEvent__ctor_m2902300852 (ModeEvent_t4253283991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeEvent::Init()
extern "C"  void ModeEvent_Init_m3231747872 (ModeEvent_t4253283991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeEvent::Clear()
extern "C"  void ModeEvent_Clear_m308434143 (ModeEvent_t4253283991 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeEvent::Update(System.Single)
extern "C"  void ModeEvent_Update_m782342514 (ModeEvent_t4253283991 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
