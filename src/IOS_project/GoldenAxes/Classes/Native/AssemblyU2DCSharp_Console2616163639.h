﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Console/Log>
struct List_1_t797035549;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t881716807;
// UnityEngine.GUIContent
struct GUIContent_t2432841515;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "UnityEngine_UnityEngine_KeyCode2371581209.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Console
struct  Console_t2616163639  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.KeyCode Console::toggleKey
	int32_t ___toggleKey_3;
	// System.Collections.Generic.List`1<Console/Log> Console::logs
	List_1_t797035549 * ___logs_4;
	// UnityEngine.Vector2 Console::scrollPosition
	Vector2_t3525329788  ___scrollPosition_5;
	// System.Boolean Console::show
	bool ___show_6;
	// System.Boolean Console::collapse
	bool ___collapse_7;
	// System.String Console::command
	String_t* ___command_8;
	// UnityEngine.Rect Console::windowRect
	Rect_t1525428817  ___windowRect_10;
	// UnityEngine.Rect Console::titleBarRect
	Rect_t1525428817  ___titleBarRect_11;
	// UnityEngine.GUIContent Console::clearLabel
	GUIContent_t2432841515 * ___clearLabel_12;
	// UnityEngine.GUIContent Console::collapseLabel
	GUIContent_t2432841515 * ___collapseLabel_13;

public:
	inline static int32_t get_offset_of_toggleKey_3() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___toggleKey_3)); }
	inline int32_t get_toggleKey_3() const { return ___toggleKey_3; }
	inline int32_t* get_address_of_toggleKey_3() { return &___toggleKey_3; }
	inline void set_toggleKey_3(int32_t value)
	{
		___toggleKey_3 = value;
	}

	inline static int32_t get_offset_of_logs_4() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___logs_4)); }
	inline List_1_t797035549 * get_logs_4() const { return ___logs_4; }
	inline List_1_t797035549 ** get_address_of_logs_4() { return &___logs_4; }
	inline void set_logs_4(List_1_t797035549 * value)
	{
		___logs_4 = value;
		Il2CppCodeGenWriteBarrier(&___logs_4, value);
	}

	inline static int32_t get_offset_of_scrollPosition_5() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___scrollPosition_5)); }
	inline Vector2_t3525329788  get_scrollPosition_5() const { return ___scrollPosition_5; }
	inline Vector2_t3525329788 * get_address_of_scrollPosition_5() { return &___scrollPosition_5; }
	inline void set_scrollPosition_5(Vector2_t3525329788  value)
	{
		___scrollPosition_5 = value;
	}

	inline static int32_t get_offset_of_show_6() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___show_6)); }
	inline bool get_show_6() const { return ___show_6; }
	inline bool* get_address_of_show_6() { return &___show_6; }
	inline void set_show_6(bool value)
	{
		___show_6 = value;
	}

	inline static int32_t get_offset_of_collapse_7() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___collapse_7)); }
	inline bool get_collapse_7() const { return ___collapse_7; }
	inline bool* get_address_of_collapse_7() { return &___collapse_7; }
	inline void set_collapse_7(bool value)
	{
		___collapse_7 = value;
	}

	inline static int32_t get_offset_of_command_8() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___command_8)); }
	inline String_t* get_command_8() const { return ___command_8; }
	inline String_t** get_address_of_command_8() { return &___command_8; }
	inline void set_command_8(String_t* value)
	{
		___command_8 = value;
		Il2CppCodeGenWriteBarrier(&___command_8, value);
	}

	inline static int32_t get_offset_of_windowRect_10() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___windowRect_10)); }
	inline Rect_t1525428817  get_windowRect_10() const { return ___windowRect_10; }
	inline Rect_t1525428817 * get_address_of_windowRect_10() { return &___windowRect_10; }
	inline void set_windowRect_10(Rect_t1525428817  value)
	{
		___windowRect_10 = value;
	}

	inline static int32_t get_offset_of_titleBarRect_11() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___titleBarRect_11)); }
	inline Rect_t1525428817  get_titleBarRect_11() const { return ___titleBarRect_11; }
	inline Rect_t1525428817 * get_address_of_titleBarRect_11() { return &___titleBarRect_11; }
	inline void set_titleBarRect_11(Rect_t1525428817  value)
	{
		___titleBarRect_11 = value;
	}

	inline static int32_t get_offset_of_clearLabel_12() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___clearLabel_12)); }
	inline GUIContent_t2432841515 * get_clearLabel_12() const { return ___clearLabel_12; }
	inline GUIContent_t2432841515 ** get_address_of_clearLabel_12() { return &___clearLabel_12; }
	inline void set_clearLabel_12(GUIContent_t2432841515 * value)
	{
		___clearLabel_12 = value;
		Il2CppCodeGenWriteBarrier(&___clearLabel_12, value);
	}

	inline static int32_t get_offset_of_collapseLabel_13() { return static_cast<int32_t>(offsetof(Console_t2616163639, ___collapseLabel_13)); }
	inline GUIContent_t2432841515 * get_collapseLabel_13() const { return ___collapseLabel_13; }
	inline GUIContent_t2432841515 ** get_address_of_collapseLabel_13() { return &___collapseLabel_13; }
	inline void set_collapseLabel_13(GUIContent_t2432841515 * value)
	{
		___collapseLabel_13 = value;
		Il2CppCodeGenWriteBarrier(&___collapseLabel_13, value);
	}
};

struct Console_t2616163639_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color> Console::logTypeColors
	Dictionary_2_t881716807 * ___logTypeColors_9;

public:
	inline static int32_t get_offset_of_logTypeColors_9() { return static_cast<int32_t>(offsetof(Console_t2616163639_StaticFields, ___logTypeColors_9)); }
	inline Dictionary_2_t881716807 * get_logTypeColors_9() const { return ___logTypeColors_9; }
	inline Dictionary_2_t881716807 ** get_address_of_logTypeColors_9() { return &___logTypeColors_9; }
	inline void set_logTypeColors_9(Dictionary_2_t881716807 * value)
	{
		___logTypeColors_9 = value;
		Il2CppCodeGenWriteBarrier(&___logTypeColors_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
