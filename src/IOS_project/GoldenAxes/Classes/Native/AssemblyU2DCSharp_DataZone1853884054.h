﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataZone
struct  DataZone_t1853884054  : public Il2CppObject
{
public:
	// System.UInt32 DataZone::id
	uint32_t ___id_0;
	// System.String DataZone::name
	String_t* ___name_1;
	// System.String DataZone::type
	String_t* ___type_2;
	// System.String DataZone::material
	String_t* ___material_3;
	// System.Single DataZone::size
	float ___size_4;
	// System.Int32 DataZone::count
	int32_t ___count_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(DataZone_t1853884054, ___id_0)); }
	inline uint32_t get_id_0() const { return ___id_0; }
	inline uint32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(DataZone_t1853884054, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(DataZone_t1853884054, ___type_2)); }
	inline String_t* get_type_2() const { return ___type_2; }
	inline String_t** get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(String_t* value)
	{
		___type_2 = value;
		Il2CppCodeGenWriteBarrier(&___type_2, value);
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(DataZone_t1853884054, ___material_3)); }
	inline String_t* get_material_3() const { return ___material_3; }
	inline String_t** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(String_t* value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier(&___material_3, value);
	}

	inline static int32_t get_offset_of_size_4() { return static_cast<int32_t>(offsetof(DataZone_t1853884054, ___size_4)); }
	inline float get_size_4() const { return ___size_4; }
	inline float* get_address_of_size_4() { return &___size_4; }
	inline void set_size_4(float value)
	{
		___size_4 = value;
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(DataZone_t1853884054, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
