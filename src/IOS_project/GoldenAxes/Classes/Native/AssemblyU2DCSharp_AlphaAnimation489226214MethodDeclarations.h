﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AlphaAnimation
struct AlphaAnimation_t489226214;

#include "codegen/il2cpp-codegen.h"

// System.Void AlphaAnimation::.ctor()
extern "C"  void AlphaAnimation__ctor_m932663157 (AlphaAnimation_t489226214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AlphaAnimation::Awake()
extern "C"  void AlphaAnimation_Awake_m1170268376 (AlphaAnimation_t489226214 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
