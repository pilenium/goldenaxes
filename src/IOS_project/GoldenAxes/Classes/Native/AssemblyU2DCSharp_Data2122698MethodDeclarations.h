﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Data
struct Data_t2122699;
// DataSetting
struct DataSetting_t2172603174;
// DataChapters
struct DataChapters_t2926209968;
// DataStages
struct DataStages_t3269946783;
// DataAxes
struct DataAxes_t1853147663;
// DataMonsters
struct DataMonsters_t1171984835;
// DataZones
struct DataZones_t1635830941;
// DataPatterns
struct DataPatterns_t2737562829;
// DataUser
struct DataUser_t1853738677;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Data_PROGRESS4076515885.h"
#include "mscorlib_System_String968488902.h"

// System.Void Data::.ctor()
extern "C"  void Data__ctor_m1242679825 (Data_t2122699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataSetting Data::get_settings()
extern "C"  DataSetting_t2172603174 * Data_get_settings_m2852704140 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataChapters Data::get_chapters()
extern "C"  DataChapters_t2926209968 * Data_get_chapters_m2935368655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataStages Data::get_stages()
extern "C"  DataStages_t3269946783 * Data_get_stages_m2462436463 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataAxes Data::get_axe()
extern "C"  DataAxes_t1853147663 * Data_get_axe_m1914057446 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataMonsters Data::get_monster()
extern "C"  DataMonsters_t1171984835 * Data_get_monster_m4060264806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataZones Data::get_zone()
extern "C"  DataZones_t1635830941 * Data_get_zone_m1284072190 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataPatterns Data::get_pattern()
extern "C"  DataPatterns_t2737562829 * Data_get_pattern_m1685008806 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataUser Data::get_user()
extern "C"  DataUser_t1853738677 * Data_get_user_m2747519663 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data::Awake()
extern "C"  void Data_Awake_m1480285044 (Data_t2122699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data::OnDestory()
extern "C"  void Data_OnDestory_m2293581092 (Data_t2122699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Data::LoadData()
extern "C"  Il2CppObject * Data_LoadData_m3661499403 (Data_t2122699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data::LoadError(Data/PROGRESS)
extern "C"  void Data_LoadError_m1812381775 (Data_t2122699 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data::LoadComplete()
extern "C"  void Data_LoadComplete_m1919134610 (Data_t2122699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Data::LoadJson(System.String)
extern "C"  String_t* Data_LoadJson_m3427881438 (Data_t2122699 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Data::Remove()
extern "C"  void Data_Remove_m4054779927 (Data_t2122699 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
