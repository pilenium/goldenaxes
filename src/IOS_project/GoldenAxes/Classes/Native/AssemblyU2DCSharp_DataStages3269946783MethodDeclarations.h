﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataStages
struct DataStages_t3269946783;
// DataStage
struct DataStage_t1629502804;
// DataStageChapter
struct DataStageChapter_t1740173401;

#include "codegen/il2cpp-codegen.h"

// System.Void DataStages::.ctor()
extern "C"  void DataStages__ctor_m730598876 (DataStages_t3269946783 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataStage DataStages::GetStageData(System.Int32,System.Int32)
extern "C"  DataStage_t1629502804 * DataStages_GetStageData_m4270061837 (DataStages_t3269946783 * __this, int32_t ___chapter0, int32_t ___stage1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataStageChapter DataStages::GetChapterData(System.Int32)
extern "C"  DataStageChapter_t1740173401 * DataStages_GetChapterData_m76190110 (DataStages_t3269946783 * __this, int32_t ___chapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
