﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Single[]
struct SingleU5BU5D_t1219431280;

#include "AssemblyU2DCSharp_Control2616196413.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SwipeControl
struct  SwipeControl_t1555979491  : public Control_t2616196413
{
public:
	// System.Single SwipeControl::maxHeight
	float ___maxHeight_3;
	// System.Single SwipeControl::powerDecrease
	float ___powerDecrease_4;
	// System.Single[] SwipeControl::powers
	SingleU5BU5D_t1219431280* ___powers_5;

public:
	inline static int32_t get_offset_of_maxHeight_3() { return static_cast<int32_t>(offsetof(SwipeControl_t1555979491, ___maxHeight_3)); }
	inline float get_maxHeight_3() const { return ___maxHeight_3; }
	inline float* get_address_of_maxHeight_3() { return &___maxHeight_3; }
	inline void set_maxHeight_3(float value)
	{
		___maxHeight_3 = value;
	}

	inline static int32_t get_offset_of_powerDecrease_4() { return static_cast<int32_t>(offsetof(SwipeControl_t1555979491, ___powerDecrease_4)); }
	inline float get_powerDecrease_4() const { return ___powerDecrease_4; }
	inline float* get_address_of_powerDecrease_4() { return &___powerDecrease_4; }
	inline void set_powerDecrease_4(float value)
	{
		___powerDecrease_4 = value;
	}

	inline static int32_t get_offset_of_powers_5() { return static_cast<int32_t>(offsetof(SwipeControl_t1555979491, ___powers_5)); }
	inline SingleU5BU5D_t1219431280* get_powers_5() const { return ___powers_5; }
	inline SingleU5BU5D_t1219431280** get_address_of_powers_5() { return &___powers_5; }
	inline void set_powers_5(SingleU5BU5D_t1219431280* value)
	{
		___powers_5 = value;
		Il2CppCodeGenWriteBarrier(&___powers_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
