﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectWave
struct EffectWave_t3709365322;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectWave::.ctor()
extern "C"  void EffectWave__ctor_m2460442513 (EffectWave_t3709365322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectWave::Awake()
extern "C"  void EffectWave_Awake_m2698047732 (EffectWave_t3709365322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectWave::OnDestroy()
extern "C"  void EffectWave_OnDestroy_m3115123722 (EffectWave_t3709365322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectWave::Init(System.Single,System.Single)
extern "C"  void EffectWave_Init_m328236749 (EffectWave_t3709365322 * __this, float ___scale0, float ___target_scale1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectWave::Update()
extern "C"  void EffectWave_Update_m691168668 (EffectWave_t3709365322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
