﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoldMonster
struct GoldMonster_t84400954;
// DataMonster
struct DataMonster_t1423279280;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_DataMonster1423279280.h"

// System.Void GoldMonster::.ctor()
extern "C"  void GoldMonster__ctor_m846045553 (GoldMonster_t84400954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldMonster::Awake()
extern "C"  void GoldMonster_Awake_m1083650772 (GoldMonster_t84400954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldMonster::OnDestroy()
extern "C"  void GoldMonster_OnDestroy_m2797557226 (GoldMonster_t84400954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldMonster::Update()
extern "C"  void GoldMonster_Update_m2184470460 (GoldMonster_t84400954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldMonster::Init(DataMonster)
extern "C"  void GoldMonster_Init_m1664766623 (GoldMonster_t84400954 * __this, DataMonster_t1423279280 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldMonster::OnEndMove()
extern "C"  void GoldMonster_OnEndMove_m2243722428 (GoldMonster_t84400954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoldMonster::Hit(System.Int32)
extern "C"  void GoldMonster_Hit_m948076339 (GoldMonster_t84400954 * __this, int32_t ___damage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
