﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GamePlay
struct GamePlay_t2590336614;
// Stage
struct Stage_t80204510;
// DataAxe
struct DataAxe_t3107820260;
// DataChapter
struct DataChapter_t925677859;
// DataStage
struct DataStage_t1629502804;
// GameUI
struct GameUI_t2125598246;
// PatternManager
struct PatternManager_t2286577181;
// Mode
struct Mode_t2403781;
// GameEvent
struct GameEvent_t2981166504;
// Axe
struct Axe_t66286;
// Diver
struct Diver_t66044126;
// Monster
struct Monster_t2901270458;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mode_TYPE2590522.h"
#include "AssemblyU2DCSharp_Axe66286.h"
#include "AssemblyU2DCSharp_Diver66044126.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"

// System.Void GamePlay::.ctor()
extern "C"  void GamePlay__ctor_m3893439221 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Stage GamePlay::get_stage()
extern "C"  Stage_t80204510 * GamePlay_get_stage_m3096959283 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 GamePlay::get_axe_id()
extern "C"  uint32_t GamePlay_get_axe_id_m4284588703 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataAxe GamePlay::get_dataAxe()
extern "C"  DataAxe_t3107820260 * GamePlay_get_dataAxe_m2446895999 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataChapter GamePlay::get_dataChapter()
extern "C"  DataChapter_t925677859 * GamePlay_get_dataChapter_m1400870973 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataStage GamePlay::get_dataStage()
extern "C"  DataStage_t1629502804 * GamePlay_get_dataStage_m2883595423 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameUI GamePlay::get_ui()
extern "C"  GameUI_t2125598246 * GamePlay_get_ui_m982624707 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PatternManager GamePlay::get_patternManager()
extern "C"  PatternManager_t2286577181 * GamePlay_get_patternManager_m3261677077 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mode GamePlay::get_mode()
extern "C"  Mode_t2403781 * GamePlay_get_mode_m3347757909 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameEvent GamePlay::get_eventFrog()
extern "C"  GameEvent_t2981166504 * GamePlay_get_eventFrog_m3172212061 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::Awake()
extern "C"  void GamePlay_Awake_m4131044440 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::OnDestroy()
extern "C"  void GamePlay_OnDestroy_m1689943406 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::Init()
extern "C"  void GamePlay_Init_m631320767 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::SetChapter(System.Int32)
extern "C"  void GamePlay_SetChapter_m1737945675 (GamePlay_t2590336614 * __this, int32_t ___chapter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::SetStage(System.Int32)
extern "C"  void GamePlay_SetStage_m3602790972 (GamePlay_t2590336614 * __this, int32_t ___stage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::SetAxe(System.UInt32)
extern "C"  void GamePlay_SetAxe_m2967815263 (GamePlay_t2590336614 * __this, uint32_t ___axe_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::SetMode(Mode/TYPE)
extern "C"  void GamePlay_SetMode_m2394895988 (GamePlay_t2590336614 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::AddCoin(System.Int32)
extern "C"  void GamePlay_AddCoin_m1214255030 (GamePlay_t2590336614 * __this, int32_t ___coin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::Update()
extern "C"  void GamePlay_Update_m2164393656 (GamePlay_t2590336614 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::HitWater(Axe)
extern "C"  void GamePlay_HitWater_m3876954609 (GamePlay_t2590336614 * __this, Axe_t66286 * ___axe0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::HitDiver(Axe,Diver)
extern "C"  void GamePlay_HitDiver_m148186040 (GamePlay_t2590336614 * __this, Axe_t66286 * ___axe0, Diver_t66044126 * ___diver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::HitMonster(Monster,Axe)
extern "C"  void GamePlay_HitMonster_m4135026560 (GamePlay_t2590336614 * __this, Monster_t2901270458 * ___monster0, Axe_t66286 * ___axe1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GamePlay::KillMonster(Monster,Axe)
extern "C"  void GamePlay_KillMonster_m2354964455 (GamePlay_t2590336614 * __this, Monster_t2901270458 * ___monster0, Axe_t66286 * ___axe1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
