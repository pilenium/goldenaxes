﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataZone
struct DataZone_t1853884054;

#include "codegen/il2cpp-codegen.h"

// System.Void DataZone::.ctor()
extern "C"  void DataZone__ctor_m3316629701 (DataZone_t1853884054 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
