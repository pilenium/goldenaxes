﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Move2404337.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveWait
struct  MoveWait_t4255274246  : public Move_t2404337
{
public:
	// System.Single MoveWait::m_time
	float ___m_time_2;

public:
	inline static int32_t get_offset_of_m_time_2() { return static_cast<int32_t>(offsetof(MoveWait_t4255274246, ___m_time_2)); }
	inline float get_m_time_2() const { return ___m_time_2; }
	inline float* get_address_of_m_time_2() { return &___m_time_2; }
	inline void set_m_time_2(float value)
	{
		___m_time_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
