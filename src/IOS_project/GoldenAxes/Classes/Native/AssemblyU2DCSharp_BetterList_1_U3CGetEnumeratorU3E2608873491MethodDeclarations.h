﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>
struct U3CGetEnumeratorU3Ec__Iterator3_t2608873491;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m4115823195_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3__ctor_m4115823195(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3__ctor_m4115823195_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector2_t3525329788  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1852087586_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1852087586(__this, method) ((  Vector2_t3525329788  (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1852087586_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m4249108523_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m4249108523(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m4249108523_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m678843577_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m678843577(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m678843577_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3834461144_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3834461144(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3834461144_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector2>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m1762256136_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Reset_m1762256136(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873491 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Reset_m1762256136_gshared)(__this, method)
