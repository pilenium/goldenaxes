﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t397689819 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396096 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396097 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t1372758802 ();
extern "C" void pinvoke_delegate_wrapper_Swapper_t4148259466 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t3698454801 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t1363551830 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t247020293 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t3831560539 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t4066867812 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t836903542 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1585748256 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t943738788 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t2379296192 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t2783314641 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t2286976850 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t379822999 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t3576143653 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t2758142267 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t4291881837 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t827025885 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t4230172209 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t4087051103 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t3008899218 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t397689820 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396098 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t3257378130 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t3726148045 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t1582269749 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t4199006061 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t3235662729 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t749510018 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t2977871350 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t1722466426 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t1908940458 ();
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t1153522766 ();
extern "C" void pinvoke_delegate_wrapper_StateChanged_t1076524291 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t1240057615 ();
extern "C" void pinvoke_delegate_wrapper_UnityAction_t909267611 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t401089076 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t999919624 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t914030844 ();
extern "C" void pinvoke_delegate_wrapper_BannerFailedToLoadDelegate_t2071786927 ();
extern "C" void pinvoke_delegate_wrapper_BannerWasClickedDelegate_t1325188367 ();
extern "C" void pinvoke_delegate_wrapper_BannerWasLoadedDelegate_t69910023 ();
extern "C" void pinvoke_delegate_wrapper_InterstitialWasLoadedDelegate_t2963828231 ();
extern "C" void pinvoke_delegate_wrapper_InterstitialWasViewedDelegate_t402653446 ();
extern "C" void pinvoke_delegate_wrapper_IteratorDelegate_t3855630387 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t3247703954 ();
extern "C" void pinvoke_delegate_wrapper_OnValidateInput_t3303221397 ();
extern "C" void pinvoke_delegate_wrapper_Callback_t4187391077 ();
extern "C" void pinvoke_delegate_wrapper_UpdateFunction_t1249117153 ();
extern "C" void pinvoke_delegate_wrapper_LoadFunction_t2829036286 ();
extern "C" void pinvoke_delegate_wrapper_OnLocalizeNotification_t1642290739 ();
extern "C" void pinvoke_delegate_wrapper_OnFinished_t4089757585 ();
extern "C" void pinvoke_delegate_wrapper_OnFinished_t4089757586 ();
extern "C" void pinvoke_delegate_wrapper_BoolDelegate_t945637040 ();
extern "C" void pinvoke_delegate_wrapper_FloatDelegate_t4185225186 ();
extern "C" void pinvoke_delegate_wrapper_GetAnyKeyFunc_t2938188301 ();
extern "C" void pinvoke_delegate_wrapper_GetAxisFunc_t3240405851 ();
extern "C" void pinvoke_delegate_wrapper_GetKeyStateFunc_t2362323372 ();
extern "C" void pinvoke_delegate_wrapper_GetTouchCallback_t2933503246 ();
extern "C" void pinvoke_delegate_wrapper_GetTouchCountCallback_t345790283 ();
extern "C" void pinvoke_delegate_wrapper_KeyCodeDelegate_t3737044594 ();
extern "C" void pinvoke_delegate_wrapper_MoveDelegate_t757231766 ();
extern "C" void pinvoke_delegate_wrapper_ObjectDelegate_t3413901829 ();
extern "C" void pinvoke_delegate_wrapper_OnCustomInput_t3775009626 ();
extern "C" void pinvoke_delegate_wrapper_OnSchemeChange_t2442067284 ();
extern "C" void pinvoke_delegate_wrapper_OnScreenResize_t3539115999 ();
extern "C" void pinvoke_delegate_wrapper_VectorDelegate_t2458450953 ();
extern "C" void pinvoke_delegate_wrapper_VoidDelegate_t1986138970 ();
extern "C" void pinvoke_delegate_wrapper_OnCenterCallback_t2517112729 ();
extern "C" void pinvoke_delegate_wrapper_OnRenderCallback_t2210118618 ();
extern "C" void pinvoke_delegate_wrapper_BoolDelegate_t945637039 ();
extern "C" void pinvoke_delegate_wrapper_FloatDelegate_t4185225185 ();
extern "C" void pinvoke_delegate_wrapper_KeyCodeDelegate_t3737044593 ();
extern "C" void pinvoke_delegate_wrapper_ObjectDelegate_t3413901828 ();
extern "C" void pinvoke_delegate_wrapper_VectorDelegate_t2458450952 ();
extern "C" void pinvoke_delegate_wrapper_VoidDelegate_t1986138969 ();
extern "C" void pinvoke_delegate_wrapper_OnReposition_t1160738747 ();
extern "C" void pinvoke_delegate_wrapper_OnValidate_t3342145589 ();
extern "C" void pinvoke_delegate_wrapper_ModifierFunc_t3846285915 ();
extern "C" void pinvoke_delegate_wrapper_OnClippingMoved_t1302042450 ();
extern "C" void pinvoke_delegate_wrapper_OnGeometryUpdated_t1871707114 ();
extern "C" void pinvoke_delegate_wrapper_LegacyEvent_t3762302001 ();
extern "C" void pinvoke_delegate_wrapper_OnDragFinished_t2614441317 ();
extern "C" void pinvoke_delegate_wrapper_OnDragNotification_t4185738334 ();
extern "C" void pinvoke_delegate_wrapper_OnReposition_t1160738748 ();
extern "C" void pinvoke_delegate_wrapper_Validate_t2938338614 ();
extern "C" void pinvoke_delegate_wrapper_HitCheck_t3012954453 ();
extern "C" void pinvoke_delegate_wrapper_OnDimensionsChanged_t481648104 ();
extern "C" void pinvoke_delegate_wrapper_OnInitializeItem_t2008047266 ();
extern const Il2CppMethodPointer g_DelegateWrappersManagedToNative[92] = 
{
	pinvoke_delegate_wrapper_PrimalityTest_t397689819,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396096,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396097,
	pinvoke_delegate_wrapper_AppDomainInitializer_t1372758802,
	pinvoke_delegate_wrapper_Swapper_t4148259466,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t3698454801,
	pinvoke_delegate_wrapper_AsyncCallback_t1363551830,
	pinvoke_delegate_wrapper_EventHandler_t247020293,
	pinvoke_delegate_wrapper_ReadDelegate_t3831560539,
	pinvoke_delegate_wrapper_WriteDelegate_t4066867812,
	pinvoke_delegate_wrapper_AddEventAdapter_t836903542,
	pinvoke_delegate_wrapper_MemberFilter_t1585748256,
	pinvoke_delegate_wrapper_GetterAdapter_t943738788,
	pinvoke_delegate_wrapper_TypeFilter_t2379296192,
	pinvoke_delegate_wrapper_ResolveEventHandler_t2783314641,
	pinvoke_delegate_wrapper_CrossContextDelegate_t2286976850,
	pinvoke_delegate_wrapper_HeaderHandler_t379822999,
	pinvoke_delegate_wrapper_CallbackHandler_t3576143653,
	pinvoke_delegate_wrapper_ThreadStart_t2758142267,
	pinvoke_delegate_wrapper_TimerCallback_t4291881837,
	pinvoke_delegate_wrapper_WaitCallback_t827025885,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t4230172209,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t4087051103,
	pinvoke_delegate_wrapper_CostDelegate_t3008899218,
	pinvoke_delegate_wrapper_PrimalityTest_t397689820,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1233396098,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t3257378130,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t3726148045,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t1582269749,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t4199006061,
	pinvoke_delegate_wrapper_LogCallback_t3235662729,
	pinvoke_delegate_wrapper_PCMReaderCallback_t749510018,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t2977871350,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t1722466426,
	pinvoke_delegate_wrapper_CameraCallback_t1908940458,
	pinvoke_delegate_wrapper_WillRenderCanvases_t1153522766,
	pinvoke_delegate_wrapper_StateChanged_t1076524291,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t1240057615,
	pinvoke_delegate_wrapper_UnityAction_t909267611,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t401089076,
	pinvoke_delegate_wrapper_WindowFunction_t999919624,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t914030844,
	pinvoke_delegate_wrapper_BannerFailedToLoadDelegate_t2071786927,
	pinvoke_delegate_wrapper_BannerWasClickedDelegate_t1325188367,
	pinvoke_delegate_wrapper_BannerWasLoadedDelegate_t69910023,
	pinvoke_delegate_wrapper_InterstitialWasLoadedDelegate_t2963828231,
	pinvoke_delegate_wrapper_InterstitialWasViewedDelegate_t402653446,
	pinvoke_delegate_wrapper_IteratorDelegate_t3855630387,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t3247703954,
	pinvoke_delegate_wrapper_OnValidateInput_t3303221397,
	pinvoke_delegate_wrapper_Callback_t4187391077,
	pinvoke_delegate_wrapper_UpdateFunction_t1249117153,
	pinvoke_delegate_wrapper_LoadFunction_t2829036286,
	pinvoke_delegate_wrapper_OnLocalizeNotification_t1642290739,
	pinvoke_delegate_wrapper_OnFinished_t4089757585,
	pinvoke_delegate_wrapper_OnFinished_t4089757586,
	pinvoke_delegate_wrapper_BoolDelegate_t945637040,
	pinvoke_delegate_wrapper_FloatDelegate_t4185225186,
	pinvoke_delegate_wrapper_GetAnyKeyFunc_t2938188301,
	pinvoke_delegate_wrapper_GetAxisFunc_t3240405851,
	pinvoke_delegate_wrapper_GetKeyStateFunc_t2362323372,
	pinvoke_delegate_wrapper_GetTouchCallback_t2933503246,
	pinvoke_delegate_wrapper_GetTouchCountCallback_t345790283,
	pinvoke_delegate_wrapper_KeyCodeDelegate_t3737044594,
	pinvoke_delegate_wrapper_MoveDelegate_t757231766,
	pinvoke_delegate_wrapper_ObjectDelegate_t3413901829,
	pinvoke_delegate_wrapper_OnCustomInput_t3775009626,
	pinvoke_delegate_wrapper_OnSchemeChange_t2442067284,
	pinvoke_delegate_wrapper_OnScreenResize_t3539115999,
	pinvoke_delegate_wrapper_VectorDelegate_t2458450953,
	pinvoke_delegate_wrapper_VoidDelegate_t1986138970,
	pinvoke_delegate_wrapper_OnCenterCallback_t2517112729,
	pinvoke_delegate_wrapper_OnRenderCallback_t2210118618,
	pinvoke_delegate_wrapper_BoolDelegate_t945637039,
	pinvoke_delegate_wrapper_FloatDelegate_t4185225185,
	pinvoke_delegate_wrapper_KeyCodeDelegate_t3737044593,
	pinvoke_delegate_wrapper_ObjectDelegate_t3413901828,
	pinvoke_delegate_wrapper_VectorDelegate_t2458450952,
	pinvoke_delegate_wrapper_VoidDelegate_t1986138969,
	pinvoke_delegate_wrapper_OnReposition_t1160738747,
	pinvoke_delegate_wrapper_OnValidate_t3342145589,
	pinvoke_delegate_wrapper_ModifierFunc_t3846285915,
	pinvoke_delegate_wrapper_OnClippingMoved_t1302042450,
	pinvoke_delegate_wrapper_OnGeometryUpdated_t1871707114,
	pinvoke_delegate_wrapper_LegacyEvent_t3762302001,
	pinvoke_delegate_wrapper_OnDragFinished_t2614441317,
	pinvoke_delegate_wrapper_OnDragNotification_t4185738334,
	pinvoke_delegate_wrapper_OnReposition_t1160738748,
	pinvoke_delegate_wrapper_Validate_t2938338614,
	pinvoke_delegate_wrapper_HitCheck_t3012954453,
	pinvoke_delegate_wrapper_OnDimensionsChanged_t481648104,
	pinvoke_delegate_wrapper_OnInitializeItem_t2008047266,
};
