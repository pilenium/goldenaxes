﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1634065389MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Mode>::.ctor()
#define List_1__ctor_m4022751406(__this, method) ((  void (*) (List_1_t799362748 *, const MethodInfo*))List_1__ctor_m574172797_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mode>::.ctor(System.Int32)
#define List_1__ctor_m4161655048(__this, ___capacity0, method) ((  void (*) (List_1_t799362748 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Mode>::.cctor()
#define List_1__cctor_m383769686(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Mode>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1036166273(__this, method) ((  Il2CppObject* (*) (List_1_t799362748 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2318606061(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t799362748 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Mode>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1937929020(__this, method) ((  Il2CppObject * (*) (List_1_t799362748 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mode>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2740355457(__this, ___item0, method) ((  int32_t (*) (List_1_t799362748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mode>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m1741869407(__this, ___item0, method) ((  bool (*) (List_1_t799362748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mode>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m3632795481(__this, ___item0, method) ((  int32_t (*) (List_1_t799362748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mode>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m914195788(__this, ___index0, ___item1, method) ((  void (*) (List_1_t799362748 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mode>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1892382812(__this, ___item0, method) ((  void (*) (List_1_t799362748 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Mode>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3754677920(__this, method) ((  bool (*) (List_1_t799362748 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mode>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m231609757(__this, method) ((  bool (*) (List_1_t799362748 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mode>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m742545999(__this, method) ((  Il2CppObject * (*) (List_1_t799362748 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mode>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m724481038(__this, method) ((  bool (*) (List_1_t799362748 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mode>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m2278494507(__this, method) ((  bool (*) (List_1_t799362748 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Mode>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m1687745558(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t799362748 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mode>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2982776355(__this, ___index0, ___value1, method) ((  void (*) (List_1_t799362748 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Mode>::Add(T)
#define List_1_Add_m580986728(__this, ___item0, method) ((  void (*) (List_1_t799362748 *, Mode_t2403781 *, const MethodInfo*))List_1_Add_m642669291_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mode>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m2673775011(__this, ___newCount0, method) ((  void (*) (List_1_t799362748 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Mode>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m640130209(__this, ___collection0, method) ((  void (*) (List_1_t799362748 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Mode>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m4196069729(__this, ___enumerable0, method) ((  void (*) (List_1_t799362748 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Mode>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m2470110358(__this, ___collection0, method) ((  void (*) (List_1_t799362748 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Mode>::AsReadOnly()
#define List_1_AsReadOnly_m4213560495(__this, method) ((  ReadOnlyCollection_1_t3165549127 * (*) (List_1_t799362748 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mode>::Clear()
#define List_1_Clear_m2837410402(__this, method) ((  void (*) (List_1_t799362748 *, const MethodInfo*))List_1_Clear_m454602559_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Mode>::Contains(T)
#define List_1_Contains_m1485869140(__this, ___item0, method) ((  bool (*) (List_1_t799362748 *, Mode_t2403781 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mode>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m1805845848(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t799362748 *, ModeU5BU5D_t1506020306*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Mode>::Find(System.Predicate`1<T>)
#define List_1_Find_m974824238(__this, ___match0, method) ((  Mode_t2403781 * (*) (List_1_t799362748 *, Predicate_1_t573367677 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mode>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3530792075(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t573367677 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Mode>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m3604407656(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t799362748 *, int32_t, int32_t, Predicate_1_t573367677 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Mode>::GetEnumerator()
#define List_1_GetEnumerator_m2716796817(__this, method) ((  Enumerator_t3180113036  (*) (List_1_t799362748 *, const MethodInfo*))List_1_GetEnumerator_m2326457258_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mode>::IndexOf(T)
#define List_1_IndexOf_m4252952868(__this, ___item0, method) ((  int32_t (*) (List_1_t799362748 *, Mode_t2403781 *, const MethodInfo*))List_1_IndexOf_m1752303327_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Mode>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3534488495(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t799362748 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Mode>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m1552213032(__this, ___index0, method) ((  void (*) (List_1_t799362748 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mode>::Insert(System.Int32,T)
#define List_1_Insert_m2758971855(__this, ___index0, ___item1, method) ((  void (*) (List_1_t799362748 *, int32_t, Mode_t2403781 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Mode>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m1511475972(__this, ___collection0, method) ((  void (*) (List_1_t799362748 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Mode>::Remove(T)
#define List_1_Remove_m3348452687(__this, ___item0, method) ((  bool (*) (List_1_t799362748 *, Mode_t2403781 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Mode>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m2072935911(__this, ___match0, method) ((  int32_t (*) (List_1_t799362748 *, Predicate_1_t573367677 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Mode>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m632824725(__this, ___index0, method) ((  void (*) (List_1_t799362748 *, int32_t, const MethodInfo*))List_1_RemoveAt_m1301016856_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mode>::Reverse()
#define List_1_Reverse_m1109850295(__this, method) ((  void (*) (List_1_t799362748 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mode>::Sort()
#define List_1_Sort_m1106958923(__this, method) ((  void (*) (List_1_t799362748 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mode>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3439938654(__this, ___comparison0, method) ((  void (*) (List_1_t799362748 *, Comparison_1_t2706078655 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Mode>::ToArray()
#define List_1_ToArray_m2318900688(__this, method) ((  ModeU5BU5D_t1506020306* (*) (List_1_t799362748 *, const MethodInfo*))List_1_ToArray_m238588755_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mode>::TrimExcess()
#define List_1_TrimExcess_m3566327204(__this, method) ((  void (*) (List_1_t799362748 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Mode>::get_Capacity()
#define List_1_get_Capacity_m3288182612(__this, method) ((  int32_t (*) (List_1_t799362748 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Mode>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m4178931125(__this, ___value0, method) ((  void (*) (List_1_t799362748 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Mode>::get_Count()
#define List_1_get_Count_m2809827543(__this, method) ((  int32_t (*) (List_1_t799362748 *, const MethodInfo*))List_1_get_Count_m2599103100_gshared)(__this, method)
// T System.Collections.Generic.List`1<Mode>::get_Item(System.Int32)
#define List_1_get_Item_m2404782779(__this, ___index0, method) ((  Mode_t2403781 * (*) (List_1_t799362748 *, int32_t, const MethodInfo*))List_1_get_Item_m2771401372_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Mode>::set_Item(System.Int32,T)
#define List_1_set_Item_m3186727654(__this, ___index0, ___value1, method) ((  void (*) (List_1_t799362748 *, int32_t, Mode_t2403781 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
