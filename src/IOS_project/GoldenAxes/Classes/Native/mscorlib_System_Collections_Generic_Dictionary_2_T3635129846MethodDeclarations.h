﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>
struct Transform_1_t3635129846;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_370248105.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::.ctor(System.Object,System.IntPtr)
extern "C"  void Transform_1__ctor_m96705034_gshared (Transform_1_t3635129846 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Transform_1__ctor_m96705034(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t3635129846 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m96705034_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::Invoke(TKey,TValue)
extern "C"  KeyValuePair_2_t370248105  Transform_1_Invoke_m724084654_gshared (Transform_1_t3635129846 * __this, int32_t ___key0, Color_t1588175760  ___value1, const MethodInfo* method);
#define Transform_1_Invoke_m724084654(__this, ___key0, ___value1, method) ((  KeyValuePair_2_t370248105  (*) (Transform_1_t3635129846 *, int32_t, Color_t1588175760 , const MethodInfo*))Transform_1_Invoke_m724084654_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Transform_1_BeginInvoke_m2665983065_gshared (Transform_1_t3635129846 * __this, int32_t ___key0, Color_t1588175760  ___value1, AsyncCallback_t1363551830 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define Transform_1_BeginInvoke_m2665983065(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t3635129846 *, int32_t, Color_t1588175760 , AsyncCallback_t1363551830 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2665983065_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.LogType,UnityEngine.Color,System.Collections.Generic.KeyValuePair`2<UnityEngine.LogType,UnityEngine.Color>>::EndInvoke(System.IAsyncResult)
extern "C"  KeyValuePair_2_t370248105  Transform_1_EndInvoke_m4095868956_gshared (Transform_1_t3635129846 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Transform_1_EndInvoke_m4095868956(__this, ___result0, method) ((  KeyValuePair_2_t370248105  (*) (Transform_1_t3635129846 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m4095868956_gshared)(__this, ___result0, method)
