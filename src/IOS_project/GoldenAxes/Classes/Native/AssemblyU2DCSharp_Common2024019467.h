﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Common
struct  Common_t2024019467  : public Il2CppObject
{
public:

public:
};

struct Common_t2024019467_StaticFields
{
public:
	// System.Single Common::POS_SURFACE_EFFECT_Y
	float ___POS_SURFACE_EFFECT_Y_0;
	// System.Single Common::POS_SURFACE_Y
	float ___POS_SURFACE_Y_1;
	// System.Single Common::POS_MONSTER_Y
	float ___POS_MONSTER_Y_2;
	// System.Single Common::POS_EFFECT_Y
	float ___POS_EFFECT_Y_3;
	// System.Single Common::POS_ZONE_Y
	float ___POS_ZONE_Y_4;
	// System.Single Common::POS_WAVE_Y
	float ___POS_WAVE_Y_5;
	// System.Single Common::POS_WATER_Y
	float ___POS_WATER_Y_6;
	// System.Single Common::POS_COIN_Y
	float ___POS_COIN_Y_7;
	// UnityEngine.Vector3 Common::SCALE_COIN
	Vector3_t3525329789  ___SCALE_COIN_8;
	// System.Single Common::SIZE_OF_DIVER
	float ___SIZE_OF_DIVER_9;
	// System.Single Common::GAP_OF_DIVER
	float ___GAP_OF_DIVER_10;
	// System.String Common::TAG_MONSTER
	String_t* ___TAG_MONSTER_11;
	// System.String Common::TAG_ZONE
	String_t* ___TAG_ZONE_12;
	// System.String Common::TAG_AXE
	String_t* ___TAG_AXE_13;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Common::<>f__switch$map4
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map4_14;

public:
	inline static int32_t get_offset_of_POS_SURFACE_EFFECT_Y_0() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___POS_SURFACE_EFFECT_Y_0)); }
	inline float get_POS_SURFACE_EFFECT_Y_0() const { return ___POS_SURFACE_EFFECT_Y_0; }
	inline float* get_address_of_POS_SURFACE_EFFECT_Y_0() { return &___POS_SURFACE_EFFECT_Y_0; }
	inline void set_POS_SURFACE_EFFECT_Y_0(float value)
	{
		___POS_SURFACE_EFFECT_Y_0 = value;
	}

	inline static int32_t get_offset_of_POS_SURFACE_Y_1() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___POS_SURFACE_Y_1)); }
	inline float get_POS_SURFACE_Y_1() const { return ___POS_SURFACE_Y_1; }
	inline float* get_address_of_POS_SURFACE_Y_1() { return &___POS_SURFACE_Y_1; }
	inline void set_POS_SURFACE_Y_1(float value)
	{
		___POS_SURFACE_Y_1 = value;
	}

	inline static int32_t get_offset_of_POS_MONSTER_Y_2() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___POS_MONSTER_Y_2)); }
	inline float get_POS_MONSTER_Y_2() const { return ___POS_MONSTER_Y_2; }
	inline float* get_address_of_POS_MONSTER_Y_2() { return &___POS_MONSTER_Y_2; }
	inline void set_POS_MONSTER_Y_2(float value)
	{
		___POS_MONSTER_Y_2 = value;
	}

	inline static int32_t get_offset_of_POS_EFFECT_Y_3() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___POS_EFFECT_Y_3)); }
	inline float get_POS_EFFECT_Y_3() const { return ___POS_EFFECT_Y_3; }
	inline float* get_address_of_POS_EFFECT_Y_3() { return &___POS_EFFECT_Y_3; }
	inline void set_POS_EFFECT_Y_3(float value)
	{
		___POS_EFFECT_Y_3 = value;
	}

	inline static int32_t get_offset_of_POS_ZONE_Y_4() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___POS_ZONE_Y_4)); }
	inline float get_POS_ZONE_Y_4() const { return ___POS_ZONE_Y_4; }
	inline float* get_address_of_POS_ZONE_Y_4() { return &___POS_ZONE_Y_4; }
	inline void set_POS_ZONE_Y_4(float value)
	{
		___POS_ZONE_Y_4 = value;
	}

	inline static int32_t get_offset_of_POS_WAVE_Y_5() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___POS_WAVE_Y_5)); }
	inline float get_POS_WAVE_Y_5() const { return ___POS_WAVE_Y_5; }
	inline float* get_address_of_POS_WAVE_Y_5() { return &___POS_WAVE_Y_5; }
	inline void set_POS_WAVE_Y_5(float value)
	{
		___POS_WAVE_Y_5 = value;
	}

	inline static int32_t get_offset_of_POS_WATER_Y_6() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___POS_WATER_Y_6)); }
	inline float get_POS_WATER_Y_6() const { return ___POS_WATER_Y_6; }
	inline float* get_address_of_POS_WATER_Y_6() { return &___POS_WATER_Y_6; }
	inline void set_POS_WATER_Y_6(float value)
	{
		___POS_WATER_Y_6 = value;
	}

	inline static int32_t get_offset_of_POS_COIN_Y_7() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___POS_COIN_Y_7)); }
	inline float get_POS_COIN_Y_7() const { return ___POS_COIN_Y_7; }
	inline float* get_address_of_POS_COIN_Y_7() { return &___POS_COIN_Y_7; }
	inline void set_POS_COIN_Y_7(float value)
	{
		___POS_COIN_Y_7 = value;
	}

	inline static int32_t get_offset_of_SCALE_COIN_8() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___SCALE_COIN_8)); }
	inline Vector3_t3525329789  get_SCALE_COIN_8() const { return ___SCALE_COIN_8; }
	inline Vector3_t3525329789 * get_address_of_SCALE_COIN_8() { return &___SCALE_COIN_8; }
	inline void set_SCALE_COIN_8(Vector3_t3525329789  value)
	{
		___SCALE_COIN_8 = value;
	}

	inline static int32_t get_offset_of_SIZE_OF_DIVER_9() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___SIZE_OF_DIVER_9)); }
	inline float get_SIZE_OF_DIVER_9() const { return ___SIZE_OF_DIVER_9; }
	inline float* get_address_of_SIZE_OF_DIVER_9() { return &___SIZE_OF_DIVER_9; }
	inline void set_SIZE_OF_DIVER_9(float value)
	{
		___SIZE_OF_DIVER_9 = value;
	}

	inline static int32_t get_offset_of_GAP_OF_DIVER_10() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___GAP_OF_DIVER_10)); }
	inline float get_GAP_OF_DIVER_10() const { return ___GAP_OF_DIVER_10; }
	inline float* get_address_of_GAP_OF_DIVER_10() { return &___GAP_OF_DIVER_10; }
	inline void set_GAP_OF_DIVER_10(float value)
	{
		___GAP_OF_DIVER_10 = value;
	}

	inline static int32_t get_offset_of_TAG_MONSTER_11() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___TAG_MONSTER_11)); }
	inline String_t* get_TAG_MONSTER_11() const { return ___TAG_MONSTER_11; }
	inline String_t** get_address_of_TAG_MONSTER_11() { return &___TAG_MONSTER_11; }
	inline void set_TAG_MONSTER_11(String_t* value)
	{
		___TAG_MONSTER_11 = value;
		Il2CppCodeGenWriteBarrier(&___TAG_MONSTER_11, value);
	}

	inline static int32_t get_offset_of_TAG_ZONE_12() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___TAG_ZONE_12)); }
	inline String_t* get_TAG_ZONE_12() const { return ___TAG_ZONE_12; }
	inline String_t** get_address_of_TAG_ZONE_12() { return &___TAG_ZONE_12; }
	inline void set_TAG_ZONE_12(String_t* value)
	{
		___TAG_ZONE_12 = value;
		Il2CppCodeGenWriteBarrier(&___TAG_ZONE_12, value);
	}

	inline static int32_t get_offset_of_TAG_AXE_13() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___TAG_AXE_13)); }
	inline String_t* get_TAG_AXE_13() const { return ___TAG_AXE_13; }
	inline String_t** get_address_of_TAG_AXE_13() { return &___TAG_AXE_13; }
	inline void set_TAG_AXE_13(String_t* value)
	{
		___TAG_AXE_13 = value;
		Il2CppCodeGenWriteBarrier(&___TAG_AXE_13, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4_14() { return static_cast<int32_t>(offsetof(Common_t2024019467_StaticFields, ___U3CU3Ef__switchU24map4_14)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map4_14() const { return ___U3CU3Ef__switchU24map4_14; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map4_14() { return &___U3CU3Ef__switchU24map4_14; }
	inline void set_U3CU3Ef__switchU24map4_14(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map4_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
