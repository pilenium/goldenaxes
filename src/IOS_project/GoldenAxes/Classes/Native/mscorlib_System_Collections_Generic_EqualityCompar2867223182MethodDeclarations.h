﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.LogType>
struct DefaultComparer_t2867223182;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LogType3529269451.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.LogType>::.ctor()
extern "C"  void DefaultComparer__ctor_m1568078213_gshared (DefaultComparer_t2867223182 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1568078213(__this, method) ((  void (*) (DefaultComparer_t2867223182 *, const MethodInfo*))DefaultComparer__ctor_m1568078213_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.LogType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3041370758_gshared (DefaultComparer_t2867223182 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3041370758(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2867223182 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m3041370758_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.LogType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2674070486_gshared (DefaultComparer_t2867223182 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2674070486(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2867223182 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m2674070486_gshared)(__this, ___x0, ___y1, method)
