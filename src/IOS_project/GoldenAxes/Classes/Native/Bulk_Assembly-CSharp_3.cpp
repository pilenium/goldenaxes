﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UIScrollView/OnDragNotification
struct OnDragNotification_t4185738334;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// UIShowControlScheme
struct UIShowControlScheme_t2031965489;
// UISlider
struct UISlider_t657469589;
// UnityEngine.Collider
struct Collider_t955670625;
// UnityEngine.Collider2D
struct Collider2D_t1890038195;
// UIWidget
struct UIWidget_t769069560;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UISliderColors
struct UISliderColors_t1894798949;
// UIProgressBar
struct UIProgressBar_t168062834;
// UIBasicSprite
struct UIBasicSprite_t2501337439;
// UISnapshotPoint
struct UISnapshotPoint_t3024846840;
// UISoundVolume
struct UISoundVolume_t1043221973;
// UISprite
struct UISprite_t661437049;
// UnityEngine.Material
struct Material_t1886596500;
// UIAtlas
struct UIAtlas_t281921111;
// System.String
struct String_t;
// UISpriteData
struct UISpriteData_t3578345923;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t727330505;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t727330504;
// BetterList`1<UnityEngine.Color>
struct BetterList_1_t3085143772;
// UISpriteAnimation
struct UISpriteAnimation_t4279777547;
// UIStorageSlot
struct UIStorageSlot_t2805916645;
// InvGameItem
struct InvGameItem_t1588794646;
// UIStretch
struct UIStretch_t3439076817;
// UnityEngine.Animation
struct Animation_t350396337;
// UIPanel
struct UIPanel_t295209936;
// UIRoot
struct UIRoot_t2503447958;
// UITable
struct UITable_t298892698;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t1081512082;
// SpringPosition
struct SpringPosition_t3802689142;
// UIScrollView
struct UIScrollView_t2113479878;
// UITable/OnReposition
struct OnReposition_t1160738748;
// UITextList
struct UITextList_t736798239;
// BetterList`1<UITextList/Paragraph>
struct BetterList_1_t1155257498;
// UILabel
struct UILabel_t291504320;
// UITextList/Paragraph
struct Paragraph_t3953256782;
// UITexture
struct UITexture_t3903132647;
// UnityEngine.Texture
struct Texture_t1769722184;
// UnityEngine.Shader
struct Shader_t3998140498;
// UIToggle
struct UIToggle_t688812808;
// UITweener[]
struct UITweenerU5BU5D_t996366285;
// System.Object[]
struct ObjectU5BU5D_t11523773;
// UIToggle/Validate
struct Validate_t2938338614;
// UIToggledComponents
struct UIToggledComponents_t3531934354;
// UIToggledObjects
struct UIToggledObjects_t793825048;
// UITooltip
struct UITooltip_t4180872911;
// UIWidget[]
struct UIWidgetU5BU5D_t4236988201;
// UITweener
struct UITweener_t105489188;
// EventDelegate/Callback
struct Callback_t4187391077;
// EventDelegate
struct EventDelegate_t4004424223;
// UIViewport
struct UIViewport_t2937361242;
// UnityEngine.Camera
struct Camera_t3533968274;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t2210118618;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t3227571696;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t262790558;
// UnityEngine.Transform
struct Transform_t284553113;
// BetterList`1<UnityEngine.Vector4>
struct BetterList_1_t727330506;
// UIWidget/HitCheck
struct HitCheck_t3012954453;
// UIWidget/OnDimensionsChanged
struct OnDimensionsChanged_t481648104;
// UIWidget/OnPostFillCallback
struct OnPostFillCallback_t294433735;
// UIWidgetContainer
struct UIWidgetContainer_t1520767337;
// UIWrapContent
struct UIWrapContent_t33025435;
// UIWrapContent/OnInitializeItem
struct OnInitializeItem_t2008047266;
// WindowAutoYaw
struct WindowAutoYaw_t1835239440;
// WindowDragTilt
struct WindowDragTilt_t3072284225;
// Zone
struct Zone_t2791372;
// DataZone
struct DataZone_t1853884054;
// UnityEngine.Renderer
struct Renderer_t1092684080;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;
// ZoneCircle
struct ZoneCircle_t627455932;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_UIScrollView_Movement4255933647.h"
#include "AssemblyU2DCSharp_UIScrollView_Movement4255933647MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIScrollView_OnDragNotification4185738334.h"
#include "AssemblyU2DCSharp_UIScrollView_OnDragNotification4185738334MethodDeclarations.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_Void2779279689.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharp_UIScrollView_ShowCondition3591302782.h"
#include "AssemblyU2DCSharp_UIScrollView_ShowCondition3591302782MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIShowControlScheme2031965489.h"
#include "AssemblyU2DCSharp_UIShowControlScheme2031965489MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "mscorlib_System_Boolean211005341.h"
#include "AssemblyU2DCSharp_UICamera_OnSchemeChange2442067284MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"
#include "AssemblyU2DCSharp_UICamera189364953MethodDeclarations.h"
#include "AssemblyU2DCSharp_UICamera_OnSchemeChange2442067284.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "AssemblyU2DCSharp_UICamera_ControlScheme1667267906.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "AssemblyU2DCSharp_UISlider657469589.h"
#include "AssemblyU2DCSharp_UISlider657469589MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIProgressBar168062834MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "AssemblyU2DCSharp_UISlider_Direction1041377119.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider955670625MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider955670625.h"
#include "UnityEngine_UnityEngine_Collider2D1890038195.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "AssemblyU2DCSharp_UIProgressBar168062834.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIProgressBar_FillDirection2100946204.h"
#include "AssemblyU2DCSharp_UIEventListener1278105402MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIEventListener_BoolDelegate945637039MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIEventListener_VectorDelegate2458450952MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIRect2503437976MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIEventListener1278105402.h"
#include "AssemblyU2DCSharp_UIEventListener_BoolDelegate945637039.h"
#include "AssemblyU2DCSharp_UIEventListener_VectorDelegate2458450952.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharp_UIProgressBar_OnDragFinished2614441317MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "AssemblyU2DCSharp_UIProgressBar_OnDragFinished2614441317.h"
#include "AssemblyU2DCSharp_UISlider_Direction1041377119MethodDeclarations.h"
#include "AssemblyU2DCSharp_UISliderColors1894798949.h"
#include "AssemblyU2DCSharp_UISliderColors1894798949MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color1588175760MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Color1588175760.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439.h"
#include "AssemblyU2DCSharp_UIBasicSprite2501337439MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf1597001355MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget769069560MethodDeclarations.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "AssemblyU2DCSharp_UISnapshotPoint3024846840.h"
#include "AssemblyU2DCSharp_UISnapshotPoint3024846840MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_String968488902.h"
#include "AssemblyU2DCSharp_UISoundVolume1043221973.h"
#include "AssemblyU2DCSharp_UISoundVolume1043221973MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUITools237277134MethodDeclarations.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback4187391077MethodDeclarations.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen506415896.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback4187391077.h"
#include "AssemblyU2DCSharp_EventDelegate4004424223.h"
#include "AssemblyU2DCSharp_UISprite661437049MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material1886596500.h"
#include "AssemblyU2DCSharp_UIAtlas281921111MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"
#include "AssemblyU2DCSharp_UISpriteData3578345923.h"
#include "mscorlib_System_Collections_Generic_List_1_gen80337596.h"
#include "mscorlib_System_Collections_Generic_List_1_gen80337596MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "AssemblyU2DCSharp_UIBasicSprite_AdvancedType2936397468.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"
#include "UnityEngine_UnityEngine_Vector43525329790MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Type2622298.h"
#include "AssemblyU2DCSharp_UIBasicSprite_Flip2192525.h"
#include "UnityEngine_UnityEngine_Debug1588791936MethodDeclarations.h"
#include "AssemblyU2DCSharp_UISpriteData3578345923MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture1769722184.h"
#include "AssemblyU2DCSharp_BetterList_1_gen727330505.h"
#include "AssemblyU2DCSharp_BetterList_1_gen727330504.h"
#include "AssemblyU2DCSharp_BetterList_1_gen3085143772.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "AssemblyU2DCSharp_NGUIMath3886757557MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback294433735MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "UnityEngine_UnityEngine_Texture1769722184MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_OnPostFillCallback294433735.h"
#include "AssemblyU2DCSharp_UISpriteAnimation4279777547.h"
#include "AssemblyU2DCSharp_UISpriteAnimation4279777547MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1765447871.h"
#include "UnityEngine_UnityEngine_Application450040189MethodDeclarations.h"
#include "AssemblyU2DCSharp_RealTime3499460011MethodDeclarations.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIStorageSlot2805916645.h"
#include "AssemblyU2DCSharp_UIStorageSlot2805916645MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIItemSlot2918167141MethodDeclarations.h"
#include "AssemblyU2DCSharp_InvGameItem1588794646.h"
#include "AssemblyU2DCSharp_UIItemStorage913329332MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIItemStorage913329332.h"
#include "AssemblyU2DCSharp_UIStretch3439076817.h"
#include "AssemblyU2DCSharp_UIStretch3439076817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "AssemblyU2DCSharp_UICamera_OnScreenResize3539115999MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animation350396337.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_UICamera_OnScreenResize3539115999.h"
#include "AssemblyU2DCSharp_UIRoot2503447958.h"
#include "AssemblyU2DCSharp_NGUITools237277134.h"
#include "UnityEngine_UnityEngine_Animation350396337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIPanel295209936MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIRoot2503447958MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Bounds3518514978.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "AssemblyU2DCSharp_UIStretch_Style80227729.h"
#include "AssemblyU2DCSharp_UIDrawCall_Clipping983261410.h"
#include "AssemblyU2DCSharp_UIStretch_Style80227729MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITable298892698.h"
#include "AssemblyU2DCSharp_UITable298892698MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidgetContainer1520767337MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1081512082.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1081512082MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen2988227989MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITable_Sorting3935038820.h"
#include "AssemblyU2DCSharp_UIGrid2503122938.h"
#include "AssemblyU2DCSharp_UIGrid2503122938MethodDeclarations.h"
#include "mscorlib_System_Comparison_1_gen2988227989.h"
#include "UnityEngine_UnityEngine_Behaviour3120504042.h"
#include "AssemblyU2DCSharp_SpringPosition3802689142.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot77126690.h"
#include "AssemblyU2DCSharp_UITable_Direction1041377119.h"
#include "AssemblyU2DCSharp_UITable_OnReposition1160738747MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITable_OnReposition1160738747.h"
#include "AssemblyU2DCSharp_UITable_Direction1041377119MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITable_Sorting3935038820MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITextList736798239.h"
#include "AssemblyU2DCSharp_UITextList736798239MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char2778706699.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2792955402MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g2792955402.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1155257498.h"
#include "AssemblyU2DCSharp_BetterList_1_gen1155257498MethodDeclarations.h"
#include "AssemblyU2DCSharp_UILabel291504320MethodDeclarations.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UILabel_Overflow594286626.h"
#include "AssemblyU2DCSharp_UITextList_Style80227729.h"
#include "AssemblyU2DCSharp_UITextList_Paragraph3953256782MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITextList_Paragraph3953256782.h"
#include "AssemblyU2DCSharp_NGUIText3886970074MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIScrollBar2839103954MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIScrollBar2839103954.h"
#include "AssemblyU2DCSharp_NGUIText3886970074.h"
#include "Assembly-CSharp_ArrayTypes.h"
#include "mscorlib_System_Text_StringBuilder3822575854MethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder3822575854.h"
#include "AssemblyU2DCSharp_UITextList_Style80227729MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "AssemblyU2DCSharp_UITexture3903132647MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material1886596500MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIDrawCall913273974.h"
#include "UnityEngine_UnityEngine_Shader3998140498.h"
#include "UnityEngine_UnityEngine_Shader3998140498MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"
#include "AssemblyU2DCSharp_UIToggle688812808MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen506415896MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2185780820MethodDeclarations.h"
#include "AssemblyU2DCSharp_BetterList_1_gen2185780820.h"
#include "AssemblyU2DCSharp_UIToggle_Validate2938338614MethodDeclarations.h"
#include "AssemblyU2DCSharp_TweenAlpha2920325587MethodDeclarations.h"
#include "AssemblyU2DCSharp_ActiveAnimation557316862MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITweener105489188MethodDeclarations.h"
#include "AssemblyU2DCSharp_ActiveAnimation557316862.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_UIToggle_Validate2938338614.h"
#include "AssemblyU2DCSharp_TweenAlpha2920325587.h"
#include "UnityEngine_UnityEngine_SendMessageOptions2623293100.h"
#include "UnityEngine_UnityEngine_Animator792326996.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction259989387.h"
#include "AssemblyU2DCSharp_AnimationOrTween_EnableCondition3725892324.h"
#include "AssemblyU2DCSharp_AnimationOrTween_DisableConditio3573771103.h"
#include "AssemblyU2DCSharp_UIToggledComponents3531934354.h"
#include "AssemblyU2DCSharp_UIToggledComponents3531934354MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3809231424.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3809231424MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIToggledObjects793825048.h"
#include "AssemblyU2DCSharp_UIToggledObjects793825048MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen514686775.h"
#include "mscorlib_System_Collections_Generic_List_1_gen514686775MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITooltip4180872911.h"
#include "AssemblyU2DCSharp_UITooltip4180872911MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe2095052507MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve3342907448MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe2095052507.h"
#include "UnityEngine_UnityEngine_AnimationCurve3342907448.h"
#include "UnityEngine_UnityEngine_Time1525492538MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITweener_Style80227729.h"
#include "AssemblyU2DCSharp_UITweener_Method2301279489.h"
#include "AssemblyU2DCSharp_UITweener_Method2301279489MethodDeclarations.h"
#include "AssemblyU2DCSharp_UITweener_Style80227729MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIViewport2937361242.h"
#include "AssemblyU2DCSharp_UIViewport2937361242MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIGeometry3586695974MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIGeometry3586695974.h"
#include "AssemblyU2DCSharp_UIDrawCall_OnRenderCallback2210118618.h"
#include "mscorlib_System_MulticastDelegate2585444626MethodDeclarations.h"
#include "mscorlib_System_MulticastDelegate2585444626.h"
#include "AssemblyU2DCSharp_UIWidget_AspectRatioSource2223856750.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint109622203.h"
#include "mscorlib_System_NotImplementedException1091014741MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_NotImplementedException1091014741.h"
#include "UnityEngine_UnityEngine_BoxCollider131631884.h"
#include "UnityEngine_UnityEngine_BoxCollider2D262790558.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint109622203MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x4277289660MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Matrix4x4277289660.h"
#include "AssemblyU2DCSharp_UIWidget_OnDimensionsChanged481648104MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_OnDimensionsChanged481648104.h"
#include "AssemblyU2DCSharp_BetterList_1_gen727330506.h"
#include "AssemblyU2DCSharp_UIWidget_AspectRatioSource2223856750MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck3012954453.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck3012954453MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot77126690MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWidgetContainer1520767337.h"
#include "AssemblyU2DCSharp_UIWrapContent33025435.h"
#include "AssemblyU2DCSharp_UIWrapContent33025435MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIPanel_OnClippingMoved1302042450MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIPanel_OnClippingMoved1302042450.h"
#include "AssemblyU2DCSharp_UIWrapContent_OnInitializeItem2008047266MethodDeclarations.h"
#include "AssemblyU2DCSharp_UIWrapContent_OnInitializeItem2008047266.h"
#include "AssemblyU2DCSharp_WindowAutoYaw1835239440.h"
#include "AssemblyU2DCSharp_WindowAutoYaw1835239440MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"
#include "AssemblyU2DCSharp_WindowDragTilt3072284225.h"
#include "AssemblyU2DCSharp_WindowDragTilt3072284225MethodDeclarations.h"
#include "AssemblyU2DCSharp_Zone2791372.h"
#include "AssemblyU2DCSharp_Zone2791372MethodDeclarations.h"
#include "AssemblyU2DCSharp_DataZone1853884054.h"
#include "UnityEngine_UnityEngine_Resources1543782994MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546MethodDeclarations.h"
#include "AssemblyU2DCSharp_GamePlay2590336614MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random3963434288MethodDeclarations.h"
#include "AssemblyU2DCSharp_Data2122698MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080.h"
#include "AssemblyU2DCSharp_DataChapter925677859.h"
#include "UnityEngine_UnityEngine_PrimitiveType3380664558.h"
#include "UnityEngine_UnityEngine_Rigidbody1972007546.h"
#include "AssemblyU2DCSharp_GamePlay2590336614.h"
#include "AssemblyU2DCSharp_DataSetting2172603174.h"
#include "AssemblyU2DCSharp_Common2024019467.h"
#include "AssemblyU2DCSharp_Common2024019467MethodDeclarations.h"
#include "AssemblyU2DCSharp_ZoneCircle627455932.h"
#include "AssemblyU2DCSharp_ZoneCircle627455932MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider>()
#define Component_GetComponent_TisCollider_t955670625_m3246438266(__this, method) ((  Collider_t955670625 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider2D>()
#define Component_GetComponent_TisCollider2D_t1890038195_m1670690088(__this, method) ((  Collider2D_t1890038195 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIWidget>()
#define Component_GetComponent_TisUIWidget_t769069560_m2158946701(__this, method) ((  UIWidget_t769069560 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIProgressBar>()
#define Component_GetComponent_TisUIProgressBar_t168062834_m3103886343(__this, method) ((  UIProgressBar_t168062834 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIBasicSprite>()
#define Component_GetComponent_TisUIBasicSprite_t2501337439_m3936925434(__this, method) ((  UIBasicSprite_t2501337439 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UISlider>()
#define Component_GetComponent_TisUISlider_t657469589_m1788897744(__this, method) ((  UISlider_t657469589 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UISprite>()
#define Component_GetComponent_TisUISprite_t661437049_m4019381612(__this, method) ((  UISprite_t661437049 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animation>()
#define Component_GetComponent_TisAnimation_t350396337_m2546983788(__this, method) ((  Animation_t350396337 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIPanel>()
#define Component_GetComponent_TisUIPanel_t295209936_m1836641897(__this, method) ((  UIPanel_t295209936 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 NGUITools::FindInParents<System.Object>(UnityEngine.GameObject)
extern "C"  Il2CppObject * NGUITools_FindInParents_TisIl2CppObject_m485072941_gshared (Il2CppObject * __this /* static, unused */, GameObject_t4012695102 * p0, const MethodInfo* method);
#define NGUITools_FindInParents_TisIl2CppObject_m485072941(__this /* static, unused */, p0, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))NGUITools_FindInParents_TisIl2CppObject_m485072941_gshared)(__this /* static, unused */, p0, method)
// !!0 NGUITools::FindInParents<UIRoot>(UnityEngine.GameObject)
#define NGUITools_FindInParents_TisUIRoot_t2503447958_m283231053(__this /* static, unused */, p0, method) ((  UIRoot_t2503447958 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))NGUITools_FindInParents_TisIl2CppObject_m485072941_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UIWidget>()
#define GameObject_GetComponent_TisUIWidget_t769069560_m3896075237(__this, method) ((  UIWidget_t769069560 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UIPanel>()
#define GameObject_GetComponent_TisUIPanel_t295209936_m1477036305(__this, method) ((  UIPanel_t295209936 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 NGUITools::FindInParents<UIPanel>(UnityEngine.GameObject)
#define NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243(__this /* static, unused */, p0, method) ((  UIPanel_t295209936 * (*) (Il2CppObject * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))NGUITools_FindInParents_TisIl2CppObject_m485072941_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<SpringPosition>()
#define Component_GetComponent_TisSpringPosition_t3802689142_m1260857295(__this, method) ((  SpringPosition_t3802689142 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UIScrollView>()
#define Component_GetComponent_TisUIScrollView_t2113479878_m1783064831(__this, method) ((  UIScrollView_t2113479878 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UILabel>()
#define Component_GetComponentInChildren_TisUILabel_t291504320_m2847097109(__this, method) ((  UILabel_t291504320 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>(System.Boolean)
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m1896280001_gshared (Component_t2126946602 * __this, bool p0, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m1896280001(__this, p0, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m1896280001_gshared)(__this, p0, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UITweener>(System.Boolean)
#define Component_GetComponentsInChildren_TisUITweener_t105489188_m3761589691(__this, p0, method) ((  UITweenerU5BU5D_t996366285* (*) (Component_t2126946602 *, bool, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m1896280001_gshared)(__this, p0, method)
// !!0 UnityEngine.Component::GetComponent<UIToggle>()
#define Component_GetComponent_TisUIToggle_t688812808_m3526831741(__this, method) ((  UIToggle_t688812808 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t11523773* Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponentsInChildren_TisIl2CppObject_m2266473418(__this, method) ((  ObjectU5BU5D_t11523773* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponentsInChildren<UIWidget>()
#define Component_GetComponentsInChildren_TisUIWidget_t769069560_m3945863294(__this, method) ((  UIWidgetU5BU5D_t4236988201* (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m2266473418_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t3533968274_m3804104198(__this, method) ((  Camera_t3533968274 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519(__this, method) ((  BoxCollider2D_t262790558 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Transform>()
#define Component_GetComponent_TisTransform_t284553113_m811718087(__this, method) ((  Transform_t284553113 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t1092684080_m4102086307(__this, method) ((  Renderer_t1092684080 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Transform>()
#define GameObject_GetComponent_TisTransform_t284553113_m3795369772(__this, method) ((  Transform_t284553113 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m337943659(__this, method) ((  Il2CppObject * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.Rigidbody>()
#define GameObject_AddComponent_TisRigidbody_t1972007546_m686365494(__this, method) ((  Rigidbody_t1972007546 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UIScrollView/OnDragNotification::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDragNotification__ctor_m2448747166 (OnDragNotification_t4185738334 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIScrollView/OnDragNotification::Invoke()
extern "C"  void OnDragNotification_Invoke_m2316738680 (OnDragNotification_t4185738334 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnDragNotification_Invoke_m2316738680((OnDragNotification_t4185738334 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnDragNotification_t4185738334(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UIScrollView/OnDragNotification::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnDragNotification_BeginInvoke_m1993632787 (OnDragNotification_t4185738334 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UIScrollView/OnDragNotification::EndInvoke(System.IAsyncResult)
extern "C"  void OnDragNotification_EndInvoke_m1026710446 (OnDragNotification_t4185738334 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UIShowControlScheme::.ctor()
extern "C"  void UIShowControlScheme__ctor_m1044317530 (UIShowControlScheme_t2031965489 * __this, const MethodInfo* method)
{
	{
		__this->set_controller_5((bool)1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIShowControlScheme::OnEnable()
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern Il2CppClass* OnSchemeChange_t2442067284_il2cpp_TypeInfo_var;
extern const MethodInfo* UIShowControlScheme_OnScheme_m2547129230_MethodInfo_var;
extern const uint32_t UIShowControlScheme_OnEnable_m592022508_MetadataUsageId;
extern "C"  void UIShowControlScheme_OnEnable_m592022508 (UIShowControlScheme_t2031965489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIShowControlScheme_OnEnable_m592022508_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		OnSchemeChange_t2442067284 * L_0 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_onSchemeChange_47();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIShowControlScheme_OnScheme_m2547129230_MethodInfo_var);
		OnSchemeChange_t2442067284 * L_2 = (OnSchemeChange_t2442067284 *)il2cpp_codegen_object_new(OnSchemeChange_t2442067284_il2cpp_TypeInfo_var);
		OnSchemeChange__ctor_m4246793249(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3660574010 * L_3 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->set_onSchemeChange_47(((OnSchemeChange_t2442067284 *)CastclassSealed(L_3, OnSchemeChange_t2442067284_il2cpp_TypeInfo_var)));
		UIShowControlScheme_OnScheme_m2547129230(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIShowControlScheme::OnDisable()
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern Il2CppClass* OnSchemeChange_t2442067284_il2cpp_TypeInfo_var;
extern const MethodInfo* UIShowControlScheme_OnScheme_m2547129230_MethodInfo_var;
extern const uint32_t UIShowControlScheme_OnDisable_m1613765825_MetadataUsageId;
extern "C"  void UIShowControlScheme_OnDisable_m1613765825 (UIShowControlScheme_t2031965489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIShowControlScheme_OnDisable_m1613765825_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		OnSchemeChange_t2442067284 * L_0 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_onSchemeChange_47();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIShowControlScheme_OnScheme_m2547129230_MethodInfo_var);
		OnSchemeChange_t2442067284 * L_2 = (OnSchemeChange_t2442067284 *)il2cpp_codegen_object_new(OnSchemeChange_t2442067284_il2cpp_TypeInfo_var);
		OnSchemeChange__ctor_m4246793249(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3660574010 * L_3 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->set_onSchemeChange_47(((OnSchemeChange_t2442067284 *)CastclassSealed(L_3, OnSchemeChange_t2442067284_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIShowControlScheme::OnScheme()
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern const uint32_t UIShowControlScheme_OnScheme_m2547129230_MetadataUsageId;
extern "C"  void UIShowControlScheme_OnScheme_m2547129230 (UIShowControlScheme_t2031965489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIShowControlScheme_OnScheme_m2547129230_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		GameObject_t4012695102 * L_0 = __this->get_target_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0068;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		int32_t L_2 = UICamera_get_currentScheme_m1314774372(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_0033;
		}
	}
	{
		GameObject_t4012695102 * L_4 = __this->get_target_2();
		bool L_5 = __this->get_mouse_3();
		NullCheck(L_4);
		GameObject_SetActive_m3538205401(L_4, L_5, /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_0033:
	{
		int32_t L_6 = V_0;
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0050;
		}
	}
	{
		GameObject_t4012695102 * L_7 = __this->get_target_2();
		bool L_8 = __this->get_touch_4();
		NullCheck(L_7);
		GameObject_SetActive_m3538205401(L_7, L_8, /*hidden argument*/NULL);
		goto IL_0068;
	}

IL_0050:
	{
		int32_t L_9 = V_0;
		if ((!(((uint32_t)L_9) == ((uint32_t)2))))
		{
			goto IL_0068;
		}
	}
	{
		GameObject_t4012695102 * L_10 = __this->get_target_2();
		bool L_11 = __this->get_controller_5();
		NullCheck(L_10);
		GameObject_SetActive_m3538205401(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0068:
	{
		return;
	}
}
// System.Void UISlider::.ctor()
extern "C"  void UISlider__ctor_m3947193766 (UISlider_t657469589 * __this, const MethodInfo* method)
{
	{
		__this->set_rawValue_17((1.0f));
		__this->set_direction_18(2);
		UIProgressBar__ctor_m2180461113(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UISlider::get_isColliderEnabled()
extern const MethodInfo* Component_GetComponent_TisCollider_t955670625_m3246438266_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider2D_t1890038195_m1670690088_MethodInfo_var;
extern const uint32_t UISlider_get_isColliderEnabled_m1481818026_MetadataUsageId;
extern "C"  bool UISlider_get_isColliderEnabled_m1481818026 (UISlider_t657469589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISlider_get_isColliderEnabled_m1481818026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Collider_t955670625 * V_0 = NULL;
	Collider2D_t1890038195 * V_1 = NULL;
	int32_t G_B5_0 = 0;
	{
		Collider_t955670625 * L_0 = Component_GetComponent_TisCollider_t955670625_m3246438266(__this, /*hidden argument*/Component_GetComponent_TisCollider_t955670625_m3246438266_MethodInfo_var);
		V_0 = L_0;
		Collider_t955670625 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Collider_t955670625 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Collider_get_enabled_m4167357801(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t1890038195 * L_5 = Component_GetComponent_TisCollider2D_t1890038195_m1670690088(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_t1890038195_m1670690088_MethodInfo_var);
		V_1 = L_5;
		Collider2D_t1890038195 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		Collider2D_t1890038195 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = Behaviour_get_enabled_m1239363704(L_8, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_9));
		goto IL_0036;
	}

IL_0035:
	{
		G_B5_0 = 0;
	}

IL_0036:
	{
		return (bool)G_B5_0;
	}
}
// System.Single UISlider::get_sliderValue()
extern "C"  float UISlider_get_sliderValue_m2734035647 (UISlider_t657469589 * __this, const MethodInfo* method)
{
	{
		float L_0 = UIProgressBar_get_value_m1994834827(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UISlider::set_sliderValue(System.Single)
extern "C"  void UISlider_set_sliderValue_m1958627316 (UISlider_t657469589 * __this, float ___value0, const MethodInfo* method)
{
	{
		float L_0 = ___value0;
		UIProgressBar_set_value_m1993536704(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UISlider::get_inverted()
extern "C"  bool UISlider_get_inverted_m1442361008 (UISlider_t657469589 * __this, const MethodInfo* method)
{
	{
		bool L_0 = UIProgressBar_get_isInverted_m2196456159(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UISlider::set_inverted(System.Boolean)
extern "C"  void UISlider_set_inverted_m1069482215 (UISlider_t657469589 * __this, bool ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UISlider::Upgrade()
extern const MethodInfo* Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var;
extern const uint32_t UISlider_Upgrade_m1626925344_MetadataUsageId;
extern "C"  void UISlider_Upgrade_m1626925344 (UISlider_t657469589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISlider_Upgrade_m1626925344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UISlider_t657469589 * G_B6_0 = NULL;
	UISlider_t657469589 * G_B5_0 = NULL;
	int32_t G_B7_0 = 0;
	UISlider_t657469589 * G_B7_1 = NULL;
	UISlider_t657469589 * G_B10_0 = NULL;
	UISlider_t657469589 * G_B9_0 = NULL;
	int32_t G_B11_0 = 0;
	UISlider_t657469589 * G_B11_1 = NULL;
	{
		int32_t L_0 = __this->get_direction_18();
		if ((((int32_t)L_0) == ((int32_t)2)))
		{
			goto IL_0081;
		}
	}
	{
		float L_1 = __this->get_rawValue_17();
		((UIProgressBar_t168062834 *)__this)->set_mValue_7(L_1);
		Transform_t284553113 * L_2 = __this->get_foreground_16();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		Transform_t284553113 * L_4 = __this->get_foreground_16();
		NullCheck(L_4);
		UIWidget_t769069560 * L_5 = Component_GetComponent_TisUIWidget_t769069560_m2158946701(L_4, /*hidden argument*/Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var);
		((UIProgressBar_t168062834 *)__this)->set_mFG_6(L_5);
	}

IL_003a:
	{
		int32_t L_6 = __this->get_direction_18();
		if (L_6)
		{
			goto IL_0062;
		}
	}
	{
		bool L_7 = __this->get_mInverted_19();
		G_B5_0 = __this;
		if (!L_7)
		{
			G_B6_0 = __this;
			goto IL_0057;
		}
	}
	{
		G_B7_0 = 1;
		G_B7_1 = G_B5_0;
		goto IL_0058;
	}

IL_0057:
	{
		G_B7_0 = 0;
		G_B7_1 = G_B6_0;
	}

IL_0058:
	{
		NullCheck(G_B7_1);
		((UIProgressBar_t168062834 *)G_B7_1)->set_mFill_8(G_B7_0);
		goto IL_007a;
	}

IL_0062:
	{
		bool L_8 = __this->get_mInverted_19();
		G_B9_0 = __this;
		if (!L_8)
		{
			G_B10_0 = __this;
			goto IL_0074;
		}
	}
	{
		G_B11_0 = 3;
		G_B11_1 = G_B9_0;
		goto IL_0075;
	}

IL_0074:
	{
		G_B11_0 = 2;
		G_B11_1 = G_B10_0;
	}

IL_0075:
	{
		NullCheck(G_B11_1);
		((UIProgressBar_t168062834 *)G_B11_1)->set_mFill_8(G_B11_0);
	}

IL_007a:
	{
		__this->set_direction_18(2);
	}

IL_0081:
	{
		return;
	}
}
// System.Void UISlider::OnStart()
extern Il2CppClass* BoolDelegate_t945637039_il2cpp_TypeInfo_var;
extern Il2CppClass* VectorDelegate_t2458450952_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t955670625_m3246438266_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider2D_t1890038195_m1670690088_MethodInfo_var;
extern const MethodInfo* UISlider_OnPressBackground_m3877979919_MethodInfo_var;
extern const MethodInfo* UISlider_OnDragBackground_m943415947_MethodInfo_var;
extern const MethodInfo* UISlider_OnPressForeground_m2545644890_MethodInfo_var;
extern const MethodInfo* UISlider_OnDragForeground_m1951230752_MethodInfo_var;
extern const uint32_t UISlider_OnStart_m4178549511_MetadataUsageId;
extern "C"  void UISlider_OnStart_m4178549511 (UISlider_t657469589 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISlider_OnStart_m4178549511_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t4012695102 * V_0 = NULL;
	UIEventListener_t1278105402 * V_1 = NULL;
	UIEventListener_t1278105402 * V_2 = NULL;
	GameObject_t4012695102 * G_B5_0 = NULL;
	{
		UIWidget_t769069560 * L_0 = ((UIProgressBar_t168062834 *)__this)->get_mBG_5();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		UIWidget_t769069560 * L_2 = ((UIProgressBar_t168062834 *)__this)->get_mBG_5();
		NullCheck(L_2);
		Collider_t955670625 * L_3 = Component_GetComponent_TisCollider_t955670625_m3246438266(L_2, /*hidden argument*/Component_GetComponent_TisCollider_t955670625_m3246438266_MethodInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_003d;
		}
	}
	{
		UIWidget_t769069560 * L_5 = ((UIProgressBar_t168062834 *)__this)->get_mBG_5();
		NullCheck(L_5);
		Collider2D_t1890038195 * L_6 = Component_GetComponent_TisCollider2D_t1890038195_m1670690088(L_5, /*hidden argument*/Component_GetComponent_TisCollider2D_t1890038195_m1670690088_MethodInfo_var);
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004d;
		}
	}

IL_003d:
	{
		UIWidget_t769069560 * L_8 = ((UIProgressBar_t168062834 *)__this)->get_mBG_5();
		NullCheck(L_8);
		GameObject_t4012695102 * L_9 = Component_get_gameObject_m1170635899(L_8, /*hidden argument*/NULL);
		G_B5_0 = L_9;
		goto IL_0053;
	}

IL_004d:
	{
		GameObject_t4012695102 * L_10 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		G_B5_0 = L_10;
	}

IL_0053:
	{
		V_0 = G_B5_0;
		GameObject_t4012695102 * L_11 = V_0;
		UIEventListener_t1278105402 * L_12 = UIEventListener_Get_m1638309430(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		UIEventListener_t1278105402 * L_13 = V_1;
		UIEventListener_t1278105402 * L_14 = L_13;
		NullCheck(L_14);
		BoolDelegate_t945637039 * L_15 = L_14->get_onPress_7();
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)UISlider_OnPressBackground_m3877979919_MethodInfo_var);
		BoolDelegate_t945637039 * L_17 = (BoolDelegate_t945637039 *)il2cpp_codegen_object_new(BoolDelegate_t945637039_il2cpp_TypeInfo_var);
		BoolDelegate__ctor_m705420811(L_17, __this, L_16, /*hidden argument*/NULL);
		Delegate_t3660574010 * L_18 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		NullCheck(L_14);
		L_14->set_onPress_7(((BoolDelegate_t945637039 *)CastclassSealed(L_18, BoolDelegate_t945637039_il2cpp_TypeInfo_var)));
		UIEventListener_t1278105402 * L_19 = V_1;
		UIEventListener_t1278105402 * L_20 = L_19;
		NullCheck(L_20);
		VectorDelegate_t2458450952 * L_21 = L_20->get_onDrag_11();
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)UISlider_OnDragBackground_m943415947_MethodInfo_var);
		VectorDelegate_t2458450952 * L_23 = (VectorDelegate_t2458450952 *)il2cpp_codegen_object_new(VectorDelegate_t2458450952_il2cpp_TypeInfo_var);
		VectorDelegate__ctor_m3847473060(L_23, __this, L_22, /*hidden argument*/NULL);
		Delegate_t3660574010 * L_24 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		NullCheck(L_20);
		L_20->set_onDrag_11(((VectorDelegate_t2458450952 *)CastclassSealed(L_24, VectorDelegate_t2458450952_il2cpp_TypeInfo_var)));
		Transform_t284553113 * L_25 = ((UIProgressBar_t168062834 *)__this)->get_thumb_4();
		bool L_26 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_25, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_015d;
		}
	}
	{
		Transform_t284553113 * L_27 = ((UIProgressBar_t168062834 *)__this)->get_thumb_4();
		NullCheck(L_27);
		Collider_t955670625 * L_28 = Component_GetComponent_TisCollider_t955670625_m3246438266(L_27, /*hidden argument*/Component_GetComponent_TisCollider_t955670625_m3246438266_MethodInfo_var);
		bool L_29 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_28, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_29)
		{
			goto IL_00dc;
		}
	}
	{
		Transform_t284553113 * L_30 = ((UIProgressBar_t168062834 *)__this)->get_thumb_4();
		NullCheck(L_30);
		Collider2D_t1890038195 * L_31 = Component_GetComponent_TisCollider2D_t1890038195_m1670690088(L_30, /*hidden argument*/Component_GetComponent_TisCollider2D_t1890038195_m1670690088_MethodInfo_var);
		bool L_32 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_31, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_015d;
		}
	}

IL_00dc:
	{
		UIWidget_t769069560 * L_33 = ((UIProgressBar_t168062834 *)__this)->get_mFG_6();
		bool L_34 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_33, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_0108;
		}
	}
	{
		Transform_t284553113 * L_35 = ((UIProgressBar_t168062834 *)__this)->get_thumb_4();
		UIWidget_t769069560 * L_36 = ((UIProgressBar_t168062834 *)__this)->get_mFG_6();
		NullCheck(L_36);
		Transform_t284553113 * L_37 = UIRect_get_cachedTransform_m1757861860(L_36, /*hidden argument*/NULL);
		bool L_38 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_35, L_37, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_015d;
		}
	}

IL_0108:
	{
		Transform_t284553113 * L_39 = ((UIProgressBar_t168062834 *)__this)->get_thumb_4();
		NullCheck(L_39);
		GameObject_t4012695102 * L_40 = Component_get_gameObject_m1170635899(L_39, /*hidden argument*/NULL);
		UIEventListener_t1278105402 * L_41 = UIEventListener_Get_m1638309430(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		V_2 = L_41;
		UIEventListener_t1278105402 * L_42 = V_2;
		UIEventListener_t1278105402 * L_43 = L_42;
		NullCheck(L_43);
		BoolDelegate_t945637039 * L_44 = L_43->get_onPress_7();
		IntPtr_t L_45;
		L_45.set_m_value_0((void*)(void*)UISlider_OnPressForeground_m2545644890_MethodInfo_var);
		BoolDelegate_t945637039 * L_46 = (BoolDelegate_t945637039 *)il2cpp_codegen_object_new(BoolDelegate_t945637039_il2cpp_TypeInfo_var);
		BoolDelegate__ctor_m705420811(L_46, __this, L_45, /*hidden argument*/NULL);
		Delegate_t3660574010 * L_47 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_44, L_46, /*hidden argument*/NULL);
		NullCheck(L_43);
		L_43->set_onPress_7(((BoolDelegate_t945637039 *)CastclassSealed(L_47, BoolDelegate_t945637039_il2cpp_TypeInfo_var)));
		UIEventListener_t1278105402 * L_48 = V_2;
		UIEventListener_t1278105402 * L_49 = L_48;
		NullCheck(L_49);
		VectorDelegate_t2458450952 * L_50 = L_49->get_onDrag_11();
		IntPtr_t L_51;
		L_51.set_m_value_0((void*)(void*)UISlider_OnDragForeground_m1951230752_MethodInfo_var);
		VectorDelegate_t2458450952 * L_52 = (VectorDelegate_t2458450952 *)il2cpp_codegen_object_new(VectorDelegate_t2458450952_il2cpp_TypeInfo_var);
		VectorDelegate__ctor_m3847473060(L_52, __this, L_51, /*hidden argument*/NULL);
		Delegate_t3660574010 * L_53 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_50, L_52, /*hidden argument*/NULL);
		NullCheck(L_49);
		L_49->set_onDrag_11(((VectorDelegate_t2458450952 *)CastclassSealed(L_53, VectorDelegate_t2458450952_il2cpp_TypeInfo_var)));
	}

IL_015d:
	{
		return;
	}
}
// System.Void UISlider::OnPressBackground(UnityEngine.GameObject,System.Boolean)
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern const uint32_t UISlider_OnPressBackground_m3877979919_MetadataUsageId;
extern "C"  void UISlider_OnPressBackground_m3877979919 (UISlider_t657469589 * __this, GameObject_t4012695102 * ___go0, bool ___isPressed1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISlider_OnPressBackground_m3877979919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		int32_t L_0 = UICamera_get_currentScheme_m1314774372(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		Camera_t3533968274 * L_1 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_currentCamera_46();
		((UIProgressBar_t168062834 *)__this)->set_mCam_12(L_1);
		Vector2_t3525329788  L_2 = UICamera_get_lastEventPosition_m1966259817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = UIProgressBar_ScreenToValue_m487337549(__this, L_2, /*hidden argument*/NULL);
		UIProgressBar_set_value_m1993536704(__this, L_3, /*hidden argument*/NULL);
		bool L_4 = ___isPressed1;
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		OnDragFinished_t2614441317 * L_5 = ((UIProgressBar_t168062834 *)__this)->get_onDragFinished_3();
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		OnDragFinished_t2614441317 * L_6 = ((UIProgressBar_t168062834 *)__this)->get_onDragFinished_3();
		NullCheck(L_6);
		OnDragFinished_Invoke_m545280739(L_6, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Void UISlider::OnDragBackground(UnityEngine.GameObject,UnityEngine.Vector2)
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern const uint32_t UISlider_OnDragBackground_m943415947_MetadataUsageId;
extern "C"  void UISlider_OnDragBackground_m943415947 (UISlider_t657469589 * __this, GameObject_t4012695102 * ___go0, Vector2_t3525329788  ___delta1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISlider_OnDragBackground_m943415947_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		int32_t L_0 = UICamera_get_currentScheme_m1314774372(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		Camera_t3533968274 * L_1 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_currentCamera_46();
		((UIProgressBar_t168062834 *)__this)->set_mCam_12(L_1);
		Vector2_t3525329788  L_2 = UICamera_get_lastEventPosition_m1966259817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = UIProgressBar_ScreenToValue_m487337549(__this, L_2, /*hidden argument*/NULL);
		UIProgressBar_set_value_m1993536704(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISlider::OnPressForeground(UnityEngine.GameObject,System.Boolean)
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern const uint32_t UISlider_OnPressForeground_m2545644890_MetadataUsageId;
extern "C"  void UISlider_OnPressForeground_m2545644890 (UISlider_t657469589 * __this, GameObject_t4012695102 * ___go0, bool ___isPressed1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISlider_OnPressForeground_m2545644890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UISlider_t657469589 * G_B5_0 = NULL;
	UISlider_t657469589 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	UISlider_t657469589 * G_B6_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		int32_t L_0 = UICamera_get_currentScheme_m1314774372(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		Camera_t3533968274 * L_1 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_currentCamera_46();
		((UIProgressBar_t168062834 *)__this)->set_mCam_12(L_1);
		bool L_2 = ___isPressed1;
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		UIWidget_t769069560 * L_3 = ((UIProgressBar_t168062834 *)__this)->get_mFG_6();
		bool L_4 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if (!L_4)
		{
			G_B5_0 = __this;
			goto IL_0039;
		}
	}
	{
		G_B6_0 = (0.0f);
		G_B6_1 = G_B4_0;
		goto IL_004b;
	}

IL_0039:
	{
		float L_5 = UIProgressBar_get_value_m1994834827(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		Vector2_t3525329788  L_6 = UICamera_get_lastEventPosition_m1966259817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_7 = UIProgressBar_ScreenToValue_m487337549(__this, L_6, /*hidden argument*/NULL);
		G_B6_0 = ((float)((float)L_5-(float)L_7));
		G_B6_1 = G_B5_0;
	}

IL_004b:
	{
		NullCheck(G_B6_1);
		((UIProgressBar_t168062834 *)G_B6_1)->set_mOffset_13(G_B6_0);
		goto IL_006b;
	}

IL_0055:
	{
		OnDragFinished_t2614441317 * L_8 = ((UIProgressBar_t168062834 *)__this)->get_onDragFinished_3();
		if (!L_8)
		{
			goto IL_006b;
		}
	}
	{
		OnDragFinished_t2614441317 * L_9 = ((UIProgressBar_t168062834 *)__this)->get_onDragFinished_3();
		NullCheck(L_9);
		OnDragFinished_Invoke_m545280739(L_9, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void UISlider::OnDragForeground(UnityEngine.GameObject,UnityEngine.Vector2)
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern const uint32_t UISlider_OnDragForeground_m1951230752_MetadataUsageId;
extern "C"  void UISlider_OnDragForeground_m1951230752 (UISlider_t657469589 * __this, GameObject_t4012695102 * ___go0, Vector2_t3525329788  ___delta1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISlider_OnDragForeground_m1951230752_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		int32_t L_0 = UICamera_get_currentScheme_m1314774372(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		Camera_t3533968274 * L_1 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_currentCamera_46();
		((UIProgressBar_t168062834 *)__this)->set_mCam_12(L_1);
		float L_2 = ((UIProgressBar_t168062834 *)__this)->get_mOffset_13();
		Vector2_t3525329788  L_3 = UICamera_get_lastEventPosition_m1966259817(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = UIProgressBar_ScreenToValue_m487337549(__this, L_3, /*hidden argument*/NULL);
		UIProgressBar_set_value_m1993536704(__this, ((float)((float)L_2+(float)L_4)), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISlider::OnPan(UnityEngine.Vector2)
extern "C"  void UISlider_OnPan_m747130616 (UISlider_t657469589 * __this, Vector2_t3525329788  ___delta0, const MethodInfo* method)
{
	{
		bool L_0 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		bool L_1 = UISlider_get_isColliderEnabled_m1481818026(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Vector2_t3525329788  L_2 = ___delta0;
		UIProgressBar_OnPan_m116988805(__this, L_2, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void UISliderColors::.ctor()
extern Il2CppClass* ColorU5BU5D_t3477081137_il2cpp_TypeInfo_var;
extern const uint32_t UISliderColors__ctor_m270522326_MetadataUsageId;
extern "C"  void UISliderColors__ctor_m270522326 (UISliderColors_t1894798949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISliderColors__ctor_m270522326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ColorU5BU5D_t3477081137* L_0 = ((ColorU5BU5D_t3477081137*)SZArrayNew(ColorU5BU5D_t3477081137_il2cpp_TypeInfo_var, (uint32_t)3));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Color_t1588175760  L_1 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t1588175760 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		ColorU5BU5D_t3477081137* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Color_t1588175760  L_3 = Color_get_yellow_m599454500(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t1588175760 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		ColorU5BU5D_t3477081137* L_4 = L_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		Color_t1588175760  L_5 = Color_get_green_m2005284533(NULL /*static, unused*/, /*hidden argument*/NULL);
		(*(Color_t1588175760 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_5;
		__this->set_colors_3(L_4);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISliderColors::Start()
extern const MethodInfo* Component_GetComponent_TisUIProgressBar_t168062834_m3103886343_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIBasicSprite_t2501337439_m3936925434_MethodInfo_var;
extern const uint32_t UISliderColors_Start_m3512627414_MetadataUsageId;
extern "C"  void UISliderColors_Start_m3512627414 (UISliderColors_t1894798949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISliderColors_Start_m3512627414_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIProgressBar_t168062834 * L_0 = Component_GetComponent_TisUIProgressBar_t168062834_m3103886343(__this, /*hidden argument*/Component_GetComponent_TisUIProgressBar_t168062834_m3103886343_MethodInfo_var);
		__this->set_mBar_4(L_0);
		UIBasicSprite_t2501337439 * L_1 = Component_GetComponent_TisUIBasicSprite_t2501337439_m3936925434(__this, /*hidden argument*/Component_GetComponent_TisUIBasicSprite_t2501337439_m3936925434_MethodInfo_var);
		__this->set_mSprite_5(L_1);
		UISliderColors_Update_m1523119607(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISliderColors::Update()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UISliderColors_Update_m1523119607_MetadataUsageId;
extern "C"  void UISliderColors_Update_m1523119607 (UISliderColors_t1894798949 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISliderColors_Update_m1523119607_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	Color_t1588175760  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Color_t1588175760  V_4;
	memset(&V_4, 0, sizeof(V_4));
	float G_B6_0 = 0.0f;
	{
		UISprite_t661437049 * L_0 = __this->get_sprite_2();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001e;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_2 = __this->get_colors_3();
		NullCheck(L_2);
		if ((((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))
		{
			goto IL_001f;
		}
	}

IL_001e:
	{
		return;
	}

IL_001f:
	{
		UIProgressBar_t168062834 * L_3 = __this->get_mBar_4();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		UIProgressBar_t168062834 * L_5 = __this->get_mBar_4();
		NullCheck(L_5);
		float L_6 = UIProgressBar_get_value_m1994834827(L_5, /*hidden argument*/NULL);
		G_B6_0 = L_6;
		goto IL_004b;
	}

IL_0040:
	{
		UIBasicSprite_t2501337439 * L_7 = __this->get_mSprite_5();
		NullCheck(L_7);
		float L_8 = UIBasicSprite_get_fillAmount_m679518448(L_7, /*hidden argument*/NULL);
		G_B6_0 = L_8;
	}

IL_004b:
	{
		V_0 = G_B6_0;
		float L_9 = V_0;
		ColorU5BU5D_t3477081137* L_10 = __this->get_colors_3();
		NullCheck(L_10);
		V_0 = ((float)((float)L_9*(float)(((float)((float)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length))))-(int32_t)1)))))));
		float L_11 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_12 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		ColorU5BU5D_t3477081137* L_13 = __this->get_colors_3();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		V_2 = (*(Color_t1588175760 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		int32_t L_14 = V_1;
		if ((((int32_t)L_14) < ((int32_t)0)))
		{
			goto IL_00ff;
		}
	}
	{
		int32_t L_15 = V_1;
		ColorU5BU5D_t3477081137* L_16 = __this->get_colors_3();
		NullCheck(L_16);
		if ((((int32_t)((int32_t)((int32_t)L_15+(int32_t)1))) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_16)->max_length)))))))
		{
			goto IL_00bf;
		}
	}
	{
		float L_17 = V_0;
		int32_t L_18 = V_1;
		V_3 = ((float)((float)L_17-(float)(((float)((float)L_18)))));
		ColorU5BU5D_t3477081137* L_19 = __this->get_colors_3();
		int32_t L_20 = V_1;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		ColorU5BU5D_t3477081137* L_21 = __this->get_colors_3();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)L_22+(int32_t)1)));
		float L_23 = V_3;
		Color_t1588175760  L_24 = Color_Lerp_m3494628845(NULL /*static, unused*/, (*(Color_t1588175760 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))), (*(Color_t1588175760 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_22+(int32_t)1)))))), L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		goto IL_00ff;
	}

IL_00bf:
	{
		int32_t L_25 = V_1;
		ColorU5BU5D_t3477081137* L_26 = __this->get_colors_3();
		NullCheck(L_26);
		if ((((int32_t)L_25) >= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_26)->max_length)))))))
		{
			goto IL_00e4;
		}
	}
	{
		ColorU5BU5D_t3477081137* L_27 = __this->get_colors_3();
		int32_t L_28 = V_1;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		V_2 = (*(Color_t1588175760 *)((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28))));
		goto IL_00ff;
	}

IL_00e4:
	{
		ColorU5BU5D_t3477081137* L_29 = __this->get_colors_3();
		ColorU5BU5D_t3477081137* L_30 = __this->get_colors_3();
		NullCheck(L_30);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)1)));
		V_2 = (*(Color_t1588175760 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length))))-(int32_t)1))))));
	}

IL_00ff:
	{
		UISprite_t661437049 * L_31 = __this->get_sprite_2();
		NullCheck(L_31);
		Color_t1588175760  L_32 = UIWidget_get_color_m2224265652(L_31, /*hidden argument*/NULL);
		V_4 = L_32;
		float L_33 = (&V_4)->get_a_3();
		(&V_2)->set_a_3(L_33);
		UISprite_t661437049 * L_34 = __this->get_sprite_2();
		Color_t1588175760  L_35 = V_2;
		NullCheck(L_34);
		UIWidget_set_color_m1905035359(L_34, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISnapshotPoint::.ctor()
extern "C"  void UISnapshotPoint__ctor_m3632316019 (UISnapshotPoint_t3024846840 * __this, const MethodInfo* method)
{
	{
		__this->set_isOrthographic_2((bool)1);
		__this->set_nearClip_3((-100.0f));
		__this->set_farClip_4((100.0f));
		__this->set_fieldOfView_5(((int32_t)35));
		__this->set_orthoSize_6((30.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISnapshotPoint::Start()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral436933337;
extern const uint32_t UISnapshotPoint_Start_m2579453811_MetadataUsageId;
extern "C"  void UISnapshotPoint_Start_m2579453811 (UISnapshotPoint_t3024846840 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISnapshotPoint_Start_m2579453811_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = Component_get_tag_m217485006(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_0, _stringLiteral436933337, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		Component_set_tag_m3240989163(__this, _stringLiteral436933337, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void UISoundVolume::.ctor()
extern "C"  void UISoundVolume__ctor_m342741814 (UISoundVolume_t1043221973 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISoundVolume::Awake()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* Callback_t4187391077_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUISlider_t657469589_m1788897744_MethodInfo_var;
extern const MethodInfo* UISoundVolume_OnChange_m3345921917_MethodInfo_var;
extern const uint32_t UISoundVolume_Awake_m580347033_MetadataUsageId;
extern "C"  void UISoundVolume_Awake_m580347033 (UISoundVolume_t1043221973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISoundVolume_Awake_m580347033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UISlider_t657469589 * V_0 = NULL;
	{
		UISlider_t657469589 * L_0 = Component_GetComponent_TisUISlider_t657469589_m1788897744(__this, /*hidden argument*/Component_GetComponent_TisUISlider_t657469589_m1788897744_MethodInfo_var);
		V_0 = L_0;
		UISlider_t657469589 * L_1 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		float L_2 = NGUITools_get_soundVolume_m1030274695(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		UIProgressBar_set_value_m1993536704(L_1, L_2, /*hidden argument*/NULL);
		UISlider_t657469589 * L_3 = V_0;
		NullCheck(L_3);
		List_1_t506415896 * L_4 = ((UIProgressBar_t168062834 *)L_3)->get_onChange_15();
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)UISoundVolume_OnChange_m3345921917_MethodInfo_var);
		Callback_t4187391077 * L_6 = (Callback_t4187391077 *)il2cpp_codegen_object_new(Callback_t4187391077_il2cpp_TypeInfo_var);
		Callback__ctor_m3018475132(L_6, __this, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1811262575(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISoundVolume::OnChange()
extern Il2CppClass* UIProgressBar_t168062834_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UISoundVolume_OnChange_m3345921917_MetadataUsageId;
extern "C"  void UISoundVolume_OnChange_m3345921917 (UISoundVolume_t1043221973 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISoundVolume_OnChange_m3345921917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIProgressBar_t168062834 * L_0 = ((UIProgressBar_t168062834_StaticFields*)UIProgressBar_t168062834_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_0);
		float L_1 = UIProgressBar_get_value_m1994834827(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_set_soundVolume_m422422852(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISprite::.ctor()
extern Il2CppClass* UIBasicSprite_t2501337439_il2cpp_TypeInfo_var;
extern const uint32_t UISprite__ctor_m1299954754_MetadataUsageId;
extern "C"  void UISprite__ctor_m1299954754 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISprite__ctor_m1299954754_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_mFillCenter_71((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(UIBasicSprite_t2501337439_il2cpp_TypeInfo_var);
		UIBasicSprite__ctor_m2527140588(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Material UISprite::get_material()
extern "C"  Material_t1886596500 * UISprite_get_material_m2331571249 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	Material_t1886596500 * G_B3_0 = NULL;
	{
		UIAtlas_t281921111 * L_0 = __this->get_mAtlas_69();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		UIAtlas_t281921111 * L_2 = __this->get_mAtlas_69();
		NullCheck(L_2);
		Material_t1886596500 * L_3 = UIAtlas_get_spriteMaterial_m1576300038(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Material_t1886596500 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UIAtlas UISprite::get_atlas()
extern "C"  UIAtlas_t281921111 * UISprite_get_atlas_m2395763654 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	{
		UIAtlas_t281921111 * L_0 = __this->get_mAtlas_69();
		return L_0;
	}
}
// System.Void UISprite::set_atlas(UIAtlas)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UISprite_set_atlas_m2780703821_MetadataUsageId;
extern "C"  void UISprite_set_atlas_m2780703821 (UISprite_t661437049 * __this, UIAtlas_t281921111 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISprite_set_atlas_m2780703821_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		UIAtlas_t281921111 * L_0 = __this->get_mAtlas_69();
		UIAtlas_t281921111 * L_1 = ___value0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00ba;
		}
	}
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		UIAtlas_t281921111 * L_3 = ___value0;
		__this->set_mAtlas_69(L_3);
		__this->set_mSpriteSet_73((bool)0);
		__this->set_mSprite_72((UISpriteData_t3578345923 *)NULL);
		String_t* L_4 = __this->get_mSpriteName_70();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_008b;
		}
	}
	{
		UIAtlas_t281921111 * L_6 = __this->get_mAtlas_69();
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_008b;
		}
	}
	{
		UIAtlas_t281921111 * L_8 = __this->get_mAtlas_69();
		NullCheck(L_8);
		List_1_t80337596 * L_9 = UIAtlas_get_spriteList_m1840269286(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UISpriteData>::get_Count() */, L_9);
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_008b;
		}
	}
	{
		UIAtlas_t281921111 * L_11 = __this->get_mAtlas_69();
		NullCheck(L_11);
		List_1_t80337596 * L_12 = UIAtlas_get_spriteList_m1840269286(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		UISpriteData_t3578345923 * L_13 = VirtFuncInvoker1< UISpriteData_t3578345923 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UISpriteData>::get_Item(System.Int32) */, L_12, 0);
		UISprite_SetAtlasSprite_m521282957(__this, L_13, /*hidden argument*/NULL);
		UISpriteData_t3578345923 * L_14 = __this->get_mSprite_72();
		NullCheck(L_14);
		String_t* L_15 = L_14->get_name_0();
		__this->set_mSpriteName_70(L_15);
	}

IL_008b:
	{
		String_t* L_16 = __this->get_mSpriteName_70();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_17 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_00ba;
		}
	}
	{
		String_t* L_18 = __this->get_mSpriteName_70();
		V_0 = L_18;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mSpriteName_70(L_19);
		String_t* L_20 = V_0;
		UISprite_set_spriteName_m4264380499(__this, L_20, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_00ba:
	{
		return;
	}
}
// System.String UISprite::get_spriteName()
extern "C"  String_t* UISprite_get_spriteName_m373728510 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mSpriteName_70();
		return L_0;
	}
}
// System.Void UISprite::set_spriteName(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UISprite_set_spriteName_m4264380499_MetadataUsageId;
extern "C"  void UISprite_set_spriteName_m4264380499 (UISprite_t661437049 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISprite_set_spriteName_m4264380499_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0041;
		}
	}
	{
		String_t* L_2 = __this->get_mSpriteName_70();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		return;
	}

IL_001c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mSpriteName_70(L_4);
		__this->set_mSprite_72((UISpriteData_t3578345923 *)NULL);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		__this->set_mSpriteSet_73((bool)0);
		goto IL_006e;
	}

IL_0041:
	{
		String_t* L_5 = __this->get_mSpriteName_70();
		String_t* L_6 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006e;
		}
	}
	{
		String_t* L_8 = ___value0;
		__this->set_mSpriteName_70(L_8);
		__this->set_mSprite_72((UISpriteData_t3578345923 *)NULL);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		__this->set_mSpriteSet_73((bool)0);
	}

IL_006e:
	{
		return;
	}
}
// System.Boolean UISprite::get_isValid()
extern "C"  bool UISprite_get_isValid_m1407144917 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	{
		UISpriteData_t3578345923 * L_0 = UISprite_GetAtlasSprite_m158078806(__this, /*hidden argument*/NULL);
		return (bool)((((int32_t)((((Il2CppObject*)(UISpriteData_t3578345923 *)L_0) == ((Il2CppObject*)(Il2CppObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UISprite::get_fillCenter()
extern "C"  bool UISprite_get_fillCenter_m1707900183 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = ((UIBasicSprite_t2501337439 *)__this)->get_centerType_62();
		return (bool)((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UISprite::set_fillCenter(System.Boolean)
extern "C"  void UISprite_set_fillCenter_m2174279438 (UISprite_t661437049 * __this, bool ___value0, const MethodInfo* method)
{
	UISprite_t661437049 * G_B3_0 = NULL;
	UISprite_t661437049 * G_B2_0 = NULL;
	int32_t G_B4_0 = 0;
	UISprite_t661437049 * G_B4_1 = NULL;
	{
		bool L_0 = ___value0;
		int32_t L_1 = ((UIBasicSprite_t2501337439 *)__this)->get_centerType_62();
		if ((((int32_t)L_0) == ((int32_t)((((int32_t)((((int32_t)L_1) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0))))
		{
			goto IL_002b;
		}
	}
	{
		bool L_2 = ___value0;
		G_B2_0 = __this;
		if (!L_2)
		{
			G_B3_0 = __this;
			goto IL_001f;
		}
	}
	{
		G_B4_0 = 1;
		G_B4_1 = G_B2_0;
		goto IL_0020;
	}

IL_001f:
	{
		G_B4_0 = 0;
		G_B4_1 = G_B3_0;
	}

IL_0020:
	{
		NullCheck(G_B4_1);
		((UIBasicSprite_t2501337439 *)G_B4_1)->set_centerType_62(G_B4_0);
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_002b:
	{
		return;
	}
}
// System.Boolean UISprite::get_applyGradient()
extern "C"  bool UISprite_get_applyGradient_m3080524577 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	{
		bool L_0 = ((UIBasicSprite_t2501337439 *)__this)->get_mApplyGradient_57();
		return L_0;
	}
}
// System.Void UISprite::set_applyGradient(System.Boolean)
extern "C"  void UISprite_set_applyGradient_m531407384 (UISprite_t661437049 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ((UIBasicSprite_t2501337439 *)__this)->get_mApplyGradient_57();
		bool L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0019;
		}
	}
	{
		bool L_2 = ___value0;
		((UIBasicSprite_t2501337439 *)__this)->set_mApplyGradient_57(L_2);
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_0019:
	{
		return;
	}
}
// UnityEngine.Color UISprite::get_gradientTop()
extern "C"  Color_t1588175760  UISprite_get_gradientTop_m4066254773 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ((UIBasicSprite_t2501337439 *)__this)->get_mGradientTop_58();
		return L_0;
	}
}
// System.Void UISprite::set_gradientTop(UnityEngine.Color)
extern "C"  void UISprite_set_gradientTop_m54075070 (UISprite_t661437049 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ((UIBasicSprite_t2501337439 *)__this)->get_mGradientTop_58();
		Color_t1588175760  L_1 = ___value0;
		bool L_2 = Color_op_Inequality_m1917261071(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Color_t1588175760  L_3 = ___value0;
		((UIBasicSprite_t2501337439 *)__this)->set_mGradientTop_58(L_3);
		bool L_4 = ((UIBasicSprite_t2501337439 *)__this)->get_mApplyGradient_57();
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_0029:
	{
		return;
	}
}
// UnityEngine.Color UISprite::get_gradientBottom()
extern "C"  Color_t1588175760  UISprite_get_gradientBottom_m1419406701 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ((UIBasicSprite_t2501337439 *)__this)->get_mGradientBottom_59();
		return L_0;
	}
}
// System.Void UISprite::set_gradientBottom(UnityEngine.Color)
extern "C"  void UISprite_set_gradientBottom_m591189388 (UISprite_t661437049 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = ((UIBasicSprite_t2501337439 *)__this)->get_mGradientBottom_59();
		Color_t1588175760  L_1 = ___value0;
		bool L_2 = Color_op_Inequality_m1917261071(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		Color_t1588175760  L_3 = ___value0;
		((UIBasicSprite_t2501337439 *)__this)->set_mGradientBottom_59(L_3);
		bool L_4 = ((UIBasicSprite_t2501337439 *)__this)->get_mApplyGradient_57();
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_0029:
	{
		return;
	}
}
// UnityEngine.Vector4 UISprite::get_border()
extern "C"  Vector4_t3525329790  UISprite_get_border_m1305010352 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	UISpriteData_t3578345923 * V_0 = NULL;
	{
		UISpriteData_t3578345923 * L_0 = UISprite_GetAtlasSprite_m158078806(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		UISpriteData_t3578345923 * L_1 = V_0;
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		Vector4_t3525329790  L_2 = UIWidget_get_border_m4155026927(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_0014:
	{
		UISpriteData_t3578345923 * L_3 = V_0;
		NullCheck(L_3);
		int32_t L_4 = L_3->get_borderLeft_5();
		UISpriteData_t3578345923 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_borderBottom_8();
		UISpriteData_t3578345923 * L_7 = V_0;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_borderRight_6();
		UISpriteData_t3578345923 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = L_9->get_borderTop_7();
		Vector4_t3525329790  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Vector4__ctor_m2441427762(&L_11, (((float)((float)L_4))), (((float)((float)L_6))), (((float)((float)L_8))), (((float)((float)L_10))), /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Single UISprite::get_pixelSize()
extern "C"  float UISprite_get_pixelSize_m2516843954 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		UIAtlas_t281921111 * L_0 = __this->get_mAtlas_69();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		UIAtlas_t281921111 * L_2 = __this->get_mAtlas_69();
		NullCheck(L_2);
		float L_3 = UIAtlas_get_pixelSize_m3644908156(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0026;
	}

IL_0021:
	{
		G_B3_0 = (1.0f);
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.Int32 UISprite::get_minWidth()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UISprite_get_minWidth_m518583757_MetadataUsageId;
extern "C"  int32_t UISprite_get_minWidth_m518583757 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISprite_get_minWidth_m518583757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector4_t3525329790  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	UISpriteData_t3578345923 * V_3 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B7_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(39 /* UIBasicSprite/Type UIBasicSprite::get_type() */, __this);
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(39 /* UIBasicSprite/Type UIBasicSprite::get_type() */, __this);
		if ((!(((uint32_t)L_1) == ((uint32_t)4))))
		{
			goto IL_0089;
		}
	}

IL_0018:
	{
		float L_2 = VirtFuncInvoker0< float >::Invoke(42 /* System.Single UISprite::get_pixelSize() */, __this);
		V_0 = L_2;
		Vector4_t3525329790  L_3 = VirtFuncInvoker0< Vector4_t3525329790  >::Invoke(36 /* UnityEngine.Vector4 UISprite::get_border() */, __this);
		float L_4 = VirtFuncInvoker0< float >::Invoke(42 /* System.Single UISprite::get_pixelSize() */, __this);
		Vector4_t3525329790  L_5 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_x_1();
		float L_7 = (&V_1)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_8 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_6+(float)L_7)), /*hidden argument*/NULL);
		V_2 = L_8;
		UISpriteData_t3578345923 * L_9 = UISprite_GetAtlasSprite_m158078806(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		UISpriteData_t3578345923 * L_10 = V_3;
		if (!L_10)
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_11 = V_2;
		float L_12 = V_0;
		UISpriteData_t3578345923 * L_13 = V_3;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_paddingLeft_9();
		UISpriteData_t3578345923 * L_15 = V_3;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_paddingRight_10();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_17 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_12*(float)(((float)((float)((int32_t)((int32_t)L_14+(int32_t)L_16))))))), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)L_17));
	}

IL_006b:
	{
		int32_t L_18 = UIBasicSprite_get_minWidth_m2338180231(__this, /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		G_B5_0 = L_18;
		if ((!(((uint32_t)((int32_t)((int32_t)L_19&(int32_t)1))) == ((uint32_t)1))))
		{
			G_B6_0 = L_18;
			goto IL_0082;
		}
	}
	{
		int32_t L_20 = V_2;
		G_B7_0 = ((int32_t)((int32_t)L_20+(int32_t)1));
		G_B7_1 = G_B5_0;
		goto IL_0083;
	}

IL_0082:
	{
		int32_t L_21 = V_2;
		G_B7_0 = L_21;
		G_B7_1 = G_B6_0;
	}

IL_0083:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_22 = Mathf_Max_m2911193737(NULL /*static, unused*/, G_B7_1, G_B7_0, /*hidden argument*/NULL);
		return L_22;
	}

IL_0089:
	{
		int32_t L_23 = UIBasicSprite_get_minWidth_m2338180231(__this, /*hidden argument*/NULL);
		return L_23;
	}
}
// System.Int32 UISprite::get_minHeight()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UISprite_get_minHeight_m3695008930_MetadataUsageId;
extern "C"  int32_t UISprite_get_minHeight_m3695008930 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISprite_get_minHeight_m3695008930_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector4_t3525329790  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	UISpriteData_t3578345923 * V_3 = NULL;
	int32_t G_B6_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B7_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(39 /* UIBasicSprite/Type UIBasicSprite::get_type() */, __this);
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(39 /* UIBasicSprite/Type UIBasicSprite::get_type() */, __this);
		if ((!(((uint32_t)L_1) == ((uint32_t)4))))
		{
			goto IL_0089;
		}
	}

IL_0018:
	{
		float L_2 = VirtFuncInvoker0< float >::Invoke(42 /* System.Single UISprite::get_pixelSize() */, __this);
		V_0 = L_2;
		Vector4_t3525329790  L_3 = VirtFuncInvoker0< Vector4_t3525329790  >::Invoke(36 /* UnityEngine.Vector4 UISprite::get_border() */, __this);
		float L_4 = VirtFuncInvoker0< float >::Invoke(42 /* System.Single UISprite::get_pixelSize() */, __this);
		Vector4_t3525329790  L_5 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (&V_1)->get_y_2();
		float L_7 = (&V_1)->get_w_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_8 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_6+(float)L_7)), /*hidden argument*/NULL);
		V_2 = L_8;
		UISpriteData_t3578345923 * L_9 = UISprite_GetAtlasSprite_m158078806(__this, /*hidden argument*/NULL);
		V_3 = L_9;
		UISpriteData_t3578345923 * L_10 = V_3;
		if (!L_10)
		{
			goto IL_006b;
		}
	}
	{
		int32_t L_11 = V_2;
		float L_12 = V_0;
		UISpriteData_t3578345923 * L_13 = V_3;
		NullCheck(L_13);
		int32_t L_14 = L_13->get_paddingTop_11();
		UISpriteData_t3578345923 * L_15 = V_3;
		NullCheck(L_15);
		int32_t L_16 = L_15->get_paddingBottom_12();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_17 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_12*(float)(((float)((float)((int32_t)((int32_t)L_14+(int32_t)L_16))))))), /*hidden argument*/NULL);
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)L_17));
	}

IL_006b:
	{
		int32_t L_18 = UIBasicSprite_get_minHeight_m4267924776(__this, /*hidden argument*/NULL);
		int32_t L_19 = V_2;
		G_B5_0 = L_18;
		if ((!(((uint32_t)((int32_t)((int32_t)L_19&(int32_t)1))) == ((uint32_t)1))))
		{
			G_B6_0 = L_18;
			goto IL_0082;
		}
	}
	{
		int32_t L_20 = V_2;
		G_B7_0 = ((int32_t)((int32_t)L_20+(int32_t)1));
		G_B7_1 = G_B5_0;
		goto IL_0083;
	}

IL_0082:
	{
		int32_t L_21 = V_2;
		G_B7_0 = L_21;
		G_B7_1 = G_B6_0;
	}

IL_0083:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_22 = Mathf_Max_m2911193737(NULL /*static, unused*/, G_B7_1, G_B7_0, /*hidden argument*/NULL);
		return L_22;
	}

IL_0089:
	{
		int32_t L_23 = UIBasicSprite_get_minHeight_m4267924776(__this, /*hidden argument*/NULL);
		return L_23;
	}
}
// UnityEngine.Vector4 UISprite::get_drawingDimensions()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UISprite_get_drawingDimensions_m3780210281_MetadataUsageId;
extern "C"  Vector4_t3525329790  UISprite_get_drawingDimensions_m3780210281 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISprite_get_drawingDimensions_m3780210281_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	Vector4_t3525329790  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	float V_20 = 0.0f;
	Vector4_t3525329790  G_B25_0;
	memset(&G_B25_0, 0, sizeof(G_B25_0));
	{
		Vector2_t3525329788  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = ((UIWidget_t769069560 *)__this)->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = ((UIWidget_t769069560 *)__this)->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = ((UIWidget_t769069560 *)__this)->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = ((UIWidget_t769069560 *)__this)->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		UISpriteData_t3578345923 * L_9 = UISprite_GetAtlasSprite_m158078806(__this, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0201;
		}
	}
	{
		int32_t L_10 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if ((((int32_t)L_10) == ((int32_t)2)))
		{
			goto IL_0201;
		}
	}
	{
		UISpriteData_t3578345923 * L_11 = __this->get_mSprite_72();
		NullCheck(L_11);
		int32_t L_12 = L_11->get_paddingLeft_9();
		V_5 = L_12;
		UISpriteData_t3578345923 * L_13 = __this->get_mSprite_72();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_paddingBottom_12();
		V_6 = L_14;
		UISpriteData_t3578345923 * L_15 = __this->get_mSprite_72();
		NullCheck(L_15);
		int32_t L_16 = L_15->get_paddingRight_10();
		V_7 = L_16;
		UISpriteData_t3578345923 * L_17 = __this->get_mSprite_72();
		NullCheck(L_17);
		int32_t L_18 = L_17->get_paddingTop_11();
		V_8 = L_18;
		int32_t L_19 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if (!L_19)
		{
			goto IL_00dc;
		}
	}
	{
		float L_20 = VirtFuncInvoker0< float >::Invoke(42 /* System.Single UISprite::get_pixelSize() */, __this);
		V_9 = L_20;
		float L_21 = V_9;
		if ((((float)L_21) == ((float)(1.0f))))
		{
			goto IL_00dc;
		}
	}
	{
		float L_22 = V_9;
		int32_t L_23 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_24 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_22*(float)(((float)((float)L_23))))), /*hidden argument*/NULL);
		V_5 = L_24;
		float L_25 = V_9;
		int32_t L_26 = V_6;
		int32_t L_27 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_25*(float)(((float)((float)L_26))))), /*hidden argument*/NULL);
		V_6 = L_27;
		float L_28 = V_9;
		int32_t L_29 = V_7;
		int32_t L_30 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_28*(float)(((float)((float)L_29))))), /*hidden argument*/NULL);
		V_7 = L_30;
		float L_31 = V_9;
		int32_t L_32 = V_8;
		int32_t L_33 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_31*(float)(((float)((float)L_32))))), /*hidden argument*/NULL);
		V_8 = L_33;
	}

IL_00dc:
	{
		UISpriteData_t3578345923 * L_34 = __this->get_mSprite_72();
		NullCheck(L_34);
		int32_t L_35 = L_34->get_width_3();
		int32_t L_36 = V_5;
		int32_t L_37 = V_7;
		V_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_35+(int32_t)L_36))+(int32_t)L_37));
		UISpriteData_t3578345923 * L_38 = __this->get_mSprite_72();
		NullCheck(L_38);
		int32_t L_39 = L_38->get_height_4();
		int32_t L_40 = V_6;
		int32_t L_41 = V_8;
		V_11 = ((int32_t)((int32_t)((int32_t)((int32_t)L_39+(int32_t)L_40))+(int32_t)L_41));
		V_12 = (1.0f);
		V_13 = (1.0f);
		int32_t L_42 = V_10;
		if ((((int32_t)L_42) <= ((int32_t)0)))
		{
			goto IL_017b;
		}
	}
	{
		int32_t L_43 = V_11;
		if ((((int32_t)L_43) <= ((int32_t)0)))
		{
			goto IL_017b;
		}
	}
	{
		int32_t L_44 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if (!L_44)
		{
			goto IL_0137;
		}
	}
	{
		int32_t L_45 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if ((!(((uint32_t)L_45) == ((uint32_t)3))))
		{
			goto IL_017b;
		}
	}

IL_0137:
	{
		int32_t L_46 = V_10;
		if (!((int32_t)((int32_t)L_46&(int32_t)1)))
		{
			goto IL_0146;
		}
	}
	{
		int32_t L_47 = V_7;
		V_7 = ((int32_t)((int32_t)L_47+(int32_t)1));
	}

IL_0146:
	{
		int32_t L_48 = V_11;
		if (!((int32_t)((int32_t)L_48&(int32_t)1)))
		{
			goto IL_0155;
		}
	}
	{
		int32_t L_49 = V_8;
		V_8 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_0155:
	{
		int32_t L_50 = V_10;
		int32_t L_51 = ((UIWidget_t769069560 *)__this)->get_mWidth_24();
		V_12 = ((float)((float)((float)((float)(1.0f)/(float)(((float)((float)L_50)))))*(float)(((float)((float)L_51)))));
		int32_t L_52 = V_11;
		int32_t L_53 = ((UIWidget_t769069560 *)__this)->get_mHeight_25();
		V_13 = ((float)((float)((float)((float)(1.0f)/(float)(((float)((float)L_52)))))*(float)(((float)((float)L_53)))));
	}

IL_017b:
	{
		int32_t L_54 = ((UIBasicSprite_t2501337439 *)__this)->get_mFlip_56();
		if ((((int32_t)L_54) == ((int32_t)1)))
		{
			goto IL_0193;
		}
	}
	{
		int32_t L_55 = ((UIBasicSprite_t2501337439 *)__this)->get_mFlip_56();
		if ((!(((uint32_t)L_55) == ((uint32_t)3))))
		{
			goto IL_01aa;
		}
	}

IL_0193:
	{
		float L_56 = V_1;
		int32_t L_57 = V_7;
		float L_58 = V_12;
		V_1 = ((float)((float)L_56+(float)((float)((float)(((float)((float)L_57)))*(float)L_58))));
		float L_59 = V_3;
		int32_t L_60 = V_5;
		float L_61 = V_12;
		V_3 = ((float)((float)L_59-(float)((float)((float)(((float)((float)L_60)))*(float)L_61))));
		goto IL_01bc;
	}

IL_01aa:
	{
		float L_62 = V_1;
		int32_t L_63 = V_5;
		float L_64 = V_12;
		V_1 = ((float)((float)L_62+(float)((float)((float)(((float)((float)L_63)))*(float)L_64))));
		float L_65 = V_3;
		int32_t L_66 = V_7;
		float L_67 = V_12;
		V_3 = ((float)((float)L_65-(float)((float)((float)(((float)((float)L_66)))*(float)L_67))));
	}

IL_01bc:
	{
		int32_t L_68 = ((UIBasicSprite_t2501337439 *)__this)->get_mFlip_56();
		if ((((int32_t)L_68) == ((int32_t)2)))
		{
			goto IL_01d4;
		}
	}
	{
		int32_t L_69 = ((UIBasicSprite_t2501337439 *)__this)->get_mFlip_56();
		if ((!(((uint32_t)L_69) == ((uint32_t)3))))
		{
			goto IL_01ed;
		}
	}

IL_01d4:
	{
		float L_70 = V_2;
		int32_t L_71 = V_8;
		float L_72 = V_13;
		V_2 = ((float)((float)L_70+(float)((float)((float)(((float)((float)L_71)))*(float)L_72))));
		float L_73 = V_4;
		int32_t L_74 = V_6;
		float L_75 = V_13;
		V_4 = ((float)((float)L_73-(float)((float)((float)(((float)((float)L_74)))*(float)L_75))));
		goto IL_0201;
	}

IL_01ed:
	{
		float L_76 = V_2;
		int32_t L_77 = V_6;
		float L_78 = V_13;
		V_2 = ((float)((float)L_76+(float)((float)((float)(((float)((float)L_77)))*(float)L_78))));
		float L_79 = V_4;
		int32_t L_80 = V_8;
		float L_81 = V_13;
		V_4 = ((float)((float)L_79-(float)((float)((float)(((float)((float)L_80)))*(float)L_81))));
	}

IL_0201:
	{
		UIAtlas_t281921111 * L_82 = __this->get_mAtlas_69();
		bool L_83 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_82, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_83)
		{
			goto IL_0228;
		}
	}
	{
		Vector4_t3525329790  L_84 = VirtFuncInvoker0< Vector4_t3525329790  >::Invoke(36 /* UnityEngine.Vector4 UISprite::get_border() */, __this);
		float L_85 = VirtFuncInvoker0< float >::Invoke(42 /* System.Single UISprite::get_pixelSize() */, __this);
		Vector4_t3525329790  L_86 = Vector4_op_Multiply_m209031836(NULL /*static, unused*/, L_84, L_85, /*hidden argument*/NULL);
		G_B25_0 = L_86;
		goto IL_022d;
	}

IL_0228:
	{
		Vector4_t3525329790  L_87 = Vector4_get_zero_m3835647092(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B25_0 = L_87;
	}

IL_022d:
	{
		V_14 = G_B25_0;
		float L_88 = (&V_14)->get_x_1();
		float L_89 = (&V_14)->get_z_3();
		V_15 = ((float)((float)L_88+(float)L_89));
		float L_90 = (&V_14)->get_y_2();
		float L_91 = (&V_14)->get_w_4();
		V_16 = ((float)((float)L_90+(float)L_91));
		float L_92 = V_1;
		float L_93 = V_3;
		float L_94 = V_15;
		Vector4_t3525329790 * L_95 = ((UIWidget_t769069560 *)__this)->get_address_of_mDrawRegion_39();
		float L_96 = L_95->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_97 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_92, ((float)((float)L_93-(float)L_94)), L_96, /*hidden argument*/NULL);
		V_17 = L_97;
		float L_98 = V_2;
		float L_99 = V_4;
		float L_100 = V_16;
		Vector4_t3525329790 * L_101 = ((UIWidget_t769069560 *)__this)->get_address_of_mDrawRegion_39();
		float L_102 = L_101->get_y_2();
		float L_103 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_98, ((float)((float)L_99-(float)L_100)), L_102, /*hidden argument*/NULL);
		V_18 = L_103;
		float L_104 = V_1;
		float L_105 = V_15;
		float L_106 = V_3;
		Vector4_t3525329790 * L_107 = ((UIWidget_t769069560 *)__this)->get_address_of_mDrawRegion_39();
		float L_108 = L_107->get_z_3();
		float L_109 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, ((float)((float)L_104+(float)L_105)), L_106, L_108, /*hidden argument*/NULL);
		V_19 = L_109;
		float L_110 = V_2;
		float L_111 = V_16;
		float L_112 = V_4;
		Vector4_t3525329790 * L_113 = ((UIWidget_t769069560 *)__this)->get_address_of_mDrawRegion_39();
		float L_114 = L_113->get_w_4();
		float L_115 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, ((float)((float)L_110+(float)L_111)), L_112, L_114, /*hidden argument*/NULL);
		V_20 = L_115;
		float L_116 = V_17;
		float L_117 = V_18;
		float L_118 = V_19;
		float L_119 = V_20;
		Vector4_t3525329790  L_120;
		memset(&L_120, 0, sizeof(L_120));
		Vector4__ctor_m2441427762(&L_120, L_116, L_117, L_118, L_119, /*hidden argument*/NULL);
		return L_120;
	}
}
// System.Boolean UISprite::get_premultipliedAlpha()
extern "C"  bool UISprite_get_premultipliedAlpha_m3157737927 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UIAtlas_t281921111 * L_0 = __this->get_mAtlas_69();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		UIAtlas_t281921111 * L_2 = __this->get_mAtlas_69();
		NullCheck(L_2);
		bool L_3 = UIAtlas_get_premultipliedAlpha_m304534381(L_2, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_3));
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = 0;
	}

IL_001f:
	{
		return (bool)G_B3_0;
	}
}
// UISpriteData UISprite::GetAtlasSprite()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral608580924;
extern const uint32_t UISprite_GetAtlasSprite_m158078806_MetadataUsageId;
extern "C"  UISpriteData_t3578345923 * UISprite_GetAtlasSprite_m158078806 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISprite_GetAtlasSprite_m158078806_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UISpriteData_t3578345923 * V_0 = NULL;
	UISpriteData_t3578345923 * V_1 = NULL;
	{
		bool L_0 = __this->get_mSpriteSet_73();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		__this->set_mSprite_72((UISpriteData_t3578345923 *)NULL);
	}

IL_0012:
	{
		UISpriteData_t3578345923 * L_1 = __this->get_mSprite_72();
		if (L_1)
		{
			goto IL_00d9;
		}
	}
	{
		UIAtlas_t281921111 * L_2 = __this->get_mAtlas_69();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00d9;
		}
	}
	{
		String_t* L_4 = __this->get_mSpriteName_70();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_005f;
		}
	}
	{
		UIAtlas_t281921111 * L_6 = __this->get_mAtlas_69();
		String_t* L_7 = __this->get_mSpriteName_70();
		NullCheck(L_6);
		UISpriteData_t3578345923 * L_8 = UIAtlas_GetSprite_m2191383547(L_6, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		UISpriteData_t3578345923 * L_9 = V_0;
		if (L_9)
		{
			goto IL_0058;
		}
	}
	{
		return (UISpriteData_t3578345923 *)NULL;
	}

IL_0058:
	{
		UISpriteData_t3578345923 * L_10 = V_0;
		UISprite_SetAtlasSprite_m521282957(__this, L_10, /*hidden argument*/NULL);
	}

IL_005f:
	{
		UISpriteData_t3578345923 * L_11 = __this->get_mSprite_72();
		if (L_11)
		{
			goto IL_00d9;
		}
	}
	{
		UIAtlas_t281921111 * L_12 = __this->get_mAtlas_69();
		NullCheck(L_12);
		List_1_t80337596 * L_13 = UIAtlas_get_spriteList_m1840269286(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		int32_t L_14 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UISpriteData>::get_Count() */, L_13);
		if ((((int32_t)L_14) <= ((int32_t)0)))
		{
			goto IL_00d9;
		}
	}
	{
		UIAtlas_t281921111 * L_15 = __this->get_mAtlas_69();
		NullCheck(L_15);
		List_1_t80337596 * L_16 = UIAtlas_get_spriteList_m1840269286(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		UISpriteData_t3578345923 * L_17 = VirtFuncInvoker1< UISpriteData_t3578345923 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UISpriteData>::get_Item(System.Int32) */, L_16, 0);
		V_1 = L_17;
		UISpriteData_t3578345923 * L_18 = V_1;
		if (L_18)
		{
			goto IL_009a;
		}
	}
	{
		return (UISpriteData_t3578345923 *)NULL;
	}

IL_009a:
	{
		UISpriteData_t3578345923 * L_19 = V_1;
		UISprite_SetAtlasSprite_m521282957(__this, L_19, /*hidden argument*/NULL);
		UISpriteData_t3578345923 * L_20 = __this->get_mSprite_72();
		if (L_20)
		{
			goto IL_00c8;
		}
	}
	{
		UIAtlas_t281921111 * L_21 = __this->get_mAtlas_69();
		NullCheck(L_21);
		String_t* L_22 = Object_get_name_m3709440845(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m138640077(NULL /*static, unused*/, L_22, _stringLiteral608580924, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogError_m4127342994(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		return (UISpriteData_t3578345923 *)NULL;
	}

IL_00c8:
	{
		UISpriteData_t3578345923 * L_24 = __this->get_mSprite_72();
		NullCheck(L_24);
		String_t* L_25 = L_24->get_name_0();
		__this->set_mSpriteName_70(L_25);
	}

IL_00d9:
	{
		UISpriteData_t3578345923 * L_26 = __this->get_mSprite_72();
		return L_26;
	}
}
// System.Void UISprite::SetAtlasSprite(UISpriteData)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UISprite_SetAtlasSprite_m521282957_MetadataUsageId;
extern "C"  void UISprite_SetAtlasSprite_m521282957 (UISprite_t661437049 * __this, UISpriteData_t3578345923 * ___sp0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISprite_SetAtlasSprite_m521282957_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UISprite_t661437049 * G_B4_0 = NULL;
	UISprite_t661437049 * G_B3_0 = NULL;
	String_t* G_B5_0 = NULL;
	UISprite_t661437049 * G_B5_1 = NULL;
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		__this->set_mSpriteSet_73((bool)1);
		UISpriteData_t3578345923 * L_0 = ___sp0;
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		UISpriteData_t3578345923 * L_1 = ___sp0;
		__this->set_mSprite_72(L_1);
		UISpriteData_t3578345923 * L_2 = __this->get_mSprite_72();
		NullCheck(L_2);
		String_t* L_3 = L_2->get_name_0();
		__this->set_mSpriteName_70(L_3);
		goto IL_005e;
	}

IL_0031:
	{
		UISpriteData_t3578345923 * L_4 = __this->get_mSprite_72();
		G_B3_0 = __this;
		if (!L_4)
		{
			G_B4_0 = __this;
			goto IL_004d;
		}
	}
	{
		UISpriteData_t3578345923 * L_5 = __this->get_mSprite_72();
		NullCheck(L_5);
		String_t* L_6 = L_5->get_name_0();
		G_B5_0 = L_6;
		G_B5_1 = G_B3_0;
		goto IL_0052;
	}

IL_004d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B5_0 = L_7;
		G_B5_1 = G_B4_0;
	}

IL_0052:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_mSpriteName_70(G_B5_0);
		UISpriteData_t3578345923 * L_8 = ___sp0;
		__this->set_mSprite_72(L_8);
	}

IL_005e:
	{
		return;
	}
}
// System.Void UISprite::MakePixelPerfect()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UISprite_MakePixelPerfect_m2230015415_MetadataUsageId;
extern "C"  void UISprite_MakePixelPerfect_m2230015415 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISprite_MakePixelPerfect_m2230015415_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UISpriteData_t3578345923 * V_0 = NULL;
	Texture_t1769722184 * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = UISprite_get_isValid_m1407144917(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		UIWidget_MakePixelPerfect_m1839593398(__this, /*hidden argument*/NULL);
		int32_t L_1 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_001f;
		}
	}
	{
		return;
	}

IL_001f:
	{
		UISpriteData_t3578345923 * L_2 = UISprite_GetAtlasSprite_m158078806(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		UISpriteData_t3578345923 * L_3 = V_0;
		if (L_3)
		{
			goto IL_002d;
		}
	}
	{
		return;
	}

IL_002d:
	{
		Texture_t1769722184 * L_4 = VirtFuncInvoker0< Texture_t1769722184 * >::Invoke(27 /* UnityEngine.Texture UIWidget::get_mainTexture() */, __this);
		V_1 = L_4;
		Texture_t1769722184 * L_5 = V_1;
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		return;
	}

IL_0041:
	{
		int32_t L_7 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if (!L_7)
		{
			goto IL_0063;
		}
	}
	{
		int32_t L_8 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if ((((int32_t)L_8) == ((int32_t)3)))
		{
			goto IL_0063;
		}
	}
	{
		UISpriteData_t3578345923 * L_9 = V_0;
		NullCheck(L_9);
		bool L_10 = UISpriteData_get_hasBorder_m4230184127(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00db;
		}
	}

IL_0063:
	{
		Texture_t1769722184 * L_11 = V_1;
		bool L_12 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_11, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00db;
		}
	}
	{
		float L_13 = VirtFuncInvoker0< float >::Invoke(42 /* System.Single UISprite::get_pixelSize() */, __this);
		UISpriteData_t3578345923 * L_14 = V_0;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_width_3();
		UISpriteData_t3578345923 * L_16 = V_0;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_paddingLeft_9();
		UISpriteData_t3578345923 * L_18 = V_0;
		NullCheck(L_18);
		int32_t L_19 = L_18->get_paddingRight_10();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_20 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_13*(float)(((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_15+(int32_t)L_17))+(int32_t)L_19))))))), /*hidden argument*/NULL);
		V_2 = L_20;
		float L_21 = VirtFuncInvoker0< float >::Invoke(42 /* System.Single UISprite::get_pixelSize() */, __this);
		UISpriteData_t3578345923 * L_22 = V_0;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_height_4();
		UISpriteData_t3578345923 * L_24 = V_0;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_paddingTop_11();
		UISpriteData_t3578345923 * L_26 = V_0;
		NullCheck(L_26);
		int32_t L_27 = L_26->get_paddingBottom_12();
		int32_t L_28 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_21*(float)(((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_23+(int32_t)L_25))+(int32_t)L_27))))))), /*hidden argument*/NULL);
		V_3 = L_28;
		int32_t L_29 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_29&(int32_t)1))) == ((uint32_t)1))))
		{
			goto IL_00c0;
		}
	}
	{
		int32_t L_30 = V_2;
		V_2 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_00c0:
	{
		int32_t L_31 = V_3;
		if ((!(((uint32_t)((int32_t)((int32_t)L_31&(int32_t)1))) == ((uint32_t)1))))
		{
			goto IL_00cd;
		}
	}
	{
		int32_t L_32 = V_3;
		V_3 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00cd:
	{
		int32_t L_33 = V_2;
		UIWidget_set_width_m3480390811(__this, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_3;
		UIWidget_set_height_m1838784918(__this, L_34, /*hidden argument*/NULL);
	}

IL_00db:
	{
		return;
	}
}
// System.Void UISprite::OnInit()
extern "C"  void UISprite_OnInit_m3262491729 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mFillCenter_71();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		__this->set_mFillCenter_71((bool)1);
		((UIBasicSprite_t2501337439 *)__this)->set_centerType_62(0);
	}

IL_0019:
	{
		UIWidget_OnInit_m3801346320(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISprite::OnUpdate()
extern "C"  void UISprite_OnUpdate_m983950794 (UISprite_t661437049 * __this, const MethodInfo* method)
{
	{
		UIWidget_OnUpdate_m3427137225(__this, /*hidden argument*/NULL);
		bool L_0 = ((UIRect_t2503437976 *)__this)->get_mChanged_10();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		bool L_1 = __this->get_mSpriteSet_73();
		if (L_1)
		{
			goto IL_0031;
		}
	}

IL_001c:
	{
		__this->set_mSpriteSet_73((bool)1);
		__this->set_mSprite_72((UISpriteData_t3578345923 *)NULL);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UISprite::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color>)
extern "C"  void UISprite_OnFill_m4239003314 (UISprite_t661437049 * __this, BetterList_1_t727330505 * ___verts0, BetterList_1_t727330504 * ___uvs1, BetterList_1_t3085143772 * ___cols2, const MethodInfo* method)
{
	Texture_t1769722184 * V_0 = NULL;
	Rect_t1525428817  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t1525428817  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t V_3 = 0;
	{
		Texture_t1769722184 * L_0 = VirtFuncInvoker0< Texture_t1769722184 * >::Invoke(27 /* UnityEngine.Texture UIWidget::get_mainTexture() */, __this);
		V_0 = L_0;
		Texture_t1769722184 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		UISpriteData_t3578345923 * L_3 = __this->get_mSprite_72();
		if (L_3)
		{
			goto IL_0036;
		}
	}
	{
		UIAtlas_t281921111 * L_4 = UISprite_get_atlas_m2395763654(__this, /*hidden argument*/NULL);
		String_t* L_5 = UISprite_get_spriteName_m373728510(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		UISpriteData_t3578345923 * L_6 = UIAtlas_GetSprite_m2191383547(L_4, L_5, /*hidden argument*/NULL);
		__this->set_mSprite_72(L_6);
	}

IL_0036:
	{
		UISpriteData_t3578345923 * L_7 = __this->get_mSprite_72();
		if (L_7)
		{
			goto IL_0042;
		}
	}
	{
		return;
	}

IL_0042:
	{
		UISpriteData_t3578345923 * L_8 = __this->get_mSprite_72();
		NullCheck(L_8);
		int32_t L_9 = L_8->get_x_1();
		UISpriteData_t3578345923 * L_10 = __this->get_mSprite_72();
		NullCheck(L_10);
		int32_t L_11 = L_10->get_y_2();
		UISpriteData_t3578345923 * L_12 = __this->get_mSprite_72();
		NullCheck(L_12);
		int32_t L_13 = L_12->get_width_3();
		UISpriteData_t3578345923 * L_14 = __this->get_mSprite_72();
		NullCheck(L_14);
		int32_t L_15 = L_14->get_height_4();
		Rect__ctor_m3291325233((&V_1), (((float)((float)L_9))), (((float)((float)L_11))), (((float)((float)L_13))), (((float)((float)L_15))), /*hidden argument*/NULL);
		UISpriteData_t3578345923 * L_16 = __this->get_mSprite_72();
		NullCheck(L_16);
		int32_t L_17 = L_16->get_x_1();
		UISpriteData_t3578345923 * L_18 = __this->get_mSprite_72();
		NullCheck(L_18);
		int32_t L_19 = L_18->get_borderLeft_5();
		UISpriteData_t3578345923 * L_20 = __this->get_mSprite_72();
		NullCheck(L_20);
		int32_t L_21 = L_20->get_y_2();
		UISpriteData_t3578345923 * L_22 = __this->get_mSprite_72();
		NullCheck(L_22);
		int32_t L_23 = L_22->get_borderTop_7();
		UISpriteData_t3578345923 * L_24 = __this->get_mSprite_72();
		NullCheck(L_24);
		int32_t L_25 = L_24->get_width_3();
		UISpriteData_t3578345923 * L_26 = __this->get_mSprite_72();
		NullCheck(L_26);
		int32_t L_27 = L_26->get_borderLeft_5();
		UISpriteData_t3578345923 * L_28 = __this->get_mSprite_72();
		NullCheck(L_28);
		int32_t L_29 = L_28->get_borderRight_6();
		UISpriteData_t3578345923 * L_30 = __this->get_mSprite_72();
		NullCheck(L_30);
		int32_t L_31 = L_30->get_height_4();
		UISpriteData_t3578345923 * L_32 = __this->get_mSprite_72();
		NullCheck(L_32);
		int32_t L_33 = L_32->get_borderBottom_8();
		UISpriteData_t3578345923 * L_34 = __this->get_mSprite_72();
		NullCheck(L_34);
		int32_t L_35 = L_34->get_borderTop_7();
		Rect__ctor_m3291325233((&V_2), (((float)((float)((int32_t)((int32_t)L_17+(int32_t)L_19))))), (((float)((float)((int32_t)((int32_t)L_21+(int32_t)L_23))))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_25-(int32_t)L_27))-(int32_t)L_29))))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_31-(int32_t)L_33))-(int32_t)L_35))))), /*hidden argument*/NULL);
		Rect_t1525428817  L_36 = V_1;
		Texture_t1769722184 * L_37 = V_0;
		NullCheck(L_37);
		int32_t L_38 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_37);
		Texture_t1769722184 * L_39 = V_0;
		NullCheck(L_39);
		int32_t L_40 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_39);
		Rect_t1525428817  L_41 = NGUIMath_ConvertToTexCoords_m3130058204(NULL /*static, unused*/, L_36, L_38, L_40, /*hidden argument*/NULL);
		V_1 = L_41;
		Rect_t1525428817  L_42 = V_2;
		Texture_t1769722184 * L_43 = V_0;
		NullCheck(L_43);
		int32_t L_44 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_43);
		Texture_t1769722184 * L_45 = V_0;
		NullCheck(L_45);
		int32_t L_46 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_45);
		Rect_t1525428817  L_47 = NGUIMath_ConvertToTexCoords_m3130058204(NULL /*static, unused*/, L_42, L_44, L_46, /*hidden argument*/NULL);
		V_2 = L_47;
		BetterList_1_t727330505 * L_48 = ___verts0;
		NullCheck(L_48);
		int32_t L_49 = L_48->get_size_1();
		V_3 = L_49;
		BetterList_1_t727330505 * L_50 = ___verts0;
		BetterList_1_t727330504 * L_51 = ___uvs1;
		BetterList_1_t3085143772 * L_52 = ___cols2;
		Rect_t1525428817  L_53 = V_1;
		Rect_t1525428817  L_54 = V_2;
		UIBasicSprite_Fill_m1415708251(__this, L_50, L_51, L_52, L_53, L_54, /*hidden argument*/NULL);
		OnPostFillCallback_t294433735 * L_55 = ((UIWidget_t769069560 *)__this)->get_onPostFill_28();
		if (!L_55)
		{
			goto IL_014b;
		}
	}
	{
		OnPostFillCallback_t294433735 * L_56 = ((UIWidget_t769069560 *)__this)->get_onPostFill_28();
		int32_t L_57 = V_3;
		BetterList_1_t727330505 * L_58 = ___verts0;
		BetterList_1_t727330504 * L_59 = ___uvs1;
		BetterList_1_t3085143772 * L_60 = ___cols2;
		NullCheck(L_56);
		OnPostFillCallback_Invoke_m1064019312(L_56, __this, L_57, L_58, L_59, L_60, /*hidden argument*/NULL);
	}

IL_014b:
	{
		return;
	}
}
// System.Void UISpriteAnimation::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1765447871_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m459821414_MethodInfo_var;
extern const uint32_t UISpriteAnimation__ctor_m3977474240_MetadataUsageId;
extern "C"  void UISpriteAnimation__ctor_m3977474240 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISpriteAnimation__ctor_m3977474240_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_mFPS_2(((int32_t)30));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_mPrefix_3(L_0);
		__this->set_mLoop_4((bool)1);
		__this->set_mSnap_5((bool)1);
		__this->set_mActive_9((bool)1);
		List_1_t1765447871 * L_1 = (List_1_t1765447871 *)il2cpp_codegen_object_new(List_1_t1765447871_il2cpp_TypeInfo_var);
		List_1__ctor_m459821414(L_1, /*hidden argument*/List_1__ctor_m459821414_MethodInfo_var);
		__this->set_mSpriteNames_10(L_1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UISpriteAnimation::get_frames()
extern "C"  int32_t UISpriteAnimation_get_frames_m1438610501 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	{
		List_1_t1765447871 * L_0 = __this->get_mSpriteNames_10();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Int32 UISpriteAnimation::get_framesPerSecond()
extern "C"  int32_t UISpriteAnimation_get_framesPerSecond_m2544945614 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mFPS_2();
		return L_0;
	}
}
// System.Void UISpriteAnimation::set_framesPerSecond(System.Int32)
extern "C"  void UISpriteAnimation_set_framesPerSecond_m1620652829 (UISpriteAnimation_t4279777547 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_mFPS_2(L_0);
		return;
	}
}
// System.String UISpriteAnimation::get_namePrefix()
extern "C"  String_t* UISpriteAnimation_get_namePrefix_m2167980967 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = __this->get_mPrefix_3();
		return L_0;
	}
}
// System.Void UISpriteAnimation::set_namePrefix(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t UISpriteAnimation_set_namePrefix_m178335908_MetadataUsageId;
extern "C"  void UISpriteAnimation_set_namePrefix_m178335908 (UISpriteAnimation_t4279777547 * __this, String_t* ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISpriteAnimation_set_namePrefix_m178335908_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = __this->get_mPrefix_3();
		String_t* L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_op_Inequality_m2125462205(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		String_t* L_3 = ___value0;
		__this->set_mPrefix_3(L_3);
		UISpriteAnimation_RebuildSpriteList_m2996903516(__this, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Boolean UISpriteAnimation::get_loop()
extern "C"  bool UISpriteAnimation_get_loop_m8362845 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mLoop_4();
		return L_0;
	}
}
// System.Void UISpriteAnimation::set_loop(System.Boolean)
extern "C"  void UISpriteAnimation_set_loop_m3202610108 (UISpriteAnimation_t4279777547 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_mLoop_4(L_0);
		return;
	}
}
// System.Boolean UISpriteAnimation::get_isPlaying()
extern "C"  bool UISpriteAnimation_get_isPlaying_m1764199917 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mActive_9();
		return L_0;
	}
}
// System.Void UISpriteAnimation::Start()
extern "C"  void UISpriteAnimation_Start_m2924612032 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	{
		UISpriteAnimation_RebuildSpriteList_m2996903516(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UISpriteAnimation::Update()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UISpriteAnimation_Update_m474511949_MetadataUsageId;
extern "C"  void UISpriteAnimation_Update_m474511949 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISpriteAnimation_Update_m474511949_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	UISpriteAnimation_t4279777547 * G_B7_0 = NULL;
	UISpriteAnimation_t4279777547 * G_B6_0 = NULL;
	float G_B8_0 = 0.0f;
	UISpriteAnimation_t4279777547 * G_B8_1 = NULL;
	{
		bool L_0 = __this->get_mActive_9();
		if (!L_0)
		{
			goto IL_0101;
		}
	}
	{
		List_1_t1765447871 * L_1 = __this->get_mSpriteNames_10();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_1);
		if ((((int32_t)L_2) <= ((int32_t)1)))
		{
			goto IL_0101;
		}
	}
	{
		bool L_3 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0101;
		}
	}
	{
		int32_t L_4 = __this->get_mFPS_2();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0101;
		}
	}
	{
		float L_5 = __this->get_mDelta_7();
		float L_6 = RealTime_get_deltaTime_m2274453566(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Min_m2322067385(NULL /*static, unused*/, (1.0f), L_6, /*hidden argument*/NULL);
		__this->set_mDelta_7(((float)((float)L_5+(float)L_7)));
		int32_t L_8 = __this->get_mFPS_2();
		V_0 = ((float)((float)(1.0f)/(float)(((float)((float)L_8)))));
		goto IL_00f5;
	}

IL_0061:
	{
		float L_9 = V_0;
		G_B6_0 = __this;
		if ((!(((float)L_9) > ((float)(0.0f)))))
		{
			G_B7_0 = __this;
			goto IL_007a;
		}
	}
	{
		float L_10 = __this->get_mDelta_7();
		float L_11 = V_0;
		G_B8_0 = ((float)((float)L_10-(float)L_11));
		G_B8_1 = G_B6_0;
		goto IL_007f;
	}

IL_007a:
	{
		G_B8_0 = (0.0f);
		G_B8_1 = G_B7_0;
	}

IL_007f:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_mDelta_7(G_B8_0);
		int32_t L_12 = __this->get_mIndex_8();
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		V_1 = L_13;
		__this->set_mIndex_8(L_13);
		int32_t L_14 = V_1;
		List_1_t1765447871 * L_15 = __this->get_mSpriteNames_10();
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_15);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_00b8;
		}
	}
	{
		__this->set_mIndex_8(0);
		bool L_17 = __this->get_mLoop_4();
		__this->set_mActive_9(L_17);
	}

IL_00b8:
	{
		bool L_18 = __this->get_mActive_9();
		if (!L_18)
		{
			goto IL_00f5;
		}
	}
	{
		UISprite_t661437049 * L_19 = __this->get_mSprite_6();
		List_1_t1765447871 * L_20 = __this->get_mSpriteNames_10();
		int32_t L_21 = __this->get_mIndex_8();
		NullCheck(L_20);
		String_t* L_22 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_20, L_21);
		NullCheck(L_19);
		UISprite_set_spriteName_m4264380499(L_19, L_22, /*hidden argument*/NULL);
		bool L_23 = __this->get_mSnap_5();
		if (!L_23)
		{
			goto IL_00f5;
		}
	}
	{
		UISprite_t661437049 * L_24 = __this->get_mSprite_6();
		NullCheck(L_24);
		VirtActionInvoker0::Invoke(33 /* System.Void UISprite::MakePixelPerfect() */, L_24);
	}

IL_00f5:
	{
		float L_25 = V_0;
		float L_26 = __this->get_mDelta_7();
		if ((((float)L_25) < ((float)L_26)))
		{
			goto IL_0061;
		}
	}

IL_0101:
	{
		return;
	}
}
// System.Void UISpriteAnimation::RebuildSpriteList()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUISprite_t661437049_m4019381612_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2806436019_MethodInfo_var;
extern const uint32_t UISpriteAnimation_RebuildSpriteList_m2996903516_MetadataUsageId;
extern "C"  void UISpriteAnimation_RebuildSpriteList_m2996903516 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISpriteAnimation_RebuildSpriteList_m2996903516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t80337596 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	UISpriteData_t3578345923 * V_3 = NULL;
	{
		UISprite_t661437049 * L_0 = __this->get_mSprite_6();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		UISprite_t661437049 * L_2 = Component_GetComponent_TisUISprite_t661437049_m4019381612(__this, /*hidden argument*/Component_GetComponent_TisUISprite_t661437049_m4019381612_MethodInfo_var);
		__this->set_mSprite_6(L_2);
	}

IL_001d:
	{
		List_1_t1765447871 * L_3 = __this->get_mSpriteNames_10();
		NullCheck(L_3);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.String>::Clear() */, L_3);
		UISprite_t661437049 * L_4 = __this->get_mSprite_6();
		bool L_5 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00c3;
		}
	}
	{
		UISprite_t661437049 * L_6 = __this->get_mSprite_6();
		NullCheck(L_6);
		UIAtlas_t281921111 * L_7 = UISprite_get_atlas_m2395763654(L_6, /*hidden argument*/NULL);
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_00c3;
		}
	}
	{
		UISprite_t661437049 * L_9 = __this->get_mSprite_6();
		NullCheck(L_9);
		UIAtlas_t281921111 * L_10 = UISprite_get_atlas_m2395763654(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		List_1_t80337596 * L_11 = UIAtlas_get_spriteList_m1840269286(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		V_1 = 0;
		List_1_t80337596 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UISpriteData>::get_Count() */, L_12);
		V_2 = L_13;
		goto IL_00b1;
	}

IL_006e:
	{
		List_1_t80337596 * L_14 = V_0;
		int32_t L_15 = V_1;
		NullCheck(L_14);
		UISpriteData_t3578345923 * L_16 = VirtFuncInvoker1< UISpriteData_t3578345923 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UISpriteData>::get_Item(System.Int32) */, L_14, L_15);
		V_3 = L_16;
		String_t* L_17 = __this->get_mPrefix_3();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_18 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_009c;
		}
	}
	{
		UISpriteData_t3578345923 * L_19 = V_3;
		NullCheck(L_19);
		String_t* L_20 = L_19->get_name_0();
		String_t* L_21 = __this->get_mPrefix_3();
		NullCheck(L_20);
		bool L_22 = String_StartsWith_m1500793453(L_20, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00ad;
		}
	}

IL_009c:
	{
		List_1_t1765447871 * L_23 = __this->get_mSpriteNames_10();
		UISpriteData_t3578345923 * L_24 = V_3;
		NullCheck(L_24);
		String_t* L_25 = L_24->get_name_0();
		NullCheck(L_23);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_23, L_25);
	}

IL_00ad:
	{
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_00b1:
	{
		int32_t L_27 = V_1;
		int32_t L_28 = V_2;
		if ((((int32_t)L_27) < ((int32_t)L_28)))
		{
			goto IL_006e;
		}
	}
	{
		List_1_t1765447871 * L_29 = __this->get_mSpriteNames_10();
		NullCheck(L_29);
		List_1_Sort_m2806436019(L_29, /*hidden argument*/List_1_Sort_m2806436019_MethodInfo_var);
	}

IL_00c3:
	{
		return;
	}
}
// System.Void UISpriteAnimation::Play()
extern "C"  void UISpriteAnimation_Play_m2910565048 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	{
		__this->set_mActive_9((bool)1);
		return;
	}
}
// System.Void UISpriteAnimation::Pause()
extern "C"  void UISpriteAnimation_Pause_m4031600212 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	{
		__this->set_mActive_9((bool)0);
		return;
	}
}
// System.Void UISpriteAnimation::ResetToBeginning()
extern "C"  void UISpriteAnimation_ResetToBeginning_m4170941207 (UISpriteAnimation_t4279777547 * __this, const MethodInfo* method)
{
	{
		__this->set_mActive_9((bool)1);
		__this->set_mIndex_8(0);
		UISprite_t661437049 * L_0 = __this->get_mSprite_6();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0062;
		}
	}
	{
		List_1_t1765447871 * L_2 = __this->get_mSpriteNames_10();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.String>::get_Count() */, L_2);
		if ((((int32_t)L_3) <= ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		UISprite_t661437049 * L_4 = __this->get_mSprite_6();
		List_1_t1765447871 * L_5 = __this->get_mSpriteNames_10();
		int32_t L_6 = __this->get_mIndex_8();
		NullCheck(L_5);
		String_t* L_7 = VirtFuncInvoker1< String_t*, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32) */, L_5, L_6);
		NullCheck(L_4);
		UISprite_set_spriteName_m4264380499(L_4, L_7, /*hidden argument*/NULL);
		bool L_8 = __this->get_mSnap_5();
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		UISprite_t661437049 * L_9 = __this->get_mSprite_6();
		NullCheck(L_9);
		VirtActionInvoker0::Invoke(33 /* System.Void UISprite::MakePixelPerfect() */, L_9);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UISpriteData::.ctor()
extern Il2CppCodeGenString* _stringLiteral2483154661;
extern const uint32_t UISpriteData__ctor_m46138424_MetadataUsageId;
extern "C"  void UISpriteData__ctor_m46138424 (UISpriteData_t3578345923 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UISpriteData__ctor_m46138424_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_name_0(_stringLiteral2483154661);
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UISpriteData::get_hasBorder()
extern "C"  bool UISpriteData_get_hasBorder_m4230184127 (UISpriteData_t3578345923 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_borderLeft_5();
		int32_t L_1 = __this->get_borderRight_6();
		int32_t L_2 = __this->get_borderTop_7();
		int32_t L_3 = __this->get_borderBottom_8();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0|(int32_t)L_1))|(int32_t)L_2))|(int32_t)L_3))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UISpriteData::get_hasPadding()
extern "C"  bool UISpriteData_get_hasPadding_m291107968 (UISpriteData_t3578345923 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_paddingLeft_9();
		int32_t L_1 = __this->get_paddingRight_10();
		int32_t L_2 = __this->get_paddingTop_11();
		int32_t L_3 = __this->get_paddingBottom_12();
		return (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0|(int32_t)L_1))|(int32_t)L_2))|(int32_t)L_3))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Void UISpriteData::SetRect(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UISpriteData_SetRect_m2351041418 (UISpriteData_t3578345923 * __this, int32_t ___x0, int32_t ___y1, int32_t ___width2, int32_t ___height3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x0;
		__this->set_x_1(L_0);
		int32_t L_1 = ___y1;
		__this->set_y_2(L_1);
		int32_t L_2 = ___width2;
		__this->set_width_3(L_2);
		int32_t L_3 = ___height3;
		__this->set_height_4(L_3);
		return;
	}
}
// System.Void UISpriteData::SetPadding(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UISpriteData_SetPadding_m2620533195 (UISpriteData_t3578345923 * __this, int32_t ___left0, int32_t ___bottom1, int32_t ___right2, int32_t ___top3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___left0;
		__this->set_paddingLeft_9(L_0);
		int32_t L_1 = ___bottom1;
		__this->set_paddingBottom_12(L_1);
		int32_t L_2 = ___right2;
		__this->set_paddingRight_10(L_2);
		int32_t L_3 = ___top3;
		__this->set_paddingTop_11(L_3);
		return;
	}
}
// System.Void UISpriteData::SetBorder(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UISpriteData_SetBorder_m3094073346 (UISpriteData_t3578345923 * __this, int32_t ___left0, int32_t ___bottom1, int32_t ___right2, int32_t ___top3, const MethodInfo* method)
{
	{
		int32_t L_0 = ___left0;
		__this->set_borderLeft_5(L_0);
		int32_t L_1 = ___bottom1;
		__this->set_borderBottom_8(L_1);
		int32_t L_2 = ___right2;
		__this->set_borderRight_6(L_2);
		int32_t L_3 = ___top3;
		__this->set_borderTop_7(L_3);
		return;
	}
}
// System.Void UISpriteData::CopyFrom(UISpriteData)
extern "C"  void UISpriteData_CopyFrom_m1707083048 (UISpriteData_t3578345923 * __this, UISpriteData_t3578345923 * ___sd0, const MethodInfo* method)
{
	{
		UISpriteData_t3578345923 * L_0 = ___sd0;
		NullCheck(L_0);
		String_t* L_1 = L_0->get_name_0();
		__this->set_name_0(L_1);
		UISpriteData_t3578345923 * L_2 = ___sd0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_x_1();
		__this->set_x_1(L_3);
		UISpriteData_t3578345923 * L_4 = ___sd0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_y_2();
		__this->set_y_2(L_5);
		UISpriteData_t3578345923 * L_6 = ___sd0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_width_3();
		__this->set_width_3(L_7);
		UISpriteData_t3578345923 * L_8 = ___sd0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_height_4();
		__this->set_height_4(L_9);
		UISpriteData_t3578345923 * L_10 = ___sd0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_borderLeft_5();
		__this->set_borderLeft_5(L_11);
		UISpriteData_t3578345923 * L_12 = ___sd0;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_borderRight_6();
		__this->set_borderRight_6(L_13);
		UISpriteData_t3578345923 * L_14 = ___sd0;
		NullCheck(L_14);
		int32_t L_15 = L_14->get_borderTop_7();
		__this->set_borderTop_7(L_15);
		UISpriteData_t3578345923 * L_16 = ___sd0;
		NullCheck(L_16);
		int32_t L_17 = L_16->get_borderBottom_8();
		__this->set_borderBottom_8(L_17);
		UISpriteData_t3578345923 * L_18 = ___sd0;
		NullCheck(L_18);
		int32_t L_19 = L_18->get_paddingLeft_9();
		__this->set_paddingLeft_9(L_19);
		UISpriteData_t3578345923 * L_20 = ___sd0;
		NullCheck(L_20);
		int32_t L_21 = L_20->get_paddingRight_10();
		__this->set_paddingRight_10(L_21);
		UISpriteData_t3578345923 * L_22 = ___sd0;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_paddingTop_11();
		__this->set_paddingTop_11(L_23);
		UISpriteData_t3578345923 * L_24 = ___sd0;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_paddingBottom_12();
		__this->set_paddingBottom_12(L_25);
		return;
	}
}
// System.Void UISpriteData::CopyBorderFrom(UISpriteData)
extern "C"  void UISpriteData_CopyBorderFrom_m3148282484 (UISpriteData_t3578345923 * __this, UISpriteData_t3578345923 * ___sd0, const MethodInfo* method)
{
	{
		UISpriteData_t3578345923 * L_0 = ___sd0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_borderLeft_5();
		__this->set_borderLeft_5(L_1);
		UISpriteData_t3578345923 * L_2 = ___sd0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_borderRight_6();
		__this->set_borderRight_6(L_3);
		UISpriteData_t3578345923 * L_4 = ___sd0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_borderTop_7();
		__this->set_borderTop_7(L_5);
		UISpriteData_t3578345923 * L_6 = ___sd0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_borderBottom_8();
		__this->set_borderBottom_8(L_7);
		return;
	}
}
// System.Void UIStorageSlot::.ctor()
extern "C"  void UIStorageSlot__ctor_m708644646 (UIStorageSlot_t2805916645 * __this, const MethodInfo* method)
{
	{
		UIItemSlot__ctor_m2623391702(__this, /*hidden argument*/NULL);
		return;
	}
}
// InvGameItem UIStorageSlot::get_observedItem()
extern "C"  InvGameItem_t1588794646 * UIStorageSlot_get_observedItem_m714319717 (UIStorageSlot_t2805916645 * __this, const MethodInfo* method)
{
	InvGameItem_t1588794646 * G_B3_0 = NULL;
	{
		UIItemStorage_t913329332 * L_0 = __this->get_storage_11();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		UIItemStorage_t913329332 * L_2 = __this->get_storage_11();
		int32_t L_3 = __this->get_slot_12();
		NullCheck(L_2);
		InvGameItem_t1588794646 * L_4 = UIItemStorage_GetItem_m1086213036(L_2, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_0028;
	}

IL_0027:
	{
		G_B3_0 = ((InvGameItem_t1588794646 *)(NULL));
	}

IL_0028:
	{
		return G_B3_0;
	}
}
// InvGameItem UIStorageSlot::Replace(InvGameItem)
extern "C"  InvGameItem_t1588794646 * UIStorageSlot_Replace_m2187386631 (UIStorageSlot_t2805916645 * __this, InvGameItem_t1588794646 * ___item0, const MethodInfo* method)
{
	InvGameItem_t1588794646 * G_B3_0 = NULL;
	{
		UIItemStorage_t913329332 * L_0 = __this->get_storage_11();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		UIItemStorage_t913329332 * L_2 = __this->get_storage_11();
		int32_t L_3 = __this->get_slot_12();
		InvGameItem_t1588794646 * L_4 = ___item0;
		NullCheck(L_2);
		InvGameItem_t1588794646 * L_5 = UIItemStorage_Replace_m1762372205(L_2, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
		goto IL_0029;
	}

IL_0028:
	{
		InvGameItem_t1588794646 * L_6 = ___item0;
		G_B3_0 = L_6;
	}

IL_0029:
	{
		return G_B3_0;
	}
}
// System.Void UIStretch::.ctor()
extern "C"  void UIStretch__ctor_m2565728954 (UIStretch_t3439076817 * __this, const MethodInfo* method)
{
	{
		__this->set_runOnlyOnce_5((bool)1);
		Vector2_t3525329788  L_0 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_relativeSize_6(L_0);
		Vector2_t3525329788  L_1 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_initialSize_7(L_1);
		Vector2_t3525329788  L_2 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_borderPadding_8(L_2);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIStretch::Awake()
extern Il2CppClass* Rect_t1525428817_il2cpp_TypeInfo_var;
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern Il2CppClass* OnScreenResize_t3539115999_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAnimation_t350396337_m2546983788_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisUISprite_t661437049_m4019381612_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIPanel_t295209936_m1836641897_MethodInfo_var;
extern const MethodInfo* UIStretch_ScreenSizeChanged_m2502851327_MethodInfo_var;
extern const uint32_t UIStretch_Awake_m2803334173_MetadataUsageId;
extern "C"  void UIStretch_Awake_m2803334173 (UIStretch_t3439076817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIStretch_Awake_m2803334173_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rect_t1525428817  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Animation_t350396337 * L_0 = Component_GetComponent_TisAnimation_t350396337_m2546983788(__this, /*hidden argument*/Component_GetComponent_TisAnimation_t350396337_m2546983788_MethodInfo_var);
		__this->set_mAnim_15(L_0);
		Initobj (Rect_t1525428817_il2cpp_TypeInfo_var, (&V_0));
		Rect_t1525428817  L_1 = V_0;
		__this->set_mRect_16(L_1);
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_mTrans_10(L_2);
		UIWidget_t769069560 * L_3 = Component_GetComponent_TisUIWidget_t769069560_m2158946701(__this, /*hidden argument*/Component_GetComponent_TisUIWidget_t769069560_m2158946701_MethodInfo_var);
		__this->set_mWidget_11(L_3);
		UISprite_t661437049 * L_4 = Component_GetComponent_TisUISprite_t661437049_m4019381612(__this, /*hidden argument*/Component_GetComponent_TisUISprite_t661437049_m4019381612_MethodInfo_var);
		__this->set_mSprite_12(L_4);
		UIPanel_t295209936 * L_5 = Component_GetComponent_TisUIPanel_t295209936_m1836641897(__this, /*hidden argument*/Component_GetComponent_TisUIPanel_t295209936_m1836641897_MethodInfo_var);
		__this->set_mPanel_13(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		OnScreenResize_t3539115999 * L_6 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_onScreenResize_8();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)UIStretch_ScreenSizeChanged_m2502851327_MethodInfo_var);
		OnScreenResize_t3539115999 * L_8 = (OnScreenResize_t3539115999 *)il2cpp_codegen_object_new(OnScreenResize_t3539115999_il2cpp_TypeInfo_var);
		OnScreenResize__ctor_m1761305900(L_8, __this, L_7, /*hidden argument*/NULL);
		Delegate_t3660574010 * L_9 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
		((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->set_onScreenResize_8(((OnScreenResize_t3539115999 *)CastclassSealed(L_9, OnScreenResize_t3539115999_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIStretch::OnDestroy()
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern Il2CppClass* OnScreenResize_t3539115999_il2cpp_TypeInfo_var;
extern const MethodInfo* UIStretch_ScreenSizeChanged_m2502851327_MethodInfo_var;
extern const uint32_t UIStretch_OnDestroy_m3589788339_MetadataUsageId;
extern "C"  void UIStretch_OnDestroy_m3589788339 (UIStretch_t3439076817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIStretch_OnDestroy_m3589788339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		OnScreenResize_t3539115999 * L_0 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_onScreenResize_8();
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIStretch_ScreenSizeChanged_m2502851327_MethodInfo_var);
		OnScreenResize_t3539115999 * L_2 = (OnScreenResize_t3539115999 *)il2cpp_codegen_object_new(OnScreenResize_t3539115999_il2cpp_TypeInfo_var);
		OnScreenResize__ctor_m1761305900(L_2, __this, L_1, /*hidden argument*/NULL);
		Delegate_t3660574010 * L_3 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->set_onScreenResize_8(((OnScreenResize_t3539115999 *)CastclassSealed(L_3, OnScreenResize_t3539115999_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void UIStretch::ScreenSizeChanged()
extern "C"  void UIStretch_ScreenSizeChanged_m2502851327 (UIStretch_t3439076817 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mStarted_17();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		bool L_1 = __this->get_runOnlyOnce_5();
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		UIStretch_Update_m3955048339(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Void UIStretch::Start()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* NGUITools_FindInParents_TisUIRoot_t2503447958_m283231053_MethodInfo_var;
extern const uint32_t UIStretch_Start_m1512866746_MetadataUsageId;
extern "C"  void UIStretch_Start_m1512866746 (UIStretch_t3439076817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIStretch_Start_m1512866746_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = __this->get_container_3();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		UIWidget_t769069560 * L_2 = __this->get_widgetContainer_9();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003a;
		}
	}
	{
		UIWidget_t769069560 * L_4 = __this->get_widgetContainer_9();
		NullCheck(L_4);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		__this->set_container_3(L_5);
		__this->set_widgetContainer_9((UIWidget_t769069560 *)NULL);
	}

IL_003a:
	{
		Camera_t3533968274 * L_6 = __this->get_uiCamera_2();
		bool L_7 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0061;
		}
	}
	{
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = GameObject_get_layer_m1648550306(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		Camera_t3533968274 * L_10 = NGUITools_FindCameraForLayer_m3244779261(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		__this->set_uiCamera_2(L_10);
	}

IL_0061:
	{
		GameObject_t4012695102 * L_11 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		UIRoot_t2503447958 * L_12 = NGUITools_FindInParents_TisUIRoot_t2503447958_m283231053(NULL /*static, unused*/, L_11, /*hidden argument*/NGUITools_FindInParents_TisUIRoot_t2503447958_m283231053_MethodInfo_var);
		__this->set_mRoot_14(L_12);
		UIStretch_Update_m3955048339(__this, /*hidden argument*/NULL);
		__this->set_mStarted_17((bool)1);
		return;
	}
}
// System.Void UIStretch::Update()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisUIWidget_t769069560_m3896075237_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisUIPanel_t295209936_m1477036305_MethodInfo_var;
extern const uint32_t UIStretch_Update_m3955048339_MetadataUsageId;
extern "C"  void UIStretch_Update_m3955048339 (UIStretch_t3439076817 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIStretch_Update_m3955048339_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIWidget_t769069560 * V_0 = NULL;
	UIPanel_t295209936 * V_1 = NULL;
	float V_2 = 0.0f;
	Bounds_t3518514978  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector4_t3525329790  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Transform_t284553113 * V_6 = NULL;
	Bounds_t3518514978  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Vector3_t3525329789  V_11;
	memset(&V_11, 0, sizeof(V_11));
	float V_12 = 0.0f;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	float V_19 = 0.0f;
	float V_20 = 0.0f;
	Vector4_t3525329790  V_21;
	memset(&V_21, 0, sizeof(V_21));
	Vector3_t3525329789  V_22;
	memset(&V_22, 0, sizeof(V_22));
	Vector3_t3525329789  V_23;
	memset(&V_23, 0, sizeof(V_23));
	Vector3_t3525329789  V_24;
	memset(&V_24, 0, sizeof(V_24));
	Vector3_t3525329789  V_25;
	memset(&V_25, 0, sizeof(V_25));
	Vector3_t3525329789  V_26;
	memset(&V_26, 0, sizeof(V_26));
	Vector3_t3525329789  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Vector3_t3525329789  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Vector3_t3525329789  V_29;
	memset(&V_29, 0, sizeof(V_29));
	UIWidget_t769069560 * G_B7_0 = NULL;
	UIPanel_t295209936 * G_B11_0 = NULL;
	float G_B18_0 = 0.0f;
	Bounds_t3518514978  G_B25_0;
	memset(&G_B25_0, 0, sizeof(G_B25_0));
	Vector3_t3525329789  G_B37_0;
	memset(&G_B37_0, 0, sizeof(G_B37_0));
	float G_B57_0 = 0.0f;
	{
		Animation_t350396337 * L_0 = __this->get_mAnim_15();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Animation_t350396337 * L_2 = __this->get_mAnim_15();
		NullCheck(L_2);
		bool L_3 = Animation_get_isPlaying_m3295833780(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		return;
	}

IL_0022:
	{
		int32_t L_4 = __this->get_style_4();
		if (!L_4)
		{
			goto IL_07d1;
		}
	}
	{
		GameObject_t4012695102 * L_5 = __this->get_container_3();
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		G_B7_0 = ((UIWidget_t769069560 *)(NULL));
		goto IL_004f;
	}

IL_0044:
	{
		GameObject_t4012695102 * L_7 = __this->get_container_3();
		NullCheck(L_7);
		UIWidget_t769069560 * L_8 = GameObject_GetComponent_TisUIWidget_t769069560_m3896075237(L_7, /*hidden argument*/GameObject_GetComponent_TisUIWidget_t769069560_m3896075237_MethodInfo_var);
		G_B7_0 = L_8;
	}

IL_004f:
	{
		V_0 = G_B7_0;
		GameObject_t4012695102 * L_9 = __this->get_container_3();
		bool L_10 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_9, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		UIWidget_t769069560 * L_11 = V_0;
		bool L_12 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_11, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0073;
		}
	}
	{
		G_B11_0 = ((UIPanel_t295209936 *)(NULL));
		goto IL_007e;
	}

IL_0073:
	{
		GameObject_t4012695102 * L_13 = __this->get_container_3();
		NullCheck(L_13);
		UIPanel_t295209936 * L_14 = GameObject_GetComponent_TisUIPanel_t295209936_m1477036305(L_13, /*hidden argument*/GameObject_GetComponent_TisUIPanel_t295209936_m1477036305_MethodInfo_var);
		G_B11_0 = L_14;
	}

IL_007e:
	{
		V_1 = G_B11_0;
		V_2 = (1.0f);
		UIWidget_t769069560 * L_15 = V_0;
		bool L_16 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_15, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0114;
		}
	}
	{
		UIWidget_t769069560 * L_17 = V_0;
		Transform_t284553113 * L_18 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_t284553113 * L_19 = Transform_get_parent_m2236876972(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Bounds_t3518514978  L_20 = UIWidget_CalculateBounds_m1943782482(L_17, L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		Rect_t1525428817 * L_21 = __this->get_address_of_mRect_16();
		Vector3_t3525329789  L_22 = Bounds_get_min_m2329472069((&V_3), /*hidden argument*/NULL);
		V_22 = L_22;
		float L_23 = (&V_22)->get_x_1();
		Rect_set_x_m577970569(L_21, L_23, /*hidden argument*/NULL);
		Rect_t1525428817 * L_24 = __this->get_address_of_mRect_16();
		Vector3_t3525329789  L_25 = Bounds_get_min_m2329472069((&V_3), /*hidden argument*/NULL);
		V_23 = L_25;
		float L_26 = (&V_23)->get_y_2();
		Rect_set_y_m67436392(L_24, L_26, /*hidden argument*/NULL);
		Rect_t1525428817 * L_27 = __this->get_address_of_mRect_16();
		Vector3_t3525329789  L_28 = Bounds_get_size_m3666348432((&V_3), /*hidden argument*/NULL);
		V_24 = L_28;
		float L_29 = (&V_24)->get_x_1();
		Rect_set_width_m3771513595(L_27, L_29, /*hidden argument*/NULL);
		Rect_t1525428817 * L_30 = __this->get_address_of_mRect_16();
		Vector3_t3525329789  L_31 = Bounds_get_size_m3666348432((&V_3), /*hidden argument*/NULL);
		V_25 = L_31;
		float L_32 = (&V_25)->get_y_2();
		Rect_set_height_m3398820332(L_30, L_32, /*hidden argument*/NULL);
		goto IL_0339;
	}

IL_0114:
	{
		UIPanel_t295209936 * L_33 = V_1;
		bool L_34 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_33, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_022f;
		}
	}
	{
		UIPanel_t295209936 * L_35 = V_1;
		NullCheck(L_35);
		int32_t L_36 = UIPanel_get_clipping_m1917206620(L_35, /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_01be;
		}
	}
	{
		UIRoot_t2503447958 * L_37 = __this->get_mRoot_14();
		bool L_38 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_37, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_015a;
		}
	}
	{
		UIRoot_t2503447958 * L_39 = __this->get_mRoot_14();
		NullCheck(L_39);
		int32_t L_40 = UIRoot_get_activeHeight_m3474867427(L_39, /*hidden argument*/NULL);
		int32_t L_41 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B18_0 = ((float)((float)((float)((float)(((float)((float)L_40)))/(float)(((float)((float)L_41)))))*(float)(0.5f)));
		goto IL_015f;
	}

IL_015a:
	{
		G_B18_0 = (0.5f);
	}

IL_015f:
	{
		V_4 = G_B18_0;
		Rect_t1525428817 * L_42 = __this->get_address_of_mRect_16();
		int32_t L_43 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_44 = V_4;
		Rect_set_xMin_m265803321(L_42, ((float)((float)(((float)((float)((-L_43)))))*(float)L_44)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_45 = __this->get_address_of_mRect_16();
		int32_t L_46 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_47 = V_4;
		Rect_set_yMin_m3716298746(L_45, ((float)((float)(((float)((float)((-L_46)))))*(float)L_47)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_48 = __this->get_address_of_mRect_16();
		Rect_t1525428817 * L_49 = __this->get_address_of_mRect_16();
		float L_50 = Rect_get_xMin_m371109962(L_49, /*hidden argument*/NULL);
		Rect_set_xMax_m1513853159(L_48, ((-L_50)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_51 = __this->get_address_of_mRect_16();
		Rect_t1525428817 * L_52 = __this->get_address_of_mRect_16();
		float L_53 = Rect_get_yMin_m399739113(L_52, /*hidden argument*/NULL);
		Rect_set_yMax_m669381288(L_51, ((-L_53)), /*hidden argument*/NULL);
		goto IL_022a;
	}

IL_01be:
	{
		UIPanel_t295209936 * L_54 = V_1;
		NullCheck(L_54);
		Vector4_t3525329790  L_55 = UIPanel_get_finalClipRegion_m2237897219(L_54, /*hidden argument*/NULL);
		V_5 = L_55;
		Rect_t1525428817 * L_56 = __this->get_address_of_mRect_16();
		float L_57 = (&V_5)->get_x_1();
		float L_58 = (&V_5)->get_z_3();
		Rect_set_x_m577970569(L_56, ((float)((float)L_57-(float)((float)((float)L_58*(float)(0.5f))))), /*hidden argument*/NULL);
		Rect_t1525428817 * L_59 = __this->get_address_of_mRect_16();
		float L_60 = (&V_5)->get_y_2();
		float L_61 = (&V_5)->get_w_4();
		Rect_set_y_m67436392(L_59, ((float)((float)L_60-(float)((float)((float)L_61*(float)(0.5f))))), /*hidden argument*/NULL);
		Rect_t1525428817 * L_62 = __this->get_address_of_mRect_16();
		float L_63 = (&V_5)->get_z_3();
		Rect_set_width_m3771513595(L_62, L_63, /*hidden argument*/NULL);
		Rect_t1525428817 * L_64 = __this->get_address_of_mRect_16();
		float L_65 = (&V_5)->get_w_4();
		Rect_set_height_m3398820332(L_64, L_65, /*hidden argument*/NULL);
	}

IL_022a:
	{
		goto IL_0339;
	}

IL_022f:
	{
		GameObject_t4012695102 * L_66 = __this->get_container_3();
		bool L_67 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_66, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_02f4;
		}
	}
	{
		Transform_t284553113 * L_68 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_68);
		Transform_t284553113 * L_69 = Transform_get_parent_m2236876972(L_68, /*hidden argument*/NULL);
		V_6 = L_69;
		Transform_t284553113 * L_70 = V_6;
		bool L_71 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_70, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_0271;
		}
	}
	{
		Transform_t284553113 * L_72 = V_6;
		GameObject_t4012695102 * L_73 = __this->get_container_3();
		NullCheck(L_73);
		Transform_t284553113 * L_74 = GameObject_get_transform_m1278640159(L_73, /*hidden argument*/NULL);
		Bounds_t3518514978  L_75 = NGUIMath_CalculateRelativeWidgetBounds_m2719356150(NULL /*static, unused*/, L_72, L_74, /*hidden argument*/NULL);
		G_B25_0 = L_75;
		goto IL_0281;
	}

IL_0271:
	{
		GameObject_t4012695102 * L_76 = __this->get_container_3();
		NullCheck(L_76);
		Transform_t284553113 * L_77 = GameObject_get_transform_m1278640159(L_76, /*hidden argument*/NULL);
		Bounds_t3518514978  L_78 = NGUIMath_CalculateRelativeWidgetBounds_m3101556671(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		G_B25_0 = L_78;
	}

IL_0281:
	{
		V_7 = G_B25_0;
		Rect_t1525428817 * L_79 = __this->get_address_of_mRect_16();
		Vector3_t3525329789  L_80 = Bounds_get_min_m2329472069((&V_7), /*hidden argument*/NULL);
		V_26 = L_80;
		float L_81 = (&V_26)->get_x_1();
		Rect_set_x_m577970569(L_79, L_81, /*hidden argument*/NULL);
		Rect_t1525428817 * L_82 = __this->get_address_of_mRect_16();
		Vector3_t3525329789  L_83 = Bounds_get_min_m2329472069((&V_7), /*hidden argument*/NULL);
		V_27 = L_83;
		float L_84 = (&V_27)->get_y_2();
		Rect_set_y_m67436392(L_82, L_84, /*hidden argument*/NULL);
		Rect_t1525428817 * L_85 = __this->get_address_of_mRect_16();
		Vector3_t3525329789  L_86 = Bounds_get_size_m3666348432((&V_7), /*hidden argument*/NULL);
		V_28 = L_86;
		float L_87 = (&V_28)->get_x_1();
		Rect_set_width_m3771513595(L_85, L_87, /*hidden argument*/NULL);
		Rect_t1525428817 * L_88 = __this->get_address_of_mRect_16();
		Vector3_t3525329789  L_89 = Bounds_get_size_m3666348432((&V_7), /*hidden argument*/NULL);
		V_29 = L_89;
		float L_90 = (&V_29)->get_y_2();
		Rect_set_height_m3398820332(L_88, L_90, /*hidden argument*/NULL);
		goto IL_0339;
	}

IL_02f4:
	{
		Camera_t3533968274 * L_91 = __this->get_uiCamera_2();
		bool L_92 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_91, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_92)
		{
			goto IL_0338;
		}
	}
	{
		Camera_t3533968274 * L_93 = __this->get_uiCamera_2();
		NullCheck(L_93);
		Rect_t1525428817  L_94 = Camera_get_pixelRect_m936851539(L_93, /*hidden argument*/NULL);
		__this->set_mRect_16(L_94);
		UIRoot_t2503447958 * L_95 = __this->get_mRoot_14();
		bool L_96 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_95, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_96)
		{
			goto IL_0333;
		}
	}
	{
		UIRoot_t2503447958 * L_97 = __this->get_mRoot_14();
		NullCheck(L_97);
		float L_98 = UIRoot_get_pixelSizeAdjustment_m2382285954(L_97, /*hidden argument*/NULL);
		V_2 = L_98;
	}

IL_0333:
	{
		goto IL_0339;
	}

IL_0338:
	{
		return;
	}

IL_0339:
	{
		Rect_t1525428817 * L_99 = __this->get_address_of_mRect_16();
		float L_100 = Rect_get_width_m2824209432(L_99, /*hidden argument*/NULL);
		V_8 = L_100;
		Rect_t1525428817 * L_101 = __this->get_address_of_mRect_16();
		float L_102 = Rect_get_height_m2154960823(L_101, /*hidden argument*/NULL);
		V_9 = L_102;
		float L_103 = V_2;
		if ((((float)L_103) == ((float)(1.0f))))
		{
			goto IL_0389;
		}
	}
	{
		float L_104 = V_9;
		if ((!(((float)L_104) > ((float)(1.0f)))))
		{
			goto IL_0389;
		}
	}
	{
		UIRoot_t2503447958 * L_105 = __this->get_mRoot_14();
		NullCheck(L_105);
		int32_t L_106 = UIRoot_get_activeHeight_m3474867427(L_105, /*hidden argument*/NULL);
		float L_107 = V_9;
		V_10 = ((float)((float)(((float)((float)L_106)))/(float)L_107));
		float L_108 = V_8;
		float L_109 = V_10;
		V_8 = ((float)((float)L_108*(float)L_109));
		float L_110 = V_9;
		float L_111 = V_10;
		V_9 = ((float)((float)L_110*(float)L_111));
	}

IL_0389:
	{
		UIWidget_t769069560 * L_112 = __this->get_mWidget_11();
		bool L_113 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_112, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_113)
		{
			goto IL_03bc;
		}
	}
	{
		UIWidget_t769069560 * L_114 = __this->get_mWidget_11();
		NullCheck(L_114);
		int32_t L_115 = UIWidget_get_width_m293857264(L_114, /*hidden argument*/NULL);
		UIWidget_t769069560 * L_116 = __this->get_mWidget_11();
		NullCheck(L_116);
		int32_t L_117 = UIWidget_get_height_m1023454943(L_116, /*hidden argument*/NULL);
		Vector3_t3525329789  L_118;
		memset(&L_118, 0, sizeof(L_118));
		Vector3__ctor_m1846874791(&L_118, (((float)((float)L_115))), (((float)((float)L_117))), /*hidden argument*/NULL);
		G_B37_0 = L_118;
		goto IL_03c7;
	}

IL_03bc:
	{
		Transform_t284553113 * L_119 = __this->get_mTrans_10();
		NullCheck(L_119);
		Vector3_t3525329789  L_120 = Transform_get_localScale_m3886572677(L_119, /*hidden argument*/NULL);
		G_B37_0 = L_120;
	}

IL_03c7:
	{
		V_11 = G_B37_0;
		int32_t L_121 = __this->get_style_4();
		if ((!(((uint32_t)L_121) == ((uint32_t)4))))
		{
			goto IL_0404;
		}
	}
	{
		Vector2_t3525329788 * L_122 = __this->get_address_of_relativeSize_6();
		float L_123 = L_122->get_x_1();
		float L_124 = V_9;
		(&V_11)->set_x_1(((float)((float)L_123*(float)L_124)));
		Vector2_t3525329788 * L_125 = __this->get_address_of_relativeSize_6();
		float L_126 = L_125->get_y_2();
		float L_127 = V_9;
		(&V_11)->set_y_2(((float)((float)L_126*(float)L_127)));
		goto IL_057c;
	}

IL_0404:
	{
		int32_t L_128 = __this->get_style_4();
		if ((!(((uint32_t)L_128) == ((uint32_t)5))))
		{
			goto IL_049f;
		}
	}
	{
		float L_129 = V_8;
		float L_130 = V_9;
		V_12 = ((float)((float)L_129/(float)L_130));
		Vector2_t3525329788 * L_131 = __this->get_address_of_initialSize_7();
		float L_132 = L_131->get_x_1();
		Vector2_t3525329788 * L_133 = __this->get_address_of_initialSize_7();
		float L_134 = L_133->get_y_2();
		V_13 = ((float)((float)L_132/(float)L_134));
		float L_135 = V_13;
		float L_136 = V_12;
		if ((!(((float)L_135) < ((float)L_136))))
		{
			goto IL_046c;
		}
	}
	{
		float L_137 = V_8;
		Vector2_t3525329788 * L_138 = __this->get_address_of_initialSize_7();
		float L_139 = L_138->get_x_1();
		V_14 = ((float)((float)L_137/(float)L_139));
		float L_140 = V_8;
		(&V_11)->set_x_1(L_140);
		Vector2_t3525329788 * L_141 = __this->get_address_of_initialSize_7();
		float L_142 = L_141->get_y_2();
		float L_143 = V_14;
		(&V_11)->set_y_2(((float)((float)L_142*(float)L_143)));
		goto IL_049a;
	}

IL_046c:
	{
		float L_144 = V_9;
		Vector2_t3525329788 * L_145 = __this->get_address_of_initialSize_7();
		float L_146 = L_145->get_y_2();
		V_15 = ((float)((float)L_144/(float)L_146));
		Vector2_t3525329788 * L_147 = __this->get_address_of_initialSize_7();
		float L_148 = L_147->get_x_1();
		float L_149 = V_15;
		(&V_11)->set_x_1(((float)((float)L_148*(float)L_149)));
		float L_150 = V_9;
		(&V_11)->set_y_2(L_150);
	}

IL_049a:
	{
		goto IL_057c;
	}

IL_049f:
	{
		int32_t L_151 = __this->get_style_4();
		if ((!(((uint32_t)L_151) == ((uint32_t)6))))
		{
			goto IL_053a;
		}
	}
	{
		float L_152 = V_8;
		float L_153 = V_9;
		V_16 = ((float)((float)L_152/(float)L_153));
		Vector2_t3525329788 * L_154 = __this->get_address_of_initialSize_7();
		float L_155 = L_154->get_x_1();
		Vector2_t3525329788 * L_156 = __this->get_address_of_initialSize_7();
		float L_157 = L_156->get_y_2();
		V_17 = ((float)((float)L_155/(float)L_157));
		float L_158 = V_17;
		float L_159 = V_16;
		if ((!(((float)L_158) > ((float)L_159))))
		{
			goto IL_0507;
		}
	}
	{
		float L_160 = V_8;
		Vector2_t3525329788 * L_161 = __this->get_address_of_initialSize_7();
		float L_162 = L_161->get_x_1();
		V_18 = ((float)((float)L_160/(float)L_162));
		float L_163 = V_8;
		(&V_11)->set_x_1(L_163);
		Vector2_t3525329788 * L_164 = __this->get_address_of_initialSize_7();
		float L_165 = L_164->get_y_2();
		float L_166 = V_18;
		(&V_11)->set_y_2(((float)((float)L_165*(float)L_166)));
		goto IL_0535;
	}

IL_0507:
	{
		float L_167 = V_9;
		Vector2_t3525329788 * L_168 = __this->get_address_of_initialSize_7();
		float L_169 = L_168->get_y_2();
		V_19 = ((float)((float)L_167/(float)L_169));
		Vector2_t3525329788 * L_170 = __this->get_address_of_initialSize_7();
		float L_171 = L_170->get_x_1();
		float L_172 = V_19;
		(&V_11)->set_x_1(((float)((float)L_171*(float)L_172)));
		float L_173 = V_9;
		(&V_11)->set_y_2(L_173);
	}

IL_0535:
	{
		goto IL_057c;
	}

IL_053a:
	{
		int32_t L_174 = __this->get_style_4();
		if ((((int32_t)L_174) == ((int32_t)2)))
		{
			goto IL_055b;
		}
	}
	{
		Vector2_t3525329788 * L_175 = __this->get_address_of_relativeSize_6();
		float L_176 = L_175->get_x_1();
		float L_177 = V_8;
		(&V_11)->set_x_1(((float)((float)L_176*(float)L_177)));
	}

IL_055b:
	{
		int32_t L_178 = __this->get_style_4();
		if ((((int32_t)L_178) == ((int32_t)1)))
		{
			goto IL_057c;
		}
	}
	{
		Vector2_t3525329788 * L_179 = __this->get_address_of_relativeSize_6();
		float L_180 = L_179->get_y_2();
		float L_181 = V_9;
		(&V_11)->set_y_2(((float)((float)L_180*(float)L_181)));
	}

IL_057c:
	{
		UISprite_t661437049 * L_182 = __this->get_mSprite_12();
		bool L_183 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_182, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_183)
		{
			goto IL_0649;
		}
	}
	{
		UISprite_t661437049 * L_184 = __this->get_mSprite_12();
		NullCheck(L_184);
		UIAtlas_t281921111 * L_185 = UISprite_get_atlas_m2395763654(L_184, /*hidden argument*/NULL);
		bool L_186 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_185, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_186)
		{
			goto IL_05b8;
		}
	}
	{
		UISprite_t661437049 * L_187 = __this->get_mSprite_12();
		NullCheck(L_187);
		UIAtlas_t281921111 * L_188 = UISprite_get_atlas_m2395763654(L_187, /*hidden argument*/NULL);
		NullCheck(L_188);
		float L_189 = UIAtlas_get_pixelSize_m3644908156(L_188, /*hidden argument*/NULL);
		G_B57_0 = L_189;
		goto IL_05bd;
	}

IL_05b8:
	{
		G_B57_0 = (1.0f);
	}

IL_05bd:
	{
		V_20 = G_B57_0;
		Vector3_t3525329789 * L_190 = (&V_11);
		float L_191 = L_190->get_x_1();
		Vector2_t3525329788 * L_192 = __this->get_address_of_borderPadding_8();
		float L_193 = L_192->get_x_1();
		float L_194 = V_20;
		L_190->set_x_1(((float)((float)L_191-(float)((float)((float)L_193*(float)L_194)))));
		Vector3_t3525329789 * L_195 = (&V_11);
		float L_196 = L_195->get_y_2();
		Vector2_t3525329788 * L_197 = __this->get_address_of_borderPadding_8();
		float L_198 = L_197->get_y_2();
		float L_199 = V_20;
		L_195->set_y_2(((float)((float)L_196-(float)((float)((float)L_198*(float)L_199)))));
		int32_t L_200 = __this->get_style_4();
		if ((((int32_t)L_200) == ((int32_t)2)))
		{
			goto IL_061a;
		}
	}
	{
		UISprite_t661437049 * L_201 = __this->get_mSprite_12();
		float L_202 = (&V_11)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_203 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_202, /*hidden argument*/NULL);
		NullCheck(L_201);
		UIWidget_set_width_m3480390811(L_201, L_203, /*hidden argument*/NULL);
	}

IL_061a:
	{
		int32_t L_204 = __this->get_style_4();
		if ((((int32_t)L_204) == ((int32_t)1)))
		{
			goto IL_063d;
		}
	}
	{
		UISprite_t661437049 * L_205 = __this->get_mSprite_12();
		float L_206 = (&V_11)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_207 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_206, /*hidden argument*/NULL);
		NullCheck(L_205);
		UIWidget_set_height_m1838784918(L_205, L_207, /*hidden argument*/NULL);
	}

IL_063d:
	{
		Vector3_t3525329789  L_208 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_11 = L_208;
		goto IL_0791;
	}

IL_0649:
	{
		UIWidget_t769069560 * L_209 = __this->get_mWidget_11();
		bool L_210 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_209, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_210)
		{
			goto IL_06c4;
		}
	}
	{
		int32_t L_211 = __this->get_style_4();
		if ((((int32_t)L_211) == ((int32_t)2)))
		{
			goto IL_0689;
		}
	}
	{
		UIWidget_t769069560 * L_212 = __this->get_mWidget_11();
		float L_213 = (&V_11)->get_x_1();
		Vector2_t3525329788 * L_214 = __this->get_address_of_borderPadding_8();
		float L_215 = L_214->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_216 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_213-(float)L_215)), /*hidden argument*/NULL);
		NullCheck(L_212);
		UIWidget_set_width_m3480390811(L_212, L_216, /*hidden argument*/NULL);
	}

IL_0689:
	{
		int32_t L_217 = __this->get_style_4();
		if ((((int32_t)L_217) == ((int32_t)1)))
		{
			goto IL_06b8;
		}
	}
	{
		UIWidget_t769069560 * L_218 = __this->get_mWidget_11();
		float L_219 = (&V_11)->get_y_2();
		Vector2_t3525329788 * L_220 = __this->get_address_of_borderPadding_8();
		float L_221 = L_220->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_222 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_219-(float)L_221)), /*hidden argument*/NULL);
		NullCheck(L_218);
		UIWidget_set_height_m1838784918(L_218, L_222, /*hidden argument*/NULL);
	}

IL_06b8:
	{
		Vector3_t3525329789  L_223 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_11 = L_223;
		goto IL_0791;
	}

IL_06c4:
	{
		UIPanel_t295209936 * L_224 = __this->get_mPanel_13();
		bool L_225 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_224, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_225)
		{
			goto IL_0747;
		}
	}
	{
		UIPanel_t295209936 * L_226 = __this->get_mPanel_13();
		NullCheck(L_226);
		Vector4_t3525329790  L_227 = UIPanel_get_baseClipRegion_m31363022(L_226, /*hidden argument*/NULL);
		V_21 = L_227;
		int32_t L_228 = __this->get_style_4();
		if ((((int32_t)L_228) == ((int32_t)2)))
		{
			goto IL_0708;
		}
	}
	{
		float L_229 = (&V_11)->get_x_1();
		Vector2_t3525329788 * L_230 = __this->get_address_of_borderPadding_8();
		float L_231 = L_230->get_x_1();
		(&V_21)->set_z_3(((float)((float)L_229-(float)L_231)));
	}

IL_0708:
	{
		int32_t L_232 = __this->get_style_4();
		if ((((int32_t)L_232) == ((int32_t)1)))
		{
			goto IL_072e;
		}
	}
	{
		float L_233 = (&V_11)->get_y_2();
		Vector2_t3525329788 * L_234 = __this->get_address_of_borderPadding_8();
		float L_235 = L_234->get_y_2();
		(&V_21)->set_w_4(((float)((float)L_233-(float)L_235)));
	}

IL_072e:
	{
		UIPanel_t295209936 * L_236 = __this->get_mPanel_13();
		Vector4_t3525329790  L_237 = V_21;
		NullCheck(L_236);
		UIPanel_set_baseClipRegion_m353982685(L_236, L_237, /*hidden argument*/NULL);
		Vector3_t3525329789  L_238 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_11 = L_238;
		goto IL_0791;
	}

IL_0747:
	{
		int32_t L_239 = __this->get_style_4();
		if ((((int32_t)L_239) == ((int32_t)2)))
		{
			goto IL_076c;
		}
	}
	{
		Vector3_t3525329789 * L_240 = (&V_11);
		float L_241 = L_240->get_x_1();
		Vector2_t3525329788 * L_242 = __this->get_address_of_borderPadding_8();
		float L_243 = L_242->get_x_1();
		L_240->set_x_1(((float)((float)L_241-(float)L_243)));
	}

IL_076c:
	{
		int32_t L_244 = __this->get_style_4();
		if ((((int32_t)L_244) == ((int32_t)1)))
		{
			goto IL_0791;
		}
	}
	{
		Vector3_t3525329789 * L_245 = (&V_11);
		float L_246 = L_245->get_y_2();
		Vector2_t3525329788 * L_247 = __this->get_address_of_borderPadding_8();
		float L_248 = L_247->get_y_2();
		L_245->set_y_2(((float)((float)L_246-(float)L_248)));
	}

IL_0791:
	{
		Transform_t284553113 * L_249 = __this->get_mTrans_10();
		NullCheck(L_249);
		Vector3_t3525329789  L_250 = Transform_get_localScale_m3886572677(L_249, /*hidden argument*/NULL);
		Vector3_t3525329789  L_251 = V_11;
		bool L_252 = Vector3_op_Inequality_m231387234(NULL /*static, unused*/, L_250, L_251, /*hidden argument*/NULL);
		if (!L_252)
		{
			goto IL_07b5;
		}
	}
	{
		Transform_t284553113 * L_253 = __this->get_mTrans_10();
		Vector3_t3525329789  L_254 = V_11;
		NullCheck(L_253);
		Transform_set_localScale_m310756934(L_253, L_254, /*hidden argument*/NULL);
	}

IL_07b5:
	{
		bool L_255 = __this->get_runOnlyOnce_5();
		if (!L_255)
		{
			goto IL_07d1;
		}
	}
	{
		bool L_256 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_256)
		{
			goto IL_07d1;
		}
	}
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_07d1:
	{
		return;
	}
}
// System.Void UITable::.ctor()
extern "C"  void UITable__ctor_m3607830289 (UITable_t298892698 * __this, const MethodInfo* method)
{
	{
		__this->set_hideInactive_7((bool)1);
		Vector2_t3525329788  L_0 = Vector2_get_zero_m199872368(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_padding_9(L_0);
		UIWidgetContainer__ctor_m2037457442(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITable::set_repositionNow(System.Boolean)
extern "C"  void UITable_set_repositionNow_m82937923 (UITable_t298892698 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		__this->set_mReposition_14((bool)1);
		Behaviour_set_enabled_m2046806933(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Transform> UITable::GetChildList()
extern Il2CppClass* List_1_t1081512082_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* Comparison_1_t2988227989_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1640872948_MethodInfo_var;
extern const MethodInfo* UIGrid_SortByName_m3179856323_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1879963539_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2423802008_MethodInfo_var;
extern const MethodInfo* UIGrid_SortHorizontal_m646783841_MethodInfo_var;
extern const MethodInfo* UIGrid_SortVertical_m3217656271_MethodInfo_var;
extern const uint32_t UITable_GetChildList_m1703713889_MetadataUsageId;
extern "C"  List_1_t1081512082 * UITable_GetChildList_m1703713889 (UITable_t298892698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITable_GetChildList_m1703713889_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t284553113 * V_0 = NULL;
	List_1_t1081512082 * V_1 = NULL;
	int32_t V_2 = 0;
	Transform_t284553113 * V_3 = NULL;
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		List_1_t1081512082 * L_1 = (List_1_t1081512082 *)il2cpp_codegen_object_new(List_1_t1081512082_il2cpp_TypeInfo_var);
		List_1__ctor_m1640872948(L_1, /*hidden argument*/List_1__ctor_m1640872948_MethodInfo_var);
		V_1 = L_1;
		V_2 = 0;
		goto IL_004d;
	}

IL_0014:
	{
		Transform_t284553113 * L_2 = V_0;
		int32_t L_3 = V_2;
		NullCheck(L_2);
		Transform_t284553113 * L_4 = Transform_GetChild_m4040462992(L_2, L_3, /*hidden argument*/NULL);
		V_3 = L_4;
		bool L_5 = __this->get_hideInactive_7();
		if (!L_5)
		{
			goto IL_0042;
		}
	}
	{
		Transform_t284553113 * L_6 = V_3;
		bool L_7 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0049;
		}
	}
	{
		Transform_t284553113 * L_8 = V_3;
		NullCheck(L_8);
		GameObject_t4012695102 * L_9 = Component_get_gameObject_m1170635899(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_10 = NGUITools_GetActive_m3605198179(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0049;
		}
	}

IL_0042:
	{
		List_1_t1081512082 * L_11 = V_1;
		Transform_t284553113 * L_12 = V_3;
		NullCheck(L_11);
		VirtActionInvoker1< Transform_t284553113 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0) */, L_11, L_12);
	}

IL_0049:
	{
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_14 = V_2;
		Transform_t284553113 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = Transform_get_childCount_m2107810675(L_15, /*hidden argument*/NULL);
		if ((((int32_t)L_14) < ((int32_t)L_16)))
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_17 = __this->get_sorting_4();
		if (!L_17)
		{
			goto IL_00f0;
		}
	}
	{
		int32_t L_18 = __this->get_sorting_4();
		if ((!(((uint32_t)L_18) == ((uint32_t)1))))
		{
			goto IL_0087;
		}
	}
	{
		List_1_t1081512082 * L_19 = V_1;
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)UIGrid_SortByName_m3179856323_MethodInfo_var);
		Comparison_1_t2988227989 * L_21 = (Comparison_1_t2988227989 *)il2cpp_codegen_object_new(Comparison_1_t2988227989_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_21, NULL, L_20, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_19);
		List_1_Sort_m2423802008(L_19, L_21, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
		goto IL_00f0;
	}

IL_0087:
	{
		int32_t L_22 = __this->get_sorting_4();
		if ((!(((uint32_t)L_22) == ((uint32_t)2))))
		{
			goto IL_00aa;
		}
	}
	{
		List_1_t1081512082 * L_23 = V_1;
		IntPtr_t L_24;
		L_24.set_m_value_0((void*)(void*)UIGrid_SortHorizontal_m646783841_MethodInfo_var);
		Comparison_1_t2988227989 * L_25 = (Comparison_1_t2988227989 *)il2cpp_codegen_object_new(Comparison_1_t2988227989_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_25, NULL, L_24, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_23);
		List_1_Sort_m2423802008(L_23, L_25, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
		goto IL_00f0;
	}

IL_00aa:
	{
		int32_t L_26 = __this->get_sorting_4();
		if ((!(((uint32_t)L_26) == ((uint32_t)3))))
		{
			goto IL_00cd;
		}
	}
	{
		List_1_t1081512082 * L_27 = V_1;
		IntPtr_t L_28;
		L_28.set_m_value_0((void*)(void*)UIGrid_SortVertical_m3217656271_MethodInfo_var);
		Comparison_1_t2988227989 * L_29 = (Comparison_1_t2988227989 *)il2cpp_codegen_object_new(Comparison_1_t2988227989_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_29, NULL, L_28, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_27);
		List_1_Sort_m2423802008(L_27, L_29, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
		goto IL_00f0;
	}

IL_00cd:
	{
		Comparison_1_t2988227989 * L_30 = __this->get_onCustomSort_11();
		if (!L_30)
		{
			goto IL_00e9;
		}
	}
	{
		List_1_t1081512082 * L_31 = V_1;
		Comparison_1_t2988227989 * L_32 = __this->get_onCustomSort_11();
		NullCheck(L_31);
		List_1_Sort_m2423802008(L_31, L_32, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
		goto IL_00f0;
	}

IL_00e9:
	{
		List_1_t1081512082 * L_33 = V_1;
		VirtActionInvoker1< List_1_t1081512082 * >::Invoke(4 /* System.Void UITable::Sort(System.Collections.Generic.List`1<UnityEngine.Transform>) */, __this, L_33);
	}

IL_00f0:
	{
		List_1_t1081512082 * L_34 = V_1;
		return L_34;
	}
}
// System.Void UITable::Sort(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern Il2CppClass* Comparison_1_t2988227989_il2cpp_TypeInfo_var;
extern const MethodInfo* UIGrid_SortByName_m3179856323_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1879963539_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2423802008_MethodInfo_var;
extern const uint32_t UITable_Sort_m1046555786_MetadataUsageId;
extern "C"  void UITable_Sort_m1046555786 (UITable_t298892698 * __this, List_1_t1081512082 * ___list0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITable_Sort_m1046555786_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1081512082 * L_0 = ___list0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)UIGrid_SortByName_m3179856323_MethodInfo_var);
		Comparison_1_t2988227989 * L_2 = (Comparison_1_t2988227989 *)il2cpp_codegen_object_new(Comparison_1_t2988227989_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_2, NULL, L_1, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_0);
		List_1_Sort_m2423802008(L_0, L_2, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
		return;
	}
}
// System.Void UITable::Start()
extern "C"  void UITable_Start_m2554968081 (UITable_t298892698 * __this, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(6 /* System.Void UITable::Init() */, __this);
		VirtActionInvoker0::Invoke(8 /* System.Void UITable::Reposition() */, __this);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITable::Init()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243_MethodInfo_var;
extern const uint32_t UITable_Init_m4224338211_MetadataUsageId;
extern "C"  void UITable_Init_m4224338211 (UITable_t298892698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITable_Init_m4224338211_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_mInitDone_13((bool)1);
		GameObject_t4012695102 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		UIPanel_t295209936 * L_1 = NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243(NULL /*static, unused*/, L_0, /*hidden argument*/NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243_MethodInfo_var);
		__this->set_mPanel_12(L_1);
		return;
	}
}
// System.Void UITable::LateUpdate()
extern "C"  void UITable_LateUpdate_m1407708642 (UITable_t298892698 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mReposition_14();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		VirtActionInvoker0::Invoke(8 /* System.Void UITable::Reposition() */, __this);
	}

IL_0011:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITable::OnValidate()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UITable_OnValidate_m3957087592_MetadataUsageId;
extern "C"  void UITable_OnValidate_m3957087592 (UITable_t298892698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITable_OnValidate_m3957087592_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_1 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		VirtActionInvoker0::Invoke(8 /* System.Void UITable::Reposition() */, __this);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UITable::RepositionVariableSize(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern Il2CppClass* BoundsU5BU2CU5D_t886857240_il2cpp_TypeInfo_var;
extern Il2CppClass* BoundsU5BU5D_t886857239_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpringPosition_t3802689142_m1260857295_MethodInfo_var;
extern const uint32_t UITable_RepositionVariableSize_m3372084677_MetadataUsageId;
extern "C"  void UITable_RepositionVariableSize_m3372084677 (UITable_t298892698 * __this, List_1_t1081512082 * ___children0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITable_RepositionVariableSize_m3372084677_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	BoundsU5BU2CU5D_t886857240* V_4 = NULL;
	BoundsU5BU5D_t886857239* V_5 = NULL;
	BoundsU5BU5D_t886857239* V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Transform_t284553113 * V_11 = NULL;
	Bounds_t3518514978  V_12;
	memset(&V_12, 0, sizeof(V_12));
	Vector3_t3525329789  V_13;
	memset(&V_13, 0, sizeof(V_13));
	Vector2_t3525329788  V_14;
	memset(&V_14, 0, sizeof(V_14));
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	Transform_t284553113 * V_17 = NULL;
	Bounds_t3518514978  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Bounds_t3518514978  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Bounds_t3518514978  V_20;
	memset(&V_20, 0, sizeof(V_20));
	Vector3_t3525329789  V_21;
	memset(&V_21, 0, sizeof(V_21));
	float V_22 = 0.0f;
	float V_23 = 0.0f;
	Bounds_t3518514978  V_24;
	memset(&V_24, 0, sizeof(V_24));
	Transform_t284553113 * V_25 = NULL;
	int32_t V_26 = 0;
	Transform_t284553113 * V_27 = NULL;
	SpringPosition_t3802689142 * V_28 = NULL;
	Vector3_t3525329789  V_29;
	memset(&V_29, 0, sizeof(V_29));
	Vector3_t3525329789  V_30;
	memset(&V_30, 0, sizeof(V_30));
	Vector3_t3525329789  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector3_t3525329789  V_32;
	memset(&V_32, 0, sizeof(V_32));
	Vector3_t3525329789  V_33;
	memset(&V_33, 0, sizeof(V_33));
	Vector3_t3525329789  V_34;
	memset(&V_34, 0, sizeof(V_34));
	Vector3_t3525329789  V_35;
	memset(&V_35, 0, sizeof(V_35));
	Vector3_t3525329789  V_36;
	memset(&V_36, 0, sizeof(V_36));
	Vector3_t3525329789  V_37;
	memset(&V_37, 0, sizeof(V_37));
	Vector3_t3525329789  V_38;
	memset(&V_38, 0, sizeof(V_38));
	Vector3_t3525329789  V_39;
	memset(&V_39, 0, sizeof(V_39));
	Vector3_t3525329789  V_40;
	memset(&V_40, 0, sizeof(V_40));
	Vector3_t3525329789  V_41;
	memset(&V_41, 0, sizeof(V_41));
	Vector3_t3525329789  V_42;
	memset(&V_42, 0, sizeof(V_42));
	Vector3_t3525329789  V_43;
	memset(&V_43, 0, sizeof(V_43));
	Vector3_t3525329789  V_44;
	memset(&V_44, 0, sizeof(V_44));
	Vector3_t3525329789  V_45;
	memset(&V_45, 0, sizeof(V_45));
	Vector3_t3525329789  V_46;
	memset(&V_46, 0, sizeof(V_46));
	Vector3_t3525329789  V_47;
	memset(&V_47, 0, sizeof(V_47));
	Vector3_t3525329789  V_48;
	memset(&V_48, 0, sizeof(V_48));
	Vector3_t3525329789  V_49;
	memset(&V_49, 0, sizeof(V_49));
	Vector3_t3525329789  V_50;
	memset(&V_50, 0, sizeof(V_50));
	Vector3_t3525329789  V_51;
	memset(&V_51, 0, sizeof(V_51));
	int32_t G_B3_0 = 0;
	int32_t G_B6_0 = 0;
	{
		V_0 = (0.0f);
		V_1 = (0.0f);
		int32_t L_0 = __this->get_columns_2();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		List_1_t1081512082 * L_1 = ___children0;
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_1);
		int32_t L_3 = __this->get_columns_2();
		G_B3_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_2/(int32_t)L_3))+(int32_t)1));
		goto IL_002d;
	}

IL_002c:
	{
		G_B3_0 = 1;
	}

IL_002d:
	{
		V_2 = G_B3_0;
		int32_t L_4 = __this->get_columns_2();
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_5 = __this->get_columns_2();
		G_B6_0 = L_5;
		goto IL_004b;
	}

IL_0045:
	{
		List_1_t1081512082 * L_6 = ___children0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_6);
		G_B6_0 = L_7;
	}

IL_004b:
	{
		V_3 = G_B6_0;
		int32_t L_8 = V_2;
		int32_t L_9 = V_3;
		il2cpp_array_size_t L_11[] = { (il2cpp_array_size_t)L_8, (il2cpp_array_size_t)L_9 };
		BoundsU5BU2CU5D_t886857240* L_10 = (BoundsU5BU2CU5D_t886857240*)GenArrayNew(BoundsU5BU2CU5D_t886857240_il2cpp_TypeInfo_var, L_11);
		V_4 = L_10;
		int32_t L_12 = V_3;
		V_5 = ((BoundsU5BU5D_t886857239*)SZArrayNew(BoundsU5BU5D_t886857239_il2cpp_TypeInfo_var, (uint32_t)L_12));
		int32_t L_13 = V_2;
		V_6 = ((BoundsU5BU5D_t886857239*)SZArrayNew(BoundsU5BU5D_t886857239_il2cpp_TypeInfo_var, (uint32_t)L_13));
		V_7 = 0;
		V_8 = 0;
		V_9 = 0;
		List_1_t1081512082 * L_14 = ___children0;
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_14);
		V_10 = L_15;
		goto IL_0124;
	}

IL_007b:
	{
		List_1_t1081512082 * L_16 = ___children0;
		int32_t L_17 = V_9;
		NullCheck(L_16);
		Transform_t284553113 * L_18 = VirtFuncInvoker1< Transform_t284553113 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, L_16, L_17);
		V_11 = L_18;
		Transform_t284553113 * L_19 = V_11;
		bool L_20 = __this->get_hideInactive_7();
		Bounds_t3518514978  L_21 = NGUIMath_CalculateRelativeWidgetBounds_m1956183518(NULL /*static, unused*/, L_19, (bool)((((int32_t)L_20) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		V_12 = L_21;
		Transform_t284553113 * L_22 = V_11;
		NullCheck(L_22);
		Vector3_t3525329789  L_23 = Transform_get_localScale_m3886572677(L_22, /*hidden argument*/NULL);
		V_13 = L_23;
		Vector3_t3525329789  L_24 = Bounds_get_min_m2329472069((&V_12), /*hidden argument*/NULL);
		Vector3_t3525329789  L_25 = V_13;
		Vector3_t3525329789  L_26 = Vector3_Scale_m3746402337(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		Bounds_set_min_m1913434478((&V_12), L_26, /*hidden argument*/NULL);
		Vector3_t3525329789  L_27 = Bounds_get_max_m2329243351((&V_12), /*hidden argument*/NULL);
		Vector3_t3525329789  L_28 = V_13;
		Vector3_t3525329789  L_29 = Vector3_Scale_m3746402337(NULL /*static, unused*/, L_27, L_28, /*hidden argument*/NULL);
		Bounds_set_max_m1639526812((&V_12), L_29, /*hidden argument*/NULL);
		BoundsU5BU2CU5D_t886857240* L_30 = V_4;
		int32_t L_31 = V_8;
		int32_t L_32 = V_7;
		Bounds_t3518514978  L_33 = V_12;
		NullCheck(L_30);
		(L_30)->SetAt(L_31, L_32, L_33);
		BoundsU5BU5D_t886857239* L_34 = V_5;
		int32_t L_35 = V_7;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		Bounds_t3518514978  L_36 = V_12;
		Bounds_Encapsulate_m2204751131(((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_35))), L_36, /*hidden argument*/NULL);
		BoundsU5BU5D_t886857239* L_37 = V_6;
		int32_t L_38 = V_8;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		Bounds_t3518514978  L_39 = V_12;
		Bounds_Encapsulate_m2204751131(((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_38))), L_39, /*hidden argument*/NULL);
		int32_t L_40 = V_7;
		int32_t L_41 = ((int32_t)((int32_t)L_40+(int32_t)1));
		V_7 = L_41;
		int32_t L_42 = __this->get_columns_2();
		if ((((int32_t)L_41) < ((int32_t)L_42)))
		{
			goto IL_011e;
		}
	}
	{
		int32_t L_43 = __this->get_columns_2();
		if ((((int32_t)L_43) <= ((int32_t)0)))
		{
			goto IL_011e;
		}
	}
	{
		V_7 = 0;
		int32_t L_44 = V_8;
		V_8 = ((int32_t)((int32_t)L_44+(int32_t)1));
	}

IL_011e:
	{
		int32_t L_45 = V_9;
		V_9 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_0124:
	{
		int32_t L_46 = V_9;
		int32_t L_47 = V_10;
		if ((((int32_t)L_46) < ((int32_t)L_47)))
		{
			goto IL_007b;
		}
	}
	{
		V_7 = 0;
		V_8 = 0;
		int32_t L_48 = __this->get_cellAlignment_6();
		Vector2_t3525329788  L_49 = NGUIMath_GetPivotOffset_m2178153133(NULL /*static, unused*/, L_48, /*hidden argument*/NULL);
		V_14 = L_49;
		V_15 = 0;
		List_1_t1081512082 * L_50 = ___children0;
		NullCheck(L_50);
		int32_t L_51 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_50);
		V_16 = L_51;
		goto IL_03ef;
	}

IL_0150:
	{
		List_1_t1081512082 * L_52 = ___children0;
		int32_t L_53 = V_15;
		NullCheck(L_52);
		Transform_t284553113 * L_54 = VirtFuncInvoker1< Transform_t284553113 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, L_52, L_53);
		V_17 = L_54;
		BoundsU5BU2CU5D_t886857240* L_55 = V_4;
		int32_t L_56 = V_8;
		int32_t L_57 = V_7;
		NullCheck(L_55);
		Bounds_t3518514978  L_58 = (L_55)->GetAt(L_56, L_57);
		V_18 = L_58;
		BoundsU5BU5D_t886857239* L_59 = V_5;
		int32_t L_60 = V_7;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		V_19 = (*(Bounds_t3518514978 *)((L_59)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_60))));
		BoundsU5BU5D_t886857239* L_61 = V_6;
		int32_t L_62 = V_8;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, L_62);
		V_20 = (*(Bounds_t3518514978 *)((L_61)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_62))));
		Transform_t284553113 * L_63 = V_17;
		NullCheck(L_63);
		Vector3_t3525329789  L_64 = Transform_get_localPosition_m668140784(L_63, /*hidden argument*/NULL);
		V_21 = L_64;
		float L_65 = V_0;
		Vector3_t3525329789  L_66 = Bounds_get_extents_m2111648188((&V_18), /*hidden argument*/NULL);
		V_30 = L_66;
		float L_67 = (&V_30)->get_x_1();
		Vector3_t3525329789  L_68 = Bounds_get_center_m4084610404((&V_18), /*hidden argument*/NULL);
		V_31 = L_68;
		float L_69 = (&V_31)->get_x_1();
		(&V_21)->set_x_1(((float)((float)((float)((float)L_65+(float)L_67))-(float)L_69)));
		Vector3_t3525329789 * L_70 = (&V_21);
		float L_71 = L_70->get_x_1();
		Vector3_t3525329789  L_72 = Bounds_get_max_m2329243351((&V_18), /*hidden argument*/NULL);
		V_32 = L_72;
		float L_73 = (&V_32)->get_x_1();
		Vector3_t3525329789  L_74 = Bounds_get_min_m2329472069((&V_18), /*hidden argument*/NULL);
		V_33 = L_74;
		float L_75 = (&V_33)->get_x_1();
		Vector3_t3525329789  L_76 = Bounds_get_max_m2329243351((&V_19), /*hidden argument*/NULL);
		V_34 = L_76;
		float L_77 = (&V_34)->get_x_1();
		Vector3_t3525329789  L_78 = Bounds_get_min_m2329472069((&V_19), /*hidden argument*/NULL);
		V_35 = L_78;
		float L_79 = (&V_35)->get_x_1();
		float L_80 = (&V_14)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_81 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)L_73-(float)L_75))-(float)L_77))+(float)L_79)), L_80, /*hidden argument*/NULL);
		Vector2_t3525329788 * L_82 = __this->get_address_of_padding_9();
		float L_83 = L_82->get_x_1();
		L_70->set_x_1(((float)((float)L_71-(float)((float)((float)L_81-(float)L_83)))));
		int32_t L_84 = __this->get_direction_3();
		if (L_84)
		{
			goto IL_02d1;
		}
	}
	{
		float L_85 = V_1;
		Vector3_t3525329789  L_86 = Bounds_get_extents_m2111648188((&V_18), /*hidden argument*/NULL);
		V_36 = L_86;
		float L_87 = (&V_36)->get_y_2();
		Vector3_t3525329789  L_88 = Bounds_get_center_m4084610404((&V_18), /*hidden argument*/NULL);
		V_37 = L_88;
		float L_89 = (&V_37)->get_y_2();
		(&V_21)->set_y_2(((float)((float)((float)((float)((-L_85))-(float)L_87))-(float)L_89)));
		Vector3_t3525329789 * L_90 = (&V_21);
		float L_91 = L_90->get_y_2();
		Vector3_t3525329789  L_92 = Bounds_get_max_m2329243351((&V_18), /*hidden argument*/NULL);
		V_38 = L_92;
		float L_93 = (&V_38)->get_y_2();
		Vector3_t3525329789  L_94 = Bounds_get_min_m2329472069((&V_18), /*hidden argument*/NULL);
		V_39 = L_94;
		float L_95 = (&V_39)->get_y_2();
		Vector3_t3525329789  L_96 = Bounds_get_max_m2329243351((&V_20), /*hidden argument*/NULL);
		V_40 = L_96;
		float L_97 = (&V_40)->get_y_2();
		Vector3_t3525329789  L_98 = Bounds_get_min_m2329472069((&V_20), /*hidden argument*/NULL);
		V_41 = L_98;
		float L_99 = (&V_41)->get_y_2();
		float L_100 = (&V_14)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_101 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, ((float)((float)((float)((float)((float)((float)L_93-(float)L_95))-(float)L_97))+(float)L_99)), (0.0f), L_100, /*hidden argument*/NULL);
		Vector2_t3525329788 * L_102 = __this->get_address_of_padding_9();
		float L_103 = L_102->get_y_2();
		L_90->set_y_2(((float)((float)L_91+(float)((float)((float)L_101-(float)L_103)))));
		goto IL_0369;
	}

IL_02d1:
	{
		float L_104 = V_1;
		Vector3_t3525329789  L_105 = Bounds_get_extents_m2111648188((&V_18), /*hidden argument*/NULL);
		V_42 = L_105;
		float L_106 = (&V_42)->get_y_2();
		Vector3_t3525329789  L_107 = Bounds_get_center_m4084610404((&V_18), /*hidden argument*/NULL);
		V_43 = L_107;
		float L_108 = (&V_43)->get_y_2();
		(&V_21)->set_y_2(((float)((float)((float)((float)L_104+(float)L_106))-(float)L_108)));
		Vector3_t3525329789 * L_109 = (&V_21);
		float L_110 = L_109->get_y_2();
		Vector3_t3525329789  L_111 = Bounds_get_max_m2329243351((&V_18), /*hidden argument*/NULL);
		V_44 = L_111;
		float L_112 = (&V_44)->get_y_2();
		Vector3_t3525329789  L_113 = Bounds_get_min_m2329472069((&V_18), /*hidden argument*/NULL);
		V_45 = L_113;
		float L_114 = (&V_45)->get_y_2();
		Vector3_t3525329789  L_115 = Bounds_get_max_m2329243351((&V_20), /*hidden argument*/NULL);
		V_46 = L_115;
		float L_116 = (&V_46)->get_y_2();
		Vector3_t3525329789  L_117 = Bounds_get_min_m2329472069((&V_20), /*hidden argument*/NULL);
		V_47 = L_117;
		float L_118 = (&V_47)->get_y_2();
		float L_119 = (&V_14)->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_120 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)L_112-(float)L_114))-(float)L_116))+(float)L_118)), L_119, /*hidden argument*/NULL);
		Vector2_t3525329788 * L_121 = __this->get_address_of_padding_9();
		float L_122 = L_121->get_y_2();
		L_109->set_y_2(((float)((float)L_110-(float)((float)((float)L_120-(float)L_122)))));
	}

IL_0369:
	{
		float L_123 = V_0;
		Vector3_t3525329789  L_124 = Bounds_get_size_m3666348432((&V_19), /*hidden argument*/NULL);
		V_48 = L_124;
		float L_125 = (&V_48)->get_x_1();
		Vector2_t3525329788 * L_126 = __this->get_address_of_padding_9();
		float L_127 = L_126->get_x_1();
		V_0 = ((float)((float)L_123+(float)((float)((float)L_125+(float)((float)((float)L_127*(float)(2.0f)))))));
		Transform_t284553113 * L_128 = V_17;
		Vector3_t3525329789  L_129 = V_21;
		NullCheck(L_128);
		Transform_set_localPosition_m3504330903(L_128, L_129, /*hidden argument*/NULL);
		int32_t L_130 = V_7;
		int32_t L_131 = ((int32_t)((int32_t)L_130+(int32_t)1));
		V_7 = L_131;
		int32_t L_132 = __this->get_columns_2();
		if ((((int32_t)L_131) < ((int32_t)L_132)))
		{
			goto IL_03e9;
		}
	}
	{
		int32_t L_133 = __this->get_columns_2();
		if ((((int32_t)L_133) <= ((int32_t)0)))
		{
			goto IL_03e9;
		}
	}
	{
		V_7 = 0;
		int32_t L_134 = V_8;
		V_8 = ((int32_t)((int32_t)L_134+(int32_t)1));
		V_0 = (0.0f);
		float L_135 = V_1;
		Vector3_t3525329789  L_136 = Bounds_get_size_m3666348432((&V_20), /*hidden argument*/NULL);
		V_49 = L_136;
		float L_137 = (&V_49)->get_y_2();
		Vector2_t3525329788 * L_138 = __this->get_address_of_padding_9();
		float L_139 = L_138->get_y_2();
		V_1 = ((float)((float)L_135+(float)((float)((float)L_137+(float)((float)((float)L_139*(float)(2.0f)))))));
	}

IL_03e9:
	{
		int32_t L_140 = V_15;
		V_15 = ((int32_t)((int32_t)L_140+(int32_t)1));
	}

IL_03ef:
	{
		int32_t L_141 = V_15;
		int32_t L_142 = V_16;
		if ((((int32_t)L_141) < ((int32_t)L_142)))
		{
			goto IL_0150;
		}
	}
	{
		int32_t L_143 = __this->get_pivot_5();
		if (!L_143)
		{
			goto IL_050a;
		}
	}
	{
		int32_t L_144 = __this->get_pivot_5();
		Vector2_t3525329788  L_145 = NGUIMath_GetPivotOffset_m2178153133(NULL /*static, unused*/, L_144, /*hidden argument*/NULL);
		V_14 = L_145;
		Transform_t284553113 * L_146 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Bounds_t3518514978  L_147 = NGUIMath_CalculateRelativeWidgetBounds_m3101556671(NULL /*static, unused*/, L_146, /*hidden argument*/NULL);
		V_24 = L_147;
		Vector3_t3525329789  L_148 = Bounds_get_size_m3666348432((&V_24), /*hidden argument*/NULL);
		V_50 = L_148;
		float L_149 = (&V_50)->get_x_1();
		float L_150 = (&V_14)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_151 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, (0.0f), L_149, L_150, /*hidden argument*/NULL);
		V_22 = L_151;
		Vector3_t3525329789  L_152 = Bounds_get_size_m3666348432((&V_24), /*hidden argument*/NULL);
		V_51 = L_152;
		float L_153 = (&V_51)->get_y_2();
		float L_154 = (&V_14)->get_y_2();
		float L_155 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, ((-L_153)), (0.0f), L_154, /*hidden argument*/NULL);
		V_23 = L_155;
		Transform_t284553113 * L_156 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		V_25 = L_156;
		V_26 = 0;
		goto IL_04fc;
	}

IL_0474:
	{
		Transform_t284553113 * L_157 = V_25;
		int32_t L_158 = V_26;
		NullCheck(L_157);
		Transform_t284553113 * L_159 = Transform_GetChild_m4040462992(L_157, L_158, /*hidden argument*/NULL);
		V_27 = L_159;
		Transform_t284553113 * L_160 = V_27;
		NullCheck(L_160);
		SpringPosition_t3802689142 * L_161 = Component_GetComponent_TisSpringPosition_t3802689142_m1260857295(L_160, /*hidden argument*/Component_GetComponent_TisSpringPosition_t3802689142_m1260857295_MethodInfo_var);
		V_28 = L_161;
		SpringPosition_t3802689142 * L_162 = V_28;
		bool L_163 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_162, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_163)
		{
			goto IL_04c4;
		}
	}
	{
		SpringPosition_t3802689142 * L_164 = V_28;
		NullCheck(L_164);
		Vector3_t3525329789 * L_165 = L_164->get_address_of_target_3();
		Vector3_t3525329789 * L_166 = L_165;
		float L_167 = L_166->get_x_1();
		float L_168 = V_22;
		L_166->set_x_1(((float)((float)L_167-(float)L_168)));
		SpringPosition_t3802689142 * L_169 = V_28;
		NullCheck(L_169);
		Vector3_t3525329789 * L_170 = L_169->get_address_of_target_3();
		Vector3_t3525329789 * L_171 = L_170;
		float L_172 = L_171->get_y_2();
		float L_173 = V_23;
		L_171->set_y_2(((float)((float)L_172-(float)L_173)));
		goto IL_04f6;
	}

IL_04c4:
	{
		Transform_t284553113 * L_174 = V_27;
		NullCheck(L_174);
		Vector3_t3525329789  L_175 = Transform_get_localPosition_m668140784(L_174, /*hidden argument*/NULL);
		V_29 = L_175;
		Vector3_t3525329789 * L_176 = (&V_29);
		float L_177 = L_176->get_x_1();
		float L_178 = V_22;
		L_176->set_x_1(((float)((float)L_177-(float)L_178)));
		Vector3_t3525329789 * L_179 = (&V_29);
		float L_180 = L_179->get_y_2();
		float L_181 = V_23;
		L_179->set_y_2(((float)((float)L_180-(float)L_181)));
		Transform_t284553113 * L_182 = V_27;
		Vector3_t3525329789  L_183 = V_29;
		NullCheck(L_182);
		Transform_set_localPosition_m3504330903(L_182, L_183, /*hidden argument*/NULL);
	}

IL_04f6:
	{
		int32_t L_184 = V_26;
		V_26 = ((int32_t)((int32_t)L_184+(int32_t)1));
	}

IL_04fc:
	{
		int32_t L_185 = V_26;
		Transform_t284553113 * L_186 = V_25;
		NullCheck(L_186);
		int32_t L_187 = Transform_get_childCount_m2107810675(L_186, /*hidden argument*/NULL);
		if ((((int32_t)L_185) < ((int32_t)L_187)))
		{
			goto IL_0474;
		}
	}

IL_050a:
	{
		return;
	}
}
// System.Void UITable::Reposition()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIScrollView_t2113479878_m1783064831_MethodInfo_var;
extern const uint32_t UITable_Reposition_m2768170991_MetadataUsageId;
extern "C"  void UITable_Reposition_m2768170991 (UITable_t298892698 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITable_Reposition_m2768170991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t284553113 * V_0 = NULL;
	List_1_t1081512082 * V_1 = NULL;
	UIScrollView_t2113479878 * V_2 = NULL;
	{
		bool L_0 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		bool L_1 = __this->get_mInitDone_13();
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_2 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void UITable::Init() */, __this);
	}

IL_0026:
	{
		__this->set_mReposition_14((bool)0);
		Transform_t284553113 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		V_0 = L_3;
		List_1_t1081512082 * L_4 = UITable_GetChildList_m1703713889(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		List_1_t1081512082 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_5);
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_004e;
		}
	}
	{
		List_1_t1081512082 * L_7 = V_1;
		UITable_RepositionVariableSize_m3372084677(__this, L_7, /*hidden argument*/NULL);
	}

IL_004e:
	{
		bool L_8 = __this->get_keepWithinPanel_8();
		if (!L_8)
		{
			goto IL_0097;
		}
	}
	{
		UIPanel_t295209936 * L_9 = __this->get_mPanel_12();
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0097;
		}
	}
	{
		UIPanel_t295209936 * L_11 = __this->get_mPanel_12();
		Transform_t284553113 * L_12 = V_0;
		NullCheck(L_11);
		UIPanel_ConstrainTargetToBounds_m4168072101(L_11, L_12, (bool)1, /*hidden argument*/NULL);
		UIPanel_t295209936 * L_13 = __this->get_mPanel_12();
		NullCheck(L_13);
		UIScrollView_t2113479878 * L_14 = Component_GetComponent_TisUIScrollView_t2113479878_m1783064831(L_13, /*hidden argument*/Component_GetComponent_TisUIScrollView_t2113479878_m1783064831_MethodInfo_var);
		V_2 = L_14;
		UIScrollView_t2113479878 * L_15 = V_2;
		bool L_16 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_15, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0097;
		}
	}
	{
		UIScrollView_t2113479878 * L_17 = V_2;
		NullCheck(L_17);
		VirtActionInvoker1< bool >::Invoke(8 /* System.Void UIScrollView::UpdateScrollbars(System.Boolean) */, L_17, (bool)1);
	}

IL_0097:
	{
		OnReposition_t1160738748 * L_18 = __this->get_onReposition_10();
		if (!L_18)
		{
			goto IL_00ad;
		}
	}
	{
		OnReposition_t1160738748 * L_19 = __this->get_onReposition_10();
		NullCheck(L_19);
		OnReposition_Invoke_m926525905(L_19, /*hidden argument*/NULL);
	}

IL_00ad:
	{
		return;
	}
}
// System.Void UITable/OnReposition::.ctor(System.Object,System.IntPtr)
extern "C"  void OnReposition__ctor_m270370487 (OnReposition_t1160738748 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UITable/OnReposition::Invoke()
extern "C"  void OnReposition_Invoke_m926525905 (OnReposition_t1160738748 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnReposition_Invoke_m926525905((OnReposition_t1160738748 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnReposition_t1160738748(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UITable/OnReposition::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnReposition_BeginInvoke_m2330278930 (OnReposition_t1160738748 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UITable/OnReposition::EndInvoke(System.IAsyncResult)
extern "C"  void OnReposition_EndInvoke_m2813783111 (OnReposition_t1160738748 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UITextList::.ctor()
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern const uint32_t UITextList__ctor_m1801479516_MetadataUsageId;
extern "C"  void UITextList__ctor_m1801479516 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList__ctor_m1801479516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_paragraphHistory_5(((int32_t)100));
		CharU5BU5D_t3416858730* L_0 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)10));
		__this->set_mSeparator_6(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITextList::.cctor()
extern Il2CppClass* Dictionary_2_t2792955402_il2cpp_TypeInfo_var;
extern Il2CppClass* UITextList_t736798239_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3310307025_MethodInfo_var;
extern const uint32_t UITextList__cctor_m3824161233_MetadataUsageId;
extern "C"  void UITextList__cctor_m3824161233 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList__cctor_m3824161233_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t2792955402 * L_0 = (Dictionary_2_t2792955402 *)il2cpp_codegen_object_new(Dictionary_2_t2792955402_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3310307025(L_0, /*hidden argument*/Dictionary_2__ctor_m3310307025_MethodInfo_var);
		((UITextList_t736798239_StaticFields*)UITextList_t736798239_il2cpp_TypeInfo_var->static_fields)->set_mHistory_12(L_0);
		return;
	}
}
// BetterList`1<UITextList/Paragraph> UITextList::get_paragraphs()
extern Il2CppClass* UITextList_t736798239_il2cpp_TypeInfo_var;
extern Il2CppClass* BetterList_1_t1155257498_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1__ctor_m878122552_MethodInfo_var;
extern const uint32_t UITextList_get_paragraphs_m3421034864_MetadataUsageId;
extern "C"  BetterList_1_t1155257498 * UITextList_get_paragraphs_m3421034864 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList_get_paragraphs_m3421034864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BetterList_1_t1155257498 * L_0 = __this->get_mParagraphs_11();
		if (L_0)
		{
			goto IL_0047;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UITextList_t736798239_il2cpp_TypeInfo_var);
		Dictionary_2_t2792955402 * L_1 = ((UITextList_t736798239_StaticFields*)UITextList_t736798239_il2cpp_TypeInfo_var->static_fields)->get_mHistory_12();
		String_t* L_2 = Object_get_name_m3709440845(__this, /*hidden argument*/NULL);
		BetterList_1_t1155257498 ** L_3 = __this->get_address_of_mParagraphs_11();
		NullCheck(L_1);
		bool L_4 = VirtFuncInvoker2< bool, String_t*, BetterList_1_t1155257498 ** >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,BetterList`1<UITextList/Paragraph>>::TryGetValue(!0,!1&) */, L_1, L_2, L_3);
		if (L_4)
		{
			goto IL_0047;
		}
	}
	{
		BetterList_1_t1155257498 * L_5 = (BetterList_1_t1155257498 *)il2cpp_codegen_object_new(BetterList_1_t1155257498_il2cpp_TypeInfo_var);
		BetterList_1__ctor_m878122552(L_5, /*hidden argument*/BetterList_1__ctor_m878122552_MethodInfo_var);
		__this->set_mParagraphs_11(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(UITextList_t736798239_il2cpp_TypeInfo_var);
		Dictionary_2_t2792955402 * L_6 = ((UITextList_t736798239_StaticFields*)UITextList_t736798239_il2cpp_TypeInfo_var->static_fields)->get_mHistory_12();
		String_t* L_7 = Object_get_name_m3709440845(__this, /*hidden argument*/NULL);
		BetterList_1_t1155257498 * L_8 = __this->get_mParagraphs_11();
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, BetterList_1_t1155257498 * >::Invoke(26 /* System.Void System.Collections.Generic.Dictionary`2<System.String,BetterList`1<UITextList/Paragraph>>::Add(!0,!1) */, L_6, L_7, L_8);
	}

IL_0047:
	{
		BetterList_1_t1155257498 * L_9 = __this->get_mParagraphs_11();
		return L_9;
	}
}
// System.Boolean UITextList::get_isValid()
extern "C"  bool UITextList_get_isValid_m4155293295 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UILabel_t291504320 * L_0 = __this->get_textLabel_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		UILabel_t291504320 * L_2 = __this->get_textLabel_2();
		NullCheck(L_2);
		Object_t3878351788 * L_3 = UILabel_get_ambigiousFont_m2019334862(L_2, /*hidden argument*/NULL);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_4));
		goto IL_0025;
	}

IL_0024:
	{
		G_B3_0 = 0;
	}

IL_0025:
	{
		return (bool)G_B3_0;
	}
}
// System.Single UITextList::get_scrollValue()
extern "C"  float UITextList_get_scrollValue_m578722441 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_mScroll_7();
		return L_0;
	}
}
// System.Void UITextList::set_scrollValue(System.Single)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UITextList_set_scrollValue_m4133436266_MetadataUsageId;
extern "C"  void UITextList_set_scrollValue_m4133436266 (UITextList_t736798239 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList_set_scrollValue_m4133436266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___value0 = L_1;
		bool L_2 = UITextList_get_isValid_m4155293295(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		float L_3 = __this->get_mScroll_7();
		float L_4 = ___value0;
		if ((((float)L_3) == ((float)L_4)))
		{
			goto IL_004e;
		}
	}
	{
		UIProgressBar_t168062834 * L_5 = __this->get_scrollBar_3();
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		UIProgressBar_t168062834 * L_7 = __this->get_scrollBar_3();
		float L_8 = ___value0;
		NullCheck(L_7);
		UIProgressBar_set_value_m1993536704(L_7, L_8, /*hidden argument*/NULL);
		goto IL_004e;
	}

IL_0041:
	{
		float L_9 = ___value0;
		__this->set_mScroll_7(L_9);
		UITextList_UpdateVisibleText_m1816301488(__this, /*hidden argument*/NULL);
	}

IL_004e:
	{
		return;
	}
}
// System.Single UITextList::get_lineHeight()
extern "C"  float UITextList_get_lineHeight_m2659975192 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		UILabel_t291504320 * L_0 = __this->get_textLabel_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		UILabel_t291504320 * L_2 = __this->get_textLabel_2();
		NullCheck(L_2);
		int32_t L_3 = UILabel_get_fontSize_m718383300(L_2, /*hidden argument*/NULL);
		UILabel_t291504320 * L_4 = __this->get_textLabel_2();
		NullCheck(L_4);
		float L_5 = UILabel_get_effectiveSpacingY_m4081174089(L_4, /*hidden argument*/NULL);
		G_B3_0 = ((float)((float)(((float)((float)L_3)))+(float)L_5));
		goto IL_0033;
	}

IL_002e:
	{
		G_B3_0 = (20.0f);
	}

IL_0033:
	{
		return G_B3_0;
	}
}
// System.Int32 UITextList::get_scrollHeight()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UITextList_get_scrollHeight_m685030579_MetadataUsageId;
extern "C"  int32_t UITextList_get_scrollHeight_m685030579 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList_get_scrollHeight_m685030579_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = UITextList_get_isValid_m4155293295(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		UILabel_t291504320 * L_1 = __this->get_textLabel_2();
		NullCheck(L_1);
		int32_t L_2 = UIWidget_get_height_m1023454943(L_1, /*hidden argument*/NULL);
		float L_3 = UITextList_get_lineHeight_m2659975192(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_4 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)(((float)((float)L_2)))/(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = __this->get_mTotalLines_8();
		int32_t L_6 = V_0;
		int32_t L_7 = Mathf_Max_m2911193737(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_5-(int32_t)L_6)), /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UITextList::Clear()
extern const MethodInfo* BetterList_1_Clear_m2579223139_MethodInfo_var;
extern const uint32_t UITextList_Clear_m3502580103_MetadataUsageId;
extern "C"  void UITextList_Clear_m3502580103 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList_Clear_m3502580103_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BetterList_1_t1155257498 * L_0 = UITextList_get_paragraphs_m3421034864(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		BetterList_1_Clear_m2579223139(L_0, /*hidden argument*/BetterList_1_Clear_m2579223139_MethodInfo_var);
		UITextList_UpdateVisibleText_m1816301488(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITextList::Start()
extern Il2CppClass* Callback_t4187391077_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisUILabel_t291504320_m2847097109_MethodInfo_var;
extern const MethodInfo* UITextList_OnScrollBar_m233289953_MethodInfo_var;
extern const uint32_t UITextList_Start_m748617308_MetadataUsageId;
extern "C"  void UITextList_Start_m748617308 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList_Start_m748617308_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UILabel_t291504320 * L_0 = __this->get_textLabel_2();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		UILabel_t291504320 * L_2 = Component_GetComponentInChildren_TisUILabel_t291504320_m2847097109(__this, /*hidden argument*/Component_GetComponentInChildren_TisUILabel_t291504320_m2847097109_MethodInfo_var);
		__this->set_textLabel_2(L_2);
	}

IL_001d:
	{
		UIProgressBar_t168062834 * L_3 = __this->get_scrollBar_3();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		UIProgressBar_t168062834 * L_5 = __this->get_scrollBar_3();
		NullCheck(L_5);
		List_1_t506415896 * L_6 = L_5->get_onChange_15();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)UITextList_OnScrollBar_m233289953_MethodInfo_var);
		Callback_t4187391077 * L_8 = (Callback_t4187391077 *)il2cpp_codegen_object_new(Callback_t4187391077_il2cpp_TypeInfo_var);
		Callback__ctor_m3018475132(L_8, __this, L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1811262575(NULL /*static, unused*/, L_6, L_8, /*hidden argument*/NULL);
	}

IL_004b:
	{
		UILabel_t291504320 * L_9 = __this->get_textLabel_2();
		NullCheck(L_9);
		UILabel_set_overflowMethod_m349526440(L_9, 1, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_style_4();
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_007f;
		}
	}
	{
		UILabel_t291504320 * L_11 = __this->get_textLabel_2();
		NullCheck(L_11);
		UIWidget_set_pivot_m2063605531(L_11, 6, /*hidden argument*/NULL);
		UITextList_set_scrollValue_m4133436266(__this, (1.0f), /*hidden argument*/NULL);
		goto IL_0096;
	}

IL_007f:
	{
		UILabel_t291504320 * L_12 = __this->get_textLabel_2();
		NullCheck(L_12);
		UIWidget_set_pivot_m2063605531(L_12, 0, /*hidden argument*/NULL);
		UITextList_set_scrollValue_m4133436266(__this, (0.0f), /*hidden argument*/NULL);
	}

IL_0096:
	{
		return;
	}
}
// System.Void UITextList::Update()
extern "C"  void UITextList_Update_m1738152241 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	{
		bool L_0 = UITextList_get_isValid_m4155293295(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003d;
		}
	}
	{
		UILabel_t291504320 * L_1 = __this->get_textLabel_2();
		NullCheck(L_1);
		int32_t L_2 = UIWidget_get_width_m293857264(L_1, /*hidden argument*/NULL);
		int32_t L_3 = __this->get_mLastWidth_9();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0037;
		}
	}
	{
		UILabel_t291504320 * L_4 = __this->get_textLabel_2();
		NullCheck(L_4);
		int32_t L_5 = UIWidget_get_height_m1023454943(L_4, /*hidden argument*/NULL);
		int32_t L_6 = __this->get_mLastHeight_10();
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_003d;
		}
	}

IL_0037:
	{
		UITextList_Rebuild_m267146645(__this, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return;
	}
}
// System.Void UITextList::OnScroll(System.Single)
extern "C"  void UITextList_OnScroll_m1316852407 (UITextList_t736798239 * __this, float ___val0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = UITextList_get_scrollHeight_m685030579(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		float L_2 = ___val0;
		float L_3 = UITextList_get_lineHeight_m2659975192(__this, /*hidden argument*/NULL);
		___val0 = ((float)((float)L_2*(float)L_3));
		float L_4 = __this->get_mScroll_7();
		float L_5 = ___val0;
		int32_t L_6 = V_0;
		UITextList_set_scrollValue_m4133436266(__this, ((float)((float)L_4-(float)((float)((float)L_5/(float)(((float)((float)L_6))))))), /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UITextList::OnDrag(UnityEngine.Vector2)
extern "C"  void UITextList_OnDrag_m2302371391 (UITextList_t736798239 * __this, Vector2_t3525329788  ___delta0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = UITextList_get_scrollHeight_m685030579(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		float L_2 = (&___delta0)->get_y_2();
		float L_3 = UITextList_get_lineHeight_m2659975192(__this, /*hidden argument*/NULL);
		V_1 = ((float)((float)L_2/(float)L_3));
		float L_4 = __this->get_mScroll_7();
		float L_5 = V_1;
		int32_t L_6 = V_0;
		UITextList_set_scrollValue_m4133436266(__this, ((float)((float)L_4+(float)((float)((float)L_5/(float)(((float)((float)L_6))))))), /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void UITextList::OnScrollBar()
extern Il2CppClass* UIProgressBar_t168062834_il2cpp_TypeInfo_var;
extern const uint32_t UITextList_OnScrollBar_m233289953_MetadataUsageId;
extern "C"  void UITextList_OnScrollBar_m233289953 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList_OnScrollBar_m233289953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIProgressBar_t168062834 * L_0 = ((UIProgressBar_t168062834_StaticFields*)UIProgressBar_t168062834_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		NullCheck(L_0);
		float L_1 = UIProgressBar_get_value_m1994834827(L_0, /*hidden argument*/NULL);
		__this->set_mScroll_7(L_1);
		UITextList_UpdateVisibleText_m1816301488(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITextList::Add(System.String)
extern "C"  void UITextList_Add_m1668405703 (UITextList_t736798239 * __this, String_t* ___text0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___text0;
		UITextList_Add_m891605718(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITextList::Add(System.String,System.Boolean)
extern Il2CppClass* Paragraph_t3953256782_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1_get_Item_m3297328632_MethodInfo_var;
extern const MethodInfo* BetterList_1_RemoveAt_m4267811188_MethodInfo_var;
extern const MethodInfo* BetterList_1_Add_m988300103_MethodInfo_var;
extern const uint32_t UITextList_Add_m891605718_MetadataUsageId;
extern "C"  void UITextList_Add_m891605718 (UITextList_t736798239 * __this, String_t* ___text0, bool ___updateVisible1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList_Add_m891605718_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Paragraph_t3953256782 * V_0 = NULL;
	{
		V_0 = (Paragraph_t3953256782 *)NULL;
		BetterList_1_t1155257498 * L_0 = UITextList_get_paragraphs_m3421034864(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_size_1();
		int32_t L_2 = __this->get_paragraphHistory_5();
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0023;
		}
	}
	{
		Paragraph_t3953256782 * L_3 = (Paragraph_t3953256782 *)il2cpp_codegen_object_new(Paragraph_t3953256782_il2cpp_TypeInfo_var);
		Paragraph__ctor_m2662697725(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_003c;
	}

IL_0023:
	{
		BetterList_1_t1155257498 * L_4 = __this->get_mParagraphs_11();
		NullCheck(L_4);
		Paragraph_t3953256782 * L_5 = BetterList_1_get_Item_m3297328632(L_4, 0, /*hidden argument*/BetterList_1_get_Item_m3297328632_MethodInfo_var);
		V_0 = L_5;
		BetterList_1_t1155257498 * L_6 = __this->get_mParagraphs_11();
		NullCheck(L_6);
		BetterList_1_RemoveAt_m4267811188(L_6, 0, /*hidden argument*/BetterList_1_RemoveAt_m4267811188_MethodInfo_var);
	}

IL_003c:
	{
		Paragraph_t3953256782 * L_7 = V_0;
		String_t* L_8 = ___text0;
		NullCheck(L_7);
		L_7->set_text_0(L_8);
		BetterList_1_t1155257498 * L_9 = __this->get_mParagraphs_11();
		Paragraph_t3953256782 * L_10 = V_0;
		NullCheck(L_9);
		BetterList_1_Add_m988300103(L_9, L_10, /*hidden argument*/BetterList_1_Add_m988300103_MethodInfo_var);
		UITextList_Rebuild_m267146645(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITextList::Rebuild()
extern Il2CppClass* NGUIText_t3886970074_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t3416858730_il2cpp_TypeInfo_var;
extern Il2CppClass* UIScrollBar_t2839103954_il2cpp_TypeInfo_var;
extern const uint32_t UITextList_Rebuild_m267146645_MetadataUsageId;
extern "C"  void UITextList_Rebuild_m267146645 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList_Rebuild_m267146645_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	String_t* V_1 = NULL;
	Paragraph_t3953256782 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	UIScrollBar_t2839103954 * V_5 = NULL;
	UIScrollBar_t2839103954 * G_B11_0 = NULL;
	UIScrollBar_t2839103954 * G_B10_0 = NULL;
	float G_B12_0 = 0.0f;
	UIScrollBar_t2839103954 * G_B12_1 = NULL;
	{
		bool L_0 = UITextList_get_isValid_m4155293295(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0164;
		}
	}
	{
		UILabel_t291504320 * L_1 = __this->get_textLabel_2();
		NullCheck(L_1);
		int32_t L_2 = UIWidget_get_width_m293857264(L_1, /*hidden argument*/NULL);
		__this->set_mLastWidth_9(L_2);
		UILabel_t291504320 * L_3 = __this->get_textLabel_2();
		NullCheck(L_3);
		int32_t L_4 = UIWidget_get_height_m1023454943(L_3, /*hidden argument*/NULL);
		__this->set_mLastHeight_10(L_4);
		UILabel_t291504320 * L_5 = __this->get_textLabel_2();
		NullCheck(L_5);
		UILabel_UpdateNGUIText_m3757111068(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUIText_t3886970074_il2cpp_TypeInfo_var);
		((NGUIText_t3886970074_StaticFields*)NGUIText_t3886970074_il2cpp_TypeInfo_var->static_fields)->set_rectHeight_10(((int32_t)1000000));
		((NGUIText_t3886970074_StaticFields*)NGUIText_t3886970074_il2cpp_TypeInfo_var->static_fields)->set_regionHeight_12(((int32_t)1000000));
		__this->set_mTotalLines_8(0);
		V_0 = 0;
		goto IL_00a9;
	}

IL_005a:
	{
		BetterList_1_t1155257498 * L_6 = __this->get_mParagraphs_11();
		NullCheck(L_6);
		ParagraphU5BU5D_t1733423547* L_7 = L_6->get_buffer_0();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		V_2 = ((L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9)));
		Paragraph_t3953256782 * L_10 = V_2;
		NullCheck(L_10);
		String_t* L_11 = L_10->get_text_0();
		IL2CPP_RUNTIME_CLASS_INIT(NGUIText_t3886970074_il2cpp_TypeInfo_var);
		NGUIText_WrapText_m2782068513(NULL /*static, unused*/, L_11, (&V_1), (bool)0, (bool)1, (bool)0, /*hidden argument*/NULL);
		Paragraph_t3953256782 * L_12 = V_2;
		String_t* L_13 = V_1;
		CharU5BU5D_t3416858730* L_14 = ((CharU5BU5D_t3416858730*)SZArrayNew(CharU5BU5D_t3416858730_il2cpp_TypeInfo_var, (uint32_t)1));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint16_t)((int32_t)10));
		NullCheck(L_13);
		StringU5BU5D_t2956870243* L_15 = String_Split_m290179486(L_13, L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		L_12->set_lines_1(L_15);
		int32_t L_16 = __this->get_mTotalLines_8();
		Paragraph_t3953256782 * L_17 = V_2;
		NullCheck(L_17);
		StringU5BU5D_t2956870243* L_18 = L_17->get_lines_1();
		NullCheck(L_18);
		__this->set_mTotalLines_8(((int32_t)((int32_t)L_16+(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_18)->max_length)))))));
		int32_t L_19 = V_0;
		V_0 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_00a9:
	{
		int32_t L_20 = V_0;
		BetterList_1_t1155257498 * L_21 = UITextList_get_paragraphs_m3421034864(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = L_21->get_size_1();
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_005a;
		}
	}
	{
		__this->set_mTotalLines_8(0);
		V_3 = 0;
		BetterList_1_t1155257498 * L_23 = __this->get_mParagraphs_11();
		NullCheck(L_23);
		int32_t L_24 = L_23->get_size_1();
		V_4 = L_24;
		goto IL_00fa;
	}

IL_00d5:
	{
		int32_t L_25 = __this->get_mTotalLines_8();
		BetterList_1_t1155257498 * L_26 = __this->get_mParagraphs_11();
		NullCheck(L_26);
		ParagraphU5BU5D_t1733423547* L_27 = L_26->get_buffer_0();
		int32_t L_28 = V_3;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		int32_t L_29 = L_28;
		NullCheck(((L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29))));
		StringU5BU5D_t2956870243* L_30 = ((L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_29)))->get_lines_1();
		NullCheck(L_30);
		__this->set_mTotalLines_8(((int32_t)((int32_t)L_25+(int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_30)->max_length)))))));
		int32_t L_31 = V_3;
		V_3 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_00fa:
	{
		int32_t L_32 = V_3;
		int32_t L_33 = V_4;
		if ((((int32_t)L_32) < ((int32_t)L_33)))
		{
			goto IL_00d5;
		}
	}
	{
		UIProgressBar_t168062834 * L_34 = __this->get_scrollBar_3();
		bool L_35 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_34, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_015e;
		}
	}
	{
		UIProgressBar_t168062834 * L_36 = __this->get_scrollBar_3();
		V_5 = ((UIScrollBar_t2839103954 *)IsInstClass(L_36, UIScrollBar_t2839103954_il2cpp_TypeInfo_var));
		UIScrollBar_t2839103954 * L_37 = V_5;
		bool L_38 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_37, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_015e;
		}
	}
	{
		UIScrollBar_t2839103954 * L_39 = V_5;
		int32_t L_40 = __this->get_mTotalLines_8();
		G_B10_0 = L_39;
		if (L_40)
		{
			G_B11_0 = L_39;
			goto IL_0144;
		}
	}
	{
		G_B12_0 = (1.0f);
		G_B12_1 = G_B10_0;
		goto IL_0159;
	}

IL_0144:
	{
		int32_t L_41 = UITextList_get_scrollHeight_m685030579(__this, /*hidden argument*/NULL);
		int32_t L_42 = __this->get_mTotalLines_8();
		G_B12_0 = ((float)((float)(1.0f)-(float)((float)((float)(((float)((float)L_41)))/(float)(((float)((float)L_42)))))));
		G_B12_1 = G_B11_0;
	}

IL_0159:
	{
		NullCheck(G_B12_1);
		UIScrollBar_set_barSize_m645706685(G_B12_1, G_B12_0, /*hidden argument*/NULL);
	}

IL_015e:
	{
		UITextList_UpdateVisibleText_m1816301488(__this, /*hidden argument*/NULL);
	}

IL_0164:
	{
		return;
	}
}
// System.Void UITextList::UpdateVisibleText()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* StringBuilder_t3822575854_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral10;
extern const uint32_t UITextList_UpdateVisibleText_m1816301488_MetadataUsageId;
extern "C"  void UITextList_UpdateVisibleText_m1816301488 (UITextList_t736798239 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITextList_UpdateVisibleText_m1816301488_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	StringBuilder_t3822575854 * V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	Paragraph_t3953256782 * V_6 = NULL;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	String_t* V_9 = NULL;
	{
		bool L_0 = UITextList_get_isValid_m4155293295(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_1 = __this->get_mTotalLines_8();
		if (L_1)
		{
			goto IL_0027;
		}
	}
	{
		UILabel_t291504320 * L_2 = __this->get_textLabel_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_2);
		UILabel_set_text_m4037075551(L_2, L_3, /*hidden argument*/NULL);
		return;
	}

IL_0027:
	{
		UILabel_t291504320 * L_4 = __this->get_textLabel_2();
		NullCheck(L_4);
		int32_t L_5 = UIWidget_get_height_m1023454943(L_4, /*hidden argument*/NULL);
		float L_6 = UITextList_get_lineHeight_m2659975192(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)(((float)((float)L_5)))/(float)L_6)), /*hidden argument*/NULL);
		V_0 = L_7;
		int32_t L_8 = __this->get_mTotalLines_8();
		int32_t L_9 = V_0;
		int32_t L_10 = Mathf_Max_m2911193737(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_8-(int32_t)L_9)), /*hidden argument*/NULL);
		V_1 = L_10;
		float L_11 = __this->get_mScroll_7();
		int32_t L_12 = V_1;
		int32_t L_13 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_11*(float)(((float)((float)L_12))))), /*hidden argument*/NULL);
		V_2 = L_13;
		int32_t L_14 = V_2;
		if ((((int32_t)L_14) >= ((int32_t)0)))
		{
			goto IL_0067;
		}
	}
	{
		V_2 = 0;
	}

IL_0067:
	{
		StringBuilder_t3822575854 * L_15 = (StringBuilder_t3822575854 *)il2cpp_codegen_object_new(StringBuilder_t3822575854_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m135953004(L_15, /*hidden argument*/NULL);
		V_3 = L_15;
		V_4 = 0;
		BetterList_1_t1155257498 * L_16 = UITextList_get_paragraphs_m3421034864(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = L_16->get_size_1();
		V_5 = L_17;
		goto IL_0102;
	}

IL_0082:
	{
		BetterList_1_t1155257498 * L_18 = __this->get_mParagraphs_11();
		NullCheck(L_18);
		ParagraphU5BU5D_t1733423547* L_19 = L_18->get_buffer_0();
		int32_t L_20 = V_4;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		V_6 = ((L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21)));
		V_7 = 0;
		Paragraph_t3953256782 * L_22 = V_6;
		NullCheck(L_22);
		StringU5BU5D_t2956870243* L_23 = L_22->get_lines_1();
		NullCheck(L_23);
		V_8 = (((int32_t)((int32_t)(((Il2CppArray *)L_23)->max_length))));
		goto IL_00ec;
	}

IL_00a5:
	{
		Paragraph_t3953256782 * L_24 = V_6;
		NullCheck(L_24);
		StringU5BU5D_t2956870243* L_25 = L_24->get_lines_1();
		int32_t L_26 = V_7;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = L_26;
		V_9 = ((L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27)));
		int32_t L_28 = V_2;
		if ((((int32_t)L_28) <= ((int32_t)0)))
		{
			goto IL_00c1;
		}
	}
	{
		int32_t L_29 = V_2;
		V_2 = ((int32_t)((int32_t)L_29-(int32_t)1));
		goto IL_00e6;
	}

IL_00c1:
	{
		StringBuilder_t3822575854 * L_30 = V_3;
		NullCheck(L_30);
		int32_t L_31 = StringBuilder_get_Length_m2443133099(L_30, /*hidden argument*/NULL);
		if ((((int32_t)L_31) <= ((int32_t)0)))
		{
			goto IL_00d9;
		}
	}
	{
		StringBuilder_t3822575854 * L_32 = V_3;
		NullCheck(L_32);
		StringBuilder_Append_m3898090075(L_32, _stringLiteral10, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		StringBuilder_t3822575854 * L_33 = V_3;
		String_t* L_34 = V_9;
		NullCheck(L_33);
		StringBuilder_Append_m3898090075(L_33, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_0;
		V_0 = ((int32_t)((int32_t)L_35-(int32_t)1));
	}

IL_00e6:
	{
		int32_t L_36 = V_7;
		V_7 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_00ec:
	{
		int32_t L_37 = V_0;
		if ((((int32_t)L_37) <= ((int32_t)0)))
		{
			goto IL_00fc;
		}
	}
	{
		int32_t L_38 = V_7;
		int32_t L_39 = V_8;
		if ((((int32_t)L_38) < ((int32_t)L_39)))
		{
			goto IL_00a5;
		}
	}

IL_00fc:
	{
		int32_t L_40 = V_4;
		V_4 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_0102:
	{
		int32_t L_41 = V_0;
		if ((((int32_t)L_41) <= ((int32_t)0)))
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_42 = V_4;
		int32_t L_43 = V_5;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_0082;
		}
	}

IL_0112:
	{
		UILabel_t291504320 * L_44 = __this->get_textLabel_2();
		StringBuilder_t3822575854 * L_45 = V_3;
		NullCheck(L_45);
		String_t* L_46 = StringBuilder_ToString_m350379841(L_45, /*hidden argument*/NULL);
		NullCheck(L_44);
		UILabel_set_text_m4037075551(L_44, L_46, /*hidden argument*/NULL);
	}

IL_0123:
	{
		return;
	}
}
// System.Void UITextList/Paragraph::.ctor()
extern "C"  void Paragraph__ctor_m2662697725 (Paragraph_t3953256782 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITexture::.ctor()
extern Il2CppClass* UIBasicSprite_t2501337439_il2cpp_TypeInfo_var;
extern const uint32_t UITexture__ctor_m1606448484_MetadataUsageId;
extern "C"  void UITexture__ctor_m1606448484 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITexture__ctor_m1606448484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t1525428817  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Rect__ctor_m3291325233(&L_0, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_mRect_69(L_0);
		Vector4_t3525329790  L_1 = Vector4_get_zero_m3835647092(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mBorder_73(L_1);
		__this->set_mPMA_75((-1));
		IL2CPP_RUNTIME_CLASS_INIT(UIBasicSprite_t2501337439_il2cpp_TypeInfo_var);
		UIBasicSprite__ctor_m2527140588(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Texture UITexture::get_mainTexture()
extern "C"  Texture_t1769722184 * UITexture_get_mainTexture_m3400120682 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	{
		Texture_t1769722184 * L_0 = __this->get_mTexture_70();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		Texture_t1769722184 * L_2 = __this->get_mTexture_70();
		return L_2;
	}

IL_0018:
	{
		Material_t1886596500 * L_3 = __this->get_mMat_71();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		Material_t1886596500 * L_5 = __this->get_mMat_71();
		NullCheck(L_5);
		Texture_t1769722184 * L_6 = Material_get_mainTexture_m1012267054(L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0035:
	{
		return (Texture_t1769722184 *)NULL;
	}
}
// System.Void UITexture::set_mainTexture(UnityEngine.Texture)
extern "C"  void UITexture_set_mainTexture_m4209454887 (UITexture_t3903132647 * __this, Texture_t1769722184 * ___value0, const MethodInfo* method)
{
	{
		Texture_t1769722184 * L_0 = __this->get_mTexture_70();
		Texture_t1769722184 * L_1 = ___value0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0076;
		}
	}
	{
		UIDrawCall_t913273974 * L_3 = ((UIWidget_t769069560 *)__this)->get_drawCall_46();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005c;
		}
	}
	{
		UIDrawCall_t913273974 * L_5 = ((UIWidget_t769069560 *)__this)->get_drawCall_46();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_widgetCount_5();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_005c;
		}
	}
	{
		Material_t1886596500 * L_7 = __this->get_mMat_71();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005c;
		}
	}
	{
		Texture_t1769722184 * L_9 = ___value0;
		__this->set_mTexture_70(L_9);
		UIDrawCall_t913273974 * L_10 = ((UIWidget_t769069560 *)__this)->get_drawCall_46();
		Texture_t1769722184 * L_11 = ___value0;
		NullCheck(L_10);
		UIDrawCall_set_mainTexture_m1626748742(L_10, L_11, /*hidden argument*/NULL);
		goto IL_0076;
	}

IL_005c:
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		Texture_t1769722184 * L_12 = ___value0;
		__this->set_mTexture_70(L_12);
		__this->set_mPMA_75((-1));
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_0076:
	{
		return;
	}
}
// UnityEngine.Material UITexture::get_material()
extern "C"  Material_t1886596500 * UITexture_get_material_m1341168497 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	{
		Material_t1886596500 * L_0 = __this->get_mMat_71();
		return L_0;
	}
}
// System.Void UITexture::set_material(UnityEngine.Material)
extern "C"  void UITexture_set_material_m2912196742 (UITexture_t3903132647 * __this, Material_t1886596500 * ___value0, const MethodInfo* method)
{
	{
		Material_t1886596500 * L_0 = __this->get_mMat_71();
		Material_t1886596500 * L_1 = ___value0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		__this->set_mShader_72((Shader_t3998140498 *)NULL);
		Material_t1886596500 * L_3 = ___value0;
		__this->set_mMat_71(L_3);
		__this->set_mPMA_75((-1));
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_0032:
	{
		return;
	}
}
// UnityEngine.Shader UITexture::get_shader()
extern Il2CppCodeGenString* _stringLiteral3699808227;
extern const uint32_t UITexture_get_shader_m3259374125_MetadataUsageId;
extern "C"  Shader_t3998140498 * UITexture_get_shader_m3259374125 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITexture_get_shader_m3259374125_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t1886596500 * L_0 = __this->get_mMat_71();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Material_t1886596500 * L_2 = __this->get_mMat_71();
		NullCheck(L_2);
		Shader_t3998140498 * L_3 = Material_get_shader_m2881845503(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001d:
	{
		Shader_t3998140498 * L_4 = __this->get_mShader_72();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003e;
		}
	}
	{
		Shader_t3998140498 * L_6 = Shader_Find_m4048047578(NULL /*static, unused*/, _stringLiteral3699808227, /*hidden argument*/NULL);
		__this->set_mShader_72(L_6);
	}

IL_003e:
	{
		Shader_t3998140498 * L_7 = __this->get_mShader_72();
		return L_7;
	}
}
// System.Void UITexture::set_shader(UnityEngine.Shader)
extern "C"  void UITexture_set_shader_m265018694 (UITexture_t3903132647 * __this, Shader_t3998140498 * ___value0, const MethodInfo* method)
{
	{
		Shader_t3998140498 * L_0 = __this->get_mShader_72();
		Shader_t3998140498 * L_1 = ___value0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_007d;
		}
	}
	{
		UIDrawCall_t913273974 * L_3 = ((UIWidget_t769069560 *)__this)->get_drawCall_46();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005c;
		}
	}
	{
		UIDrawCall_t913273974 * L_5 = ((UIWidget_t769069560 *)__this)->get_drawCall_46();
		NullCheck(L_5);
		int32_t L_6 = L_5->get_widgetCount_5();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_005c;
		}
	}
	{
		Material_t1886596500 * L_7 = __this->get_mMat_71();
		bool L_8 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005c;
		}
	}
	{
		Shader_t3998140498 * L_9 = ___value0;
		__this->set_mShader_72(L_9);
		UIDrawCall_t913273974 * L_10 = ((UIWidget_t769069560 *)__this)->get_drawCall_46();
		Shader_t3998140498 * L_11 = ___value0;
		NullCheck(L_10);
		UIDrawCall_set_shader_m1330587813(L_10, L_11, /*hidden argument*/NULL);
		goto IL_007d;
	}

IL_005c:
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		Shader_t3998140498 * L_12 = ___value0;
		__this->set_mShader_72(L_12);
		__this->set_mPMA_75((-1));
		__this->set_mMat_71((Material_t1886596500 *)NULL);
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_007d:
	{
		return;
	}
}
// System.Boolean UITexture::get_premultipliedAlpha()
extern Il2CppCodeGenString* _stringLiteral4289704054;
extern const uint32_t UITexture_get_premultipliedAlpha_m1638491133_MetadataUsageId;
extern "C"  bool UITexture_get_premultipliedAlpha_m1638491133 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITexture_get_premultipliedAlpha_m1638491133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Material_t1886596500 * V_0 = NULL;
	UITexture_t3903132647 * G_B5_0 = NULL;
	UITexture_t3903132647 * G_B2_0 = NULL;
	UITexture_t3903132647 * G_B3_0 = NULL;
	UITexture_t3903132647 * G_B4_0 = NULL;
	int32_t G_B6_0 = 0;
	UITexture_t3903132647 * G_B6_1 = NULL;
	{
		int32_t L_0 = __this->get_mPMA_75();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0057;
		}
	}
	{
		Material_t1886596500 * L_1 = VirtFuncInvoker0< Material_t1886596500 * >::Invoke(25 /* UnityEngine.Material UITexture::get_material() */, __this);
		V_0 = L_1;
		Material_t1886596500 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		G_B2_0 = __this;
		if (!L_3)
		{
			G_B5_0 = __this;
			goto IL_0051;
		}
	}
	{
		Material_t1886596500 * L_4 = V_0;
		NullCheck(L_4);
		Shader_t3998140498 * L_5 = Material_get_shader_m2881845503(L_4, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		G_B3_0 = G_B2_0;
		if (!L_6)
		{
			G_B5_0 = G_B2_0;
			goto IL_0051;
		}
	}
	{
		Material_t1886596500 * L_7 = V_0;
		NullCheck(L_7);
		Shader_t3998140498 * L_8 = Material_get_shader_m2881845503(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		String_t* L_9 = Object_get_name_m3709440845(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		bool L_10 = String_Contains_m3032019141(L_9, _stringLiteral4289704054, /*hidden argument*/NULL);
		G_B4_0 = G_B3_0;
		if (!L_10)
		{
			G_B5_0 = G_B3_0;
			goto IL_0051;
		}
	}
	{
		G_B6_0 = 1;
		G_B6_1 = G_B4_0;
		goto IL_0052;
	}

IL_0051:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
	}

IL_0052:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_mPMA_75(G_B6_0);
	}

IL_0057:
	{
		int32_t L_11 = __this->get_mPMA_75();
		return (bool)((((int32_t)L_11) == ((int32_t)1))? 1 : 0);
	}
}
// UnityEngine.Vector4 UITexture::get_border()
extern "C"  Vector4_t3525329790  UITexture_get_border_m2382663228 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	{
		Vector4_t3525329790  L_0 = __this->get_mBorder_73();
		return L_0;
	}
}
// System.Void UITexture::set_border(UnityEngine.Vector4)
extern "C"  void UITexture_set_border_m2360646895 (UITexture_t3903132647 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	{
		Vector4_t3525329790  L_0 = __this->get_mBorder_73();
		Vector4_t3525329790  L_1 = ___value0;
		bool L_2 = Vector4_op_Inequality_m3118756897(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Vector4_t3525329790  L_3 = ___value0;
		__this->set_mBorder_73(L_3);
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_001e:
	{
		return;
	}
}
// UnityEngine.Rect UITexture::get_uvRect()
extern "C"  Rect_t1525428817  UITexture_get_uvRect_m2135926764 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	{
		Rect_t1525428817  L_0 = __this->get_mRect_69();
		return L_0;
	}
}
// System.Void UITexture::set_uvRect(UnityEngine.Rect)
extern "C"  void UITexture_set_uvRect_m3913517031 (UITexture_t3903132647 * __this, Rect_t1525428817  ___value0, const MethodInfo* method)
{
	{
		Rect_t1525428817  L_0 = __this->get_mRect_69();
		Rect_t1525428817  L_1 = ___value0;
		bool L_2 = Rect_op_Inequality_m2236552616(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Rect_t1525428817  L_3 = ___value0;
		__this->set_mRect_69(L_3);
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_001e:
	{
		return;
	}
}
// UnityEngine.Vector4 UITexture::get_drawingDimensions()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UITexture_get_drawingDimensions_m1970525_MetadataUsageId;
extern "C"  Vector4_t3525329790  UITexture_get_drawingDimensions_m1970525 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITexture_get_drawingDimensions_m1970525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	float V_12 = 0.0f;
	Vector4_t3525329790  V_13;
	memset(&V_13, 0, sizeof(V_13));
	float V_14 = 0.0f;
	float V_15 = 0.0f;
	float V_16 = 0.0f;
	float V_17 = 0.0f;
	{
		Vector2_t3525329788  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = ((UIWidget_t769069560 *)__this)->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = ((UIWidget_t769069560 *)__this)->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = ((UIWidget_t769069560 *)__this)->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = ((UIWidget_t769069560 *)__this)->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Texture_t1769722184 * L_9 = __this->get_mTexture_70();
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0154;
		}
	}
	{
		int32_t L_11 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if ((((int32_t)L_11) == ((int32_t)2)))
		{
			goto IL_0154;
		}
	}
	{
		Texture_t1769722184 * L_12 = __this->get_mTexture_70();
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_12);
		V_5 = L_13;
		Texture_t1769722184 * L_14 = __this->get_mTexture_70();
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_14);
		V_6 = L_15;
		V_7 = 0;
		V_8 = 0;
		V_9 = (1.0f);
		V_10 = (1.0f);
		int32_t L_16 = V_5;
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_00f4;
		}
	}
	{
		int32_t L_17 = V_6;
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_00f4;
		}
	}
	{
		int32_t L_18 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if (!L_18)
		{
			goto IL_00b0;
		}
	}
	{
		int32_t L_19 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if ((!(((uint32_t)L_19) == ((uint32_t)3))))
		{
			goto IL_00f4;
		}
	}

IL_00b0:
	{
		int32_t L_20 = V_5;
		if (!((int32_t)((int32_t)L_20&(int32_t)1)))
		{
			goto IL_00bf;
		}
	}
	{
		int32_t L_21 = V_7;
		V_7 = ((int32_t)((int32_t)L_21+(int32_t)1));
	}

IL_00bf:
	{
		int32_t L_22 = V_6;
		if (!((int32_t)((int32_t)L_22&(int32_t)1)))
		{
			goto IL_00ce;
		}
	}
	{
		int32_t L_23 = V_8;
		V_8 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00ce:
	{
		int32_t L_24 = V_5;
		int32_t L_25 = ((UIWidget_t769069560 *)__this)->get_mWidth_24();
		V_9 = ((float)((float)((float)((float)(1.0f)/(float)(((float)((float)L_24)))))*(float)(((float)((float)L_25)))));
		int32_t L_26 = V_6;
		int32_t L_27 = ((UIWidget_t769069560 *)__this)->get_mHeight_25();
		V_10 = ((float)((float)((float)((float)(1.0f)/(float)(((float)((float)L_26)))))*(float)(((float)((float)L_27)))));
	}

IL_00f4:
	{
		int32_t L_28 = ((UIBasicSprite_t2501337439 *)__this)->get_mFlip_56();
		if ((((int32_t)L_28) == ((int32_t)1)))
		{
			goto IL_010c;
		}
	}
	{
		int32_t L_29 = ((UIBasicSprite_t2501337439 *)__this)->get_mFlip_56();
		if ((!(((uint32_t)L_29) == ((uint32_t)3))))
		{
			goto IL_011a;
		}
	}

IL_010c:
	{
		float L_30 = V_1;
		int32_t L_31 = V_7;
		float L_32 = V_9;
		V_1 = ((float)((float)L_30+(float)((float)((float)(((float)((float)L_31)))*(float)L_32))));
		goto IL_0123;
	}

IL_011a:
	{
		float L_33 = V_3;
		int32_t L_34 = V_7;
		float L_35 = V_9;
		V_3 = ((float)((float)L_33-(float)((float)((float)(((float)((float)L_34)))*(float)L_35))));
	}

IL_0123:
	{
		int32_t L_36 = ((UIBasicSprite_t2501337439 *)__this)->get_mFlip_56();
		if ((((int32_t)L_36) == ((int32_t)2)))
		{
			goto IL_013b;
		}
	}
	{
		int32_t L_37 = ((UIBasicSprite_t2501337439 *)__this)->get_mFlip_56();
		if ((!(((uint32_t)L_37) == ((uint32_t)3))))
		{
			goto IL_0149;
		}
	}

IL_013b:
	{
		float L_38 = V_2;
		int32_t L_39 = V_8;
		float L_40 = V_10;
		V_2 = ((float)((float)L_38+(float)((float)((float)(((float)((float)L_39)))*(float)L_40))));
		goto IL_0154;
	}

IL_0149:
	{
		float L_41 = V_4;
		int32_t L_42 = V_8;
		float L_43 = V_10;
		V_4 = ((float)((float)L_41-(float)((float)((float)(((float)((float)L_42)))*(float)L_43))));
	}

IL_0154:
	{
		bool L_44 = __this->get_mFixedAspect_74();
		if (!L_44)
		{
			goto IL_0172;
		}
	}
	{
		V_11 = (0.0f);
		V_12 = (0.0f);
		goto IL_019c;
	}

IL_0172:
	{
		Vector4_t3525329790  L_45 = VirtFuncInvoker0< Vector4_t3525329790  >::Invoke(36 /* UnityEngine.Vector4 UITexture::get_border() */, __this);
		V_13 = L_45;
		float L_46 = (&V_13)->get_x_1();
		float L_47 = (&V_13)->get_z_3();
		V_11 = ((float)((float)L_46+(float)L_47));
		float L_48 = (&V_13)->get_y_2();
		float L_49 = (&V_13)->get_w_4();
		V_12 = ((float)((float)L_48+(float)L_49));
	}

IL_019c:
	{
		float L_50 = V_1;
		float L_51 = V_3;
		float L_52 = V_11;
		Vector4_t3525329790 * L_53 = ((UIWidget_t769069560 *)__this)->get_address_of_mDrawRegion_39();
		float L_54 = L_53->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_55 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_50, ((float)((float)L_51-(float)L_52)), L_54, /*hidden argument*/NULL);
		V_14 = L_55;
		float L_56 = V_2;
		float L_57 = V_4;
		float L_58 = V_12;
		Vector4_t3525329790 * L_59 = ((UIWidget_t769069560 *)__this)->get_address_of_mDrawRegion_39();
		float L_60 = L_59->get_y_2();
		float L_61 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_56, ((float)((float)L_57-(float)L_58)), L_60, /*hidden argument*/NULL);
		V_15 = L_61;
		float L_62 = V_1;
		float L_63 = V_11;
		float L_64 = V_3;
		Vector4_t3525329790 * L_65 = ((UIWidget_t769069560 *)__this)->get_address_of_mDrawRegion_39();
		float L_66 = L_65->get_z_3();
		float L_67 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, ((float)((float)L_62+(float)L_63)), L_64, L_66, /*hidden argument*/NULL);
		V_16 = L_67;
		float L_68 = V_2;
		float L_69 = V_12;
		float L_70 = V_4;
		Vector4_t3525329790 * L_71 = ((UIWidget_t769069560 *)__this)->get_address_of_mDrawRegion_39();
		float L_72 = L_71->get_w_4();
		float L_73 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, ((float)((float)L_68+(float)L_69)), L_70, L_72, /*hidden argument*/NULL);
		V_17 = L_73;
		float L_74 = V_14;
		float L_75 = V_15;
		float L_76 = V_16;
		float L_77 = V_17;
		Vector4_t3525329790  L_78;
		memset(&L_78, 0, sizeof(L_78));
		Vector4__ctor_m2441427762(&L_78, L_74, L_75, L_76, L_77, /*hidden argument*/NULL);
		return L_78;
	}
}
// System.Boolean UITexture::get_fixedAspect()
extern "C"  bool UITexture_get_fixedAspect_m1097060057 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mFixedAspect_74();
		return L_0;
	}
}
// System.Void UITexture::set_fixedAspect(System.Boolean)
extern "C"  void UITexture_set_fixedAspect_m3934661096 (UITexture_t3903132647 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mFixedAspect_74();
		bool L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0038;
		}
	}
	{
		bool L_2 = ___value0;
		__this->set_mFixedAspect_74(L_2);
		Vector4_t3525329790  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m2441427762(&L_3, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((UIWidget_t769069560 *)__this)->set_mDrawRegion_39(L_3);
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_0038:
	{
		return;
	}
}
// System.Void UITexture::MakePixelPerfect()
extern "C"  void UITexture_MakePixelPerfect_m2252403541 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	Texture_t1769722184 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		UIWidget_MakePixelPerfect_m1839593398(__this, /*hidden argument*/NULL);
		int32_t L_0 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if ((!(((uint32_t)L_0) == ((uint32_t)2))))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}

IL_0013:
	{
		Texture_t1769722184 * L_1 = VirtFuncInvoker0< Texture_t1769722184 * >::Invoke(27 /* UnityEngine.Texture UITexture::get_mainTexture() */, __this);
		V_0 = L_1;
		Texture_t1769722184 * L_2 = V_0;
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		return;
	}

IL_0027:
	{
		int32_t L_4 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if (!L_4)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_5 = ((UIBasicSprite_t2501337439 *)__this)->get_mType_52();
		if ((((int32_t)L_5) == ((int32_t)3)))
		{
			goto IL_0049;
		}
	}
	{
		bool L_6 = UIBasicSprite_get_hasBorder_m529620315(__this, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_008b;
		}
	}

IL_0049:
	{
		Texture_t1769722184 * L_7 = V_0;
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_008b;
		}
	}
	{
		Texture_t1769722184 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_9);
		V_1 = L_10;
		Texture_t1769722184 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_11);
		V_2 = L_12;
		int32_t L_13 = V_1;
		if ((!(((uint32_t)((int32_t)((int32_t)L_13&(int32_t)1))) == ((uint32_t)1))))
		{
			goto IL_0070;
		}
	}
	{
		int32_t L_14 = V_1;
		V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0070:
	{
		int32_t L_15 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_15&(int32_t)1))) == ((uint32_t)1))))
		{
			goto IL_007d;
		}
	}
	{
		int32_t L_16 = V_2;
		V_2 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_007d:
	{
		int32_t L_17 = V_1;
		UIWidget_set_width_m3480390811(__this, L_17, /*hidden argument*/NULL);
		int32_t L_18 = V_2;
		UIWidget_set_height_m1838784918(__this, L_18, /*hidden argument*/NULL);
	}

IL_008b:
	{
		return;
	}
}
// System.Void UITexture::OnUpdate()
extern "C"  void UITexture_OnUpdate_m638189928 (UITexture_t3903132647 * __this, const MethodInfo* method)
{
	Texture_t1769722184 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	{
		UIWidget_OnUpdate_m3427137225(__this, /*hidden argument*/NULL);
		bool L_0 = __this->get_mFixedAspect_74();
		if (!L_0)
		{
			goto IL_00d9;
		}
	}
	{
		Texture_t1769722184 * L_1 = VirtFuncInvoker0< Texture_t1769722184 * >::Invoke(27 /* UnityEngine.Texture UITexture::get_mainTexture() */, __this);
		V_0 = L_1;
		Texture_t1769722184 * L_2 = V_0;
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00d9;
		}
	}
	{
		Texture_t1769722184 * L_4 = V_0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_4);
		V_1 = L_5;
		Texture_t1769722184 * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_6);
		V_2 = L_7;
		int32_t L_8 = V_1;
		if ((!(((uint32_t)((int32_t)((int32_t)L_8&(int32_t)1))) == ((uint32_t)1))))
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_2;
		if ((!(((uint32_t)((int32_t)((int32_t)L_10&(int32_t)1))) == ((uint32_t)1))))
		{
			goto IL_004c;
		}
	}
	{
		int32_t L_11 = V_2;
		V_2 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004c:
	{
		int32_t L_12 = ((UIWidget_t769069560 *)__this)->get_mWidth_24();
		V_3 = (((float)((float)L_12)));
		int32_t L_13 = ((UIWidget_t769069560 *)__this)->get_mHeight_25();
		V_4 = (((float)((float)L_13)));
		float L_14 = V_3;
		float L_15 = V_4;
		V_5 = ((float)((float)L_14/(float)L_15));
		int32_t L_16 = V_1;
		int32_t L_17 = V_2;
		V_6 = ((float)((float)(((float)((float)L_16)))/(float)(((float)((float)L_17)))));
		float L_18 = V_6;
		float L_19 = V_5;
		if ((!(((float)L_18) < ((float)L_19))))
		{
			goto IL_00a8;
		}
	}
	{
		float L_20 = V_3;
		float L_21 = V_4;
		float L_22 = V_6;
		float L_23 = V_3;
		V_7 = ((float)((float)((float)((float)((float)((float)L_20-(float)((float)((float)L_21*(float)L_22))))/(float)L_23))*(float)(0.5f)));
		float L_24 = V_7;
		float L_25 = V_7;
		Vector4_t3525329790  L_26;
		memset(&L_26, 0, sizeof(L_26));
		Vector4__ctor_m2441427762(&L_26, L_24, (0.0f), ((float)((float)(1.0f)-(float)L_25)), (1.0f), /*hidden argument*/NULL);
		UIWidget_set_drawRegion_m231796482(__this, L_26, /*hidden argument*/NULL);
		goto IL_00d9;
	}

IL_00a8:
	{
		float L_27 = V_4;
		float L_28 = V_3;
		float L_29 = V_6;
		float L_30 = V_4;
		V_8 = ((float)((float)((float)((float)((float)((float)L_27-(float)((float)((float)L_28/(float)L_29))))/(float)L_30))*(float)(0.5f)));
		float L_31 = V_8;
		float L_32 = V_8;
		Vector4_t3525329790  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector4__ctor_m2441427762(&L_33, (0.0f), L_31, (1.0f), ((float)((float)(1.0f)-(float)L_32)), /*hidden argument*/NULL);
		UIWidget_set_drawRegion_m231796482(__this, L_33, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		return;
	}
}
// System.Void UITexture::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color>)
extern "C"  void UITexture_OnFill_m436096724 (UITexture_t3903132647 * __this, BetterList_1_t727330505 * ___verts0, BetterList_1_t727330504 * ___uvs1, BetterList_1_t3085143772 * ___cols2, const MethodInfo* method)
{
	Texture_t1769722184 * V_0 = NULL;
	Rect_t1525428817  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t1525428817  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t3525329790  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	{
		Texture_t1769722184 * L_0 = VirtFuncInvoker0< Texture_t1769722184 * >::Invoke(27 /* UnityEngine.Texture UITexture::get_mainTexture() */, __this);
		V_0 = L_0;
		Texture_t1769722184 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}

IL_0014:
	{
		Rect_t1525428817 * L_3 = __this->get_address_of_mRect_69();
		float L_4 = Rect_get_x_m982385354(L_3, /*hidden argument*/NULL);
		Texture_t1769722184 * L_5 = V_0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_5);
		Rect_t1525428817 * L_7 = __this->get_address_of_mRect_69();
		float L_8 = Rect_get_y_m982386315(L_7, /*hidden argument*/NULL);
		Texture_t1769722184 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_9);
		Texture_t1769722184 * L_11 = V_0;
		NullCheck(L_11);
		int32_t L_12 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_11);
		Rect_t1525428817 * L_13 = __this->get_address_of_mRect_69();
		float L_14 = Rect_get_width_m2824209432(L_13, /*hidden argument*/NULL);
		Texture_t1769722184 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_15);
		Rect_t1525428817 * L_17 = __this->get_address_of_mRect_69();
		float L_18 = Rect_get_height_m2154960823(L_17, /*hidden argument*/NULL);
		Rect__ctor_m3291325233((&V_1), ((float)((float)L_4*(float)(((float)((float)L_6))))), ((float)((float)L_8*(float)(((float)((float)L_10))))), ((float)((float)(((float)((float)L_12)))*(float)L_14)), ((float)((float)(((float)((float)L_16)))*(float)L_18)), /*hidden argument*/NULL);
		Rect_t1525428817  L_19 = V_1;
		V_2 = L_19;
		Vector4_t3525329790  L_20 = VirtFuncInvoker0< Vector4_t3525329790  >::Invoke(36 /* UnityEngine.Vector4 UITexture::get_border() */, __this);
		V_3 = L_20;
		Rect_t1525428817 * L_21 = (&V_2);
		float L_22 = Rect_get_xMin_m371109962(L_21, /*hidden argument*/NULL);
		float L_23 = (&V_3)->get_x_1();
		Rect_set_xMin_m265803321(L_21, ((float)((float)L_22+(float)L_23)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_24 = (&V_2);
		float L_25 = Rect_get_yMin_m399739113(L_24, /*hidden argument*/NULL);
		float L_26 = (&V_3)->get_y_2();
		Rect_set_yMin_m3716298746(L_24, ((float)((float)L_25+(float)L_26)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_27 = (&V_2);
		float L_28 = Rect_get_xMax_m370881244(L_27, /*hidden argument*/NULL);
		float L_29 = (&V_3)->get_z_3();
		Rect_set_xMax_m1513853159(L_27, ((float)((float)L_28-(float)L_29)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_30 = (&V_2);
		float L_31 = Rect_get_yMax_m399510395(L_30, /*hidden argument*/NULL);
		float L_32 = (&V_3)->get_w_4();
		Rect_set_yMax_m669381288(L_30, ((float)((float)L_31-(float)L_32)), /*hidden argument*/NULL);
		Texture_t1769722184 * L_33 = V_0;
		NullCheck(L_33);
		int32_t L_34 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_33);
		V_4 = ((float)((float)(1.0f)/(float)(((float)((float)L_34)))));
		Texture_t1769722184 * L_35 = V_0;
		NullCheck(L_35);
		int32_t L_36 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_35);
		V_5 = ((float)((float)(1.0f)/(float)(((float)((float)L_36)))));
		Rect_t1525428817 * L_37 = (&V_1);
		float L_38 = Rect_get_xMin_m371109962(L_37, /*hidden argument*/NULL);
		float L_39 = V_4;
		Rect_set_xMin_m265803321(L_37, ((float)((float)L_38*(float)L_39)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_40 = (&V_1);
		float L_41 = Rect_get_xMax_m370881244(L_40, /*hidden argument*/NULL);
		float L_42 = V_4;
		Rect_set_xMax_m1513853159(L_40, ((float)((float)L_41*(float)L_42)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_43 = (&V_1);
		float L_44 = Rect_get_yMin_m399739113(L_43, /*hidden argument*/NULL);
		float L_45 = V_5;
		Rect_set_yMin_m3716298746(L_43, ((float)((float)L_44*(float)L_45)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_46 = (&V_1);
		float L_47 = Rect_get_yMax_m399510395(L_46, /*hidden argument*/NULL);
		float L_48 = V_5;
		Rect_set_yMax_m669381288(L_46, ((float)((float)L_47*(float)L_48)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_49 = (&V_2);
		float L_50 = Rect_get_xMin_m371109962(L_49, /*hidden argument*/NULL);
		float L_51 = V_4;
		Rect_set_xMin_m265803321(L_49, ((float)((float)L_50*(float)L_51)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_52 = (&V_2);
		float L_53 = Rect_get_xMax_m370881244(L_52, /*hidden argument*/NULL);
		float L_54 = V_4;
		Rect_set_xMax_m1513853159(L_52, ((float)((float)L_53*(float)L_54)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_55 = (&V_2);
		float L_56 = Rect_get_yMin_m399739113(L_55, /*hidden argument*/NULL);
		float L_57 = V_5;
		Rect_set_yMin_m3716298746(L_55, ((float)((float)L_56*(float)L_57)), /*hidden argument*/NULL);
		Rect_t1525428817 * L_58 = (&V_2);
		float L_59 = Rect_get_yMax_m399510395(L_58, /*hidden argument*/NULL);
		float L_60 = V_5;
		Rect_set_yMax_m669381288(L_58, ((float)((float)L_59*(float)L_60)), /*hidden argument*/NULL);
		BetterList_1_t727330505 * L_61 = ___verts0;
		NullCheck(L_61);
		int32_t L_62 = L_61->get_size_1();
		V_6 = L_62;
		BetterList_1_t727330505 * L_63 = ___verts0;
		BetterList_1_t727330504 * L_64 = ___uvs1;
		BetterList_1_t3085143772 * L_65 = ___cols2;
		Rect_t1525428817  L_66 = V_1;
		Rect_t1525428817  L_67 = V_2;
		UIBasicSprite_Fill_m1415708251(__this, L_63, L_64, L_65, L_66, L_67, /*hidden argument*/NULL);
		OnPostFillCallback_t294433735 * L_68 = ((UIWidget_t769069560 *)__this)->get_onPostFill_28();
		if (!L_68)
		{
			goto IL_0191;
		}
	}
	{
		OnPostFillCallback_t294433735 * L_69 = ((UIWidget_t769069560 *)__this)->get_onPostFill_28();
		int32_t L_70 = V_6;
		BetterList_1_t727330505 * L_71 = ___verts0;
		BetterList_1_t727330504 * L_72 = ___uvs1;
		BetterList_1_t3085143772 * L_73 = ___cols2;
		NullCheck(L_69);
		OnPostFillCallback_Invoke_m1064019312(L_69, __this, L_70, L_71, L_72, L_73, /*hidden argument*/NULL);
	}

IL_0191:
	{
		return;
	}
}
// System.Void UIToggle::.ctor()
extern Il2CppClass* List_1_t506415896_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1287572350_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3107443730;
extern const uint32_t UIToggle__ctor_m3478074515_MetadataUsageId;
extern "C"  void UIToggle__ctor_m3478074515 (UIToggle_t688812808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggle__ctor_m3478074515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t506415896 * L_0 = (List_1_t506415896 *)il2cpp_codegen_object_new(List_1_t506415896_il2cpp_TypeInfo_var);
		List_1__ctor_m1287572350(L_0, /*hidden argument*/List_1__ctor_m1287572350_MethodInfo_var);
		__this->set_onChange_13(L_0);
		__this->set_functionName_18(_stringLiteral3107443730);
		__this->set_mIsActive_20((bool)1);
		UIWidgetContainer__ctor_m2037457442(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIToggle::.cctor()
extern Il2CppClass* BetterList_1_t2185780820_il2cpp_TypeInfo_var;
extern Il2CppClass* UIToggle_t688812808_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1__ctor_m4055276258_MethodInfo_var;
extern const uint32_t UIToggle__cctor_m4258998650_MetadataUsageId;
extern "C"  void UIToggle__cctor_m4258998650 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggle__cctor_m4258998650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		BetterList_1_t2185780820 * L_0 = (BetterList_1_t2185780820 *)il2cpp_codegen_object_new(BetterList_1_t2185780820_il2cpp_TypeInfo_var);
		BetterList_1__ctor_m4055276258(L_0, /*hidden argument*/BetterList_1__ctor_m4055276258_MethodInfo_var);
		((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->set_list_2(L_0);
		return;
	}
}
// System.Boolean UIToggle::get_value()
extern "C"  bool UIToggle_get_value_m2236783845 (UIToggle_t688812808 * __this, const MethodInfo* method)
{
	bool G_B3_0 = false;
	{
		bool L_0 = __this->get_mStarted_21();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_mIsActive_20();
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		bool L_2 = __this->get_startsActive_10();
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Void UIToggle::set_value(System.Boolean)
extern "C"  void UIToggle_set_value_m116851932 (UIToggle_t688812808 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mStarted_21();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1 = ___value0;
		__this->set_startsActive_10(L_1);
		goto IL_0046;
	}

IL_0017:
	{
		int32_t L_2 = __this->get_group_4();
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		bool L_3 = ___value0;
		if (L_3)
		{
			goto IL_003e;
		}
	}
	{
		bool L_4 = __this->get_optionCanBeNone_12();
		if (L_4)
		{
			goto IL_003e;
		}
	}
	{
		bool L_5 = __this->get_mStarted_21();
		if (L_5)
		{
			goto IL_0046;
		}
	}

IL_003e:
	{
		bool L_6 = ___value0;
		UIToggle_Set_m3475184755(__this, L_6, (bool)1, /*hidden argument*/NULL);
	}

IL_0046:
	{
		return;
	}
}
// System.Boolean UIToggle::get_isColliderEnabled()
extern const MethodInfo* Component_GetComponent_TisCollider_t955670625_m3246438266_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider2D_t1890038195_m1670690088_MethodInfo_var;
extern const uint32_t UIToggle_get_isColliderEnabled_m1749226647_MetadataUsageId;
extern "C"  bool UIToggle_get_isColliderEnabled_m1749226647 (UIToggle_t688812808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_get_isColliderEnabled_m1749226647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Collider_t955670625 * V_0 = NULL;
	Collider2D_t1890038195 * V_1 = NULL;
	int32_t G_B5_0 = 0;
	{
		Collider_t955670625 * L_0 = Component_GetComponent_TisCollider_t955670625_m3246438266(__this, /*hidden argument*/Component_GetComponent_TisCollider_t955670625_m3246438266_MethodInfo_var);
		V_0 = L_0;
		Collider_t955670625 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Collider_t955670625 * L_3 = V_0;
		NullCheck(L_3);
		bool L_4 = Collider_get_enabled_m4167357801(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t1890038195 * L_5 = Component_GetComponent_TisCollider2D_t1890038195_m1670690088(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_t1890038195_m1670690088_MethodInfo_var);
		V_1 = L_5;
		Collider2D_t1890038195 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0035;
		}
	}
	{
		Collider2D_t1890038195 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = Behaviour_get_enabled_m1239363704(L_8, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_9));
		goto IL_0036;
	}

IL_0035:
	{
		G_B5_0 = 0;
	}

IL_0036:
	{
		return (bool)G_B5_0;
	}
}
// System.Boolean UIToggle::get_isChecked()
extern "C"  bool UIToggle_get_isChecked_m803657233 (UIToggle_t688812808 * __this, const MethodInfo* method)
{
	{
		bool L_0 = UIToggle_get_value_m2236783845(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UIToggle::set_isChecked(System.Boolean)
extern "C"  void UIToggle_set_isChecked_m63185160 (UIToggle_t688812808 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		UIToggle_set_value_m116851932(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UIToggle UIToggle::GetActiveToggle(System.Int32)
extern Il2CppClass* UIToggle_t688812808_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1_get_Item_m1887401614_MethodInfo_var;
extern const uint32_t UIToggle_GetActiveToggle_m348454301_MetadataUsageId;
extern "C"  UIToggle_t688812808 * UIToggle_GetActiveToggle_m348454301 (Il2CppObject * __this /* static, unused */, int32_t ___group0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_GetActiveToggle_m348454301_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	UIToggle_t688812808 * V_1 = NULL;
	{
		V_0 = 0;
		goto IL_003c;
	}

IL_0007:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		BetterList_1_t2185780820 * L_0 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_list_2();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		UIToggle_t688812808 * L_2 = BetterList_1_get_Item_m1887401614(L_0, L_1, /*hidden argument*/BetterList_1_get_Item_m1887401614_MethodInfo_var);
		V_1 = L_2;
		UIToggle_t688812808 * L_3 = V_1;
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		UIToggle_t688812808 * L_5 = V_1;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_group_4();
		int32_t L_7 = ___group0;
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_0038;
		}
	}
	{
		UIToggle_t688812808 * L_8 = V_1;
		NullCheck(L_8);
		bool L_9 = L_8->get_mIsActive_20();
		if (!L_9)
		{
			goto IL_0038;
		}
	}
	{
		UIToggle_t688812808 * L_10 = V_1;
		return L_10;
	}

IL_0038:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_003c:
	{
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		BetterList_1_t2185780820 * L_13 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_list_2();
		NullCheck(L_13);
		int32_t L_14 = L_13->get_size_1();
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_0007;
		}
	}
	{
		return (UIToggle_t688812808 *)NULL;
	}
}
// System.Void UIToggle::OnEnable()
extern Il2CppClass* UIToggle_t688812808_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1_Add_m1922072925_MethodInfo_var;
extern const uint32_t UIToggle_OnEnable_m1303438867_MetadataUsageId;
extern "C"  void UIToggle_OnEnable_m1303438867 (UIToggle_t688812808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_OnEnable_m1303438867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		BetterList_1_t2185780820 * L_0 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_list_2();
		NullCheck(L_0);
		BetterList_1_Add_m1922072925(L_0, __this, /*hidden argument*/BetterList_1_Add_m1922072925_MethodInfo_var);
		return;
	}
}
// System.Void UIToggle::OnDisable()
extern Il2CppClass* UIToggle_t688812808_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1_Remove_m2264897668_MethodInfo_var;
extern const uint32_t UIToggle_OnDisable_m2192836474_MetadataUsageId;
extern "C"  void UIToggle_OnDisable_m2192836474 (UIToggle_t688812808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_OnDisable_m2192836474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		BetterList_1_t2185780820 * L_0 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_list_2();
		NullCheck(L_0);
		BetterList_1_Remove_m2264897668(L_0, __this, /*hidden argument*/BetterList_1_Remove_m2264897668_MethodInfo_var);
		return;
	}
}
// System.Void UIToggle::Start()
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t UIToggle_Start_m2425212307_MetadataUsageId;
extern "C"  void UIToggle_Start_m2425212307 (UIToggle_t688812808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_Start_m2425212307_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	UIWidget_t769069560 * G_B18_0 = NULL;
	UIWidget_t769069560 * G_B14_0 = NULL;
	UIWidget_t769069560 * G_B16_0 = NULL;
	UIWidget_t769069560 * G_B15_0 = NULL;
	float G_B17_0 = 0.0f;
	UIWidget_t769069560 * G_B17_1 = NULL;
	float G_B21_0 = 0.0f;
	UIWidget_t769069560 * G_B21_1 = NULL;
	UIWidget_t769069560 * G_B20_0 = NULL;
	UIWidget_t769069560 * G_B19_0 = NULL;
	{
		bool L_0 = __this->get_mStarted_21();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		bool L_1 = __this->get_startsChecked_19();
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		__this->set_startsChecked_19((bool)0);
		__this->set_startsActive_10((bool)1);
	}

IL_0025:
	{
		bool L_2 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0126;
		}
	}
	{
		UISprite_t661437049 * L_3 = __this->get_checkSprite_15();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0064;
		}
	}
	{
		UIWidget_t769069560 * L_5 = __this->get_activeSprite_5();
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0064;
		}
	}
	{
		UISprite_t661437049 * L_7 = __this->get_checkSprite_15();
		__this->set_activeSprite_5(L_7);
		__this->set_checkSprite_15((UISprite_t661437049 *)NULL);
	}

IL_0064:
	{
		Animation_t350396337 * L_8 = __this->get_checkAnimation_16();
		bool L_9 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_8, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0099;
		}
	}
	{
		Animation_t350396337 * L_10 = __this->get_activeAnimation_7();
		bool L_11 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_10, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0099;
		}
	}
	{
		Animation_t350396337 * L_12 = __this->get_checkAnimation_16();
		__this->set_activeAnimation_7(L_12);
		__this->set_checkAnimation_16((Animation_t350396337 *)NULL);
	}

IL_0099:
	{
		bool L_13 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0103;
		}
	}
	{
		UIWidget_t769069560 * L_14 = __this->get_activeSprite_5();
		bool L_15 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_14, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0103;
		}
	}
	{
		UIWidget_t769069560 * L_16 = __this->get_activeSprite_5();
		bool L_17 = __this->get_invertSpriteState_6();
		G_B14_0 = L_16;
		if (!L_17)
		{
			G_B18_0 = L_16;
			goto IL_00e4;
		}
	}
	{
		bool L_18 = __this->get_startsActive_10();
		G_B15_0 = G_B14_0;
		if (!L_18)
		{
			G_B16_0 = G_B14_0;
			goto IL_00da;
		}
	}
	{
		G_B17_0 = (0.0f);
		G_B17_1 = G_B15_0;
		goto IL_00df;
	}

IL_00da:
	{
		G_B17_0 = (1.0f);
		G_B17_1 = G_B16_0;
	}

IL_00df:
	{
		G_B21_0 = G_B17_0;
		G_B21_1 = G_B17_1;
		goto IL_00fe;
	}

IL_00e4:
	{
		bool L_19 = __this->get_startsActive_10();
		G_B19_0 = G_B18_0;
		if (!L_19)
		{
			G_B20_0 = G_B18_0;
			goto IL_00f9;
		}
	}
	{
		G_B21_0 = (1.0f);
		G_B21_1 = G_B19_0;
		goto IL_00fe;
	}

IL_00f9:
	{
		G_B21_0 = (0.0f);
		G_B21_1 = G_B20_0;
	}

IL_00fe:
	{
		NullCheck(G_B21_1);
		VirtActionInvoker1< float >::Invoke(8 /* System.Void UIWidget::set_alpha(System.Single) */, G_B21_1, G_B21_0);
	}

IL_0103:
	{
		List_1_t506415896 * L_20 = __this->get_onChange_13();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		bool L_21 = EventDelegate_IsValid_m979192339(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		if (!L_21)
		{
			goto IL_0121;
		}
	}
	{
		__this->set_eventReceiver_17((GameObject_t4012695102 *)NULL);
		__this->set_functionName_18((String_t*)NULL);
	}

IL_0121:
	{
		goto IL_015e;
	}

IL_0126:
	{
		bool L_22 = __this->get_startsActive_10();
		__this->set_mIsActive_20((bool)((((int32_t)L_22) == ((int32_t)0))? 1 : 0));
		__this->set_mStarted_21((bool)1);
		bool L_23 = __this->get_instantTween_11();
		V_0 = L_23;
		__this->set_instantTween_11((bool)1);
		bool L_24 = __this->get_startsActive_10();
		UIToggle_Set_m3475184755(__this, L_24, (bool)1, /*hidden argument*/NULL);
		bool L_25 = V_0;
		__this->set_instantTween_11(L_25);
	}

IL_015e:
	{
		return;
	}
}
// System.Void UIToggle::OnClick()
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern const uint32_t UIToggle_OnClick_m2789257818_MetadataUsageId;
extern "C"  void UIToggle_OnClick_m2789257818 (UIToggle_t688812808 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_OnClick_m2789257818_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		bool L_1 = UIToggle_get_isColliderEnabled_m1749226647(__this, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		int32_t L_2 = ((UICamera_t189364953_StaticFields*)UICamera_t189364953_il2cpp_TypeInfo_var->static_fields)->get_currentTouchID_49();
		if ((((int32_t)L_2) == ((int32_t)((int32_t)-2))))
		{
			goto IL_0031;
		}
	}
	{
		bool L_3 = UIToggle_get_value_m2236783845(__this, /*hidden argument*/NULL);
		UIToggle_set_value_m116851932(__this, (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UIToggle::Set(System.Boolean,System.Boolean)
extern Il2CppClass* UIToggle_t688812808_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const MethodInfo* BetterList_1_get_Item_m1887401614_MethodInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisUITweener_t105489188_m3761589691_MethodInfo_var;
extern const uint32_t UIToggle_Set_m3475184755_MetadataUsageId;
extern "C"  void UIToggle_Set_m3475184755 (UIToggle_t688812808 * __this, bool ___state0, bool ___notify1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggle_Set_m3475184755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	UIToggle_t688812808 * V_2 = NULL;
	UIToggle_t688812808 * V_3 = NULL;
	ActiveAnimation_t557316862 * V_4 = NULL;
	ActiveAnimation_t557316862 * V_5 = NULL;
	bool V_6 = false;
	UITweenerU5BU5D_t996366285* V_7 = NULL;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	UITweener_t105489188 * V_10 = NULL;
	UIWidget_t769069560 * G_B10_0 = NULL;
	UIWidget_t769069560 * G_B6_0 = NULL;
	UIWidget_t769069560 * G_B8_0 = NULL;
	UIWidget_t769069560 * G_B7_0 = NULL;
	float G_B9_0 = 0.0f;
	UIWidget_t769069560 * G_B9_1 = NULL;
	float G_B13_0 = 0.0f;
	UIWidget_t769069560 * G_B13_1 = NULL;
	UIWidget_t769069560 * G_B12_0 = NULL;
	UIWidget_t769069560 * G_B11_0 = NULL;
	UIWidget_t769069560 * G_B34_0 = NULL;
	UIWidget_t769069560 * G_B30_0 = NULL;
	UIWidget_t769069560 * G_B32_0 = NULL;
	UIWidget_t769069560 * G_B31_0 = NULL;
	float G_B33_0 = 0.0f;
	UIWidget_t769069560 * G_B33_1 = NULL;
	float G_B37_0 = 0.0f;
	UIWidget_t769069560 * G_B37_1 = NULL;
	UIWidget_t769069560 * G_B36_0 = NULL;
	UIWidget_t769069560 * G_B35_0 = NULL;
	float G_B43_0 = 0.0f;
	GameObject_t4012695102 * G_B43_1 = NULL;
	float G_B39_0 = 0.0f;
	GameObject_t4012695102 * G_B39_1 = NULL;
	float G_B41_0 = 0.0f;
	GameObject_t4012695102 * G_B41_1 = NULL;
	float G_B40_0 = 0.0f;
	GameObject_t4012695102 * G_B40_1 = NULL;
	float G_B42_0 = 0.0f;
	float G_B42_1 = 0.0f;
	GameObject_t4012695102 * G_B42_2 = NULL;
	float G_B46_0 = 0.0f;
	float G_B46_1 = 0.0f;
	GameObject_t4012695102 * G_B46_2 = NULL;
	float G_B45_0 = 0.0f;
	GameObject_t4012695102 * G_B45_1 = NULL;
	float G_B44_0 = 0.0f;
	GameObject_t4012695102 * G_B44_1 = NULL;
	Il2CppObject * G_B58_0 = NULL;
	Animator_t792326996 * G_B58_1 = NULL;
	Il2CppObject * G_B57_0 = NULL;
	Animator_t792326996 * G_B57_1 = NULL;
	int32_t G_B59_0 = 0;
	Il2CppObject * G_B59_1 = NULL;
	Animator_t792326996 * G_B59_2 = NULL;
	Il2CppObject * G_B67_0 = NULL;
	Animation_t350396337 * G_B67_1 = NULL;
	Il2CppObject * G_B66_0 = NULL;
	Animation_t350396337 * G_B66_1 = NULL;
	int32_t G_B68_0 = 0;
	Il2CppObject * G_B68_1 = NULL;
	Animation_t350396337 * G_B68_2 = NULL;
	UITweener_t105489188 * G_B81_0 = NULL;
	UITweener_t105489188 * G_B80_0 = NULL;
	float G_B82_0 = 0.0f;
	UITweener_t105489188 * G_B82_1 = NULL;
	UITweener_t105489188 * G_B90_0 = NULL;
	UITweener_t105489188 * G_B89_0 = NULL;
	float G_B91_0 = 0.0f;
	UITweener_t105489188 * G_B91_1 = NULL;
	{
		Validate_t2938338614 * L_0 = __this->get_validator_14();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Validate_t2938338614 * L_1 = __this->get_validator_14();
		bool L_2 = ___state0;
		NullCheck(L_1);
		bool L_3 = Validate_Invoke_m2875745233(L_1, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_001d;
		}
	}
	{
		return;
	}

IL_001d:
	{
		bool L_4 = __this->get_mStarted_21();
		if (L_4)
		{
			goto IL_0091;
		}
	}
	{
		bool L_5 = ___state0;
		__this->set_mIsActive_20(L_5);
		bool L_6 = ___state0;
		__this->set_startsActive_10(L_6);
		UIWidget_t769069560 * L_7 = __this->get_activeSprite_5();
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_7, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_008c;
		}
	}
	{
		UIWidget_t769069560 * L_9 = __this->get_activeSprite_5();
		bool L_10 = __this->get_invertSpriteState_6();
		G_B6_0 = L_9;
		if (!L_10)
		{
			G_B10_0 = L_9;
			goto IL_0072;
		}
	}
	{
		bool L_11 = ___state0;
		G_B7_0 = G_B6_0;
		if (!L_11)
		{
			G_B8_0 = G_B6_0;
			goto IL_0068;
		}
	}
	{
		G_B9_0 = (0.0f);
		G_B9_1 = G_B7_0;
		goto IL_006d;
	}

IL_0068:
	{
		G_B9_0 = (1.0f);
		G_B9_1 = G_B8_0;
	}

IL_006d:
	{
		G_B13_0 = G_B9_0;
		G_B13_1 = G_B9_1;
		goto IL_0087;
	}

IL_0072:
	{
		bool L_12 = ___state0;
		G_B11_0 = G_B10_0;
		if (!L_12)
		{
			G_B12_0 = G_B10_0;
			goto IL_0082;
		}
	}
	{
		G_B13_0 = (1.0f);
		G_B13_1 = G_B11_0;
		goto IL_0087;
	}

IL_0082:
	{
		G_B13_0 = (0.0f);
		G_B13_1 = G_B12_0;
	}

IL_0087:
	{
		NullCheck(G_B13_1);
		VirtActionInvoker1< float >::Invoke(8 /* System.Void UIWidget::set_alpha(System.Single) */, G_B13_1, G_B13_0);
	}

IL_008c:
	{
		goto IL_0425;
	}

IL_0091:
	{
		bool L_13 = __this->get_mIsActive_20();
		bool L_14 = ___state0;
		if ((((int32_t)L_13) == ((int32_t)L_14)))
		{
			goto IL_0425;
		}
	}
	{
		int32_t L_15 = __this->get_group_4();
		if (!L_15)
		{
			goto IL_011e;
		}
	}
	{
		bool L_16 = ___state0;
		if (!L_16)
		{
			goto IL_011e;
		}
	}
	{
		V_0 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		BetterList_1_t2185780820 * L_17 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_list_2();
		NullCheck(L_17);
		int32_t L_18 = L_17->get_size_1();
		V_1 = L_18;
		goto IL_0117;
	}

IL_00c0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		BetterList_1_t2185780820 * L_19 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_list_2();
		int32_t L_20 = V_0;
		NullCheck(L_19);
		UIToggle_t688812808 * L_21 = BetterList_1_get_Item_m1887401614(L_19, L_20, /*hidden argument*/BetterList_1_get_Item_m1887401614_MethodInfo_var);
		V_2 = L_21;
		UIToggle_t688812808 * L_22 = V_2;
		bool L_23 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_22, __this, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00f1;
		}
	}
	{
		UIToggle_t688812808 * L_24 = V_2;
		NullCheck(L_24);
		int32_t L_25 = L_24->get_group_4();
		int32_t L_26 = __this->get_group_4();
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_00f1;
		}
	}
	{
		UIToggle_t688812808 * L_27 = V_2;
		NullCheck(L_27);
		UIToggle_Set_m3475184755(L_27, (bool)0, (bool)1, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		BetterList_1_t2185780820 * L_28 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_list_2();
		NullCheck(L_28);
		int32_t L_29 = L_28->get_size_1();
		int32_t L_30 = V_1;
		if ((((int32_t)L_29) == ((int32_t)L_30)))
		{
			goto IL_0113;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		BetterList_1_t2185780820 * L_31 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_list_2();
		NullCheck(L_31);
		int32_t L_32 = L_31->get_size_1();
		V_1 = L_32;
		V_0 = 0;
		goto IL_0117;
	}

IL_0113:
	{
		int32_t L_33 = V_0;
		V_0 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_0117:
	{
		int32_t L_34 = V_0;
		int32_t L_35 = V_1;
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_00c0;
		}
	}

IL_011e:
	{
		bool L_36 = ___state0;
		__this->set_mIsActive_20(L_36);
		UIWidget_t769069560 * L_37 = __this->get_activeSprite_5();
		bool L_38 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_37, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_01fa;
		}
	}
	{
		bool L_39 = __this->get_instantTween_11();
		if (L_39)
		{
			goto IL_014c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_40 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_01a0;
		}
	}

IL_014c:
	{
		UIWidget_t769069560 * L_41 = __this->get_activeSprite_5();
		bool L_42 = __this->get_invertSpriteState_6();
		G_B30_0 = L_41;
		if (!L_42)
		{
			G_B34_0 = L_41;
			goto IL_017c;
		}
	}
	{
		bool L_43 = __this->get_mIsActive_20();
		G_B31_0 = G_B30_0;
		if (!L_43)
		{
			G_B32_0 = G_B30_0;
			goto IL_0172;
		}
	}
	{
		G_B33_0 = (0.0f);
		G_B33_1 = G_B31_0;
		goto IL_0177;
	}

IL_0172:
	{
		G_B33_0 = (1.0f);
		G_B33_1 = G_B32_0;
	}

IL_0177:
	{
		G_B37_0 = G_B33_0;
		G_B37_1 = G_B33_1;
		goto IL_0196;
	}

IL_017c:
	{
		bool L_44 = __this->get_mIsActive_20();
		G_B35_0 = G_B34_0;
		if (!L_44)
		{
			G_B36_0 = G_B34_0;
			goto IL_0191;
		}
	}
	{
		G_B37_0 = (1.0f);
		G_B37_1 = G_B35_0;
		goto IL_0196;
	}

IL_0191:
	{
		G_B37_0 = (0.0f);
		G_B37_1 = G_B36_0;
	}

IL_0196:
	{
		NullCheck(G_B37_1);
		VirtActionInvoker1< float >::Invoke(8 /* System.Void UIWidget::set_alpha(System.Single) */, G_B37_1, G_B37_0);
		goto IL_01fa;
	}

IL_01a0:
	{
		UIWidget_t769069560 * L_45 = __this->get_activeSprite_5();
		NullCheck(L_45);
		GameObject_t4012695102 * L_46 = Component_get_gameObject_m1170635899(L_45, /*hidden argument*/NULL);
		bool L_47 = __this->get_invertSpriteState_6();
		G_B39_0 = (0.15f);
		G_B39_1 = L_46;
		if (!L_47)
		{
			G_B43_0 = (0.15f);
			G_B43_1 = L_46;
			goto IL_01da;
		}
	}
	{
		bool L_48 = __this->get_mIsActive_20();
		G_B40_0 = G_B39_0;
		G_B40_1 = G_B39_1;
		if (!L_48)
		{
			G_B41_0 = G_B39_0;
			G_B41_1 = G_B39_1;
			goto IL_01d0;
		}
	}
	{
		G_B42_0 = (0.0f);
		G_B42_1 = G_B40_0;
		G_B42_2 = G_B40_1;
		goto IL_01d5;
	}

IL_01d0:
	{
		G_B42_0 = (1.0f);
		G_B42_1 = G_B41_0;
		G_B42_2 = G_B41_1;
	}

IL_01d5:
	{
		G_B46_0 = G_B42_0;
		G_B46_1 = G_B42_1;
		G_B46_2 = G_B42_2;
		goto IL_01f4;
	}

IL_01da:
	{
		bool L_49 = __this->get_mIsActive_20();
		G_B44_0 = G_B43_0;
		G_B44_1 = G_B43_1;
		if (!L_49)
		{
			G_B45_0 = G_B43_0;
			G_B45_1 = G_B43_1;
			goto IL_01ef;
		}
	}
	{
		G_B46_0 = (1.0f);
		G_B46_1 = G_B44_0;
		G_B46_2 = G_B44_1;
		goto IL_01f4;
	}

IL_01ef:
	{
		G_B46_0 = (0.0f);
		G_B46_1 = G_B45_0;
		G_B46_2 = G_B45_1;
	}

IL_01f4:
	{
		TweenAlpha_Begin_m3125886119(NULL /*static, unused*/, G_B46_2, G_B46_1, G_B46_0, /*hidden argument*/NULL);
	}

IL_01fa:
	{
		bool L_50 = ___notify1;
		if (!L_50)
		{
			goto IL_0280;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		UIToggle_t688812808 * L_51 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_current_3();
		bool L_52 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_51, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0280;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		UIToggle_t688812808 * L_53 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_current_3();
		V_3 = L_53;
		((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->set_current_3(__this);
		List_1_t506415896 * L_54 = __this->get_onChange_13();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		bool L_55 = EventDelegate_IsValid_m979192339(NULL /*static, unused*/, L_54, /*hidden argument*/NULL);
		if (!L_55)
		{
			goto IL_023c;
		}
	}
	{
		List_1_t506415896 * L_56 = __this->get_onChange_13();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Execute_m895247138(NULL /*static, unused*/, L_56, /*hidden argument*/NULL);
		goto IL_027a;
	}

IL_023c:
	{
		GameObject_t4012695102 * L_57 = __this->get_eventReceiver_17();
		bool L_58 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_57, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_58)
		{
			goto IL_027a;
		}
	}
	{
		String_t* L_59 = __this->get_functionName_18();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_60 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (L_60)
		{
			goto IL_027a;
		}
	}
	{
		GameObject_t4012695102 * L_61 = __this->get_eventReceiver_17();
		String_t* L_62 = __this->get_functionName_18();
		bool L_63 = __this->get_mIsActive_20();
		bool L_64 = L_63;
		Il2CppObject * L_65 = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &L_64);
		NullCheck(L_61);
		GameObject_SendMessage_m423373689(L_61, L_62, L_65, 1, /*hidden argument*/NULL);
	}

IL_027a:
	{
		UIToggle_t688812808 * L_66 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->set_current_3(L_66);
	}

IL_0280:
	{
		Animator_t792326996 * L_67 = __this->get_animator_8();
		bool L_68 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_67, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_68)
		{
			goto IL_02dd;
		}
	}
	{
		Animator_t792326996 * L_69 = __this->get_animator_8();
		bool L_70 = ___state0;
		G_B57_0 = NULL;
		G_B57_1 = L_69;
		if (!L_70)
		{
			G_B58_0 = NULL;
			G_B58_1 = L_69;
			goto IL_02a4;
		}
	}
	{
		G_B59_0 = 1;
		G_B59_1 = G_B57_0;
		G_B59_2 = G_B57_1;
		goto IL_02a5;
	}

IL_02a4:
	{
		G_B59_0 = (-1);
		G_B59_1 = G_B58_0;
		G_B59_2 = G_B58_1;
	}

IL_02a5:
	{
		ActiveAnimation_t557316862 * L_71 = ActiveAnimation_Play_m2827471594(NULL /*static, unused*/, G_B59_2, (String_t*)G_B59_1, G_B59_0, 2, 0, /*hidden argument*/NULL);
		V_4 = L_71;
		ActiveAnimation_t557316862 * L_72 = V_4;
		bool L_73 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_72, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_73)
		{
			goto IL_02d8;
		}
	}
	{
		bool L_74 = __this->get_instantTween_11();
		if (L_74)
		{
			goto IL_02d1;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_75 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (L_75)
		{
			goto IL_02d8;
		}
	}

IL_02d1:
	{
		ActiveAnimation_t557316862 * L_76 = V_4;
		NullCheck(L_76);
		ActiveAnimation_Finish_m3165870026(L_76, /*hidden argument*/NULL);
	}

IL_02d8:
	{
		goto IL_0425;
	}

IL_02dd:
	{
		Animation_t350396337 * L_77 = __this->get_activeAnimation_7();
		bool L_78 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_77, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_033a;
		}
	}
	{
		Animation_t350396337 * L_79 = __this->get_activeAnimation_7();
		bool L_80 = ___state0;
		G_B66_0 = NULL;
		G_B66_1 = L_79;
		if (!L_80)
		{
			G_B67_0 = NULL;
			G_B67_1 = L_79;
			goto IL_0301;
		}
	}
	{
		G_B68_0 = 1;
		G_B68_1 = G_B66_0;
		G_B68_2 = G_B66_1;
		goto IL_0302;
	}

IL_0301:
	{
		G_B68_0 = (-1);
		G_B68_1 = G_B67_0;
		G_B68_2 = G_B67_1;
	}

IL_0302:
	{
		ActiveAnimation_t557316862 * L_81 = ActiveAnimation_Play_m2459698113(NULL /*static, unused*/, G_B68_2, (String_t*)G_B68_1, G_B68_0, 2, 0, /*hidden argument*/NULL);
		V_5 = L_81;
		ActiveAnimation_t557316862 * L_82 = V_5;
		bool L_83 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_82, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_83)
		{
			goto IL_0335;
		}
	}
	{
		bool L_84 = __this->get_instantTween_11();
		if (L_84)
		{
			goto IL_032e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_85 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (L_85)
		{
			goto IL_0335;
		}
	}

IL_032e:
	{
		ActiveAnimation_t557316862 * L_86 = V_5;
		NullCheck(L_86);
		ActiveAnimation_Finish_m3165870026(L_86, /*hidden argument*/NULL);
	}

IL_0335:
	{
		goto IL_0425;
	}

IL_033a:
	{
		UITweener_t105489188 * L_87 = __this->get_tween_9();
		bool L_88 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_87, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_88)
		{
			goto IL_0425;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_89 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_6 = L_89;
		UITweener_t105489188 * L_90 = __this->get_tween_9();
		NullCheck(L_90);
		int32_t L_91 = L_90->get_tweenGroup_10();
		if (!L_91)
		{
			goto IL_03e7;
		}
	}
	{
		UITweener_t105489188 * L_92 = __this->get_tween_9();
		NullCheck(L_92);
		UITweenerU5BU5D_t996366285* L_93 = Component_GetComponentsInChildren_TisUITweener_t105489188_m3761589691(L_92, (bool)1, /*hidden argument*/Component_GetComponentsInChildren_TisUITweener_t105489188_m3761589691_MethodInfo_var);
		V_7 = L_93;
		V_8 = 0;
		UITweenerU5BU5D_t996366285* L_94 = V_7;
		NullCheck(L_94);
		V_9 = (((int32_t)((int32_t)(((Il2CppArray *)L_94)->max_length))));
		goto IL_03d9;
	}

IL_037f:
	{
		UITweenerU5BU5D_t996366285* L_95 = V_7;
		int32_t L_96 = V_8;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, L_96);
		int32_t L_97 = L_96;
		V_10 = ((L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_97)));
		UITweener_t105489188 * L_98 = V_10;
		NullCheck(L_98);
		int32_t L_99 = L_98->get_tweenGroup_10();
		UITweener_t105489188 * L_100 = __this->get_tween_9();
		NullCheck(L_100);
		int32_t L_101 = L_100->get_tweenGroup_10();
		if ((!(((uint32_t)L_99) == ((uint32_t)L_101))))
		{
			goto IL_03d3;
		}
	}
	{
		UITweener_t105489188 * L_102 = V_10;
		bool L_103 = ___state0;
		NullCheck(L_102);
		UITweener_Play_m1190576008(L_102, L_103, /*hidden argument*/NULL);
		bool L_104 = __this->get_instantTween_11();
		if (L_104)
		{
			goto IL_03b7;
		}
	}
	{
		bool L_105 = V_6;
		if (L_105)
		{
			goto IL_03d3;
		}
	}

IL_03b7:
	{
		UITweener_t105489188 * L_106 = V_10;
		bool L_107 = ___state0;
		G_B80_0 = L_106;
		if (!L_107)
		{
			G_B81_0 = L_106;
			goto IL_03c9;
		}
	}
	{
		G_B82_0 = (1.0f);
		G_B82_1 = G_B80_0;
		goto IL_03ce;
	}

IL_03c9:
	{
		G_B82_0 = (0.0f);
		G_B82_1 = G_B81_0;
	}

IL_03ce:
	{
		NullCheck(G_B82_1);
		UITweener_set_tweenFactor_m826357225(G_B82_1, G_B82_0, /*hidden argument*/NULL);
	}

IL_03d3:
	{
		int32_t L_108 = V_8;
		V_8 = ((int32_t)((int32_t)L_108+(int32_t)1));
	}

IL_03d9:
	{
		int32_t L_109 = V_8;
		int32_t L_110 = V_9;
		if ((((int32_t)L_109) < ((int32_t)L_110)))
		{
			goto IL_037f;
		}
	}
	{
		goto IL_0425;
	}

IL_03e7:
	{
		UITweener_t105489188 * L_111 = __this->get_tween_9();
		bool L_112 = ___state0;
		NullCheck(L_111);
		UITweener_Play_m1190576008(L_111, L_112, /*hidden argument*/NULL);
		bool L_113 = __this->get_instantTween_11();
		if (L_113)
		{
			goto IL_0405;
		}
	}
	{
		bool L_114 = V_6;
		if (L_114)
		{
			goto IL_0425;
		}
	}

IL_0405:
	{
		UITweener_t105489188 * L_115 = __this->get_tween_9();
		bool L_116 = ___state0;
		G_B89_0 = L_115;
		if (!L_116)
		{
			G_B90_0 = L_115;
			goto IL_041b;
		}
	}
	{
		G_B91_0 = (1.0f);
		G_B91_1 = G_B89_0;
		goto IL_0420;
	}

IL_041b:
	{
		G_B91_0 = (0.0f);
		G_B91_1 = G_B90_0;
	}

IL_0420:
	{
		NullCheck(G_B91_1);
		UITweener_set_tweenFactor_m826357225(G_B91_1, G_B91_0, /*hidden argument*/NULL);
	}

IL_0425:
	{
		return;
	}
}
// System.Void UIToggle/Validate::.ctor(System.Object,System.IntPtr)
extern "C"  void Validate__ctor_m3672608180 (Validate_t2938338614 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UIToggle/Validate::Invoke(System.Boolean)
extern "C"  bool Validate_Invoke_m2875745233 (Validate_t2938338614 * __this, bool ___choice0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Validate_Invoke_m2875745233((Validate_t2938338614 *)__this->get_prev_9(),___choice0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, bool ___choice0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___choice0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, bool ___choice0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___choice0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" bool pinvoke_delegate_wrapper_Validate_t2938338614(Il2CppObject* delegate, bool ___choice0)
{
	typedef int32_t (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___choice0' to native representation

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(___choice0);

	// Marshaling cleanup of parameter '___choice0' native representation

	return _return_value;
}
// System.IAsyncResult UIToggle/Validate::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern Il2CppClass* Boolean_t211005341_il2cpp_TypeInfo_var;
extern const uint32_t Validate_BeginInvoke_m1410260394_MetadataUsageId;
extern "C"  Il2CppObject * Validate_BeginInvoke_m1410260394 (Validate_t2938338614 * __this, bool ___choice0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Validate_BeginInvoke_m1410260394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t211005341_il2cpp_TypeInfo_var, &___choice0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean UIToggle/Validate::EndInvoke(System.IAsyncResult)
extern "C"  bool Validate_EndInvoke_m4229339536 (Validate_t2938338614 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void UIToggledComponents::.ctor()
extern "C"  void UIToggledComponents__ctor_m92720409 (UIToggledComponents_t3531934354 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIToggledComponents::Awake()
extern Il2CppClass* Callback_t4187391077_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIToggle_t688812808_m3526831741_MethodInfo_var;
extern const MethodInfo* UIToggledComponents_Toggle_m2062071007_MethodInfo_var;
extern const uint32_t UIToggledComponents_Awake_m330325628_MetadataUsageId;
extern "C"  void UIToggledComponents_Awake_m330325628 (UIToggledComponents_t3531934354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggledComponents_Awake_m330325628_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIToggle_t688812808 * V_0 = NULL;
	{
		MonoBehaviour_t3012272455 * L_0 = __this->get_target_4();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006f;
		}
	}
	{
		List_1_t3809231424 * L_2 = __this->get_activate_2();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::get_Count() */, L_2);
		if (L_3)
		{
			goto IL_0068;
		}
	}
	{
		List_1_t3809231424 * L_4 = __this->get_deactivate_3();
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::get_Count() */, L_4);
		if (L_5)
		{
			goto IL_0068;
		}
	}
	{
		bool L_6 = __this->get_inverse_5();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		List_1_t3809231424 * L_7 = __this->get_deactivate_3();
		MonoBehaviour_t3012272455 * L_8 = __this->get_target_4();
		NullCheck(L_7);
		VirtActionInvoker1< MonoBehaviour_t3012272455 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::Add(!0) */, L_7, L_8);
		goto IL_0063;
	}

IL_0052:
	{
		List_1_t3809231424 * L_9 = __this->get_activate_2();
		MonoBehaviour_t3012272455 * L_10 = __this->get_target_4();
		NullCheck(L_9);
		VirtActionInvoker1< MonoBehaviour_t3012272455 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::Add(!0) */, L_9, L_10);
	}

IL_0063:
	{
		goto IL_006f;
	}

IL_0068:
	{
		__this->set_target_4((MonoBehaviour_t3012272455 *)NULL);
	}

IL_006f:
	{
		UIToggle_t688812808 * L_11 = Component_GetComponent_TisUIToggle_t688812808_m3526831741(__this, /*hidden argument*/Component_GetComponent_TisUIToggle_t688812808_m3526831741_MethodInfo_var);
		V_0 = L_11;
		UIToggle_t688812808 * L_12 = V_0;
		NullCheck(L_12);
		List_1_t506415896 * L_13 = L_12->get_onChange_13();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)UIToggledComponents_Toggle_m2062071007_MethodInfo_var);
		Callback_t4187391077 * L_15 = (Callback_t4187391077 *)il2cpp_codegen_object_new(Callback_t4187391077_il2cpp_TypeInfo_var);
		Callback__ctor_m3018475132(L_15, __this, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1811262575(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIToggledComponents::Toggle()
extern Il2CppClass* UIToggle_t688812808_il2cpp_TypeInfo_var;
extern const uint32_t UIToggledComponents_Toggle_m2062071007_MetadataUsageId;
extern "C"  void UIToggledComponents_Toggle_m2062071007 (UIToggledComponents_t3531934354 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggledComponents_Toggle_m2062071007_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	MonoBehaviour_t3012272455 * V_1 = NULL;
	int32_t V_2 = 0;
	MonoBehaviour_t3012272455 * V_3 = NULL;
	{
		bool L_0 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0080;
		}
	}
	{
		V_0 = 0;
		goto IL_0033;
	}

IL_0012:
	{
		List_1_t3809231424 * L_1 = __this->get_activate_2();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		MonoBehaviour_t3012272455 * L_3 = VirtFuncInvoker1< MonoBehaviour_t3012272455 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::get_Item(System.Int32) */, L_1, L_2);
		V_1 = L_3;
		MonoBehaviour_t3012272455 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		UIToggle_t688812808 * L_5 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_current_3();
		NullCheck(L_5);
		bool L_6 = UIToggle_get_value_m2236783845(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		Behaviour_set_enabled_m2046806933(L_4, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_0;
		V_0 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_8 = V_0;
		List_1_t3809231424 * L_9 = __this->get_activate_2();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::get_Count() */, L_9);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0012;
		}
	}
	{
		V_2 = 0;
		goto IL_006f;
	}

IL_004b:
	{
		List_1_t3809231424 * L_11 = __this->get_deactivate_3();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		MonoBehaviour_t3012272455 * L_13 = VirtFuncInvoker1< MonoBehaviour_t3012272455 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::get_Item(System.Int32) */, L_11, L_12);
		V_3 = L_13;
		MonoBehaviour_t3012272455 * L_14 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		UIToggle_t688812808 * L_15 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_current_3();
		NullCheck(L_15);
		bool L_16 = UIToggle_get_value_m2236783845(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		Behaviour_set_enabled_m2046806933(L_14, (bool)((((int32_t)L_16) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_17 = V_2;
		V_2 = ((int32_t)((int32_t)L_17+(int32_t)1));
	}

IL_006f:
	{
		int32_t L_18 = V_2;
		List_1_t3809231424 * L_19 = __this->get_deactivate_3();
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>::get_Count() */, L_19);
		if ((((int32_t)L_18) < ((int32_t)L_20)))
		{
			goto IL_004b;
		}
	}

IL_0080:
	{
		return;
	}
}
// System.Void UIToggledObjects::.ctor()
extern "C"  void UIToggledObjects__ctor_m1012912771 (UIToggledObjects_t793825048 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIToggledObjects::Awake()
extern Il2CppClass* Callback_t4187391077_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIToggle_t688812808_m3526831741_MethodInfo_var;
extern const MethodInfo* UIToggledObjects_Toggle_m523263157_MethodInfo_var;
extern const uint32_t UIToggledObjects_Awake_m1250517990_MetadataUsageId;
extern "C"  void UIToggledObjects_Awake_m1250517990 (UIToggledObjects_t793825048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggledObjects_Awake_m1250517990_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIToggle_t688812808 * V_0 = NULL;
	{
		GameObject_t4012695102 * L_0 = __this->get_target_4();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_006f;
		}
	}
	{
		List_1_t514686775 * L_2 = __this->get_activate_2();
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count() */, L_2);
		if (L_3)
		{
			goto IL_0068;
		}
	}
	{
		List_1_t514686775 * L_4 = __this->get_deactivate_3();
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count() */, L_4);
		if (L_5)
		{
			goto IL_0068;
		}
	}
	{
		bool L_6 = __this->get_inverse_5();
		if (!L_6)
		{
			goto IL_0052;
		}
	}
	{
		List_1_t514686775 * L_7 = __this->get_deactivate_3();
		GameObject_t4012695102 * L_8 = __this->get_target_4();
		NullCheck(L_7);
		VirtActionInvoker1< GameObject_t4012695102 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0) */, L_7, L_8);
		goto IL_0063;
	}

IL_0052:
	{
		List_1_t514686775 * L_9 = __this->get_activate_2();
		GameObject_t4012695102 * L_10 = __this->get_target_4();
		NullCheck(L_9);
		VirtActionInvoker1< GameObject_t4012695102 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.GameObject>::Add(!0) */, L_9, L_10);
	}

IL_0063:
	{
		goto IL_006f;
	}

IL_0068:
	{
		__this->set_target_4((GameObject_t4012695102 *)NULL);
	}

IL_006f:
	{
		UIToggle_t688812808 * L_11 = Component_GetComponent_TisUIToggle_t688812808_m3526831741(__this, /*hidden argument*/Component_GetComponent_TisUIToggle_t688812808_m3526831741_MethodInfo_var);
		V_0 = L_11;
		UIToggle_t688812808 * L_12 = V_0;
		NullCheck(L_12);
		List_1_t506415896 * L_13 = L_12->get_onChange_13();
		IntPtr_t L_14;
		L_14.set_m_value_0((void*)(void*)UIToggledObjects_Toggle_m523263157_MethodInfo_var);
		Callback_t4187391077 * L_15 = (Callback_t4187391077 *)il2cpp_codegen_object_new(Callback_t4187391077_il2cpp_TypeInfo_var);
		Callback__ctor_m3018475132(L_15, __this, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1811262575(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIToggledObjects::Toggle()
extern Il2CppClass* UIToggle_t688812808_il2cpp_TypeInfo_var;
extern const uint32_t UIToggledObjects_Toggle_m523263157_MetadataUsageId;
extern "C"  void UIToggledObjects_Toggle_m523263157 (UIToggledObjects_t793825048 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggledObjects_Toggle_m523263157_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIToggle_t688812808_il2cpp_TypeInfo_var);
		UIToggle_t688812808 * L_0 = ((UIToggle_t688812808_StaticFields*)UIToggle_t688812808_il2cpp_TypeInfo_var->static_fields)->get_current_3();
		NullCheck(L_0);
		bool L_1 = UIToggle_get_value_m2236783845(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0077;
		}
	}
	{
		V_1 = 0;
		goto IL_0034;
	}

IL_001d:
	{
		List_1_t514686775 * L_3 = __this->get_activate_2();
		int32_t L_4 = V_1;
		NullCheck(L_3);
		GameObject_t4012695102 * L_5 = VirtFuncInvoker1< GameObject_t4012695102 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32) */, L_3, L_4);
		bool L_6 = V_0;
		UIToggledObjects_Set_m1453031266(__this, L_5, L_6, /*hidden argument*/NULL);
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_8 = V_1;
		List_1_t514686775 * L_9 = __this->get_activate_2();
		NullCheck(L_9);
		int32_t L_10 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count() */, L_9);
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_001d;
		}
	}
	{
		V_2 = 0;
		goto IL_0066;
	}

IL_004c:
	{
		List_1_t514686775 * L_11 = __this->get_deactivate_3();
		int32_t L_12 = V_2;
		NullCheck(L_11);
		GameObject_t4012695102 * L_13 = VirtFuncInvoker1< GameObject_t4012695102 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Item(System.Int32) */, L_11, L_12);
		bool L_14 = V_0;
		UIToggledObjects_Set_m1453031266(__this, L_13, (bool)((((int32_t)L_14) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		int32_t L_15 = V_2;
		V_2 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_16 = V_2;
		List_1_t514686775 * L_17 = __this->get_deactivate_3();
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.GameObject>::get_Count() */, L_17);
		if ((((int32_t)L_16) < ((int32_t)L_18)))
		{
			goto IL_004c;
		}
	}

IL_0077:
	{
		return;
	}
}
// System.Void UIToggledObjects::Set(UnityEngine.GameObject,System.Boolean)
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UIToggledObjects_Set_m1453031266_MetadataUsageId;
extern "C"  void UIToggledObjects_Set_m1453031266 (UIToggledObjects_t793825048 * __this, GameObject_t4012695102 * ___go0, bool ___state1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIToggledObjects_Set_m1453031266_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = ___go0;
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		GameObject_t4012695102 * L_2 = ___go0;
		bool L_3 = ___state1;
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m3941650786(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
	}

IL_0013:
	{
		return;
	}
}
// System.Void UITooltip::.ctor()
extern "C"  void UITooltip__ctor_m3101310844 (UITooltip_t4180872911 * __this, const MethodInfo* method)
{
	{
		__this->set_appearSpeed_7((10.0f));
		__this->set_scalingTransitions_8((bool)1);
		Vector3_t3525329789  L_0 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mSize_14(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UITooltip::get_isVisible()
extern Il2CppClass* UITooltip_t4180872911_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip_get_isVisible_m3118396397_MetadataUsageId;
extern "C"  bool UITooltip_get_isVisible_m3118396397 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_get_isVisible_m3118396397_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		UITooltip_t4180872911 * L_0 = ((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->get_mInstance_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		UITooltip_t4180872911 * L_2 = ((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->get_mInstance_2();
		NullCheck(L_2);
		float L_3 = L_2->get_mTarget_11();
		G_B3_0 = ((((float)L_3) == ((float)(1.0f)))? 1 : 0);
		goto IL_0024;
	}

IL_0023:
	{
		G_B3_0 = 0;
	}

IL_0024:
	{
		return (bool)G_B3_0;
	}
}
// System.Void UITooltip::Awake()
extern Il2CppClass* UITooltip_t4180872911_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip_Awake_m3338916063_MetadataUsageId;
extern "C"  void UITooltip_Awake_m3338916063 (UITooltip_t4180872911 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Awake_m3338916063_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->set_mInstance_2(__this);
		return;
	}
}
// System.Void UITooltip::OnDestroy()
extern Il2CppClass* UITooltip_t4180872911_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip_OnDestroy_m3393713781_MetadataUsageId;
extern "C"  void UITooltip_OnDestroy_m3393713781 (UITooltip_t4180872911 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_OnDestroy_m3393713781_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->set_mInstance_2((UITooltip_t4180872911 *)NULL);
		return;
	}
}
// System.Void UITooltip::Start()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisUIWidget_t769069560_m3945863294_MethodInfo_var;
extern const uint32_t UITooltip_Start_m2048448636_MetadataUsageId;
extern "C"  void UITooltip_Start_m2048448636 (UITooltip_t4180872911 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Start_m2048448636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_mTrans_10(L_0);
		UIWidgetU5BU5D_t4236988201* L_1 = Component_GetComponentsInChildren_TisUIWidget_t769069560_m3945863294(__this, /*hidden argument*/Component_GetComponentsInChildren_TisUIWidget_t769069560_m3945863294_MethodInfo_var);
		__this->set_mWidgets_15(L_1);
		Transform_t284553113 * L_2 = __this->get_mTrans_10();
		NullCheck(L_2);
		Vector3_t3525329789  L_3 = Transform_get_localPosition_m668140784(L_2, /*hidden argument*/NULL);
		__this->set_mPos_13(L_3);
		Camera_t3533968274 * L_4 = __this->get_uiCamera_3();
		bool L_5 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_4, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0050;
		}
	}
	{
		GameObject_t4012695102 * L_6 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		int32_t L_7 = GameObject_get_layer_m1648550306(L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		Camera_t3533968274 * L_8 = NGUITools_FindCameraForLayer_m3244779261(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_uiCamera_3(L_8);
	}

IL_0050:
	{
		VirtActionInvoker1< float >::Invoke(6 /* System.Void UITooltip::SetAlpha(System.Single) */, __this, (0.0f));
		return;
	}
}
// System.Void UITooltip::Update()
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip_Update_m3378217745_MetadataUsageId;
extern "C"  void UITooltip_Update_m3378217745 (UITooltip_t4180872911 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Update_m3378217745_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		GameObject_t4012695102 * L_0 = __this->get_mTooltip_9();
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		GameObject_t4012695102 * L_1 = UICamera_get_tooltipObject_m2595603908(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_mTooltip_9((GameObject_t4012695102 *)NULL);
		__this->set_mTarget_11((0.0f));
	}

IL_0027:
	{
		float L_3 = __this->get_mCurrent_12();
		float L_4 = __this->get_mTarget_11();
		if ((((float)L_3) == ((float)L_4)))
		{
			goto IL_0114;
		}
	}
	{
		float L_5 = __this->get_mCurrent_12();
		float L_6 = __this->get_mTarget_11();
		float L_7 = RealTime_get_deltaTime_m2274453566(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = __this->get_appearSpeed_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_9 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_5, L_6, ((float)((float)L_7*(float)L_8)), /*hidden argument*/NULL);
		__this->set_mCurrent_12(L_9);
		float L_10 = __this->get_mCurrent_12();
		float L_11 = __this->get_mTarget_11();
		float L_12 = fabsf(((float)((float)L_10-(float)L_11)));
		if ((!(((float)L_12) < ((float)(0.001f)))))
		{
			goto IL_0083;
		}
	}
	{
		float L_13 = __this->get_mTarget_11();
		__this->set_mCurrent_12(L_13);
	}

IL_0083:
	{
		float L_14 = __this->get_mCurrent_12();
		float L_15 = __this->get_mCurrent_12();
		VirtActionInvoker1< float >::Invoke(6 /* System.Void UITooltip::SetAlpha(System.Single) */, __this, ((float)((float)L_14*(float)L_15)));
		bool L_16 = __this->get_scalingTransitions_8();
		if (!L_16)
		{
			goto IL_0114;
		}
	}
	{
		Vector3_t3525329789  L_17 = __this->get_mSize_14();
		Vector3_t3525329789  L_18 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_17, (0.25f), /*hidden argument*/NULL);
		V_0 = L_18;
		float L_19 = (&V_0)->get_y_2();
		(&V_0)->set_y_2(((-L_19)));
		Vector3_t3525329789  L_20 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_21 = __this->get_mCurrent_12();
		Vector3_t3525329789  L_22 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_20, ((float)((float)(1.5f)-(float)((float)((float)L_21*(float)(0.5f))))), /*hidden argument*/NULL);
		V_1 = L_22;
		Vector3_t3525329789  L_23 = __this->get_mPos_13();
		Vector3_t3525329789  L_24 = V_0;
		Vector3_t3525329789  L_25 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		Vector3_t3525329789  L_26 = __this->get_mPos_13();
		float L_27 = __this->get_mCurrent_12();
		Vector3_t3525329789  L_28 = Vector3_Lerp_m650470329(NULL /*static, unused*/, L_25, L_26, L_27, /*hidden argument*/NULL);
		V_2 = L_28;
		Transform_t284553113 * L_29 = __this->get_mTrans_10();
		Vector3_t3525329789  L_30 = V_2;
		NullCheck(L_29);
		Transform_set_localPosition_m3504330903(L_29, L_30, /*hidden argument*/NULL);
		Transform_t284553113 * L_31 = __this->get_mTrans_10();
		Vector3_t3525329789  L_32 = V_1;
		NullCheck(L_31);
		Transform_set_localScale_m310756934(L_31, L_32, /*hidden argument*/NULL);
	}

IL_0114:
	{
		return;
	}
}
// System.Void UITooltip::SetAlpha(System.Single)
extern "C"  void UITooltip_SetAlpha_m2402469447 (UITooltip_t4180872911 * __this, float ___val0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	UIWidget_t769069560 * V_2 = NULL;
	Color_t1588175760  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		V_0 = 0;
		UIWidgetU5BU5D_t4236988201* L_0 = __this->get_mWidgets_15();
		NullCheck(L_0);
		V_1 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		goto IL_0033;
	}

IL_0010:
	{
		UIWidgetU5BU5D_t4236988201* L_1 = __this->get_mWidgets_15();
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_2 = ((L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3)));
		UIWidget_t769069560 * L_4 = V_2;
		NullCheck(L_4);
		Color_t1588175760  L_5 = UIWidget_get_color_m2224265652(L_4, /*hidden argument*/NULL);
		V_3 = L_5;
		float L_6 = ___val0;
		(&V_3)->set_a_3(L_6);
		UIWidget_t769069560 * L_7 = V_2;
		Color_t1588175760  L_8 = V_3;
		NullCheck(L_7);
		UIWidget_set_color_m1905035359(L_7, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0033:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) < ((int32_t)L_11)))
		{
			goto IL_0010;
		}
	}
	{
		return;
	}
}
// System.Void UITooltip::SetText(System.String)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2735365941;
extern const uint32_t UITooltip_SetText_m2521650777_MetadataUsageId;
extern "C"  void UITooltip_SetText_m2521650777 (UITooltip_t4180872911 * __this, String_t* ___tooltipText0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_SetText_m2521650777_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t284553113 * V_0 = NULL;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector4_t3525329790  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector2_t3525329788  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t3525329789  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		UILabel_t291504320 * L_0 = __this->get_text_4();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_03e0;
		}
	}
	{
		String_t* L_2 = ___tooltipText0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_03e0;
		}
	}
	{
		__this->set_mTarget_11((1.0f));
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		GameObject_t4012695102 * L_4 = UICamera_get_tooltipObject_m2595603908(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mTooltip_9(L_4);
		UILabel_t291504320 * L_5 = __this->get_text_4();
		String_t* L_6 = ___tooltipText0;
		NullCheck(L_5);
		UILabel_set_text_m4037075551(L_5, L_6, /*hidden argument*/NULL);
		Vector2_t3525329788  L_7 = UICamera_get_lastEventPosition_m1966259817(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_8 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		__this->set_mPos_13(L_8);
		UILabel_t291504320 * L_9 = __this->get_text_4();
		NullCheck(L_9);
		Transform_t284553113 * L_10 = Component_get_transform_m4257140443(L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		Transform_t284553113 * L_11 = V_0;
		NullCheck(L_11);
		Vector3_t3525329789  L_12 = Transform_get_localPosition_m668140784(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Transform_t284553113 * L_13 = V_0;
		NullCheck(L_13);
		Vector3_t3525329789  L_14 = Transform_get_localScale_m3886572677(L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		UILabel_t291504320 * L_15 = __this->get_text_4();
		NullCheck(L_15);
		Vector2_t3525329788  L_16 = UILabel_get_printedSize_m3716427080(L_15, /*hidden argument*/NULL);
		Vector3_t3525329789  L_17 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		__this->set_mSize_14(L_17);
		Vector3_t3525329789 * L_18 = __this->get_address_of_mSize_14();
		Vector3_t3525329789 * L_19 = L_18;
		float L_20 = L_19->get_x_1();
		float L_21 = (&V_2)->get_x_1();
		L_19->set_x_1(((float)((float)L_20*(float)L_21)));
		Vector3_t3525329789 * L_22 = __this->get_address_of_mSize_14();
		Vector3_t3525329789 * L_23 = L_22;
		float L_24 = L_23->get_y_2();
		float L_25 = (&V_2)->get_y_2();
		L_23->set_y_2(((float)((float)L_24*(float)L_25)));
		UISprite_t661437049 * L_26 = __this->get_background_6();
		bool L_27 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_26, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0172;
		}
	}
	{
		UISprite_t661437049 * L_28 = __this->get_background_6();
		NullCheck(L_28);
		Vector4_t3525329790  L_29 = VirtFuncInvoker0< Vector4_t3525329790  >::Invoke(36 /* UnityEngine.Vector4 UISprite::get_border() */, L_28);
		V_3 = L_29;
		Vector3_t3525329789 * L_30 = __this->get_address_of_mSize_14();
		Vector3_t3525329789 * L_31 = L_30;
		float L_32 = L_31->get_x_1();
		float L_33 = (&V_3)->get_x_1();
		float L_34 = (&V_3)->get_z_3();
		float L_35 = (&V_1)->get_x_1();
		float L_36 = (&V_3)->get_x_1();
		L_31->set_x_1(((float)((float)L_32+(float)((float)((float)((float)((float)L_33+(float)L_34))+(float)((float)((float)((float)((float)L_35-(float)L_36))*(float)(2.0f))))))));
		Vector3_t3525329789 * L_37 = __this->get_address_of_mSize_14();
		Vector3_t3525329789 * L_38 = L_37;
		float L_39 = L_38->get_y_2();
		float L_40 = (&V_3)->get_y_2();
		float L_41 = (&V_3)->get_w_4();
		float L_42 = (&V_1)->get_y_2();
		float L_43 = (&V_3)->get_y_2();
		L_38->set_y_2(((float)((float)L_39+(float)((float)((float)((float)((float)L_40+(float)L_41))+(float)((float)((float)((float)((float)((-L_42))-(float)L_43))*(float)(2.0f))))))));
		UISprite_t661437049 * L_44 = __this->get_background_6();
		Vector3_t3525329789 * L_45 = __this->get_address_of_mSize_14();
		float L_46 = L_45->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_47 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		NullCheck(L_44);
		UIWidget_set_width_m3480390811(L_44, L_47, /*hidden argument*/NULL);
		UISprite_t661437049 * L_48 = __this->get_background_6();
		Vector3_t3525329789 * L_49 = __this->get_address_of_mSize_14();
		float L_50 = L_49->get_y_2();
		int32_t L_51 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		NullCheck(L_48);
		UIWidget_set_height_m1838784918(L_48, L_51, /*hidden argument*/NULL);
	}

IL_0172:
	{
		Camera_t3533968274 * L_52 = __this->get_uiCamera_3();
		bool L_53 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_52, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_02e2;
		}
	}
	{
		Vector3_t3525329789 * L_54 = __this->get_address_of_mPos_13();
		Vector3_t3525329789 * L_55 = __this->get_address_of_mPos_13();
		float L_56 = L_55->get_x_1();
		int32_t L_57 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_58 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, ((float)((float)L_56/(float)(((float)((float)L_57))))), /*hidden argument*/NULL);
		L_54->set_x_1(L_58);
		Vector3_t3525329789 * L_59 = __this->get_address_of_mPos_13();
		Vector3_t3525329789 * L_60 = __this->get_address_of_mPos_13();
		float L_61 = L_60->get_y_2();
		int32_t L_62 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_63 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, ((float)((float)L_61/(float)(((float)((float)L_62))))), /*hidden argument*/NULL);
		L_59->set_y_2(L_63);
		Camera_t3533968274 * L_64 = __this->get_uiCamera_3();
		NullCheck(L_64);
		float L_65 = Camera_get_orthographicSize_m3215515490(L_64, /*hidden argument*/NULL);
		Transform_t284553113 * L_66 = __this->get_mTrans_10();
		NullCheck(L_66);
		Transform_t284553113 * L_67 = Transform_get_parent_m2236876972(L_66, /*hidden argument*/NULL);
		NullCheck(L_67);
		Vector3_t3525329789  L_68 = Transform_get_lossyScale_m3749612506(L_67, /*hidden argument*/NULL);
		V_7 = L_68;
		float L_69 = (&V_7)->get_y_2();
		V_4 = ((float)((float)L_65/(float)L_69));
		int32_t L_70 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_71 = V_4;
		V_5 = ((float)((float)((float)((float)(((float)((float)L_70)))*(float)(0.5f)))/(float)L_71));
		float L_72 = V_5;
		Vector3_t3525329789 * L_73 = __this->get_address_of_mSize_14();
		float L_74 = L_73->get_x_1();
		int32_t L_75 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_76 = V_5;
		Vector3_t3525329789 * L_77 = __this->get_address_of_mSize_14();
		float L_78 = L_77->get_y_2();
		int32_t L_79 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector2__ctor_m1517109030((&V_6), ((float)((float)((float)((float)L_72*(float)L_74))/(float)(((float)((float)L_75))))), ((float)((float)((float)((float)L_76*(float)L_78))/(float)(((float)((float)L_79))))), /*hidden argument*/NULL);
		Vector3_t3525329789 * L_80 = __this->get_address_of_mPos_13();
		Vector3_t3525329789 * L_81 = __this->get_address_of_mPos_13();
		float L_82 = L_81->get_x_1();
		float L_83 = (&V_6)->get_x_1();
		float L_84 = Mathf_Min_m2322067385(NULL /*static, unused*/, L_82, ((float)((float)(1.0f)-(float)L_83)), /*hidden argument*/NULL);
		L_80->set_x_1(L_84);
		Vector3_t3525329789 * L_85 = __this->get_address_of_mPos_13();
		Vector3_t3525329789 * L_86 = __this->get_address_of_mPos_13();
		float L_87 = L_86->get_y_2();
		float L_88 = (&V_6)->get_y_2();
		float L_89 = Mathf_Max_m3923796455(NULL /*static, unused*/, L_87, L_88, /*hidden argument*/NULL);
		L_85->set_y_2(L_89);
		Transform_t284553113 * L_90 = __this->get_mTrans_10();
		Camera_t3533968274 * L_91 = __this->get_uiCamera_3();
		Vector3_t3525329789  L_92 = __this->get_mPos_13();
		NullCheck(L_91);
		Vector3_t3525329789  L_93 = Camera_ViewportToWorldPoint_m1641213412(L_91, L_92, /*hidden argument*/NULL);
		NullCheck(L_90);
		Transform_set_position_m3111394108(L_90, L_93, /*hidden argument*/NULL);
		Transform_t284553113 * L_94 = __this->get_mTrans_10();
		NullCheck(L_94);
		Vector3_t3525329789  L_95 = Transform_get_localPosition_m668140784(L_94, /*hidden argument*/NULL);
		__this->set_mPos_13(L_95);
		Vector3_t3525329789 * L_96 = __this->get_address_of_mPos_13();
		Vector3_t3525329789 * L_97 = __this->get_address_of_mPos_13();
		float L_98 = L_97->get_x_1();
		float L_99 = bankers_roundf(L_98);
		L_96->set_x_1(L_99);
		Vector3_t3525329789 * L_100 = __this->get_address_of_mPos_13();
		Vector3_t3525329789 * L_101 = __this->get_address_of_mPos_13();
		float L_102 = L_101->get_y_2();
		float L_103 = bankers_roundf(L_102);
		L_100->set_y_2(L_103);
		goto IL_0394;
	}

IL_02e2:
	{
		Vector3_t3525329789 * L_104 = __this->get_address_of_mPos_13();
		float L_105 = L_104->get_x_1();
		Vector3_t3525329789 * L_106 = __this->get_address_of_mSize_14();
		float L_107 = L_106->get_x_1();
		int32_t L_108 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)((float)((float)L_105+(float)L_107))) > ((float)(((float)((float)L_108)))))))
		{
			goto IL_0321;
		}
	}
	{
		Vector3_t3525329789 * L_109 = __this->get_address_of_mPos_13();
		int32_t L_110 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789 * L_111 = __this->get_address_of_mSize_14();
		float L_112 = L_111->get_x_1();
		L_109->set_x_1(((float)((float)(((float)((float)L_110)))-(float)L_112)));
	}

IL_0321:
	{
		Vector3_t3525329789 * L_113 = __this->get_address_of_mPos_13();
		float L_114 = L_113->get_y_2();
		Vector3_t3525329789 * L_115 = __this->get_address_of_mSize_14();
		float L_116 = L_115->get_y_2();
		if ((!(((float)((float)((float)L_114-(float)L_116))) < ((float)(0.0f)))))
		{
			goto IL_0358;
		}
	}
	{
		Vector3_t3525329789 * L_117 = __this->get_address_of_mPos_13();
		Vector3_t3525329789 * L_118 = __this->get_address_of_mSize_14();
		float L_119 = L_118->get_y_2();
		L_117->set_y_2(L_119);
	}

IL_0358:
	{
		Vector3_t3525329789 * L_120 = __this->get_address_of_mPos_13();
		Vector3_t3525329789 * L_121 = L_120;
		float L_122 = L_121->get_x_1();
		int32_t L_123 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_121->set_x_1(((float)((float)L_122-(float)((float)((float)(((float)((float)L_123)))*(float)(0.5f))))));
		Vector3_t3525329789 * L_124 = __this->get_address_of_mPos_13();
		Vector3_t3525329789 * L_125 = L_124;
		float L_126 = L_125->get_y_2();
		int32_t L_127 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_125->set_y_2(((float)((float)L_126-(float)((float)((float)(((float)((float)L_127)))*(float)(0.5f))))));
	}

IL_0394:
	{
		Transform_t284553113 * L_128 = __this->get_mTrans_10();
		Vector3_t3525329789  L_129 = __this->get_mPos_13();
		NullCheck(L_128);
		Transform_set_localPosition_m3504330903(L_128, L_129, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_130 = __this->get_tooltipRoot_5();
		bool L_131 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_130, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_03cb;
		}
	}
	{
		GameObject_t4012695102 * L_132 = __this->get_tooltipRoot_5();
		NullCheck(L_132);
		GameObject_BroadcastMessage_m3644001332(L_132, _stringLiteral2735365941, /*hidden argument*/NULL);
		goto IL_03db;
	}

IL_03cb:
	{
		UILabel_t291504320 * L_133 = __this->get_text_4();
		NullCheck(L_133);
		Component_BroadcastMessage_m2857110644(L_133, _stringLiteral2735365941, /*hidden argument*/NULL);
	}

IL_03db:
	{
		goto IL_03f2;
	}

IL_03e0:
	{
		__this->set_mTooltip_9((GameObject_t4012695102 *)NULL);
		__this->set_mTarget_11((0.0f));
	}

IL_03f2:
	{
		return;
	}
}
// System.Void UITooltip::ShowText(System.String)
extern Il2CppClass* UITooltip_t4180872911_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip_ShowText_m3776738864_MetadataUsageId;
extern "C"  void UITooltip_ShowText_m3776738864 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_ShowText_m3776738864_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UITooltip_t4180872911 * L_0 = ((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->get_mInstance_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UITooltip_t4180872911 * L_2 = ((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->get_mInstance_2();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void UITooltip::SetText(System.String) */, L_2, L_3);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UITooltip::Show(System.String)
extern Il2CppClass* UITooltip_t4180872911_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip_Show_m2641932573_MetadataUsageId;
extern "C"  void UITooltip_Show_m2641932573 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Show_m2641932573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UITooltip_t4180872911 * L_0 = ((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->get_mInstance_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		UITooltip_t4180872911 * L_2 = ((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->get_mInstance_2();
		String_t* L_3 = ___text0;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(7 /* System.Void UITooltip::SetText(System.String) */, L_2, L_3);
	}

IL_001b:
	{
		return;
	}
}
// System.Void UITooltip::Hide()
extern Il2CppClass* UITooltip_t4180872911_il2cpp_TypeInfo_var;
extern const uint32_t UITooltip_Hide_m1957831434_MetadataUsageId;
extern "C"  void UITooltip_Hide_m1957831434 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITooltip_Hide_m1957831434_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UITooltip_t4180872911 * L_0 = ((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->get_mInstance_2();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002a;
		}
	}
	{
		UITooltip_t4180872911 * L_2 = ((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->get_mInstance_2();
		NullCheck(L_2);
		L_2->set_mTooltip_9((GameObject_t4012695102 *)NULL);
		UITooltip_t4180872911 * L_3 = ((UITooltip_t4180872911_StaticFields*)UITooltip_t4180872911_il2cpp_TypeInfo_var->static_fields)->get_mInstance_2();
		NullCheck(L_3);
		L_3->set_mTarget_11((0.0f));
	}

IL_002a:
	{
		return;
	}
}
// System.Void UITweener::.ctor()
extern Il2CppClass* KeyframeU5BU5D_t2477572954_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3342907448_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t506415896_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1287572350_MethodInfo_var;
extern const uint32_t UITweener__ctor_m2521610951_MetadataUsageId;
extern "C"  void UITweener__ctor_m2521610951 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener__ctor_m2521610951_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		KeyframeU5BU5D_t2477572954* L_0 = ((KeyframeU5BU5D_t2477572954*)SZArrayNew(KeyframeU5BU5D_t2477572954_il2cpp_TypeInfo_var, (uint32_t)2));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		Keyframe_t2095052507  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m3412708539(&L_1, (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t2095052507 *)((L_0)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		KeyframeU5BU5D_t2477572954* L_2 = L_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		Keyframe_t2095052507  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m3412708539(&L_3, (1.0f), (1.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t2095052507 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		AnimationCurve_t3342907448 * L_4 = (AnimationCurve_t3342907448 *)il2cpp_codegen_object_new(AnimationCurve_t3342907448_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2436282331(L_4, L_2, /*hidden argument*/NULL);
		__this->set_animationCurve_5(L_4);
		__this->set_ignoreTimeScale_6((bool)1);
		__this->set_duration_8((1.0f));
		List_1_t506415896 * L_5 = (List_1_t506415896 *)il2cpp_codegen_object_new(List_1_t506415896_il2cpp_TypeInfo_var);
		List_1__ctor_m1287572350(L_5, /*hidden argument*/List_1__ctor_m1287572350_MethodInfo_var);
		__this->set_onFinished_11(L_5);
		__this->set_mAmountPerDelta_17((1000.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UITweener::get_amountPerDelta()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_get_amountPerDelta_m342139981_MetadataUsageId;
extern "C"  float UITweener_get_amountPerDelta_m342139981 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_get_amountPerDelta_m342139981_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_duration_8();
		if ((!(((float)L_0) == ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		return (1000.0f);
	}

IL_0016:
	{
		float L_1 = __this->get_mDuration_16();
		float L_2 = __this->get_duration_8();
		if ((((float)L_1) == ((float)L_2)))
		{
			goto IL_0056;
		}
	}
	{
		float L_3 = __this->get_duration_8();
		__this->set_mDuration_16(L_3);
		float L_4 = __this->get_duration_8();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_5 = fabsf(((float)((float)(1.0f)/(float)L_4)));
		float L_6 = __this->get_mAmountPerDelta_17();
		float L_7 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_mAmountPerDelta_17(((float)((float)L_5*(float)L_7)));
	}

IL_0056:
	{
		float L_8 = __this->get_mAmountPerDelta_17();
		return L_8;
	}
}
// System.Single UITweener::get_tweenFactor()
extern "C"  float UITweener_get_tweenFactor_m3023342018 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_mFactor_18();
		return L_0;
	}
}
// System.Void UITweener::set_tweenFactor(System.Single)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_set_tweenFactor_m826357225_MetadataUsageId;
extern "C"  void UITweener_set_tweenFactor_m826357225 (UITweener_t105489188 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_set_tweenFactor_m826357225_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->set_mFactor_18(L_1);
		return;
	}
}
// AnimationOrTween.Direction UITweener::get_direction()
extern "C"  int32_t UITweener_get_direction_m1737209411 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		float L_0 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = (-1);
		goto IL_0017;
	}

IL_0016:
	{
		G_B3_0 = 1;
	}

IL_0017:
	{
		return (int32_t)(G_B3_0);
	}
}
// System.Void UITweener::Reset()
extern "C"  void UITweener_Reset_m168043892 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mStarted_14();
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void UITweener::SetStartToCurrentValue() */, __this);
		VirtActionInvoker0::Invoke(7 /* System.Void UITweener::SetEndToCurrentValue() */, __this);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UITweener::Start()
extern "C"  void UITweener_Start_m1468748743 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		UITweener_Update_m2587390246(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::Update()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* UITweener_t105489188_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t506415896_il2cpp_TypeInfo_var;
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1287572350_MethodInfo_var;
extern const uint32_t UITweener_Update_m2587390246_MetadataUsageId;
extern "C"  void UITweener_Update_m2587390246 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Update_m2587390246_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	UITweener_t105489188 * V_2 = NULL;
	int32_t V_3 = 0;
	EventDelegate_t4004424223 * V_4 = NULL;
	float G_B3_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B12_0 = 0.0f;
	UITweener_t105489188 * G_B12_1 = NULL;
	float G_B11_0 = 0.0f;
	UITweener_t105489188 * G_B11_1 = NULL;
	float G_B13_0 = 0.0f;
	float G_B13_1 = 0.0f;
	UITweener_t105489188 * G_B13_2 = NULL;
	{
		bool L_0 = __this->get_ignoreTimeScale_6();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		float L_1 = RealTime_get_deltaTime_m2274453566(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001a;
	}

IL_0015:
	{
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001a:
	{
		V_0 = G_B3_0;
		bool L_3 = __this->get_ignoreTimeScale_6();
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		float L_4 = RealTime_get_time_m3537010614(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_4;
		goto IL_0035;
	}

IL_0030:
	{
		float L_5 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B6_0 = L_5;
	}

IL_0035:
	{
		V_1 = G_B6_0;
		bool L_6 = __this->get_mStarted_14();
		if (L_6)
		{
			goto IL_005c;
		}
	}
	{
		V_0 = (0.0f);
		__this->set_mStarted_14((bool)1);
		float L_7 = V_1;
		float L_8 = __this->get_delay_7();
		__this->set_mStartTime_15(((float)((float)L_7+(float)L_8)));
	}

IL_005c:
	{
		float L_9 = V_1;
		float L_10 = __this->get_mStartTime_15();
		if ((!(((float)L_9) < ((float)L_10))))
		{
			goto IL_0069;
		}
	}
	{
		return;
	}

IL_0069:
	{
		float L_11 = __this->get_mFactor_18();
		float L_12 = __this->get_duration_8();
		G_B11_0 = L_11;
		G_B11_1 = __this;
		if ((!(((float)L_12) == ((float)(0.0f)))))
		{
			G_B12_0 = L_11;
			G_B12_1 = __this;
			goto IL_008a;
		}
	}
	{
		G_B13_0 = (1.0f);
		G_B13_1 = G_B11_0;
		G_B13_2 = G_B11_1;
		goto IL_0092;
	}

IL_008a:
	{
		float L_13 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		float L_14 = V_0;
		G_B13_0 = ((float)((float)L_13*(float)L_14));
		G_B13_1 = G_B12_0;
		G_B13_2 = G_B12_1;
	}

IL_0092:
	{
		NullCheck(G_B13_2);
		G_B13_2->set_mFactor_18(((float)((float)G_B13_1+(float)G_B13_0)));
		int32_t L_15 = __this->get_style_4();
		if ((!(((uint32_t)L_15) == ((uint32_t)1))))
		{
			goto IL_00d1;
		}
	}
	{
		float L_16 = __this->get_mFactor_18();
		if ((!(((float)L_16) > ((float)(1.0f)))))
		{
			goto IL_00cc;
		}
	}
	{
		float L_17 = __this->get_mFactor_18();
		float L_18 = __this->get_mFactor_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_19 = floorf(L_18);
		__this->set_mFactor_18(((float)((float)L_17-(float)L_19)));
	}

IL_00cc:
	{
		goto IL_015f;
	}

IL_00d1:
	{
		int32_t L_20 = __this->get_style_4();
		if ((!(((uint32_t)L_20) == ((uint32_t)2))))
		{
			goto IL_015f;
		}
	}
	{
		float L_21 = __this->get_mFactor_18();
		if ((!(((float)L_21) > ((float)(1.0f)))))
		{
			goto IL_011d;
		}
	}
	{
		float L_22 = __this->get_mFactor_18();
		float L_23 = __this->get_mFactor_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_24 = floorf(L_23);
		__this->set_mFactor_18(((float)((float)(1.0f)-(float)((float)((float)L_22-(float)L_24)))));
		float L_25 = __this->get_mAmountPerDelta_17();
		__this->set_mAmountPerDelta_17(((-L_25)));
		goto IL_015f;
	}

IL_011d:
	{
		float L_26 = __this->get_mFactor_18();
		if ((!(((float)L_26) < ((float)(0.0f)))))
		{
			goto IL_015f;
		}
	}
	{
		float L_27 = __this->get_mFactor_18();
		__this->set_mFactor_18(((-L_27)));
		float L_28 = __this->get_mFactor_18();
		float L_29 = __this->get_mFactor_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_30 = floorf(L_29);
		__this->set_mFactor_18(((float)((float)L_28-(float)L_30)));
		float L_31 = __this->get_mAmountPerDelta_17();
		__this->set_mAmountPerDelta_17(((-L_31)));
	}

IL_015f:
	{
		int32_t L_32 = __this->get_style_4();
		if (L_32)
		{
			goto IL_029f;
		}
	}
	{
		float L_33 = __this->get_duration_8();
		if ((((float)L_33) == ((float)(0.0f))))
		{
			goto IL_019a;
		}
	}
	{
		float L_34 = __this->get_mFactor_18();
		if ((((float)L_34) > ((float)(1.0f))))
		{
			goto IL_019a;
		}
	}
	{
		float L_35 = __this->get_mFactor_18();
		if ((!(((float)L_35) < ((float)(0.0f)))))
		{
			goto IL_029f;
		}
	}

IL_019a:
	{
		float L_36 = __this->get_mFactor_18();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_37 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		__this->set_mFactor_18(L_37);
		float L_38 = __this->get_mFactor_18();
		UITweener_Sample_m1154056121(__this, L_38, (bool)1, /*hidden argument*/NULL);
		Behaviour_set_enabled_m2046806933(__this, (bool)0, /*hidden argument*/NULL);
		UITweener_t105489188 * L_39 = ((UITweener_t105489188_StaticFields*)UITweener_t105489188_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		bool L_40 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_39, __this, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_029a;
		}
	}
	{
		UITweener_t105489188 * L_41 = ((UITweener_t105489188_StaticFields*)UITweener_t105489188_il2cpp_TypeInfo_var->static_fields)->get_current_2();
		V_2 = L_41;
		((UITweener_t105489188_StaticFields*)UITweener_t105489188_il2cpp_TypeInfo_var->static_fields)->set_current_2(__this);
		List_1_t506415896 * L_42 = __this->get_onFinished_11();
		if (!L_42)
		{
			goto IL_0260;
		}
	}
	{
		List_1_t506415896 * L_43 = __this->get_onFinished_11();
		__this->set_mTemp_19(L_43);
		List_1_t506415896 * L_44 = (List_1_t506415896 *)il2cpp_codegen_object_new(List_1_t506415896_il2cpp_TypeInfo_var);
		List_1__ctor_m1287572350(L_44, /*hidden argument*/List_1__ctor_m1287572350_MethodInfo_var);
		__this->set_onFinished_11(L_44);
		List_1_t506415896 * L_45 = __this->get_mTemp_19();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Execute_m895247138(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		V_3 = 0;
		goto IL_0248;
	}

IL_020f:
	{
		List_1_t506415896 * L_46 = __this->get_mTemp_19();
		int32_t L_47 = V_3;
		NullCheck(L_46);
		EventDelegate_t4004424223 * L_48 = VirtFuncInvoker1< EventDelegate_t4004424223 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<EventDelegate>::get_Item(System.Int32) */, L_46, L_47);
		V_4 = L_48;
		EventDelegate_t4004424223 * L_49 = V_4;
		if (!L_49)
		{
			goto IL_0244;
		}
	}
	{
		EventDelegate_t4004424223 * L_50 = V_4;
		NullCheck(L_50);
		bool L_51 = L_50->get_oneShot_3();
		if (L_51)
		{
			goto IL_0244;
		}
	}
	{
		List_1_t506415896 * L_52 = __this->get_onFinished_11();
		EventDelegate_t4004424223 * L_53 = V_4;
		EventDelegate_t4004424223 * L_54 = V_4;
		NullCheck(L_54);
		bool L_55 = L_54->get_oneShot_3();
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m3670177826(NULL /*static, unused*/, L_52, L_53, L_55, /*hidden argument*/NULL);
	}

IL_0244:
	{
		int32_t L_56 = V_3;
		V_3 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_0248:
	{
		int32_t L_57 = V_3;
		List_1_t506415896 * L_58 = __this->get_mTemp_19();
		NullCheck(L_58);
		int32_t L_59 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<EventDelegate>::get_Count() */, L_58);
		if ((((int32_t)L_57) < ((int32_t)L_59)))
		{
			goto IL_020f;
		}
	}
	{
		__this->set_mTemp_19((List_1_t506415896 *)NULL);
	}

IL_0260:
	{
		GameObject_t4012695102 * L_60 = __this->get_eventReceiver_12();
		bool L_61 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_60, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_0294;
		}
	}
	{
		String_t* L_62 = __this->get_callWhenFinished_13();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_63 = String_IsNullOrEmpty_m1256468773(NULL /*static, unused*/, L_62, /*hidden argument*/NULL);
		if (L_63)
		{
			goto IL_0294;
		}
	}
	{
		GameObject_t4012695102 * L_64 = __this->get_eventReceiver_12();
		String_t* L_65 = __this->get_callWhenFinished_13();
		NullCheck(L_64);
		GameObject_SendMessage_m423373689(L_64, L_65, __this, 1, /*hidden argument*/NULL);
	}

IL_0294:
	{
		UITweener_t105489188 * L_66 = V_2;
		((UITweener_t105489188_StaticFields*)UITweener_t105489188_il2cpp_TypeInfo_var->static_fields)->set_current_2(L_66);
	}

IL_029a:
	{
		goto IL_02ac;
	}

IL_029f:
	{
		float L_67 = __this->get_mFactor_18();
		UITweener_Sample_m1154056121(__this, L_67, (bool)0, /*hidden argument*/NULL);
	}

IL_02ac:
	{
		return;
	}
}
// System.Void UITweener::SetOnFinished(EventDelegate/Callback)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_SetOnFinished_m2532336547_MetadataUsageId;
extern "C"  void UITweener_SetOnFinished_m2532336547 (UITweener_t105489188 * __this, Callback_t4187391077 * ___del0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_SetOnFinished_m2532336547_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t506415896 * L_0 = __this->get_onFinished_11();
		Callback_t4187391077 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Set_m3964967374(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::SetOnFinished(EventDelegate)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_SetOnFinished_m4129108443_MetadataUsageId;
extern "C"  void UITweener_SetOnFinished_m4129108443 (UITweener_t105489188 * __this, EventDelegate_t4004424223 * ___del0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_SetOnFinished_m4129108443_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t506415896 * L_0 = __this->get_onFinished_11();
		EventDelegate_t4004424223 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Set_m2854322620(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::AddOnFinished(EventDelegate/Callback)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_AddOnFinished_m1398296610_MetadataUsageId;
extern "C"  void UITweener_AddOnFinished_m1398296610 (UITweener_t105489188 * __this, Callback_t4187391077 * ___del0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_AddOnFinished_m1398296610_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t506415896 * L_0 = __this->get_onFinished_11();
		Callback_t4187391077 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m1811262575(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::AddOnFinished(EventDelegate)
extern Il2CppClass* EventDelegate_t4004424223_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_AddOnFinished_m2961468028_MetadataUsageId;
extern "C"  void UITweener_AddOnFinished_m2961468028 (UITweener_t105489188 * __this, EventDelegate_t4004424223 * ___del0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_AddOnFinished_m2961468028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t506415896 * L_0 = __this->get_onFinished_11();
		EventDelegate_t4004424223 * L_1 = ___del0;
		IL2CPP_RUNTIME_CLASS_INIT(EventDelegate_t4004424223_il2cpp_TypeInfo_var);
		EventDelegate_Add_m4032808443(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::RemoveOnFinished(EventDelegate)
extern "C"  void UITweener_RemoveOnFinished_m1549062977 (UITweener_t105489188 * __this, EventDelegate_t4004424223 * ___del0, const MethodInfo* method)
{
	{
		List_1_t506415896 * L_0 = __this->get_onFinished_11();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		List_1_t506415896 * L_1 = __this->get_onFinished_11();
		EventDelegate_t4004424223 * L_2 = ___del0;
		NullCheck(L_1);
		VirtFuncInvoker1< bool, EventDelegate_t4004424223 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<EventDelegate>::Remove(!0) */, L_1, L_2);
	}

IL_0018:
	{
		List_1_t506415896 * L_3 = __this->get_mTemp_19();
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		List_1_t506415896 * L_4 = __this->get_mTemp_19();
		EventDelegate_t4004424223 * L_5 = ___del0;
		NullCheck(L_4);
		VirtFuncInvoker1< bool, EventDelegate_t4004424223 * >::Invoke(26 /* System.Boolean System.Collections.Generic.List`1<EventDelegate>::Remove(!0) */, L_4, L_5);
	}

IL_0030:
	{
		return;
	}
}
// System.Void UITweener::OnDisable()
extern "C"  void UITweener_OnDisable_m3864744878 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		__this->set_mStarted_14((bool)0);
		return;
	}
}
// System.Void UITweener::Sample(System.Single,System.Boolean)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_Sample_m1154056121_MetadataUsageId;
extern "C"  void UITweener_Sample_m1154056121 (UITweener_t105489188 * __this, float ___factor0, bool ___isFinished1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Sample_m1154056121_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	UITweener_t105489188 * G_B18_0 = NULL;
	UITweener_t105489188 * G_B17_0 = NULL;
	float G_B19_0 = 0.0f;
	UITweener_t105489188 * G_B19_1 = NULL;
	{
		float L_0 = ___factor0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m2272733930(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_method_3();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_0040;
		}
	}
	{
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_4 = sinf(((float)((float)(1.57079637f)*(float)((float)((float)(1.0f)-(float)L_3)))));
		V_0 = ((float)((float)(1.0f)-(float)L_4));
		bool L_5 = __this->get_steeperCurves_9();
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		float L_6 = V_0;
		float L_7 = V_0;
		V_0 = ((float)((float)L_6*(float)L_7));
	}

IL_003b:
	{
		goto IL_0121;
	}

IL_0040:
	{
		int32_t L_8 = __this->get_method_3();
		if ((!(((uint32_t)L_8) == ((uint32_t)2))))
		{
			goto IL_007b;
		}
	}
	{
		float L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_10 = sinf(((float)((float)(1.57079637f)*(float)L_9)));
		V_0 = L_10;
		bool L_11 = __this->get_steeperCurves_9();
		if (!L_11)
		{
			goto IL_0076;
		}
	}
	{
		float L_12 = V_0;
		V_0 = ((float)((float)(1.0f)-(float)L_12));
		float L_13 = V_0;
		float L_14 = V_0;
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)L_13*(float)L_14))));
	}

IL_0076:
	{
		goto IL_0121;
	}

IL_007b:
	{
		int32_t L_15 = __this->get_method_3();
		if ((!(((uint32_t)L_15) == ((uint32_t)3))))
		{
			goto IL_00e8;
		}
	}
	{
		float L_16 = V_0;
		float L_17 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_18 = sinf(((float)((float)L_17*(float)(6.28318548f))));
		V_0 = ((float)((float)L_16-(float)((float)((float)L_18/(float)(6.28318548f)))));
		bool L_19 = __this->get_steeperCurves_9();
		if (!L_19)
		{
			goto IL_00e3;
		}
	}
	{
		float L_20 = V_0;
		V_0 = ((float)((float)((float)((float)L_20*(float)(2.0f)))-(float)(1.0f)));
		float L_21 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_22 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_2 = L_22;
		float L_23 = V_0;
		float L_24 = fabsf(L_23);
		V_0 = ((float)((float)(1.0f)-(float)L_24));
		float L_25 = V_0;
		float L_26 = V_0;
		V_0 = ((float)((float)(1.0f)-(float)((float)((float)L_25*(float)L_26))));
		float L_27 = V_2;
		float L_28 = V_0;
		V_0 = ((float)((float)((float)((float)((float)((float)L_27*(float)L_28))*(float)(0.5f)))+(float)(0.5f)));
	}

IL_00e3:
	{
		goto IL_0121;
	}

IL_00e8:
	{
		int32_t L_29 = __this->get_method_3();
		if ((!(((uint32_t)L_29) == ((uint32_t)4))))
		{
			goto IL_0101;
		}
	}
	{
		float L_30 = V_0;
		float L_31 = UITweener_BounceLogic_m384101156(__this, L_30, /*hidden argument*/NULL);
		V_0 = L_31;
		goto IL_0121;
	}

IL_0101:
	{
		int32_t L_32 = __this->get_method_3();
		if ((!(((uint32_t)L_32) == ((uint32_t)5))))
		{
			goto IL_0121;
		}
	}
	{
		float L_33 = V_0;
		float L_34 = UITweener_BounceLogic_m384101156(__this, ((float)((float)(1.0f)-(float)L_33)), /*hidden argument*/NULL);
		V_0 = ((float)((float)(1.0f)-(float)L_34));
	}

IL_0121:
	{
		AnimationCurve_t3342907448 * L_35 = __this->get_animationCurve_5();
		G_B17_0 = __this;
		if (!L_35)
		{
			G_B18_0 = __this;
			goto IL_013e;
		}
	}
	{
		AnimationCurve_t3342907448 * L_36 = __this->get_animationCurve_5();
		float L_37 = V_0;
		NullCheck(L_36);
		float L_38 = AnimationCurve_Evaluate_m547727012(L_36, L_37, /*hidden argument*/NULL);
		G_B19_0 = L_38;
		G_B19_1 = G_B17_0;
		goto IL_013f;
	}

IL_013e:
	{
		float L_39 = V_0;
		G_B19_0 = L_39;
		G_B19_1 = G_B18_0;
	}

IL_013f:
	{
		bool L_40 = ___isFinished1;
		NullCheck(G_B19_1);
		VirtActionInvoker2< float, bool >::Invoke(5 /* System.Void UITweener::OnUpdate(System.Single,System.Boolean) */, G_B19_1, G_B19_0, L_40);
		return;
	}
}
// System.Single UITweener::BounceLogic(System.Single)
extern "C"  float UITweener_BounceLogic_m384101156 (UITweener_t105489188 * __this, float ___val0, const MethodInfo* method)
{
	{
		float L_0 = ___val0;
		if ((!(((float)L_0) < ((float)(0.363636f)))))
		{
			goto IL_001b;
		}
	}
	{
		float L_1 = ___val0;
		float L_2 = ___val0;
		___val0 = ((float)((float)((float)((float)(7.5685f)*(float)L_1))*(float)L_2));
		goto IL_0089;
	}

IL_001b:
	{
		float L_3 = ___val0;
		if ((!(((float)L_3) < ((float)(0.727272f)))))
		{
			goto IL_0045;
		}
	}
	{
		float L_4 = ___val0;
		float L_5 = ((float)((float)L_4-(float)(0.545454f)));
		___val0 = L_5;
		float L_6 = ___val0;
		___val0 = ((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_5))*(float)L_6))+(float)(0.75f)));
		goto IL_0089;
	}

IL_0045:
	{
		float L_7 = ___val0;
		if ((!(((float)L_7) < ((float)(0.90909f)))))
		{
			goto IL_006f;
		}
	}
	{
		float L_8 = ___val0;
		float L_9 = ((float)((float)L_8-(float)(0.818181f)));
		___val0 = L_9;
		float L_10 = ___val0;
		___val0 = ((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_9))*(float)L_10))+(float)(0.9375f)));
		goto IL_0089;
	}

IL_006f:
	{
		float L_11 = ___val0;
		float L_12 = ((float)((float)L_11-(float)(0.9545454f)));
		___val0 = L_12;
		float L_13 = ___val0;
		___val0 = ((float)((float)((float)((float)((float)((float)(7.5625f)*(float)L_12))*(float)L_13))+(float)(0.984375f)));
	}

IL_0089:
	{
		float L_14 = ___val0;
		return L_14;
	}
}
// System.Void UITweener::Play()
extern "C"  void UITweener_Play_m3833433041 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		UITweener_Play_m1190576008(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::PlayForward()
extern "C"  void UITweener_PlayForward_m3184544150 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		UITweener_Play_m1190576008(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::PlayReverse()
extern "C"  void UITweener_PlayReverse_m1477146227 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		UITweener_Play_m1190576008(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::Play(System.Boolean)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_Play_m1190576008_MetadataUsageId;
extern "C"  void UITweener_Play_m1190576008 (UITweener_t105489188 * __this, bool ___forward0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Play_m1190576008_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_1 = fabsf(L_0);
		__this->set_mAmountPerDelta_17(L_1);
		bool L_2 = ___forward0;
		if (L_2)
		{
			goto IL_0024;
		}
	}
	{
		float L_3 = __this->get_mAmountPerDelta_17();
		__this->set_mAmountPerDelta_17(((-L_3)));
	}

IL_0024:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)1, /*hidden argument*/NULL);
		UITweener_Update_m2587390246(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::ResetToBeginning()
extern "C"  void UITweener_ResetToBeginning_m292257456 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	UITweener_t105489188 * G_B2_0 = NULL;
	UITweener_t105489188 * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	UITweener_t105489188 * G_B3_1 = NULL;
	{
		__this->set_mStarted_14((bool)0);
		float L_0 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		G_B1_0 = __this;
		if ((!(((float)L_0) < ((float)(0.0f)))))
		{
			G_B2_0 = __this;
			goto IL_0022;
		}
	}
	{
		G_B3_0 = (1.0f);
		G_B3_1 = G_B1_0;
		goto IL_0027;
	}

IL_0022:
	{
		G_B3_0 = (0.0f);
		G_B3_1 = G_B2_0;
	}

IL_0027:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_mFactor_18(G_B3_0);
		float L_1 = __this->get_mFactor_18();
		UITweener_Sample_m1154056121(__this, L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::Toggle()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UITweener_Toggle_m48266481_MetadataUsageId;
extern "C"  void UITweener_Toggle_m48266481 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UITweener_Toggle_m48266481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = __this->get_mFactor_18();
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0022;
		}
	}
	{
		float L_1 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		__this->set_mAmountPerDelta_17(((-L_1)));
		goto IL_0033;
	}

IL_0022:
	{
		float L_2 = UITweener_get_amountPerDelta_m342139981(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_3 = fabsf(L_2);
		__this->set_mAmountPerDelta_17(L_3);
	}

IL_0033:
	{
		Behaviour_set_enabled_m2046806933(__this, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UITweener::SetStartToCurrentValue()
extern "C"  void UITweener_SetStartToCurrentValue_m1490938032 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UITweener::SetEndToCurrentValue()
extern "C"  void UITweener_SetEndToCurrentValue_m3403311529 (UITweener_t105489188 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIViewport::.ctor()
extern "C"  void UIViewport__ctor_m2969447041 (UIViewport_t2937361242 * __this, const MethodInfo* method)
{
	{
		__this->set_fullSize_5((1.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIViewport::Start()
extern const MethodInfo* Component_GetComponent_TisCamera_t3533968274_m3804104198_MethodInfo_var;
extern const uint32_t UIViewport_Start_m1916584833_MetadataUsageId;
extern "C"  void UIViewport_Start_m1916584833 (UIViewport_t2937361242 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIViewport_Start_m1916584833_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t3533968274 * L_0 = Component_GetComponent_TisCamera_t3533968274_m3804104198(__this, /*hidden argument*/Component_GetComponent_TisCamera_t3533968274_m3804104198_MethodInfo_var);
		__this->set_mCam_6(L_0);
		Camera_t3533968274 * L_1 = __this->get_sourceCamera_2();
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0028;
		}
	}
	{
		Camera_t3533968274 * L_3 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_sourceCamera_2(L_3);
	}

IL_0028:
	{
		return;
	}
}
// System.Void UIViewport::LateUpdate()
extern "C"  void UIViewport_LateUpdate_m1044547698 (UIViewport_t2937361242 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Rect_t1525428817  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	{
		Transform_t284553113 * L_0 = __this->get_topLeft_3();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_011f;
		}
	}
	{
		Transform_t284553113 * L_2 = __this->get_bottomRight_4();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_011f;
		}
	}
	{
		Transform_t284553113 * L_4 = __this->get_topLeft_3();
		NullCheck(L_4);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = GameObject_get_activeInHierarchy_m612450965(L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0113;
		}
	}
	{
		Camera_t3533968274 * L_7 = __this->get_sourceCamera_2();
		Transform_t284553113 * L_8 = __this->get_topLeft_3();
		NullCheck(L_8);
		Vector3_t3525329789  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t3525329789  L_10 = Camera_WorldToScreenPoint_m2400233676(L_7, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		Camera_t3533968274 * L_11 = __this->get_sourceCamera_2();
		Transform_t284553113 * L_12 = __this->get_bottomRight_4();
		NullCheck(L_12);
		Vector3_t3525329789  L_13 = Transform_get_position_m2211398607(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Vector3_t3525329789  L_14 = Camera_WorldToScreenPoint_m2400233676(L_11, L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		float L_15 = (&V_0)->get_x_1();
		int32_t L_16 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_17 = (&V_1)->get_y_2();
		int32_t L_18 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_19 = (&V_1)->get_x_1();
		float L_20 = (&V_0)->get_x_1();
		int32_t L_21 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_22 = (&V_0)->get_y_2();
		float L_23 = (&V_1)->get_y_2();
		int32_t L_24 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect__ctor_m3291325233((&V_2), ((float)((float)L_15/(float)(((float)((float)L_16))))), ((float)((float)L_17/(float)(((float)((float)L_18))))), ((float)((float)((float)((float)L_19-(float)L_20))/(float)(((float)((float)L_21))))), ((float)((float)((float)((float)L_22-(float)L_23))/(float)(((float)((float)L_24))))), /*hidden argument*/NULL);
		float L_25 = __this->get_fullSize_5();
		float L_26 = Rect_get_height_m2154960823((&V_2), /*hidden argument*/NULL);
		V_3 = ((float)((float)L_25*(float)L_26));
		Rect_t1525428817  L_27 = V_2;
		Camera_t3533968274 * L_28 = __this->get_mCam_6();
		NullCheck(L_28);
		Rect_t1525428817  L_29 = Camera_get_rect_m3083266205(L_28, /*hidden argument*/NULL);
		bool L_30 = Rect_op_Inequality_m2236552616(NULL /*static, unused*/, L_27, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_00e5;
		}
	}
	{
		Camera_t3533968274 * L_31 = __this->get_mCam_6();
		Rect_t1525428817  L_32 = V_2;
		NullCheck(L_31);
		Camera_set_rect_m1907189602(L_31, L_32, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		Camera_t3533968274 * L_33 = __this->get_mCam_6();
		NullCheck(L_33);
		float L_34 = Camera_get_orthographicSize_m3215515490(L_33, /*hidden argument*/NULL);
		float L_35 = V_3;
		if ((((float)L_34) == ((float)L_35)))
		{
			goto IL_0102;
		}
	}
	{
		Camera_t3533968274 * L_36 = __this->get_mCam_6();
		float L_37 = V_3;
		NullCheck(L_36);
		Camera_set_orthographicSize_m3910539041(L_36, L_37, /*hidden argument*/NULL);
	}

IL_0102:
	{
		Camera_t3533968274 * L_38 = __this->get_mCam_6();
		NullCheck(L_38);
		Behaviour_set_enabled_m2046806933(L_38, (bool)1, /*hidden argument*/NULL);
		goto IL_011f;
	}

IL_0113:
	{
		Camera_t3533968274 * L_39 = __this->get_mCam_6();
		NullCheck(L_39);
		Behaviour_set_enabled_m2046806933(L_39, (bool)0, /*hidden argument*/NULL);
	}

IL_011f:
	{
		return;
	}
}
// System.Void UIWidget::.ctor()
extern Il2CppClass* UIGeometry_t3586695974_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3U5BU5D_t3227571696_il2cpp_TypeInfo_var;
extern Il2CppClass* UIRect_t2503437976_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget__ctor_m70411171_MetadataUsageId;
extern "C"  void UIWidget__ctor_m70411171 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget__ctor_m70411171_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t1588175760  L_0 = Color_get_white_m3038282331(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mColor_22(L_0);
		__this->set_mPivot_23(4);
		__this->set_mWidth_24(((int32_t)100));
		__this->set_mHeight_25(((int32_t)100));
		__this->set_aspectRatio_33((1.0f));
		UIGeometry_t3586695974 * L_1 = (UIGeometry_t3586695974 *)il2cpp_codegen_object_new(UIGeometry_t3586695974_il2cpp_TypeInfo_var);
		UIGeometry__ctor_m3438354485(L_1, /*hidden argument*/NULL);
		__this->set_geometry_36(L_1);
		__this->set_fillGeometry_37((bool)1);
		__this->set_mPlayMode_38((bool)1);
		Vector4_t3525329790  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m2441427762(&L_2, (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_mDrawRegion_39(L_2);
		__this->set_mIsVisibleByAlpha_41((bool)1);
		__this->set_mIsVisibleByPanel_42((bool)1);
		__this->set_mIsInFront_43((bool)1);
		__this->set_mCorners_47(((Vector3U5BU5D_t3227571696*)SZArrayNew(Vector3U5BU5D_t3227571696_il2cpp_TypeInfo_var, (uint32_t)4)));
		__this->set_mAlphaFrameID_48((-1));
		__this->set_mMatrixFrame_49((-1));
		IL2CPP_RUNTIME_CLASS_INIT(UIRect_t2503437976_il2cpp_TypeInfo_var);
		UIRect__ctor_m1059512579(__this, /*hidden argument*/NULL);
		return;
	}
}
// UIDrawCall/OnRenderCallback UIWidget::get_onRender()
extern "C"  OnRenderCallback_t2210118618 * UIWidget_get_onRender_m4268807263 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		OnRenderCallback_t2210118618 * L_0 = __this->get_mOnRender_29();
		return L_0;
	}
}
// System.Void UIWidget::set_onRender(UIDrawCall/OnRenderCallback)
extern Il2CppClass* OnRenderCallback_t2210118618_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_onRender_m3581219820_MetadataUsageId;
extern "C"  void UIWidget_set_onRender_m3581219820 (UIWidget_t769069560 * __this, OnRenderCallback_t2210118618 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_onRender_m3581219820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		OnRenderCallback_t2210118618 * L_0 = __this->get_mOnRender_29();
		OnRenderCallback_t2210118618 * L_1 = ___value0;
		bool L_2 = MulticastDelegate_op_Inequality_m3462187897(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0092;
		}
	}
	{
		UIDrawCall_t913273974 * L_3 = __this->get_drawCall_46();
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005e;
		}
	}
	{
		UIDrawCall_t913273974 * L_5 = __this->get_drawCall_46();
		NullCheck(L_5);
		OnRenderCallback_t2210118618 * L_6 = L_5->get_onRender_33();
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		OnRenderCallback_t2210118618 * L_7 = __this->get_mOnRender_29();
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		UIDrawCall_t913273974 * L_8 = __this->get_drawCall_46();
		UIDrawCall_t913273974 * L_9 = L_8;
		NullCheck(L_9);
		OnRenderCallback_t2210118618 * L_10 = L_9->get_onRender_33();
		OnRenderCallback_t2210118618 * L_11 = __this->get_mOnRender_29();
		Delegate_t3660574010 * L_12 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		L_9->set_onRender_33(((OnRenderCallback_t2210118618 *)CastclassSealed(L_12, OnRenderCallback_t2210118618_il2cpp_TypeInfo_var)));
	}

IL_005e:
	{
		OnRenderCallback_t2210118618 * L_13 = ___value0;
		__this->set_mOnRender_29(L_13);
		UIDrawCall_t913273974 * L_14 = __this->get_drawCall_46();
		bool L_15 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_14, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0092;
		}
	}
	{
		UIDrawCall_t913273974 * L_16 = __this->get_drawCall_46();
		UIDrawCall_t913273974 * L_17 = L_16;
		NullCheck(L_17);
		OnRenderCallback_t2210118618 * L_18 = L_17->get_onRender_33();
		OnRenderCallback_t2210118618 * L_19 = ___value0;
		Delegate_t3660574010 * L_20 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		L_17->set_onRender_33(((OnRenderCallback_t2210118618 *)CastclassSealed(L_20, OnRenderCallback_t2210118618_il2cpp_TypeInfo_var)));
	}

IL_0092:
	{
		return;
	}
}
// UnityEngine.Vector4 UIWidget::get_drawRegion()
extern "C"  Vector4_t3525329790  UIWidget_get_drawRegion_m382413595 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Vector4_t3525329790  L_0 = __this->get_mDrawRegion_39();
		return L_0;
	}
}
// System.Void UIWidget::set_drawRegion(UnityEngine.Vector4)
extern "C"  void UIWidget_set_drawRegion_m231796482 (UIWidget_t769069560 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	{
		Vector4_t3525329790  L_0 = __this->get_mDrawRegion_39();
		Vector4_t3525329790  L_1 = ___value0;
		bool L_2 = Vector4_op_Inequality_m3118756897(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		Vector4_t3525329790  L_3 = ___value0;
		__this->set_mDrawRegion_39(L_3);
		bool L_4 = __this->get_autoResizeBoxCollider_30();
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		UIWidget_ResizeCollider_m654642185(__this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_002f:
	{
		return;
	}
}
// UnityEngine.Vector2 UIWidget::get_pivotOffset()
extern "C"  Vector2_t3525329788  UIWidget_get_pivotOffset_m2000981938 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = UIWidget_get_pivot_m3551772392(__this, /*hidden argument*/NULL);
		Vector2_t3525329788  L_1 = NGUIMath_GetPivotOffset_m2178153133(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UIWidget::get_width()
extern "C"  int32_t UIWidget_get_width_m293857264 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mWidth_24();
		return L_0;
	}
}
// System.Void UIWidget::set_width(System.Int32)
extern "C"  void UIWidget_set_width_m3480390811 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = V_0;
		___value0 = L_3;
	}

IL_0011:
	{
		int32_t L_4 = __this->get_mWidth_24();
		int32_t L_5 = ___value0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0191;
		}
	}
	{
		int32_t L_6 = __this->get_keepAspectRatio_32();
		if ((((int32_t)L_6) == ((int32_t)2)))
		{
			goto IL_0191;
		}
	}
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean UIRect::get_isAnchoredHorizontally() */, __this);
		if (!L_7)
		{
			goto IL_0184;
		}
	}
	{
		AnchorPoint_t109622203 * L_8 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_8);
		Transform_t284553113 * L_9 = L_8->get_target_0();
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0128;
		}
	}
	{
		AnchorPoint_t109622203 * L_11 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_11);
		Transform_t284553113 * L_12 = L_11->get_target_0();
		bool L_13 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_12, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_14 = __this->get_mPivot_23();
		if ((((int32_t)L_14) == ((int32_t)6)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_15 = __this->get_mPivot_23();
		if ((((int32_t)L_15) == ((int32_t)3)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_16 = __this->get_mPivot_23();
		if (L_16)
		{
			goto IL_00a6;
		}
	}

IL_0083:
	{
		int32_t L_17 = ___value0;
		int32_t L_18 = __this->get_mWidth_24();
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_17-(int32_t)L_18))))), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a6:
	{
		int32_t L_19 = __this->get_mPivot_23();
		if ((((int32_t)L_19) == ((int32_t)8)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_20 = __this->get_mPivot_23();
		if ((((int32_t)L_20) == ((int32_t)5)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_21 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00ed;
		}
	}

IL_00ca:
	{
		int32_t L_22 = __this->get_mWidth_24();
		int32_t L_23 = ___value0;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (((float)((float)((int32_t)((int32_t)L_22-(int32_t)L_23))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00ed:
	{
		int32_t L_24 = ___value0;
		int32_t L_25 = __this->get_mWidth_24();
		V_1 = ((int32_t)((int32_t)L_24-(int32_t)L_25));
		int32_t L_26 = V_1;
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_26-(int32_t)((int32_t)((int32_t)L_27&(int32_t)1))));
		int32_t L_28 = V_1;
		if (!L_28)
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_29 = V_1;
		int32_t L_30 = V_1;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, ((float)((float)(((float)((float)((-L_29)))))*(float)(0.5f))), (0.0f), ((float)((float)(((float)((float)L_30)))*(float)(0.5f))), (0.0f), /*hidden argument*/NULL);
	}

IL_0123:
	{
		goto IL_017f;
	}

IL_0128:
	{
		AnchorPoint_t109622203 * L_31 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_31);
		Transform_t284553113 * L_32 = L_31->get_target_0();
		bool L_33 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_32, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_34 = ___value0;
		int32_t L_35 = __this->get_mWidth_24();
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_34-(int32_t)L_35))))), (0.0f), /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0161:
	{
		int32_t L_36 = __this->get_mWidth_24();
		int32_t L_37 = ___value0;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (((float)((float)((int32_t)((int32_t)L_36-(int32_t)L_37))))), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_017f:
	{
		goto IL_0191;
	}

IL_0184:
	{
		int32_t L_38 = ___value0;
		int32_t L_39 = __this->get_mHeight_25();
		UIWidget_SetDimensions_m163939926(__this, L_38, L_39, /*hidden argument*/NULL);
	}

IL_0191:
	{
		return;
	}
}
// System.Int32 UIWidget::get_height()
extern "C"  int32_t UIWidget_get_height_m1023454943 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mHeight_25();
		return L_0;
	}
}
// System.Void UIWidget::set_height(System.Int32)
extern "C"  void UIWidget_set_height_m1838784918 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_0 = L_0;
		int32_t L_1 = ___value0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_1) >= ((int32_t)L_2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = V_0;
		___value0 = L_3;
	}

IL_0011:
	{
		int32_t L_4 = __this->get_mHeight_25();
		int32_t L_5 = ___value0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0191;
		}
	}
	{
		int32_t L_6 = __this->get_keepAspectRatio_32();
		if ((((int32_t)L_6) == ((int32_t)1)))
		{
			goto IL_0191;
		}
	}
	{
		bool L_7 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean UIRect::get_isAnchoredVertically() */, __this);
		if (!L_7)
		{
			goto IL_0184;
		}
	}
	{
		AnchorPoint_t109622203 * L_8 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_8);
		Transform_t284553113 * L_9 = L_8->get_target_0();
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0128;
		}
	}
	{
		AnchorPoint_t109622203 * L_11 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_11);
		Transform_t284553113 * L_12 = L_11->get_target_0();
		bool L_13 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_12, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0128;
		}
	}
	{
		int32_t L_14 = __this->get_mPivot_23();
		if ((((int32_t)L_14) == ((int32_t)6)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_15 = __this->get_mPivot_23();
		if ((((int32_t)L_15) == ((int32_t)7)))
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_16 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_16) == ((uint32_t)8))))
		{
			goto IL_00a7;
		}
	}

IL_0084:
	{
		int32_t L_17 = ___value0;
		int32_t L_18 = __this->get_mHeight_25();
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_17-(int32_t)L_18))))), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a7:
	{
		int32_t L_19 = __this->get_mPivot_23();
		if (!L_19)
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_20 = __this->get_mPivot_23();
		if ((((int32_t)L_20) == ((int32_t)1)))
		{
			goto IL_00ca;
		}
	}
	{
		int32_t L_21 = __this->get_mPivot_23();
		if ((!(((uint32_t)L_21) == ((uint32_t)2))))
		{
			goto IL_00ed;
		}
	}

IL_00ca:
	{
		int32_t L_22 = __this->get_mHeight_25();
		int32_t L_23 = ___value0;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (((float)((float)((int32_t)((int32_t)L_22-(int32_t)L_23))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00ed:
	{
		int32_t L_24 = ___value0;
		int32_t L_25 = __this->get_mHeight_25();
		V_1 = ((int32_t)((int32_t)L_24-(int32_t)L_25));
		int32_t L_26 = V_1;
		int32_t L_27 = V_1;
		V_1 = ((int32_t)((int32_t)L_26-(int32_t)((int32_t)((int32_t)L_27&(int32_t)1))));
		int32_t L_28 = V_1;
		if (!L_28)
		{
			goto IL_0123;
		}
	}
	{
		int32_t L_29 = V_1;
		int32_t L_30 = V_1;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), ((float)((float)(((float)((float)((-L_29)))))*(float)(0.5f))), (0.0f), ((float)((float)(((float)((float)L_30)))*(float)(0.5f))), /*hidden argument*/NULL);
	}

IL_0123:
	{
		goto IL_017f;
	}

IL_0128:
	{
		AnchorPoint_t109622203 * L_31 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_31);
		Transform_t284553113 * L_32 = L_31->get_target_0();
		bool L_33 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_32, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0161;
		}
	}
	{
		int32_t L_34 = ___value0;
		int32_t L_35 = __this->get_mHeight_25();
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (0.0f), (0.0f), (((float)((float)((int32_t)((int32_t)L_34-(int32_t)L_35))))), /*hidden argument*/NULL);
		goto IL_017f;
	}

IL_0161:
	{
		int32_t L_36 = __this->get_mHeight_25();
		int32_t L_37 = ___value0;
		NGUIMath_AdjustWidget_m3476709133(NULL /*static, unused*/, __this, (0.0f), (((float)((float)((int32_t)((int32_t)L_36-(int32_t)L_37))))), (0.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_017f:
	{
		goto IL_0191;
	}

IL_0184:
	{
		int32_t L_38 = __this->get_mWidth_24();
		int32_t L_39 = ___value0;
		UIWidget_SetDimensions_m163939926(__this, L_38, L_39, /*hidden argument*/NULL);
	}

IL_0191:
	{
		return;
	}
}
// UnityEngine.Color UIWidget::get_color()
extern "C"  Color_t1588175760  UIWidget_get_color_m2224265652 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760  L_0 = __this->get_mColor_22();
		return L_0;
	}
}
// System.Void UIWidget::set_color(UnityEngine.Color)
extern "C"  void UIWidget_set_color_m1905035359 (UIWidget_t769069560 * __this, Color_t1588175760  ___value0, const MethodInfo* method)
{
	bool V_0 = false;
	{
		Color_t1588175760  L_0 = __this->get_mColor_22();
		Color_t1588175760  L_1 = ___value0;
		bool L_2 = Color_op_Inequality_m1917261071(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0037;
		}
	}
	{
		Color_t1588175760 * L_3 = __this->get_address_of_mColor_22();
		float L_4 = L_3->get_a_3();
		float L_5 = (&___value0)->get_a_3();
		V_0 = (bool)((((int32_t)((((float)L_4) == ((float)L_5))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		Color_t1588175760  L_6 = ___value0;
		__this->set_mColor_22(L_6);
		bool L_7 = V_0;
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIWidget::Invalidate(System.Boolean) */, __this, L_7);
	}

IL_0037:
	{
		return;
	}
}
// System.Single UIWidget::get_alpha()
extern "C"  float UIWidget_get_alpha_m3456740746 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Color_t1588175760 * L_0 = __this->get_address_of_mColor_22();
		float L_1 = L_0->get_a_3();
		return L_1;
	}
}
// System.Void UIWidget::set_alpha(System.Single)
extern "C"  void UIWidget_set_alpha_m3960663305 (UIWidget_t769069560 * __this, float ___value0, const MethodInfo* method)
{
	{
		Color_t1588175760 * L_0 = __this->get_address_of_mColor_22();
		float L_1 = L_0->get_a_3();
		float L_2 = ___value0;
		if ((((float)L_1) == ((float)L_2)))
		{
			goto IL_0024;
		}
	}
	{
		Color_t1588175760 * L_3 = __this->get_address_of_mColor_22();
		float L_4 = ___value0;
		L_3->set_a_3(L_4);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIWidget::Invalidate(System.Boolean) */, __this, (bool)1);
	}

IL_0024:
	{
		return;
	}
}
// System.Boolean UIWidget::get_isVisible()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_isVisible_m4257222444_MetadataUsageId;
extern "C"  bool UIWidget_get_isVisible_m4257222444 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_isVisible_m4257222444_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B6_0 = 0;
	{
		bool L_0 = __this->get_mIsVisibleByPanel_42();
		if (!L_0)
		{
			goto IL_0039;
		}
	}
	{
		bool L_1 = __this->get_mIsVisibleByAlpha_41();
		if (!L_1)
		{
			goto IL_0039;
		}
	}
	{
		bool L_2 = __this->get_mIsInFront_43();
		if (!L_2)
		{
			goto IL_0039;
		}
	}
	{
		float L_3 = ((UIRect_t2503437976 *)__this)->get_finalAlpha_20();
		if ((!(((float)L_3) > ((float)(0.001f)))))
		{
			goto IL_0039;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_4 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_4));
		goto IL_003a;
	}

IL_0039:
	{
		G_B6_0 = 0;
	}

IL_003a:
	{
		return (bool)G_B6_0;
	}
}
// System.Boolean UIWidget::get_hasVertices()
extern "C"  bool UIWidget_get_hasVertices_m262926455 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		UIGeometry_t3586695974 * L_0 = __this->get_geometry_36();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		UIGeometry_t3586695974 * L_1 = __this->get_geometry_36();
		NullCheck(L_1);
		bool L_2 = UIGeometry_get_hasVertices_m167811977(L_1, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		return (bool)G_B3_0;
	}
}
// UIWidget/Pivot UIWidget::get_rawPivot()
extern "C"  int32_t UIWidget_get_rawPivot_m978044726 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		return L_0;
	}
}
// System.Void UIWidget::set_rawPivot(UIWidget/Pivot)
extern "C"  void UIWidget_set_rawPivot_m1762094445 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_2 = ___value0;
		__this->set_mPivot_23(L_2);
		bool L_3 = __this->get_autoResizeBoxCollider_30();
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		UIWidget_ResizeCollider_m654642185(__this, /*hidden argument*/NULL);
	}

IL_0024:
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_002a:
	{
		return;
	}
}
// UIWidget/Pivot UIWidget::get_pivot()
extern "C"  int32_t UIWidget_get_pivot_m3551772392 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mPivot_23();
		return L_0;
	}
}
// System.Void UIWidget::set_pivot(UIWidget/Pivot)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_set_pivot_m2063605531_MetadataUsageId;
extern "C"  void UIWidget_set_pivot_m2063605531 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_pivot_m2063605531_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Transform_t284553113 * V_2 = NULL;
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	Vector3_t3525329789  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		int32_t L_0 = __this->get_mPivot_23();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_00ea;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_2 = VirtFuncInvoker0< Vector3U5BU5D_t3227571696* >::Invoke(11 /* UnityEngine.Vector3[] UIWidget::get_worldCorners() */, __this);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		V_0 = (*(Vector3_t3525329789 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		int32_t L_3 = ___value0;
		__this->set_mPivot_23(L_3);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		Vector3U5BU5D_t3227571696* L_4 = VirtFuncInvoker0< Vector3U5BU5D_t3227571696* >::Invoke(11 /* UnityEngine.Vector3[] UIWidget::get_worldCorners() */, __this);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		V_1 = (*(Vector3_t3525329789 *)((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(0))));
		Transform_t284553113 * L_5 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_2 = L_5;
		Transform_t284553113 * L_6 = V_2;
		NullCheck(L_6);
		Vector3_t3525329789  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		Transform_t284553113 * L_8 = V_2;
		NullCheck(L_8);
		Vector3_t3525329789  L_9 = Transform_get_localPosition_m668140784(L_8, /*hidden argument*/NULL);
		V_5 = L_9;
		float L_10 = (&V_5)->get_z_3();
		V_4 = L_10;
		Vector3_t3525329789 * L_11 = (&V_3);
		float L_12 = L_11->get_x_1();
		float L_13 = (&V_0)->get_x_1();
		float L_14 = (&V_1)->get_x_1();
		L_11->set_x_1(((float)((float)L_12+(float)((float)((float)L_13-(float)L_14)))));
		Vector3_t3525329789 * L_15 = (&V_3);
		float L_16 = L_15->get_y_2();
		float L_17 = (&V_0)->get_y_2();
		float L_18 = (&V_1)->get_y_2();
		L_15->set_y_2(((float)((float)L_16+(float)((float)((float)L_17-(float)L_18)))));
		Transform_t284553113 * L_19 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_20 = V_3;
		NullCheck(L_19);
		Transform_set_position_m3111394108(L_19, L_20, /*hidden argument*/NULL);
		Transform_t284553113 * L_21 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Vector3_t3525329789  L_22 = Transform_get_localPosition_m668140784(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		float L_23 = (&V_3)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_24 = bankers_roundf(L_23);
		(&V_3)->set_x_1(L_24);
		float L_25 = (&V_3)->get_y_2();
		float L_26 = bankers_roundf(L_25);
		(&V_3)->set_y_2(L_26);
		float L_27 = V_4;
		(&V_3)->set_z_3(L_27);
		Transform_t284553113 * L_28 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_29 = V_3;
		NullCheck(L_28);
		Transform_set_localPosition_m3504330903(L_28, L_29, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		return;
	}
}
// System.Int32 UIWidget::get_depth()
extern "C"  int32_t UIWidget_get_depth_m507722157 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mDepth_26();
		return L_0;
	}
}
// System.Void UIWidget::set_depth(System.Int32)
extern "C"  void UIWidget_set_depth_m1098656472 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mDepth_26();
		int32_t L_1 = ___value0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t295209936 * L_2 = __this->get_panel_35();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0029;
		}
	}
	{
		UIPanel_t295209936 * L_4 = __this->get_panel_35();
		NullCheck(L_4);
		UIPanel_RemoveWidget_m3627052857(L_4, __this, /*hidden argument*/NULL);
	}

IL_0029:
	{
		int32_t L_5 = ___value0;
		__this->set_mDepth_26(L_5);
		UIPanel_t295209936 * L_6 = __this->get_panel_35();
		bool L_7 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t295209936 * L_8 = __this->get_panel_35();
		NullCheck(L_8);
		UIPanel_AddWidget_m1579877318(L_8, __this, /*hidden argument*/NULL);
		bool L_9 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_006d;
		}
	}
	{
		UIPanel_t295209936 * L_10 = __this->get_panel_35();
		NullCheck(L_10);
		UIPanel_SortWidgets_m710919082(L_10, /*hidden argument*/NULL);
		UIPanel_t295209936 * L_11 = __this->get_panel_35();
		NullCheck(L_11);
		UIPanel_RebuildAllDrawCalls_m1475381252(L_11, /*hidden argument*/NULL);
	}

IL_006d:
	{
		return;
	}
}
// System.Int32 UIWidget::get_raycastDepth()
extern "C"  int32_t UIWidget_get_raycastDepth_m697183634 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	int32_t G_B5_0 = 0;
	{
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		UIPanel_t295209936 * L_2 = __this->get_panel_35();
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		int32_t L_4 = __this->get_mDepth_26();
		UIPanel_t295209936 * L_5 = __this->get_panel_35();
		NullCheck(L_5);
		int32_t L_6 = UIPanel_get_depth_m2725856257(L_5, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)((int32_t)L_4+(int32_t)((int32_t)((int32_t)L_6*(int32_t)((int32_t)1000)))));
		goto IL_004c;
	}

IL_0046:
	{
		int32_t L_7 = __this->get_mDepth_26();
		G_B5_0 = L_7;
	}

IL_004c:
	{
		return G_B5_0;
	}
}
// UnityEngine.Vector3[] UIWidget::get_localCorners()
extern "C"  Vector3U5BU5D_t3227571696* UIWidget_get_localCorners_m4123646229 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	{
		Vector2_t3525329788  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Vector3U5BU5D_t3227571696* L_9 = __this->get_mCorners_47();
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 0);
		float L_10 = V_1;
		float L_11 = V_2;
		Vector3_t3525329789  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m1846874791(&L_12, L_10, L_11, /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_12;
		Vector3U5BU5D_t3227571696* L_13 = __this->get_mCorners_47();
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 1);
		float L_14 = V_1;
		float L_15 = V_4;
		Vector3_t3525329789  L_16;
		memset(&L_16, 0, sizeof(L_16));
		Vector3__ctor_m1846874791(&L_16, L_14, L_15, /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_16;
		Vector3U5BU5D_t3227571696* L_17 = __this->get_mCorners_47();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		float L_18 = V_3;
		float L_19 = V_4;
		Vector3_t3525329789  L_20;
		memset(&L_20, 0, sizeof(L_20));
		Vector3__ctor_m1846874791(&L_20, L_18, L_19, /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_17)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_20;
		Vector3U5BU5D_t3227571696* L_21 = __this->get_mCorners_47();
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 3);
		float L_22 = V_3;
		float L_23 = V_2;
		Vector3_t3525329789  L_24;
		memset(&L_24, 0, sizeof(L_24));
		Vector3__ctor_m1846874791(&L_24, L_22, L_23, /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_24;
		Vector3U5BU5D_t3227571696* L_25 = __this->get_mCorners_47();
		return L_25;
	}
}
// UnityEngine.Vector2 UIWidget::get_localSize()
extern "C"  Vector2_t3525329788  UIWidget_get_localSize_m237983625 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t3227571696* V_0 = NULL;
	{
		Vector3U5BU5D_t3227571696* L_0 = VirtFuncInvoker0< Vector3U5BU5D_t3227571696* >::Invoke(10 /* UnityEngine.Vector3[] UIWidget::get_localCorners() */, __this);
		V_0 = L_0;
		Vector3U5BU5D_t3227571696* L_1 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 2);
		Vector3U5BU5D_t3227571696* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		Vector3_t3525329789  L_3 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, (*(Vector3_t3525329789 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (*(Vector3_t3525329789 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		Vector2_t3525329788  L_4 = Vector2_op_Implicit_m4083860659(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector3 UIWidget::get_localCenter()
extern "C"  Vector3_t3525329789  UIWidget_get_localCenter_m64081310 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Vector3U5BU5D_t3227571696* V_0 = NULL;
	{
		Vector3U5BU5D_t3227571696* L_0 = VirtFuncInvoker0< Vector3U5BU5D_t3227571696* >::Invoke(10 /* UnityEngine.Vector3[] UIWidget::get_localCorners() */, __this);
		V_0 = L_0;
		Vector3U5BU5D_t3227571696* L_1 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		Vector3U5BU5D_t3227571696* L_2 = V_0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 2);
		Vector3_t3525329789  L_3 = Vector3_Lerp_m650470329(NULL /*static, unused*/, (*(Vector3_t3525329789 *)((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector3_t3525329789 *)((L_2)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (0.5f), /*hidden argument*/NULL);
		return L_3;
	}
}
// UnityEngine.Vector3[] UIWidget::get_worldCorners()
extern "C"  Vector3U5BU5D_t3227571696* UIWidget_get_worldCorners_m3779221710 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Transform_t284553113 * V_5 = NULL;
	{
		Vector2_t3525329788  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Transform_t284553113 * L_9 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_5 = L_9;
		Vector3U5BU5D_t3227571696* L_10 = __this->get_mCorners_47();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		Transform_t284553113 * L_11 = V_5;
		float L_12 = V_1;
		float L_13 = V_2;
		NullCheck(L_11);
		Vector3_t3525329789  L_14 = Transform_TransformPoint_m3094713172(L_11, L_12, L_13, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_14;
		Vector3U5BU5D_t3227571696* L_15 = __this->get_mCorners_47();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		Transform_t284553113 * L_16 = V_5;
		float L_17 = V_1;
		float L_18 = V_4;
		NullCheck(L_16);
		Vector3_t3525329789  L_19 = Transform_TransformPoint_m3094713172(L_16, L_17, L_18, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_19;
		Vector3U5BU5D_t3227571696* L_20 = __this->get_mCorners_47();
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 2);
		Transform_t284553113 * L_21 = V_5;
		float L_22 = V_3;
		float L_23 = V_4;
		NullCheck(L_21);
		Vector3_t3525329789  L_24 = Transform_TransformPoint_m3094713172(L_21, L_22, L_23, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_24;
		Vector3U5BU5D_t3227571696* L_25 = __this->get_mCorners_47();
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 3);
		Transform_t284553113 * L_26 = V_5;
		float L_27 = V_3;
		float L_28 = V_2;
		NullCheck(L_26);
		Vector3_t3525329789  L_29 = Transform_TransformPoint_m3094713172(L_26, L_27, L_28, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_29;
		Vector3U5BU5D_t3227571696* L_30 = __this->get_mCorners_47();
		return L_30;
	}
}
// UnityEngine.Vector3 UIWidget::get_worldCenter()
extern "C"  Vector3_t3525329789  UIWidget_get_worldCenter_m3655201477 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_1 = UIWidget_get_localCenter_m64081310(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_2 = Transform_TransformPoint_m437395512(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Vector4 UIWidget::get_drawingDimensions()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_get_drawingDimensions_m3426169802_MetadataUsageId;
extern "C"  Vector4_t3525329790  UIWidget_get_drawingDimensions_m3426169802 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_drawingDimensions_m3426169802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float G_B3_0 = 0.0f;
	float G_B5_0 = 0.0f;
	float G_B4_0 = 0.0f;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	float G_B9_2 = 0.0f;
	float G_B11_0 = 0.0f;
	float G_B11_1 = 0.0f;
	float G_B11_2 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B10_1 = 0.0f;
	float G_B10_2 = 0.0f;
	float G_B12_0 = 0.0f;
	float G_B12_1 = 0.0f;
	float G_B12_2 = 0.0f;
	float G_B12_3 = 0.0f;
	{
		Vector2_t3525329788  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		Vector4_t3525329790 * L_9 = __this->get_address_of_mDrawRegion_39();
		float L_10 = L_9->get_x_1();
		if ((!(((float)L_10) == ((float)(0.0f)))))
		{
			goto IL_0059;
		}
	}
	{
		float L_11 = V_1;
		G_B3_0 = L_11;
		goto IL_006b;
	}

IL_0059:
	{
		float L_12 = V_1;
		float L_13 = V_3;
		Vector4_t3525329790 * L_14 = __this->get_address_of_mDrawRegion_39();
		float L_15 = L_14->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		G_B3_0 = L_16;
	}

IL_006b:
	{
		Vector4_t3525329790 * L_17 = __this->get_address_of_mDrawRegion_39();
		float L_18 = L_17->get_y_2();
		G_B4_0 = G_B3_0;
		if ((!(((float)L_18) == ((float)(0.0f)))))
		{
			G_B5_0 = G_B3_0;
			goto IL_0086;
		}
	}
	{
		float L_19 = V_2;
		G_B6_0 = L_19;
		G_B6_1 = G_B4_0;
		goto IL_0099;
	}

IL_0086:
	{
		float L_20 = V_2;
		float L_21 = V_4;
		Vector4_t3525329790 * L_22 = __this->get_address_of_mDrawRegion_39();
		float L_23 = L_22->get_y_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_24 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_20, L_21, L_23, /*hidden argument*/NULL);
		G_B6_0 = L_24;
		G_B6_1 = G_B5_0;
	}

IL_0099:
	{
		Vector4_t3525329790 * L_25 = __this->get_address_of_mDrawRegion_39();
		float L_26 = L_25->get_z_3();
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		if ((!(((float)L_26) == ((float)(1.0f)))))
		{
			G_B8_0 = G_B6_0;
			G_B8_1 = G_B6_1;
			goto IL_00b4;
		}
	}
	{
		float L_27 = V_3;
		G_B9_0 = L_27;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_00c6;
	}

IL_00b4:
	{
		float L_28 = V_1;
		float L_29 = V_3;
		Vector4_t3525329790 * L_30 = __this->get_address_of_mDrawRegion_39();
		float L_31 = L_30->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_32 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_28, L_29, L_31, /*hidden argument*/NULL);
		G_B9_0 = L_32;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_00c6:
	{
		Vector4_t3525329790 * L_33 = __this->get_address_of_mDrawRegion_39();
		float L_34 = L_33->get_w_4();
		G_B10_0 = G_B9_0;
		G_B10_1 = G_B9_1;
		G_B10_2 = G_B9_2;
		if ((!(((float)L_34) == ((float)(1.0f)))))
		{
			G_B11_0 = G_B9_0;
			G_B11_1 = G_B9_1;
			G_B11_2 = G_B9_2;
			goto IL_00e2;
		}
	}
	{
		float L_35 = V_4;
		G_B12_0 = L_35;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		G_B12_3 = G_B10_2;
		goto IL_00f5;
	}

IL_00e2:
	{
		float L_36 = V_2;
		float L_37 = V_4;
		Vector4_t3525329790 * L_38 = __this->get_address_of_mDrawRegion_39();
		float L_39 = L_38->get_w_4();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_40 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_36, L_37, L_39, /*hidden argument*/NULL);
		G_B12_0 = L_40;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
		G_B12_3 = G_B11_2;
	}

IL_00f5:
	{
		Vector4_t3525329790  L_41;
		memset(&L_41, 0, sizeof(L_41));
		Vector4__ctor_m2441427762(&L_41, G_B12_3, G_B12_2, G_B12_1, G_B12_0, /*hidden argument*/NULL);
		return L_41;
	}
}
// UnityEngine.Material UIWidget::get_material()
extern "C"  Material_t1886596500 * UIWidget_get_material_m1008364976 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		return (Material_t1886596500 *)NULL;
	}
}
// System.Void UIWidget::set_material(UnityEngine.Material)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4087805087;
extern const uint32_t UIWidget_set_material_m1657715431_MetadataUsageId;
extern "C"  void UIWidget_set_material_m1657715431 (UIWidget_t769069560 * __this, Material_t1886596500 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_material_m1657715431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m389863537(NULL /*static, unused*/, L_0, _stringLiteral4087805087, /*hidden argument*/NULL);
		NotImplementedException_t1091014741 * L_2 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// UnityEngine.Texture UIWidget::get_mainTexture()
extern "C"  Texture_t1769722184 * UIWidget_get_mainTexture_m1554632171 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Material_t1886596500 * V_0 = NULL;
	Texture_t1769722184 * G_B3_0 = NULL;
	{
		Material_t1886596500 * L_0 = VirtFuncInvoker0< Material_t1886596500 * >::Invoke(25 /* UnityEngine.Material UIWidget::get_material() */, __this);
		V_0 = L_0;
		Material_t1886596500 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Material_t1886596500 * L_3 = V_0;
		NullCheck(L_3);
		Texture_t1769722184 * L_4 = Material_get_mainTexture_m1012267054(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = ((Texture_t1769722184 *)(NULL));
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::set_mainTexture(UnityEngine.Texture)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3660659926;
extern const uint32_t UIWidget_set_mainTexture_m1243757896_MetadataUsageId;
extern "C"  void UIWidget_set_mainTexture_m1243757896 (UIWidget_t769069560 * __this, Texture_t1769722184 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_mainTexture_m1243757896_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m389863537(NULL /*static, unused*/, L_0, _stringLiteral3660659926, /*hidden argument*/NULL);
		NotImplementedException_t1091014741 * L_2 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// UnityEngine.Shader UIWidget::get_shader()
extern "C"  Shader_t3998140498 * UIWidget_get_shader_m1849353712 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	Material_t1886596500 * V_0 = NULL;
	Shader_t3998140498 * G_B3_0 = NULL;
	{
		Material_t1886596500 * L_0 = VirtFuncInvoker0< Material_t1886596500 * >::Invoke(25 /* UnityEngine.Material UIWidget::get_material() */, __this);
		V_0 = L_0;
		Material_t1886596500 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Material_t1886596500 * L_3 = V_0;
		NullCheck(L_3);
		Shader_t3998140498 * L_4 = Material_get_shader_m2881845503(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		goto IL_001f;
	}

IL_001e:
	{
		G_B3_0 = ((Shader_t3998140498 *)(NULL));
	}

IL_001f:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::set_shader(UnityEngine.Shader)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* NotImplementedException_t1091014741_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1613366753;
extern const uint32_t UIWidget_set_shader_m1936742439_MetadataUsageId;
extern "C"  void UIWidget_set_shader_m1936742439 (UIWidget_t769069560 * __this, Shader_t3998140498 * ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_set_shader_m1936742439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = Object_GetType_m2022236990(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = String_Concat_m389863537(NULL /*static, unused*/, L_0, _stringLiteral1613366753, /*hidden argument*/NULL);
		NotImplementedException_t1091014741 * L_2 = (NotImplementedException_t1091014741 *)il2cpp_codegen_object_new(NotImplementedException_t1091014741_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_2, L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}
}
// UnityEngine.Vector2 UIWidget::get_relativeSize()
extern "C"  Vector2_t3525329788  UIWidget_get_relativeSize_m726361522 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Vector2_t3525329788  L_0 = Vector2_get_one_m2767488832(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UIWidget::get_hasBoxCollider()
extern Il2CppClass* BoxCollider_t131631884_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCollider_t955670625_m3246438266_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var;
extern const uint32_t UIWidget_get_hasBoxCollider_m2767222083_MetadataUsageId;
extern "C"  bool UIWidget_get_hasBoxCollider_m2767222083 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_get_hasBoxCollider_m2767222083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	BoxCollider_t131631884 * V_0 = NULL;
	{
		Collider_t955670625 * L_0 = Component_GetComponent_TisCollider_t955670625_m3246438266(__this, /*hidden argument*/Component_GetComponent_TisCollider_t955670625_m3246438266_MethodInfo_var);
		V_0 = ((BoxCollider_t131631884 *)IsInstSealed(L_0, BoxCollider_t131631884_il2cpp_TypeInfo_var));
		BoxCollider_t131631884 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		return (bool)1;
	}

IL_001a:
	{
		BoxCollider2D_t262790558 * L_3 = Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t262790558_m2584023519_MethodInfo_var);
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UIWidget::SetDimensions(System.Int32,System.Int32)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_SetDimensions_m163939926_MetadataUsageId;
extern "C"  void UIWidget_SetDimensions_m163939926 (UIWidget_t769069560 * __this, int32_t ___w0, int32_t ___h1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetDimensions_m163939926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_mWidth_24();
		int32_t L_1 = ___w0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = __this->get_mHeight_25();
		int32_t L_3 = ___h1;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_00b8;
		}
	}

IL_0018:
	{
		int32_t L_4 = ___w0;
		__this->set_mWidth_24(L_4);
		int32_t L_5 = ___h1;
		__this->set_mHeight_25(L_5);
		int32_t L_6 = __this->get_keepAspectRatio_32();
		if ((!(((uint32_t)L_6) == ((uint32_t)1))))
		{
			goto IL_0050;
		}
	}
	{
		int32_t L_7 = __this->get_mWidth_24();
		float L_8 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_9 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)(((float)((float)L_7)))/(float)L_8)), /*hidden argument*/NULL);
		__this->set_mHeight_25(L_9);
		goto IL_009a;
	}

IL_0050:
	{
		int32_t L_10 = __this->get_keepAspectRatio_32();
		if ((!(((uint32_t)L_10) == ((uint32_t)2))))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_11 = __this->get_mHeight_25();
		float L_12 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_13 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)(((float)((float)L_11)))*(float)L_12)), /*hidden argument*/NULL);
		__this->set_mWidth_24(L_13);
		goto IL_009a;
	}

IL_007a:
	{
		int32_t L_14 = __this->get_keepAspectRatio_32();
		if (L_14)
		{
			goto IL_009a;
		}
	}
	{
		int32_t L_15 = __this->get_mWidth_24();
		int32_t L_16 = __this->get_mHeight_25();
		__this->set_aspectRatio_33(((float)((float)(((float)((float)L_15)))/(float)(((float)((float)L_16))))));
	}

IL_009a:
	{
		__this->set_mMoved_45((bool)1);
		bool L_17 = __this->get_autoResizeBoxCollider_30();
		if (!L_17)
		{
			goto IL_00b2;
		}
	}
	{
		UIWidget_ResizeCollider_m654642185(__this, /*hidden argument*/NULL);
	}

IL_00b2:
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_00b8:
	{
		return;
	}
}
// UnityEngine.Vector3[] UIWidget::GetSides(UnityEngine.Transform)
extern "C"  Vector3U5BU5D_t3227571696* UIWidget_GetSides_m715844286 (UIWidget_t769069560 * __this, Transform_t284553113 * ___relativeTo0, const MethodInfo* method)
{
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	Transform_t284553113 * V_7 = NULL;
	int32_t V_8 = 0;
	{
		Vector2_t3525329788  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = (&V_0)->get_x_1();
		int32_t L_2 = __this->get_mWidth_24();
		V_1 = ((float)((float)((-L_1))*(float)(((float)((float)L_2)))));
		float L_3 = (&V_0)->get_y_2();
		int32_t L_4 = __this->get_mHeight_25();
		V_2 = ((float)((float)((-L_3))*(float)(((float)((float)L_4)))));
		float L_5 = V_1;
		int32_t L_6 = __this->get_mWidth_24();
		V_3 = ((float)((float)L_5+(float)(((float)((float)L_6)))));
		float L_7 = V_2;
		int32_t L_8 = __this->get_mHeight_25();
		V_4 = ((float)((float)L_7+(float)(((float)((float)L_8)))));
		float L_9 = V_1;
		float L_10 = V_3;
		V_5 = ((float)((float)((float)((float)L_9+(float)L_10))*(float)(0.5f)));
		float L_11 = V_2;
		float L_12 = V_4;
		V_6 = ((float)((float)((float)((float)L_11+(float)L_12))*(float)(0.5f)));
		Transform_t284553113 * L_13 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_7 = L_13;
		Vector3U5BU5D_t3227571696* L_14 = __this->get_mCorners_47();
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		Transform_t284553113 * L_15 = V_7;
		float L_16 = V_1;
		float L_17 = V_6;
		NullCheck(L_15);
		Vector3_t3525329789  L_18 = Transform_TransformPoint_m3094713172(L_15, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))) = L_18;
		Vector3U5BU5D_t3227571696* L_19 = __this->get_mCorners_47();
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 1);
		Transform_t284553113 * L_20 = V_7;
		float L_21 = V_5;
		float L_22 = V_4;
		NullCheck(L_20);
		Vector3_t3525329789  L_23 = Transform_TransformPoint_m3094713172(L_20, L_21, L_22, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))) = L_23;
		Vector3U5BU5D_t3227571696* L_24 = __this->get_mCorners_47();
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 2);
		Transform_t284553113 * L_25 = V_7;
		float L_26 = V_3;
		float L_27 = V_6;
		NullCheck(L_25);
		Vector3_t3525329789  L_28 = Transform_TransformPoint_m3094713172(L_25, L_26, L_27, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_24)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))) = L_28;
		Vector3U5BU5D_t3227571696* L_29 = __this->get_mCorners_47();
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 3);
		Transform_t284553113 * L_30 = V_7;
		float L_31 = V_5;
		float L_32 = V_2;
		NullCheck(L_30);
		Vector3_t3525329789  L_33 = Transform_TransformPoint_m3094713172(L_30, L_31, L_32, (0.0f), /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_29)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))) = L_33;
		Transform_t284553113 * L_34 = ___relativeTo0;
		bool L_35 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_34, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_012a;
		}
	}
	{
		V_8 = 0;
		goto IL_0122;
	}

IL_00f2:
	{
		Vector3U5BU5D_t3227571696* L_36 = __this->get_mCorners_47();
		int32_t L_37 = V_8;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		Transform_t284553113 * L_38 = ___relativeTo0;
		Vector3U5BU5D_t3227571696* L_39 = __this->get_mCorners_47();
		int32_t L_40 = V_8;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		NullCheck(L_38);
		Vector3_t3525329789  L_41 = Transform_InverseTransformPoint_m1626812000(L_38, (*(Vector3_t3525329789 *)((L_39)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_40)))), /*hidden argument*/NULL);
		(*(Vector3_t3525329789 *)((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_37)))) = L_41;
		int32_t L_42 = V_8;
		V_8 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_0122:
	{
		int32_t L_43 = V_8;
		if ((((int32_t)L_43) < ((int32_t)4)))
		{
			goto IL_00f2;
		}
	}

IL_012a:
	{
		Vector3U5BU5D_t3227571696* L_44 = __this->get_mCorners_47();
		return L_44;
	}
}
// System.Single UIWidget::CalculateFinalAlpha(System.Int32)
extern "C"  float UIWidget_CalculateFinalAlpha_m3596038484 (UIWidget_t769069560 * __this, int32_t ___frameID0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mAlphaFrameID_48();
		int32_t L_1 = ___frameID0;
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_2 = ___frameID0;
		__this->set_mAlphaFrameID_48(L_2);
		int32_t L_3 = ___frameID0;
		UIWidget_UpdateFinalAlpha_m4090184323(__this, L_3, /*hidden argument*/NULL);
	}

IL_001a:
	{
		float L_4 = ((UIRect_t2503437976 *)__this)->get_finalAlpha_20();
		return L_4;
	}
}
// System.Void UIWidget::UpdateFinalAlpha(System.Int32)
extern "C"  void UIWidget_UpdateFinalAlpha_m4090184323 (UIWidget_t769069560 * __this, int32_t ___frameID0, const MethodInfo* method)
{
	UIRect_t2503437976 * V_0 = NULL;
	UIWidget_t769069560 * G_B5_0 = NULL;
	UIWidget_t769069560 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	UIWidget_t769069560 * G_B6_1 = NULL;
	{
		bool L_0 = __this->get_mIsVisibleByAlpha_41();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_mIsInFront_43();
		if (L_1)
		{
			goto IL_0026;
		}
	}

IL_0016:
	{
		((UIRect_t2503437976 *)__this)->set_finalAlpha_20((0.0f));
		goto IL_0062;
	}

IL_0026:
	{
		UIRect_t2503437976 * L_2 = UIRect_get_parent_m2985047705(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		UIRect_t2503437976 * L_3 = V_0;
		bool L_4 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_3, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		G_B4_0 = __this;
		if (!L_4)
		{
			G_B5_0 = __this;
			goto IL_0052;
		}
	}
	{
		UIRect_t2503437976 * L_5 = V_0;
		int32_t L_6 = ___frameID0;
		NullCheck(L_5);
		float L_7 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, L_5, L_6);
		Color_t1588175760 * L_8 = __this->get_address_of_mColor_22();
		float L_9 = L_8->get_a_3();
		G_B6_0 = ((float)((float)L_7*(float)L_9));
		G_B6_1 = G_B4_0;
		goto IL_005d;
	}

IL_0052:
	{
		Color_t1588175760 * L_10 = __this->get_address_of_mColor_22();
		float L_11 = L_10->get_a_3();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
	}

IL_005d:
	{
		NullCheck(G_B6_1);
		((UIRect_t2503437976 *)G_B6_1)->set_finalAlpha_20(G_B6_0);
	}

IL_0062:
	{
		return;
	}
}
// System.Void UIWidget::Invalidate(System.Boolean)
extern "C"  void UIWidget_Invalidate_m466609203 (UIWidget_t769069560 * __this, bool ___includeChildren0, const MethodInfo* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		__this->set_mAlphaFrameID_48((-1));
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007f;
		}
	}
	{
		bool L_2 = __this->get_hideIfOffScreen_31();
		if (L_2)
		{
			goto IL_003a;
		}
	}
	{
		UIPanel_t295209936 * L_3 = __this->get_panel_35();
		NullCheck(L_3);
		bool L_4 = UIPanel_get_hasCumulativeClipping_m3545993459(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}

IL_003a:
	{
		UIPanel_t295209936 * L_5 = __this->get_panel_35();
		NullCheck(L_5);
		bool L_6 = UIPanel_IsVisible_m3718013533(L_5, __this, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_6));
		goto IL_004c;
	}

IL_004b:
	{
		G_B5_0 = 1;
	}

IL_004c:
	{
		V_0 = (bool)G_B5_0;
		int32_t L_7 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_8 = UIWidget_CalculateCumulativeAlpha_m2518513923(__this, L_7, /*hidden argument*/NULL);
		bool L_9 = V_0;
		UIWidget_UpdateVisibility_m143185718(__this, (bool)((((float)L_8) > ((float)(0.001f)))? 1 : 0), L_9, /*hidden argument*/NULL);
		int32_t L_10 = Time_get_frameCount_m3434184975(NULL /*static, unused*/, /*hidden argument*/NULL);
		UIWidget_UpdateFinalAlpha_m4090184323(__this, L_10, /*hidden argument*/NULL);
		bool L_11 = ___includeChildren0;
		if (!L_11)
		{
			goto IL_007f;
		}
	}
	{
		UIRect_Invalidate_m3602426579(__this, (bool)1, /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Single UIWidget::CalculateCumulativeAlpha(System.Int32)
extern "C"  float UIWidget_CalculateCumulativeAlpha_m2518513923 (UIWidget_t769069560 * __this, int32_t ___frameID0, const MethodInfo* method)
{
	UIRect_t2503437976 * V_0 = NULL;
	float G_B3_0 = 0.0f;
	{
		UIRect_t2503437976 * L_0 = UIRect_get_parent_m2985047705(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		UIRect_t2503437976 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002b;
		}
	}
	{
		UIRect_t2503437976 * L_3 = V_0;
		int32_t L_4 = ___frameID0;
		NullCheck(L_3);
		float L_5 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIRect::CalculateFinalAlpha(System.Int32) */, L_3, L_4);
		Color_t1588175760 * L_6 = __this->get_address_of_mColor_22();
		float L_7 = L_6->get_a_3();
		G_B3_0 = ((float)((float)L_5*(float)L_7));
		goto IL_0036;
	}

IL_002b:
	{
		Color_t1588175760 * L_8 = __this->get_address_of_mColor_22();
		float L_9 = L_8->get_a_3();
		G_B3_0 = L_9;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UIWidget::SetRect(System.Single,System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_SetRect_m2259467731_MetadataUsageId;
extern "C"  void UIWidget_SetRect_m2259467731 (UIWidget_t769069560 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_SetRect_m2259467731_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t3525329788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Transform_t284553113 * V_5 = NULL;
	Vector3_t3525329789  V_6;
	memset(&V_6, 0, sizeof(V_6));
	{
		Vector2_t3525329788  L_0 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ___x0;
		float L_2 = ___x0;
		float L_3 = ___width2;
		float L_4 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_5 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_1, ((float)((float)L_2+(float)L_3)), L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = ___y1;
		float L_7 = ___y1;
		float L_8 = ___height3;
		float L_9 = (&V_0)->get_y_2();
		float L_10 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_6, ((float)((float)L_7+(float)L_8)), L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = ___width2;
		int32_t L_12 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)L_11+(float)(0.5f))), /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = ___height3;
		int32_t L_14 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)L_13+(float)(0.5f))), /*hidden argument*/NULL);
		V_4 = L_14;
		float L_15 = (&V_0)->get_x_1();
		if ((!(((float)L_15) == ((float)(0.5f)))))
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_16 = V_3;
		V_3 = ((int32_t)((int32_t)((int32_t)((int32_t)L_16>>(int32_t)1))<<(int32_t)1));
	}

IL_005d:
	{
		float L_17 = (&V_0)->get_y_2();
		if ((!(((float)L_17) == ((float)(0.5f)))))
		{
			goto IL_0076;
		}
	}
	{
		int32_t L_18 = V_4;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)L_18>>(int32_t)1))<<(int32_t)1));
	}

IL_0076:
	{
		Transform_t284553113 * L_19 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_5 = L_19;
		Transform_t284553113 * L_20 = V_5;
		NullCheck(L_20);
		Vector3_t3525329789  L_21 = Transform_get_localPosition_m668140784(L_20, /*hidden argument*/NULL);
		V_6 = L_21;
		float L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_23 = floorf(((float)((float)L_22+(float)(0.5f))));
		(&V_6)->set_x_1(L_23);
		float L_24 = V_2;
		float L_25 = floorf(((float)((float)L_24+(float)(0.5f))));
		(&V_6)->set_y_2(L_25);
		int32_t L_26 = V_3;
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		if ((((int32_t)L_26) >= ((int32_t)L_27)))
		{
			goto IL_00c0;
		}
	}
	{
		int32_t L_28 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_3 = L_28;
	}

IL_00c0:
	{
		int32_t L_29 = V_4;
		int32_t L_30 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		if ((((int32_t)L_29) >= ((int32_t)L_30)))
		{
			goto IL_00d5;
		}
	}
	{
		int32_t L_31 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_4 = L_31;
	}

IL_00d5:
	{
		Transform_t284553113 * L_32 = V_5;
		Vector3_t3525329789  L_33 = V_6;
		NullCheck(L_32);
		Transform_set_localPosition_m3504330903(L_32, L_33, /*hidden argument*/NULL);
		int32_t L_34 = V_3;
		UIWidget_set_width_m3480390811(__this, L_34, /*hidden argument*/NULL);
		int32_t L_35 = V_4;
		UIWidget_set_height_m1838784918(__this, L_35, /*hidden argument*/NULL);
		bool L_36 = UIRect_get_isAnchored_m3752430236(__this, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0192;
		}
	}
	{
		Transform_t284553113 * L_37 = V_5;
		NullCheck(L_37);
		Transform_t284553113 * L_38 = Transform_get_parent_m2236876972(L_37, /*hidden argument*/NULL);
		V_5 = L_38;
		AnchorPoint_t109622203 * L_39 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_39);
		Transform_t284553113 * L_40 = L_39->get_target_0();
		bool L_41 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0124;
		}
	}
	{
		AnchorPoint_t109622203 * L_42 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t284553113 * L_43 = V_5;
		float L_44 = ___x0;
		NullCheck(L_42);
		AnchorPoint_SetHorizontal_m2786298215(L_42, L_43, L_44, /*hidden argument*/NULL);
	}

IL_0124:
	{
		AnchorPoint_t109622203 * L_45 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_45);
		Transform_t284553113 * L_46 = L_45->get_target_0();
		bool L_47 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		if (!L_47)
		{
			goto IL_0149;
		}
	}
	{
		AnchorPoint_t109622203 * L_48 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		Transform_t284553113 * L_49 = V_5;
		float L_50 = ___x0;
		float L_51 = ___width2;
		NullCheck(L_48);
		AnchorPoint_SetHorizontal_m2786298215(L_48, L_49, ((float)((float)L_50+(float)L_51)), /*hidden argument*/NULL);
	}

IL_0149:
	{
		AnchorPoint_t109622203 * L_52 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_52);
		Transform_t284553113 * L_53 = L_52->get_target_0();
		bool L_54 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_53, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_016c;
		}
	}
	{
		AnchorPoint_t109622203 * L_55 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		Transform_t284553113 * L_56 = V_5;
		float L_57 = ___y1;
		NullCheck(L_55);
		AnchorPoint_SetVertical_m4045095893(L_55, L_56, L_57, /*hidden argument*/NULL);
	}

IL_016c:
	{
		AnchorPoint_t109622203 * L_58 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_58);
		Transform_t284553113 * L_59 = L_58->get_target_0();
		bool L_60 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		if (!L_60)
		{
			goto IL_0192;
		}
	}
	{
		AnchorPoint_t109622203 * L_61 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		Transform_t284553113 * L_62 = V_5;
		float L_63 = ___y1;
		float L_64 = ___height3;
		NullCheck(L_61);
		AnchorPoint_SetVertical_m4045095893(L_61, L_62, ((float)((float)L_63+(float)L_64)), /*hidden argument*/NULL);
	}

IL_0192:
	{
		return;
	}
}
// System.Void UIWidget::ResizeCollider()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_ResizeCollider_m654642185_MetadataUsageId;
extern "C"  void UIWidget_ResizeCollider_m654642185 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_ResizeCollider_m654642185_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_0 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_UpdateWidgetCollider_m2709673216(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		return;
	}
}
// System.Int32 UIWidget::FullCompareFunc(UIWidget,UIWidget)
extern Il2CppClass* UIPanel_t295209936_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_FullCompareFunc_m3896523897_MetadataUsageId;
extern "C"  int32_t UIWidget_FullCompareFunc_m3896523897 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___left0, UIWidget_t769069560 * ___right1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_FullCompareFunc_m3896523897_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		UIWidget_t769069560 * L_0 = ___left0;
		NullCheck(L_0);
		UIPanel_t295209936 * L_1 = L_0->get_panel_35();
		UIWidget_t769069560 * L_2 = ___right1;
		NullCheck(L_2);
		UIPanel_t295209936 * L_3 = L_2->get_panel_35();
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t295209936_il2cpp_TypeInfo_var);
		int32_t L_4 = UIPanel_CompareFunc_m951703214(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if (L_5)
		{
			goto IL_0024;
		}
	}
	{
		UIWidget_t769069560 * L_6 = ___left0;
		UIWidget_t769069560 * L_7 = ___right1;
		int32_t L_8 = UIWidget_PanelCompareFunc_m1938899026(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		G_B3_0 = L_8;
		goto IL_0025;
	}

IL_0024:
	{
		int32_t L_9 = V_0;
		G_B3_0 = L_9;
	}

IL_0025:
	{
		return G_B3_0;
	}
}
// System.Int32 UIWidget::PanelCompareFunc(UIWidget,UIWidget)
extern "C"  int32_t UIWidget_PanelCompareFunc_m1938899026 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___left0, UIWidget_t769069560 * ___right1, const MethodInfo* method)
{
	Material_t1886596500 * V_0 = NULL;
	Material_t1886596500 * V_1 = NULL;
	int32_t G_B13_0 = 0;
	{
		UIWidget_t769069560 * L_0 = ___left0;
		NullCheck(L_0);
		int32_t L_1 = L_0->get_mDepth_26();
		UIWidget_t769069560 * L_2 = ___right1;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_mDepth_26();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0013;
		}
	}
	{
		return (-1);
	}

IL_0013:
	{
		UIWidget_t769069560 * L_4 = ___left0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_mDepth_26();
		UIWidget_t769069560 * L_6 = ___right1;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_mDepth_26();
		if ((((int32_t)L_5) <= ((int32_t)L_7)))
		{
			goto IL_0026;
		}
	}
	{
		return 1;
	}

IL_0026:
	{
		UIWidget_t769069560 * L_8 = ___left0;
		NullCheck(L_8);
		Material_t1886596500 * L_9 = VirtFuncInvoker0< Material_t1886596500 * >::Invoke(25 /* UnityEngine.Material UIWidget::get_material() */, L_8);
		V_0 = L_9;
		UIWidget_t769069560 * L_10 = ___right1;
		NullCheck(L_10);
		Material_t1886596500 * L_11 = VirtFuncInvoker0< Material_t1886596500 * >::Invoke(25 /* UnityEngine.Material UIWidget::get_material() */, L_10);
		V_1 = L_11;
		Material_t1886596500 * L_12 = V_0;
		Material_t1886596500 * L_13 = V_1;
		bool L_14 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0042;
		}
	}
	{
		return 0;
	}

IL_0042:
	{
		Material_t1886596500 * L_15 = V_0;
		bool L_16 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_15, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0050;
		}
	}
	{
		return 1;
	}

IL_0050:
	{
		Material_t1886596500 * L_17 = V_1;
		bool L_18 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_17, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_005e;
		}
	}
	{
		return (-1);
	}

IL_005e:
	{
		Material_t1886596500 * L_19 = V_0;
		NullCheck(L_19);
		int32_t L_20 = Object_GetInstanceID_m200424466(L_19, /*hidden argument*/NULL);
		Material_t1886596500 * L_21 = V_1;
		NullCheck(L_21);
		int32_t L_22 = Object_GetInstanceID_m200424466(L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_20) >= ((int32_t)L_22)))
		{
			goto IL_0075;
		}
	}
	{
		G_B13_0 = (-1);
		goto IL_0076;
	}

IL_0075:
	{
		G_B13_0 = 1;
	}

IL_0076:
	{
		return G_B13_0;
	}
}
// UnityEngine.Bounds UIWidget::CalculateBounds()
extern "C"  Bounds_t3518514978  UIWidget_CalculateBounds_m2567301067 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Bounds_t3518514978  L_0 = UIWidget_CalculateBounds_m1943782482(__this, (Transform_t284553113 *)NULL, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Bounds UIWidget::CalculateBounds(UnityEngine.Transform)
extern "C"  Bounds_t3518514978  UIWidget_CalculateBounds_m1943782482 (UIWidget_t769069560 * __this, Transform_t284553113 * ___relativeParent0, const MethodInfo* method)
{
	Vector3U5BU5D_t3227571696* V_0 = NULL;
	Bounds_t3518514978  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Matrix4x4_t277289660  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3U5BU5D_t3227571696* V_4 = NULL;
	Bounds_t3518514978  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	{
		Transform_t284553113 * L_0 = ___relativeParent0;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_2 = VirtFuncInvoker0< Vector3U5BU5D_t3227571696* >::Invoke(10 /* UnityEngine.Vector3[] UIWidget::get_localCorners() */, __this);
		V_0 = L_2;
		Vector3U5BU5D_t3227571696* L_3 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 0);
		Vector3_t3525329789  L_4 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds__ctor_m4160293652((&V_1), (*(Vector3_t3525329789 *)((L_3)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), L_4, /*hidden argument*/NULL);
		V_2 = 1;
		goto IL_0049;
	}

IL_0032:
	{
		Vector3U5BU5D_t3227571696* L_5 = V_0;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		Bounds_Encapsulate_m3624685234((&V_1), (*(Vector3_t3525329789 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)))), /*hidden argument*/NULL);
		int32_t L_7 = V_2;
		V_2 = ((int32_t)((int32_t)L_7+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_8 = V_2;
		if ((((int32_t)L_8) < ((int32_t)4)))
		{
			goto IL_0032;
		}
	}
	{
		Bounds_t3518514978  L_9 = V_1;
		return L_9;
	}

IL_0052:
	{
		Transform_t284553113 * L_10 = ___relativeParent0;
		NullCheck(L_10);
		Matrix4x4_t277289660  L_11 = Transform_get_worldToLocalMatrix_m3792395652(L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		Vector3U5BU5D_t3227571696* L_12 = VirtFuncInvoker0< Vector3U5BU5D_t3227571696* >::Invoke(11 /* UnityEngine.Vector3[] UIWidget::get_worldCorners() */, __this);
		V_4 = L_12;
		Vector3U5BU5D_t3227571696* L_13 = V_4;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		Vector3_t3525329789  L_14 = Matrix4x4_MultiplyPoint3x4_m2198174902((&V_3), (*(Vector3_t3525329789 *)((L_13)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), /*hidden argument*/NULL);
		Vector3_t3525329789  L_15 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		Bounds__ctor_m4160293652((&V_5), L_14, L_15, /*hidden argument*/NULL);
		V_6 = 1;
		goto IL_00ab;
	}

IL_0089:
	{
		Vector3U5BU5D_t3227571696* L_16 = V_4;
		int32_t L_17 = V_6;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		Vector3_t3525329789  L_18 = Matrix4x4_MultiplyPoint3x4_m2198174902((&V_3), (*(Vector3_t3525329789 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17)))), /*hidden argument*/NULL);
		Bounds_Encapsulate_m3624685234((&V_5), L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_6;
		V_6 = ((int32_t)((int32_t)L_19+(int32_t)1));
	}

IL_00ab:
	{
		int32_t L_20 = V_6;
		if ((((int32_t)L_20) < ((int32_t)4)))
		{
			goto IL_0089;
		}
	}
	{
		Bounds_t3518514978  L_21 = V_5;
		return L_21;
	}
}
// System.Void UIWidget::SetDirty()
extern "C"  void UIWidget_SetDirty_m3979225553 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIDrawCall_t913273974 * L_0 = __this->get_drawCall_46();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		UIDrawCall_t913273974 * L_2 = __this->get_drawCall_46();
		NullCheck(L_2);
		L_2->set_isDirty_31((bool)1);
		goto IL_003f;
	}

IL_0022:
	{
		bool L_3 = UIWidget_get_isVisible_m4257222444(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003f;
		}
	}
	{
		bool L_4 = UIWidget_get_hasVertices_m262926455(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003f;
		}
	}
	{
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
	}

IL_003f:
	{
		return;
	}
}
// System.Void UIWidget::RemoveFromPanel()
extern "C"  void UIWidget_RemoveFromPanel_m3233223575 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		UIPanel_t295209936 * L_2 = __this->get_panel_35();
		NullCheck(L_2);
		UIPanel_RemoveWidget_m3627052857(L_2, __this, /*hidden argument*/NULL);
		__this->set_panel_35((UIPanel_t295209936 *)NULL);
	}

IL_0024:
	{
		__this->set_drawCall_46((UIDrawCall_t913273974 *)NULL);
		return;
	}
}
// System.Void UIWidget::MarkAsChanged()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_MarkAsChanged_m3551758678_MetadataUsageId;
extern "C"  void UIWidget_MarkAsChanged_m3551758678 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_MarkAsChanged_m3551758678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_0 = NGUITools_GetActive_m4064435393(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0055;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		UIPanel_t295209936 * L_1 = __this->get_panel_35();
		bool L_2 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0055;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0055;
		}
	}
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_5 = NGUITools_GetActive_m3605198179(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0055;
		}
	}
	{
		bool L_6 = __this->get_mPlayMode_38();
		if (L_6)
		{
			goto IL_0055;
		}
	}
	{
		UIWidget_SetDirty_m3979225553(__this, /*hidden argument*/NULL);
		UIWidget_CheckLayer_m388028458(__this, /*hidden argument*/NULL);
	}

IL_0055:
	{
		return;
	}
}
// UIPanel UIWidget::CreatePanel()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern Il2CppClass* UIPanel_t295209936_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_CreatePanel_m851962502_MetadataUsageId;
extern "C"  UIPanel_t295209936 * UIWidget_CreatePanel_m851962502 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CreatePanel_m851962502_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ((UIRect_t2503437976 *)__this)->get_mStarted_19();
		if (!L_0)
		{
			goto IL_0085;
		}
	}
	{
		UIPanel_t295209936 * L_1 = __this->get_panel_35();
		bool L_2 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_1, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0085;
		}
	}
	{
		bool L_3 = Behaviour_get_enabled_m1239363704(__this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0085;
		}
	}
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		bool L_5 = NGUITools_GetActive_m3605198179(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0085;
		}
	}
	{
		Transform_t284553113 * L_6 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = UIRect_get_cachedGameObject_m418520850(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = GameObject_get_layer_m1648550306(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t295209936_il2cpp_TypeInfo_var);
		UIPanel_t295209936 * L_9 = UIPanel_Find_m1125240312(NULL /*static, unused*/, L_6, (bool)1, L_8, /*hidden argument*/NULL);
		__this->set_panel_35(L_9);
		UIPanel_t295209936 * L_10 = __this->get_panel_35();
		bool L_11 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_10, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0085;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mParentFound_11((bool)0);
		UIPanel_t295209936 * L_12 = __this->get_panel_35();
		NullCheck(L_12);
		UIPanel_AddWidget_m1579877318(L_12, __this, /*hidden argument*/NULL);
		UIWidget_CheckLayer_m388028458(__this, /*hidden argument*/NULL);
		VirtActionInvoker1< bool >::Invoke(12 /* System.Void UIWidget::Invalidate(System.Boolean) */, __this, (bool)1);
	}

IL_0085:
	{
		UIPanel_t295209936 * L_13 = __this->get_panel_35();
		return L_13;
	}
}
// System.Void UIWidget::CheckLayer()
extern Il2CppClass* Debug_t1588791936_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral427372784;
extern const uint32_t UIWidget_CheckLayer_m388028458_MetadataUsageId;
extern "C"  void UIWidget_CheckLayer_m388028458 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_CheckLayer_m388028458_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0057;
		}
	}
	{
		UIPanel_t295209936 * L_2 = __this->get_panel_35();
		NullCheck(L_2);
		GameObject_t4012695102 * L_3 = Component_get_gameObject_m1170635899(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m1648550306(L_3, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = GameObject_get_layer_m1648550306(L_5, /*hidden argument*/NULL);
		if ((((int32_t)L_4) == ((int32_t)L_6)))
		{
			goto IL_0057;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1588791936_il2cpp_TypeInfo_var);
		Debug_LogWarning_m4097176146(NULL /*static, unused*/, _stringLiteral427372784, __this, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		UIPanel_t295209936 * L_8 = __this->get_panel_35();
		NullCheck(L_8);
		GameObject_t4012695102 * L_9 = Component_get_gameObject_m1170635899(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = GameObject_get_layer_m1648550306(L_9, /*hidden argument*/NULL);
		NullCheck(L_7);
		GameObject_set_layer_m1872241535(L_7, L_10, /*hidden argument*/NULL);
	}

IL_0057:
	{
		return;
	}
}
// System.Void UIWidget::ParentHasChanged()
extern Il2CppClass* UIPanel_t295209936_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_ParentHasChanged_m3250288453_MetadataUsageId;
extern "C"  void UIWidget_ParentHasChanged_m3250288453 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_ParentHasChanged_m3250288453_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	UIPanel_t295209936 * V_0 = NULL;
	{
		UIRect_ParentHasChanged_m2258951653(__this, /*hidden argument*/NULL);
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		Transform_t284553113 * L_2 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = UIRect_get_cachedGameObject_m418520850(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = GameObject_get_layer_m1648550306(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UIPanel_t295209936_il2cpp_TypeInfo_var);
		UIPanel_t295209936 * L_5 = UIPanel_Find_m1125240312(NULL /*static, unused*/, L_2, (bool)1, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		UIPanel_t295209936 * L_6 = __this->get_panel_35();
		UIPanel_t295209936 * L_7 = V_0;
		bool L_8 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_004d;
		}
	}
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
	}

IL_004d:
	{
		return;
	}
}
// System.Void UIWidget::Awake()
extern "C"  void UIWidget_Awake_m308016390 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIRect_Awake_m1297117798(__this, /*hidden argument*/NULL);
		bool L_0 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mPlayMode_38(L_0);
		return;
	}
}
// System.Void UIWidget::OnInit()
extern "C"  void UIWidget_OnInit_m3801346320 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIRect_OnInit_m103751600(__this, /*hidden argument*/NULL);
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		__this->set_mMoved_45((bool)1);
		UIRect_Update_m212013674(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::UpgradeFrom265()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_UpgradeFrom265_m3823217484_MetadataUsageId;
extern "C"  void UIWidget_UpgradeFrom265_m3823217484 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_UpgradeFrom265_m3823217484_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_localScale_m3886572677(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		int32_t L_4 = Mathf_Abs_m4265466780(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_mWidth_24(L_4);
		float L_5 = (&V_0)->get_y_2();
		int32_t L_6 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		int32_t L_7 = Mathf_Abs_m4265466780(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_mHeight_25(L_7);
		GameObject_t4012695102 * L_8 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_UpdateWidgetCollider_m1680432765(NULL /*static, unused*/, L_8, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnStart()
extern "C"  void UIWidget_OnStart_m2327121348 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnAnchor()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_OnAnchor_m1133360565_MetadataUsageId;
extern "C"  void UIWidget_OnAnchor_m1133360565 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_OnAnchor_m1133360565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	Transform_t284553113 * V_4 = NULL;
	Transform_t284553113 * V_5 = NULL;
	Vector3_t3525329789  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector2_t3525329788  V_7;
	memset(&V_7, 0, sizeof(V_7));
	Vector3U5BU5D_t3227571696* V_8 = NULL;
	Vector3_t3525329789  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Vector3U5BU5D_t3227571696* V_10 = NULL;
	Vector3U5BU5D_t3227571696* V_11 = NULL;
	Vector3U5BU5D_t3227571696* V_12 = NULL;
	Vector3U5BU5D_t3227571696* V_13 = NULL;
	Vector3_t3525329789  V_14;
	memset(&V_14, 0, sizeof(V_14));
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	Vector3_t3525329789  V_17;
	memset(&V_17, 0, sizeof(V_17));
	Vector3_t3525329789  V_18;
	memset(&V_18, 0, sizeof(V_18));
	Vector3_t3525329789  V_19;
	memset(&V_19, 0, sizeof(V_19));
	Vector3_t3525329789  V_20;
	memset(&V_20, 0, sizeof(V_20));
	UIWidget_t769069560 * G_B7_0 = NULL;
	UIWidget_t769069560 * G_B6_0 = NULL;
	int32_t G_B8_0 = 0;
	UIWidget_t769069560 * G_B8_1 = NULL;
	{
		Transform_t284553113 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_4 = L_0;
		Transform_t284553113 * L_1 = V_4;
		NullCheck(L_1);
		Transform_t284553113 * L_2 = Transform_get_parent_m2236876972(L_1, /*hidden argument*/NULL);
		V_5 = L_2;
		Transform_t284553113 * L_3 = V_4;
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_localPosition_m668140784(L_3, /*hidden argument*/NULL);
		V_6 = L_4;
		Vector2_t3525329788  L_5 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_7 = L_5;
		AnchorPoint_t109622203 * L_6 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_6);
		Transform_t284553113 * L_7 = L_6->get_target_0();
		AnchorPoint_t109622203 * L_8 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_8);
		Transform_t284553113 * L_9 = L_8->get_target_0();
		bool L_10 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t109622203 * L_11 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_11);
		Transform_t284553113 * L_12 = L_11->get_target_0();
		AnchorPoint_t109622203 * L_13 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_13);
		Transform_t284553113 * L_14 = L_13->get_target_0();
		bool L_15 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t109622203 * L_16 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_16);
		Transform_t284553113 * L_17 = L_16->get_target_0();
		AnchorPoint_t109622203 * L_18 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_18);
		Transform_t284553113 * L_19 = L_18->get_target_0();
		bool L_20 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0212;
		}
	}
	{
		AnchorPoint_t109622203 * L_21 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t284553113 * L_22 = V_5;
		NullCheck(L_21);
		Vector3U5BU5D_t3227571696* L_23 = AnchorPoint_GetSides_m1119345522(L_21, L_22, /*hidden argument*/NULL);
		V_8 = L_23;
		Vector3U5BU5D_t3227571696* L_24 = V_8;
		if (!L_24)
		{
			goto IL_0184;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_25 = V_8;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, 0);
		float L_26 = ((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t3227571696* L_27 = V_8;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 2);
		float L_28 = ((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t109622203 * L_29 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_29);
		float L_30 = L_29->get_relative_1();
		float L_31 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_26, L_28, L_30, /*hidden argument*/NULL);
		AnchorPoint_t109622203 * L_32 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_32);
		int32_t L_33 = L_32->get_absolute_2();
		V_0 = ((float)((float)L_31+(float)(((float)((float)L_33)))));
		Vector3U5BU5D_t3227571696* L_34 = V_8;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 0);
		float L_35 = ((L_34)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t3227571696* L_36 = V_8;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 2);
		float L_37 = ((L_36)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t109622203 * L_38 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_38);
		float L_39 = L_38->get_relative_1();
		float L_40 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_35, L_37, L_39, /*hidden argument*/NULL);
		AnchorPoint_t109622203 * L_41 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_41);
		int32_t L_42 = L_41->get_absolute_2();
		V_2 = ((float)((float)L_40+(float)(((float)((float)L_42)))));
		Vector3U5BU5D_t3227571696* L_43 = V_8;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, 3);
		float L_44 = ((L_43)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t3227571696* L_45 = V_8;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 1);
		float L_46 = ((L_45)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t109622203 * L_47 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_47);
		float L_48 = L_47->get_relative_1();
		float L_49 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_44, L_46, L_48, /*hidden argument*/NULL);
		AnchorPoint_t109622203 * L_50 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_50);
		int32_t L_51 = L_50->get_absolute_2();
		V_1 = ((float)((float)L_49+(float)(((float)((float)L_51)))));
		Vector3U5BU5D_t3227571696* L_52 = V_8;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, 3);
		float L_53 = ((L_52)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t3227571696* L_54 = V_8;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 1);
		float L_55 = ((L_54)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t109622203 * L_56 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_56);
		float L_57 = L_56->get_relative_1();
		float L_58 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_53, L_55, L_57, /*hidden argument*/NULL);
		AnchorPoint_t109622203 * L_59 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_59);
		int32_t L_60 = L_59->get_absolute_2();
		V_3 = ((float)((float)L_58+(float)(((float)((float)L_60)))));
		__this->set_mIsInFront_43((bool)1);
		goto IL_020d;
	}

IL_0184:
	{
		AnchorPoint_t109622203 * L_61 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t284553113 * L_62 = V_5;
		Vector3_t3525329789  L_63 = UIRect_GetLocalPos_m763968249(__this, L_61, L_62, /*hidden argument*/NULL);
		V_9 = L_63;
		float L_64 = (&V_9)->get_x_1();
		AnchorPoint_t109622203 * L_65 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_65);
		int32_t L_66 = L_65->get_absolute_2();
		V_0 = ((float)((float)L_64+(float)(((float)((float)L_66)))));
		float L_67 = (&V_9)->get_y_2();
		AnchorPoint_t109622203 * L_68 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_68);
		int32_t L_69 = L_68->get_absolute_2();
		V_1 = ((float)((float)L_67+(float)(((float)((float)L_69)))));
		float L_70 = (&V_9)->get_x_1();
		AnchorPoint_t109622203 * L_71 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_71);
		int32_t L_72 = L_71->get_absolute_2();
		V_2 = ((float)((float)L_70+(float)(((float)((float)L_72)))));
		float L_73 = (&V_9)->get_y_2();
		AnchorPoint_t109622203 * L_74 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_74);
		int32_t L_75 = L_74->get_absolute_2();
		V_3 = ((float)((float)L_73+(float)(((float)((float)L_75)))));
		bool L_76 = __this->get_hideIfOffScreen_31();
		G_B6_0 = __this;
		if (!L_76)
		{
			G_B7_0 = __this;
			goto IL_0207;
		}
	}
	{
		float L_77 = (&V_9)->get_z_3();
		G_B8_0 = ((((int32_t)((!(((float)L_77) >= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		G_B8_1 = G_B6_0;
		goto IL_0208;
	}

IL_0207:
	{
		G_B8_0 = 1;
		G_B8_1 = G_B7_0;
	}

IL_0208:
	{
		NullCheck(G_B8_1);
		G_B8_1->set_mIsInFront_43((bool)G_B8_0);
	}

IL_020d:
	{
		goto IL_04d1;
	}

IL_0212:
	{
		__this->set_mIsInFront_43((bool)1);
		AnchorPoint_t109622203 * L_78 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_78);
		Transform_t284553113 * L_79 = L_78->get_target_0();
		bool L_80 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_79, /*hidden argument*/NULL);
		if (!L_80)
		{
			goto IL_02ab;
		}
	}
	{
		AnchorPoint_t109622203 * L_81 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t284553113 * L_82 = V_5;
		NullCheck(L_81);
		Vector3U5BU5D_t3227571696* L_83 = AnchorPoint_GetSides_m1119345522(L_81, L_82, /*hidden argument*/NULL);
		V_10 = L_83;
		Vector3U5BU5D_t3227571696* L_84 = V_10;
		if (!L_84)
		{
			goto IL_0281;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_85 = V_10;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, 0);
		float L_86 = ((L_85)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t3227571696* L_87 = V_10;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, 2);
		float L_88 = ((L_87)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t109622203 * L_89 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_89);
		float L_90 = L_89->get_relative_1();
		float L_91 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_86, L_88, L_90, /*hidden argument*/NULL);
		AnchorPoint_t109622203 * L_92 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_92);
		int32_t L_93 = L_92->get_absolute_2();
		V_0 = ((float)((float)L_91+(float)(((float)((float)L_93)))));
		goto IL_02a6;
	}

IL_0281:
	{
		AnchorPoint_t109622203 * L_94 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		Transform_t284553113 * L_95 = V_5;
		Vector3_t3525329789  L_96 = UIRect_GetLocalPos_m763968249(__this, L_94, L_95, /*hidden argument*/NULL);
		V_17 = L_96;
		float L_97 = (&V_17)->get_x_1();
		AnchorPoint_t109622203 * L_98 = ((UIRect_t2503437976 *)__this)->get_leftAnchor_2();
		NullCheck(L_98);
		int32_t L_99 = L_98->get_absolute_2();
		V_0 = ((float)((float)L_97+(float)(((float)((float)L_99)))));
	}

IL_02a6:
	{
		goto IL_02c3;
	}

IL_02ab:
	{
		float L_100 = (&V_6)->get_x_1();
		float L_101 = (&V_7)->get_x_1();
		int32_t L_102 = __this->get_mWidth_24();
		V_0 = ((float)((float)L_100-(float)((float)((float)L_101*(float)(((float)((float)L_102)))))));
	}

IL_02c3:
	{
		AnchorPoint_t109622203 * L_103 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_103);
		Transform_t284553113 * L_104 = L_103->get_target_0();
		bool L_105 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_104, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_0355;
		}
	}
	{
		AnchorPoint_t109622203 * L_106 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		Transform_t284553113 * L_107 = V_5;
		NullCheck(L_106);
		Vector3U5BU5D_t3227571696* L_108 = AnchorPoint_GetSides_m1119345522(L_106, L_107, /*hidden argument*/NULL);
		V_11 = L_108;
		Vector3U5BU5D_t3227571696* L_109 = V_11;
		if (!L_109)
		{
			goto IL_032b;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_110 = V_11;
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, 0);
		float L_111 = ((L_110)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		Vector3U5BU5D_t3227571696* L_112 = V_11;
		NullCheck(L_112);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_112, 2);
		float L_113 = ((L_112)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		AnchorPoint_t109622203 * L_114 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_114);
		float L_115 = L_114->get_relative_1();
		float L_116 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_111, L_113, L_115, /*hidden argument*/NULL);
		AnchorPoint_t109622203 * L_117 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_117);
		int32_t L_118 = L_117->get_absolute_2();
		V_2 = ((float)((float)L_116+(float)(((float)((float)L_118)))));
		goto IL_0350;
	}

IL_032b:
	{
		AnchorPoint_t109622203 * L_119 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		Transform_t284553113 * L_120 = V_5;
		Vector3_t3525329789  L_121 = UIRect_GetLocalPos_m763968249(__this, L_119, L_120, /*hidden argument*/NULL);
		V_18 = L_121;
		float L_122 = (&V_18)->get_x_1();
		AnchorPoint_t109622203 * L_123 = ((UIRect_t2503437976 *)__this)->get_rightAnchor_3();
		NullCheck(L_123);
		int32_t L_124 = L_123->get_absolute_2();
		V_2 = ((float)((float)L_122+(float)(((float)((float)L_124)))));
	}

IL_0350:
	{
		goto IL_0375;
	}

IL_0355:
	{
		float L_125 = (&V_6)->get_x_1();
		float L_126 = (&V_7)->get_x_1();
		int32_t L_127 = __this->get_mWidth_24();
		int32_t L_128 = __this->get_mWidth_24();
		V_2 = ((float)((float)((float)((float)L_125-(float)((float)((float)L_126*(float)(((float)((float)L_127)))))))+(float)(((float)((float)L_128)))));
	}

IL_0375:
	{
		AnchorPoint_t109622203 * L_129 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_129);
		Transform_t284553113 * L_130 = L_129->get_target_0();
		bool L_131 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_130, /*hidden argument*/NULL);
		if (!L_131)
		{
			goto IL_0407;
		}
	}
	{
		AnchorPoint_t109622203 * L_132 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		Transform_t284553113 * L_133 = V_5;
		NullCheck(L_132);
		Vector3U5BU5D_t3227571696* L_134 = AnchorPoint_GetSides_m1119345522(L_132, L_133, /*hidden argument*/NULL);
		V_12 = L_134;
		Vector3U5BU5D_t3227571696* L_135 = V_12;
		if (!L_135)
		{
			goto IL_03dd;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_136 = V_12;
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, 3);
		float L_137 = ((L_136)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t3227571696* L_138 = V_12;
		NullCheck(L_138);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_138, 1);
		float L_139 = ((L_138)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t109622203 * L_140 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_140);
		float L_141 = L_140->get_relative_1();
		float L_142 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_137, L_139, L_141, /*hidden argument*/NULL);
		AnchorPoint_t109622203 * L_143 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_143);
		int32_t L_144 = L_143->get_absolute_2();
		V_1 = ((float)((float)L_142+(float)(((float)((float)L_144)))));
		goto IL_0402;
	}

IL_03dd:
	{
		AnchorPoint_t109622203 * L_145 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		Transform_t284553113 * L_146 = V_5;
		Vector3_t3525329789  L_147 = UIRect_GetLocalPos_m763968249(__this, L_145, L_146, /*hidden argument*/NULL);
		V_19 = L_147;
		float L_148 = (&V_19)->get_y_2();
		AnchorPoint_t109622203 * L_149 = ((UIRect_t2503437976 *)__this)->get_bottomAnchor_4();
		NullCheck(L_149);
		int32_t L_150 = L_149->get_absolute_2();
		V_1 = ((float)((float)L_148+(float)(((float)((float)L_150)))));
	}

IL_0402:
	{
		goto IL_041f;
	}

IL_0407:
	{
		float L_151 = (&V_6)->get_y_2();
		float L_152 = (&V_7)->get_y_2();
		int32_t L_153 = __this->get_mHeight_25();
		V_1 = ((float)((float)L_151-(float)((float)((float)L_152*(float)(((float)((float)L_153)))))));
	}

IL_041f:
	{
		AnchorPoint_t109622203 * L_154 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_154);
		Transform_t284553113 * L_155 = L_154->get_target_0();
		bool L_156 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_155, /*hidden argument*/NULL);
		if (!L_156)
		{
			goto IL_04b1;
		}
	}
	{
		AnchorPoint_t109622203 * L_157 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		Transform_t284553113 * L_158 = V_5;
		NullCheck(L_157);
		Vector3U5BU5D_t3227571696* L_159 = AnchorPoint_GetSides_m1119345522(L_157, L_158, /*hidden argument*/NULL);
		V_13 = L_159;
		Vector3U5BU5D_t3227571696* L_160 = V_13;
		if (!L_160)
		{
			goto IL_0487;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_161 = V_13;
		NullCheck(L_161);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_161, 3);
		float L_162 = ((L_161)->GetAddressAt(static_cast<il2cpp_array_size_t>(3)))->get_y_2();
		Vector3U5BU5D_t3227571696* L_163 = V_13;
		NullCheck(L_163);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_163, 1);
		float L_164 = ((L_163)->GetAddressAt(static_cast<il2cpp_array_size_t>(1)))->get_y_2();
		AnchorPoint_t109622203 * L_165 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_165);
		float L_166 = L_165->get_relative_1();
		float L_167 = NGUIMath_Lerp_m2088444884(NULL /*static, unused*/, L_162, L_164, L_166, /*hidden argument*/NULL);
		AnchorPoint_t109622203 * L_168 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_168);
		int32_t L_169 = L_168->get_absolute_2();
		V_3 = ((float)((float)L_167+(float)(((float)((float)L_169)))));
		goto IL_04ac;
	}

IL_0487:
	{
		AnchorPoint_t109622203 * L_170 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		Transform_t284553113 * L_171 = V_5;
		Vector3_t3525329789  L_172 = UIRect_GetLocalPos_m763968249(__this, L_170, L_171, /*hidden argument*/NULL);
		V_20 = L_172;
		float L_173 = (&V_20)->get_y_2();
		AnchorPoint_t109622203 * L_174 = ((UIRect_t2503437976 *)__this)->get_topAnchor_5();
		NullCheck(L_174);
		int32_t L_175 = L_174->get_absolute_2();
		V_3 = ((float)((float)L_173+(float)(((float)((float)L_175)))));
	}

IL_04ac:
	{
		goto IL_04d1;
	}

IL_04b1:
	{
		float L_176 = (&V_6)->get_y_2();
		float L_177 = (&V_7)->get_y_2();
		int32_t L_178 = __this->get_mHeight_25();
		int32_t L_179 = __this->get_mHeight_25();
		V_3 = ((float)((float)((float)((float)L_176-(float)((float)((float)L_177*(float)(((float)((float)L_178)))))))+(float)(((float)((float)L_179)))));
	}

IL_04d1:
	{
		float L_180 = V_0;
		float L_181 = V_2;
		float L_182 = (&V_7)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_183 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_180, L_181, L_182, /*hidden argument*/NULL);
		float L_184 = V_1;
		float L_185 = V_3;
		float L_186 = (&V_7)->get_y_2();
		float L_187 = Mathf_Lerp_m3257777633(NULL /*static, unused*/, L_184, L_185, L_186, /*hidden argument*/NULL);
		float L_188 = (&V_6)->get_z_3();
		Vector3__ctor_m2926210380((&V_14), L_183, L_187, L_188, /*hidden argument*/NULL);
		float L_189 = (&V_14)->get_x_1();
		float L_190 = bankers_roundf(L_189);
		(&V_14)->set_x_1(L_190);
		float L_191 = (&V_14)->get_y_2();
		float L_192 = bankers_roundf(L_191);
		(&V_14)->set_y_2(L_192);
		float L_193 = V_2;
		float L_194 = V_0;
		int32_t L_195 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)((float)((float)L_193-(float)L_194))+(float)(0.5f))), /*hidden argument*/NULL);
		V_15 = L_195;
		float L_196 = V_3;
		float L_197 = V_1;
		int32_t L_198 = Mathf_FloorToInt_m268511322(NULL /*static, unused*/, ((float)((float)((float)((float)L_196-(float)L_197))+(float)(0.5f))), /*hidden argument*/NULL);
		V_16 = L_198;
		int32_t L_199 = __this->get_keepAspectRatio_32();
		if (!L_199)
		{
			goto IL_058f;
		}
	}
	{
		float L_200 = __this->get_aspectRatio_33();
		if ((((float)L_200) == ((float)(0.0f))))
		{
			goto IL_058f;
		}
	}
	{
		int32_t L_201 = __this->get_keepAspectRatio_32();
		if ((!(((uint32_t)L_201) == ((uint32_t)2))))
		{
			goto IL_057e;
		}
	}
	{
		int32_t L_202 = V_16;
		float L_203 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_204 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)(((float)((float)L_202)))*(float)L_203)), /*hidden argument*/NULL);
		V_15 = L_204;
		goto IL_058f;
	}

IL_057e:
	{
		int32_t L_205 = V_15;
		float L_206 = __this->get_aspectRatio_33();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_207 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)(((float)((float)L_205)))/(float)L_206)), /*hidden argument*/NULL);
		V_16 = L_207;
	}

IL_058f:
	{
		int32_t L_208 = V_15;
		int32_t L_209 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		if ((((int32_t)L_208) >= ((int32_t)L_209)))
		{
			goto IL_05a4;
		}
	}
	{
		int32_t L_210 = VirtFuncInvoker0< int32_t >::Invoke(34 /* System.Int32 UIWidget::get_minWidth() */, __this);
		V_15 = L_210;
	}

IL_05a4:
	{
		int32_t L_211 = V_16;
		int32_t L_212 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		if ((((int32_t)L_211) >= ((int32_t)L_212)))
		{
			goto IL_05b9;
		}
	}
	{
		int32_t L_213 = VirtFuncInvoker0< int32_t >::Invoke(35 /* System.Int32 UIWidget::get_minHeight() */, __this);
		V_16 = L_213;
	}

IL_05b9:
	{
		Vector3_t3525329789  L_214 = V_6;
		Vector3_t3525329789  L_215 = V_14;
		Vector3_t3525329789  L_216 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_214, L_215, /*hidden argument*/NULL);
		float L_217 = Vector3_SqrMagnitude_m1662776270(NULL /*static, unused*/, L_216, /*hidden argument*/NULL);
		if ((!(((float)L_217) > ((float)(0.001f)))))
		{
			goto IL_05f0;
		}
	}
	{
		Transform_t284553113 * L_218 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_219 = V_14;
		NullCheck(L_218);
		Transform_set_localPosition_m3504330903(L_218, L_219, /*hidden argument*/NULL);
		bool L_220 = __this->get_mIsInFront_43();
		if (!L_220)
		{
			goto IL_05f0;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
	}

IL_05f0:
	{
		int32_t L_221 = __this->get_mWidth_24();
		int32_t L_222 = V_15;
		if ((!(((uint32_t)L_221) == ((uint32_t)L_222))))
		{
			goto IL_060a;
		}
	}
	{
		int32_t L_223 = __this->get_mHeight_25();
		int32_t L_224 = V_16;
		if ((((int32_t)L_223) == ((int32_t)L_224)))
		{
			goto IL_063d;
		}
	}

IL_060a:
	{
		int32_t L_225 = V_15;
		__this->set_mWidth_24(L_225);
		int32_t L_226 = V_16;
		__this->set_mHeight_25(L_226);
		bool L_227 = __this->get_mIsInFront_43();
		if (!L_227)
		{
			goto IL_062c;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
	}

IL_062c:
	{
		bool L_228 = __this->get_autoResizeBoxCollider_30();
		if (!L_228)
		{
			goto IL_063d;
		}
	}
	{
		UIWidget_ResizeCollider_m654642185(__this, /*hidden argument*/NULL);
	}

IL_063d:
	{
		return;
	}
}
// System.Void UIWidget::OnUpdate()
extern "C"  void UIWidget_OnUpdate_m3427137225 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIPanel_t295209936 * L_0 = __this->get_panel_35();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UIWidget_CreatePanel_m851962502(__this, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// System.Void UIWidget::OnApplicationPause(System.Boolean)
extern "C"  void UIWidget_OnApplicationPause_m307910013 (UIWidget_t769069560 * __this, bool ___paused0, const MethodInfo* method)
{
	{
		bool L_0 = ___paused0;
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		VirtActionInvoker0::Invoke(31 /* System.Void UIWidget::MarkAsChanged() */, __this);
	}

IL_000c:
	{
		return;
	}
}
// System.Void UIWidget::OnDisable()
extern "C"  void UIWidget_OnDisable_m625553034 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		UIRect_OnDisable_m2902457322(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::OnDestroy()
extern "C"  void UIWidget_OnDestroy_m2803085084 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		UIWidget_RemoveFromPanel_m3233223575(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UIWidget::UpdateVisibility(System.Boolean,System.Boolean)
extern "C"  bool UIWidget_UpdateVisibility_m143185718 (UIWidget_t769069560 * __this, bool ___visibleByAlpha0, bool ___visibleByPanel1, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_mIsVisibleByAlpha_41();
		bool L_1 = ___visibleByAlpha0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0018;
		}
	}
	{
		bool L_2 = __this->get_mIsVisibleByPanel_42();
		bool L_3 = ___visibleByPanel1;
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002f;
		}
	}

IL_0018:
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
		bool L_4 = ___visibleByAlpha0;
		__this->set_mIsVisibleByAlpha_41(L_4);
		bool L_5 = ___visibleByPanel1;
		__this->set_mIsVisibleByPanel_42(L_5);
		return (bool)1;
	}

IL_002f:
	{
		return (bool)0;
	}
}
// System.Boolean UIWidget::UpdateTransform(System.Int32)
extern "C"  bool UIWidget_UpdateTransform_m2760912577 (UIWidget_t769069560 * __this, int32_t ___frame0, const MethodInfo* method)
{
	Transform_t284553113 * V_0 = NULL;
	Vector2_t3525329788  V_1;
	memset(&V_1, 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector2_t3525329788  V_6;
	memset(&V_6, 0, sizeof(V_6));
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	Vector3_t3525329789  V_11;
	memset(&V_11, 0, sizeof(V_11));
	Vector3_t3525329789  V_12;
	memset(&V_12, 0, sizeof(V_12));
	int32_t G_B13_0 = 0;
	{
		Transform_t284553113 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = Application_get_isPlaying_m987993960(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_mPlayMode_38(L_1);
		bool L_2 = __this->get_mMoved_45();
		if (!L_2)
		{
			goto IL_00be;
		}
	}
	{
		__this->set_mMoved_45((bool)1);
		__this->set_mMatrixFrame_49((-1));
		Transform_t284553113 * L_3 = V_0;
		NullCheck(L_3);
		Transform_set_hasChanged_m1599503109(L_3, (bool)0, /*hidden argument*/NULL);
		Vector2_t3525329788  L_4 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_x_1();
		int32_t L_6 = __this->get_mWidth_24();
		V_2 = ((float)((float)((-L_5))*(float)(((float)((float)L_6)))));
		float L_7 = (&V_1)->get_y_2();
		int32_t L_8 = __this->get_mHeight_25();
		V_3 = ((float)((float)((-L_7))*(float)(((float)((float)L_8)))));
		float L_9 = V_2;
		int32_t L_10 = __this->get_mWidth_24();
		V_4 = ((float)((float)L_9+(float)(((float)((float)L_10)))));
		float L_11 = V_3;
		int32_t L_12 = __this->get_mHeight_25();
		V_5 = ((float)((float)L_11+(float)(((float)((float)L_12)))));
		UIPanel_t295209936 * L_13 = __this->get_panel_35();
		NullCheck(L_13);
		Matrix4x4_t277289660 * L_14 = L_13->get_address_of_worldToLocal_35();
		Transform_t284553113 * L_15 = V_0;
		float L_16 = V_2;
		float L_17 = V_3;
		NullCheck(L_15);
		Vector3_t3525329789  L_18 = Transform_TransformPoint_m3094713172(L_15, L_16, L_17, (0.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_19 = Matrix4x4_MultiplyPoint3x4_m2198174902(L_14, L_18, /*hidden argument*/NULL);
		__this->set_mOldV0_50(L_19);
		UIPanel_t295209936 * L_20 = __this->get_panel_35();
		NullCheck(L_20);
		Matrix4x4_t277289660 * L_21 = L_20->get_address_of_worldToLocal_35();
		Transform_t284553113 * L_22 = V_0;
		float L_23 = V_4;
		float L_24 = V_5;
		NullCheck(L_22);
		Vector3_t3525329789  L_25 = Transform_TransformPoint_m3094713172(L_22, L_23, L_24, (0.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_26 = Matrix4x4_MultiplyPoint3x4_m2198174902(L_21, L_25, /*hidden argument*/NULL);
		__this->set_mOldV1_51(L_26);
		goto IL_01bc;
	}

IL_00be:
	{
		UIPanel_t295209936 * L_27 = __this->get_panel_35();
		NullCheck(L_27);
		bool L_28 = L_27->get_widgetsAreStatic_26();
		if (L_28)
		{
			goto IL_01bc;
		}
	}
	{
		Transform_t284553113 * L_29 = V_0;
		NullCheck(L_29);
		bool L_30 = Transform_get_hasChanged_m248005108(L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_01bc;
		}
	}
	{
		__this->set_mMatrixFrame_49((-1));
		Transform_t284553113 * L_31 = V_0;
		NullCheck(L_31);
		Transform_set_hasChanged_m1599503109(L_31, (bool)0, /*hidden argument*/NULL);
		Vector2_t3525329788  L_32 = UIWidget_get_pivotOffset_m2000981938(__this, /*hidden argument*/NULL);
		V_6 = L_32;
		float L_33 = (&V_6)->get_x_1();
		int32_t L_34 = __this->get_mWidth_24();
		V_7 = ((float)((float)((-L_33))*(float)(((float)((float)L_34)))));
		float L_35 = (&V_6)->get_y_2();
		int32_t L_36 = __this->get_mHeight_25();
		V_8 = ((float)((float)((-L_35))*(float)(((float)((float)L_36)))));
		float L_37 = V_7;
		int32_t L_38 = __this->get_mWidth_24();
		V_9 = ((float)((float)L_37+(float)(((float)((float)L_38)))));
		float L_39 = V_8;
		int32_t L_40 = __this->get_mHeight_25();
		V_10 = ((float)((float)L_39+(float)(((float)((float)L_40)))));
		UIPanel_t295209936 * L_41 = __this->get_panel_35();
		NullCheck(L_41);
		Matrix4x4_t277289660 * L_42 = L_41->get_address_of_worldToLocal_35();
		Transform_t284553113 * L_43 = V_0;
		float L_44 = V_7;
		float L_45 = V_8;
		NullCheck(L_43);
		Vector3_t3525329789  L_46 = Transform_TransformPoint_m3094713172(L_43, L_44, L_45, (0.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_47 = Matrix4x4_MultiplyPoint3x4_m2198174902(L_42, L_46, /*hidden argument*/NULL);
		V_11 = L_47;
		UIPanel_t295209936 * L_48 = __this->get_panel_35();
		NullCheck(L_48);
		Matrix4x4_t277289660 * L_49 = L_48->get_address_of_worldToLocal_35();
		Transform_t284553113 * L_50 = V_0;
		float L_51 = V_9;
		float L_52 = V_10;
		NullCheck(L_50);
		Vector3_t3525329789  L_53 = Transform_TransformPoint_m3094713172(L_50, L_51, L_52, (0.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_54 = Matrix4x4_MultiplyPoint3x4_m2198174902(L_49, L_53, /*hidden argument*/NULL);
		V_12 = L_54;
		Vector3_t3525329789  L_55 = __this->get_mOldV0_50();
		Vector3_t3525329789  L_56 = V_11;
		Vector3_t3525329789  L_57 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_55, L_56, /*hidden argument*/NULL);
		float L_58 = Vector3_SqrMagnitude_m1662776270(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		if ((((float)L_58) > ((float)(1.0E-06f))))
		{
			goto IL_01a5;
		}
	}
	{
		Vector3_t3525329789  L_59 = __this->get_mOldV1_51();
		Vector3_t3525329789  L_60 = V_12;
		Vector3_t3525329789  L_61 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_59, L_60, /*hidden argument*/NULL);
		float L_62 = Vector3_SqrMagnitude_m1662776270(NULL /*static, unused*/, L_61, /*hidden argument*/NULL);
		if ((!(((float)L_62) > ((float)(1.0E-06f)))))
		{
			goto IL_01bc;
		}
	}

IL_01a5:
	{
		__this->set_mMoved_45((bool)1);
		Vector3_t3525329789  L_63 = V_11;
		__this->set_mOldV0_50(L_63);
		Vector3_t3525329789  L_64 = V_12;
		__this->set_mOldV1_51(L_64);
	}

IL_01bc:
	{
		bool L_65 = __this->get_mMoved_45();
		if (!L_65)
		{
			goto IL_01dd;
		}
	}
	{
		OnDimensionsChanged_t481648104 * L_66 = __this->get_onChange_27();
		if (!L_66)
		{
			goto IL_01dd;
		}
	}
	{
		OnDimensionsChanged_t481648104 * L_67 = __this->get_onChange_27();
		NullCheck(L_67);
		OnDimensionsChanged_Invoke_m2470653394(L_67, /*hidden argument*/NULL);
	}

IL_01dd:
	{
		bool L_68 = __this->get_mMoved_45();
		if (L_68)
		{
			goto IL_01f0;
		}
	}
	{
		bool L_69 = ((UIRect_t2503437976 *)__this)->get_mChanged_10();
		G_B13_0 = ((int32_t)(L_69));
		goto IL_01f1;
	}

IL_01f0:
	{
		G_B13_0 = 1;
	}

IL_01f1:
	{
		return (bool)G_B13_0;
	}
}
// System.Boolean UIWidget::UpdateGeometry(System.Int32)
extern "C"  bool UIWidget_UpdateGeometry_m76295649 (UIWidget_t769069560 * __this, int32_t ___frame0, const MethodInfo* method)
{
	float V_0 = 0.0f;
	bool V_1 = false;
	{
		int32_t L_0 = ___frame0;
		float L_1 = VirtFuncInvoker1< float, int32_t >::Invoke(9 /* System.Single UIWidget::CalculateFinalAlpha(System.Int32) */, __this, L_0);
		V_0 = L_1;
		bool L_2 = __this->get_mIsVisibleByAlpha_41();
		if (!L_2)
		{
			goto IL_0026;
		}
	}
	{
		float L_3 = __this->get_mLastAlpha_44();
		float L_4 = V_0;
		if ((((float)L_3) == ((float)L_4)))
		{
			goto IL_0026;
		}
	}
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)1);
	}

IL_0026:
	{
		float L_5 = V_0;
		__this->set_mLastAlpha_44(L_5);
		bool L_6 = ((UIRect_t2503437976 *)__this)->get_mChanged_10();
		if (!L_6)
		{
			goto IL_015c;
		}
	}
	{
		bool L_7 = __this->get_mIsVisibleByAlpha_41();
		if (!L_7)
		{
			goto IL_0121;
		}
	}
	{
		float L_8 = V_0;
		if ((!(((float)L_8) > ((float)(0.001f)))))
		{
			goto IL_0121;
		}
	}
	{
		Shader_t3998140498 * L_9 = VirtFuncInvoker0< Shader_t3998140498 * >::Invoke(29 /* UnityEngine.Shader UIWidget::get_shader() */, __this);
		bool L_10 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_9, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0121;
		}
	}
	{
		UIGeometry_t3586695974 * L_11 = __this->get_geometry_36();
		NullCheck(L_11);
		bool L_12 = UIGeometry_get_hasVertices_m167811977(L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		bool L_13 = __this->get_fillGeometry_37();
		if (!L_13)
		{
			goto IL_00a8;
		}
	}
	{
		UIGeometry_t3586695974 * L_14 = __this->get_geometry_36();
		NullCheck(L_14);
		UIGeometry_Clear_m844487776(L_14, /*hidden argument*/NULL);
		UIGeometry_t3586695974 * L_15 = __this->get_geometry_36();
		NullCheck(L_15);
		BetterList_1_t727330505 * L_16 = L_15->get_verts_0();
		UIGeometry_t3586695974 * L_17 = __this->get_geometry_36();
		NullCheck(L_17);
		BetterList_1_t727330504 * L_18 = L_17->get_uvs_1();
		UIGeometry_t3586695974 * L_19 = __this->get_geometry_36();
		NullCheck(L_19);
		BetterList_1_t3085143772 * L_20 = L_19->get_cols_2();
		VirtActionInvoker3< BetterList_1_t727330505 *, BetterList_1_t727330504 *, BetterList_1_t3085143772 * >::Invoke(38 /* System.Void UIWidget::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color>) */, __this, L_16, L_18, L_20);
	}

IL_00a8:
	{
		UIGeometry_t3586695974 * L_21 = __this->get_geometry_36();
		NullCheck(L_21);
		bool L_22 = UIGeometry_get_hasVertices_m167811977(L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0118;
		}
	}
	{
		int32_t L_23 = __this->get_mMatrixFrame_49();
		int32_t L_24 = ___frame0;
		if ((((int32_t)L_23) == ((int32_t)L_24)))
		{
			goto IL_00ec;
		}
	}
	{
		UIPanel_t295209936 * L_25 = __this->get_panel_35();
		NullCheck(L_25);
		Matrix4x4_t277289660  L_26 = L_25->get_worldToLocal_35();
		Transform_t284553113 * L_27 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		Matrix4x4_t277289660  L_28 = Transform_get_localToWorldMatrix_m3571020210(L_27, /*hidden argument*/NULL);
		Matrix4x4_t277289660  L_29 = Matrix4x4_op_Multiply_m4108203689(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
		__this->set_mLocalToPanel_40(L_29);
		int32_t L_30 = ___frame0;
		__this->set_mMatrixFrame_49(L_30);
	}

IL_00ec:
	{
		UIGeometry_t3586695974 * L_31 = __this->get_geometry_36();
		Matrix4x4_t277289660  L_32 = __this->get_mLocalToPanel_40();
		UIPanel_t295209936 * L_33 = __this->get_panel_35();
		NullCheck(L_33);
		bool L_34 = L_33->get_generateNormals_25();
		NullCheck(L_31);
		UIGeometry_ApplyTransform_m336861776(L_31, L_32, L_34, /*hidden argument*/NULL);
		__this->set_mMoved_45((bool)0);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		return (bool)1;
	}

IL_0118:
	{
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		bool L_35 = V_1;
		return L_35;
	}

IL_0121:
	{
		UIGeometry_t3586695974 * L_36 = __this->get_geometry_36();
		NullCheck(L_36);
		bool L_37 = UIGeometry_get_hasVertices_m167811977(L_36, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0157;
		}
	}
	{
		bool L_38 = __this->get_fillGeometry_37();
		if (!L_38)
		{
			goto IL_0147;
		}
	}
	{
		UIGeometry_t3586695974 * L_39 = __this->get_geometry_36();
		NullCheck(L_39);
		UIGeometry_Clear_m844487776(L_39, /*hidden argument*/NULL);
	}

IL_0147:
	{
		__this->set_mMoved_45((bool)0);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		return (bool)1;
	}

IL_0157:
	{
		goto IL_01d7;
	}

IL_015c:
	{
		bool L_40 = __this->get_mMoved_45();
		if (!L_40)
		{
			goto IL_01d7;
		}
	}
	{
		UIGeometry_t3586695974 * L_41 = __this->get_geometry_36();
		NullCheck(L_41);
		bool L_42 = UIGeometry_get_hasVertices_m167811977(L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_01d7;
		}
	}
	{
		int32_t L_43 = __this->get_mMatrixFrame_49();
		int32_t L_44 = ___frame0;
		if ((((int32_t)L_43) == ((int32_t)L_44)))
		{
			goto IL_01ab;
		}
	}
	{
		UIPanel_t295209936 * L_45 = __this->get_panel_35();
		NullCheck(L_45);
		Matrix4x4_t277289660  L_46 = L_45->get_worldToLocal_35();
		Transform_t284553113 * L_47 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_47);
		Matrix4x4_t277289660  L_48 = Transform_get_localToWorldMatrix_m3571020210(L_47, /*hidden argument*/NULL);
		Matrix4x4_t277289660  L_49 = Matrix4x4_op_Multiply_m4108203689(NULL /*static, unused*/, L_46, L_48, /*hidden argument*/NULL);
		__this->set_mLocalToPanel_40(L_49);
		int32_t L_50 = ___frame0;
		__this->set_mMatrixFrame_49(L_50);
	}

IL_01ab:
	{
		UIGeometry_t3586695974 * L_51 = __this->get_geometry_36();
		Matrix4x4_t277289660  L_52 = __this->get_mLocalToPanel_40();
		UIPanel_t295209936 * L_53 = __this->get_panel_35();
		NullCheck(L_53);
		bool L_54 = L_53->get_generateNormals_25();
		NullCheck(L_51);
		UIGeometry_ApplyTransform_m336861776(L_51, L_52, L_54, /*hidden argument*/NULL);
		__this->set_mMoved_45((bool)0);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		return (bool)1;
	}

IL_01d7:
	{
		__this->set_mMoved_45((bool)0);
		((UIRect_t2503437976 *)__this)->set_mChanged_10((bool)0);
		return (bool)0;
	}
}
// System.Void UIWidget::WriteToBuffers(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color>,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector4>)
extern "C"  void UIWidget_WriteToBuffers_m400261937 (UIWidget_t769069560 * __this, BetterList_1_t727330505 * ___v0, BetterList_1_t727330504 * ___u1, BetterList_1_t3085143772 * ___c2, BetterList_1_t727330505 * ___n3, BetterList_1_t727330506 * ___t4, const MethodInfo* method)
{
	{
		UIGeometry_t3586695974 * L_0 = __this->get_geometry_36();
		BetterList_1_t727330505 * L_1 = ___v0;
		BetterList_1_t727330504 * L_2 = ___u1;
		BetterList_1_t3085143772 * L_3 = ___c2;
		BetterList_1_t727330505 * L_4 = ___n3;
		BetterList_1_t727330506 * L_5 = ___t4;
		NullCheck(L_0);
		UIGeometry_WriteToBuffers_m1074852803(L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWidget::MakePixelPerfect()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UIWidget_MakePixelPerfect_m1839593398_MetadataUsageId;
extern "C"  void UIWidget_MakePixelPerfect_m1839593398 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWidget_MakePixelPerfect_m1839593398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_t284553113 * L_0 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_localPosition_m668140784(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		float L_3 = bankers_roundf(L_2);
		(&V_0)->set_z_3(L_3);
		float L_4 = (&V_0)->get_x_1();
		float L_5 = bankers_roundf(L_4);
		(&V_0)->set_x_1(L_5);
		float L_6 = (&V_0)->get_y_2();
		float L_7 = bankers_roundf(L_6);
		(&V_0)->set_y_2(L_7);
		Transform_t284553113 * L_8 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		Vector3_t3525329789  L_9 = V_0;
		NullCheck(L_8);
		Transform_set_localPosition_m3504330903(L_8, L_9, /*hidden argument*/NULL);
		Transform_t284553113 * L_10 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t3525329789  L_11 = Transform_get_localScale_m3886572677(L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		Transform_t284553113 * L_12 = UIRect_get_cachedTransform_m1757861860(__this, /*hidden argument*/NULL);
		float L_13 = (&V_1)->get_x_1();
		float L_14 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		float L_15 = (&V_1)->get_y_2();
		float L_16 = Mathf_Sign_m4040614993(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		Vector3_t3525329789  L_17;
		memset(&L_17, 0, sizeof(L_17));
		Vector3__ctor_m2926210380(&L_17, L_14, L_16, (1.0f), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localScale_m310756934(L_12, L_17, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UIWidget::get_minWidth()
extern "C"  int32_t UIWidget_get_minWidth_m3490344780 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		return 2;
	}
}
// System.Int32 UIWidget::get_minHeight()
extern "C"  int32_t UIWidget_get_minHeight_m1330320131 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		return 2;
	}
}
// UnityEngine.Vector4 UIWidget::get_border()
extern "C"  Vector4_t3525329790  UIWidget_get_border_m4155026927 (UIWidget_t769069560 * __this, const MethodInfo* method)
{
	{
		Vector4_t3525329790  L_0 = Vector4_get_zero_m3835647092(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UIWidget::set_border(UnityEngine.Vector4)
extern "C"  void UIWidget_set_border_m2644475438 (UIWidget_t769069560 * __this, Vector4_t3525329790  ___value0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIWidget::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color>)
extern "C"  void UIWidget_OnFill_m2078855571 (UIWidget_t769069560 * __this, BetterList_1_t727330505 * ___verts0, BetterList_1_t727330504 * ___uvs1, BetterList_1_t3085143772 * ___cols2, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UIWidget/HitCheck::.ctor(System.Object,System.IntPtr)
extern "C"  void HitCheck__ctor_m17538403 (HitCheck_t3012954453 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean UIWidget/HitCheck::Invoke(UnityEngine.Vector3)
extern "C"  bool HitCheck_Invoke_m3382841584 (HitCheck_t3012954453 * __this, Vector3_t3525329789  ___worldPos0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		HitCheck_Invoke_m3382841584((HitCheck_t3012954453 *)__this->get_prev_9(),___worldPos0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t3525329789  ___worldPos0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___worldPos0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t3525329789  ___worldPos0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___worldPos0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" bool pinvoke_delegate_wrapper_HitCheck_t3012954453(Il2CppObject* delegate, Vector3_t3525329789  ___worldPos0)
{
	typedef int32_t (STDCALL *native_function_ptr_type)(Vector3_t3525329789_marshaled_pinvoke);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___worldPos0' to native representation
	Vector3_t3525329789_marshaled_pinvoke ____worldPos0_marshaled = { };
	Vector3_t3525329789_marshal_pinvoke(___worldPos0, ____worldPos0_marshaled);

	// Native function invocation and marshaling of return value back from native representation
	int32_t _return_value = _il2cpp_pinvoke_func(____worldPos0_marshaled);

	// Marshaling cleanup of parameter '___worldPos0' native representation
	Vector3_t3525329789_marshal_pinvoke_cleanup(____worldPos0_marshaled);

	return _return_value;
}
// System.IAsyncResult UIWidget/HitCheck::BeginInvoke(UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern Il2CppClass* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern const uint32_t HitCheck_BeginInvoke_m1200520187_MetadataUsageId;
extern "C"  Il2CppObject * HitCheck_BeginInvoke_m1200520187 (HitCheck_t3012954453 * __this, Vector3_t3525329789  ___worldPos0, AsyncCallback_t1363551830 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HitCheck_BeginInvoke_m1200520187_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t3525329789_il2cpp_TypeInfo_var, &___worldPos0);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean UIWidget/HitCheck::EndInvoke(System.IAsyncResult)
extern "C"  bool HitCheck_EndInvoke_m1381750975 (HitCheck_t3012954453 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void UIWidget/OnDimensionsChanged::.ctor(System.Object,System.IntPtr)
extern "C"  void OnDimensionsChanged__ctor_m148577144 (OnDimensionsChanged_t481648104 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWidget/OnDimensionsChanged::Invoke()
extern "C"  void OnDimensionsChanged_Invoke_m2470653394 (OnDimensionsChanged_t481648104 * __this, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnDimensionsChanged_Invoke_m2470653394((OnDimensionsChanged_t481648104 *)__this->get_prev_9(), method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if ((__this->get_m_target_2() != NULL || MethodHasParameters((MethodInfo*)(__this->get_method_3().get_m_value_0()))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnDimensionsChanged_t481648104(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UIWidget/OnDimensionsChanged::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnDimensionsChanged_BeginInvoke_m602434801 (OnDimensionsChanged_t481648104 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback0, (Il2CppObject*)___object1);
}
// System.Void UIWidget/OnDimensionsChanged::EndInvoke(System.IAsyncResult)
extern "C"  void OnDimensionsChanged_EndInvoke_m1144457608 (OnDimensionsChanged_t481648104 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UIWidget/OnPostFillCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void OnPostFillCallback__ctor_m2088632341 (OnPostFillCallback_t294433735 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWidget/OnPostFillCallback::Invoke(UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color>)
extern "C"  void OnPostFillCallback_Invoke_m1064019312 (OnPostFillCallback_t294433735 * __this, UIWidget_t769069560 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t727330505 * ___verts2, BetterList_1_t727330504 * ___uvs3, BetterList_1_t3085143772 * ___cols4, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnPostFillCallback_Invoke_m1064019312((OnPostFillCallback_t294433735 *)__this->get_prev_9(),___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, UIWidget_t769069560 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t727330505 * ___verts2, BetterList_1_t727330504 * ___uvs3, BetterList_1_t3085143772 * ___cols4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, UIWidget_t769069560 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t727330505 * ___verts2, BetterList_1_t727330504 * ___uvs3, BetterList_1_t3085143772 * ___cols4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___bufferOffset1, BetterList_1_t727330505 * ___verts2, BetterList_1_t727330504 * ___uvs3, BetterList_1_t3085143772 * ___cols4, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___widget0, ___bufferOffset1, ___verts2, ___uvs3, ___cols4,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UIWidget/OnPostFillCallback::BeginInvoke(UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color>,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t OnPostFillCallback_BeginInvoke_m3883823459_MetadataUsageId;
extern "C"  Il2CppObject * OnPostFillCallback_BeginInvoke_m3883823459 (OnPostFillCallback_t294433735 * __this, UIWidget_t769069560 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t727330505 * ___verts2, BetterList_1_t727330504 * ___uvs3, BetterList_1_t3085143772 * ___cols4, AsyncCallback_t1363551830 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnPostFillCallback_BeginInvoke_m3883823459_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[6] = {0};
	__d_args[0] = ___widget0;
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___bufferOffset1);
	__d_args[2] = ___verts2;
	__d_args[3] = ___uvs3;
	__d_args[4] = ___cols4;
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback5, (Il2CppObject*)___object6);
}
// System.Void UIWidget/OnPostFillCallback::EndInvoke(System.IAsyncResult)
extern "C"  void OnPostFillCallback_EndInvoke_m2996225189 (OnPostFillCallback_t294433735 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UIWidgetContainer::.ctor()
extern "C"  void UIWidgetContainer__ctor_m2037457442 (UIWidgetContainer_t1520767337 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::.ctor()
extern Il2CppClass* List_1_t1081512082_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1640872948_MethodInfo_var;
extern const uint32_t UIWrapContent__ctor_m282366512_MetadataUsageId;
extern "C"  void UIWrapContent__ctor_m282366512 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent__ctor_m282366512_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_itemSize_2(((int32_t)100));
		__this->set_cullContent_3((bool)1);
		__this->set_mFirstTime_12((bool)1);
		List_1_t1081512082 * L_0 = (List_1_t1081512082 *)il2cpp_codegen_object_new(List_1_t1081512082_il2cpp_TypeInfo_var);
		List_1__ctor_m1640872948(L_0, /*hidden argument*/List_1__ctor_m1640872948_MethodInfo_var);
		__this->set_mChildren_13(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::Start()
extern Il2CppClass* OnClippingMoved_t1302042450_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIPanel_t295209936_m1836641897_MethodInfo_var;
extern const uint32_t UIWrapContent_Start_m3524471600_MetadataUsageId;
extern "C"  void UIWrapContent_Start_m3524471600 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_Start_m3524471600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		VirtActionInvoker0::Invoke(6 /* System.Void UIWrapContent::SortBasedOnScrollMovement() */, __this);
		VirtActionInvoker0::Invoke(9 /* System.Void UIWrapContent::WrapContent() */, __this);
		UIScrollView_t2113479878 * L_0 = __this->get_mScroll_10();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003a;
		}
	}
	{
		UIScrollView_t2113479878 * L_2 = __this->get_mScroll_10();
		NullCheck(L_2);
		UIPanel_t295209936 * L_3 = Component_GetComponent_TisUIPanel_t295209936_m1836641897(L_2, /*hidden argument*/Component_GetComponent_TisUIPanel_t295209936_m1836641897_MethodInfo_var);
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)GetVirtualMethodInfo(__this, 5));
		OnClippingMoved_t1302042450 * L_5 = (OnClippingMoved_t1302042450 *)il2cpp_codegen_object_new(OnClippingMoved_t1302042450_il2cpp_TypeInfo_var);
		OnClippingMoved__ctor_m3635995498(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		L_3->set_onClipMove_37(L_5);
	}

IL_003a:
	{
		__this->set_mFirstTime_12((bool)0);
		return;
	}
}
// System.Void UIWrapContent::OnMove(UIPanel)
extern "C"  void UIWrapContent_OnMove_m3675848574 (UIWrapContent_t33025435 * __this, UIPanel_t295209936 * ___panel0, const MethodInfo* method)
{
	{
		VirtActionInvoker0::Invoke(9 /* System.Void UIWrapContent::WrapContent() */, __this);
		return;
	}
}
// System.Void UIWrapContent::SortBasedOnScrollMovement()
extern Il2CppClass* Comparison_1_t2988227989_il2cpp_TypeInfo_var;
extern const MethodInfo* UIGrid_SortHorizontal_m646783841_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1879963539_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2423802008_MethodInfo_var;
extern const MethodInfo* UIGrid_SortVertical_m3217656271_MethodInfo_var;
extern const uint32_t UIWrapContent_SortBasedOnScrollMovement_m695931902_MetadataUsageId;
extern "C"  void UIWrapContent_SortBasedOnScrollMovement_m695931902 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_SortBasedOnScrollMovement_m695931902_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Transform_t284553113 * V_1 = NULL;
	{
		bool L_0 = UIWrapContent_CacheScrollView_m1949504758(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t1081512082 * L_1 = __this->get_mChildren_13();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Clear() */, L_1);
		V_0 = 0;
		goto IL_005b;
	}

IL_001e:
	{
		Transform_t284553113 * L_2 = __this->get_mTrans_8();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_t284553113 * L_4 = Transform_GetChild_m4040462992(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = __this->get_hideInactive_6();
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		Transform_t284553113 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = GameObject_get_activeInHierarchy_m612450965(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004b;
		}
	}
	{
		goto IL_0057;
	}

IL_004b:
	{
		List_1_t1081512082 * L_9 = __this->get_mChildren_13();
		Transform_t284553113 * L_10 = V_1;
		NullCheck(L_9);
		VirtActionInvoker1< Transform_t284553113 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0) */, L_9, L_10);
	}

IL_0057:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_12 = V_0;
		Transform_t284553113 * L_13 = __this->get_mTrans_8();
		NullCheck(L_13);
		int32_t L_14 = Transform_get_childCount_m2107810675(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_001e;
		}
	}
	{
		bool L_15 = __this->get_mHorizontal_11();
		if (!L_15)
		{
			goto IL_0093;
		}
	}
	{
		List_1_t1081512082 * L_16 = __this->get_mChildren_13();
		IntPtr_t L_17;
		L_17.set_m_value_0((void*)(void*)UIGrid_SortHorizontal_m646783841_MethodInfo_var);
		Comparison_1_t2988227989 * L_18 = (Comparison_1_t2988227989 *)il2cpp_codegen_object_new(Comparison_1_t2988227989_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_18, NULL, L_17, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_16);
		List_1_Sort_m2423802008(L_16, L_18, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
		goto IL_00aa;
	}

IL_0093:
	{
		List_1_t1081512082 * L_19 = __this->get_mChildren_13();
		IntPtr_t L_20;
		L_20.set_m_value_0((void*)(void*)UIGrid_SortVertical_m3217656271_MethodInfo_var);
		Comparison_1_t2988227989 * L_21 = (Comparison_1_t2988227989 *)il2cpp_codegen_object_new(Comparison_1_t2988227989_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_21, NULL, L_20, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_19);
		List_1_Sort_m2423802008(L_19, L_21, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
	}

IL_00aa:
	{
		VirtActionInvoker0::Invoke(8 /* System.Void UIWrapContent::ResetChildPositions() */, __this);
		return;
	}
}
// System.Void UIWrapContent::SortAlphabetically()
extern Il2CppClass* Comparison_1_t2988227989_il2cpp_TypeInfo_var;
extern const MethodInfo* UIGrid_SortByName_m3179856323_MethodInfo_var;
extern const MethodInfo* Comparison_1__ctor_m1879963539_MethodInfo_var;
extern const MethodInfo* List_1_Sort_m2423802008_MethodInfo_var;
extern const uint32_t UIWrapContent_SortAlphabetically_m1473128343_MetadataUsageId;
extern "C"  void UIWrapContent_SortAlphabetically_m1473128343 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_SortAlphabetically_m1473128343_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Transform_t284553113 * V_1 = NULL;
	{
		bool L_0 = UIWrapContent_CacheScrollView_m1949504758(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		List_1_t1081512082 * L_1 = __this->get_mChildren_13();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Clear() */, L_1);
		V_0 = 0;
		goto IL_005b;
	}

IL_001e:
	{
		Transform_t284553113 * L_2 = __this->get_mTrans_8();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_t284553113 * L_4 = Transform_GetChild_m4040462992(L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = __this->get_hideInactive_6();
		if (!L_5)
		{
			goto IL_004b;
		}
	}
	{
		Transform_t284553113 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		bool L_8 = GameObject_get_activeInHierarchy_m612450965(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004b;
		}
	}
	{
		goto IL_0057;
	}

IL_004b:
	{
		List_1_t1081512082 * L_9 = __this->get_mChildren_13();
		Transform_t284553113 * L_10 = V_1;
		NullCheck(L_9);
		VirtActionInvoker1< Transform_t284553113 * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0) */, L_9, L_10);
	}

IL_0057:
	{
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_12 = V_0;
		Transform_t284553113 * L_13 = __this->get_mTrans_8();
		NullCheck(L_13);
		int32_t L_14 = Transform_get_childCount_m2107810675(L_13, /*hidden argument*/NULL);
		if ((((int32_t)L_12) < ((int32_t)L_14)))
		{
			goto IL_001e;
		}
	}
	{
		List_1_t1081512082 * L_15 = __this->get_mChildren_13();
		IntPtr_t L_16;
		L_16.set_m_value_0((void*)(void*)UIGrid_SortByName_m3179856323_MethodInfo_var);
		Comparison_1_t2988227989 * L_17 = (Comparison_1_t2988227989 *)il2cpp_codegen_object_new(Comparison_1_t2988227989_il2cpp_TypeInfo_var);
		Comparison_1__ctor_m1879963539(L_17, NULL, L_16, /*hidden argument*/Comparison_1__ctor_m1879963539_MethodInfo_var);
		NullCheck(L_15);
		List_1_Sort_m2423802008(L_15, L_17, /*hidden argument*/List_1_Sort_m2423802008_MethodInfo_var);
		VirtActionInvoker0::Invoke(8 /* System.Void UIWrapContent::ResetChildPositions() */, __this);
		return;
	}
}
// System.Boolean UIWrapContent::CacheScrollView()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const MethodInfo* NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisUIScrollView_t2113479878_m1783064831_MethodInfo_var;
extern const uint32_t UIWrapContent_CacheScrollView_m1949504758_MetadataUsageId;
extern "C"  bool UIWrapContent_CacheScrollView_m1949504758 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_CacheScrollView_m1949504758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_mTrans_8(L_0);
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		UIPanel_t295209936 * L_2 = NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243(NULL /*static, unused*/, L_1, /*hidden argument*/NGUITools_FindInParents_TisUIPanel_t295209936_m2641381243_MethodInfo_var);
		__this->set_mPanel_9(L_2);
		UIPanel_t295209936 * L_3 = __this->get_mPanel_9();
		NullCheck(L_3);
		UIScrollView_t2113479878 * L_4 = Component_GetComponent_TisUIScrollView_t2113479878_m1783064831(L_3, /*hidden argument*/Component_GetComponent_TisUIScrollView_t2113479878_m1783064831_MethodInfo_var);
		__this->set_mScroll_10(L_4);
		UIScrollView_t2113479878 * L_5 = __this->get_mScroll_10();
		bool L_6 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_5, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0041;
		}
	}
	{
		return (bool)0;
	}

IL_0041:
	{
		UIScrollView_t2113479878 * L_7 = __this->get_mScroll_10();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_movement_3();
		if (L_8)
		{
			goto IL_005d;
		}
	}
	{
		__this->set_mHorizontal_11((bool)1);
		goto IL_007c;
	}

IL_005d:
	{
		UIScrollView_t2113479878 * L_9 = __this->get_mScroll_10();
		NullCheck(L_9);
		int32_t L_10 = L_9->get_movement_3();
		if ((!(((uint32_t)L_10) == ((uint32_t)1))))
		{
			goto IL_007a;
		}
	}
	{
		__this->set_mHorizontal_11((bool)0);
		goto IL_007c;
	}

IL_007a:
	{
		return (bool)0;
	}

IL_007c:
	{
		return (bool)1;
	}
}
// System.Void UIWrapContent::ResetChildPositions()
extern "C"  void UIWrapContent_ResetChildPositions_m1267405611 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Transform_t284553113 * V_2 = NULL;
	Transform_t284553113 * G_B3_0 = NULL;
	Transform_t284553113 * G_B2_0 = NULL;
	Vector3_t3525329789  G_B4_0;
	memset(&G_B4_0, 0, sizeof(G_B4_0));
	Transform_t284553113 * G_B4_1 = NULL;
	{
		V_0 = 0;
		List_1_t1081512082 * L_0 = __this->get_mChildren_13();
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_0);
		V_1 = L_1;
		goto IL_0073;
	}

IL_0013:
	{
		List_1_t1081512082 * L_2 = __this->get_mChildren_13();
		int32_t L_3 = V_0;
		NullCheck(L_2);
		Transform_t284553113 * L_4 = VirtFuncInvoker1< Transform_t284553113 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, L_2, L_3);
		V_2 = L_4;
		Transform_t284553113 * L_5 = V_2;
		bool L_6 = __this->get_mHorizontal_11();
		G_B2_0 = L_5;
		if (!L_6)
		{
			G_B3_0 = L_5;
			goto IL_0049;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = __this->get_itemSize_2();
		Vector3_t3525329789  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2926210380(&L_9, (((float)((float)((int32_t)((int32_t)L_7*(int32_t)L_8))))), (0.0f), (0.0f), /*hidden argument*/NULL);
		G_B4_0 = L_9;
		G_B4_1 = G_B2_0;
		goto IL_0062;
	}

IL_0049:
	{
		int32_t L_10 = V_0;
		int32_t L_11 = __this->get_itemSize_2();
		Vector3_t3525329789  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Vector3__ctor_m2926210380(&L_12, (0.0f), (((float)((float)((int32_t)((int32_t)((-L_10))*(int32_t)L_11))))), (0.0f), /*hidden argument*/NULL);
		G_B4_0 = L_12;
		G_B4_1 = G_B3_0;
	}

IL_0062:
	{
		NullCheck(G_B4_1);
		Transform_set_localPosition_m3504330903(G_B4_1, G_B4_0, /*hidden argument*/NULL);
		Transform_t284553113 * L_13 = V_2;
		int32_t L_14 = V_0;
		VirtActionInvoker2< Transform_t284553113 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_13, L_14);
		int32_t L_15 = V_0;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0073:
	{
		int32_t L_16 = V_0;
		int32_t L_17 = V_1;
		if ((((int32_t)L_16) < ((int32_t)L_17)))
		{
			goto IL_0013;
		}
	}
	{
		return;
	}
}
// System.Void UIWrapContent::WrapContent()
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern Il2CppClass* UICamera_t189364953_il2cpp_TypeInfo_var;
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t UIWrapContent_WrapContent_m2368030365_MetadataUsageId;
extern "C"  void UIWrapContent_WrapContent_m2368030365 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_WrapContent_m2368030365_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3U5BU5D_t3227571696* V_1 = NULL;
	int32_t V_2 = 0;
	Vector3_t3525329789  V_3;
	memset(&V_3, 0, sizeof(V_3));
	Vector3_t3525329789  V_4;
	memset(&V_4, 0, sizeof(V_4));
	bool V_5 = false;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	Transform_t284553113 * V_11 = NULL;
	float V_12 = 0.0f;
	Vector3_t3525329789  V_13;
	memset(&V_13, 0, sizeof(V_13));
	int32_t V_14 = 0;
	Vector3_t3525329789  V_15;
	memset(&V_15, 0, sizeof(V_15));
	int32_t V_16 = 0;
	float V_17 = 0.0f;
	float V_18 = 0.0f;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	Transform_t284553113 * V_21 = NULL;
	float V_22 = 0.0f;
	Vector3_t3525329789  V_23;
	memset(&V_23, 0, sizeof(V_23));
	int32_t V_24 = 0;
	Vector3_t3525329789  V_25;
	memset(&V_25, 0, sizeof(V_25));
	int32_t V_26 = 0;
	Vector3_t3525329789  V_27;
	memset(&V_27, 0, sizeof(V_27));
	Vector2_t3525329788  V_28;
	memset(&V_28, 0, sizeof(V_28));
	Vector3_t3525329789  V_29;
	memset(&V_29, 0, sizeof(V_29));
	Vector3_t3525329789  V_30;
	memset(&V_30, 0, sizeof(V_30));
	Vector2_t3525329788  V_31;
	memset(&V_31, 0, sizeof(V_31));
	Vector3_t3525329789  V_32;
	memset(&V_32, 0, sizeof(V_32));
	GameObject_t4012695102 * G_B25_0 = NULL;
	GameObject_t4012695102 * G_B24_0 = NULL;
	int32_t G_B26_0 = 0;
	GameObject_t4012695102 * G_B26_1 = NULL;
	GameObject_t4012695102 * G_B51_0 = NULL;
	GameObject_t4012695102 * G_B50_0 = NULL;
	int32_t G_B52_0 = 0;
	GameObject_t4012695102 * G_B52_1 = NULL;
	{
		int32_t L_0 = __this->get_itemSize_2();
		List_1_t1081512082 * L_1 = __this->get_mChildren_13();
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_1);
		V_0 = ((float)((float)(((float)((float)((int32_t)((int32_t)L_0*(int32_t)L_2)))))*(float)(0.5f)));
		UIPanel_t295209936 * L_3 = __this->get_mPanel_9();
		NullCheck(L_3);
		Vector3U5BU5D_t3227571696* L_4 = VirtFuncInvoker0< Vector3U5BU5D_t3227571696* >::Invoke(11 /* UnityEngine.Vector3[] UIPanel::get_worldCorners() */, L_3);
		V_1 = L_4;
		V_2 = 0;
		goto IL_0058;
	}

IL_002d:
	{
		Vector3U5BU5D_t3227571696* L_5 = V_1;
		int32_t L_6 = V_2;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		V_3 = (*(Vector3_t3525329789 *)((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6))));
		Transform_t284553113 * L_7 = __this->get_mTrans_8();
		Vector3_t3525329789  L_8 = V_3;
		NullCheck(L_7);
		Vector3_t3525329789  L_9 = Transform_InverseTransformPoint_m1626812000(L_7, L_8, /*hidden argument*/NULL);
		V_3 = L_9;
		Vector3U5BU5D_t3227571696* L_10 = V_1;
		int32_t L_11 = V_2;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		Vector3_t3525329789  L_12 = V_3;
		(*(Vector3_t3525329789 *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))) = L_12;
		int32_t L_13 = V_2;
		V_2 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0058:
	{
		int32_t L_14 = V_2;
		if ((((int32_t)L_14) < ((int32_t)4)))
		{
			goto IL_002d;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_15 = V_1;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 0);
		Vector3U5BU5D_t3227571696* L_16 = V_1;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 2);
		Vector3_t3525329789  L_17 = Vector3_Lerp_m650470329(NULL /*static, unused*/, (*(Vector3_t3525329789 *)((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))), (*(Vector3_t3525329789 *)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))), (0.5f), /*hidden argument*/NULL);
		V_4 = L_17;
		V_5 = (bool)1;
		float L_18 = V_0;
		V_6 = ((float)((float)L_18*(float)(2.0f)));
		bool L_19 = __this->get_mHorizontal_11();
		if (!L_19)
		{
			goto IL_02bd;
		}
	}
	{
		Vector3U5BU5D_t3227571696* L_20 = V_1;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		float L_21 = ((L_20)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_x_1();
		int32_t L_22 = __this->get_itemSize_2();
		V_7 = ((float)((float)L_21-(float)(((float)((float)L_22)))));
		Vector3U5BU5D_t3227571696* L_23 = V_1;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 2);
		float L_24 = ((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_x_1();
		int32_t L_25 = __this->get_itemSize_2();
		V_8 = ((float)((float)L_24+(float)(((float)((float)L_25)))));
		V_9 = 0;
		List_1_t1081512082 * L_26 = __this->get_mChildren_13();
		NullCheck(L_26);
		int32_t L_27 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_26);
		V_10 = L_27;
		goto IL_02af;
	}

IL_00db:
	{
		List_1_t1081512082 * L_28 = __this->get_mChildren_13();
		int32_t L_29 = V_9;
		NullCheck(L_28);
		Transform_t284553113 * L_30 = VirtFuncInvoker1< Transform_t284553113 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, L_28, L_29);
		V_11 = L_30;
		Transform_t284553113 * L_31 = V_11;
		NullCheck(L_31);
		Vector3_t3525329789  L_32 = Transform_get_localPosition_m668140784(L_31, /*hidden argument*/NULL);
		V_27 = L_32;
		float L_33 = (&V_27)->get_x_1();
		float L_34 = (&V_4)->get_x_1();
		V_12 = ((float)((float)L_33-(float)L_34));
		float L_35 = V_12;
		float L_36 = V_0;
		if ((!(((float)L_35) < ((float)((-L_36))))))
		{
			goto IL_0198;
		}
	}
	{
		Transform_t284553113 * L_37 = V_11;
		NullCheck(L_37);
		Vector3_t3525329789  L_38 = Transform_get_localPosition_m668140784(L_37, /*hidden argument*/NULL);
		V_13 = L_38;
		Vector3_t3525329789 * L_39 = (&V_13);
		float L_40 = L_39->get_x_1();
		float L_41 = V_6;
		L_39->set_x_1(((float)((float)L_40+(float)L_41)));
		float L_42 = (&V_13)->get_x_1();
		float L_43 = (&V_4)->get_x_1();
		V_12 = ((float)((float)L_42-(float)L_43));
		float L_44 = (&V_13)->get_x_1();
		int32_t L_45 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_46 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_44/(float)(((float)((float)L_45))))), /*hidden argument*/NULL);
		V_14 = L_46;
		int32_t L_47 = __this->get_minIndex_4();
		int32_t L_48 = __this->get_maxIndex_5();
		if ((((int32_t)L_47) == ((int32_t)L_48)))
		{
			goto IL_0178;
		}
	}
	{
		int32_t L_49 = __this->get_minIndex_4();
		int32_t L_50 = V_14;
		if ((((int32_t)L_49) > ((int32_t)L_50)))
		{
			goto IL_0190;
		}
	}
	{
		int32_t L_51 = V_14;
		int32_t L_52 = __this->get_maxIndex_5();
		if ((((int32_t)L_51) > ((int32_t)L_52)))
		{
			goto IL_0190;
		}
	}

IL_0178:
	{
		Transform_t284553113 * L_53 = V_11;
		Vector3_t3525329789  L_54 = V_13;
		NullCheck(L_53);
		Transform_set_localPosition_m3504330903(L_53, L_54, /*hidden argument*/NULL);
		Transform_t284553113 * L_55 = V_11;
		int32_t L_56 = V_9;
		VirtActionInvoker2< Transform_t284553113 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_55, L_56);
		goto IL_0193;
	}

IL_0190:
	{
		V_5 = (bool)0;
	}

IL_0193:
	{
		goto IL_0240;
	}

IL_0198:
	{
		float L_57 = V_12;
		float L_58 = V_0;
		if ((!(((float)L_57) > ((float)L_58))))
		{
			goto IL_022b;
		}
	}
	{
		Transform_t284553113 * L_59 = V_11;
		NullCheck(L_59);
		Vector3_t3525329789  L_60 = Transform_get_localPosition_m668140784(L_59, /*hidden argument*/NULL);
		V_15 = L_60;
		Vector3_t3525329789 * L_61 = (&V_15);
		float L_62 = L_61->get_x_1();
		float L_63 = V_6;
		L_61->set_x_1(((float)((float)L_62-(float)L_63)));
		float L_64 = (&V_15)->get_x_1();
		float L_65 = (&V_4)->get_x_1();
		V_12 = ((float)((float)L_64-(float)L_65));
		float L_66 = (&V_15)->get_x_1();
		int32_t L_67 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_68 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_66/(float)(((float)((float)L_67))))), /*hidden argument*/NULL);
		V_16 = L_68;
		int32_t L_69 = __this->get_minIndex_4();
		int32_t L_70 = __this->get_maxIndex_5();
		if ((((int32_t)L_69) == ((int32_t)L_70)))
		{
			goto IL_020b;
		}
	}
	{
		int32_t L_71 = __this->get_minIndex_4();
		int32_t L_72 = V_16;
		if ((((int32_t)L_71) > ((int32_t)L_72)))
		{
			goto IL_0223;
		}
	}
	{
		int32_t L_73 = V_16;
		int32_t L_74 = __this->get_maxIndex_5();
		if ((((int32_t)L_73) > ((int32_t)L_74)))
		{
			goto IL_0223;
		}
	}

IL_020b:
	{
		Transform_t284553113 * L_75 = V_11;
		Vector3_t3525329789  L_76 = V_15;
		NullCheck(L_75);
		Transform_set_localPosition_m3504330903(L_75, L_76, /*hidden argument*/NULL);
		Transform_t284553113 * L_77 = V_11;
		int32_t L_78 = V_9;
		VirtActionInvoker2< Transform_t284553113 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_77, L_78);
		goto IL_0226;
	}

IL_0223:
	{
		V_5 = (bool)0;
	}

IL_0226:
	{
		goto IL_0240;
	}

IL_022b:
	{
		bool L_79 = __this->get_mFirstTime_12();
		if (!L_79)
		{
			goto IL_0240;
		}
	}
	{
		Transform_t284553113 * L_80 = V_11;
		int32_t L_81 = V_9;
		VirtActionInvoker2< Transform_t284553113 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_80, L_81);
	}

IL_0240:
	{
		bool L_82 = __this->get_cullContent_3();
		if (!L_82)
		{
			goto IL_02a9;
		}
	}
	{
		float L_83 = V_12;
		UIPanel_t295209936 * L_84 = __this->get_mPanel_9();
		NullCheck(L_84);
		Vector2_t3525329788  L_85 = UIPanel_get_clipOffset_m3093353146(L_84, /*hidden argument*/NULL);
		V_28 = L_85;
		float L_86 = (&V_28)->get_x_1();
		Transform_t284553113 * L_87 = __this->get_mTrans_8();
		NullCheck(L_87);
		Vector3_t3525329789  L_88 = Transform_get_localPosition_m668140784(L_87, /*hidden argument*/NULL);
		V_29 = L_88;
		float L_89 = (&V_29)->get_x_1();
		V_12 = ((float)((float)L_83+(float)((float)((float)L_86-(float)L_89))));
		Transform_t284553113 * L_90 = V_11;
		NullCheck(L_90);
		GameObject_t4012695102 * L_91 = Component_get_gameObject_m1170635899(L_90, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		bool L_92 = UICamera_IsPressed_m2263685468(NULL /*static, unused*/, L_91, /*hidden argument*/NULL);
		if (L_92)
		{
			goto IL_02a9;
		}
	}
	{
		Transform_t284553113 * L_93 = V_11;
		NullCheck(L_93);
		GameObject_t4012695102 * L_94 = Component_get_gameObject_m1170635899(L_93, /*hidden argument*/NULL);
		float L_95 = V_12;
		float L_96 = V_7;
		G_B24_0 = L_94;
		if ((!(((float)L_95) > ((float)L_96))))
		{
			G_B25_0 = L_94;
			goto IL_02a2;
		}
	}
	{
		float L_97 = V_12;
		float L_98 = V_8;
		G_B26_0 = ((((float)L_97) < ((float)L_98))? 1 : 0);
		G_B26_1 = G_B24_0;
		goto IL_02a3;
	}

IL_02a2:
	{
		G_B26_0 = 0;
		G_B26_1 = G_B25_0;
	}

IL_02a3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m1380602331(NULL /*static, unused*/, G_B26_1, (bool)G_B26_0, (bool)0, /*hidden argument*/NULL);
	}

IL_02a9:
	{
		int32_t L_99 = V_9;
		V_9 = ((int32_t)((int32_t)L_99+(int32_t)1));
	}

IL_02af:
	{
		int32_t L_100 = V_9;
		int32_t L_101 = V_10;
		if ((((int32_t)L_100) < ((int32_t)L_101)))
		{
			goto IL_00db;
		}
	}
	{
		goto IL_04db;
	}

IL_02bd:
	{
		Vector3U5BU5D_t3227571696* L_102 = V_1;
		NullCheck(L_102);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_102, 0);
		float L_103 = ((L_102)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))->get_y_2();
		int32_t L_104 = __this->get_itemSize_2();
		V_17 = ((float)((float)L_103-(float)(((float)((float)L_104)))));
		Vector3U5BU5D_t3227571696* L_105 = V_1;
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, 2);
		float L_106 = ((L_105)->GetAddressAt(static_cast<il2cpp_array_size_t>(2)))->get_y_2();
		int32_t L_107 = __this->get_itemSize_2();
		V_18 = ((float)((float)L_106+(float)(((float)((float)L_107)))));
		V_19 = 0;
		List_1_t1081512082 * L_108 = __this->get_mChildren_13();
		NullCheck(L_108);
		int32_t L_109 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Count() */, L_108);
		V_20 = L_109;
		goto IL_04d2;
	}

IL_02fe:
	{
		List_1_t1081512082 * L_110 = __this->get_mChildren_13();
		int32_t L_111 = V_19;
		NullCheck(L_110);
		Transform_t284553113 * L_112 = VirtFuncInvoker1< Transform_t284553113 *, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Transform>::get_Item(System.Int32) */, L_110, L_111);
		V_21 = L_112;
		Transform_t284553113 * L_113 = V_21;
		NullCheck(L_113);
		Vector3_t3525329789  L_114 = Transform_get_localPosition_m668140784(L_113, /*hidden argument*/NULL);
		V_30 = L_114;
		float L_115 = (&V_30)->get_y_2();
		float L_116 = (&V_4)->get_y_2();
		V_22 = ((float)((float)L_115-(float)L_116));
		float L_117 = V_22;
		float L_118 = V_0;
		if ((!(((float)L_117) < ((float)((-L_118))))))
		{
			goto IL_03bb;
		}
	}
	{
		Transform_t284553113 * L_119 = V_21;
		NullCheck(L_119);
		Vector3_t3525329789  L_120 = Transform_get_localPosition_m668140784(L_119, /*hidden argument*/NULL);
		V_23 = L_120;
		Vector3_t3525329789 * L_121 = (&V_23);
		float L_122 = L_121->get_y_2();
		float L_123 = V_6;
		L_121->set_y_2(((float)((float)L_122+(float)L_123)));
		float L_124 = (&V_23)->get_y_2();
		float L_125 = (&V_4)->get_y_2();
		V_22 = ((float)((float)L_124-(float)L_125));
		float L_126 = (&V_23)->get_y_2();
		int32_t L_127 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_128 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_126/(float)(((float)((float)L_127))))), /*hidden argument*/NULL);
		V_24 = L_128;
		int32_t L_129 = __this->get_minIndex_4();
		int32_t L_130 = __this->get_maxIndex_5();
		if ((((int32_t)L_129) == ((int32_t)L_130)))
		{
			goto IL_039b;
		}
	}
	{
		int32_t L_131 = __this->get_minIndex_4();
		int32_t L_132 = V_24;
		if ((((int32_t)L_131) > ((int32_t)L_132)))
		{
			goto IL_03b3;
		}
	}
	{
		int32_t L_133 = V_24;
		int32_t L_134 = __this->get_maxIndex_5();
		if ((((int32_t)L_133) > ((int32_t)L_134)))
		{
			goto IL_03b3;
		}
	}

IL_039b:
	{
		Transform_t284553113 * L_135 = V_21;
		Vector3_t3525329789  L_136 = V_23;
		NullCheck(L_135);
		Transform_set_localPosition_m3504330903(L_135, L_136, /*hidden argument*/NULL);
		Transform_t284553113 * L_137 = V_21;
		int32_t L_138 = V_19;
		VirtActionInvoker2< Transform_t284553113 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_137, L_138);
		goto IL_03b6;
	}

IL_03b3:
	{
		V_5 = (bool)0;
	}

IL_03b6:
	{
		goto IL_0463;
	}

IL_03bb:
	{
		float L_139 = V_22;
		float L_140 = V_0;
		if ((!(((float)L_139) > ((float)L_140))))
		{
			goto IL_044e;
		}
	}
	{
		Transform_t284553113 * L_141 = V_21;
		NullCheck(L_141);
		Vector3_t3525329789  L_142 = Transform_get_localPosition_m668140784(L_141, /*hidden argument*/NULL);
		V_25 = L_142;
		Vector3_t3525329789 * L_143 = (&V_25);
		float L_144 = L_143->get_y_2();
		float L_145 = V_6;
		L_143->set_y_2(((float)((float)L_144-(float)L_145)));
		float L_146 = (&V_25)->get_y_2();
		float L_147 = (&V_4)->get_y_2();
		V_22 = ((float)((float)L_146-(float)L_147));
		float L_148 = (&V_25)->get_y_2();
		int32_t L_149 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_150 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_148/(float)(((float)((float)L_149))))), /*hidden argument*/NULL);
		V_26 = L_150;
		int32_t L_151 = __this->get_minIndex_4();
		int32_t L_152 = __this->get_maxIndex_5();
		if ((((int32_t)L_151) == ((int32_t)L_152)))
		{
			goto IL_042e;
		}
	}
	{
		int32_t L_153 = __this->get_minIndex_4();
		int32_t L_154 = V_26;
		if ((((int32_t)L_153) > ((int32_t)L_154)))
		{
			goto IL_0446;
		}
	}
	{
		int32_t L_155 = V_26;
		int32_t L_156 = __this->get_maxIndex_5();
		if ((((int32_t)L_155) > ((int32_t)L_156)))
		{
			goto IL_0446;
		}
	}

IL_042e:
	{
		Transform_t284553113 * L_157 = V_21;
		Vector3_t3525329789  L_158 = V_25;
		NullCheck(L_157);
		Transform_set_localPosition_m3504330903(L_157, L_158, /*hidden argument*/NULL);
		Transform_t284553113 * L_159 = V_21;
		int32_t L_160 = V_19;
		VirtActionInvoker2< Transform_t284553113 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_159, L_160);
		goto IL_0449;
	}

IL_0446:
	{
		V_5 = (bool)0;
	}

IL_0449:
	{
		goto IL_0463;
	}

IL_044e:
	{
		bool L_161 = __this->get_mFirstTime_12();
		if (!L_161)
		{
			goto IL_0463;
		}
	}
	{
		Transform_t284553113 * L_162 = V_21;
		int32_t L_163 = V_19;
		VirtActionInvoker2< Transform_t284553113 *, int32_t >::Invoke(10 /* System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32) */, __this, L_162, L_163);
	}

IL_0463:
	{
		bool L_164 = __this->get_cullContent_3();
		if (!L_164)
		{
			goto IL_04cc;
		}
	}
	{
		float L_165 = V_22;
		UIPanel_t295209936 * L_166 = __this->get_mPanel_9();
		NullCheck(L_166);
		Vector2_t3525329788  L_167 = UIPanel_get_clipOffset_m3093353146(L_166, /*hidden argument*/NULL);
		V_31 = L_167;
		float L_168 = (&V_31)->get_y_2();
		Transform_t284553113 * L_169 = __this->get_mTrans_8();
		NullCheck(L_169);
		Vector3_t3525329789  L_170 = Transform_get_localPosition_m668140784(L_169, /*hidden argument*/NULL);
		V_32 = L_170;
		float L_171 = (&V_32)->get_y_2();
		V_22 = ((float)((float)L_165+(float)((float)((float)L_168-(float)L_171))));
		Transform_t284553113 * L_172 = V_21;
		NullCheck(L_172);
		GameObject_t4012695102 * L_173 = Component_get_gameObject_m1170635899(L_172, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(UICamera_t189364953_il2cpp_TypeInfo_var);
		bool L_174 = UICamera_IsPressed_m2263685468(NULL /*static, unused*/, L_173, /*hidden argument*/NULL);
		if (L_174)
		{
			goto IL_04cc;
		}
	}
	{
		Transform_t284553113 * L_175 = V_21;
		NullCheck(L_175);
		GameObject_t4012695102 * L_176 = Component_get_gameObject_m1170635899(L_175, /*hidden argument*/NULL);
		float L_177 = V_22;
		float L_178 = V_17;
		G_B50_0 = L_176;
		if ((!(((float)L_177) > ((float)L_178))))
		{
			G_B51_0 = L_176;
			goto IL_04c5;
		}
	}
	{
		float L_179 = V_22;
		float L_180 = V_18;
		G_B52_0 = ((((float)L_179) < ((float)L_180))? 1 : 0);
		G_B52_1 = G_B50_0;
		goto IL_04c6;
	}

IL_04c5:
	{
		G_B52_0 = 0;
		G_B52_1 = G_B51_0;
	}

IL_04c6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		NGUITools_SetActive_m1380602331(NULL /*static, unused*/, G_B52_1, (bool)G_B52_0, (bool)0, /*hidden argument*/NULL);
	}

IL_04cc:
	{
		int32_t L_181 = V_19;
		V_19 = ((int32_t)((int32_t)L_181+(int32_t)1));
	}

IL_04d2:
	{
		int32_t L_182 = V_19;
		int32_t L_183 = V_20;
		if ((((int32_t)L_182) < ((int32_t)L_183)))
		{
			goto IL_02fe;
		}
	}

IL_04db:
	{
		UIScrollView_t2113479878 * L_184 = __this->get_mScroll_10();
		bool L_185 = V_5;
		NullCheck(L_184);
		L_184->set_restrictWithinPanel_5((bool)((((int32_t)L_185) == ((int32_t)0))? 1 : 0));
		UIScrollView_t2113479878 * L_186 = __this->get_mScroll_10();
		NullCheck(L_186);
		UIScrollView_InvalidateBounds_m2073271519(L_186, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UIWrapContent::OnValidate()
extern "C"  void UIWrapContent_OnValidate_m3655927209 (UIWrapContent_t33025435 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_maxIndex_5();
		int32_t L_1 = __this->get_minIndex_4();
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_001d;
		}
	}
	{
		int32_t L_2 = __this->get_minIndex_4();
		__this->set_maxIndex_5(L_2);
	}

IL_001d:
	{
		int32_t L_3 = __this->get_minIndex_4();
		int32_t L_4 = __this->get_maxIndex_5();
		if ((((int32_t)L_3) <= ((int32_t)L_4)))
		{
			goto IL_003a;
		}
	}
	{
		int32_t L_5 = __this->get_minIndex_4();
		__this->set_maxIndex_5(L_5);
	}

IL_003a:
	{
		return;
	}
}
// System.Void UIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32)
extern Il2CppClass* Mathf_t1597001355_il2cpp_TypeInfo_var;
extern const uint32_t UIWrapContent_UpdateItem_m3898182666_MetadataUsageId;
extern "C"  void UIWrapContent_UpdateItem_m3898182666 (UIWrapContent_t33025435 * __this, Transform_t284553113 * ___item0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UIWrapContent_UpdateItem_m3898182666_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Vector3_t3525329789  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t3525329789  V_2;
	memset(&V_2, 0, sizeof(V_2));
	int32_t G_B4_0 = 0;
	{
		OnInitializeItem_t2008047266 * L_0 = __this->get_onInitializeItem_7();
		if (!L_0)
		{
			goto IL_006b;
		}
	}
	{
		UIScrollView_t2113479878 * L_1 = __this->get_mScroll_10();
		NullCheck(L_1);
		int32_t L_2 = L_1->get_movement_3();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_003c;
		}
	}
	{
		Transform_t284553113 * L_3 = ___item0;
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_localPosition_m668140784(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = (&V_1)->get_y_2();
		int32_t L_6 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_7 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_5/(float)(((float)((float)L_6))))), /*hidden argument*/NULL);
		G_B4_0 = L_7;
		goto IL_0057;
	}

IL_003c:
	{
		Transform_t284553113 * L_8 = ___item0;
		NullCheck(L_8);
		Vector3_t3525329789  L_9 = Transform_get_localPosition_m668140784(L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		float L_10 = (&V_2)->get_x_1();
		int32_t L_11 = __this->get_itemSize_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1597001355_il2cpp_TypeInfo_var);
		int32_t L_12 = Mathf_RoundToInt_m3163545820(NULL /*static, unused*/, ((float)((float)L_10/(float)(((float)((float)L_11))))), /*hidden argument*/NULL);
		G_B4_0 = L_12;
	}

IL_0057:
	{
		V_0 = G_B4_0;
		OnInitializeItem_t2008047266 * L_13 = __this->get_onInitializeItem_7();
		Transform_t284553113 * L_14 = ___item0;
		NullCheck(L_14);
		GameObject_t4012695102 * L_15 = Component_get_gameObject_m1170635899(L_14, /*hidden argument*/NULL);
		int32_t L_16 = ___index1;
		int32_t L_17 = V_0;
		NullCheck(L_13);
		OnInitializeItem_Invoke_m2616578991(L_13, L_15, L_16, L_17, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void UIWrapContent/OnInitializeItem::.ctor(System.Object,System.IntPtr)
extern "C"  void OnInitializeItem__ctor_m953163837 (OnInitializeItem_t2008047266 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->method);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UIWrapContent/OnInitializeItem::Invoke(UnityEngine.GameObject,System.Int32,System.Int32)
extern "C"  void OnInitializeItem_Invoke_m2616578991 (OnInitializeItem_t2008047266 * __this, GameObject_t4012695102 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		OnInitializeItem_Invoke_m2616578991((OnInitializeItem_t2008047266 *)__this->get_prev_9(),___go0, ___wrapIndex1, ___realIndex2, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Il2CppObject *, void* __this, GameObject_t4012695102 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,il2cpp_codegen_get_delegate_this(__this),___go0, ___wrapIndex1, ___realIndex2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, GameObject_t4012695102 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(il2cpp_codegen_get_delegate_this(__this),___go0, ___wrapIndex1, ___realIndex2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, int32_t ___wrapIndex1, int32_t ___realIndex2, const MethodInfo* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___go0, ___wrapIndex1, ___realIndex2,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
extern "C" void pinvoke_delegate_wrapper_OnInitializeItem_t2008047266(Il2CppObject* delegate, GameObject_t4012695102 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2)
{
	// Marshaling of parameter '___go0' to native representation
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'UnityEngine.GameObject'."));
}
// System.IAsyncResult UIWrapContent/OnInitializeItem::BeginInvoke(UnityEngine.GameObject,System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t OnInitializeItem_BeginInvoke_m3195738938_MetadataUsageId;
extern "C"  Il2CppObject * OnInitializeItem_BeginInvoke_m3195738938 (OnInitializeItem_t2008047266 * __this, GameObject_t4012695102 * ___go0, int32_t ___wrapIndex1, int32_t ___realIndex2, AsyncCallback_t1363551830 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OnInitializeItem_BeginInvoke_m3195738938_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[4] = {0};
	__d_args[0] = ___go0;
	__d_args[1] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___wrapIndex1);
	__d_args[2] = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &___realIndex2);
	return (Il2CppObject *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback3, (Il2CppObject*)___object4);
}
// System.Void UIWrapContent/OnInitializeItem::EndInvoke(System.IAsyncResult)
extern "C"  void OnInitializeItem_EndInvoke_m2249144525 (OnInitializeItem_t2008047266 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void WindowAutoYaw::.ctor()
extern "C"  void WindowAutoYaw__ctor_m560393051 (WindowAutoYaw_t1835239440 * __this, const MethodInfo* method)
{
	{
		__this->set_yawAmount_4((20.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowAutoYaw::OnDisable()
extern "C"  void WindowAutoYaw_OnDisable_m16980546 (WindowAutoYaw_t1835239440 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = __this->get_mTrans_5();
		Quaternion_t1891715979  L_1 = Quaternion_get_identity_m1743882806(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localRotation_m3719981474(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowAutoYaw::OnEnable()
extern Il2CppClass* NGUITools_t237277134_il2cpp_TypeInfo_var;
extern const uint32_t WindowAutoYaw_OnEnable_m2203081291_MetadataUsageId;
extern "C"  void WindowAutoYaw_OnEnable_m2203081291 (WindowAutoYaw_t1835239440 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WindowAutoYaw_OnEnable_m2203081291_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t3533968274 * L_0 = __this->get_uiCamera_3();
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		GameObject_t4012695102 * L_2 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = GameObject_get_layer_m1648550306(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(NGUITools_t237277134_il2cpp_TypeInfo_var);
		Camera_t3533968274 * L_4 = NGUITools_FindCameraForLayer_m3244779261(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		__this->set_uiCamera_3(L_4);
	}

IL_0027:
	{
		Transform_t284553113 * L_5 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_mTrans_5(L_5);
		return;
	}
}
// System.Void WindowAutoYaw::Update()
extern "C"  void WindowAutoYaw_Update_m1919177490 (WindowAutoYaw_t1835239440 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t3533968274 * L_0 = __this->get_uiCamera_3();
		bool L_1 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_005c;
		}
	}
	{
		Camera_t3533968274 * L_2 = __this->get_uiCamera_3();
		Transform_t284553113 * L_3 = __this->get_mTrans_5();
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t3525329789  L_5 = Camera_WorldToViewportPoint_m3480725126(L_2, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t284553113 * L_6 = __this->get_mTrans_5();
		float L_7 = (&V_0)->get_x_1();
		float L_8 = __this->get_yawAmount_4();
		Quaternion_t1891715979  L_9 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (0.0f), ((float)((float)((float)((float)((float)((float)L_7*(float)(2.0f)))-(float)(1.0f)))*(float)L_8)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_localRotation_m3719981474(L_6, L_9, /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Void WindowDragTilt::.ctor()
extern "C"  void WindowDragTilt__ctor_m4101828474 (WindowDragTilt_t3072284225 * __this, const MethodInfo* method)
{
	{
		__this->set_degrees_3((30.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WindowDragTilt::OnEnable()
extern "C"  void WindowDragTilt_OnEnable_m3529108940 (WindowDragTilt_t3072284225 * __this, const MethodInfo* method)
{
	{
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_mTrans_5(L_0);
		Transform_t284553113 * L_1 = __this->get_mTrans_5();
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		__this->set_mLastPos_4(L_2);
		return;
	}
}
// System.Void WindowDragTilt::Update()
extern "C"  void WindowDragTilt_Update_m34525907 (WindowDragTilt_t3072284225 * __this, const MethodInfo* method)
{
	Vector3_t3525329789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t284553113 * L_0 = __this->get_mTrans_5();
		NullCheck(L_0);
		Vector3_t3525329789  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Vector3_t3525329789  L_2 = __this->get_mLastPos_4();
		Vector3_t3525329789  L_3 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		Transform_t284553113 * L_4 = __this->get_mTrans_5();
		NullCheck(L_4);
		Vector3_t3525329789  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		__this->set_mLastPos_4(L_5);
		float L_6 = __this->get_mAngle_6();
		float L_7 = (&V_0)->get_x_1();
		float L_8 = __this->get_degrees_3();
		__this->set_mAngle_6(((float)((float)L_6+(float)((float)((float)L_7*(float)L_8)))));
		float L_9 = __this->get_mAngle_6();
		float L_10 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = NGUIMath_SpringLerp_m3176191852(NULL /*static, unused*/, L_9, (0.0f), (20.0f), L_10, /*hidden argument*/NULL);
		__this->set_mAngle_6(L_11);
		Transform_t284553113 * L_12 = __this->get_mTrans_5();
		float L_13 = __this->get_mAngle_6();
		Quaternion_t1891715979  L_14 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (0.0f), (0.0f), ((-L_13)), /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_set_localRotation_m3719981474(L_12, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zone::.ctor()
extern "C"  void Zone__ctor_m2295658575 (Zone_t2791372 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zone::Awake()
extern const MethodInfo* Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var;
extern const uint32_t Zone_Awake_m2533263794_MetadataUsageId;
extern "C"  void Zone_Awake_m2533263794 (Zone_t2791372 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Zone_Awake_m2533263794_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t284553113 * L_0 = Component_GetComponent_TisTransform_t284553113_m811718087(__this, /*hidden argument*/Component_GetComponent_TisTransform_t284553113_m811718087_MethodInfo_var);
		__this->set_m_tr_3(L_0);
		return;
	}
}
// System.Void Zone::Update()
extern "C"  void Zone_Update_m4172801182 (Zone_t2791372 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Zone::Init(DataZone)
extern Il2CppClass* Material_t1886596500_il2cpp_TypeInfo_var;
extern Il2CppClass* GamePlay_t2590336614_il2cpp_TypeInfo_var;
extern Il2CppClass* Common_t2024019467_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t1092684080_m4102086307_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisRigidbody_t1972007546_m686365494_MethodInfo_var;
extern const uint32_t Zone_Init_m1862295631_MetadataUsageId;
extern "C"  void Zone_Init_m1862295631 (Zone_t2791372 * __this, DataZone_t1853884054 * ___data0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Zone_Init_m1862295631_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Renderer_t1092684080 * V_0 = NULL;
	DataChapter_t925677859 * V_1 = NULL;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		DataZone_t1853884054 * L_0 = ___data0;
		__this->set_m_data_2(L_0);
		DataZone_t1853884054 * L_1 = ___data0;
		NullCheck(L_1);
		String_t* L_2 = L_1->get_name_1();
		Object_set_name_m1123518500(__this, L_2, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_3 = GameObject_CreatePrimitive_m3259337130(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		__this->set_m_zone_4(L_3);
		GameObject_t4012695102 * L_4 = __this->get_m_zone_4();
		NullCheck(L_4);
		Renderer_t1092684080 * L_5 = GameObject_GetComponent_TisRenderer_t1092684080_m4102086307(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t1092684080_m4102086307_MethodInfo_var);
		V_0 = L_5;
		Renderer_t1092684080 * L_6 = V_0;
		DataZone_t1853884054 * L_7 = ___data0;
		NullCheck(L_7);
		String_t* L_8 = L_7->get_material_3();
		Object_t3878351788 * L_9 = Resources_Load_m2187391845(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Renderer_set_material_m1012580896(L_6, ((Material_t1886596500 *)IsInstClass(L_9, Material_t1886596500_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		GameObject_t4012695102 * L_10 = __this->get_m_zone_4();
		NullCheck(L_10);
		Transform_t284553113 * L_11 = GameObject_GetComponent_TisTransform_t284553113_m3795369772(L_10, /*hidden argument*/GameObject_GetComponent_TisTransform_t284553113_m3795369772_MethodInfo_var);
		__this->set_m_zone_tr_5(L_11);
		Transform_t284553113 * L_12 = __this->get_m_zone_tr_5();
		Transform_t284553113 * L_13 = __this->get_m_tr_3();
		NullCheck(L_12);
		Transform_set_parent_m3231272063(L_12, L_13, /*hidden argument*/NULL);
		Transform_t284553113 * L_14 = __this->get_m_zone_tr_5();
		Quaternion_t1891715979  L_15 = Quaternion_Euler_m1204688217(NULL /*static, unused*/, (90.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_14);
		Transform_set_rotation_m1525803229(L_14, L_15, /*hidden argument*/NULL);
		Transform_t284553113 * L_16 = __this->get_m_zone_tr_5();
		Vector3_t3525329789  L_17 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataZone_t1853884054 * L_18 = ___data0;
		NullCheck(L_18);
		float L_19 = L_18->get_size_4();
		Vector3_t3525329789  L_20 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		NullCheck(L_16);
		Transform_set_localScale_m310756934(L_16, L_20, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_21 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_21);
		Rigidbody_t1972007546 * L_22 = GameObject_AddComponent_TisRigidbody_t1972007546_m686365494(L_21, /*hidden argument*/GameObject_AddComponent_TisRigidbody_t1972007546_m686365494_MethodInfo_var);
		__this->set_m_rigidBody_6(L_22);
		Rigidbody_t1972007546 * L_23 = __this->get_m_rigidBody_6();
		NullCheck(L_23);
		Rigidbody_set_useGravity_m2620827635(L_23, (bool)0, /*hidden argument*/NULL);
		Rigidbody_t1972007546 * L_24 = __this->get_m_rigidBody_6();
		NullCheck(L_24);
		Rigidbody_set_isKinematic_m294703295(L_24, (bool)1, /*hidden argument*/NULL);
		GamePlay_t2590336614 * L_25 = ((GamePlay_t2590336614_StaticFields*)GamePlay_t2590336614_il2cpp_TypeInfo_var->static_fields)->get_instance_2();
		NullCheck(L_25);
		DataChapter_t925677859 * L_26 = GamePlay_get_dataChapter_m1400870973(L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		DataChapter_t925677859 * L_27 = V_1;
		NullCheck(L_27);
		Rect_t1525428817 * L_28 = L_27->get_address_of_rect_2();
		float L_29 = Rect_get_x_m982385354(L_28, /*hidden argument*/NULL);
		DataZone_t1853884054 * L_30 = ___data0;
		NullCheck(L_30);
		float L_31 = L_30->get_size_4();
		float L_32 = Random_get_value_m2402066692(NULL /*static, unused*/, /*hidden argument*/NULL);
		DataChapter_t925677859 * L_33 = V_1;
		NullCheck(L_33);
		Rect_t1525428817 * L_34 = L_33->get_address_of_rect_2();
		float L_35 = Rect_get_width_m2824209432(L_34, /*hidden argument*/NULL);
		DataZone_t1853884054 * L_36 = ___data0;
		NullCheck(L_36);
		float L_37 = L_36->get_size_4();
		V_2 = ((float)((float)((float)((float)L_29+(float)((float)((float)L_31/(float)(2.0f)))))+(float)((float)((float)L_32*(float)((float)((float)L_35-(float)L_37))))));
		DataChapter_t925677859 * L_38 = V_1;
		NullCheck(L_38);
		Rect_t1525428817 * L_39 = L_38->get_address_of_rect_2();
		float L_40 = Rect_get_y_m982386315(L_39, /*hidden argument*/NULL);
		DataChapter_t925677859 * L_41 = V_1;
		NullCheck(L_41);
		Rect_t1525428817 * L_42 = L_41->get_address_of_rect_2();
		float L_43 = Rect_get_height_m2154960823(L_42, /*hidden argument*/NULL);
		DataSetting_t2172603174 * L_44 = Data_get_settings_m2852704140(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_44);
		float L_45 = L_44->get_BG_SCROLL_TOP_GAP_1();
		V_3 = ((float)((float)((float)((float)L_40+(float)L_43))+(float)L_45));
		Transform_t284553113 * L_46 = __this->get_m_tr_3();
		float L_47 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Common_t2024019467_il2cpp_TypeInfo_var);
		float L_48 = ((Common_t2024019467_StaticFields*)Common_t2024019467_il2cpp_TypeInfo_var->static_fields)->get_POS_ZONE_Y_4();
		float L_49 = V_3;
		Vector3_t3525329789  L_50;
		memset(&L_50, 0, sizeof(L_50));
		Vector3__ctor_m2926210380(&L_50, L_47, L_48, L_49, /*hidden argument*/NULL);
		NullCheck(L_46);
		Transform_set_position_m3111394108(L_46, L_50, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zone::IsHit(UnityEngine.Vector3)
extern "C"  bool Zone_IsHit_m458776983 (Zone_t2791372 * __this, Vector3_t3525329789  ___point0, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Void ZoneCircle::.ctor()
extern "C"  void ZoneCircle__ctor_m1449284703 (ZoneCircle_t627455932 * __this, const MethodInfo* method)
{
	{
		Zone__ctor_m2295658575(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZoneCircle::Awake()
extern "C"  void ZoneCircle_Awake_m1686889922 (ZoneCircle_t627455932 * __this, const MethodInfo* method)
{
	{
		Zone_Awake_m2533263794(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ZoneCircle::Update()
extern "C"  void ZoneCircle_Update_m3705014926 (ZoneCircle_t627455932 * __this, const MethodInfo* method)
{
	{
		Zone_Update_m4172801182(__this, /*hidden argument*/NULL);
		Transform_t284553113 * L_0 = ((Zone_t2791372 *)__this)->get_m_zone_tr_5();
		Vector3_t3525329789  L_1 = Vector3_get_back_m1326515313(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m4090005356(L_0, L_1, ((float)((float)L_2*(float)(50.0f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ZoneCircle::IsHit(UnityEngine.Vector3)
extern "C"  bool ZoneCircle_IsHit_m3374716039 (ZoneCircle_t627455932 * __this, Vector3_t3525329789  ___point0, const MethodInfo* method)
{
	{
		Vector3_t3525329789  L_0 = ___point0;
		Transform_t284553113 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t3525329789  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_0, L_2, /*hidden argument*/NULL);
		DataZone_t1853884054 * L_4 = ((Zone_t2791372 *)__this)->get_m_data_2();
		NullCheck(L_4);
		float L_5 = L_4->get_size_4();
		if ((!(((float)L_3) <= ((float)((float)((float)L_5/(float)(2.0f)))))))
		{
			goto IL_0029;
		}
	}
	{
		return (bool)1;
	}

IL_0029:
	{
		return (bool)0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
