﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Diver
struct Diver_t66044126;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void Diver::.ctor()
extern "C"  void Diver__ctor_m225321549 (Diver_t66044126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Diver::get_IsMonster()
extern "C"  bool Diver_get_IsMonster_m1684747526 (Diver_t66044126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Diver::get_MonsterID()
extern "C"  uint32_t Diver_get_MonsterID_m3449010450 (Diver_t66044126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Diver Diver::Alloc()
extern "C"  Diver_t66044126 * Diver_Alloc_m547669845 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Diver::Awake()
extern "C"  void Diver_Awake_m462926768 (Diver_t66044126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Diver::Update()
extern "C"  void Diver_Update_m121895520 (Diver_t66044126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Diver::Init(System.Single)
extern "C"  void Diver_Init_m156627620 (Diver_t66044126 * __this, float ___size0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Diver::SetMonster(System.UInt32)
extern "C"  void Diver_SetMonster_m2845158763 (Diver_t66044126 * __this, uint32_t ___monster_id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Diver::OnEndMove()
extern "C"  void Diver_OnEndMove_m4170788760 (Diver_t66044126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Diver::UpdateMove(UnityEngine.Vector3)
extern "C"  void Diver_UpdateMove_m2930291112 (Diver_t66044126 * __this, Vector3_t3525329789  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Diver::Hit(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Diver_Hit_m1917533498 (Diver_t66044126 * __this, Vector3_t3525329789  ___point0, Vector3_t3525329789  ___collider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
