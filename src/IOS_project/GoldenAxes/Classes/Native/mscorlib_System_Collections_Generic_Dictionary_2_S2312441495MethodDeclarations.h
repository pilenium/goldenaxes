﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>
struct ShimEnumerator_t2312441495;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t881716807;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry130027246.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4056223825_gshared (ShimEnumerator_t2312441495 * __this, Dictionary_2_t881716807 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m4056223825(__this, ___host0, method) ((  void (*) (ShimEnumerator_t2312441495 *, Dictionary_2_t881716807 *, const MethodInfo*))ShimEnumerator__ctor_m4056223825_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1946435120_gshared (ShimEnumerator_t2312441495 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1946435120(__this, method) ((  bool (*) (ShimEnumerator_t2312441495 *, const MethodInfo*))ShimEnumerator_MoveNext_m1946435120_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::get_Entry()
extern "C"  DictionaryEntry_t130027246  ShimEnumerator_get_Entry_m993214820_gshared (ShimEnumerator_t2312441495 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m993214820(__this, method) ((  DictionaryEntry_t130027246  (*) (ShimEnumerator_t2312441495 *, const MethodInfo*))ShimEnumerator_get_Entry_m993214820_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3030715583_gshared (ShimEnumerator_t2312441495 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3030715583(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2312441495 *, const MethodInfo*))ShimEnumerator_get_Key_m3030715583_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1578284945_gshared (ShimEnumerator_t2312441495 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1578284945(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2312441495 *, const MethodInfo*))ShimEnumerator_get_Value_m1578284945_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2033067481_gshared (ShimEnumerator_t2312441495 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2033067481(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t2312441495 *, const MethodInfo*))ShimEnumerator_get_Current_m2033067481_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.LogType,UnityEngine.Color>::Reset()
extern "C"  void ShimEnumerator_Reset_m4069900579_gshared (ShimEnumerator_t2312441495 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m4069900579(__this, method) ((  void (*) (ShimEnumerator_t2312441495 *, const MethodInfo*))ShimEnumerator_Reset_m4069900579_gshared)(__this, method)
