﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DownloadTexture/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t4138896567;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DownloadTexture/<Start>c__Iterator0::.ctor()
extern "C"  void U3CStartU3Ec__Iterator0__ctor_m285235664 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadTexture/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3620367874 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DownloadTexture/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3947672982 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DownloadTexture/<Start>c__Iterator0::MoveNext()
extern "C"  bool U3CStartU3Ec__Iterator0_MoveNext_m2313841700 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadTexture/<Start>c__Iterator0::Dispose()
extern "C"  void U3CStartU3Ec__Iterator0_Dispose_m3426816525 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DownloadTexture/<Start>c__Iterator0::Reset()
extern "C"  void U3CStartU3Ec__Iterator0_Reset_m2226635901 (U3CStartU3Ec__Iterator0_t4138896567 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
