﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MonsterProgress
struct MonsterProgress_t2391190535;

#include "codegen/il2cpp-codegen.h"

// System.Void MonsterProgress::.ctor()
extern "C"  void MonsterProgress__ctor_m1080341316 (MonsterProgress_t2391190535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterProgress::Awake()
extern "C"  void MonsterProgress_Awake_m1317946535 (MonsterProgress_t2391190535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterProgress::SetScale(System.Single)
extern "C"  void MonsterProgress_SetScale_m1760754851 (MonsterProgress_t2391190535 * __this, float ___scale0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MonsterProgress::SetProgress(System.Single)
extern "C"  void MonsterProgress_SetProgress_m3285678266 (MonsterProgress_t2391190535 * __this, float ___progress0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
