﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<DataZone>
struct List_1_t2650843023;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataZones
struct  DataZones_t1635830941  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<DataZone> DataZones::zones
	List_1_t2650843023 * ___zones_0;

public:
	inline static int32_t get_offset_of_zones_0() { return static_cast<int32_t>(offsetof(DataZones_t1635830941, ___zones_0)); }
	inline List_1_t2650843023 * get_zones_0() const { return ___zones_0; }
	inline List_1_t2650843023 ** get_address_of_zones_0() { return &___zones_0; }
	inline void set_zones_0(List_1_t2650843023 * value)
	{
		___zones_0 = value;
		Il2CppCodeGenWriteBarrier(&___zones_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
