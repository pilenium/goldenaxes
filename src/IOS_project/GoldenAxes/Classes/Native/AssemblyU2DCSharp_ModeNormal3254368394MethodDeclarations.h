﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModeNormal
struct ModeNormal_t3254368394;

#include "codegen/il2cpp-codegen.h"

// System.Void ModeNormal::.ctor()
extern "C"  void ModeNormal__ctor_m2060966737 (ModeNormal_t3254368394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeNormal::Init()
extern "C"  void ModeNormal_Init_m156566755 (ModeNormal_t3254368394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeNormal::Clear()
extern "C"  void ModeNormal_Clear_m3762067324 (ModeNormal_t3254368394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeNormal::Update(System.Single)
extern "C"  void ModeNormal_Update_m1676707151 (ModeNormal_t3254368394 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeNormal::HitWater()
extern "C"  void ModeNormal_HitWater_m2688495383 (ModeNormal_t3254368394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeNormal::HitDiver()
extern "C"  void ModeNormal_HitDiver_m3236674878 (ModeNormal_t3254368394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeNormal::HitMonster()
extern "C"  void ModeNormal_HitMonster_m1267423578 (ModeNormal_t3254368394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeNormal::KillMonster()
extern "C"  void ModeNormal_KillMonster_m2358050187 (ModeNormal_t3254368394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeNormal::PatternSuccess()
extern "C"  void ModeNormal_PatternSuccess_m4261168870 (ModeNormal_t3254368394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeNormal::PatternFail()
extern "C"  void ModeNormal_PatternFail_m289007933 (ModeNormal_t3254368394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
