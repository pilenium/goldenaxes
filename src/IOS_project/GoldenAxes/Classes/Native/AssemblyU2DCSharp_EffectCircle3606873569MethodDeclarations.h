﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectCircle
struct EffectCircle_t3606873569;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectCircle::.ctor()
extern "C"  void EffectCircle__ctor_m2828892634 (EffectCircle_t3606873569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCircle::Awake()
extern "C"  void EffectCircle_Awake_m3066497853 (EffectCircle_t3606873569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCircle::Update()
extern "C"  void EffectCircle_Update_m3523187827 (EffectCircle_t3606873569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
