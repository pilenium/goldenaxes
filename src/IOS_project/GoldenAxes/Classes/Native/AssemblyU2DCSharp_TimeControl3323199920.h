﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Control2616196413.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeControl
struct  TimeControl_t3323199920  : public Control_t2616196413
{
public:
	// System.Single TimeControl::m_power
	float ___m_power_3;
	// System.Single TimeControl::m_deltaPower
	float ___m_deltaPower_4;

public:
	inline static int32_t get_offset_of_m_power_3() { return static_cast<int32_t>(offsetof(TimeControl_t3323199920, ___m_power_3)); }
	inline float get_m_power_3() const { return ___m_power_3; }
	inline float* get_address_of_m_power_3() { return &___m_power_3; }
	inline void set_m_power_3(float value)
	{
		___m_power_3 = value;
	}

	inline static int32_t get_offset_of_m_deltaPower_4() { return static_cast<int32_t>(offsetof(TimeControl_t3323199920, ___m_deltaPower_4)); }
	inline float get_m_deltaPower_4() const { return ___m_deltaPower_4; }
	inline float* get_address_of_m_deltaPower_4() { return &___m_deltaPower_4; }
	inline void set_m_deltaPower_4(float value)
	{
		___m_deltaPower_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
