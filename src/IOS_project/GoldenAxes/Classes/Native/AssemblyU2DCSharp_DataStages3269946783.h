﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<DataStageChapter>
struct List_1_t2537132370;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataStages
struct  DataStages_t3269946783  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<DataStageChapter> DataStages::chapters
	List_1_t2537132370 * ___chapters_0;

public:
	inline static int32_t get_offset_of_chapters_0() { return static_cast<int32_t>(offsetof(DataStages_t3269946783, ___chapters_0)); }
	inline List_1_t2537132370 * get_chapters_0() const { return ___chapters_0; }
	inline List_1_t2537132370 ** get_address_of_chapters_0() { return &___chapters_0; }
	inline void set_chapters_0(List_1_t2537132370 * value)
	{
		___chapters_0 = value;
		Il2CppCodeGenWriteBarrier(&___chapters_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
