﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pattern>
struct List_1_t1670521961;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t190145395;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PatternManager
struct  PatternManager_t2286577181  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Pattern> PatternManager::m_patterns
	List_1_t1670521961 * ___m_patterns_0;

public:
	inline static int32_t get_offset_of_m_patterns_0() { return static_cast<int32_t>(offsetof(PatternManager_t2286577181, ___m_patterns_0)); }
	inline List_1_t1670521961 * get_m_patterns_0() const { return ___m_patterns_0; }
	inline List_1_t1670521961 ** get_address_of_m_patterns_0() { return &___m_patterns_0; }
	inline void set_m_patterns_0(List_1_t1670521961 * value)
	{
		___m_patterns_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_patterns_0, value);
	}
};

struct PatternManager_t2286577181_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PatternManager::<>f__switch$map5
	Dictionary_2_t190145395 * ___U3CU3Ef__switchU24map5_1;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map5_1() { return static_cast<int32_t>(offsetof(PatternManager_t2286577181_StaticFields, ___U3CU3Ef__switchU24map5_1)); }
	inline Dictionary_2_t190145395 * get_U3CU3Ef__switchU24map5_1() const { return ___U3CU3Ef__switchU24map5_1; }
	inline Dictionary_2_t190145395 ** get_address_of_U3CU3Ef__switchU24map5_1() { return &___U3CU3Ef__switchU24map5_1; }
	inline void set_U3CU3Ef__switchU24map5_1(Dictionary_2_t190145395 * value)
	{
		___U3CU3Ef__switchU24map5_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map5_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
