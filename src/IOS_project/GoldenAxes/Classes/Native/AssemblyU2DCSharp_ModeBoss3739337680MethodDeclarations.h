﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ModeBoss
struct ModeBoss_t3739337680;

#include "codegen/il2cpp-codegen.h"

// System.Void ModeBoss::.ctor()
extern "C"  void ModeBoss__ctor_m3281405131 (ModeBoss_t3739337680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeBoss::Init()
extern "C"  void ModeBoss_Init_m1858503721 (ModeBoss_t3739337680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeBoss::Clear()
extern "C"  void ModeBoss_Clear_m687538422 (ModeBoss_t3739337680 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ModeBoss::Update(System.Single)
extern "C"  void ModeBoss_Update_m1498665289 (ModeBoss_t3739337680 * __this, float ___dt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
