﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectParticle
struct EffectParticle_t2854106967;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectParticle::.ctor()
extern "C"  void EffectParticle__ctor_m167438116 (EffectParticle_t2854106967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectParticle::Awake()
extern "C"  void EffectParticle_Awake_m405043335 (EffectParticle_t2854106967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
