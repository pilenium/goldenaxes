﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera/OnSchemeChange
struct OnSchemeChange_t2442067284;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UICamera/OnSchemeChange::.ctor(System.Object,System.IntPtr)
extern "C"  void OnSchemeChange__ctor_m4246793249 (OnSchemeChange_t2442067284 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera/OnSchemeChange::Invoke()
extern "C"  void OnSchemeChange_Invoke_m303300539 (OnSchemeChange_t2442067284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_OnSchemeChange_t2442067284(Il2CppObject* delegate);
// System.IAsyncResult UICamera/OnSchemeChange::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnSchemeChange_BeginInvoke_m2923559792 (OnSchemeChange_t2442067284 * __this, AsyncCallback_t1363551830 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera/OnSchemeChange::EndInvoke(System.IAsyncResult)
extern "C"  void OnSchemeChange_EndInvoke_m3427374769 (OnSchemeChange_t2442067284 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
