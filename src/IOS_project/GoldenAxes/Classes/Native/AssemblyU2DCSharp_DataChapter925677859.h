﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1765447871;

#include "mscorlib_System_Object837106420.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Quaternion1891715979.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DataChapter
struct  DataChapter_t925677859  : public Il2CppObject
{
public:
	// System.Int32 DataChapter::chapter
	int32_t ___chapter_0;
	// System.String DataChapter::name
	String_t* ___name_1;
	// UnityEngine.Rect DataChapter::rect
	Rect_t1525428817  ___rect_2;
	// System.Single DataChapter::speed
	float ___speed_3;
	// System.Collections.Generic.List`1<System.String> DataChapter::bg
	List_1_t1765447871 * ___bg_4;
	// System.String DataChapter::effect_drop
	String_t* ___effect_drop_5;
	// System.String DataChapter::effect_wave
	String_t* ___effect_wave_6;
	// System.String DataChapter::effect_god
	String_t* ___effect_god_7;
	// System.String DataChapter::diver
	String_t* ___diver_8;
	// System.String DataChapter::surface_mat
	String_t* ___surface_mat_9;
	// System.String DataChapter::water_mat
	String_t* ___water_mat_10;
	// System.Single DataChapter::water_mat_width
	float ___water_mat_width_11;
	// System.Single DataChapter::water_width
	float ___water_width_12;
	// System.Single DataChapter::water_height
	float ___water_height_13;
	// UnityEngine.Vector3 DataChapter::cameraPosition
	Vector3_t3525329789  ___cameraPosition_14;
	// UnityEngine.Quaternion DataChapter::cameraRotation
	Quaternion_t1891715979  ___cameraRotation_15;

public:
	inline static int32_t get_offset_of_chapter_0() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___chapter_0)); }
	inline int32_t get_chapter_0() const { return ___chapter_0; }
	inline int32_t* get_address_of_chapter_0() { return &___chapter_0; }
	inline void set_chapter_0(int32_t value)
	{
		___chapter_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_rect_2() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___rect_2)); }
	inline Rect_t1525428817  get_rect_2() const { return ___rect_2; }
	inline Rect_t1525428817 * get_address_of_rect_2() { return &___rect_2; }
	inline void set_rect_2(Rect_t1525428817  value)
	{
		___rect_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}

	inline static int32_t get_offset_of_bg_4() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___bg_4)); }
	inline List_1_t1765447871 * get_bg_4() const { return ___bg_4; }
	inline List_1_t1765447871 ** get_address_of_bg_4() { return &___bg_4; }
	inline void set_bg_4(List_1_t1765447871 * value)
	{
		___bg_4 = value;
		Il2CppCodeGenWriteBarrier(&___bg_4, value);
	}

	inline static int32_t get_offset_of_effect_drop_5() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___effect_drop_5)); }
	inline String_t* get_effect_drop_5() const { return ___effect_drop_5; }
	inline String_t** get_address_of_effect_drop_5() { return &___effect_drop_5; }
	inline void set_effect_drop_5(String_t* value)
	{
		___effect_drop_5 = value;
		Il2CppCodeGenWriteBarrier(&___effect_drop_5, value);
	}

	inline static int32_t get_offset_of_effect_wave_6() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___effect_wave_6)); }
	inline String_t* get_effect_wave_6() const { return ___effect_wave_6; }
	inline String_t** get_address_of_effect_wave_6() { return &___effect_wave_6; }
	inline void set_effect_wave_6(String_t* value)
	{
		___effect_wave_6 = value;
		Il2CppCodeGenWriteBarrier(&___effect_wave_6, value);
	}

	inline static int32_t get_offset_of_effect_god_7() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___effect_god_7)); }
	inline String_t* get_effect_god_7() const { return ___effect_god_7; }
	inline String_t** get_address_of_effect_god_7() { return &___effect_god_7; }
	inline void set_effect_god_7(String_t* value)
	{
		___effect_god_7 = value;
		Il2CppCodeGenWriteBarrier(&___effect_god_7, value);
	}

	inline static int32_t get_offset_of_diver_8() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___diver_8)); }
	inline String_t* get_diver_8() const { return ___diver_8; }
	inline String_t** get_address_of_diver_8() { return &___diver_8; }
	inline void set_diver_8(String_t* value)
	{
		___diver_8 = value;
		Il2CppCodeGenWriteBarrier(&___diver_8, value);
	}

	inline static int32_t get_offset_of_surface_mat_9() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___surface_mat_9)); }
	inline String_t* get_surface_mat_9() const { return ___surface_mat_9; }
	inline String_t** get_address_of_surface_mat_9() { return &___surface_mat_9; }
	inline void set_surface_mat_9(String_t* value)
	{
		___surface_mat_9 = value;
		Il2CppCodeGenWriteBarrier(&___surface_mat_9, value);
	}

	inline static int32_t get_offset_of_water_mat_10() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___water_mat_10)); }
	inline String_t* get_water_mat_10() const { return ___water_mat_10; }
	inline String_t** get_address_of_water_mat_10() { return &___water_mat_10; }
	inline void set_water_mat_10(String_t* value)
	{
		___water_mat_10 = value;
		Il2CppCodeGenWriteBarrier(&___water_mat_10, value);
	}

	inline static int32_t get_offset_of_water_mat_width_11() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___water_mat_width_11)); }
	inline float get_water_mat_width_11() const { return ___water_mat_width_11; }
	inline float* get_address_of_water_mat_width_11() { return &___water_mat_width_11; }
	inline void set_water_mat_width_11(float value)
	{
		___water_mat_width_11 = value;
	}

	inline static int32_t get_offset_of_water_width_12() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___water_width_12)); }
	inline float get_water_width_12() const { return ___water_width_12; }
	inline float* get_address_of_water_width_12() { return &___water_width_12; }
	inline void set_water_width_12(float value)
	{
		___water_width_12 = value;
	}

	inline static int32_t get_offset_of_water_height_13() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___water_height_13)); }
	inline float get_water_height_13() const { return ___water_height_13; }
	inline float* get_address_of_water_height_13() { return &___water_height_13; }
	inline void set_water_height_13(float value)
	{
		___water_height_13 = value;
	}

	inline static int32_t get_offset_of_cameraPosition_14() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___cameraPosition_14)); }
	inline Vector3_t3525329789  get_cameraPosition_14() const { return ___cameraPosition_14; }
	inline Vector3_t3525329789 * get_address_of_cameraPosition_14() { return &___cameraPosition_14; }
	inline void set_cameraPosition_14(Vector3_t3525329789  value)
	{
		___cameraPosition_14 = value;
	}

	inline static int32_t get_offset_of_cameraRotation_15() { return static_cast<int32_t>(offsetof(DataChapter_t925677859, ___cameraRotation_15)); }
	inline Quaternion_t1891715979  get_cameraRotation_15() const { return ___cameraRotation_15; }
	inline Quaternion_t1891715979 * get_address_of_cameraRotation_15() { return &___cameraRotation_15; }
	inline void set_cameraRotation_15(Quaternion_t1891715979  value)
	{
		___cameraRotation_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
