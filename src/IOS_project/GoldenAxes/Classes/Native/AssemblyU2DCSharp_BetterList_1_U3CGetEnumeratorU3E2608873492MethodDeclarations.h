﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>
struct U3CGetEnumeratorU3Ec__Iterator3_t2608873492;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m2318871836_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3__ctor_m2318871836(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3__ctor_m2318871836_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector3_t3525329789  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2132437475_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2132437475(__this, method) ((  Vector3_t3525329789  (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2132437475_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m923722250_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m923722250(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m923722250_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m173284952_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m173284952(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m173284952_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3541058137_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3541058137(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3541058137_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector3>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m4260272073_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Reset_m4260272073(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873492 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Reset_m4260272073_gshared)(__this, method)
