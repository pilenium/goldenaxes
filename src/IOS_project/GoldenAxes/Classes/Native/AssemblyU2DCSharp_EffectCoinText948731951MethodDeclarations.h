﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectCoinText
struct EffectCoinText_t948731951;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"

// System.Void EffectCoinText::.ctor()
extern "C"  void EffectCoinText__ctor_m2635403596 (EffectCoinText_t948731951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoinText::Awake()
extern "C"  void EffectCoinText_Awake_m2873008815 (EffectCoinText_t948731951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoinText::InitCoin(System.Int32,UnityEngine.Vector3)
extern "C"  void EffectCoinText_InitCoin_m2759943901 (EffectCoinText_t948731951 * __this, int32_t ___coin0, Vector3_t3525329789  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoinText::Update()
extern "C"  void EffectCoinText_Update_m1819994945 (EffectCoinText_t948731951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoinText::UpdateMove()
extern "C"  void EffectCoinText_UpdateMove_m3587588850 (EffectCoinText_t948731951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCoinText::UpdateDisappear()
extern "C"  void EffectCoinText_UpdateDisappear_m2856886660 (EffectCoinText_t948731951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
