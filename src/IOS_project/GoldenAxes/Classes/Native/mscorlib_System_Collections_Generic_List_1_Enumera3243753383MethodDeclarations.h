﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4014815677MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Diver>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m4216257861(__this, ___l0, method) ((  void (*) (Enumerator_t3243753383 *, List_1_t863003095 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Diver>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1473126381(__this, method) ((  void (*) (Enumerator_t3243753383 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Diver>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2225493593(__this, method) ((  Il2CppObject * (*) (Enumerator_t3243753383 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Diver>::Dispose()
#define Enumerator_Dispose_m698838122(__this, method) ((  void (*) (Enumerator_t3243753383 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Diver>::VerifyState()
#define Enumerator_VerifyState_m1595598499(__this, method) ((  void (*) (Enumerator_t3243753383 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Diver>::MoveNext()
#define Enumerator_MoveNext_m2600701456(__this, method) ((  bool (*) (Enumerator_t3243753383 *, const MethodInfo*))Enumerator_MoveNext_m844464217_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Diver>::get_Current()
#define Enumerator_get_Current_m822182912(__this, method) ((  Diver_t66044126 * (*) (Enumerator_t3243753383 *, const MethodInfo*))Enumerator_get_Current_m4198990746_gshared)(__this, method)
