﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t284553113;
// Move
struct Move_t2404337;
// Pattern
struct Pattern_t873562992;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Character
struct  Character_t3568163593  : public MonoBehaviour_t3012272455
{
public:
	// UnityEngine.Transform Character::m_tr
	Transform_t284553113 * ___m_tr_2;
	// Move Character::m_move
	Move_t2404337 * ___m_move_3;
	// Pattern Character::m_pattern
	Pattern_t873562992 * ___m_pattern_4;

public:
	inline static int32_t get_offset_of_m_tr_2() { return static_cast<int32_t>(offsetof(Character_t3568163593, ___m_tr_2)); }
	inline Transform_t284553113 * get_m_tr_2() const { return ___m_tr_2; }
	inline Transform_t284553113 ** get_address_of_m_tr_2() { return &___m_tr_2; }
	inline void set_m_tr_2(Transform_t284553113 * value)
	{
		___m_tr_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_2, value);
	}

	inline static int32_t get_offset_of_m_move_3() { return static_cast<int32_t>(offsetof(Character_t3568163593, ___m_move_3)); }
	inline Move_t2404337 * get_m_move_3() const { return ___m_move_3; }
	inline Move_t2404337 ** get_address_of_m_move_3() { return &___m_move_3; }
	inline void set_m_move_3(Move_t2404337 * value)
	{
		___m_move_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_move_3, value);
	}

	inline static int32_t get_offset_of_m_pattern_4() { return static_cast<int32_t>(offsetof(Character_t3568163593, ___m_pattern_4)); }
	inline Pattern_t873562992 * get_m_pattern_4() const { return ___m_pattern_4; }
	inline Pattern_t873562992 ** get_address_of_m_pattern_4() { return &___m_pattern_4; }
	inline void set_m_pattern_4(Pattern_t873562992 * value)
	{
		___m_pattern_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_pattern_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
