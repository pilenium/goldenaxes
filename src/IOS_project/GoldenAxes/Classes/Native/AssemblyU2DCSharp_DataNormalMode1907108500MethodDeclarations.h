﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataNormalMode
struct DataNormalMode_t1907108500;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void DataNormalMode::.ctor()
extern "C"  void DataNormalMode__ctor_m1559120007 (DataNormalMode_t1907108500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DataNormalMode::GetType()
extern "C"  String_t* DataNormalMode_GetType_m910607250 (DataNormalMode_t1907108500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataNormalMode::GetRandomCount()
extern "C"  int32_t DataNormalMode_GetRandomCount_m1036559937 (DataNormalMode_t1907108500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DataNormalMode::GetCount(System.Single)
extern "C"  int32_t DataNormalMode_GetCount_m1647762599 (DataNormalMode_t1907108500 * __this, float ___current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DataNormalMode::GetRandomDelay()
extern "C"  float DataNormalMode_GetRandomDelay_m665011315 (DataNormalMode_t1907108500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DataNormalMode::GetDelay(System.Single)
extern "C"  float DataNormalMode_GetDelay_m2525530421 (DataNormalMode_t1907108500 * __this, float ___current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
