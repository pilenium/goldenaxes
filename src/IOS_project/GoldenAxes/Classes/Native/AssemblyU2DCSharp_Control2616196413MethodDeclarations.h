﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Control
struct Control_t2616196413;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void Control::.ctor()
extern "C"  void Control__ctor_m3714434766 (Control_t2616196413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Control::Awake()
extern "C"  void Control_Awake_m3952039985 (Control_t2616196413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Control::OnDestroy()
extern "C"  void Control_OnDestroy_m607880391 (Control_t2616196413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Control::Update()
extern "C"  void Control_Update_m910222847 (Control_t2616196413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Control::StartTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Control_StartTouch_m106134380 (Control_t2616196413 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Control::MovedTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Control_MovedTouch_m2670271099 (Control_t2616196413 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Control::EndTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Control_EndTouch_m4134908051 (Control_t2616196413 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Control::CanceldTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void Control_CanceldTouch_m44628068 (Control_t2616196413 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
