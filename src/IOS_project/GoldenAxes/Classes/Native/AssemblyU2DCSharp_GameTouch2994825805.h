﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Control
struct Control_t2616196413;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameTouch
struct  GameTouch_t2994825805  : public MonoBehaviour_t3012272455
{
public:
	// Control GameTouch::m_control
	Control_t2616196413 * ___m_control_2;
	// System.Int32 GameTouch::m_controlID
	int32_t ___m_controlID_3;

public:
	inline static int32_t get_offset_of_m_control_2() { return static_cast<int32_t>(offsetof(GameTouch_t2994825805, ___m_control_2)); }
	inline Control_t2616196413 * get_m_control_2() const { return ___m_control_2; }
	inline Control_t2616196413 ** get_address_of_m_control_2() { return &___m_control_2; }
	inline void set_m_control_2(Control_t2616196413 * value)
	{
		___m_control_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_control_2, value);
	}

	inline static int32_t get_offset_of_m_controlID_3() { return static_cast<int32_t>(offsetof(GameTouch_t2994825805, ___m_controlID_3)); }
	inline int32_t get_m_controlID_3() const { return ___m_controlID_3; }
	inline int32_t* get_address_of_m_controlID_3() { return &___m_controlID_3; }
	inline void set_m_controlID_3(int32_t value)
	{
		___m_controlID_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
