﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataAxe
struct DataAxe_t3107820260;

#include "codegen/il2cpp-codegen.h"

// System.Void DataAxe::.ctor()
extern "C"  void DataAxe__ctor_m2172165383 (DataAxe_t3107820260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataAxe::PreCalculate()
extern "C"  void DataAxe_PreCalculate_m3808590496 (DataAxe_t3107820260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataAxe::CalculateVelocity(System.Single)
extern "C"  void DataAxe_CalculateVelocity_m3396167907 (DataAxe_t3107820260 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DataAxe::Finalize()
extern "C"  void DataAxe_Finalize_m3104150011 (DataAxe_t3107820260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
