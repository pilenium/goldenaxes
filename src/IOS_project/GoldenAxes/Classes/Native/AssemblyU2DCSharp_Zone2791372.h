﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// DataZone
struct DataZone_t1853884054;
// UnityEngine.Transform
struct Transform_t284553113;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.Rigidbody
struct Rigidbody_t1972007546;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zone
struct  Zone_t2791372  : public MonoBehaviour_t3012272455
{
public:
	// DataZone Zone::m_data
	DataZone_t1853884054 * ___m_data_2;
	// UnityEngine.Transform Zone::m_tr
	Transform_t284553113 * ___m_tr_3;
	// UnityEngine.GameObject Zone::m_zone
	GameObject_t4012695102 * ___m_zone_4;
	// UnityEngine.Transform Zone::m_zone_tr
	Transform_t284553113 * ___m_zone_tr_5;
	// UnityEngine.Rigidbody Zone::m_rigidBody
	Rigidbody_t1972007546 * ___m_rigidBody_6;
	// System.Int32 Zone::m_count
	int32_t ___m_count_7;

public:
	inline static int32_t get_offset_of_m_data_2() { return static_cast<int32_t>(offsetof(Zone_t2791372, ___m_data_2)); }
	inline DataZone_t1853884054 * get_m_data_2() const { return ___m_data_2; }
	inline DataZone_t1853884054 ** get_address_of_m_data_2() { return &___m_data_2; }
	inline void set_m_data_2(DataZone_t1853884054 * value)
	{
		___m_data_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_data_2, value);
	}

	inline static int32_t get_offset_of_m_tr_3() { return static_cast<int32_t>(offsetof(Zone_t2791372, ___m_tr_3)); }
	inline Transform_t284553113 * get_m_tr_3() const { return ___m_tr_3; }
	inline Transform_t284553113 ** get_address_of_m_tr_3() { return &___m_tr_3; }
	inline void set_m_tr_3(Transform_t284553113 * value)
	{
		___m_tr_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_tr_3, value);
	}

	inline static int32_t get_offset_of_m_zone_4() { return static_cast<int32_t>(offsetof(Zone_t2791372, ___m_zone_4)); }
	inline GameObject_t4012695102 * get_m_zone_4() const { return ___m_zone_4; }
	inline GameObject_t4012695102 ** get_address_of_m_zone_4() { return &___m_zone_4; }
	inline void set_m_zone_4(GameObject_t4012695102 * value)
	{
		___m_zone_4 = value;
		Il2CppCodeGenWriteBarrier(&___m_zone_4, value);
	}

	inline static int32_t get_offset_of_m_zone_tr_5() { return static_cast<int32_t>(offsetof(Zone_t2791372, ___m_zone_tr_5)); }
	inline Transform_t284553113 * get_m_zone_tr_5() const { return ___m_zone_tr_5; }
	inline Transform_t284553113 ** get_address_of_m_zone_tr_5() { return &___m_zone_tr_5; }
	inline void set_m_zone_tr_5(Transform_t284553113 * value)
	{
		___m_zone_tr_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_zone_tr_5, value);
	}

	inline static int32_t get_offset_of_m_rigidBody_6() { return static_cast<int32_t>(offsetof(Zone_t2791372, ___m_rigidBody_6)); }
	inline Rigidbody_t1972007546 * get_m_rigidBody_6() const { return ___m_rigidBody_6; }
	inline Rigidbody_t1972007546 ** get_address_of_m_rigidBody_6() { return &___m_rigidBody_6; }
	inline void set_m_rigidBody_6(Rigidbody_t1972007546 * value)
	{
		___m_rigidBody_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_rigidBody_6, value);
	}

	inline static int32_t get_offset_of_m_count_7() { return static_cast<int32_t>(offsetof(Zone_t2791372, ___m_count_7)); }
	inline int32_t get_m_count_7() const { return ___m_count_7; }
	inline int32_t* get_address_of_m_count_7() { return &___m_count_7; }
	inline void set_m_count_7(int32_t value)
	{
		___m_count_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
