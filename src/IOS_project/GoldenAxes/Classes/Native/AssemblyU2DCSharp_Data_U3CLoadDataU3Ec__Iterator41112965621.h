﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// Data
struct Data_t2122699;

#include "mscorlib_System_Object837106420.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Data/<LoadData>c__Iterator4
struct  U3CLoadDataU3Ec__Iterator4_t1112965621  : public Il2CppObject
{
public:
	// System.String Data/<LoadData>c__Iterator4::<json>__0
	String_t* ___U3CjsonU3E__0_0;
	// System.Int32 Data/<LoadData>c__Iterator4::$PC
	int32_t ___U24PC_1;
	// System.Object Data/<LoadData>c__Iterator4::$current
	Il2CppObject * ___U24current_2;
	// Data Data/<LoadData>c__Iterator4::<>f__this
	Data_t2122699 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_U3CjsonU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadDataU3Ec__Iterator4_t1112965621, ___U3CjsonU3E__0_0)); }
	inline String_t* get_U3CjsonU3E__0_0() const { return ___U3CjsonU3E__0_0; }
	inline String_t** get_address_of_U3CjsonU3E__0_0() { return &___U3CjsonU3E__0_0; }
	inline void set_U3CjsonU3E__0_0(String_t* value)
	{
		___U3CjsonU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CjsonU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CLoadDataU3Ec__Iterator4_t1112965621, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CLoadDataU3Ec__Iterator4_t1112965621, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CLoadDataU3Ec__Iterator4_t1112965621, ___U3CU3Ef__this_3)); }
	inline Data_t2122699 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline Data_t2122699 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(Data_t2122699 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
