﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>
struct U3CGetEnumeratorU3Ec__Iterator3_t179375053;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TypewriterEffect_FadeEntry1095831350.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m276727948_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3__ctor_m276727948(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t179375053 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3__ctor_m276727948_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  FadeEntry_t1095831350  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m150729717_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m150729717(__this, method) ((  FadeEntry_t1095831350  (*) (U3CGetEnumeratorU3Ec__Iterator3_t179375053 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m150729717_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1575443940_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1575443940(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t179375053 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1575443940_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m649803728_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m649803728(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator3_t179375053 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m649803728_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3840836041_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3840836041(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t179375053 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3840836041_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<TypewriterEffect/FadeEntry>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m2218128185_gshared (U3CGetEnumeratorU3Ec__Iterator3_t179375053 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Reset_m2218128185(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t179375053 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Reset_m2218128185_gshared)(__this, method)
