﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoControl
struct AutoControl_t1228330126;

#include "codegen/il2cpp-codegen.h"

// System.Void AutoControl::.ctor()
extern "C"  void AutoControl__ctor_m1495262877 (AutoControl_t1228330126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoControl::Awake()
extern "C"  void AutoControl_Awake_m1732868096 (AutoControl_t1228330126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoControl::OnDestroy()
extern "C"  void AutoControl_OnDestroy_m4080215318 (AutoControl_t1228330126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoControl::Update()
extern "C"  void AutoControl_Update_m835371024 (AutoControl_t1228330126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
