﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectAnimation
struct EffectAnimation_t1201827027;

#include "codegen/il2cpp-codegen.h"

// System.Void EffectAnimation::.ctor()
extern "C"  void EffectAnimation__ctor_m1426528248 (EffectAnimation_t1201827027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectAnimation::Awake()
extern "C"  void EffectAnimation_Awake_m1664133467 (EffectAnimation_t1201827027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectAnimation::Update()
extern "C"  void EffectAnimation_Update_m2999564821 (EffectAnimation_t1201827027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EffectAnimation::get_isPlaying()
extern "C"  bool EffectAnimation_get_isPlaying_m2743687717 (EffectAnimation_t1201827027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
