﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<Console/Log>
struct Comparer_1_t454736916;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"

// System.Void System.Collections.Generic.Comparer`1<Console/Log>::.ctor()
extern "C"  void Comparer_1__ctor_m3899809745_gshared (Comparer_1_t454736916 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m3899809745(__this, method) ((  void (*) (Comparer_1_t454736916 *, const MethodInfo*))Comparer_1__ctor_m3899809745_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<Console/Log>::.cctor()
extern "C"  void Comparer_1__cctor_m152921596_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m152921596(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m152921596_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<Console/Log>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C"  int32_t Comparer_1_System_Collections_IComparer_Compare_m3229444350_gshared (Comparer_1_t454736916 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m3229444350(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparer_1_t454736916 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m3229444350_gshared)(__this, ___x0, ___y1, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<Console/Log>::get_Default()
extern "C"  Comparer_1_t454736916 * Comparer_1_get_Default_m2973077013_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m2973077013(__this /* static, unused */, method) ((  Comparer_1_t454736916 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m2973077013_gshared)(__this /* static, unused */, method)
