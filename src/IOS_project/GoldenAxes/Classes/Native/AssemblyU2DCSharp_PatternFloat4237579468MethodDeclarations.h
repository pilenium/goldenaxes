﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PatternFloat
struct PatternFloat_t4237579468;

#include "codegen/il2cpp-codegen.h"

// System.Void PatternFloat::.ctor()
extern "C"  void PatternFloat__ctor_m894976847 (PatternFloat_t4237579468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PatternFloat::Init(System.Int32,System.Boolean)
extern "C"  void PatternFloat_Init_m2546433479 (PatternFloat_t4237579468 * __this, int32_t ___count0, bool ___isMonster1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
