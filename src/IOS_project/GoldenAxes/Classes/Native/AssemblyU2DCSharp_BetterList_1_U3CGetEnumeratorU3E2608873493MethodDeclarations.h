﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>
struct U3CGetEnumeratorU3Ec__Iterator3_t2608873493;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector43525329790.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3__ctor_m521920477_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3__ctor_m521920477(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3__ctor_m521920477_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector4_t3525329790  U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2412787364_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2412787364(__this, method) ((  Vector4_t3525329790  (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m2412787364_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1893303273_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1893303273(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1893303273_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m3962693623_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m3962693623(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_MoveNext_m3962693623_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3247655130_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3247655130(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Dispose_m3247655130_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator3<UnityEngine.Vector4>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator3_Reset_m2463320714_gshared (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator3_Reset_m2463320714(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator3_t2608873493 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator3_Reset_m2463320714_gshared)(__this, method)
