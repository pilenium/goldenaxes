﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwipeControl
struct SwipeControl_t1555979491;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"

// System.Void SwipeControl::.ctor()
extern "C"  void SwipeControl__ctor_m2727372056 (SwipeControl_t1555979491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeControl::Awake()
extern "C"  void SwipeControl_Awake_m2964977275 (SwipeControl_t1555979491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeControl::OnDestroy()
extern "C"  void SwipeControl_OnDestroy_m4210683409 (SwipeControl_t1555979491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeControl::Update()
extern "C"  void SwipeControl_Update_m376049909 (SwipeControl_t1555979491 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeControl::StartTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void SwipeControl_StartTouch_m3174517858 (SwipeControl_t1555979491 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeControl::MovedTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void SwipeControl_MovedTouch_m1443687281 (SwipeControl_t1555979491 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeControl::EndTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void SwipeControl_EndTouch_m182798089 (SwipeControl_t1555979491 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeControl::CanceldTouch(System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  void SwipeControl_CanceldTouch_m2413585370 (SwipeControl_t1555979491 * __this, int32_t ___id0, Vector2_t3525329788  ___position1, Vector2_t3525329788  ___deltaPosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeControl::throwAxe(System.Single,System.Single)
extern "C"  void SwipeControl_throwAxe_m1665330428 (SwipeControl_t1555979491 * __this, float ___position0, float ___power1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
