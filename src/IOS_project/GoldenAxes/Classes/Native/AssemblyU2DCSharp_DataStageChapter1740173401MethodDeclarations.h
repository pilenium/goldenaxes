﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DataStageChapter
struct DataStageChapter_t1740173401;
// DataStage
struct DataStage_t1629502804;

#include "codegen/il2cpp-codegen.h"

// System.Void DataStageChapter::.ctor()
extern "C"  void DataStageChapter__ctor_m1999800930 (DataStageChapter_t1740173401 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DataStage DataStageChapter::GetStageData(System.Int32)
extern "C"  DataStage_t1629502804 * DataStageChapter_GetStageData_m1832735364 (DataStageChapter_t1740173401 * __this, int32_t ___stage0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
