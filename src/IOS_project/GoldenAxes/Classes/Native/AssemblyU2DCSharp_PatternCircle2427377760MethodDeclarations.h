﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PatternCircle
struct PatternCircle_t2427377760;

#include "codegen/il2cpp-codegen.h"

// System.Void PatternCircle::.ctor()
extern "C"  void PatternCircle__ctor_m3435721995 (PatternCircle_t2427377760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
