﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FeverLights
struct FeverLights_t2165513633;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"

// System.Void FeverLights::.ctor()
extern "C"  void FeverLights__ctor_m1266229994 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights::Awake()
extern "C"  void FeverLights_Awake_m1503835213 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights::OnDestroy()
extern "C"  void FeverLights_OnDestroy_m1657500387 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights::Init()
extern "C"  void FeverLights_Init_m269477418 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights::Clear()
extern "C"  void FeverLights_Clear_m2967330581 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FeverLights::ChangeLight()
extern "C"  Il2CppObject * FeverLights_ChangeLight_m2739986646 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights::SetIdle()
extern "C"  void FeverLights_SetIdle_m1306842142 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights::SetFlicker(System.Int32)
extern "C"  void FeverLights_SetFlicker_m2686271841 (FeverLights_t2165513633 * __this, int32_t ___times0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights::UpdateFlicker()
extern "C"  void FeverLights_UpdateFlicker_m2483547703 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights::UpdateIdle()
extern "C"  void FeverLights_UpdateIdle_m2390725463 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FeverLights::UpdateRotate()
extern "C"  void FeverLights_UpdateRotate_m3880325790 (FeverLights_t2165513633 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
